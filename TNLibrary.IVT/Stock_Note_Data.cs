﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.IVT
{
    public class Stock_Note_Data
    {
        #region [Input]
        public static DataTable NoteInput()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber FROM IVT_Stock_Note_Input A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable NoteInput(int CustomerKey, int FinancialYear)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM IVT_Stock_Note_Input "
                + " WHERE CustomerKey = @CustomerKey AND YEAR(NoteDate) = @FinancialYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                nCommand.Parameters.Add("@FinancialYear", SqlDbType.NVarChar).Value = FinancialYear;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable NoteInput(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "IVT_Stock_Note_Input_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = 0;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable NoteInput(DateTime FromDate, DateTime ToDate, int WarehouseKey, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "IVT_Stock_Note_Input_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = WarehouseKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
 
        public static string GetAutoNoteInputID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT MAX(RIGHT(NoteID,@LenIDRight)+1) FROM IVT_Stock_Note_Input "
                        + " WHERE LEFT(NoteID,@LenIDLeft) =  @ID AND LEN(NoteID) = @LenID "
                        + "AND ISNUMERIC(RIGHT(NoteID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            ID = IDLeft + nResult.ToString().PadLeft(IDRight.Length, '0');
            return ID;

        }
        #endregion

        #region [Output]
        public static DataTable NoteOutput()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber FROM IVT_Stock_Note_Output A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable NoteOutput(int CustomerKey, int FinancialYear)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM IVT_Stock_Note_Output "
                + " WHERE CustomerKey = @CustomerKey AND YEAR(NoteDate) = @FinancialYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                nCommand.Parameters.Add("@FinancialYear", SqlDbType.NVarChar).Value = FinancialYear;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable NoteOutput(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "IVT_Stock_Note_Output_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = 0;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable NoteOutput(DateTime FromDate, DateTime ToDate, int WarehouseKey, string CustomerID, string CustomerName,string CreatedBy)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "IVT_Stock_Note_Output_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = WarehouseKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static string GetAutoNoteOutputID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(NoteID,@LenIDRight)+1) FROM IVT_Stock_Note_Output "
                        + " WHERE LEFT(NoteID,@LenIDLeft) =  @ID AND LEN(NoteID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(NoteID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            ID = IDLeft + nResult.ToString().PadLeft(IDRight.Length, '0');
            return ID;

        }
        #endregion

        #region [Inventory]
        public static DataTable Inventory(int CustomerKey, int Year)
        {
            DataTable nTable = new DataTable();

            string nSQL = "IVT_Inventory_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = CustomerKey;
                nCommand.Parameters.Add("@YearView", SqlDbType.NVarChar).Value = Year;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Inventory(string CloseMonth, int WarehouseKey,DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Inventory_FromDateToDate_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        #endregion

        public static int CheckExistWarehouse(int WarehouseKey)
        {
            int nResult = 0;
            string nSQL = "IVT_CheckExistWarehouse";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.CommandType = CommandType.StoredProcedure;
            nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();
            return nResult;
        }
    }
}
