﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.IVT
{
    public class Stock_Note_Output_Info
    {
        private int m_NoteKey = 0;
        private string m_NoteID = "";

        private DateTime m_NoteDate = DateTime.Now;

        private string m_NoteDescription = "";
        private int m_WarehouseKey = 0;
        private string m_WarehouseName = "";

        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";

        private string m_Deliverer = "";
        private string m_DocumentAttached = "";
        private DateTime m_DocumentDate = DateTime.MinValue;
        private string m_DocumentOrigin = "";

        private double m_AmountCurrencyMain = 0;

        private bool m_IsInternal = false;

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";

        public Stock_Note_Output_Info()
        {

        }
        #region [ Constructor Get Information ]
        public Stock_Note_Output_Info(int NoteKey)
        {
            string nSQL = "SELECT A.*,B.WarehouseName,C.CustomerID,C.CustomerName FROM IVT_Stock_Note_Output A "
                        + " INNER JOIN IVT_Warehouse B ON A.WarehouseKey = B.WarehouseKey "
                        + " LEFT JOIN CRM_Customers C ON A.CustomerKey = C.CustomerKey "
                        + " WHERE NoteKey = @NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = NoteKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_NoteKey = int.Parse(nReader["NoteKey"].ToString());
                    m_NoteID = nReader["NoteID"].ToString();
                    m_NoteDescription = nReader["NoteDescription"].ToString();
                    m_NoteDate = (DateTime)nReader["NoteDate"];

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());
                    m_WarehouseName = nReader["WarehouseName"].ToString();

                    m_Deliverer = nReader["Deliverer"].ToString();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_IsInternal = (bool)nReader["IsInternal"];

                    m_DocumentAttached = nReader["DocumentAttached"].ToString();

                    string nDocumentDate = nReader["DocumentDate"].ToString();
                    if (nDocumentDate.Length > 0)
                        m_DocumentDate = (DateTime)nReader["DocumentDate"];
                    else
                        m_DocumentDate = DateTime.MinValue;

                    m_DocumentOrigin = nReader["DocumentOrigin"].ToString();

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public Stock_Note_Output_Info(string NoteID)
        {
            string nSQL = "SELECT A.*,B.WarehouseName,C.CustomerID,C.CustomerName FROM IVT_Stock_Note_Output A "
                        + " INNER JOIN IVT_Warehouse B ON A.WarehouseKey = B.WarehouseKey "
                        + " LEFT JOIN CRM_Customers C ON A.CustomerKey = C.CustomerKey "
                        + " WHERE NoteID = @NoteID";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@NoteID", SqlDbType.NVarChar).Value = NoteID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_NoteKey = int.Parse(nReader["NoteKey"].ToString());
                    m_NoteID = nReader["NoteID"].ToString();
                    m_NoteDescription = nReader["NoteDescription"].ToString();
                    m_NoteDate = (DateTime)nReader["NoteDate"];

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());
                    m_WarehouseName = nReader["WarehouseName"].ToString();

                    m_Deliverer = nReader["Deliverer"].ToString();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_IsInternal = (bool)nReader["IsInternal"];

                    m_DocumentAttached = nReader["DocumentAttached"].ToString();

                    string nDocumentDate = nReader["DocumentDate"].ToString();
                    if (nDocumentDate.Length > 0)
                        m_DocumentDate = (DateTime)nReader["DocumentDate"];
                    else
                        m_DocumentDate = DateTime.MinValue;

                    m_DocumentOrigin = nReader["DocumentOrigin"].ToString();

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public void GetProduct_Output_Info(int NoteKey)
        {
            string nSQL = "SELECT A.*,B.WarehouseName,C.CustomerID,C.CustomerName FROM IVT_Stock_Note_Output A "
                       + " INNER JOIN IVT_Warehouse B ON A.WarehouseKey = B.WarehouseKey "
                       + " LEFT JOIN CRM_Customers C ON A.CustomerKey = C.CustomerKey "
                       + " WHERE NoteKey = @NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = NoteKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_NoteKey = int.Parse(nReader["NoteKey"].ToString());
                    m_NoteID = nReader["NoteID"].ToString();
                    m_NoteDescription = nReader["NoteDescription"].ToString();
                    m_NoteDate = (DateTime)nReader["NoteDate"];

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());
                    m_WarehouseName = nReader["WarehouseName"].ToString();

                    m_Deliverer = nReader["Deliverer"].ToString();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_IsInternal = (bool)nReader["IsInternal"];

                    m_DocumentAttached = nReader["DocumentAttached"].ToString();

                    string nDocumentDate = nReader["DocumentDate"].ToString();
                    if (nDocumentDate.Length > 0)
                        m_DocumentDate = (DateTime)nReader["DocumentDate"];
                    else
                        m_DocumentDate = DateTime.MinValue;

                    m_DocumentOrigin = nReader["DocumentOrigin"].ToString();

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]
        public int NoteKey
        {
            get { return m_NoteKey; }
            set { m_NoteKey = value; }
        }
        public string NoteID
        {
            get { return m_NoteID; }
            set { m_NoteID = value; }
        }
        public string NoteDescription
        {
            get { return m_NoteDescription; }
            set { m_NoteDescription = value; }
        }
        public DateTime NoteDate
        {
            get { return m_NoteDate; }
            set { m_NoteDate = value; }
        }
        public int WarehouseKey
        {
            get { return m_WarehouseKey; }
            set { m_WarehouseKey = value; }
        }
        public string WarehouseName
        {
            get { return m_WarehouseName; }
            set { m_WarehouseName = value; }
        }
        public string Deliverer
        {
            get { return m_Deliverer; }
            set { m_Deliverer = value; }
        }
        public string DocumentAttached
        {
            get { return m_DocumentAttached; }
            set { m_DocumentAttached = value; }
        }
        public DateTime DocumentDate
        {
            get { return m_DocumentDate; }
            set { m_DocumentDate = value; }
        }
        public string DocumentOrigin
        {
            get { return m_DocumentOrigin; }
            set { m_DocumentOrigin = value; }
        }

        public double AmountCurrencyMain
        {
            get { return m_AmountCurrencyMain; }
            set { m_AmountCurrencyMain = value; }
        }

        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }
        public bool IsInternal
        {
            get { return m_IsInternal; }
            set { m_IsInternal = value; }
        }

        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            string nResult = "";
            string nSQL = "INSERT INTO IVT_Stock_Note_Output( "
                             + " NoteID, NoteDescription, NoteDate, WarehouseKey, "
                             + " Deliverer,DocumentAttached, DocumentDate, DocumentOrigin,"
                             + " AmountCurrencyMain, CustomerKey,IsInternal,"
                             + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                             + " VALUES(@NoteID, @NoteDescription,  @NoteDate,@WarehouseKey,"
                             + " @Deliverer,@DocumentAttached, @DocumentDate, @DocumentOrigin,"
                             + " @AmountCurrencyMain, @CustomerKey,@IsInternal,"
                             + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                             + " SELECT NoteKey FROM IVT_Stock_Note_Output WHERE NoteKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@NoteID", SqlDbType.NVarChar).Value = m_NoteID;
                nCommand.Parameters.Add("@NoteDescription", SqlDbType.NText).Value = m_NoteDescription;
                nCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = m_NoteDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;

                nCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = m_Deliverer;
                nCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = m_DocumentAttached;

                if (m_DocumentDate == DateTime.MinValue)
                    nCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = m_DocumentDate;

                nCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = m_DocumentOrigin;
                nCommand.Parameters.Add("@IsInternal", SqlDbType.Bit).Value = m_IsInternal;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                int nNoteKey = 0;
                int.TryParse(nResult, out nNoteKey);
                m_NoteKey = nNoteKey;

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Update()
        {
            string nResult = "";
            string nSQL = "UPDATE IVT_Stock_Note_Output SET "
                            + " NoteID = @NoteID,"
                            + " NoteDescription = @NoteDescription, "
                            + " NoteDate = @NoteDate ,"
                            + " WarehouseKey = @WarehouseKey ,"
                            + " CustomerKey = @CustomerKey ,"

                            + " Deliverer = @Deliverer ,"
                            + " DocumentAttached = @DocumentAttached ,"
                            + " DocumentDate = @DocumentDate ,"
                            + " DocumentOrigin = @DocumentOrigin ,"

                            + " AmountCurrencyMain = @AmountCurrencyMain ,"
                            + " IsInternal = @IsInternal, "
                            + " ModifiedBy= @ModifiedBy,"
                            + " ModifiedDateTime=getdate() "
                            + " WHERE NoteKey = @NoteKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = m_NoteKey;
                nCommand.Parameters.Add("@NoteID", SqlDbType.NVarChar).Value = m_NoteID;
                nCommand.Parameters.Add("@NoteDescription", SqlDbType.NText).Value = m_NoteDescription;
                nCommand.Parameters.Add("@NoteDate", SqlDbType.DateTime).Value = m_NoteDate;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;

                nCommand.Parameters.Add("@Deliverer", SqlDbType.NVarChar).Value = m_Deliverer;
                nCommand.Parameters.Add("@DocumentAttached", SqlDbType.NVarChar).Value = m_DocumentAttached;
                if (m_DocumentDate == DateTime.MinValue)
                    nCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@DocumentDate", SqlDbType.DateTime).Value = m_DocumentDate;
                nCommand.Parameters.Add("@DocumentOrigin", SqlDbType.NVarChar).Value = m_DocumentOrigin;

                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;
                nCommand.Parameters.Add("@IsInternal", SqlDbType.Bit).Value = m_IsInternal;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult = "";
            if (m_NoteKey == 0)
              nResult =  Create();
            else
               nResult= Update();

            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM IVT_Stock_Note_Output WHERE NoteKey = @NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = m_NoteKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
        #endregion
    }
}
