﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Data;

using TNLibrary.SYS;
using TNLibrary.PUL;

namespace TNLibrary.IVT.Forms
{
    public class DataGridViewProducts : DataGridView
    {

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private ArrayList mListProduct = new ArrayList();
        private int mMaxRow = 50;
        private DataTable mItemTable = new DataTable();

        private Image mImageHeader;

        public DataGridViewProducts()
        {
        }

        #region [ Properties ]

        public void SetupLayoutGridView(bool ForEMS)
        {
            // Setup Column 
            base.Columns.Add("No", "");
            base.Columns.Add("ProductID", "");
            base.Columns.Add("ProductName", "");
            base.Columns.Add("Unit", "");
            base.Columns.Add("Quantity", "");
            base.Columns.Add("Price", "");
            base.Columns.Add("CurrencyID", "");
            base.Columns.Add("CurrencyRate", "");
            base.Columns.Add("CurrencyForeign", "");
            base.Columns.Add("CurrencyMain", "");

            base.Columns["No"].Width = 40;
            base.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            base.Columns["ProductID"].Width = 90;
            base.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            base.Columns["ProductName"].Width = 215;
            base.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            base.Columns["ProductName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            base.Columns["Unit"].Width = 60;
            base.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            base.Columns["Quantity"].Width = 60;
            base.Columns["Quantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["Price"].Width = 100;
            base.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["CurrencyID"].Width = 60;
            base.Columns["CurrencyID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            base.Columns["CurrencyRate"].Width = 65;
            base.Columns["CurrencyRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["CurrencyForeign"].Width = 130;
            base.Columns["CurrencyForeign"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["CurrencyMain"].Width = 130;
            base.Columns["CurrencyMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            base.BackgroundColor = Color.White;
            base.GridColor = Color.FromArgb(227, 239, 255);
            base.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            base.DefaultCellStyle.ForeColor = Color.Navy;
            base.DefaultCellStyle.Font = new Font("Arial", 9);

            base.AllowUserToResizeRows = false;
            base.AllowUserToResizeColumns = true;

            base.RowHeadersVisible = false;
            base.AllowUserToAddRows = true;
            base.AllowUserToDeleteRows = true;

            //// setup Height Header
            base.ColumnHeadersHeight = base.ColumnHeadersHeight * 2;
            base.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            base.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= mMaxRow; i++)
            {
                base.Rows.Add();
            }
        }
        public void SetupLayoutGridView()
        {
            // Setup Column 
            base.Columns.Add("No", "");
            base.Columns.Add("ProductID", "");
            base.Columns.Add("ProductName", "");
            base.Columns.Add("Unit", "");
            base.Columns.Add("Quantity", "");
            base.Columns.Add("Price", "");
            base.Columns.Add("CurrencyID", "");
            base.Columns.Add("CurrencyRate", "");
            base.Columns.Add("CurrencyForeign", "");
            base.Columns.Add("CurrencyMain", "");

            base.Columns["No"].Width = 40;
            base.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            base.Columns["ProductID"].Width = 90;
            base.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            base.Columns["ProductName"].Width = 310;
            base.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          //  base.Columns["ProductName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            base.Columns["Unit"].Width = 100;
            base.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            base.Columns["Quantity"].Width = 100;
            base.Columns["Quantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["Price"].Width = 120;
            base.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            base.Columns["CurrencyID"].Width = 0;
            base.Columns["CurrencyID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            base.Columns["CurrencyID"].Visible = false;

            base.Columns["CurrencyRate"].Width = 0;
            base.Columns["CurrencyRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            base.Columns["CurrencyRate"].Visible = false;

            base.Columns["CurrencyForeign"].Width = 0;
            base.Columns["CurrencyForeign"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            base.Columns["CurrencyForeign"].Visible = false;

            base.Columns["CurrencyMain"].Width = 180;
            base.Columns["CurrencyMain"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            base.BackgroundColor = Color.White;
            base.GridColor = Color.FromArgb(227, 239, 255);
            base.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            base.DefaultCellStyle.ForeColor = Color.Navy;
            base.DefaultCellStyle.Font = new Font("Arial", 9);

            base.AllowUserToResizeRows = false;
            base.AllowUserToResizeColumns = true;

            base.RowHeadersVisible = false;
            base.AllowUserToAddRows = true;
            base.AllowUserToDeleteRows = true;

            //// setup Height Header
            base.ColumnHeadersHeight = base.ColumnHeadersHeight * 3/2;
            base.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            base.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= mMaxRow; i++)
            {
                base.Rows.Add();
            }
        }
        public ArrayList ListProduct
        {
            set
            {
                if (base.ColumnCount > 0)
                    ClearProducts();
                mListProduct = value;
                int i = 0;
                foreach (InventoryProduct nProduct in mListProduct)
                {
                    InsertInventoryProductIntoRow(nProduct, i);
                    i++;
                }
            }
            get
            {
                mListProduct = new ArrayList();

                for (int i = 0; i < base.Rows.Count; i++)
                {
                    if (!IsRowEmpty(i) && !IsRowError(i))
                        mListProduct.Add(InventoryProductInRow(i));
                }
                return mListProduct;

            }
        }
        public ArrayList ListProductInventory
        {
            set
            {
                if (base.ColumnCount > 0)
                    ClearProducts();
                mListProduct = value;
                int i = 0;
                foreach (InventoryProduct nProduct in mListProduct)
                {
                    InsertInventoryProductIntoRow(nProduct, i);
                    i++;
                }
            }
            get
            {
                mListProduct = new ArrayList();

                for (int i = 0; i < base.Rows.Count; i++)
                {
                    if (!IsRowEmpty(i) && !IsRowError(i))
                    {
                        InventoryProduct nProduct = InventoryProductInRow(i);
                        nProduct.GetProduct_Info();
                        if(nProduct.TypeKey == 1)
                            mListProduct.Add(nProduct);
                        
                    }
                       
                }
                return mListProduct;

            }
        }


        public int RowDisplay
        {
            set
            {
                mMaxRow = value;
            }
        }
        public Image ImageHeader
        {
            set
            {
                mImageHeader = value;
            }
        }
        public bool IsError
        {
            get
            {
                for (int i = 0; i < base.Rows.Count; i++)
                {
                    if (IsRowError(i))
                        return true;
                }
                return false;
            }
        }
        public void ClearProducts()
        {
            base.Rows.Clear();
            mListProduct = new ArrayList();
            for (int i = 1; i <= mMaxRow; i++)
            {
                base.Rows.Add();
            }
        }
        #endregion

        #region  [ Override ]

        protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            base.OnEditingControlShowing(e);
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (base.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductID FROM PUL_Products");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBoxMutiColumn(te, "SELECT ProductName,ProductID FROM PUL_Products");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4: // Unit Price
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6: // loai tien te
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT CurrencyID FROM SYS_Currency");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 7: // tỷ giá
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 8: // ngoai te
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 9: //nguyen te
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                default:
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

                    break;
            }

        }
        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            base.OnCellEndEdit(e);
            DataGridViewRow nRowEdit = base.Rows[e.RowIndex];


            switch (e.ColumnIndex)
            {
                case 1: // thong tin ve san pham
                    if (nRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        nRowEdit.Cells["ProductID"].ErrorText = "";
                        nRowEdit.Cells[2].ReadOnly = false;
                    }
                    else
                    {
                        string nProductID = nRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;

                        Product_Info nProduct = new Product_Info(nProductID);
                        if (nProduct.Key > 0)
                        {
                            nRowEdit.Cells["ProductID"].Tag = nProduct.Key;
                            nRowEdit.Cells["ProductName"].Value = nProduct.Name;
                            nRowEdit.Cells["Unit"].Value = nProduct.Unit;
                            nRowEdit.Cells["Price"].Value = nProduct.PurchasePrice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyID"].Value = mCurrencyMain;

                            int nHasAtRow = CheckProductID(nProductID.Trim(), e.RowIndex);

                            if (nHasAtRow == -1)
                                nRowEdit.Cells["ProductID"].ErrorText = "";

                            else
                            {
                                int n = nHasAtRow + 1;
                                nRowEdit.Cells["ProductID"].ErrorText = "Ðã có sản phẩm này ở dòng thứ " + n.ToString();

                            }

                        }
                        else
                        {
                            nRowEdit.Cells["ProductID"].Tag = 0;
                            nRowEdit.Cells["ProductName"].Value = "Không có sản phẩm này";
                            nRowEdit.Cells["Unit"].Value = "";
                            nRowEdit.Cells["Price"].Value = "";
                            nRowEdit.Cells["CurrencyID"].Value = "";
                            nRowEdit.Cells["ProductID"].ErrorText = " Error";
                        }


                        nRowEdit.Cells[e.ColumnIndex].Value = nProductID;
                        nRowEdit.Cells[2].ReadOnly = true;
                    }
                    base.AutoResizeRow(e.RowIndex);
                    break;
                case 2:
                    string nProductName = nRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;
                    string[] nValue = nProductName.Split(':');
                    string n_ProductID = nValue[1].Trim();
                    //nRowEdit.Cells[e.ColumnIndex].Value = nValue[0];
                    if (nValue.Length > 1)
                    {

                        Product_Info nProduct = new Product_Info(n_ProductID);
                        if (nProduct.Key > 0)
                        {
                            nRowEdit.Cells["ProductID"].Tag = nProduct.Key;
                            nRowEdit.Cells["ProductID"].Value = nProduct.ID;
                            nRowEdit.Cells["ProductName"].Value = nProduct.Name;
                            nRowEdit.Cells["Unit"].Value = nProduct.Unit;
                            nRowEdit.Cells["Price"].Value = nProduct.PurchasePrice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyID"].Value = mCurrencyMain;

                            int nHasAtRow = CheckProductID(n_ProductID.Trim(), e.RowIndex);

                            if (nHasAtRow == -1)
                                nRowEdit.Cells["ProductID"].ErrorText = "";

                            else
                            {
                                int n = nHasAtRow + 1;
                                nRowEdit.Cells["ProductID"].ErrorText = "Ðã có sản phẩm này ở dòng thứ " + n.ToString();

                            }
                        }
                    }
                    base.AutoResizeRow(e.RowIndex);

                    break;
                case 4: // thong tin ve so luong
                    float nQuantity = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (float.TryParse(nRowEdit.Cells["Quantity"].Value.ToString(), NumberStyles.Any, mFormatProviderDecimal, out nQuantity))
                        {
                            nRowEdit.Cells["Quantity"].Value = nQuantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                            nRowEdit.Cells["Quantity"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["Quantity"].ErrorText = "Sai định dạng số lượng";
                    }
                    CaculateAmount(e.RowIndex);
                    break;
                case 5: // thong tin ve don gia
                    double nUnitPrice = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (double.TryParse(nRowEdit.Cells["Price"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nUnitPrice))
                        {
                            nRowEdit.Cells["Price"].Value = nUnitPrice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["Price"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["Price"].ErrorText = "Sai định dạng đơn giá";
                    }
                    CaculateAmount(e.RowIndex);
                    break;
                case 6:// thong tin ve loai tien te
                    nRowEdit.Cells["CurrencyID"].Value = nRowEdit.Cells["CurrencyID"].Value.ToString().ToUpper();
                    CaculateAmount(e.RowIndex);
                    break;
                case 7: // thong tin ve tỷ giá
                    double nAmountCurrencyRate = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (double.TryParse(nRowEdit.Cells["CurrencyRate"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nAmountCurrencyRate))
                        {
                            nRowEdit.Cells["CurrencyRate"].Value = nAmountCurrencyRate.ToString(mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyRate"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["CurrencyRate"].ErrorText = "Sai định dạng";
                    }
                    CaculateAmount(e.RowIndex);
                    break;
                case 8: // thong tin ve thanh tien ngoai te
                    double nAmountCurrencyForeign = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["CurrencyForeign"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nAmountCurrencyForeign))
                        {
                            nRowEdit.Cells["CurrencyForeign"].Value = nAmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyForeign"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["CurrencyForeign"].ErrorText = "Sai định dạng ngoại tệ";
                    }
                    break;
                case 9: // thong tin ve thanh tien nguyên t?
                    double nAmountCurrencyMain = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (double.TryParse(nRowEdit.Cells["CurrencyMain"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nAmountCurrencyMain))
                        {
                            nRowEdit.Cells["CurrencyMain"].Value = nAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyMain"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["CurrencyMain"].ErrorText = "Sai định dạng tiền tệ";
                    }
                    break;
            }


        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.Delete)
            {
                base.Rows.Remove(base.CurrentRow);
                base.Rows.Add();
            }

        }
        protected override void OnRowValidated(DataGridViewCellEventArgs e)
        {
            base.OnRowValidated(e);
            int nRowIndex = base.CurrentRow.Index;
            if (!IsRowEmpty(nRowIndex))
            {
                CheckRowError(nRowIndex);
                CaculateAmount(nRowIndex);
            }

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Bitmap drawing = null;

            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);
            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(base.Width, base.ColumnHeadersHeight, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            if (base.Columns["CurrencyID"] != null && base.Columns["CurrencyID"].Visible)
            {
                string[] TitleMerge = { "Thành tiền" };
                string[] TitleColumn = { "STT", "Mã ", "Tên nguyên vật liệu", "Đơn vị tính", "Số lượng", "Đơn giá", "Tiền tệ", "Tỷ giá", "Ngoại tệ", mCurrencyMain };

                int nColumnTotal = base.ColumnCount;
                // bat dau merge column
                int nColumnBeginMerge = 8;

                for (int i = 0; i < nColumnTotal; i++)
                {
                    Rectangle r1 = base.GetCellDisplayRectangle(i, -1, true);

                    r1.X += 1;
                    r1.Y += 1;

                    if (i > 7) // binh thuong
                    {
                        r1.Height = r1.Height / 2 - 2;
                        r1.Width = r1.Width - 2;
                        r1.Y = r1.Height + 3;
                        g.DrawImage(mImageHeader, r1);
                    }
                    else // merge
                    {
                        r1.Width = r1.Width - 2;
                        r1.Height = r1.Height - 2;
                        g.DrawImage(mImageHeader, r1);
                    }
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;

                    g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

                }

                int k = 0;

                for (int j = nColumnBeginMerge; j < nColumnTotal; j += 2)
                {

                    Rectangle r1 = base.GetCellDisplayRectangle(j, -1, true);

                    int w2 = base.GetCellDisplayRectangle(j + 1, -1, true).Width;

                    r1.X += 1;
                    r1.Y += 1;

                    r1.Width = r1.Width + w2 - 2;
                    r1.Height = r1.Height / 2 - 2;

                    g.DrawImage(mImageHeader, r1);

                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;

                    g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                    k++;
                }
            }
            else
            {
                string[] TitleColumn = { "STT", "Mã ", "Tên nguyên vật liệu", "Đơn vị tính", "Số lượng", "Đơn giá", "Tiền tệ", "Tỷ giá", "Ngoại tệ", mCurrencyMain };

                int nColumnTotal = base.ColumnCount;
                // bat dau merge column

                for (int i = 0; i < nColumnTotal; i++)
                {
                    Rectangle r1 = base.GetCellDisplayRectangle(i, -1, true);

                    r1.X += 1;
                    r1.Y += 1;

                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;
                    g.DrawImage(mImageHeader, r1);

                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;

                    g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

                }

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        protected override void OnRowEnter(DataGridViewCellEventArgs e)
        {
            base.OnRowEnter(e);
            base.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        protected override void OnRowLeave(DataGridViewCellEventArgs e)
        {
            base.OnRowLeave(e);
            base.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
        }
        #endregion

        #region [ Function ]
        private int CheckProductID(string ProductID, int nRowCurrency)
        {
            int i = 1;
            foreach (DataGridViewRow nRow in base.Rows)
            {
                if (nRow.Cells["ProductID"].Value != null)
                {
                    nRow.Cells["No"].Value = i;
                    if (nRow.Index != nRowCurrency && nRow.Cells["ProductID"].Value.ToString().Trim() == ProductID)
                    {
                        return nRow.Index;
                    }
                    i++;
                }
            }
            return -1;
        }
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private bool IsRowEmpty(int RowIndex)
        {
            for (int i = 0; i < base.ColumnCount; i++)
            {
                if (base.Rows[RowIndex].Cells[i].Value != null)
                    return false;
            }
            return true;
        }
        private bool IsRowError(int RowIndex)
        {
            for (int i = 1; i < base.ColumnCount; i++)
            {
                if (base.Rows[RowIndex].Cells[i].ErrorText.Length > 0)
                    return true;
            }
            return false;
        }
        private void CheckRowError(int RowIndex)
        {
            DataGridViewRow nRow = base.Rows[RowIndex];

            if (nRow.Cells["ProductID"].Value == null && nRow.Cells["ProductName"].Value == null)
                nRow.Cells["ProductID"].ErrorText = " Phải nhập sản phẩm vào";

            if (nRow.Cells["ProductID"].Value == null && nRow.Cells["ProductName"].Value != null)
            {
                nRow.Cells["ProductID"].Tag = 0;
                nRow.Cells["ProductID"].ErrorText = "";
            }

            if (nRow.Cells["Quantity"].Value == null)
                nRow.Cells["Quantity"].ErrorText = " Phải nhập số lượng vào";

            if (nRow.Cells["Price"].Value == null)
                nRow.Cells["Price"].ErrorText = " Phải nhập hóa đơn vào";
            else
                nRow.Cells["Price"].ErrorText = "";

            if (nRow.Cells["CurrencyForeign"].Value == null)
                nRow.Cells["CurrencyForeign"].ErrorText = "Phải nhập số tiền vào";
            else
                nRow.Cells["CurrencyForeign"].ErrorText = "";

            if (nRow.Cells["CurrencyID"].Value == null)
                nRow.Cells["CurrencyID"].ErrorText = "Phải nhập loại tiền tệ vào";
            else
                nRow.Cells["CurrencyID"].ErrorText = "";

            if (nRow.Cells["CurrencyMain"].Value == null)
                nRow.Cells["CurrencyMain"].ErrorText = "Phải nhập số tiền vào";
            else
                nRow.Cells["CurrencyMain"].ErrorText = "";
        }

        private InventoryProduct InventoryProductInRow(int RowIndex)
        {
            InventoryProduct nProduct = new InventoryProduct();
            DataGridViewRow nRow = base.Rows[RowIndex];
            if (nRow.Cells["ProductID"].Tag != null && (int)nRow.Cells["ProductID"].Tag > 0)
            {
                nProduct.Key = (int)nRow.Cells["ProductID"].Tag;
                nProduct.ID = nRow.Cells["ProductID"].Value.ToString();
                nProduct.Name = nRow.Cells["ProductName"].Value.ToString();
            }

            nProduct.ItemName = nRow.Cells["ProductName"].Value.ToString();

            if (nRow.Cells["ProductName"].Value != null)
            {
                if (nRow.Cells["Unit"].Value == null)
                    nProduct.ItemUnit = "";
                else
                    nProduct.ItemUnit = nRow.Cells["Unit"].Value.ToString();
            }
            nProduct.Quantity = float.Parse(nRow.Cells["Quantity"].Value.ToString(), mFormatProviderDecimal);
            nProduct.UnitPrice = double.Parse(nRow.Cells["Price"].Value.ToString(), mFormatProviderCurrency);
            nProduct.CurrencyID = nRow.Cells["CurrencyID"].Value.ToString();
            nProduct.AmountCurrencyForeign = double.Parse(nRow.Cells["CurrencyForeign"].Value.ToString(), mFormatProviderCurrency);
            nProduct.AmountCurrencyMain = double.Parse(nRow.Cells["CurrencyMain"].Value.ToString(), mFormatProviderCurrency);

            if (nProduct.AmountCurrencyForeign > 0)
                nProduct.CurrencyRate = (float)(nProduct.AmountCurrencyMain / nProduct.AmountCurrencyForeign);

            return nProduct;
        }
        private void InsertInventoryProductIntoRow(InventoryProduct Product, int RowIndex)
        {
            DataGridViewRow nRow = base.Rows[RowIndex];
            nRow.Cells["No"].Value = RowIndex + 1;
            nRow.Cells["ProductID"].Tag = Product.Key;
            if (Product.Key > 0)
            {
                nRow.Cells["ProductID"].Value = Product.ID;
                nRow.Cells["ProductName"].Value = Product.Name;
                nRow.Cells["Unit"].Value = Product.Unit;
            }
            else
            {
                nRow.Cells["ProductName"].Value = Product.ItemName;
                nRow.Cells["Unit"].Value = Product.ItemUnit;
            }

            nRow.Cells["Quantity"].Value = Product.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal);
            nRow.Cells["Price"].Value = Product.UnitPrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
            nRow.Cells["CurrencyID"].Value = Product.CurrencyID;
            nRow.Cells["CurrencyRate"].Value = Product.CurrencyRate;
            nRow.Cells["CurrencyForeign"].Value = Product.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            nRow.Cells["CurrencyMain"].Value = Product.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        private void CaculateAmount(int RowIndex)
        {
            DataGridViewRow nRow = base.Rows[RowIndex];
            float nQuantity = 0;
            double nUnitPrice = 0;
            double nCurrencyRate = 0;
            double nAmountCurrencyMain = 0;
            double nAmountCurrencyForeign = 0;
            if (nRow.Cells["Quantity"].Value == null || nRow.Cells["Price"].Value == null || nRow.Cells["CurrencyID"].Value == null)
                return;

            float.TryParse(nRow.Cells["Quantity"].Value.ToString(), NumberStyles.Any, mFormatProviderDecimal, out nQuantity);
            double.TryParse(nRow.Cells["Price"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nUnitPrice);

            if (nRow.Cells["CurrencyID"].Value.ToString() == mCurrencyMain)
            {
                nAmountCurrencyMain = nQuantity * nUnitPrice;
                nRow.Cells["CurrencyRate"].Value = 1;
                nAmountCurrencyForeign = 0;
            }
            else
            {
                if (nRow.Cells["CurrencyRate"].Value != null)
                {
                    double.TryParse(nRow.Cells["CurrencyRate"].Value.ToString(), NumberStyles.Currency, mFormatProviderCurrency, out nCurrencyRate);

                    nAmountCurrencyForeign = nQuantity * nUnitPrice;
                    nAmountCurrencyMain = nAmountCurrencyForeign * nCurrencyRate;
                }
            }

            nRow.Cells["CurrencyForeign"].Value = nAmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            nRow.Cells["CurrencyMain"].Value = nAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        #endregion


    }
}
