﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.IVT
{
    public class Stock_Product_Output_Info
    {
        private int m_NoteKey = 0;
        private InventoryProduct m_Item = new InventoryProduct();
        private string m_Message = "";
        public Stock_Product_Output_Info()
        {

        }
        #region [ Properties ]
        public int NoteKey
        {
            set
            {
                m_NoteKey = value;
            }
            get
            {
                return m_NoteKey;
            }
        }
        public InventoryProduct Item
        {
            set
            {
                m_Item = value;
            }
            get
            {
                return m_Item;
            }
        }
        public string Message
        {
            get
            {
                return m_Message;
            }
        }
        #endregion 

        #region [ Constructor Update Information ]
        public string Save()
        {

            string nResult = "";

            string nSQL = "INSERT INTO IVT_Stock_Products_Output( "
                        + " NoteKey, ProductKey, CurrencyID,CurrencyRate,Quantity,UnitPrice, AmountCurrencyMain, AmountCurrencyForeign)"
                        + " VALUES(@NoteKey, @ProductKey, @CurrencyID,@CurrencyRate,@Quantity,@UnitPrice, @AmountCurrencyMain, @AmountCurrencyForeign)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@NoteKey", SqlDbType.Int).Value = m_NoteKey;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = Item.Key;

                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = Item.CurrencyID;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = Item.CurrencyRate;

                nCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Item.Quantity;
                nCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = Item.UnitPrice;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Item.AmountCurrencyMain;
                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Item.AmountCurrencyForeign;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
