﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.IVT
{
    public class Stock_Note_Input_Object : Stock_Note_Input_Info
    {
        private ArrayList m_ProductsInput = new ArrayList();

        #region [ Constructor Get Information ]
        public Stock_Note_Input_Object():base()
        {
           GetProductsInput();
        }
        public Stock_Note_Input_Object(int NoteKey)
            : base(NoteKey)
        {
            GetProductsInput();
        }

        public Stock_Note_Input_Object(string NoteID)
            : base(NoteID)
        {
            GetProductsInput();
        }

        #endregion 
        
        #region [Properties]

        public ArrayList ProductsInput
        {
            set
            {
                m_ProductsInput = value; 
            }
            get
            {
                return m_ProductsInput;
               
            }
        }
        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.NoteKey > 0)
            {
                DeleteProductsInput();
                SaveProductsInput();
            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DeleteProductsInput();
            return nResult;
        }

        #endregion

        #region [ Product Input ]
        private void GetProductsInput()
        {
            string strSQL = "SELECT A.*,B.ProductID,B.ProductName,B.Unit "
                         + " FROM IVT_Stock_Products_Input A "
                         + " LEFT JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                         + " WHERE NoteKey =@NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@NoteKey",SqlDbType.Int).Value = base.NoteKey;
                SqlDataReader nReader = nCommand.ExecuteReader();
                while (nReader.Read())
                {
                    InventoryProduct nInventoryProduct = new InventoryProduct();

                    nInventoryProduct.Key = int.Parse(nReader["ProductKey"].ToString());
                    nInventoryProduct.ID = nReader["ProductID"].ToString();
                    nInventoryProduct.Name = nReader["ProductName"].ToString();
                    nInventoryProduct.Unit = nReader["Unit"].ToString();

                    nInventoryProduct.Quantity = float.Parse(nReader["Quantity"].ToString());
                    nInventoryProduct.UnitPrice = double.Parse(nReader["UnitPrice"].ToString());
                    nInventoryProduct.AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    nInventoryProduct.AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());

                    m_ProductsInput.Add(nInventoryProduct);

                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }


        }
        private void SaveProductsInput()
        {
            int nLeng = m_ProductsInput.Count;
            for (int i = 0; i < nLeng; i++)
            {
                InventoryProduct nProduct = (InventoryProduct)m_ProductsInput[i];
                Stock_Product_Input_Info nItem = new Stock_Product_Input_Info();

                nItem.NoteKey = base.NoteKey;
                nItem.Item = nProduct;
                nItem.Save();
            }
        }
        public string DeleteProductsInput()
        {
            string nResult = "";
            string strSQL = " DELETE FROM IVT_Stock_Products_Input WHERE NoteKey = @NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@NoteKey",SqlDbType.Int).Value = base.NoteKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

    }
}
