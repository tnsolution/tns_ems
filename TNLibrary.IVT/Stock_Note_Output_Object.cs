﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.IVT
{
    public class Stock_Note_Output_Object : Stock_Note_Output_Info
    {
        private ArrayList m_ProductsOutput = new ArrayList();

        #region [ Constructor Get Information ]
        public Stock_Note_Output_Object():base()
        {
           GetProductsOutput();
        }
        public Stock_Note_Output_Object(int NoteKey)
            : base(NoteKey)
        {
            GetProductsOutput();
        }

        public Stock_Note_Output_Object(string NoteID)
            : base(NoteID)
        {
            GetProductsOutput();
        }

        #endregion 
        
        #region [Properties]

        public ArrayList ProductsOutput
        {
            set
            {
                m_ProductsOutput = value; 
            }
            get
            {
                return m_ProductsOutput;
               
            }
        }
        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.NoteKey > 0)
            {
                DeleteProductsOutput();
                SaveProductsOutput();
            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DeleteProductsOutput();
            return nResult;
        }

        #endregion

        #region [ Product Output ]
        private void GetProductsOutput()
        {
            string strSQL = "SELECT A.*,B.ProductID,B.ProductName,B.Unit "
                         + " FROM IVT_Stock_Products_Output A "
                         + " LEFT JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                         + " WHERE NoteKey =@NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@NoteKey",SqlDbType.Int).Value = base.NoteKey;
                SqlDataReader nReader = nCommand.ExecuteReader();
                while (nReader.Read())
                {
                    InventoryProduct nInventoryProduct = new InventoryProduct();

                    nInventoryProduct.Key = int.Parse(nReader["ProductKey"].ToString());
                    nInventoryProduct.ID = nReader["ProductID"].ToString();
                    nInventoryProduct.Name = nReader["ProductName"].ToString();
                    nInventoryProduct.Unit = nReader["Unit"].ToString();

                    nInventoryProduct.Quantity = float.Parse(nReader["Quantity"].ToString());
                    nInventoryProduct.UnitPrice = double.Parse(nReader["UnitPrice"].ToString());
                    nInventoryProduct.AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    nInventoryProduct.AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());

                    m_ProductsOutput.Add(nInventoryProduct);

                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }


        }
        private void SaveProductsOutput()
        {
            int nLeng = m_ProductsOutput.Count;
            for (int i = 0; i < nLeng; i++)
            {
                InventoryProduct nProduct = (InventoryProduct)m_ProductsOutput[i];
                Stock_Product_Output_Info nItem = new Stock_Product_Output_Info();

                nItem.NoteKey = base.NoteKey;
                nItem.Item = nProduct;
                nItem.Save();
            }
        }
        public string DeleteProductsOutput()
        {
            string nResult = "";
            string strSQL = " DELETE FROM IVT_Stock_Products_Output WHERE NoteKey = @NoteKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@NoteKey",SqlDbType.Int).Value = base.NoteKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

    }
}
