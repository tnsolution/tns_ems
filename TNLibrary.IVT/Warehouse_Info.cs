﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.IVT
{
    public class Warehouse_Info
    {
        private int m_WarehouseKey = 0;
        private string m_WarehouseName = "";
        private string m_Address = "";
        private string m_Description = "";
        private int m_BranchKey = 0;

        private string m_Message = "";
        public Warehouse_Info()
        {
        }

        #region [ Constructor Get Information ]

        public Warehouse_Info(int WarehouseKey)
        {
            string nSQL = "SELECT * FROM IVT_Warehouse WHERE WarehouseKey = @WarehouseKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_WarehouseKey = (int)nReader["WarehouseKey"];
                    m_WarehouseName = nReader["WarehouseName"].ToString();
                    m_Address = nReader["Address"].ToString();
                    m_Description = nReader["Description"].ToString();
                    m_BranchKey = (int)nReader["BranchKey"];

                }
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public void Get_Info(int BranchKey)
        {
            string nSQL = "SELECT * FROM IVT_Warehouse WHERE BranchKey = @BranchKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_WarehouseKey = (int)nReader["WarehouseKey"];
                    m_WarehouseName = nReader["WarehouseName"].ToString();
                    m_Address = nReader["Address"].ToString();
                    m_Description = nReader["Description"].ToString();
                    m_BranchKey = (int)nReader["BranchKey"];

                }
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [Properties ]

        public int Key
        {
            set { m_WarehouseKey = value; }
            get { return m_WarehouseKey; }
        }
        public string Name
        {
            set { m_WarehouseName = value; }
            get { return m_WarehouseName; }
        }
        public string Address
        {
            set
            {
                m_Address = value;
            }
            get { return m_Address; }
        }
        public string Description
        {
            set { m_Description = value; }
            get { return m_Description; }
        }
        public int BranchKey
        {
            set { m_BranchKey = value; }
            get { return m_BranchKey; }
        }
        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Save()
        {
            if (m_WarehouseKey > 0)
                return Update();
            else
                return Create();
        }
        public string Update()
        {
            string nResult = ""; ;

            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE IVT_Warehouse SET "
                        + " WarehouseName = @WarehouseName,"
                        + " Address=@Address, "
                        + " Description = @Description,"
                        + " BranchKey = @BranchKey "
                        + " WHERE WarehouseKey = @WarehouseKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;
                nCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = m_WarehouseName;
                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@Description", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO IVT_Warehouse( "
                        + " WarehouseName, Address, Description,BranchKey)"
                        + " VALUES(@WarehouseName, @Address, @Description,@BranchKey)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;
                nCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = m_WarehouseName;
                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@Description", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM IVT_Warehouse WHERE WarehouseKey = @WarehouseKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.NVarChar).Value = m_WarehouseKey;
                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        #endregion
    }

}
