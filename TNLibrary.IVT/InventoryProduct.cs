﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TNLibrary.PUL;

namespace TNLibrary.IVT
{
    public class InventoryProduct : Product_Info
    {
        private string m_ItemName = "";
        private string m_ItemUnit = "";

        private string m_CurrencyID = "VND";
        private float m_CurrencyRate = 0;

        private float m_Quantity = 0;
        private int m_WarehouseKey = 0;
        private double m_AmountCurrencyMain = 0;
        private double m_AmountCurrencyForeign = 0;

        public InventoryProduct(int ProductKey)
            : base(ProductKey)
        {

        }
        public InventoryProduct(string ProductID)
            : base(ProductID)
        {

        }
        public InventoryProduct()
        {

        }

        public string ItemName
        {
            get
            {
                return m_ItemName;
            }
            set
            {
                m_ItemName = value;
            }
        }
        public string ItemUnit
        {
            get
            {
                return m_ItemUnit;
            }
            set
            {
                m_ItemUnit = value;
            }
        }

        public string CurrencyID
        {
            get
            {
                return m_CurrencyID;
            }
            set
            {
                m_CurrencyID = value;
            }
        }
        public float CurrencyRate
        {
            get
            {
                return m_CurrencyRate;
            }
            set
            {
                m_CurrencyRate = value;
            }
        }

        public float Quantity
        {
            get
            {
                return m_Quantity;
            }
            set
            {
                m_Quantity = value;
            }
        }
        public int WarehouseKey
        {
            get
            {
                return m_WarehouseKey;
            }
            set
            {
                m_WarehouseKey = value;
            }
        }
        public double AmountCurrencyMain
        {
            get
            {
                return m_AmountCurrencyMain;
            }
            set
            {
                m_AmountCurrencyMain = value;
            }
        }
        public double AmountCurrencyForeign
        {
            get
            {
                return m_AmountCurrencyForeign;
            }
            set
            {
                m_AmountCurrencyForeign = value;
            }
        }

       

    }
}
