﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace TNConfig
{
    public class ConnectDataBaseInfo
    {
        private string mDataSource = @".\SQLEXPRESS";
        private string mDataBase = "TN_HOTEL";
        private string mUser = "sa";
        private string mPassword = "triviet";

        private string mAttachDbFilename = Path.GetFullPath(@"DataBase\TN_HOTEL.MDF");
        private bool mIsConnectLocal = true;
        private string mConnectionString = "";

        public ConnectDataBaseInfo()
        {
        }
        public string DataSource
        {
            get
            {
                return mDataSource;
            }
            set
            {
                mDataSource = value;
            }
        }
        public string DataBase
        {
            get
            {
                return mDataBase;
            }
            set
            {
                mDataBase = value;
            }
        }
        public string UserName
        {
            get
            {
                return mUser;
            }
            set
            {
                mUser = value;
            }
        }
        public string Password
        {
            get
            {
                return mPassword;
            }
            set
            {
                mPassword = value;
            }
        }
        public string AttachDbFilename
        {
            get
            {
                return mAttachDbFilename;
            }
            set
            {
                mAttachDbFilename = value;
            }
        }
        public bool IsConnectLocal
        {
            get
            {
                return mIsConnectLocal;
            }
            set
            {
                mIsConnectLocal = value;
            }
        }
        public string ConnectionString
        {
            get
            {
                if (!mIsConnectLocal)
                {
                    mConnectionString = "Data Source = " + mDataSource + ";DataBase= " + mDataBase + ";User=" + mUser + ";Password= " + mPassword;
                }
                else
                {
                    mConnectionString = "Data Source = " + mDataSource + ";AttachDbFilename= " + mAttachDbFilename + ";User Instance =true;Integrated Security= SSPI";
                }
                return mConnectionString;
            }
            set
            {
                mConnectionString = value;
            }
        }
    }
}
