﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using TNConfig;
/// <summary>
/// Summary description for HRM_EMPLOYEE_Data
/// </summary>
/// 
namespace TNLibrary.HRM
{
    public class Employees_Data
    {

        #region [ Employee ]
        public static DataTable EmployeeList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT  A.* , B.DepartmentName FROM HRM_Employees A "
                        + " LEFT JOIN SYS_Departments B ON A.DepartmentKey = B.DepartmentKey"
                        + " ORDER BY FirstName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nEmployees = new SqlDataAdapter(nCommand);

                nEmployees.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable EmployeeList(string EmployeeID, string EmployeeName, string PhoneNumber)
        {
            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT * FROM HRM_Employees WHERE   EmployeeID   LIKE @EmployeeID  AND (FirstName LIKE @EmployeeName  OR LastName   LIKE @EmployeeName)  AND HomePhone  LIKE @Phone ";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = "%" + EmployeeID + "%";
                nCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = "%" + EmployeeName + "%";
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + PhoneNumber + "%";
                new SqlDataAdapter(nCommand).Fill(nTable);
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }
        #endregion

        #region [Payroll]
        public static DataTable Salarys(int Month, int Year)
        {

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT A.*,B.EmployeeID,(B.LastName + ' ' + B.FirstName) AS EmployeeName,C.DepartmentName   "
                        + " FROM HRM_Salarys A "
                        + " LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey "
                        + " LEFT JOIN SYS_Departments C ON B.DepartmentKey = C.DepartmentKey  "
                        + " WHERE (A.MonthSalary=@MonthSalary AND A.MonthSalary=@MonthSalary)";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@MonthSalary", SqlDbType.Int).Value = Month;
                nCommand.Parameters.Add("@YearSalary", SqlDbType.Int).Value = Year;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }
        #endregion

        public static int CheckExistEmployee(int EmployeeKey)
        {
            int nResult = 0;
            string nSQL = "SELECT COUNT(*) FROM SYS_Users WHERE EmployeeKey = @EmployeeKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.CommandType = CommandType.Text;
            nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;

            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();

            return nResult;
        }


    }
}