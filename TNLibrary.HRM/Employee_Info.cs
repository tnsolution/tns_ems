﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;

using TNConfig;
namespace TNLibrary.HRM
{
    public class Employee_Info
    {
        private int m_EmployeeKey = 0;
        private string m_EmployeeID = "";
        private string m_LastName = "";
        private string m_FirstName = "";
        private bool m_Gender;
        private bool m_MaritalStatus;
        private DateTime m_BirthDate = DateTime.Now.AddYears(-16);
        private DateTime m_HireDate = DateTime.Now;
        private DateTime m_QuitDate = new DateTime();

        private string m_PassportNumber = "";
        private DateTime m_IssueDate = new DateTime();
        private DateTime m_ExpireDate = new DateTime();
        private string m_IssuePlace = "";
        private string m_CurrentNationality = "";
        private string m_CurrentOccupation = "";

        private string m_Address = "";
        private string m_Region = "";
        private string m_PostalCode = "";
        private string m_City = "";
        private string m_Country = "";

        private string m_HomePhone = "";
        private string m_MobiPhone = "";
        private string m_Email = "";
        private string m_PhotoPath = "";
        private byte[] m_Photo;
        private string m_Notes = "";
        private bool m_IsWorking;
        private int m_StyleContractKey = 0;
        private double m_BasicSalary = 0;
        //Relation
        private int m_DepartmentKey = 0;
        private string m_DepartmentName ="";
        private int m_PositionKey = 0;
        private string m_PositionName = "";
        private int m_ReportTo = 0;
        private int m_BranchKey = 0;

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";
        public Employee_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region [ Constructor Get Information ]
        public Employee_Info(int EmployeeKey)
        {
            string nSQL = "SELECT * FROM HRM_Employees "
                    + " WHERE EmployeeKey =@EmployeeKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());
                    m_EmployeeID = nReader["EmployeeID"].ToString();
                    m_LastName = nReader["LastName"].ToString();
                    m_FirstName = nReader["FirstName"].ToString();
                    m_Gender = (bool)nReader["Gender"];
                    m_MaritalStatus = (bool)nReader["MaritalStatus"];

                    m_PassportNumber = nReader["PassportNumber"].ToString();
                    if (nReader["IssueDate"] != DBNull.Value)
                        m_IssueDate = (DateTime)nReader["IssueDate"];
                    if (nReader["ExpireDate"] != DBNull.Value)
                        m_ExpireDate = (DateTime)nReader["ExpireDate"];
                    m_IssuePlace = nReader["IssuePlace"].ToString();

                    m_CurrentNationality = nReader["CurrentNationality"].ToString();
                    m_CurrentOccupation = nReader["CurrentOccupation"].ToString();
                    m_BirthDate = DateTime.Parse(nReader["BirthDate"].ToString());

                    m_PhotoPath = nReader["PhotoPath"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_Region = nReader["Region"].ToString();
                    m_PostalCode = nReader["PostalCode"].ToString();
                    m_City = nReader["City"].ToString();
                    m_Country = nReader["Country"].ToString();

                    m_HomePhone = nReader["HomePhone"].ToString();
                    m_MobiPhone = nReader["MobiPhone"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_Notes = nReader["Notes"].ToString();

                    m_HireDate = DateTime.Parse(nReader["HireDate"].ToString());
                    if (nReader["QuitDate"] != DBNull.Value)
                        m_QuitDate = (DateTime)nReader["QuitDate"];
                    m_IsWorking = (bool)nReader["IsWorking"];

                    if (nReader["Photo"] != DBNull.Value)
                        m_Photo = (byte[])(nReader["Photo"]);

                    m_BasicSalary = double.Parse(nReader["BasicSalary"].ToString());
                    //Relation
                    m_DepartmentKey = int.Parse(nReader["DepartmentKey"].ToString());
                    m_ReportTo = int.Parse(nReader["ReportTo"].ToString());
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_StyleContractKey = (int)nReader["StyleContractKey"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Employee_Info(string EmployeeID)
        {
            string nSQL = "SELECT * FROM HRM_Employees "
                    + " WHERE EmployeeID =@EmployeeID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());

                    m_EmployeeID = nReader["EmployeeID"].ToString();
                    m_LastName = nReader["LastName"].ToString();
                    m_FirstName = nReader["FirstName"].ToString();
                    m_Gender = (bool)nReader["Gender"];
                    m_MaritalStatus = (bool)nReader["MaritalStatus"];

                    m_PassportNumber = nReader["PassportNumber"].ToString();
                    if (nReader["IssueDate"] != DBNull.Value)
                        m_IssueDate = (DateTime)nReader["IssueDate"];
                    if (nReader["ExpireDate"] != DBNull.Value)
                        m_ExpireDate = (DateTime)nReader["ExpireDate"];
                    m_IssuePlace = nReader["IssuePlace"].ToString();

                    m_CurrentNationality = nReader["CurrentNationality"].ToString();
                    m_CurrentOccupation = nReader["CurrentOccupation"].ToString();
                    m_BirthDate = DateTime.Parse(nReader["BirthDate"].ToString());

                    m_PhotoPath = nReader["PhotoPath"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_Region = nReader["Region"].ToString();
                    m_PostalCode = nReader["PostalCode"].ToString();
                    m_City = nReader["City"].ToString();
                    m_Country = nReader["Country"].ToString();

                    m_HomePhone = nReader["HomePhone"].ToString();
                    m_MobiPhone = nReader["MobiPhone"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_Notes = nReader["Notes"].ToString();

                    m_HireDate = DateTime.Parse(nReader["HireDate"].ToString());
                    if (nReader["QuitDate"] != DBNull.Value)
                        m_QuitDate = (DateTime)nReader["QuitDate"];
                    m_IsWorking = (bool)nReader["IsWorking"];

                    if (nReader["Photo"] != DBNull.Value)
                        m_Photo = (byte[])(nReader["Photo"]);

                    m_BasicSalary = double.Parse(nReader["BasicSalary"].ToString());

                    //Relation
                    m_DepartmentKey = int.Parse(nReader["DepartmentKey"].ToString());
                    m_ReportTo = int.Parse(nReader["ReportTo"].ToString());
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_StyleContractKey = (int)nReader["StyleContractKey"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public void GetEmployee_Info(int EmployeeKey)
        {
            string nSQL = "SELECT * FROM HRM_Employees WHERE EmployeeKey =@EmployeeKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());
                    m_EmployeeID = nReader["EmployeeID"].ToString();
                    m_LastName = nReader["LastName"].ToString();
                    m_FirstName = nReader["FirstName"].ToString();
                    m_Gender = (bool)nReader["Gender"];
                    m_MaritalStatus = (bool)nReader["MaritalStatus"];

                    m_PassportNumber = nReader["PassportNumber"].ToString();
                    if (nReader["IssueDate"] != DBNull.Value)
                        m_IssueDate = (DateTime)nReader["IssueDate"];
                    if (nReader["ExpireDate"] != DBNull.Value)
                        m_ExpireDate = (DateTime)nReader["ExpireDate"];
                    m_IssuePlace = nReader["IssuePlace"].ToString();

                    m_CurrentNationality = nReader["CurrentNationality"].ToString();
                    m_CurrentOccupation = nReader["CurrentOccupation"].ToString();
                    m_BirthDate = DateTime.Parse(nReader["BirthDate"].ToString());

                    m_PhotoPath = nReader["PhotoPath"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_Region = nReader["Region"].ToString();
                    m_PostalCode = nReader["PostalCode"].ToString();
                    m_City = nReader["City"].ToString();
                    m_Country = nReader["Country"].ToString();

                    m_HomePhone = nReader["HomePhone"].ToString();
                    m_MobiPhone = nReader["MobiPhone"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_Notes = nReader["Notes"].ToString();

                    m_HireDate = DateTime.Parse(nReader["HireDate"].ToString());
                    if (nReader["QuitDate"] != DBNull.Value)
                        m_QuitDate = (DateTime)nReader["QuitDate"];
                    m_IsWorking = (bool)nReader["IsWorking"];

                    if (nReader["Photo"] != DBNull.Value)
                        m_Photo = (byte[])(nReader["Photo"]);

                    m_BasicSalary = double.Parse(nReader["BasicSalary"].ToString());

                    //Relation
                    m_DepartmentKey = int.Parse(nReader["DepartmentKey"].ToString());
                    m_ReportTo = int.Parse(nReader["ReportTo"].ToString());
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_StyleContractKey = (int)nReader["StyleContractKey"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public void GetEmployee_Info(string EmployeeID)
        {
            string nSQL = "SELECT * FROM HRM_Employees WHERE EmployeeID =@EmployeeID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@EmployeeID", SqlDbType.NChar).Value = EmployeeID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_EmployeeKey = int.Parse(nReader["EmployeeKey"].ToString());

                    m_EmployeeID = nReader["EmployeeID"].ToString();
                    m_LastName = nReader["LastName"].ToString();
                    m_FirstName = nReader["FirstName"].ToString();
                    m_Gender = (bool)nReader["Gender"];
                    m_MaritalStatus = (bool)nReader["MaritalStatus"];

                    m_PassportNumber = nReader["PassportNumber"].ToString();
                    if (nReader["IssueDate"] != DBNull.Value)
                        m_IssueDate = (DateTime)nReader["IssueDate"];
                    if (nReader["ExpireDate"] != DBNull.Value)
                        m_ExpireDate = (DateTime)nReader["ExpireDate"];
                    m_IssuePlace = nReader["IssuePlace"].ToString();

                    m_CurrentNationality = nReader["CurrentNationality"].ToString();
                    m_CurrentOccupation = nReader["CurrentOccupation"].ToString();
                    m_BirthDate = DateTime.Parse(nReader["BirthDate"].ToString());

                    m_PhotoPath = nReader["PhotoPath"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_Region = nReader["Region"].ToString();
                    m_PostalCode = nReader["PostalCode"].ToString();
                    m_City = nReader["City"].ToString();
                    m_Country = nReader["Country"].ToString();

                    m_HomePhone = nReader["HomePhone"].ToString();
                    m_MobiPhone = nReader["MobiPhone"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_Notes = nReader["Notes"].ToString();

                    m_HireDate = DateTime.Parse(nReader["HireDate"].ToString());
                    if (nReader["QuitDate"] != DBNull.Value)
                        m_QuitDate = (DateTime)nReader["QuitDate"];
                    m_IsWorking = (bool)nReader["IsWorking"];

                    if (nReader["Photo"] != DBNull.Value)
                        m_Photo = (byte[])(nReader["Photo"]);

                    m_BasicSalary = double.Parse(nReader["BasicSalary"].ToString());

                    //Relation
                    m_DepartmentKey = int.Parse(nReader["DepartmentKey"].ToString());
                    m_ReportTo = int.Parse(nReader["ReportTo"].ToString());
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_StyleContractKey = (int)nReader["StyleContractKey"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

        #region [ Properties ]
        public int EmployeeKey
        {
            get { return m_EmployeeKey; }
            set { m_EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return m_EmployeeID; }
            set { m_EmployeeID = value; }
        }
        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        public bool Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        public bool MaritalStatus
        {
            get { return m_MaritalStatus; }
            set { m_MaritalStatus = value; }
        }
        public DateTime BirthDate
        {
            get { return m_BirthDate; }
            set { m_BirthDate = value; }
        }

        public string PassportNumber
        {
            get { return m_PassportNumber; }
            set { m_PassportNumber = value; }
        }
        public DateTime IssueDate
        {
            get { return m_IssueDate; }
            set { m_IssueDate = value; }
        }
        public DateTime ExpireDate
        {
            get { return m_ExpireDate; }
            set { m_ExpireDate = value; }
        }
        public string IssuePlace
        {
            get { return m_IssuePlace; }
            set { m_IssuePlace = value; }
        }
        public string CurrentNationality
        {
            get { return m_CurrentNationality; }
            set { m_CurrentNationality = value; }
        }
        public string CurrentOccupation
        {
            get { return m_CurrentOccupation; }
            set { m_CurrentOccupation = value; }
        }

        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }
        public string Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }
        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string HomePhone
        {
            get { return m_HomePhone; }
            set { m_HomePhone = value; }
        }
        public string MobiPhone
        {
            get { return m_MobiPhone; }
            set { m_MobiPhone = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string PhotoPath
        {
            get { return m_PhotoPath; }
            set { m_PhotoPath = value; }
        }
        public byte[] Photo
        {
            set
            {
                m_Photo = value;
            }
            get
            {
                return m_Photo;
            }
        }
        public Image ImagePhoto
        {

            get
            {
                try
                {
                    if (m_Photo.Length == 0)
                        return null;
                    else
                    {
                        MemoryStream ms;
                        ms = new MemoryStream(m_Photo);
                        Image nImg = Image.FromStream(ms);
                        ms.Close();
                        return nImg;
                    }
                }
                catch
                {
                    return null;
                }

            }
        }
        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }
        public DateTime HireDate
        {
            get { return m_HireDate; }
            set { m_HireDate = value; }
        }
        public DateTime QuitDate
        {
            get { return m_QuitDate; }
            set { m_QuitDate = value; }
        }
        public bool IsWorking
        {
            get { return m_IsWorking; }
            set { m_IsWorking = value; }
        }
        public int StyleContractKey
        {
            get { return m_StyleContractKey; }
            set { m_StyleContractKey = value; }
        }
        public double BasicSalary
        {
            get { return m_BasicSalary; }
            set { m_BasicSalary = value; }
        }

        //Relation
        public int DepartmentKey
        {
            get { return m_DepartmentKey; }
            set { m_DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return m_DepartmentName; }
        }
        public int PositionKey
        {
            get { return m_PositionKey; }
            set { m_PositionKey = value; }
        }
        public string PositionName
        {
            get
            {
                return m_PositionName;
            }
        }
        public int ReportTo
        {
            get { return m_ReportTo; }
            set { m_ReportTo = value; }
        }

        public int BranchKey
        {
            get { return m_BranchKey; }
            set { m_BranchKey = value; }
        }

        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            string nSQL = "UPDATE HRM_Employees SET "
                        + " EmployeeID = @EmployeeID,"
                        + " LastName = @LastName ,"
                        + " FirstName = @FirstName, "
                        + " Gender= @Gender ,"
                        + " BirthDate = @BirthDate ,"
                        + " MaritalStatus = @MaritalStatus, "
                        + " PassportNumber = @PassportNumber, "
                        + " IssueDate = @IssueDate, "
                        + " ExpireDate = @ExpireDate, "
                        + " IssuePlace = @IssuePlace, "
                        + " CurrentNationality = @CurrentNationality, "

                        + " CurrentOccupation = @CurrentOccupation, "
                        + " Address = @Address ,"
                        + " Region = @Region ,"
                        + " PostalCode = @PostalCode ,"
                        + " City = @City ,"
                        + " Country = @Country ,"
                        + " PhotoPath = @PhotoPath ,"
                        + " Photo = @Photo,"
                        + " HomePhone = @HomePhone ,"
                        + " MobiPhone = @MobiPhone ,"
                        + " Email = @Email  ,"
                        + " HireDate = @HireDate  ,"
                        + " QuitDate = @QuitDate  ,"
                        + " IsWorking = @IsWorking  ,"
                        + " Notes = @Notes, "
                        + " BasicSalary = @BasicSalary, "
                        + " StyleContractKey = @StyleContractKey, "

                        + " DepartmentKey= @DepartmentKey,"
                        + " ReportTo= @ReportTo,"
                        + " BranchKey = @BranchKey,"
                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE EmployeeKey = @EmployeeKey";


            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = m_EmployeeKey;
                nCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = m_EmployeeID;
                nCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = m_LastName;
                nCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = m_FirstName;
                nCommand.Parameters.Add("@Gender", SqlDbType.Bit).Value = m_Gender;
                nCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = m_BirthDate;
                nCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = m_MaritalStatus;

                nCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = m_PassportNumber;

                nCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = m_IssueDate;
                if (m_ExpireDate.Year == 0001)
                    nCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = m_ExpireDate;
                nCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = m_IssuePlace;
                nCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = m_CurrentNationality;

                nCommand.Parameters.Add("@CurrentOccupation", SqlDbType.NVarChar).Value = m_CurrentOccupation;

                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@Region", SqlDbType.NVarChar).Value = m_Region;
                nCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = m_PostalCode;
                nCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = m_City;
                nCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = m_Country;

                nCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = m_HomePhone;
                nCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = m_MobiPhone;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;

                nCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = m_PhotoPath;

                nCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = m_HireDate;
                if (m_QuitDate.Year == 0001)
                    nCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = m_QuitDate;

                nCommand.Parameters.Add("@IsWorking", SqlDbType.Bit).Value = m_IsWorking;
                nCommand.Parameters.Add("@StyleContractKey", SqlDbType.Int).Value = m_StyleContractKey;

                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = m_Photo;

                nCommand.Parameters.Add("@BasicSalary", SqlDbType.Int).Value = m_BasicSalary;

                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = m_DepartmentKey;
                nCommand.Parameters.Add("@ReportTo", SqlDbType.Int).Value = m_ReportTo;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO HRM_Employees( "
                        + " EmployeeID, LastName, FirstName, Gender, BirthDate,MaritalStatus, PassportNumber,IssueDate,ExpireDate,IssuePlace,CurrentNationality,  CurrentOccupation, "
                        + " Address, City, Region, PostalCode, Country, HomePhone, PhotoPath,Photo, HireDate, QuitDate,IsWorking,StyleContractKey, Notes, BasicSalary, DepartmentKey, ReportTo, BranchKey ,CreatedBy,CreatedDatetime,ModifiedBy,ModifiedDatetime )"
                        + " VALUES(@EmployeeID, @LastName, @FirstName, @Gender, @BirthDate,@MaritalStatus, @PassportNumber,@IssueDate,@ExpireDate,@IssuePlace,@CurrentNationality,@CurrentOccupation, "
                        + " @Address, @City, @Region, @PostalCode, @Country, @HomePhone, @PhotoPath,@Photo,@HireDate, @QuitDate,@IsWorking,@StyleContractKey, @Notes, @BasicSalary, @DepartmentKey, @ReportTo,@BranchKey,@CreatedBy,getdate(),@ModifiedBy,getdate())"
                        + " SELECT EmployeeKey FROM HRM_Employees WHERE EmployeeKey = SCOPE_IDENTITY()";

            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = m_EmployeeID;
                nCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = m_LastName;
                nCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = m_FirstName;
                nCommand.Parameters.Add("@Gender", SqlDbType.Bit).Value = m_Gender;
                nCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = m_BirthDate;
                nCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = m_MaritalStatus;

                nCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = m_PassportNumber;

                nCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = m_IssueDate;
                if (m_ExpireDate.Year == 0001)
                    nCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = m_ExpireDate;
                nCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = m_IssuePlace;
                nCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = m_CurrentNationality;

                nCommand.Parameters.Add("@CurrentOccupation", SqlDbType.NVarChar).Value = m_CurrentOccupation;

                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@Region", SqlDbType.NVarChar).Value = m_Region;
                nCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = m_PostalCode;
                nCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = m_City;
                nCommand.Parameters.Add("@Country", SqlDbType.NVarChar).Value = m_Country;

                nCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = m_HomePhone;
                nCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = m_MobiPhone;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;

                nCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = m_PhotoPath;

                nCommand.Parameters.Add("@HireDate", SqlDbType.DateTime).Value = m_HireDate;
                if (m_QuitDate.Year == 0001)
                    nCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@QuitDate", SqlDbType.DateTime).Value = m_QuitDate;

                nCommand.Parameters.Add("@IsWorking", SqlDbType.Bit).Value = m_IsWorking;
                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@Photo", SqlDbType.Image).Value = m_Photo;
                nCommand.Parameters.Add("@BasicSalary", SqlDbType.Int).Value = m_BasicSalary;
                nCommand.Parameters.Add("@StyleContractKey", SqlDbType.Int).Value = m_StyleContractKey;

                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = m_DepartmentKey;
                nCommand.Parameters.Add("@ReportTo", SqlDbType.Int).Value = m_ReportTo;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                m_EmployeeKey = int.Parse(nResult);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (m_EmployeeKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult; 
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM HRM_Employees WHERE EmployeeKey = @EmployeeKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = m_EmployeeKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }

}
