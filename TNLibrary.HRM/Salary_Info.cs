﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

using TNConfig;

namespace TNLibrary.HRM
{
    public class Salary_Info
    {
        private int mSalaryKey = 0;
        private int mEmployeeKey = 0;
        private string mEmployeeID = "";
        private string mEmployeeName = "";
        private string mDepartmentName = "";
        private int mMonth = 0;
        private int mYear = 0;

        private double mBasicSalary = 0;
        private double mNetSalary = 0;
        private float mWorkDays = 0;
        private double mBonus = 0;
        private double mTotalSalary = 0;

        private string m_Message = "";
        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        public Salary_Info()
        {

        }

        public Salary_Info(int EmployeeKey, int Month, int Year)
        {
            mEmployeeKey = EmployeeKey;
            mMonth = Month;
            mYear = Year;

            string nSQL = " SELECT A.*,B.EmployeeID,B.LastName,B.FirstName,C.DepartmentName   "
                        + " FROM HRM_Salarys A "
                        + " LEFT JOIN HRM_Employees B ON A.EmployeeKey = B.EmployeeKey "
                        + " LEFT JOIN SYS_Departments C ON B.DepartmentKey = C.DepartmentKey  "
                        + " WHERE (A.EmployeeKey =@EmployeeKey AND A.MonthSalary=@MonthSalary AND A.MonthSalary=@MonthSalary)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = EmployeeKey;
                nCommand.Parameters.Add("@MonthSalary", SqlDbType.Int).Value = Month;
                nCommand.Parameters.Add("@YearSalary", SqlDbType.Int).Value = Year;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    mSalaryKey = int.Parse(nReader["SalaryKey"].ToString());
                    mEmployeeID = nReader["EmployeeID"].ToString();
                    mEmployeeName = nReader["LastName"].ToString() + " " + nReader["FirstName"].ToString();
                    mDepartmentName = nReader["DepartmentName"].ToString();

                    mBasicSalary = double.Parse(nReader["BasicSalary"].ToString());
                    mNetSalary = double.Parse(nReader["NetSalary"].ToString());
                    mWorkDays = float.Parse(nReader["WorkDays"].ToString());
                    mBonus = double.Parse(nReader["Bonus"].ToString());

                    mTotalSalary = double.Parse(nReader["TotalSalary"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        #region [ Properties ]
        public int SalaryKey
        {
            get
            {
                return mSalaryKey;
            }
        }
        public int EmployeeKey
        {
            get
            {
                return mEmployeeKey;
            }
            set
            {
                mEmployeeKey = value;
            }
        }
        private string EmployeeID
        {
            get
            {
                return mEmployeeID;
            }
        }
        private string EmployeeName
        {
            get
            {
                return mEmployeeName;
            }
        }
        private string DepartmentName
        {
            get
            {
                return mDepartmentName;
            }
        }
        public int Month
        {
            get
            {
                return mMonth;
            }
            set
            {
                mMonth = value;
            }
        }
        public int Year
        {
            get
            {
                return mYear;
            }
            set
            {
                mYear = value;
            }
        }

        public double NetSalary
        {
            get
            {
                return mNetSalary;
            }
            set
            {
                mNetSalary = value;
            }
        }
        public double BasicSalary
        {
            get
            {
                return mBasicSalary;
            }
            set
            {
                mBasicSalary = value;
            }
        }
        public float WorkDays
        {
            get
            {
                return mWorkDays;
            }
            set
            {
                mWorkDays = value;
            }
        }
        public double Bonus
        {
            get
            {
                return mBonus;
            }
            set
            {
                mBonus = value;
            }
        }
        public double TotalSalary
        {
            get
            {
                return mTotalSalary;
            }
            set
            {
                mTotalSalary = value;
            }
        }
        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            string nSQL = "UPDATE HRM_Salarys SET  "
                        + " NetSalary =@NetSalary,"
                        + " BasicSalary =@BasicSalary,"
                        + " TotalSalary =@TotalSalary,"
                        + " WorkDays = @WorkDays,"
                        + " Bonus = @Bonus,"
                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "
                        + " WHERE (EmployeeKey = @EmployeeKey AND MonthSalary=@MonthSalary AND YearSalary=@YearSalary)";


            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = mEmployeeKey;
                nCommand.Parameters.Add("@MonthSalary", SqlDbType.Int).Value = mMonth;
                nCommand.Parameters.Add("@YearSalary", SqlDbType.Int).Value = mYear;

                nCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = mBasicSalary;
                nCommand.Parameters.Add("@NetSalary", SqlDbType.Money).Value = mNetSalary;
                nCommand.Parameters.Add("@WorkDays", SqlDbType.Float).Value = mWorkDays;
                nCommand.Parameters.Add("@Bonus", SqlDbType.Money).Value = mBonus;
                nCommand.Parameters.Add("@TotalSalary", SqlDbType.Money).Value = mTotalSalary;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO HRM_Salarys( "
                    + " EmployeeKey,MonthSalary,YearSalary,BasicSalary,NetSalary,WorkDays,Bonus,TotalSalary,CreatedBy,CreatedDatetime,ModifiedBy,ModifiedDatetime)"
                    + " VALUES(@EmployeeKey,@MonthSalary,@YearSalary,@BasicSalary,@NetSalary,@WorkDays,@Bonus,@TotalSalary,@CreatedBy,getdate(),@ModifiedBy,getdate())";

            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = mEmployeeKey;
                nCommand.Parameters.Add("@MonthSalary", SqlDbType.Int).Value = mMonth;
                nCommand.Parameters.Add("@YearSalary", SqlDbType.Int).Value = mYear;

                nCommand.Parameters.Add("@BasicSalary", SqlDbType.Money).Value = mBasicSalary;
                nCommand.Parameters.Add("@NetSalary", SqlDbType.Money).Value = mNetSalary;
                nCommand.Parameters.Add("@WorkDays", SqlDbType.Float).Value = mWorkDays;
                nCommand.Parameters.Add("@Bonus", SqlDbType.Money).Value = mBonus;
                nCommand.Parameters.Add("@TotalSalary", SqlDbType.Money).Value = mTotalSalary;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (mSalaryKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM HRM_Salarys WHERE (EmployeeKey = @EmployeeKey AND MonthSalary=@MonthSalary AND YearSalary=@YearSalary)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.Int).Value = mEmployeeKey;
                nCommand.Parameters.Add("@MonthSalary", SqlDbType.Int).Value = mMonth;
                nCommand.Parameters.Add("@YearSalary", SqlDbType.Int).Value = mYear;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

    }
}
