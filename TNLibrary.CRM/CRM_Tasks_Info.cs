﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;

public class CRM_Tasks_Info
{
   
    #region [Bien cuc bo]
    

    private int m_TaskKey = 0;  
    private string m_TaskName = "";
    private string m_TaskContent = "";

    private DateTime m_StartDate;
    private DateTime m_DueDate;

    private int m_StatusKey = 0;
    private int m_Priority = 0;
    private int m_Complete = 0;
    private DateTime m_ReminderDate;
    private int m_CategoryKey = 0;
    private int m_CustomerKey = 0;

    private string m_CREATE_BY = "";
    private DateTime m_CREATE_ON;

    private string m_MODIFY_BY = "";
    private DateTime m_MODIFY_ON;
    
    #endregion

    #region [ Properties ]

    public int TaskKey
    {
        get { return m_TaskKey; }
        set { m_TaskKey = value; }
    }
    public string TaskName
    {
        get { return m_TaskName; }
        set { m_TaskName = value; }
    }
    public string TaskContent
    {
        get { return m_TaskContent; }
        set { m_TaskContent = value; }
    }
    public DateTime StartDate
    {
        get { return m_StartDate; }
        set { m_StartDate = value; }
    }
   public DateTime DueDate
   {
        get {return m_DueDate;}
        set {m_DueDate=value;}
   }
  
   public int StatusKey
    {
        get { return m_StatusKey; }
        set { m_StatusKey = value; }
    }

   public int Priority
    {
        get { return m_Priority; }
        set { m_Priority = value; }
    }
    public int Complete
    {
        get {return m_Complete;}
        set {m_Complete=value ;}
    }
    public DateTime ReminderDate
    {
        get { return m_ReminderDate; }
        set { ReminderDate = value; }
    }

    public int CategoryKey
    {
        get { return m_CategoryKey; }
        set { m_CategoryKey = value; }
    }
    public int CustomerKey
    {
        get {return m_CustomerKey;}
        set {m_CustomerKey=value ;}
    }
    public string CREATE_BY
    {
        get { return m_CREATE_BY; }
        set { m_CREATE_BY = value; }
    }
    public DateTime  CREATE_ON
    {
        get { return m_CREATE_ON; }
        set { m_CREATE_ON = value; }
    }
    public string MODIFY_BY
    {
        get { return m_MODIFY_BY; }
        set { m_MODIFY_BY = value; }
    }

    public DateTime MODIFY_ON
    {
        get { return m_MODIFY_ON; }
        set { m_MODIFY_ON = value; }
    }

    #endregion

    #region [Constructor]

    public CRM_Tasks_Info()
    {
        
    }

    public CRM_Tasks_Info(int strTaskKey)
    {


        string strSelect = "SELECT * FROM CRM_Appointments WHERE TaskKey=@TaskKey";

        // create and open oracle connection
        SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
        Connection.Open();

        // create oracle command object
        SqlCommand command = Connection.CreateCommand();

        command.CommandText = strSelect;

        command.Parameters.Add("@TaskKey", SqlDbType.Int).Value = strTaskKey;

        SqlDataReader nReader = command.ExecuteReader();

        if (nReader.HasRows)
        {
            nReader.Read();
          
            m_TaskKey = int.Parse(nReader["TaskKey"].ToString());
            m_TaskName = nReader["TaskName"].ToString();
            m_TaskContent = nReader["TaskContent"].ToString();

            if (nReader["StartDate"] != DBNull.Value)
                m_StartDate = (DateTime)nReader["StartDate"];
            if (nReader["DueDate"] != DBNull.Value)
                m_DueDate = (DateTime)nReader["DueDate"];            
             
            m_StatusKey = int.Parse(nReader["StatusKey"].ToString());
            m_Priority = int.Parse(nReader["Priority"].ToString());
            m_Complete = int.Parse(nReader["Complete"].ToString());
            if (nReader["ReminderDate"] != DBNull.Value)
                m_ReminderDate = (DateTime)nReader["ReminderDate"];

            m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
            m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());

            m_CREATE_BY = nReader["CreatedBy"].ToString();
            m_MODIFY_BY = nReader["ModifiedBy"].ToString();

            if (nReader["CreatedOn"] != DBNull.Value)
                m_CREATE_ON = (DateTime)nReader["CreatedOn"];
            if (nReader["ModifiedOn"] != DBNull.Value)
                m_MODIFY_ON = (DateTime)nReader["ModifiedOn"];

        }
        nReader.Close();
        command.Dispose();


        // Close and Dispose SqlConnection object
        Connection.Close();
        Connection.Dispose();

    }
    #endregion

    #region [Method]

    public string Update()
    {
        string Result = "";
        try
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "dbo.UPDATE_APPOINTMENTS";
            // create and open oracle connection
            SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
            Connection.Open();
            // create oracle command object
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = nSQL;
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.Add("@TaskKey", SqlDbType.Int ).Value = m_TaskKey;
            command.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = m_TaskName;
            command.Parameters.Add("@TaskContent", SqlDbType.Int).Value = m_TaskContent;

            command.Parameters.Add("@StartDate", SqlDbType.DateTime ).Value = m_StartDate;
            command.Parameters.Add("@DueDate", SqlDbType.DateTime ).Value = m_DueDate;

            command.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
            command.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
            command.Parameters.Add("@Priority", SqlDbType.Int).Value = m_Priority;
            command.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = m_ReminderDate;
            command.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
            command.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
            
            command.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_MODIFY_BY;
            command.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = m_MODIFY_ON;
            Result = command.ExecuteNonQuery().ToString();
            // Close and Dispose SqlConnection object
            Connection.Close();
            Connection.Dispose();
        }
        catch (Exception err)
        {

            Result = err.Message.ToString();
        }
        return Result;
    }

    public string Create()
    {
        string Result = "";
        try
        {
            string nSQL = "dbo.INSERT_APPOINTMENTS";
            // create and open oracle connection
            SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
            Connection.Open();

            // create oracle command object
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = nSQL;
            command.CommandType = CommandType.StoredProcedure;


            command.Parameters.Add("@TaskKey", SqlDbType.Int).Value = m_TaskKey;
            command.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = m_TaskName;
            command.Parameters.Add("@TaskContent", SqlDbType.Int).Value = m_TaskContent;

            command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
            command.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;

            command.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
            command.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
            command.Parameters.Add("@Priority", SqlDbType.Int).Value = m_Priority;
            command.Parameters.Add("@ReminderDate", SqlDbType.DateTime).Value = m_ReminderDate;
            command.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
            command.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;            
          
            command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CREATE_BY;
            command.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = m_CREATE_ON;

            Result = command.ExecuteNonQuery().ToString();

            // Close and Dispose SqlConnection object
            Connection.Close();
            Connection.Dispose();
        }
        catch (Exception err)
        {
            Result = err.Message.ToString();
        }
        return Result;
    }

    public string Delete()
    {
        string Result = "";

        //---------- String SQL Access Database ---------------
        //string nSQL = "DELETE FROM VMS_MASTER WHERE QTN_NUM = @QTN_NUM";
        string nSQL = "dbo.DELETE_APPOINTMENTS";

        // create and open oracle connection
        SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
        Connection.Open();

        // create oracle command object
        SqlCommand command = Connection.CreateCommand();
        command.CommandText = nSQL;
        command.CommandType = CommandType.StoredProcedure;
        

        command.Parameters.Add("@TaskKey", SqlDbType.VarChar).Value = m_TaskKey;
        Result = command.ExecuteNonQuery().ToString();

        // Close and Dispose SqlConnection object
        Connection.Close();
        Connection.Dispose();
        return Result;
    }

   
    #endregion

}

