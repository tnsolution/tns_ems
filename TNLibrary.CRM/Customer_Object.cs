﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using TNConfig;

namespace TNLibrary.CRM
{
    public class Customer_Object :Customer_Info
    {
        private DataTable m_ContactPersonals = new DataTable();

        #region [ Constructor Get Information ]

        public Customer_Object() :base()
        {
            m_ContactPersonals = GetContactPersonals();
        }
        public Customer_Object(int CustomerKey)
            : base(CustomerKey)
        {
            m_ContactPersonals = GetContactPersonals();
        }
        public Customer_Object(string CustomerID)
            : base(CustomerID)
        {
            m_ContactPersonals = GetContactPersonals();
        }

        #endregion

        #region [Properties]

        public DataTable ContactPersonals
        {
            set
            {
                m_ContactPersonals = value;
            }
            get
            {
                return m_ContactPersonals;
            }
        }
        
        #endregion

        #region [ Constructor Update Information ]

        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            DelContactPersonals();
            SaveContactPersonals();
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DelContactPersonals();
            return nResult;
        }

        #endregion

        #region [ Contact Personals ]
        private DataTable GetContactPersonals()
        {
            DataTable nTable = new DataTable();
            string strSQL = " SELECT * FROM CRM_ContactPersonal WHERE CustomerKey = @CustomerKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@CustomerKey",SqlDbType.Int).Value = base.Key;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);

                nAdapter.Fill(nTable);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }       
        private void SaveContactPersonals()
        {
            foreach (DataRow nRow in m_ContactPersonals.Rows)
            {
                ContactPersonal_Info nPerson = new ContactPersonal_Info();
                nPerson.CustomerKey = base.Key;
                nPerson.ContactName = nRow["ContactName"].ToString();
                nPerson.MobiPhone = nRow["MobiPhone"].ToString();
                nPerson.BusinessPhone = nRow["BusinessPhone"].ToString();
                nPerson.Email = nRow["Email"].ToString();
                nPerson.DepartmentName = nRow["DepartmentName"].ToString();
                nPerson.Position = nRow["Position"].ToString();
                nPerson.Save();
            }
        }
        private string DelContactPersonals()
        {
            string nResult = "";
            string strSQL = " DELETE FROM CRM_ContactPersonal WHERE CustomerKey = @CustomerKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@CustomerKey",SqlDbType.Int).Value =  base.Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
