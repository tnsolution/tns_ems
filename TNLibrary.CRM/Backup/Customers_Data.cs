﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.CRM
{
    public class Customers_Data
    {
        public static DataTable AllList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT A.*, B.CategoryName  CategoryName FROM CRM_Customers A "
                    + " INNER JOIN CRM_CustomerCategories B ON A.CategoryKey = B.CategoryKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }

        public static DataTable AllList(string CustomerID, string CustomerName, string TaxNumber, string PhoneNumber)
        {

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT * FROM CRM_Customers WHERE  "
                          + "  CustomerID   LIKE @CustomerID "
                          + " AND CustomerName LIKE @CustomerName "
                          + " AND TaxNumber    LIKE @TaxNumber "
                          + " AND Phone        LIKE @Phone ";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%"; ;
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = "%" + TaxNumber + "%";
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + PhoneNumber + "%";

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }

        #region [ Customer ]
        public static DataTable CustomerList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM CRM_Customers WHERE IsCustomer = 1 ORDER BY CustomerName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable CustomerList(string CustomerID, string CustomerName, string TaxNumber, string PhoneNumber)
        {

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT * FROM CRM_Customers WHERE IsCustomer = 1 "
                          + " AND CustomerID   LIKE @CustomerID "
                          + " AND CustomerName LIKE @CustomerName "
                          + " AND TaxNumber    LIKE @TaxNumber "
                          + " AND Phone        LIKE @Phone ";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%"; ;
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = "%" + TaxNumber + "%";
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + PhoneNumber + "%";

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }
        public static DataTable CustomerList(string CustomerName,string PhoneNumber, int BranchKey)
        {

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT * FROM CRM_Customers WHERE IsCustomer = 1 "
                          + " AND CustomerName LIKE @CustomerName "
                          + " AND Phone        LIKE @Phone "
                          + " AND BranchKey    = @BranchKey ";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = CustomerName ;
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value =  PhoneNumber ;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }

        #endregion

        #region [ Vendor ]

        public static DataTable VendorList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM CRM_Customers WHERE IsVendor = 1 ORDER BY CustomerName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable VendorList(string CustomerID, string CustomerName, string TaxNumber, string PhoneNumber)
        {
            string nSQL = " SELECT * FROM CRM_Customers WHERE IsVendor = 1 "
                           + " AND CustomerID   LIKE @CustomerID "
                           + " AND CustomerName LIKE @CustomerName "
                           + " AND TaxNumber    LIKE @TaxNumber "
                           + " AND Phone        LIKE @Phone ";

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%"; ;
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = "%" + TaxNumber + "%";
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + PhoneNumber + "%";

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }
        public static DataTable VendorList(string CustomerName, string PhoneNumber, int BranchKey)
        {

            DataTable nTable = new DataTable();
            string nConnectionString = ConnectDataBase.ConnectionString;
            string nSQL = " SELECT * FROM CRM_Customers WHERE IsVendor = 1 "
                          + " AND CustomerName LIKE @CustomerName "
                          + " AND Phone        LIKE @Phone "
                          + " AND BranchKey    = @BranchKey ";
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value =  CustomerName ;
                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value =  PhoneNumber ;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }
            return nTable;
        }
        #endregion

        public static int CheckExistCustomer(int CustomerKey)
        {
            int nResult = 0;
            string nSQL = "CRM_CheckExistCustomer";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.CommandType = CommandType.StoredProcedure;
            nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();
            return nResult;
        }

        public static string GetAutoCustomerID(string ID)
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT dbo.AutoCustomerID(@ID)";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                nResult = nCommand.ExecuteScalar().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
    }
}