﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using TNConfig;

public class CRM_Data_Dropdown
{

    public static void DropDown_Nation(DropDownList ddl)
    {
        DataTable dtt = new DataTable();
        SqlConnection conn = new SqlConnection(ConnectWedInfo.ConnectionString);
        conn.Open();
        try
        {
            string strSql = "";

            strSql += " SELECT NAT_CODE, NAT_NAME";
            strSql += " FROM SYS_NATION ORDER BY NAT_NAME";

            SqlCommand ocm = new SqlCommand(strSql, conn);

            SqlDataAdapter oda = new SqlDataAdapter(ocm);

            oda.Fill(dtt);

            ddl.DataSource = dtt;

            ddl.DataTextField = dtt.Columns[1].ColumnName;
            ddl.DataValueField = dtt.Columns[0].ColumnName;
            ddl.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }
    }
    public static void CustomerCategories(DropDownList ddl)
    {
        DataTable dtt = new DataTable();
        SqlConnection conn = new SqlConnection(ConnectWedInfo.ConnectionString);
        conn.Open();
        try
        {
            string strSql = "";

            strSql += " SELECT CategoryKey, CategoryName";
            strSql += " FROM CRM_CustomerCategories ORDER BY CategoryName";

            SqlCommand ocm = new SqlCommand(strSql, conn);

            SqlDataAdapter oda = new SqlDataAdapter(ocm);

            oda.Fill(dtt);

            ddl.DataSource = dtt;

            ddl.DataTextField = dtt.Columns[1].ColumnName;
            ddl.DataValueField = dtt.Columns[0].ColumnName;
            ddl.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conn.Close();
        }
    }
  
}

