﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using TNConfig;
namespace TNLibrary.CRM
{
    public class Task_Info
    {

        #region [ Field Table ]

        private int m_TaskKey = 0;
        private string m_TaskName = "";
        private string m_TaskContent = "";

        private Nullable<DateTime> m_StartDate;
        private Nullable<DateTime> m_DueDate;

        private int m_StatusKey = 0;
        private string m_Priority = "";
        private int m_Complete = 0;
        private DateTime m_ReminderDate;
        private int m_CategoryKey = 0;

        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";

        private int m_ProjectKey = 0;
        private string m_ProjectName = "";

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;

        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";
        #endregion

        #region [Constructor]

        public Task_Info()
        {

        }

        public Task_Info(int TaskKey)
        {

            string nSQL = " SELECT A.*,B.CustomerID, B.CustomerName,C.ProjectName FROM CRM_Tasks A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN CRM_Projects C ON A.ProjectKey = C.ProjectKey "
                        + " WHERE A.TaskKey = @TaskKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = TaskKey;

                SqlDataReader nReader = nCommand.ExecuteReader();

                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_TaskKey = int.Parse(nReader["TaskKey"].ToString());
                    m_TaskName = nReader["TaskName"].ToString();
                    m_TaskContent = nReader["TaskContent"].ToString();

                    if (nReader["StartDate"] != DBNull.Value)
                        m_StartDate = (DateTime)nReader["StartDate"];
                    if (nReader["DueDate"] != DBNull.Value)
                        m_DueDate = (DateTime)nReader["DueDate"];

                    m_StatusKey = int.Parse(nReader["StatusKey"].ToString());
                    m_Priority = nReader["Priority"].ToString();
                    m_Complete = int.Parse(nReader["Complete"].ToString());
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
                    if (nReader["ReminderDate"] != DBNull.Value)
                        m_ReminderDate = (DateTime)nReader["ReminderDate"];

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_ProjectKey = int.Parse(nReader["ProjectKey"].ToString());
                    m_ProjectName = nReader["ProjectName"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    m_ModifiedBy = nReader["ModifiedBy"].ToString();

                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]

        public int TaskKey
        {
            get { return m_TaskKey; }
            set { m_TaskKey = value; }
        }
        public string TaskName
        {
            get { return m_TaskName; }
            set { m_TaskName = value; }
        }
        public string TaskContent
        {
            get { return m_TaskContent; }
            set { m_TaskContent = value; }
        }
        public Nullable<DateTime> StartDate
        {
            get { return m_StartDate; }
            set { m_StartDate = value; }
        }
        public Nullable<DateTime> DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public int StatusKey
        {
            get { return m_StatusKey; }
            set { m_StatusKey = value; }
        }

        public string Priority
        {
            get { return m_Priority; }
            set { m_Priority = value; }
        }
        public int Complete
        {
            get { return m_Complete; }
            set { m_Complete = value; }
        }
        public DateTime ReminderDate
        {
            get { return m_ReminderDate; }
            set { ReminderDate = value; }
        }

        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }

        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set{m_CustomerKey = value;}
        }

        public string CustomerID
        {
            get { return m_CustomerID; }
        }
        public string CustomerName
        {
            get { return m_CustomerName; }
        }

        public int ProjectKey
        {
            get { return m_ProjectKey; }
            set { m_ProjectKey = value; }
        }
        public string ProjectName
        {
            get { return m_ProjectName; }
        }

        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }
        public DateTime CreatedDateTime
        {
            get { return m_CreatedDateTime; }
            set { m_CreatedDateTime = value; }
        }
        public string ModifiedBy
        {
            get { return m_ModifiedBy; }
            set { m_ModifiedBy = value; }
        }

        public DateTime ModifiedDateTime
        {
            get { return m_ModifiedDateTime; }
            set { m_ModifiedDateTime = value; }
        }

        public string Message
        {
            set
            {
                m_Message = value;
            }
            get
            {
                return m_Message;
            }
        }
        #endregion  

        #region [Method]
        public string Save()
        {
            if (m_TaskKey == 0)
                return Create();
            else
                return Update();
        }
        public string Update()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE CRM_Tasks SET "
                         + " TaskName = @TaskName, "
                         + " TaskContent = @TaskContent, "
                         + " StartDate = @StartDate, "
                         + " DueDate = @DueDate, "
                         + " StatusKey = @StatusKey, "
                         + " Priority = @Priority, "
                         + " Complete = @Complete, "                    
                         + " CategoryKey = @CategoryKey, "                       
                         + " ProjectKey = @ProjectKey, "
                         + " CustomerKey = @CustomerKey, "
                         + " ModifiedBy = @ModifiedBy, "
                         + " ModifiedDateTime = getdate() "
                         + " WHERE TaskKey = @TaskKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = m_TaskKey;
                nCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = m_TaskName;
                nCommand.Parameters.Add("@TaskContent", SqlDbType.NText).Value = m_TaskContent;

                if (m_StartDate != null)
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
                else
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                if (m_DueDate != null)
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;
                else
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
                nCommand.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
                nCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = m_Priority;
                
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = m_ProjectKey;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
        public string Create()
        {
            string nResult = "";
            string nSQL = "INSERT INTO CRM_Tasks(TaskName, TaskContent, StartDate, DueDate, StatusKey, Priority, "
                + " Complete,  CategoryKey, ProjectKey,CustomerKey, CreatedBy, CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                + " VALUES (@TaskName, @TaskContent, @StartDate, @DueDate, @StatusKey, @Priority, "
                + " @Complete,  @CategoryKey, @ProjectKey,@CustomerKey, @CreatedBy, getdate(),@ModifiedBy,getdate())";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;


                nCommand.Parameters.Add("@TaskKey", SqlDbType.Int).Value = m_TaskKey;
                nCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = m_TaskName;
                nCommand.Parameters.Add("@TaskContent", SqlDbType.NVarChar).Value = m_TaskContent;

                if (m_StartDate != null)
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
                else
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                if (m_DueDate != null)
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;
                else
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
                nCommand.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
                nCommand.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = m_Priority;
               
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = m_ProjectKey;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;
                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            string nSQL = "DELETE CRM_Tasks WHERE TaskKey = @TaskKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TaskKey", SqlDbType.VarChar).Value = m_TaskKey;
                nResult = nCommand.ExecuteNonQuery().ToString();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }


        #endregion

    }

}