﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.CRM
{
    public class ContactPersonal_Info
    {
        private int m_ContactKey = 0;

        private int m_CustomerKey = 0;
        private string m_ContactName = "";

        private bool m_Gender = false;
        private DateTime m_BirthDate = DateTime.Now;
        private bool m_MaritalStatus = false;

        private string m_CurrentNationality = "";
        private string m_PassportNumber = "";
        private DateTime m_PassportDate = DateTime.Now;
        private string m_PassportIssue = "";

        private string m_Occupation = "";

        private string m_HomePhone = "";
        private string m_MobiPhone = "";

        private string m_BusinessPhone = "";
        private string m_DepartmentName = "";
        private string m_Position = "";
        private string m_Fax = "";
        private string m_Email = "";

        private string m_Message = "";

        public ContactPersonal_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ContactPersonal_Info(int CustomerKey)
        {
            string nSQL = "SELECT * FROM CRM_PersonalContact WHERE CustomerKey =@CustomerKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_ContactName = nReader["ContactName"].ToString();
                    m_Gender = (bool)nReader["Gender"];
                    m_BirthDate = (DateTime)nReader["BirthDate"];
                    m_MaritalStatus = (bool)nReader["MaritalStatus"];
                    m_CurrentNationality = nReader["CurrentNationality"].ToString();

                    m_PassportNumber = nReader["PassportNumber"].ToString();
                    m_PassportDate = (DateTime)nReader["PassportDate"];
                    m_PassportIssue = nReader["PassportIssue"].ToString();

                    m_Occupation = nReader["Occupation"].ToString();

                    m_HomePhone = nReader["HomePhone"].ToString();
                    m_MobiPhone = nReader["MobiPhone"].ToString();

                    m_BusinessPhone = nReader["BusinessPhone"].ToString();
                    m_DepartmentName = nReader["DepartmentName"].ToString();
                    m_Position = nReader["Position"].ToString();
                    m_Fax = nReader["Fax"].ToString();
                    m_Email = nReader["Email"].ToString();


                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }

        #region [ Properties ]
        // Info Customer
        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string ContactName
        {
            get { return m_ContactName; }
            set { m_ContactName = value; }
        }
        public bool Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }
        public DateTime BirthDate
        {
            get { return m_BirthDate; }
            set { m_BirthDate = value; }
        }
        public bool MaritalStatus
        {
            get { return m_MaritalStatus; }
            set { m_MaritalStatus = value; }
        }

        public string CurrentNationality
        {
            get { return m_CurrentNationality; }
            set { m_CurrentNationality = value; }
        }
        public DateTime PassportDate
        {
            get { return m_PassportDate; }
            set { m_PassportDate = value; }
        }
        public string PassportNumber
        {
            get { return m_PassportNumber; }
            set { m_PassportNumber = value; }
        }
        public string PassportIssue
        {
            get { return m_PassportIssue; }
            set { m_PassportIssue = value; }
        }
        public string Occupation
        {
            get { return m_Occupation; }
            set { m_Occupation = value; }
        }
        public string HomePhone
        {
            get { return m_HomePhone; }
            set { m_HomePhone = value; }
        }
        public string MobiPhone
        {
            get { return m_MobiPhone; }
            set { m_MobiPhone = value; }
        }

        public string BusinessPhone
        {
            get { return m_BusinessPhone; }
            set { m_BusinessPhone = value; }
        }
        public string DepartmentName
        {
            get { return m_DepartmentName; }
            set { m_DepartmentName = value; }
        }
        public string Position
        {
            get { return m_Position; }
            set { m_Position = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        #endregion

        #region [ Methor ]
        public string Update()
        {
            string nResult = "";


            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE CRM_PersonalContact SET "
                        + " ContactName = @ContactName, "
                        + " CustomerKey = @CustomerKey, "
                        + " Gender = @Gender,"
                        + " BirthDate = @BirthDate ,"
                        + " MaritalStatus = @MaritalStatus ,"

                        + " CurrentNationality = @CurrentNationality ,"
                        + " PassportNumber = @PassportNumber ,"
                        + " PassportDate = @PassportDate ,"
                        + " PassportIssue = @PassportIssue ,"

                        + " Occupation = @Occupation ,"
                        + " HomePhone = @HomePhone ,"
                        + " MobiPhone = @MobiPhone ,"

                        + " BusinessPhone = @BusinessPhone ,"
                        + " DepartmentName = @DepartmentName ,"
                        + " Position = @Position ,"
                        + " Fax= @Fax, "
                        + " Email = @Email  "
                        + " WHERE ContactKey = @ContactKey";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ContactKey", SqlDbType.NVarChar).Value = m_ContactKey;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = m_CustomerKey;
                nCommand.Parameters.Add("@ContactName", SqlDbType.NVarChar).Value = m_ContactName;
                nCommand.Parameters.Add("@Gender", SqlDbType.Bit).Value = m_Gender;
                nCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = m_BirthDate;
                nCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = m_MaritalStatus;

                nCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = m_CurrentNationality;
                nCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = m_PassportNumber;
                nCommand.Parameters.Add("@PassportDate", SqlDbType.DateTime).Value = m_PassportDate;
                nCommand.Parameters.Add("@PassportIssue", SqlDbType.NVarChar).Value = m_PassportIssue;

                nCommand.Parameters.Add("@Occupation", SqlDbType.NVarChar).Value = m_Occupation;
                nCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = m_HomePhone;
                nCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = m_MobiPhone;

                nCommand.Parameters.Add("@BusinessPhone", SqlDbType.NVarChar).Value = m_BusinessPhone;
                nCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = m_DepartmentName;
                nCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = m_Position;
                nCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = m_Fax;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO CRM_ContactPersonal( "
                        + " CustomerKey,ContactName,Gender, BirthDate,MaritalStatus,CurrentNationality,PassportNumber,PassportDate,PassportIssue,Occupation, HomePhone, MobiPhone,"
                        + " BusinessPhone,DepartmentName,Position,Fax,Email)"
                        + " VALUES( @CustomerKey,@ContactName,@Gender, @BirthDate,@MaritalStatus,@CurrentNationality,@PassportNumber,@PassportDate,@PassportIssue,@Occupation, @HomePhone, @MobiPhone,"
                        + " @BusinessPhone,@DepartmentName,@Position,@Fax,@Email)"
                        + " SELECT ContactKey FROM CRM_ContactPersonal WHERE ContactKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();

            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.NVarChar).Value = m_CustomerKey;
                nCommand.Parameters.Add("@ContactName", SqlDbType.NVarChar).Value = m_ContactName;
                nCommand.Parameters.Add("@Gender", SqlDbType.Bit).Value = m_Gender;
                nCommand.Parameters.Add("@BirthDate", SqlDbType.DateTime).Value = m_BirthDate;
                nCommand.Parameters.Add("@MaritalStatus", SqlDbType.Bit).Value = m_MaritalStatus;

                nCommand.Parameters.Add("@CurrentNationality", SqlDbType.NVarChar).Value = m_CurrentNationality;
                nCommand.Parameters.Add("@PassportNumber", SqlDbType.NVarChar).Value = m_PassportNumber;
                nCommand.Parameters.Add("@PassportDate", SqlDbType.DateTime).Value = m_PassportDate;
                nCommand.Parameters.Add("@PassportIssue", SqlDbType.NVarChar).Value = m_PassportIssue;

                nCommand.Parameters.Add("@Occupation", SqlDbType.NVarChar).Value = m_Occupation;
                nCommand.Parameters.Add("@HomePhone", SqlDbType.NVarChar).Value = m_HomePhone;
                nCommand.Parameters.Add("@MobiPhone", SqlDbType.NVarChar).Value = m_MobiPhone;

                nCommand.Parameters.Add("@BusinessPhone", SqlDbType.NVarChar).Value = m_BusinessPhone;
                nCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = m_DepartmentName;
                nCommand.Parameters.Add("@Position", SqlDbType.NVarChar).Value = m_Position;
                nCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = m_Fax;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;

                nResult = nCommand.ExecuteScalar().ToString();
                m_ContactKey = int.Parse(nResult);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (m_ContactKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM CRM_ContactPersonal WHERE ContactKey = @ContactKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_ContactKey;
                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
