﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.CRM
{
    public class Customer_Info
    {
        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";

        private string m_CurrencyID = "VND";
        private string m_AccountBank = "";
        private string m_TaxNumber = "";

        private string m_Address = "";
        private string m_CityName = "";
        private string m_ProvinceName = "";
        private int m_CountryKey = 1;
        private string m_CountryName = "";

        private string m_Phone = "";
        private string m_Fax = "";
        private string m_Email = "";
        private string m_WebSite = "";

        private string m_Note = "";

        private int m_CategoryKey = 0;
        private string m_CategoryName = "";

        private int m_CustomerType = 1;
        private int m_BranchKey = 0;
        private bool m_IsVendor;
        private bool m_IsCustomer;
        private string m_Message = "";

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        public Customer_Info()
        {
            //
            // TODO: Add constructor logic here
            //

        }

        #region [ Constructor Get Information ]

        public Customer_Info(int CustomerKey)
        {
            string nSQL = " SELECT A.*,B.CountryName,C.CategoryName FROM CRM_Customers A "
                    + " LEFT JOIN SYS_Loc_Country B ON B.CountryKey = A.CountryKey "
                    + " LEFT JOIN CRM_CustomerCategories C ON C.CategoryKey = A.CategoryKey "
                    + " WHERE A.CustomerKey = @CustomerKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString().Trim();
                    m_CustomerName = nReader["CustomerName"].ToString().Trim();

                    m_CurrencyID = nReader["CurrencyID"].ToString();
                    m_TaxNumber = nReader["TaxNumber"].ToString();
                    m_AccountBank = nReader["AccountBank"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_CityName = nReader["CityName"].ToString();
                    m_ProvinceName = nReader["ProvinceName"].ToString();
                    m_CountryKey = (int)nReader["CountryKey"];
                    m_CountryName = nReader["CountryName"].ToString();

                    m_Phone = nReader["Phone"].ToString();
                    m_Fax = nReader["Fax"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_WebSite = nReader["WebSite"].ToString();

                    m_Note = nReader["Note"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_CategoryName = nReader["CategoryName"].ToString();

                    m_CustomerType = (int)nReader["CustomerType"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_IsVendor = (bool)nReader["IsVendor"];
                    m_IsCustomer = (bool)nReader["IsCustomer"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Customer_Info(string CustomerID)
        {
            string nSQL = " SELECT A.*,B.CountryName,C.CategoryName FROM CRM_Customers A "
             + " LEFT JOIN SYS_Loc_Country B ON B.CountryKey = A.CountryKey "
             + " LEFT JOIN CRM_CustomerCategories C ON C.CategoryKey = A.CategoryKey "
             + " WHERE A.CustomerID = @CustomerID ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = CustomerID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString().Trim();
                    m_CustomerName = nReader["CustomerName"].ToString().Trim();

                    m_CurrencyID = nReader["CurrencyID"].ToString();
                    m_TaxNumber = nReader["TaxNumber"].ToString();
                    m_AccountBank = nReader["AccountBank"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_CityName = nReader["CityName"].ToString();
                    m_ProvinceName = nReader["ProvinceName"].ToString();
                    m_CountryKey = (int)nReader["CountryKey"];
                    m_CountryName = nReader["CountryName"].ToString();

                    m_Phone = nReader["Phone"].ToString();
                    m_Fax = nReader["Fax"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_WebSite = nReader["WebSite"].ToString();

                    m_Note = nReader["Note"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_CategoryName = nReader["CategoryName"].ToString();

                    m_CustomerType = (int)nReader["CustomerType"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_IsVendor = (bool)nReader["IsVendor"];
                    m_IsCustomer = (bool)nReader["IsCustomer"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public void GetCustomerInfo()
        {
            string nSQL = " SELECT A.* ,B.CountryName,C.CategoryNameFROM CRM_Customers A "
                   + " LEFT JOIN SYS_Loc_Country B ON B.CountryKey = A.CountryKey "
                   + " LEFT JOIN CRM_CustomerCategories C ON C.CategoryKey = A.CategoryKey "
                   + " WHERE A.CustomerKey = @CustomerKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString().Trim();
                    m_CustomerName = nReader["CustomerName"].ToString().Trim();

                    m_CurrencyID = nReader["CurrencyID"].ToString();
                    m_TaxNumber = nReader["TaxNumber"].ToString();
                    m_AccountBank = nReader["AccountBank"].ToString();

                    m_Address = nReader["Address"].ToString();
                    m_CityName = nReader["CityName"].ToString();
                    m_ProvinceName = nReader["ProvinceName"].ToString();
                    m_CountryKey = (int)nReader["CountryKey"];
                    m_CountryName = nReader["CountryName"].ToString();

                    m_Phone = nReader["Phone"].ToString();
                    m_Fax = nReader["Fax"].ToString();
                    m_Email = nReader["Email"].ToString();
                    m_WebSite = nReader["WebSite"].ToString();

                    m_Note = nReader["Note"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_CategoryName = nReader["CategoryName"].ToString();
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_CustomerType = (int)nReader["CustomerType"];
                    m_IsVendor = (bool)nReader["IsVendor"];
                    m_IsCustomer = (bool)nReader["IsCustomer"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }

        #endregion

        #region [ Properties ]

        public int Key
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string ID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
        public string Name
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }

        public string CurrencyID
        {
            get { return m_CurrencyID; }
            set { m_CurrencyID = value; }
        }
        public string AccountBank
        {
            get { return m_AccountBank; }
            set { m_AccountBank = value; }
        }
        public string TaxNumber
        {
            get { return m_TaxNumber; }
            set { m_TaxNumber = value; }
        }

        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }
        public string CityName
        {
            get { return m_CityName; }
            set { m_CityName = value; }
        }
        public string ProvinceName
        {
            get { return m_ProvinceName; }
            set { m_ProvinceName = value; }
        }
        public int CountryKey
        {
            get { return m_CountryKey; }
            set { m_CountryKey = value; }
        }
        public string CountryName
        {
            get { return m_CountryName; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
        public string WebSite
        {
            get { return m_WebSite; }
            set { m_WebSite = value; }
        }

        public string Note
        {
            get { return m_Note; }
            set { m_Note = value; }
        }

        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return m_CategoryName; }
        }

        public int CustomerType
        {
            get { return m_CustomerType; }
            set { m_CustomerType = value; }
        }
        public int BranchKey
        {
            get { return m_BranchKey; }
            set { m_BranchKey = value; }
        }
        public bool IsVendor
        {
            get { return m_IsVendor; }
            set { m_IsVendor = value; }
        }
        public bool IsCustomer
        {
            get { return m_IsCustomer; }
            set { m_IsCustomer = value; }
        }

        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }

      
        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE CRM_Customers SET "
                        + " CustomerID = @CustomerID,"
                        + " CustomerName = @CustomerName ,"

                        + " CurrencyID = @CurrencyID, "
                        + " TaxNumber = @TaxNumber, "
                        + " AccountBank = @AccountBank, "

                        + " Address = @Address ,"
                        + " CityName = @CityName ,"
                        + " ProvinceName = @ProvinceName ,"
                        + " CountryKey = @CountryKey ,"

                        + " Phone = @Phone ,"
                        + " Fax= @Fax, "
                        + " Email = @Email  ,"
                        + " Website = @Website ,"
                        + " Note = @Note, "

                        + " CategoryKey = @CategoryKey,"
                        + " CustomerType = @CustomerType,"
                        + " IsVendor = @IsVendor,"
                        + " IsCustomer = @IsCustomer,"

                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE CustomerKey = @CustomerKey";

            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = m_CustomerID;
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = m_CustomerName;

                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NChar).Value = m_CurrencyID;
                nCommand.Parameters.Add("@AccountBank", SqlDbType.NChar).Value = m_AccountBank;
                nCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = m_TaxNumber;

                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = m_CityName;
                nCommand.Parameters.Add("@ProvinceName", SqlDbType.NVarChar).Value = m_ProvinceName;
                nCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = m_CountryKey;

                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = m_Phone;
                nCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = m_Fax;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;
                nCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = m_WebSite;

                nCommand.Parameters.Add("@Note", SqlDbType.NText).Value = m_Note;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = m_CustomerType;
                nCommand.Parameters.Add("@IsVendor", SqlDbType.Bit).Value = m_IsVendor;
                nCommand.Parameters.Add("@IsCustomer", SqlDbType.Bit).Value = m_IsCustomer;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO CRM_Customers( "
                        + " CustomerID, CustomerName, AccountBank, TaxNumber,CurrencyID,"
                        + " Address, CityName, ProvinceName, CountryKey, "
                        + " Phone,Fax,Email,Website,  Note, CategoryKey,BranchKey, CustomerType, IsVendor,IsCustomer,"
                        + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                        + " VALUES(@CustomerID, @CustomerName, @AccountBank, @TaxNumber,@CurrencyID, "
                        + " @Address, @CityName, @ProvinceName, @CountryKey, "
                        + " @Phone,@Fax,@Email,@Website,  @Note,@CategoryKey,@BranchKey,@CustomerType,@IsVendor,@IsCustomer, "
                        + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                        + " SELECT CustomerKey FROM CRM_Customers WHERE CustomerKey = SCOPE_IDENTITY()";

            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = m_CustomerID;
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = m_CustomerName;

                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NChar).Value = m_CurrencyID;
                nCommand.Parameters.Add("@AccountBank", SqlDbType.NChar).Value = m_AccountBank;
                nCommand.Parameters.Add("@TaxNumber", SqlDbType.NVarChar).Value = m_TaxNumber;

                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;
                nCommand.Parameters.Add("@CityName", SqlDbType.NVarChar).Value = m_CityName;
                nCommand.Parameters.Add("@ProvinceName", SqlDbType.NVarChar).Value = m_ProvinceName;
                nCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = m_CountryKey;

                nCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = m_Phone;
                nCommand.Parameters.Add("@Fax", SqlDbType.NVarChar).Value = m_Fax;
                nCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = m_Email;
                nCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = m_WebSite;

                nCommand.Parameters.Add("@Note", SqlDbType.NText).Value = m_Note;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nCommand.Parameters.Add("@CustomerType", SqlDbType.Int).Value = m_CustomerType;
                nCommand.Parameters.Add("@IsVendor", SqlDbType.Bit).Value = m_IsVendor;
                nCommand.Parameters.Add("@IsCustomer", SqlDbType.Bit).Value = m_IsCustomer;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                m_CustomerKey = int.Parse(nResult);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (m_CustomerKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM CRM_Customers WHERE CustomerKey = @CustomerKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

     
    }
}

