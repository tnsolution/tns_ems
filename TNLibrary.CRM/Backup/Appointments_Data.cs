﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

  public  class CRM_Appointments_Data
    {
      public static DataTable Appoint_List(int strCustomerKey)
        {
            SqlConnection conn = new SqlConnection(ConnectWedInfo.ConnectionString);
            conn.Open();
            try
            {
                DataTable dtt = new DataTable();

                string strSql = "";

                strSql += " SELECT * FROM CRM_Appointments WHERE CustomerKey = @CustomerKey";
              
                SqlCommand ocm = new SqlCommand(strSql, conn);

                ocm.Parameters.Add("@CustomerKey",SqlDbType.Int).Value = strCustomerKey;

                SqlDataAdapter oda = new SqlDataAdapter(ocm);

                oda.Fill(dtt);

                return dtt;
            }
            finally
            {
                conn.Close();
            }
        }
    }

