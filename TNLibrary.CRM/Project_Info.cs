﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.CRM
{
    public class Project_Info
    {
        private int m_ProjectKey = 0;
        private string m_ProjectID = "";
        private string m_ProjectName = "";
        private string m_ProjectContent = "";

        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";

        private Nullable<DateTime> m_StartDate;
        private Nullable<DateTime> m_DueDate;

        private int m_StatusKey = 0;
        private int m_Complete = 0;
        private double m_ProjectPrice = 0;

        private string m_Message = "";
        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;


        public Project_Info()
        {
            //
            // TODO: Add constructor logic here
            //

        }

        #region [ Constructor Get Information ]

        public Project_Info(int ProjectKey)
        {
            string nSQL = " SELECT A.*,B.CustomerID,B.CustomerName FROM CRM_Projects  A"
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " WHERE A.ProjectKey = @ProjectKey";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ProjectKey = int.Parse(nReader["ProjectKey"].ToString());
                    m_ProjectID = nReader["ProjectID"].ToString();
                    m_ProjectName = nReader["ProjectName"].ToString().Trim();
                    m_ProjectContent = nReader["ProjectContent"].ToString().Trim();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    if (nReader["StartDate"] != DBNull.Value)
                        m_StartDate = (DateTime)nReader["StartDate"];
                    if (nReader["DueDate"] != DBNull.Value)
                        m_DueDate = (DateTime)nReader["DueDate"];

                    m_StatusKey = int.Parse(nReader["StatusKey"].ToString());
                    m_Complete = int.Parse(nReader["Complete"].ToString());

                    m_ProjectPrice = double.Parse(nReader["ProjectPrice"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Project_Info(string ProjectID)
        {
            string nSQL = " SELECT A.*,B.CustomerID,B.CustomerName FROM CRM_Projects  A"
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " WHERE A.ProjectID = @ProjectID";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = ProjectID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ProjectKey = int.Parse(nReader["ProjectID"].ToString());
                    m_ProjectID = nReader["ProjectID"].ToString();
                    m_ProjectName = nReader["ProjectName"].ToString().Trim();
                    m_ProjectContent = nReader["ProjectContent"].ToString().Trim();

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    if (nReader["StartDate"] != DBNull.Value)
                        m_StartDate = (DateTime)nReader["StartDate"];
                    if (nReader["DueDate"] != DBNull.Value)
                        m_DueDate = (DateTime)nReader["DueDate"];

                    m_StatusKey = int.Parse(nReader["StatusKey"].ToString());
                    m_Complete = int.Parse(nReader["Complete"].ToString());

                    m_ProjectPrice = double.Parse(nReader["ProjectPrice"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                nReader.Close();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]

        public int ProjectKey
        {
            get { return m_ProjectKey; }
            set { m_ProjectKey = value; }
        }
        public string ProjectID
        {
            get { return m_ProjectID; }
            set { m_ProjectID = value; }
        }
        public string ProjectName
        {
            get { return m_ProjectName; }
            set { m_ProjectName = value; }
        }
        public string ProjectContent
        {
            get { return m_ProjectContent; }
            set { m_ProjectContent = value; }
        }

        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }
        public Nullable<DateTime> StartDate
        {
            get { return m_StartDate; }
            set { m_StartDate = value; }
        }
        public Nullable<DateTime> DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public int StatusKey
        {
            get { return m_StatusKey; }
            set { m_StatusKey = value; }
        }
        public double ProjectPrice
        {
            get { return m_ProjectPrice; }
            set { m_ProjectPrice = value; }
        }
        public int Complete
        {
            get { return m_Complete; }
            set { m_Complete = value; }
        }
        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }


        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE CRM_Projects SET "
                        + " ProjectID = @ProjectID, "
                        + " ProjectName = @ProjectName,"
                        + " ProjectContent = @ProjectContent ,"

                        + " CustomerKey = @CustomerKey, "
                        + " StartDate = @StartDate, "
                        + " DueDate = @DueDate, "

                        + " StatusKey = @StatusKey ,"
                        + " ProjectPrice = @ProjectPrice ,"
                        + " Complete = @Complete, "
                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE ProjectKey = @ProjectKey";

            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = m_ProjectKey;
                nCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = m_ProjectID;
                nCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = m_ProjectName;
                nCommand.Parameters.Add("@ProjectContent", SqlDbType.NVarChar).Value = m_ProjectContent;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                if (m_StartDate != null)
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
                else
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;
                if (m_DueDate != null)
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;
                else
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
                nCommand.Parameters.Add("@ProjectPrice", SqlDbType.Money).Value = m_ProjectPrice;
                nCommand.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO CRM_Projects(ProjectID, ProjectName, ProjectContent, CustomerKey, StartDate, DueDate, "
                        + " StatusKey, ProjectPrice,Complete, CreatedBy, CreatedDateTime,ModifiedBy,ModifiedDateTime)"
                        + " VALUES( @ProjectID,@ProjectName, @ProjectContent, @CustomerKey, @StartDate, @DueDate, "
                        + " @StatusKey, @ProjectPrice, @Complete, @CreatedBy, getdate(),@ModifiedBy,getdate())";


            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {

                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProjectID", SqlDbType.NVarChar).Value = m_ProjectID;
                nCommand.Parameters.Add("@ProjectName", SqlDbType.NVarChar).Value = m_ProjectName;
                nCommand.Parameters.Add("@ProjectContent", SqlDbType.NVarChar).Value = m_ProjectContent;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                if (m_StartDate != null)
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
                else
                    nCommand.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = System.DBNull.Value;
                if (m_DueDate != null)
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;
                else
                    nCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = System.DBNull.Value;

                nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = m_StatusKey;
                nCommand.Parameters.Add("@ProjectPrice", SqlDbType.Money).Value = m_ProjectPrice;
                nCommand.Parameters.Add("@Complete", SqlDbType.Money).Value = m_Complete;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult;
            if (m_ProjectKey == 0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM CRM_Projects WHERE ProjectKey = @ProjectKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = m_ProjectKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion


    }
}

