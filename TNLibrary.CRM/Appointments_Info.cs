﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Data.SqlClient;

public class CRM_Appointments_Info
{
   
    #region [Bien cuc bo]
    private int m_AppointmentKey = 0;  
    private string m_AppointmentName = "";
    private string m_AppointmentContent = "";

    private DateTime m_StartDate;
    private DateTime m_DueDate;

    private string m_StatusName = "";

    private int m_Priority = 0;
    private int m_Complete = 0;
    private int m_TypeKey = 0;
    private int m_CustomerKey = 0;

    private string m_CREATE_BY = "";
    private DateTime m_CREATE_ON;

    private string m_MODIFY_BY = "";
    private DateTime m_MODIFY_ON;
    
    #endregion

    #region [Properties]

    public int AppointmentKey
    {
        get { return m_AppointmentKey; }
        set { m_AppointmentKey = value; }
    }
    public string AppointmentName
    {
        get { return m_AppointmentName; }
        set { m_AppointmentName = value; }
    }
    public string AppointmentContent
    {
        get { return m_AppointmentContent; }
        set { m_AppointmentContent = value; }
    }
    public DateTime StartDate
    {
        get { return m_StartDate; }
        set { m_StartDate = value; }
    }
   public DateTime DueDate
   {
        get {return m_DueDate;}
        set {m_DueDate=value;}
   }
    public string StatusName
    {
        get {return m_StatusName;}
        set {m_StatusName=value ;}
    }

    public int Priority
    {
        get {return m_Priority;}
        set {m_Priority=value;}
    }
    public int Complete
    {
        get {return m_Complete;}
        set {m_Complete=value ;}
    }

    public int TypeKey
    {
        get {return m_TypeKey;}
        set {m_TypeKey=value ;}
    }
    public int CustomerKey
    {
        get {return m_CustomerKey;}
        set {m_CustomerKey=value ;}
    }
    public string CREATE_BY
    {
        get { return m_CREATE_BY; }
        set { m_CREATE_BY = value; }
    }
    public DateTime  CREATE_ON
    {
        get { return m_CREATE_ON; }
        set { m_CREATE_ON = value; }
    }
    public string MODIFY_BY
    {
        get { return m_MODIFY_BY; }
        set { m_MODIFY_BY = value; }
    }

    public DateTime MODIFY_ON
    {
        get { return m_MODIFY_ON; }
        set { m_MODIFY_ON = value; }
    }

    #endregion

    #region [Constructor]

    public CRM_Appointments_Info()
    {
        
    }

    public CRM_Appointments_Info(int strAppointmentKey)
    {


        string strSelect = "SELECT * FROM CRM_Appointments WHERE AppointmentKey=@AppointmentKey";

        // create and open oracle connection
        SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
        Connection.Open();

        // create oracle command object
        SqlCommand command = Connection.CreateCommand();

        command.CommandText = strSelect;

        command.Parameters.Add("@AppointmentKey", SqlDbType.Int).Value = strAppointmentKey;

        SqlDataReader nReader = command.ExecuteReader();

        if (nReader.HasRows)
        {
            nReader.Read();

            m_AppointmentKey = int.Parse(nReader["AppointmentKey"].ToString());
            m_AppointmentName = nReader["AppointmentName"].ToString();
            m_AppointmentContent = nReader["AppointmentContent"].ToString();

            if (nReader["StartDate"] != DBNull.Value)
                m_StartDate = (DateTime)nReader["StartDate"];
            if (nReader["DueDate"] != DBNull.Value)
                m_DueDate = (DateTime)nReader["DueDate"];

            m_StatusName = nReader["StatusName"].ToString();

            m_Priority = int.Parse(nReader["Priority"].ToString());
            m_Complete = int.Parse(nReader["Complete"].ToString());
            m_TypeKey = int.Parse(nReader["TypeKey"].ToString());
            m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());

            m_CREATE_BY = nReader["CreatedBy"].ToString();
            m_MODIFY_BY = nReader["ModifiedBy"].ToString();

            if (nReader["CreatedOn"] != DBNull.Value)
                m_CREATE_ON = (DateTime)nReader["CreatedOn"];
            if (nReader["ModifiedOn"] != DBNull.Value)
                m_MODIFY_ON = (DateTime)nReader["ModifiedOn"];

        }
        nReader.Close();
        command.Dispose();


        // Close and Dispose SqlConnection object
        Connection.Close();
        Connection.Dispose();

    }
    #endregion

    #region [Method]

    public string Update()
    {
        string Result = "";
        try
        {

            //---------- String SQL Access Database ---------------
            string nSQL = "dbo.UPDATE_APPOINTMENTS";
            // create and open oracle connection
            SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
            Connection.Open();
            // create oracle command object
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = nSQL;
            command.CommandType = CommandType.StoredProcedure;
            

            command.Parameters.Add("@AppointmentKey", SqlDbType.Int ).Value = m_AppointmentKey;
            command.Parameters.Add("@AppointmentName", SqlDbType.NVarChar).Value = m_AppointmentName;
            command.Parameters.Add("@AppointmentContent", SqlDbType.Int).Value = m_AppointmentContent;

            command.Parameters.Add("@StartDate", SqlDbType.DateTime ).Value = m_StartDate;
            command.Parameters.Add("@DueDate", SqlDbType.DateTime ).Value = m_DueDate;

            command.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = m_StatusName;

            command.Parameters.Add("@Priority", SqlDbType.Int).Value = m_Priority;
            command.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
            command.Parameters.Add("@TypeKey", SqlDbType.Int).Value = m_TypeKey;
            command.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
            
            command.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_MODIFY_BY;
            command.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = m_MODIFY_ON;
            Result = command.ExecuteNonQuery().ToString();
            // Close and Dispose SqlConnection object
            Connection.Close();
            Connection.Dispose();
        }
        catch (Exception err)
        {

            Result = err.Message.ToString();
        }
        return Result;
    }

    public string Create()
    {
        string Result = "";
        try
        {
            string nSQL = "dbo.INSERT_APPOINTMENTS";
            // create and open oracle connection
            SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
            Connection.Open();

            // create oracle command object
            SqlCommand command = Connection.CreateCommand();
            command.CommandText = nSQL;
            command.CommandType = CommandType.StoredProcedure;


            command.Parameters.Add("@AppointmentName", SqlDbType.NVarChar).Value = m_AppointmentName;
            command.Parameters.Add("@AppointmentContent", SqlDbType.NVarChar ).Value = m_AppointmentContent;

            command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = m_StartDate;
            command.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = m_DueDate;

            command.Parameters.Add("@StatusName", SqlDbType.NVarChar).Value = m_StatusName;

            command.Parameters.Add("@Priority", SqlDbType.Int).Value = m_Priority;
            command.Parameters.Add("@Complete", SqlDbType.Int).Value = m_Complete;
            command.Parameters.Add("@TypeKey", SqlDbType.Int).Value = m_TypeKey;
            command.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
          
            command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CREATE_BY;
            command.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = m_CREATE_ON;

            Result = command.ExecuteNonQuery().ToString();

            // Close and Dispose SqlConnection object
            Connection.Close();
            Connection.Dispose();
        }
        catch (Exception err)
        {
            Result = err.Message.ToString();
        }
        return Result;
    }

    public string Delete()
    {
        string Result = "";

        //---------- String SQL Access Database ---------------
        //string nSQL = "DELETE FROM VMS_MASTER WHERE QTN_NUM = @QTN_NUM";
        string nSQL = "dbo.DELETE_APPOINTMENTS";

        // create and open oracle connection
        SqlConnection Connection = new SqlConnection(ConnectWedInfo.ConnectionString);
        Connection.Open();

        // create oracle command object
        SqlCommand command = Connection.CreateCommand();
        command.CommandText = nSQL;
        command.CommandType = CommandType.StoredProcedure;
        

        command.Parameters.Add("@AppointmentKey", SqlDbType.VarChar).Value = m_AppointmentKey;
        Result = command.ExecuteNonQuery().ToString();

        // Close and Dispose SqlConnection object
        Connection.Close();
        Connection.Dispose();
        return Result;
    }

   
    #endregion

}

