﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TNConfig;


public class Tasks_Data
{
    //==================
    public static DataTable Status()
    {
        string Result = "";
        DataTable nTable = new DataTable();
        string nSQL = " SELECT * FROM CRM_TaskStatus " ;
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }

    //==================
    public static DataTable Task_List()
    {
        string Result = "";
        DataTable nTable = new DataTable();
        string nSQL = " SELECT A.*,B.CategoryName CategoryName, C.ProjectName ProjectName,E.StatusName, D.CustomerName CustomerName,H.UserName  "
                     + " FROM CRM_TASKS A "
                     + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey "
                     + " LEFT JOIN CRM_Projects C ON C.ProjectKey = A.ProjectKey "
                     + " LEFT JOIN CRM_Customers D ON A.CustomerKey = D.CustomerKey "
                     + " LEFT JOIN SYS_Users H ON A.CreatedBy = H.UserKey "
                     + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey ORDER BY A.StartDate ASC";
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }
    public static DataTable Task_List(string TaskName, string CreatedBy)
    {
        string Result = "";

        DataTable nTable = new DataTable();
        string nSQL = " SELECT A.*,B.CategoryName CategoryName, C.ProjectName ProjectName,E.StatusName, D.CustomerName CustomerName,H.UserName  "
                     + " FROM CRM_TASKS A "
                     + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey "
                     + " LEFT JOIN CRM_Projects C ON C.ProjectKey = A.ProjectKey "
                     + " LEFT JOIN CRM_Customers D ON A.CustomerKey = D.CustomerKey "
                     + " LEFT JOIN SYS_Users H ON A.CreatedBy = H.UserKey "
                     + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                     + " WHERE A.TaskName LIKE @TaskName AND A.CreatedBy = @CreatedBy "
                     + " ORDER BY A.StartDate ASC";

        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand nCommand = new SqlCommand(nSQL, connection);
            nCommand.CommandType = CommandType.Text;
            nCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = "%" + TaskName + "%";
            nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;

            SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
            mAdapter.Fill(nTable);
            nCommand.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }
    public static DataTable Task_List(string TaskName,string CreatedBy,DateTime FromDate, DateTime ToDate)
    {
        string Result = "";
        FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
        ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

        DataTable nTable = new DataTable();
        string nSQL = " SELECT A.*,B.CategoryName CategoryName, C.ProjectName ProjectName,E.StatusName, D.CustomerName CustomerName,H.UserName  "
                     + " FROM CRM_TASKS A "
                     + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey "
                     + " LEFT JOIN CRM_Projects C ON C.ProjectKey = A.ProjectKey "
                     + " LEFT JOIN CRM_Customers D ON A.CustomerKey = D.CustomerKey "
                     + " LEFT JOIN SYS_Users H ON A.CreatedBy = H.UserKey "
                     + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                     + " WHERE A.TaskName LIKE @TaskName AND A.CreatedBy = @CreatedBy "
                     + " AND (A.StartDate >= FromDate AND A.StartDate <= @ToDate) " 
                     + " ORDER BY A.StartDate ASC";
                    
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand nCommand = new SqlCommand(nSQL, connection);
            nCommand.CommandType = CommandType.Text;
            nCommand.Parameters.Add("@TaskName", SqlDbType.NVarChar).Value = "%" + TaskName +"%";
            nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
            nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
            nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;

            SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
            mAdapter.Fill(nTable);
            nCommand.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }
    public static int Count(int StatusKey, string CreatedBy)
    {
        int nResult = 0;

        //---------- String SQL Access Database ---------------
        string nSQL = "SELECT Count(*) FROM CRM_Tasks ";
            nSQL += " WHERE CreatedBy = @CreatedBy";
        if(StatusKey != 0)
            nSQL += " AND StatusKey = @StatusKey";
        
        string nConnectionString = ConnectDataBase.ConnectionString;
        SqlConnection nConnect = new SqlConnection(nConnectionString);
        nConnect.Open();
        try
        {
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = StatusKey;
            nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;

            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();

        }
        catch (Exception Err)
        {
            string m_Message = Err.ToString();
        }
        finally
        {
            nConnect.Close();
        }
        return nResult;
    }

    public static DataTable Task_List(string UserKey)
    {
        string Result = "";
        DataTable nTable = new DataTable();
        string nSQL = " SELECT A.*,B.CategoryName CategoryName, C.ProjectName ProjectName,E.StatusName, D.CustomerName CustomerName "
                     + " FROM CRM_TASKS A "
                     + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey "
                     + " LEFT JOIN CRM_Projects C ON C.ProjectKey = A.ProjectKey "
                     + " LEFT JOIN CRM_Customers D ON D.CustomerKey = A.CustomerKey "
                     + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                     + " WHERE A.CreatedBy = @CreatedBy ORDER BY A.StartDate ASC";
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;
            command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }
 
    public static DataTable Task_Today(string UserKey)
    {
        string Result = "";
        DataTable nTable = new DataTable();
        string nSQL = " SELECT * FROM CRM_TASKS "
                    + " WHERE StartDate <= GETDATE() AND StatusKey <> 4 AND CreatedBy = @CreatedBy "
                    + " ORDER BY StartDate ASC";
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;
            command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }
    public static DataTable ReportTask_List(int Customer, int Project, string UserKey)
    {
        string Result = "";
        string nSQL = "";
        DataTable nTable = new DataTable();
        if (Customer != 0 && Project != 0 && UserKey != "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                        + " FROM CRM_TASKS A"
                        + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                        + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                        + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                        + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                        + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
                        + " WHERE A.CreatedBy = @CreatedBy "
                        + " AND A.ProjectKey = @ProjectKey "
                        + " AND A.CustomerKey = @CustomerKey ";
        if (Customer == 0 && Project == 0 && UserKey == "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                   + " FROM CRM_TASKS A"
                   + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                   + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                   + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                   + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                   + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  ";
        if (Customer != 0 && Project == 0 && UserKey == "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
              + " FROM CRM_TASKS A"
              + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
              + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
              + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
              + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
              + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
              + " WHERE A.CustomerKey = @CustomerKey ";
        if (Customer == 0 && Project != 0 && UserKey == "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                   + " FROM CRM_TASKS A"
                   + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                   + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                   + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                   + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                   + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
                   + " WHERE  "
                   + "  A.ProjectKey = @ProjectKey ";
        if (Customer == 0 && Project == 0 && UserKey != "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                  + " FROM CRM_TASKS A"
                  + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                  + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                  + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                  + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                  + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
                  + " WHERE A.CreatedBy = @CreatedBy ";
        if (Customer != 0 && Project != 0 && UserKey == "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
             + " FROM CRM_TASKS A"
             + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
             + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
             + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
             + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
             + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
             + " WHERE  "
             + " A.ProjectKey = @ProjectKey "
             + " AND A.CustomerKey = @CustomerKey ";
        if (Customer != 0 && Project == 0 && UserKey != "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                 + " FROM CRM_TASKS A"
                 + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                 + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                 + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                 + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                 + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
                 + " WHERE A.CreatedBy = @CreatedBy "
                 + " AND A.CustomerKey = @CustomerKey ";
        if (Customer == 0 && Project != 0 && UserKey != "00000000-0000-0000-0000-000000000000")
            nSQL = " SELECT A.*,B.CategoryName CategoryName,C.CustomerName CustomerName,D.ProjectName ProjectName,E.StatusName, F.UserName Created "
                   + " FROM CRM_TASKS A"
                   + " LEFT JOIN CRM_TaskCategories B ON A.CategoryKey = B.CategoryKey"
                   + " LEFT JOIN CRM_Customers C ON C.CustomerKey = A.CustomerKey "
                   + " LEFT JOIN CRM_Projects D ON D.ProjectKey = A.ProjectKey "
                   + " LEFT JOIN CRM_TaskStatus E ON E.StatusKey = A.StatusKey "
                   + " INNER JOIN SYS_Users F ON A.CreatedBy = F.UserKey  "
                   + " WHERE A.CreatedBy = @CreatedBy "
                   + " AND A.ProjectKey = @ProjectKey ";

        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;
            command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;
            command.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = Project;
            command.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = Customer;

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }



}

