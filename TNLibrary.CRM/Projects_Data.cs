﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TNConfig;


public class Projects_Data
{
    public static DataTable Project_List()
    {
        string Result = "";
        DataTable nTable = new DataTable();
        string nSQL = "SELECT A.*, B.CustomerName, B.CustomerID, C.StatusName FROM CRM_Projects A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN CRM_TaskStatus C ON A.StatusKey = C.StatusKey";
        try
        {
            SqlConnection connection = new SqlConnection(ConnectDataBase.ConnectionString);
            SqlCommand command = new SqlCommand(nSQL, connection);
            command.CommandType = CommandType.Text;
      

            SqlDataAdapter mAdapter = new SqlDataAdapter(command);
            mAdapter.Fill(nTable);
            command.Dispose();
        }
        catch (Exception err)
        {
            Result = err.ToString();
        }
        return nTable;
    }

    public static int Count(int StatusKey)
    {
        int nResult = 0;

        //---------- String SQL Access Database ---------------
        string nSQL = "SELECT Count(*) FROM CRM_Projects ";
        if (StatusKey != 0)
            nSQL += " WHERE StatusKey = @StatusKey";

        string nConnectionString = ConnectDataBase.ConnectionString;
        SqlConnection nConnect = new SqlConnection(nConnectionString);
        nConnect.Open();
        try
        {
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.Parameters.Add("@StatusKey", SqlDbType.Int).Value = StatusKey;
            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();

        }
        catch (Exception Err)
        {
            string m_Message = Err.ToString();
        }
        finally
        {
            nConnect.Close();
        }
        return nResult;
    }
    public static int CheckExistProject(int ProjectKey)
    {
        int nResult = 0;
        string nSQL = "SELECT COUNT(*) FROM CRM_Tasks WHERE ProjectKey = @ProjectKey";
        string nConnectionString = ConnectDataBase.ConnectionString;
        SqlConnection nConnect = new SqlConnection(nConnectionString);
        nConnect.Open();
        SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
        nCommand.CommandType = CommandType.Text;
        nCommand.Parameters.Add("@ProjectKey", SqlDbType.Int).Value = ProjectKey;

        string strAmount = nCommand.ExecuteScalar().ToString();
        int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
        nCommand.Dispose();

        return nResult;
    }


}

