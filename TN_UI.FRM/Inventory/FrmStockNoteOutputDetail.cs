using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.IVT;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmStockNoteOutputDetail : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;

        public Stock_Note_Output_Object mNoteOutput;
        public int mNoteKey;
        public bool FlagTemp;

        public FrmStockNoteOutputDetail()
        {
            InitializeComponent();
            dataGridViewProducts1.ImageHeader = imageList1.Images[0];
            dataGridViewProducts1.SetupLayoutGridView();
            dataGridViewProducts1.KeyDown += new System.Windows.Forms.KeyEventHandler(dataGridViewProducts1_KeyDown);
            dataGridViewProducts1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridViewProducts1_RowValidated);

        }
        private void FrmStockNoteOutputDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coWarehouse, "SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse", false);
            LoadDataToToolbox.AutoCompleteTextBox(txtCustomerID, "SELECT CustomerID FROM CRM_Customers WHERE IsCustomer = 1");
            txtDocumentDate.CustomFormat = mFormatDate;
            mNoteOutput = new Stock_Note_Output_Object(mNoteKey);
            LoadStockNoteOutputInfo();

            if (FlagTemp)
            {
                DisplayForNoteTemp();
            }
            coWarehouse.SelectedIndex = 0;
            PickerExportNoteDate.CustomFormat = mFormatDate;
            txtNoteID.Focus();
        }

        #region [ Layout]

        #endregion

        #region [ Note Output Infomation ]
        private bool mNoteOutputIDChanged = false;
        private void txtNoteID_TextChanged(object sender, EventArgs e)
        {
            mNoteOutputIDChanged = true;
        }
        private void txtNoteID_Leave(object sender, EventArgs e)
        {
            if (mNoteOutputIDChanged)
            {
                Stock_Note_Output_Object nStockNote = new Stock_Note_Output_Object(txtNoteID.Text);
                if (nStockNote.NoteKey > 0)
                {
                    if (MessageBox.Show("Đã có số phiếu nhập này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        mNoteOutput = nStockNote;
                        LoadStockNoteOutputInfo();
                    }
                    else
                    {
                        txtNoteID.Focus();
                    }
                }

            }

        }
        private void LoadStockNoteOutputInfo()
        {
            PickerExportNoteDate.Value = mNoteOutput.NoteDate;
            txtNoteID.Text = mNoteOutput.NoteID.Trim();
            txtNoteName.Text = mNoteOutput.NoteDescription;
            txtDeliverer.Text = mNoteOutput.Deliverer;
            coWarehouse.SelectedValue = mNoteOutput.WarehouseKey;
            mCustomer = new Customer_Info(mNoteOutput.CustomerKey);
            txtCustomerID.Text = mCustomer.ID;
            CoCustomers.Text = mCustomer.Name;
            chkIsInternal.Checked = mNoteOutput.IsInternal;

            txtDocumentAttached.Text = mNoteOutput.DocumentAttached;
            txtDocumentOrigin.Text = mNoteOutput.DocumentOrigin;
            txtDocumentDate.Value = mNoteOutput.DocumentDate;

            dataGridViewProducts1.ListProduct = mNoteOutput.ProductsOutput;
            txtSumOrder.Text = mNoteOutput.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            if (mNoteOutput.AmountCurrencyMain == 0)
            {
                double nSumAmountCurrencyMain = 0;
                foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
                {
                    if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                    {
                        nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                    }
                }
                txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            }
            mNoteOutputIDChanged = false;
        }

        #endregion

        #region [ Note Output Detail Information ]

        private void dataGridViewProducts1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            double nSumAmountCurrencyMain = 0;
            foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
            {
                if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                {
                    nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void dataGridViewProducts1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter & e.Control)
            {
                if (dataGridViewProducts1.CurrentCell.ColumnIndex == 1)
                {
                    FRM.List.FrmProductList Frm = new FRM.List.FrmProductList();
                    Frm.FlagView = true;
                    Frm.ShowDialog();
                    dataGridViewProducts1.CurrentCell.Value = Frm.mProductID;
                    dataGridViewProducts1.BeginEdit(true);
                }
            }
        }

        #endregion

        #region [ Customer Infomation]
        private Customer_Info mCustomer;
        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            if (txtCustomerID.Text.Trim() == "*")
            {
                mCustomer = new Customer_Info();
                LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1 ", true);
            }
            else
            {
                mCustomer = new Customer_Info(txtCustomerID.Text.Trim());
                LoadInfoCustomer();
                if (mCustomer.Key == 0 && txtCustomerID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1 AND CustomerID LIKE '" + txtCustomerID.Text.Trim() + "%'", true);

            }
        }
        private void txtCustomerID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                //FrmCustomerList frm = new FrmCustomerList();
                //frm.FlagView = true;
                //frm.ShowDialog();
                //if (frm.ListItemSelect != null)
                //    txtCustomerID.Text = frm.ListItemSelect.Text;
                //frm.Close();
            }
        }
        private void CoCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nCustomerKey;
            if (int.TryParse(CoCustomers.SelectedValue.ToString(), out nCustomerKey))
            {
                mCustomer = new Customer_Info(nCustomerKey);
                LoadInfoCustomer();
            }

        }
        private void LoadInfoCustomer()
        {
            if (mCustomer.Key == 0)
            {
                CoCustomers.Text = "...";
                //  txtAddress.Text = "...";
                //  txtDeliverer.Text = "..";
            }
            else
            {
                txtCustomerID.Text = mCustomer.ID.Trim();
                CoCustomers.Text = mCustomer.Name;
                //  txtAddress.Text =Customer.Address + ", " +Customer.City + ", " + Customer.Country ;
                //  txtDeliverer.Text = "..";
            }
        }

        #endregion

        #region [ Function]
        private bool CheckBeforeSave()
        {
            txtNoteID.Focus();

            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerExportNoteDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerExportNoteDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

            if (txtNoteID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtNoteID.Focus();
                return false;
            }
            if (txtCustomerID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã nhà cung cấp");
                txtCustomerID.Focus();
                return false;
            }
            if (CoCustomers.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên nhà cung cấp");
                CoCustomers.Focus();
                return false;
            }
            if (dataGridViewProducts1.IsError)
            {
                MessageBox.Show("Vui lòng kiểm tra lại danh sách vật tư mua hàng");
                dataGridViewProducts1.Focus();
                return false;
            }
            return true;
        }

        private void ClearNoteOutputInfo()
        {
            txtNoteID.Text = "";
            txtNoteName.Text = "";
            txtDeliverer.Text = "";
            txtCustomerID.Text = "";
            CoCustomers.Text = "";
            txtCustomerID.Text = "";
            CoCustomers.Text = "";
            txtDocumentAttached.Text = "";
            txtDocumentOrigin.Text = "";

            dataGridViewProducts1.ClearProducts();
            txtSumOrder.Text = "0";
        }
        private void DisplayForNoteTemp()
        {
            coWarehouse.Enabled = false;
            cmdSaveNew.Enabled = false;
        }
        #endregion

        #region [ Action Button]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (SaveNoteOutputInfomation())
            {
                if (!FlagTemp)
                {
                    mNoteOutput.SaveObject();
                    this.Close();
                }

                else
                    this.Close();
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNoteOutputInfomation())
            {
                mNoteOutput.SaveObject();

                string strID = mNoteOutput.NoteID.Trim();

                mCustomer = new Customer_Info();
                mNoteOutput = new Stock_Note_Output_Object();

                ClearNoteOutputInfo();
                txtNoteID.Focus();
                txtNoteID.Text = Stock_Note_Data.GetAutoNoteOutputID(strID);
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa phiếu nhập này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                mNoteOutput.DeleteObject();
                ClearNoteOutputInfo();
                txtNoteID.Focus();
            }

        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmStockNoteOutputReport frm = new FrmStockNoteOutputReport();
            SaveNoteOutputInfomation();
            mNoteOutput.WarehouseName = coWarehouse.Text;
            frm.mNote_Output = mNoteOutput;
            frm.ShowDialog();
        }
        private void txtSumOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void FrmStockNoteOutputDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Update Note Output ]
        private bool SaveNoteOutputInfomation()
        {
            if (!CheckBeforeSave()) return false;
            if (!FlagTemp)
            {
                if (mCustomer == null || mCustomer.Key == 0)
                {
                    if (MessageBox.Show("Chưa có nhà cung cấp này, ? Bạn có muốn thêm nhà cung cấp này ? ", "Thêm ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        mCustomer = new Customer_Info();
                        mCustomer.ID = txtCustomerID.Text;
                        mCustomer.Name = CoCustomers.Text;
                        //  Customer.Address = txt.Text;
                        mCustomer.CategoryKey = 2;
                        mCustomer.IsCustomer = true;
                        mCustomer.Create();
                    }
                    else
                        return false;
                }
            }

            int nWarehouseKey = (int)coWarehouse.SelectedValue;

            mNoteOutput.NoteID = txtNoteID.Text;
            mNoteOutput.NoteDate = PickerExportNoteDate.Value;
            mNoteOutput.NoteDescription = txtNoteName.Text;
            mNoteOutput.WarehouseKey = nWarehouseKey;
            mNoteOutput.IsInternal = chkIsInternal.Checked;

            mNoteOutput.DocumentAttached = txtDocumentAttached.Text;
            mNoteOutput.DocumentOrigin = txtDocumentOrigin.Text;
            mNoteOutput.DocumentDate = txtDocumentDate.Value;

            mNoteOutput.CustomerKey = mCustomer.Key;
            mNoteOutput.Deliverer = txtDeliverer.Text;
            mNoteOutput.AmountCurrencyMain = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            mNoteOutput.CreatedBy = SessionUser.UserLogin.Key;
            mNoteOutput.ModifiedBy = SessionUser.UserLogin.Key;

            mNoteOutput.ProductsOutput = dataGridViewProducts1.ListProduct;

            return true;
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("IVT003");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

    }
}