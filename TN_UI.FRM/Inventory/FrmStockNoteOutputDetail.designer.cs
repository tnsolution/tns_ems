namespace TN_UI.FRM.Inventory
{
    partial class FrmStockNoteOutputDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStockNoteOutputDetail));
            this.cmdSaveNew = new System.Windows.Forms.ToolStripButton();
            this.cmdPrinter = new System.Windows.Forms.ToolStripButton();
            this.cmdSaveClose = new System.Windows.Forms.ToolStripButton();
            this.cmdDel = new System.Windows.Forms.ToolStripButton();
            this.txtTitle = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.label1 = new System.Windows.Forms.Label();
            this.PickerExportNoteDate = new System.Windows.Forms.DateTimePicker();
            this.txtNoteID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNoteName = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDeliverer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.coWarehouse = new System.Windows.Forms.ComboBox();
            this.CoCustomers = new System.Windows.Forms.ComboBox();
            this.txtSumOrder = new System.Windows.Forms.TextBox();
            this.lbSumOrder = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDocumentAttached = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDocumentOrigin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.GroupAccount = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtImportNoteDebitNo = new System.Windows.Forms.TextBox();
            this.txtImportNoteCreditNo = new System.Windows.Forms.TextBox();
            this.chkIsInternal = new System.Windows.Forms.CheckBox();
            this.dataGridViewProducts1 = new TNLibrary.IVT.Forms.DataGridViewProducts();
            this.txtDocumentDate = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1.SuspendLayout();
            this.GroupAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdSaveNew
            // 
            this.cmdSaveNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveNew.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveNew.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveNew.Image")));
            this.cmdSaveNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveNew.Name = "cmdSaveNew";
            this.cmdSaveNew.Size = new System.Drawing.Size(94, 22);
            this.cmdSaveNew.Text = "Save && New";
            this.cmdSaveNew.Click += new System.EventHandler(this.cmdSaveNew_Click);
            // 
            // cmdPrinter
            // 
            this.cmdPrinter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrinter.ForeColor = System.Drawing.Color.Navy;
            this.cmdPrinter.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrinter.Image")));
            this.cmdPrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdPrinter.Name = "cmdPrinter";
            this.cmdPrinter.Size = new System.Drawing.Size(63, 22);
            this.cmdPrinter.Text = "Printer";
            this.cmdPrinter.Click += new System.EventHandler(this.cmdPrinter_Click);
            // 
            // cmdSaveClose
            // 
            this.cmdSaveClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveClose.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveClose.Image")));
            this.cmdSaveClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdSaveClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveClose.Name = "cmdSaveClose";
            this.cmdSaveClose.Size = new System.Drawing.Size(95, 22);
            this.cmdSaveClose.Text = "Save && Close";
            this.cmdSaveClose.Click += new System.EventHandler(this.cmdSaveClose_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDel.ForeColor = System.Drawing.Color.Navy;
            this.cmdDel.Image = ((System.Drawing.Image)(resources.GetObject("cmdDel.Image")));
            this.cmdDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.Size = new System.Drawing.Size(63, 22);
            this.cmdDel.Text = "Delete";
            this.cmdDel.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(324, 33);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(184, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "PHIẾU XUẤT KHO";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(143)))), ((int)(((byte)(191)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1001, 5);
            this.panel2.TabIndex = 74;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSaveClose,
            this.cmdSaveNew,
            this.cmdPrinter,
            this.cmdDel,
            this.toolStripSeparator1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1001, 25);
            this.toolStrip1.TabIndex = 72;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(305, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 50;
            this.label1.Text = "Ngày xuất";
            // 
            // PickerExportNoteDate
            // 
            this.PickerExportNoteDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PickerExportNoteDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerExportNoteDate.Location = new System.Drawing.Point(385, 60);
            this.PickerExportNoteDate.Name = "PickerExportNoteDate";
            this.PickerExportNoteDate.Size = new System.Drawing.Size(130, 21);
            this.PickerExportNoteDate.TabIndex = 0;
            // 
            // txtNoteID
            // 
            this.txtNoteID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtNoteID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoteID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtNoteID.ForeColor = System.Drawing.Color.Navy;
            this.txtNoteID.Location = new System.Drawing.Point(385, 84);
            this.txtNoteID.Name = "txtNoteID";
            this.txtNoteID.Size = new System.Drawing.Size(130, 21);
            this.txtNoteID.TabIndex = 1;
            this.txtNoteID.TextChanged += new System.EventHandler(this.txtNoteID_TextChanged);
            this.txtNoteID.Leave += new System.EventHandler(this.txtNoteID_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(287, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Số phiếu xuất";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(12, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 61;
            this.label4.Text = "Lý do xuất";
            // 
            // txtNoteName
            // 
            this.txtNoteName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtNoteName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtNoteName.ForeColor = System.Drawing.Color.Navy;
            this.txtNoteName.Location = new System.Drawing.Point(126, 173);
            this.txtNoteName.Name = "txtNoteName";
            this.txtNoteName.Size = new System.Drawing.Size(863, 21);
            this.txtNoteName.TabIndex = 5;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCustomerID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtCustomerID.ForeColor = System.Drawing.Color.Navy;
            this.txtCustomerID.Location = new System.Drawing.Point(126, 117);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(130, 21);
            this.txtCustomerID.TabIndex = 2;
            this.txtCustomerID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerID_KeyDown);
            this.txtCustomerID.Leave += new System.EventHandler(this.txtCustomerID_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(15, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 15);
            this.label2.TabIndex = 80;
            this.label2.Text = "Mã khách hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(269, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 15);
            this.label5.TabIndex = 82;
            this.label5.Text = "Tên khách hàng";
            // 
            // txtDeliverer
            // 
            this.txtDeliverer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtDeliverer.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtDeliverer.ForeColor = System.Drawing.Color.Navy;
            this.txtDeliverer.Location = new System.Drawing.Point(126, 144);
            this.txtDeliverer.Name = "txtDeliverer";
            this.txtDeliverer.Size = new System.Drawing.Size(863, 21);
            this.txtDeliverer.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(12, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 15);
            this.label6.TabIndex = 84;
            this.label6.Text = "Tên Người giao";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(15, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 15);
            this.label7.TabIndex = 9;
            this.label7.Text = "Xuất tại kho";
            // 
            // coWarehouse
            // 
            this.coWarehouse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.coWarehouse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coWarehouse.ForeColor = System.Drawing.Color.Navy;
            this.coWarehouse.FormattingEnabled = true;
            this.coWarehouse.Location = new System.Drawing.Point(126, 229);
            this.coWarehouse.Name = "coWarehouse";
            this.coWarehouse.Size = new System.Drawing.Size(260, 23);
            this.coWarehouse.TabIndex = 10;
            // 
            // CoCustomers
            // 
            this.CoCustomers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.CoCustomers.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.CoCustomers.ForeColor = System.Drawing.Color.Navy;
            this.CoCustomers.FormattingEnabled = true;
            this.CoCustomers.Location = new System.Drawing.Point(385, 114);
            this.CoCustomers.Name = "CoCustomers";
            this.CoCustomers.Size = new System.Drawing.Size(604, 23);
            this.CoCustomers.TabIndex = 3;
            this.CoCustomers.SelectedIndexChanged += new System.EventHandler(this.CoCustomers_SelectedIndexChanged);
            // 
            // txtSumOrder
            // 
            this.txtSumOrder.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSumOrder.ForeColor = System.Drawing.Color.Maroon;
            this.txtSumOrder.Location = new System.Drawing.Point(801, 543);
            this.txtSumOrder.Name = "txtSumOrder";
            this.txtSumOrder.Size = new System.Drawing.Size(188, 20);
            this.txtSumOrder.TabIndex = 0;
            this.txtSumOrder.Text = "0";
            this.txtSumOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSumOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSumOrder_KeyPress);
            // 
            // lbSumOrder
            // 
            this.lbSumOrder.AutoSize = true;
            this.lbSumOrder.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSumOrder.ForeColor = System.Drawing.Color.Navy;
            this.lbSumOrder.Location = new System.Drawing.Point(720, 547);
            this.lbSumOrder.Name = "lbSumOrder";
            this.lbSumOrder.Size = new System.Drawing.Size(75, 15);
            this.lbSumOrder.TabIndex = 91;
            this.lbSumOrder.Text = "Tổng số tiền";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(12, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 15);
            this.label10.TabIndex = 92;
            this.label10.Text = "Theo Chứng từ";
            // 
            // txtDocumentAttached
            // 
            this.txtDocumentAttached.BackColor = System.Drawing.Color.White;
            this.txtDocumentAttached.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtDocumentAttached.ForeColor = System.Drawing.Color.Navy;
            this.txtDocumentAttached.Location = new System.Drawing.Point(126, 200);
            this.txtDocumentAttached.Name = "txtDocumentAttached";
            this.txtDocumentAttached.Size = new System.Drawing.Size(130, 21);
            this.txtDocumentAttached.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(267, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 15);
            this.label12.TabIndex = 96;
            this.label12.Text = "Ngày";
            // 
            // txtDocumentOrigin
            // 
            this.txtDocumentOrigin.BackColor = System.Drawing.Color.White;
            this.txtDocumentOrigin.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtDocumentOrigin.ForeColor = System.Drawing.Color.Navy;
            this.txtDocumentOrigin.Location = new System.Drawing.Point(516, 230);
            this.txtDocumentOrigin.Name = "txtDocumentOrigin";
            this.txtDocumentOrigin.Size = new System.Drawing.Size(473, 21);
            this.txtDocumentOrigin.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(405, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 97;
            this.label8.Text = "Chứng từ gốc";
            // 
            // GroupAccount
            // 
            this.GroupAccount.Controls.Add(this.label14);
            this.GroupAccount.Controls.Add(this.label13);
            this.GroupAccount.Controls.Add(this.txtImportNoteDebitNo);
            this.GroupAccount.Controls.Add(this.txtImportNoteCreditNo);
            this.GroupAccount.Location = new System.Drawing.Point(821, 33);
            this.GroupAccount.Name = "GroupAccount";
            this.GroupAccount.Size = new System.Drawing.Size(168, 75);
            this.GroupAccount.TabIndex = 99;
            this.GroupAccount.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(6, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 15);
            this.label14.TabIndex = 101;
            this.label14.Text = "Có";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(6, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 15);
            this.label13.TabIndex = 100;
            this.label13.Text = "Nợ";
            // 
            // txtImportNoteDebitNo
            // 
            this.txtImportNoteDebitNo.Enabled = false;
            this.txtImportNoteDebitNo.ForeColor = System.Drawing.Color.Navy;
            this.txtImportNoteDebitNo.Location = new System.Drawing.Point(36, 19);
            this.txtImportNoteDebitNo.Name = "txtImportNoteDebitNo";
            this.txtImportNoteDebitNo.Size = new System.Drawing.Size(126, 20);
            this.txtImportNoteDebitNo.TabIndex = 50;
            // 
            // txtImportNoteCreditNo
            // 
            this.txtImportNoteCreditNo.Enabled = false;
            this.txtImportNoteCreditNo.ForeColor = System.Drawing.Color.Navy;
            this.txtImportNoteCreditNo.Location = new System.Drawing.Point(36, 45);
            this.txtImportNoteCreditNo.Name = "txtImportNoteCreditNo";
            this.txtImportNoteCreditNo.Size = new System.Drawing.Size(126, 20);
            this.txtImportNoteCreditNo.TabIndex = 51;
            // 
            // chkIsInternal
            // 
            this.chkIsInternal.AutoSize = true;
            this.chkIsInternal.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkIsInternal.ForeColor = System.Drawing.Color.Navy;
            this.chkIsInternal.Location = new System.Drawing.Point(748, 202);
            this.chkIsInternal.Name = "chkIsInternal";
            this.chkIsInternal.Size = new System.Drawing.Size(100, 18);
            this.chkIsInternal.TabIndex = 8;
            this.chkIsInternal.Text = "Xuất kho nội bộ";
            this.chkIsInternal.UseVisualStyleBackColor = true;
            // 
            // dataGridViewProducts1
            // 
            this.dataGridViewProducts1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProducts1.ListProduct = ((System.Collections.ArrayList)(resources.GetObject("dataGridViewProducts1.ListProduct")));
            this.dataGridViewProducts1.Location = new System.Drawing.Point(18, 258);
            this.dataGridViewProducts1.Name = "dataGridViewProducts1";
            this.dataGridViewProducts1.Size = new System.Drawing.Size(971, 280);
            this.dataGridViewProducts1.TabIndex = 101;
            // 
            // txtDocumentDate
            // 
            this.txtDocumentDate.CustomFormat = "dd/MM/yyyy";
            this.txtDocumentDate.Location = new System.Drawing.Point(309, 201);
            this.txtDocumentDate.Name = "txtDocumentDate";
            this.txtDocumentDate.Size = new System.Drawing.Size(102, 20);
            this.txtDocumentDate.TabIndex = 102;
            this.txtDocumentDate.Value = new System.DateTime(((long)(0)));
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bkHeader.png");
            // 
            // FrmStockNoteOutputDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1001, 575);
            this.Controls.Add(this.txtDocumentDate);
            this.Controls.Add(this.dataGridViewProducts1);
            this.Controls.Add(this.chkIsInternal);
            this.Controls.Add(this.GroupAccount);
            this.Controls.Add(this.txtDocumentOrigin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.CoCustomers);
            this.Controls.Add(this.txtDocumentAttached);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.coWarehouse);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCustomerID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoteName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PickerExportNoteDate);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDeliverer);
            this.Controls.Add(this.lbSumOrder);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNoteID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSumOrder);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmStockNoteOutputDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu xuất kho";
            this.Load += new System.EventHandler(this.FrmStockNoteOutputDetail_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmStockNoteOutputDetail_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.GroupAccount.ResumeLayout(false);
            this.GroupAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton cmdSaveNew;
        private System.Windows.Forms.ToolStripButton cmdPrinter;
        private System.Windows.Forms.ToolStripButton cmdSaveClose;
        private System.Windows.Forms.ToolStripButton cmdDel;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker PickerExportNoteDate;
        private System.Windows.Forms.TextBox txtNoteID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNoteName;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDeliverer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox coWarehouse;
        private System.Windows.Forms.TextBox txtSumOrder;
        private System.Windows.Forms.Label lbSumOrder;
        private System.Windows.Forms.ComboBox CoCustomers;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDocumentAttached;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDocumentOrigin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox GroupAccount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtImportNoteDebitNo;
        private System.Windows.Forms.TextBox txtImportNoteCreditNo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox chkIsInternal;
        private TNLibrary.IVT.Forms.DataGridViewProducts dataGridViewProducts1;
        private TNLibrary.SYS.Forms.TNDateTimePicker txtDocumentDate;
        private System.Windows.Forms.ImageList imageList1;
    }
}