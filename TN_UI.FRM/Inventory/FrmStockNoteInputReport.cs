using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary.SYS;
using TNLibrary.IVT;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmStockNoteInputReport : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        public Stock_Note_Input_Object mNote_Input;

        public FrmStockNoteInputReport()
        {
            InitializeComponent();
            
        }

        private void FrmStockNoteInputReport_Load(object sender, EventArgs e)
        {

            SetupCrystalReport();
            PopulateCrystalReport();
        }
    
        
        #region [ CrystalReport]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {

            ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\PhieuNhapKho.rpt");

            DataTable nTableTitle = TableReport.TablePhieuNhapKhoTieuDe();
            DataTable nTableDetail = TableReport.TablePhieuNhapKhoDetail();
            DataTable nTableTencongty = TableReport.CompanyInfo();


            DataRow nRow = nTableTitle.NewRow();
            nRow["BoPhan"] = " ";
            nRow["NgayNhap"] = mNote_Input.NoteDate.ToString(mFormatDate);
            nRow["No"] = "";
            nRow["Co"] = "";
            nRow["So"] = mNote_Input.NoteID;
            nRow["TenNguoiGiao"] = mNote_Input.Deliverer;
            nRow["LyDoNhap"] = mNote_Input.NoteDescription;
            nRow["NhapTaiKho"] = mNote_Input.WarehouseName;
            nRow["DiaDiem"] = "";
            nRow["TongThanhTien"] = mNote_Input.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency) ;
            nRow["TongTienBangChu"] = "  ";
            nRow["ChungTuGoc"] = mNote_Input.DocumentAttached;
            nRow["NgayVietPhieuNhap"] = mNote_Input.NoteDate;

            nTableTitle.Rows.Add(nRow);

            int No = 1;
            foreach (InventoryProduct nProduct in mNote_Input.ProductsInput)
            {
                nRow = nTableDetail.NewRow();
                nRow["STT"] = No.ToString();
                nRow["TenSP"] = nProduct.Name;
                nRow["Ma"] = nProduct.ID;
                nRow["DonViTinh"] = nProduct.Unit;
                nRow["SoLuongChungTu"] = nProduct.Quantity.ToString(mFormatDecimal,mFormatProviderDecimal);
                nRow["SoLuongThucNhap"] = nProduct.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRow["DonGia"] = nProduct.UnitPrice.ToString(mFormatCurrencyMainTwoPoint,mFormatProviderCurrency);
                nRow["ThanhTien"] = nProduct.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

                nTableDetail.Rows.Add(nRow);
                No++;
            }


            // thay doi ten table

            rptTam.Database.Tables["TablePhieuNhapKhoTieuDe"].SetDataSource(nTableTitle);
            rptTam.Database.Tables["TablePhieuNhapKhoDetail"].SetDataSource(nTableDetail);
            rptTam.Database.Tables["TableTencongty"].SetDataSource(nTableTencongty);

            crystalReportViewer1.ReportSource = rptTam;

            // thay doi do zoom
            crystalReportViewer1.Zoom(1);

        }
        #endregion

       

    }
}