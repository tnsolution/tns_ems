using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.SYS.Forms;
using TNLibrary.IVT;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmStockNoteInputList : Form
    {

        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyOther = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDate = GlobalSystemConfig.FormatDate;
        public bool FlagView = false;

        public FrmStockNoteInputList()
        {
            InitializeComponent();

        }

        private void FrmStockNoteInputList_Load(object sender, EventArgs e)
        {
            CheckRole();
            InitListView();

            LoadDataToToolbox.AutoCompleteTextBox(txtVendorID, "SELECT CustomerID FROM CRM_Customers WHERE Isvendor = 1 ");
            LoadDataToToolbox.ComboBoxData(coWarehouse, "SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse", true);

            PickerToDay.CustomFormat = mFormatDate;
            PickerFromDay.CustomFormat = mFormatDate;

            PickerFromDay.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            if (FlagView)
            {
                ListViewData.CheckBoxes = true;
                cmdSelect.Visible = true;
                ListViewData.Columns[0].Width = 50;
                panelSearch.Height = 98;
            }

            DataTable nTable = Stock_Note_Data.NoteInput(PickerFromDay.Value, PickerToDay.Value);
            LoadDataFields(ListViewData, nTable);

            panelSearch.Height = 98;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;

            this.WindowState = FormWindowState.Maximized;
        }

        #region [Action Button]
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            SearchNote();
        }
        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            FrmStockNoteInputDetail frm = new FrmStockNoteInputDetail();
            frm.ShowDialog();
            SearchNote();
        }
        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "ID";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diễn giải";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng tiền";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhà cung cấp";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            ListViewData.SmallImageList = SmallImageLV;

            LVGroup = new ListViewMyGroup(ListViewData);

        }

        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            int nNoteKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
            FrmStockNoteInputDetail frm = new FrmStockNoteInputDetail();
            frm.mNoteKey = nNoteKey;
            frm.ShowDialog();

            SearchNote();
            SelectIndexInListView(nNoteKey);
        }

        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)
                {
                    
                    if (MessageBox.Show("Bạn có muốn xóa phiếu nhập này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        int nIndex = ListViewData.SelectedItems[0].Index;
                        Stock_Note_Input_Object nNote = new Stock_Note_Input_Object((int)ListViewData.SelectedItems[0].Tag);
                        nNote.DeleteObject();
                        ListViewData.SelectedItems[0].Remove();
                        if (ListViewData.Items.Count > 0)
                            if (nIndex == 0)
                                ListViewData.Items[nIndex].Selected = true;
                            else
                                ListViewData.Items[nIndex - 1].Selected = true;
                        ListViewData.Focus();
                    }

                }
                else
                    MessageBox.Show("Chọn khách hàng hoặc nhà cung cấp");

            }

        }

        // Load Database
        public void LoadDataFields(ListView LV, DataTable nTable)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            int n = nTable.Rows.Count;

            LV.Items.Clear();
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = " ";
                lvi.Tag = nRow["NoteKey"]; // Set the tag to 

                lvi.ImageIndex = 0;

                DateTime nImportNoteDate = (DateTime)nRow["NoteDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nImportNoteDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NoteID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["NoteDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

        }
        #endregion

        #region [Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 98;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtTitle.Left = panelTitleCenter.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Danh cho khi truy cap tu form khac vao ]
        private int mVendorKey = 0;
        private void cmdSelect_Click(object sender, EventArgs e)
        {
            if (FlagView)
            {
                CheckImportNoteSelect();
                this.Close();
            }
        }
        private void ListViewData_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                int nVendorKeySelect = (int)ListViewData.Items[e.Index].SubItems[5].Tag;
                if (mVendorKey == 0)
                    mVendorKey = nVendorKeySelect;
                else
                    if (mVendorKey != nVendorKeySelect)
                    {
                        MessageBox.Show("Không cùng nhà cung cấp");
                        e.NewValue = CheckState.Unchecked;
                    }
            }
            else
            {
                if (ListViewData.CheckedItems.Count == 1)
                {
                    mVendorKey = 0;
                }
            }

        }
        private void CheckImportNoteSelect()
        {
            int n = ListViewData.CheckedItems.Count;
            for (int i = 0; i < n; i++)
            {
                // ListImportNote.Add(ListViewData.CheckedItems[i].SubItems[2].Text);
            }
        }
        #endregion

        #region [ Function ]
        private void SearchNote()
        {
            DataTable nTable = Stock_Note_Data.NoteInput(PickerFromDay.Value, PickerToDay.Value, (int)coWarehouse.SelectedValue, txtVendorID.Text, txtVendorName.Text);
            LoadDataFields(ListViewData, nTable);

        }
        private void SelectIndexInListView(int ImportKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == ImportKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercutiry ]
        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT002");
            if (nRole.Del)
                m_RoleDelete = true;
        }
        #endregion
    }
}