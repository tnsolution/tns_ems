namespace TN_UI.FRM.Inventory
{
    partial class FrmBegin_Inventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBegin_Inventory));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panelSearch = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CoCategories = new System.Windows.Forms.ComboBox();
            this.coWarehouse = new System.Windows.Forms.ComboBox();
            this.txtSearchProductName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSearchProductID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.cmdUnhideCenter = new System.Windows.Forms.PictureBox();
            this.cmdHideCenter = new System.Windows.Forms.PictureBox();
            this.panelHead = new System.Windows.Forms.Panel();
            this.txtTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.ImageListGV = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioViewReport = new System.Windows.Forms.RadioButton();
            this.radioGridView = new System.Windows.Forms.RadioButton();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panelSearch.SuspendLayout();
            this.panelTitleCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).BeginInit();
            this.panelHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelSearch.Controls.Add(this.groupBox1);
            this.panelSearch.Controls.Add(this.label2);
            this.panelSearch.Controls.Add(this.label10);
            this.panelSearch.Controls.Add(this.CoCategories);
            this.panelSearch.Controls.Add(this.coWarehouse);
            this.panelSearch.Controls.Add(this.txtSearchProductName);
            this.panelSearch.Controls.Add(this.label36);
            this.panelSearch.Controls.Add(this.txtSearchProductID);
            this.panelSearch.Controls.Add(this.label11);
            this.panelSearch.Controls.Add(this.cmdSearch);
            this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSearch.Location = new System.Drawing.Point(0, 64);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(1006, 73);
            this.panelSearch.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(365, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 74;
            this.label2.Text = "Nhóm sản phẩm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Maroon;
            this.label10.Location = new System.Drawing.Point(365, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 16);
            this.label10.TabIndex = 72;
            this.label10.Text = "Kho";
            // 
            // CoCategories
            // 
            this.CoCategories.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CoCategories.ForeColor = System.Drawing.Color.Navy;
            this.CoCategories.FormattingEnabled = true;
            this.CoCategories.Location = new System.Drawing.Point(471, 7);
            this.CoCategories.Name = "CoCategories";
            this.CoCategories.Size = new System.Drawing.Size(184, 21);
            this.CoCategories.TabIndex = 73;
            // 
            // coWarehouse
            // 
            this.coWarehouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coWarehouse.ForeColor = System.Drawing.Color.Navy;
            this.coWarehouse.FormattingEnabled = true;
            this.coWarehouse.Location = new System.Drawing.Point(471, 38);
            this.coWarehouse.Name = "coWarehouse";
            this.coWarehouse.Size = new System.Drawing.Size(184, 21);
            this.coWarehouse.TabIndex = 71;
            this.coWarehouse.SelectedIndexChanged += new System.EventHandler(this.coWarehouse_SelectedIndexChanged);
            // 
            // txtSearchProductName
            // 
            this.txtSearchProductName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchProductName.ForeColor = System.Drawing.Color.Navy;
            this.txtSearchProductName.Location = new System.Drawing.Point(119, 35);
            this.txtSearchProductName.Name = "txtSearchProductName";
            this.txtSearchProductName.Size = new System.Drawing.Size(236, 21);
            this.txtSearchProductName.TabIndex = 17;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(10, 38);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(85, 14);
            this.label36.TabIndex = 16;
            this.label36.Text = "Tên sản phẩm";
            // 
            // txtSearchProductID
            // 
            this.txtSearchProductID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchProductID.ForeColor = System.Drawing.Color.Navy;
            this.txtSearchProductID.Location = new System.Drawing.Point(119, 6);
            this.txtSearchProductID.Name = "txtSearchProductID";
            this.txtSearchProductID.Size = new System.Drawing.Size(236, 21);
            this.txtSearchProductID.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(10, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 14);
            this.label11.TabIndex = 15;
            this.label11.Text = "Mã Sản phẩm";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.ForeColor = System.Drawing.Color.Navy;
            this.cmdSearch.Location = new System.Drawing.Point(684, 6);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(77, 23);
            this.cmdSearch.TabIndex = 14;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.Navy;
            this.textBox12.Location = new System.Drawing.Point(87, 289);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(157, 23);
            this.textBox12.TabIndex = 24;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 291);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 16);
            this.label20.TabIndex = 23;
            this.label20.Text = "Email";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.Navy;
            this.textBox13.Location = new System.Drawing.Point(87, 165);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(157, 23);
            this.textBox13.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(6, 168);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Region";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(87, 135);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(157, 22);
            this.comboBox3.TabIndex = 20;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.Navy;
            this.textBox14.Location = new System.Drawing.Point(87, 258);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(157, 23);
            this.textBox14.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(6, 230);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 16);
            this.label22.TabIndex = 18;
            this.label22.Text = "Home Phone";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.Navy;
            this.textBox15.Location = new System.Drawing.Point(87, 196);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(157, 23);
            this.textBox15.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(6, 197);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 16;
            this.label23.Text = "Postal";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.Navy;
            this.textBox16.Location = new System.Drawing.Point(87, 227);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(157, 23);
            this.textBox16.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(6, 260);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 16);
            this.label24.TabIndex = 16;
            this.label24.Text = "Mobile";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(6, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 16);
            this.label25.TabIndex = 14;
            this.label25.Text = "Country";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.Navy;
            this.textBox17.Location = new System.Drawing.Point(87, 104);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(157, 23);
            this.textBox17.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(6, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 16);
            this.label26.TabIndex = 12;
            this.label26.Text = "City";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.Navy;
            this.textBox18.Location = new System.Drawing.Point(87, 14);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox18.Size = new System.Drawing.Size(157, 82);
            this.textBox18.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(6, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Address";
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.ForeColor = System.Drawing.Color.Navy;
            this.textBox19.Location = new System.Drawing.Point(87, 289);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(157, 23);
            this.textBox19.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(6, 291);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Email";
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.ForeColor = System.Drawing.Color.Navy;
            this.textBox20.Location = new System.Drawing.Point(87, 165);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(157, 23);
            this.textBox20.TabIndex = 22;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(6, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "Region";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(87, 135);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(157, 22);
            this.comboBox4.TabIndex = 20;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.ForeColor = System.Drawing.Color.Navy;
            this.textBox21.Location = new System.Drawing.Point(87, 258);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(157, 23);
            this.textBox21.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(6, 230);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 16);
            this.label30.TabIndex = 18;
            this.label30.Text = "Home Phone";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.ForeColor = System.Drawing.Color.Navy;
            this.textBox22.Location = new System.Drawing.Point(87, 196);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(157, 23);
            this.textBox22.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(6, 197);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 16;
            this.label31.Text = "Postal";
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Navy;
            this.textBox23.Location = new System.Drawing.Point(87, 227);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(157, 23);
            this.textBox23.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(6, 260);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 16);
            this.label32.TabIndex = 16;
            this.label32.Text = "Mobile";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(6, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 16);
            this.label33.TabIndex = 14;
            this.label33.Text = "Country";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.Navy;
            this.textBox24.Location = new System.Drawing.Point(87, 104);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(157, 23);
            this.textBox24.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(6, 105);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 16);
            this.label34.TabIndex = 12;
            this.label34.Text = "City";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.Navy;
            this.textBox25.Location = new System.Drawing.Point(87, 14);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox25.Size = new System.Drawing.Size(157, 82);
            this.textBox25.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(6, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 10;
            this.label35.Text = "Address";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Controls.Add(this.cmdUnhideCenter);
            this.panelTitleCenter.Controls.Add(this.cmdHideCenter);
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(0, 35);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(1006, 29);
            this.panelTitleCenter.TabIndex = 18;
            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);
            // 
            // cmdUnhideCenter
            // 
            this.cmdUnhideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhideCenter.BackgroundImage")));
            this.cmdUnhideCenter.Location = new System.Drawing.Point(725, 5);
            this.cmdUnhideCenter.Name = "cmdUnhideCenter";
            this.cmdUnhideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdUnhideCenter.TabIndex = 6;
            this.cmdUnhideCenter.TabStop = false;
            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);
            // 
            // cmdHideCenter
            // 
            this.cmdHideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHideCenter.BackgroundImage")));
            this.cmdHideCenter.Location = new System.Drawing.Point(725, 5);
            this.cmdHideCenter.Name = "cmdHideCenter";
            this.cmdHideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdHideCenter.TabIndex = 5;
            this.cmdHideCenter.TabStop = false;
            this.cmdHideCenter.Visible = false;
            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);
            // 
            // panelHead
            // 
            this.panelHead.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelHead.BackgroundImage")));
            this.panelHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHead.Controls.Add(this.txtTitle);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(1006, 35);
            this.panelHead.TabIndex = 0;
            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(412, 9);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(253, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "HÀNG TỒN KHO ĐẦU KỲ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 591);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1006, 34);
            this.panel1.TabIndex = 20;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(477, 172);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(384, 339);
            this.dataGridView1.TabIndex = 27;
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "bkHeader.png");
            // 
            // ImageListGV
            // 
            this.ImageListGV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageListGV.ImageStream")));
            this.ImageListGV.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageListGV.Images.SetKeyName(0, "New.png");
            this.ImageListGV.Images.SetKeyName(1, "Delete.png");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioViewReport);
            this.groupBox1.Controls.Add(this.radioGridView);
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(785, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(144, 61);
            this.groupBox1.TabIndex = 96;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hiển thị";
            // 
            // radioViewReport
            // 
            this.radioViewReport.AutoSize = true;
            this.radioViewReport.ForeColor = System.Drawing.Color.Navy;
            this.radioViewReport.Location = new System.Drawing.Point(7, 39);
            this.radioViewReport.Name = "radioViewReport";
            this.radioViewReport.Size = new System.Drawing.Size(118, 17);
            this.radioViewReport.TabIndex = 1;
            this.radioViewReport.Text = "Hiện thị dạng report";
            this.radioViewReport.UseVisualStyleBackColor = true;
            this.radioViewReport.CheckedChanged += new System.EventHandler(this.radioViewReport_CheckedChanged);
            // 
            // radioGridView
            // 
            this.radioGridView.AutoSize = true;
            this.radioGridView.Checked = true;
            this.radioGridView.ForeColor = System.Drawing.Color.Navy;
            this.radioGridView.Location = new System.Drawing.Point(6, 19);
            this.radioGridView.Name = "radioGridView";
            this.radioGridView.Size = new System.Drawing.Size(107, 17);
            this.radioGridView.TabIndex = 0;
            this.radioGridView.TabStop = true;
            this.radioGridView.Text = "Hiện thị dạng lưới";
            this.radioGridView.UseVisualStyleBackColor = true;
            this.radioGridView.CheckedChanged += new System.EventHandler(this.radioGridView_CheckedChanged);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.DisplayGroupTree = false;
            this.crystalReportViewer1.Location = new System.Drawing.Point(57, 172);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.ShowCloseButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(339, 206);
            this.crystalReportViewer1.TabIndex = 30;
            this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            this.crystalReportViewer1.Visible = false;
            // 
            // FrmBegin_Inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 625);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.panelTitleCenter);
            this.Controls.Add(this.panelHead);
            this.Name = "FrmBegin_Inventory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hàng tồn đầu kỳ";
            this.Load += new System.EventHandler(this.FrmBegin_Inventory_Load);
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            this.panelTitleCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).EndInit();
            this.panelHead.ResumeLayout(false);
            this.panelHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.PictureBox cmdHideCenter;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.PictureBox cmdUnhideCenter;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox coWarehouse;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtSearchProductName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtSearchProductID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ImageList imageList4;
        private System.Windows.Forms.ImageList ImageListGV;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CoCategories;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioViewReport;
        private System.Windows.Forms.RadioButton radioGridView;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;





    }
}