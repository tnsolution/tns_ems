using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.IVT;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmWarehouseDetail : Form
    {
        public int mWarehouseKey = 0;
        private Warehouse_Info mWarehouse;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmWarehouseDetail()
        {
            InitializeComponent();
        }
        private void FrmWarehouseDetail_Load(object sender, EventArgs e)
        {
            CheckRole();
            mWarehouse = new Warehouse_Info(mWarehouseKey);
            LoadInfoWarehouse();

        }

        #region [ Warehouse Information ]

        protected void LoadInfoWarehouse()
        {
            txtWarehouseName.Text = mWarehouse.Name;
            txtAddress.Text = mWarehouse.Address;
            txtDescription.Text = mWarehouse.Description;
        }

        #endregion

        #region [ Update Warehouse Information]
        private bool CheckBeforeSave()
        {

            if (txtWarehouseName.Text.Trim().Length == 0)
            {
                MessageBox.Show("vui lòng nhập tên kho");
                txtWarehouseName.Focus();
                return false;
            }
            return true;

        }
        private bool SaveWarehouseInfomation()
        {
            mWarehouse.Name = txtWarehouseName.Text;
            mWarehouse.Address = txtAddress.Text;
            mWarehouse.Description = txtDescription.Text;

            return true;

        }
        #endregion

        #region [ Action Button ]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveWarehouseInfomation();
                mWarehouse.Save();
                this.Close();
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveWarehouseInfomation();
                mWarehouse.Save();
                mWarehouse = new Warehouse_Info();
                LoadInfoWarehouse();
                txtWarehouseName.Focus();
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (Stock_Note_Data.CheckExistWarehouse(mWarehouse.Key) > 0)
            {
                MessageBox.Show("Không thể xóa thông tin này");
            }
            else
            {
                mWarehouse.Delete();
                this.Close();
            }

        }
        #endregion

        #region [ Short Key ]
        private void FrmWarehouseDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Sercurity ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);

        private void CheckRole()
        {
            mRole.Check_Role("IVT001");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

      

    }
}