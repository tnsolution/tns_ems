using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;

using TNLibrary.IVT;
using TNLibrary.SYS;
using TNLibrary.FNC;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmReportInventory : Form
    {

        ReportDocument rpt = new ReportDocument();
        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        string mFormatDate = GlobalSystemConfig.FormatDate ;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        bool IsExport;
        string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        string mCloseMonth = "";
        public FrmReportInventory()
        {
            InitializeComponent();
            
        }
        private void FrmReportInventory_Load(object sender, EventArgs e)
        {
            panelSearch.Height = 86;
            cmdHideCenter.Visible = true;
            cmdUnhideCenter.Visible = false;

            FinancialYear_Data.FinancialMonth(coFromMonth);
            FinancialYear_Data.FinancialMonth(coToMonth);

            FinancialYear_Info nYear = new FinancialYear_Info(true);
            mCloseMonth = nYear.FromMonth;

            PickerFromDate.CustomFormat = mFormatDate;
            PickerToDate.CustomFormat = mFormatDate;
            coDisplay.SelectedIndex = 0;
            LoadDataToToolbox.ComboBoxData(coWarehouse,"SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse",true);
            this.WindowState = FormWindowState.Maximized;
            SetupLayoutGridView(dataGridView1);

            //PopulateDataGridView(dataGridView1, Stock_Note_Data.Inventory(mCloseMonth, nWarehouseKey, FromDate, ToDate));
            SetupCrystalReport();

            coFromMonth.SelectedIndex = 0;
            coToMonth.SelectedIndex = DateTime.Now.Month - 1;
        }


        private void cmdSearch_Click(object sender, EventArgs e)
        {
            DateTime nFromDate = new DateTime();
            DateTime nToDate = new DateTime();



            Cursor.Current = Cursors.WaitCursor;

            if (radioFollowMonth.Checked == true)
            {
                nFromDate = TNFunctions.TransferToDateBeginMonth(coFromMonth.Text);
                nToDate = TNFunctions.TransferToDateEndMonth(coToMonth.Text);
            }
            else
            {
                nFromDate = new DateTime(PickerFromDate.Value.Year, PickerFromDate.Value.Month, PickerFromDate.Value.Day, 0, 0, 0);
                nToDate = new DateTime(PickerToDate.Value.Year, PickerToDate.Value.Month, PickerToDate.Value.Day, 23, 59, 0);

            }
            LoadInventoryFromDateToDate(nFromDate, nToDate);

            Cursor.Current = Cursors.Default;

        }

        #region [Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 82;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width-5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width-5;
            
            coDisplay.Left = cmdHideCenter.Left - coDisplay.Width - 5;
            lbDisplay.Left = coDisplay.Left - lbDisplay.Width - 5;

            txtTitle.Left = panelTitleCenter.Width / 2 - txtTitle.Width / 2; 
        }
        #endregion

        #region [ Process Data ]

        public void LoadInventoryFromDateToDate(DateTime FromDate, DateTime ToDate)
        {
            int nWarehouseKey = (int)coWarehouse.SelectedValue;
            DataTable nTableInventory = Stock_Note_Data.Inventory(mCloseMonth, nWarehouseKey, FromDate, ToDate);
            if (!IsExport)
            {
                if (coDisplay.SelectedIndex == 0)
                {
                    PopulateDataGridView(dataGridView1, nTableInventory);
                }
                else
                {
                    SetupCrystalReport();
                    PopulateCrystalReport(nTableInventory);
                }
            }
            else
            {
                PopulateExcel(nTableInventory);
                IsExport = false;

                //MessageBox.Show("export thành công ");
            }
        }
      
        #endregion
    
        #region [ GridView ]
        private void SetupLayoutGridView(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "");
            //GV.Columns.Add("ProductKey", "");
            GV.Columns.Add("WarehouseID", "");
            GV.Columns.Add("ProductName", "");
            GV.Columns.Add("Unit", "");

            GV.Columns.Add("Begin_Quantity", "");
            GV.Columns.Add("Begin_AmountCurrencyMain", "");


            GV.Columns.Add("In_Quantity", "");
            GV.Columns.Add("In_AmountCurrencyMain", "");
            GV.Columns.Add("Out_Quantity", "");
            GV.Columns.Add("Out_AmountCurrencyMain", "");
            GV.Columns.Add("End_Quantity", "");
            GV.Columns.Add("End_AmountCurrencyMain", "");
            //GV.Columns.Add("End_Quantity", "");
            //GV.Columns.Add("End_AmountCurrencyMain", "");

            GV.Columns[0].Width = 50;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[1].Width = 110;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 200;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 70;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[4].Width = 90;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[5].Width = 118;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 90;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 118;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[8].Width = 90;
            GV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[9].Width = 118;
            GV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[10].Width = 90;
            GV.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[11].Width = 118;
            GV.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * 2;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(GridView_CellFormatting);
            GV.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(GridView_CellPainting);

            GV.Paint += new PaintEventHandler(GridView_Paint);

            GV.ColumnWidthChanged += new DataGridViewColumnEventHandler(GridView_ColumnWidthChanged);

            GV.Resize += new System.EventHandler(GridView_Resize);

        }
        private void PopulateDataGridView(DataGridView GV, DataTable TableView)
        {

            double SumBeginAmount = 0;
            double SumImportAmount = 0;
            double SumExportAmount = 0;
            double SumEndAmount = 0;
            int No;
            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];

                float Begin_Quantity = float.Parse(nRow["Begin_Quantity"].ToString());
                double Begin_AmountCurrencyMain = double.Parse(nRow["Begin_AmountCurrencyMain"].ToString());
                float In_Quantity = float.Parse(nRow["In_Quantity"].ToString());
                double In_AmountCurrencyMain = double.Parse(nRow["In_AmountCurrencyMain"].ToString());
                float Out_Quantity = float.Parse(nRow["Out_Quantity"].ToString());
                double Out_AmountCurrencyMain = double.Parse(nRow["Out_AmountCurrencyMain"].ToString());
                float End_Quantity = float.Parse(nRow["End_Quantity"].ToString());
                double End_AmountCurrencyMain = double.Parse(nRow["End_AmountCurrencyMain"].ToString());


                SumBeginAmount += Begin_AmountCurrencyMain;
                SumImportAmount += In_AmountCurrencyMain;
                SumExportAmount += Out_AmountCurrencyMain;
                SumEndAmount += End_AmountCurrencyMain;
                No = i + 1;
              
                string[] nRowView = 
                    { 
                        No.ToString(),
                   
                        nRow["ProductID"].ToString().Trim(), 
                        nRow["ProductName"].ToString(),
                        nRow["Unit"].ToString(),
                        //Begin_Quantity.ToString(mFormatDecimal, mFormatProviderCurrency), 
                        //Begin_AmountCurrencyMain.ToString(mFormatDecimal, mFormatProviderCurrency), 
                        
                        
                        Begin_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        Begin_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        
                        In_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        In_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        Out_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        Out_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        End_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        End_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency)
               

            };
            GV.Rows.Add(nRowView);
        }
            //GV.Rows[i].Cells["No"].Value = GV.Rows.Count;
            string[] nRowTotal = 
            { 
                        " ", 
                        " ", 
                     
                      
 
                        "Tổng cộng ",
                     
                         " ", 
                        " ", 
                        SumBeginAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        
                        "", 
                        SumImportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        "", 
                        SumExportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        "", 
                        SumEndAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        
            };
            GV.Rows.Add(nRowTotal);

        }

        void GridView_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;


            string[] TitleMerge = { "Tồn đầu kỳ", "Nhập", "Xuất","Cuối kỳ" };
            string[] TitleColumn = { "STT", "Mã", "Tên nguyên vật liệu", "Đơn vị tính", "Số lượng", " Thành tiền", "Số lượng", " Thành tiền", "Số lượng", " Thành tiền", "Số lượng", " Thành tiền" };

            int nColumnTotal = GV.ColumnCount;
            // bat dau merge column
            int nColumnBeginMerge = 4;
            
            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;


                if (i>3) // binh thuong
                {
                    r1.Height = r1.Height / 2 - 2;
                    r1.Width = r1.Width - 2;
                    r1.Y = r1.Height + 3;
                    g.DrawImage(image, r1);
                }
                else // merge
                {
                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;
                    g.DrawImage(image, r1);
                }

                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }

            int k = 0;

            for (int j = nColumnBeginMerge; j < nColumnTotal; j += 2)
            {

                Rectangle r1 = GV.GetCellDisplayRectangle(j, -1, true);

                int w2 = GV.GetCellDisplayRectangle(j + 1, -1, true).Width;

                r1.X += 1;
                r1.Y += 1;

                r1.Width = r1.Width + w2 - 2;
                r1.Height = r1.Height / 2 - 2;

                g.DrawImage(image, r1);
                // g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                k++;
            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        void GridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView GV = dataGridView1;

            Rectangle rtHeader = GV.DisplayRectangle;
            //  rtHeader.Height = GV.ColumnHeadersHeight / 2;
            GV.Invalidate(rtHeader);

        }
       
        private void GridView_Resize(object sender, EventArgs e)
        {
            DataGridView GV = dataGridView1;

            Rectangle rtHeader = GV.DisplayRectangle;
            //rtHeader.Height = this.dataGridView1.ColumnHeadersHeight / 2;
            GV.Invalidate(rtHeader);
        }

        void GridView_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            DataGridView GV = dataGridView1;
            if (e.RowIndex >=0)
            {
                DataGridViewRow nRow = GV.Rows[e.RowIndex];
                if (nRow.Cells["No"].Value.ToString().Trim().Length == 0)
                {
                    e.CellStyle.BackColor = Color.FromArgb(242, 248, 255);
                    e.CellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

                    e.CellStyle.ForeColor = Color.Navy;
                    
                }
            }
        }
        private void GridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            DataGridView GV = dataGridView1;
            if (e.RowIndex >= 0 && (e.ColumnIndex == 1 || e.ColumnIndex == 2))
            {
                DataGridViewRow nRow = GV.Rows[e.RowIndex];
                if (nRow.Cells[0].Value.ToString().Trim().Length == 0)
                {
                    Rectangle r1 = GV.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    r1.Width = r1.Width;
                    r1.Height = r1.Height - 1;
                    Brush gridBrush = new SolidBrush(GV.GridColor);
                    Pen gridLinePen = new Pen(gridBrush);

                    SolidBrush backColorBrush = new SolidBrush(e.CellStyle.BackColor);

                    e.Graphics.FillRectangle(backColorBrush, r1);

                    e.Graphics.DrawLine(gridLinePen, e.CellBounds.Left,
                   e.CellBounds.Bottom - 1, e.CellBounds.Right - 1,
                   e.CellBounds.Bottom - 1);

                    Rectangle rstr = GV.GetCellDisplayRectangle(1, e.RowIndex, true);
                    int w2 = GV.GetCellDisplayRectangle(2, e.RowIndex, true).Width;
                    rstr.Width = rstr.Width + w2 - 2;
                    rstr.Height = rstr.Height - 2;
                    rstr.Y = rstr.Top + 3;


                    e.Graphics.DrawString(nRow.Cells[2].Value.ToString(), e.CellStyle.Font, new SolidBrush(e.CellStyle.ForeColor), rstr);

                    e.Handled = true;

                }
            }

        }
        #endregion

        #region [ Display ]
        private void coDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

            //DataTable nTableInventory;
            //nTableInventory = Stock_Note_Data.Inventory(mCloseMonth, nWarehouseKey, FromDate, ToDate);

            if (coDisplay.SelectedIndex == 0)
            {
                dataGridView1.Visible = true;

                crystalReportViewer1.Visible = false;
            }
            else
            {
                dataGridView1.Visible = false;
                crystalReportViewer1.Visible = true;

            }
            //PopulateCrystalReport(nTableInventory);
        }
        private void radioFollowDate_Click(object sender, EventArgs e)
        {
            radioFollowDate.Checked = true;
            radioFollowMonth.Checked = false;
        }
        private void radioFollowMonth_Click(object sender, EventArgs e)
        {
            radioFollowDate.Checked = false;
            radioFollowMonth.Checked = true;
        }

        #endregion

       
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;
            dataGridView1.Dock = DockStyle.Fill;
        }
        private void PopulateCrystalReport(DataTable Table)
        {

            // thay ten report
            rpt.Load("FilesReport\\CrystalReportInventory.rpt");
            DataTable nTableTitle = TableReport.TableInventoryDetail();
            DataTable nTableInventory = TableReport.TableInventory();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            DataRow nRow;
            double SumBeginAmount = 0;
            double SumImportAmount = 0;
            double SumExportAmount = 0;
            double SumEndAmount = 0;

           
            int No;
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];

                float Begin_Quantity = float.Parse(nRow["Begin_Quantity"].ToString());
                double Begin_AmountCurrencyMain = double.Parse(nRow["Begin_AmountCurrencyMain"].ToString());
                float In_Quantity = float.Parse(nRow["In_Quantity"].ToString());
                double In_AmountCurrencyMain = double.Parse(nRow["In_AmountCurrencyMain"].ToString());
                float Out_Quantity = float.Parse(nRow["Out_Quantity"].ToString());
                double Out_AmountCurrencyMain = double.Parse(nRow["Out_AmountCurrencyMain"].ToString());
                float End_Quantity = float.Parse(nRow["End_Quantity"].ToString());
                double End_AmountCurrencyMain = double.Parse(nRow["End_AmountCurrencyMain"].ToString());


                SumBeginAmount += Begin_AmountCurrencyMain;
                SumImportAmount += In_AmountCurrencyMain;
                SumExportAmount += Out_AmountCurrencyMain;
                SumEndAmount += End_AmountCurrencyMain;
                No = i + 1;

                string[] nRowView = 
                    { 
                        No.ToString(),
                   
                        nRow["ProductID"].ToString().Trim(), 
                        nRow["ProductName"].ToString(),
                        nRow["Unit"].ToString(),
                       
                        
                        Begin_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        Begin_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        
                        In_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        In_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        Out_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        Out_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        End_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        End_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency)
 
                    };
                nTableInventory.Rows.Add(nRowView);
            }

       
              string strTitleHeader = "";
            string strTitleFooter = "";
            if (radioFollowMonth.Checked == true)
            {
                strTitleHeader = "Từ tháng " + coFromMonth.Text + " đến tháng " + coToMonth.Text;
            }
            else
            {
                strTitleHeader = "Từ ngày " + PickerFromDate.Value.ToString(mFormatDate) + " đến ngày " + PickerToDate.Value.ToString(mFormatDate);

            }
            if (radioFollowMonth.Checked == true)
            {
                strTitleFooter = "Ngày " + TNFunctions.DayEndMonth(coToMonth.Text) + " tháng " + coToMonth.Text.Substring(0, 2) + " năm " + coToMonth.Text.Substring(3, 4);
            }
            else
            {
                strTitleFooter = "Ngày " + PickerToDate.Value.Day.ToString() + " tháng " + PickerToDate.Value.Month.ToString() + " năm " + PickerToDate.Value.Year.ToString();
            }

            string[] nRowTotal = 
            { 
                    strTitleHeader, 
                    strTitleFooter, 
                     "Tai Khoan", 
                    SumImportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                    SumExportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                    SumEndAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency),  
                    SumBeginAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency)
                        
                        
            };
            nTableTitle.Rows.Add(nRowTotal);

            rpt.Database.Tables["Inventory"].SetDataSource(nTableInventory);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);
            rpt.Database.Tables["InventoryDetail"].SetDataSource(nTableTitle);
            crystalReportViewer1.ReportSource = rpt;
            
            crystalReportViewer1.Zoom(1);

        }

  

        private void radioFollowMonth_CheckedChanged(object sender, EventArgs e)
        {
            if(radioFollowMonth.Checked==true)
            radioFollowDate.Checked = false;
            
        }

        private void radioFollowDate_CheckedChanged(object sender, EventArgs e)
        {
            if (radioFollowDate.Checked == true)
           
            radioFollowMonth.Checked = false;
        }



        private void PopulateExcel(DataTable Table)
        {
            DataTable nTableInventory = TableReport.TableInventory();
            DataTable nCompanyInfo = TableReport.CompanyInfo();
            DataTable nTableTitle = TableReport.TableInventoryDetail();

            DataRow nRow;
            //DataRow nRow;
            double SumBeginAmount = 0;
            double SumImportAmount = 0;
            double SumExportAmount = 0;
            double SumEndAmount = 0;

            int No;
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];



                float Begin_Quantity = float.Parse(nRow["Begin_Quantity"].ToString());
                double Begin_AmountCurrencyMain = double.Parse(nRow["Begin_AmountCurrencyMain"].ToString());

                float In_Quantity = float.Parse(nRow["In_Quantity"].ToString());
                double In_AmountCurrencyMain = double.Parse(nRow["In_AmountCurrencyMain"].ToString());

                float Out_Quantity = float.Parse(nRow["Out_Quantity"].ToString());
                double Out_AmountCurrencyMain = double.Parse(nRow["Out_AmountCurrencyMain"].ToString());

                float End_Quantity = float.Parse(nRow["End_Quantity"].ToString());
                double End_AmountCurrencyMain = double.Parse(nRow["End_AmountCurrencyMain"].ToString());

                SumBeginAmount += Begin_AmountCurrencyMain;
                SumImportAmount += In_AmountCurrencyMain;
                SumExportAmount += Out_AmountCurrencyMain;
                SumEndAmount += End_AmountCurrencyMain;
                No = i + 1;
                string[] nRowView = 
                    { 
                        No.ToString(),
                        nRow["ProductID"].ToString().Trim(), 
                        nRow["ProductName"].ToString().Trim(),
                        nRow["Unit"].ToString(),
                      
                        
                        
                        Begin_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal), 
                        Begin_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        
                        In_Quantity.ToString(mFormatDecimal, mFormatProviderCurrency), 
                        In_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        Out_Quantity.ToString(mFormatDecimal, mFormatProviderCurrency), 
                        Out_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 

                        End_Quantity.ToString(mFormatDecimal, mFormatProviderCurrency), 
                        End_AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency)
                     
                       
 
                    };
                nTableInventory.Rows.Add(nRowView);
            }
            ArrayList a = new ArrayList();


            string strTitleHeader = "";
            string strTitleFooter = "";
            if (radioFollowMonth.Checked == true)
            {
                strTitleHeader = "Từ tháng " + coFromMonth.Text + " đến tháng " + coToMonth.Text;
            }
            else
            {
                strTitleHeader = "Từ ngày " + PickerFromDate.Value.ToString(mFormatDate) + " đến ngày " + PickerToDate.Value.ToString(mFormatDate);

            }
            if (radioFollowMonth.Checked == true)
            {
                strTitleFooter = "Ngày " + TNFunctions.DayEndMonth(coToMonth.Text) + " tháng " + coToMonth.Text.Substring(0, 2) + " năm " + coToMonth.Text.Substring(3, 4);
            }
            else
            {
                strTitleFooter = "Ngày " + PickerToDate.Value.Day.ToString() + " tháng " + PickerToDate.Value.Month.ToString() + " năm " + PickerToDate.Value.Year.ToString();
            }
            string[] nRowTotal = 
            { 
                    strTitleHeader, 
                    strTitleFooter, 
                     "Tai Khoan", 
                    SumImportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                    SumExportAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                    SumEndAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency),  
                    SumBeginAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency)
                        
                        
            };
            nTableTitle.Rows.Add(nRowTotal);
            ExportToExcel(nTableInventory, nCompanyInfo, nTableTitle);
        }

        private static void ExportToExcel(DataTable dtDetail,DataTable dtCongty, DataTable dtTitle)
        {
            // Create an Excel object and add workbook...a
            string FileName, FileType;
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            DialogSave.Title = "Save a file to...";

            //save.ShowDialog();

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                FileName = DialogSave.FileName;
                FileType = System.IO.Path.GetExtension(FileName);


                Excel.Application excel = new Excel.Application();
                Excel.Workbook exBook = excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Workbook workbook = excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet exSheet = (Excel.Worksheet)exBook.Worksheets[1];
                Excel.Range nRange;

                excel.ActiveWindow.DisplayGridlines = true;

               
                // add thong tin cong ty
                foreach (DataRow r in dtCongty.Rows)
                {

                    excel.Cells[1, 1] = r["CompanyName"].ToString();
                    excel.Cells[2, 1] = r["Address"].ToString();
                }
                //Add Tieu de
                excel.Cells[4, 5] = "BẢNG BÁO CÁO NHẬP XUẤT TỒN";
                //add Column Name
                excel.Cells[9, 1] = "STT";
                excel.Cells[9, 2] = "Mã";
                excel.Cells[9, 3] = "Tên nguyên liệu";
                excel.Cells[9, 4] = "Đơn vị tính";
                excel.Cells[8, 5] = "Tồn đầu kỳ";
                excel.Cells[9, 5] = "Số lượng";
                excel.Cells[9, 6] = "Thành tiền";
                excel.Cells[8, 7] = "Nhập";
                excel.Cells[9, 7] = "Số lượng";
                excel.Cells[9, 8] = "Thành tiền";
                excel.Cells[8, 9] = "Xuất";
                excel.Cells[9, 9] = "Số lượng";
                excel.Cells[9, 10] = "Thành tiền";
                excel.Cells[8, 11] = "Tồn cuối kỳ";
                excel.Cells[9, 11] = "Số lượng";
                excel.Cells[9, 12] = "Thành tiền";
                //excel.Cells[6, 5] = "Tài khoản";
                //excel.Cells[12, 3] = "Tổng cộng";
               
                //bat dau ghi chi tiet vo file excel
                int iCol = 0;
                // for each row of data...
                int iRow = 0;
                foreach (DataRow r in dtDetail.Rows)
                {
                    iRow++;

                    // add each row's cell data...(dl hien thi)
                    iCol = 0;
                    foreach (DataColumn c in dtDetail.Columns)
                    {
                        iCol++;
                        excel.Cells[iRow + 9, iCol] = r[c.ColumnName].ToString();
                    }
                    excel.Cells[iRow + 10, iCol] = "";

                }

                foreach (DataRow r in dtTitle.Rows)
                {
                    excel.Cells[5, 5] = " " + r["TieuDeTren"].ToString();

                    excel.Cells[iRow + 12, 11] = r["TieuDeDuoi"].ToString();

                    excel.Cells[iRow + 10, 3] = "Tổng cộng";
                    //excel.Cells[iRow + 6, 6] = r["TaiKhoan"].ToString();
                    excel.Cells[iRow + 10, 6] = r["SumTTTonDau"].ToString();

                    excel.Cells[iRow + 10, 8] = r["SumTTNhap"].ToString();
                    excel.Cells[iRow + 10, 10] = r["SumTTXuat"].ToString();
                    excel.Cells[iRow + 10, 12] = r["SumTTTonCuoi"].ToString();


                }

                //add Footer
                excel.Cells[iRow + 13, 1] = "    Người ghi sổ";
                excel.Cells[iRow + 13, 6] = "Kế toán trưởng";
                excel.Cells[iRow + 13, 11] = "        Giám đốc";


                ////tao border
                //exApp.Range nRange;
                Excel.Worksheet worksheet1 = (Excel.Worksheet)excel.ActiveSheet;
                //dinh dang tieu de
                nRange = worksheet1.get_Range("A4", "L4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;


                nRange = worksheet1.get_Range("E" + (iRow + 10), "L" + iRow);
                nRange.NumberFormat = "#,###,##0.00";
                //nRange.Font.Bold = true;
                 
                //nRange = worksheet1.get_Range("E10", );
                //nRange.NumberFormat = "###,0"; 
                
                //dinh dang Summuavao
                int nS = iRow + 10;
                nRange = worksheet1.get_Range("A" + nS, "L" + nS);
             
                nRange.Font.Bold = true;
                nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;


                //dinh dang footer
                int nRo1 = iRow + 10;
                int nRo2 = iRow + 13;
                nRange = worksheet1.get_Range("A" + nRo1, "L" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                //nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                //nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                
                nRange = worksheet1.get_Range("A" + nRo2, "L" + nRo2);
                nRange.Font.Bold = true;

                //dinh dang columns Name
                nRange = worksheet1.get_Range("A8", "L9");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Interior.Color = Color.Silver.ToArgb();
                nRange.Cells.RowHeight = 26;
                nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                //dinh dang Columns width
                nRange = worksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 6;
                
                //2
                nRange = worksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 10;
                
                //3
                nRange = worksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 35;
                //4
                nRange = worksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;
                
                //5
                nRange = worksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //6
                nRange = worksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //7
                nRange = worksheet1.get_Range("G1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //8
                nRange = worksheet1.get_Range("H1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //9
                nRange = worksheet1.get_Range("I1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //10
                nRange = worksheet1.get_Range("J1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //11
                nRange = worksheet1.get_Range("K1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //12
                nRange = worksheet1.get_Range("L1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;



                // chon vung tao border
                int x = iRow + 10;//tang cac o xuong duoi
                nRange = worksheet1.get_Range("A8", "L" + x);
                nRange.Borders.Weight = 2;

                //merger cac columns
                //nRange.Borders.LineStyle = Excel.Constants.xlSolid;

                nRange = worksheet1.get_Range("E8", "F8");
                nRange.Merge(1);
                
               
                nRange = worksheet1.get_Range("G8", "H8");
                nRange.Merge(Type.Missing);
               

                nRange = worksheet1.get_Range("I8", "J8");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("K8", "L8");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("A8", "A9");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("B8", "B9");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("C8", "C9");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("D8", "D9");
                nRange.Merge(Type.Missing);

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo f = new FileInfo(FileName);
                if (f.Exists)
                    f.Delete(); // delete the file if it already exist.

                workbook.SaveAs(FileName,
                Excel.XlFileFormat.xlWorkbookNormal, missing, missing,
                false, false, Excel.XlSaveAsAccessMode.xlNoChange,
                missing, missing, missing, missing, missing);


                workbook.Close(null, null, null);
                excel.Workbooks.Close();
                excel.Application.Quit();
                excel.Quit();
                //Excel.WorksheeSystem.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
                //worksheet = null;
                workbook = null;
                excel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {

            IsExport = true;
            DateTime nFromDate = new DateTime();
            DateTime nToDate = new DateTime();



            Cursor.Current = Cursors.WaitCursor;

            if (radioFollowMonth.Checked == true)
            {
                nFromDate = TNFunctions.TransferToDateBeginMonth(coFromMonth.Text);
                nToDate = TNFunctions.TransferToDateEndMonth(coToMonth.Text);
            }
            else
            {
                nFromDate = new DateTime(PickerFromDate.Value.Year, PickerFromDate.Value.Month, PickerFromDate.Value.Day, 0, 0, 0);
                nToDate = new DateTime(PickerToDate.Value.Year, PickerToDate.Value.Month, PickerToDate.Value.Day, 23, 59, 0);

            }
            LoadInventoryFromDateToDate(nFromDate, nToDate);

            Cursor.Current = Cursors.Default;
        }  


        
    
    
    }
}