using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.IVT;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmStockNoteInputDetail : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;

        public Stock_Note_Input_Object mNoteInput;
        public int mNoteKey;
        public bool FlagTemp;

        public FrmStockNoteInputDetail()
        {
            InitializeComponent();
            dataGridViewProducts1.ImageHeader = imageList1.Images[0];
            dataGridViewProducts1.SetupLayoutGridView();
            dataGridViewProducts1.KeyDown += new System.Windows.Forms.KeyEventHandler(dataGridViewProducts1_KeyDown);
            dataGridViewProducts1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridViewProducts1_RowValidated);

        }
        private void FrmStockNoteInputDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coWarehouse, "SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse", false);
            LoadDataToToolbox.AutoCompleteTextBox(txtVendorID, "SELECT CustomerID FROM CRM_Customers WHERE Isvendor = 1");
            txtDocumentDate.CustomFormat = mFormatDate;
            mNoteInput = new Stock_Note_Input_Object(mNoteKey);
            LoadStockNoteInputInfo();

            if (FlagTemp)
            {
                DisplayForNoteTemp();
            }
            PickerImportNoteDate.CustomFormat = mFormatDate;
            txtNoteID.Focus();
            coWarehouse.SelectedIndex = 0;
        }

        #region [ Layout]

        #endregion

        #region [ Load Note Input Infomation ]
        private bool mNoteInputIDChanged = false;
        private void txtNoteID_TextChanged(object sender, EventArgs e)
        {
            mNoteInputIDChanged = true;
        }
        private void txtNoteID_Leave(object sender, EventArgs e)
        {
            if (mNoteInputIDChanged)
            {
                Stock_Note_Input_Object nStockNote = new Stock_Note_Input_Object(txtNoteID.Text);
                if (nStockNote.NoteKey > 0)
                {
                    if (MessageBox.Show("Đã có số phiếu nhập này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        mNoteInput = nStockNote;
                        LoadStockNoteInputInfo();
                    }
                    else
                    {
                        txtNoteID.Focus();
                    }
                }

            }

        }
        private void LoadStockNoteInputInfo()
        {
            PickerImportNoteDate.Value = mNoteInput.NoteDate;
            txtNoteID.Text = mNoteInput.NoteID.Trim();
            txtNoteName.Text = mNoteInput.NoteDescription;
            txtDeliverer.Text = mNoteInput.Deliverer;
            coWarehouse.SelectedValue = mNoteInput.WarehouseKey;
            mVendor = new Customer_Info(mNoteInput.CustomerKey);
            txtVendorID.Text = mVendor.ID;
            CoVendors.Text = mVendor.Name;
            chkIsInternal.Checked = mNoteInput.IsInternal;

            txtDocumentAttached.Text = mNoteInput.DocumentAttached;
            txtDocumentOrigin.Text = mNoteInput.DocumentOrigin;
            txtDocumentDate.Value = mNoteInput.DocumentDate;

            dataGridViewProducts1.ListProduct = mNoteInput.ProductsInput;
            txtSumOrder.Text = mNoteInput.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            if (mNoteInput.AmountCurrencyMain == 0)
            {
                double nSumAmountCurrencyMain = 0;
                foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
                {
                    if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                    {
                        nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                    }
                }
                txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            }
            mNoteInputIDChanged = false;
        }

        #endregion

        #region [ Update Note Input]
        private bool SaveNoteInputInfomation()
        {
            if (!CheckBeforeSave()) return false;
            if (!FlagTemp)
            {
                if (mVendor == null || mVendor.Key == 0)
                {
                    if (MessageBox.Show("Chưa có nhà cung cấp này, ? Bạn có muốn thêm nhà cung cấp này ? ", "Thêm ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        mVendor = new Customer_Info();
                        mVendor.ID = txtVendorID.Text;
                        mVendor.Name = CoVendors.Text;
                        //  Vendor.Address = txt.Text;
                        mVendor.CategoryKey = 2;
                        mVendor.IsVendor = true;
                        mVendor.Create();
                    }
                    else
                        return false;
                }
            }

            int nWarehouseKey = (int)coWarehouse.SelectedValue;

            mNoteInput.NoteID = txtNoteID.Text;
            mNoteInput.NoteDate = PickerImportNoteDate.Value;
            mNoteInput.NoteDescription = txtNoteName.Text;
            mNoteInput.WarehouseKey = nWarehouseKey;
            mNoteInput.IsInternal = chkIsInternal.Checked;

            mNoteInput.DocumentAttached = txtDocumentAttached.Text;
            mNoteInput.DocumentOrigin = txtDocumentOrigin.Text;
            mNoteInput.DocumentDate = txtDocumentDate.Value;

            mNoteInput.CustomerKey = mVendor.Key;
            mNoteInput.Deliverer = txtDeliverer.Text;
            mNoteInput.AmountCurrencyMain = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            mNoteInput.CreatedBy = SessionUser.UserLogin.Key;
            mNoteInput.ModifiedBy = SessionUser.UserLogin.Key;

            mNoteInput.ProductsInput = dataGridViewProducts1.ListProduct;

            return true;
        }
        #endregion

        #region [ Note Input Detail Infomation ]

        private void dataGridViewProducts1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            double nSumAmountCurrencyMain = 0;
            foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
            {
                if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                {
                    nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void dataGridViewProducts1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Control)
            {
                if (dataGridViewProducts1.CurrentCell.ColumnIndex == 1)
                {
                    FRM.List.FrmProductList Frm = new FRM.List.FrmProductList();
                    Frm.FlagView = true;
                    Frm.ShowDialog();
                    dataGridViewProducts1.CurrentCell.Value = Frm.mProductID;
                    dataGridViewProducts1.BeginEdit(true);
                }
            }
        }

        #endregion

        #region [ Vendor Infomation]
        private Customer_Info mVendor;
        private void txtVendorID_Leave(object sender, EventArgs e)
        {
            if (txtVendorID.Text.Trim() == "*")
            {
                mVendor = new Customer_Info();
                LoadDataToToolbox.ComboBoxData(CoVendors, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsVendor = 1 ", true);
            }
            else
            {
                mVendor = new Customer_Info(txtVendorID.Text.Trim());
                LoadInfoVendor();
                if (mVendor.Key == 0 && txtVendorID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoVendors, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsVendor = 1 AND CustomerID LIKE '" + txtVendorID.Text.Trim() + "%'", true);

            }
        }
        private void txtVendorID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                //FrmVendorList frm = new FrmVendorList();
                //frm.FlagView = true;
                //frm.ShowDialog();
                //if (frm.ListItemSelect != null)
                //    txtVendorID.Text = frm.ListItemSelect.Text;
                //frm.Close();
            }
        }
        private void CoVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nVendorKey;
            if (int.TryParse(CoVendors.SelectedValue.ToString(), out nVendorKey))
            {
                mVendor = new Customer_Info(nVendorKey);
                LoadInfoVendor();
            }

        }
        private void LoadInfoVendor()
        {
            if (mVendor.Key == 0)
            {
                CoVendors.Text = "...";
                //  txtAddress.Text = "...";
                //  txtDeliverer.Text = "..";
            }
            else
            {
                txtVendorID.Text = mVendor.ID.Trim();
                CoVendors.Text = mVendor.Name;
                //  txtAddress.Text =Vendor.Address + ", " +Vendor.City + ", " + Vendor.Country ;
                //  txtDeliverer.Text = "..";
            }
        }

        #endregion

        #region [ Function]
        private bool CheckBeforeSave()
        {
            txtNoteID.Focus();
            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerImportNoteDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerImportNoteDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

            if (txtNoteID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtNoteID.Focus();
                return false;
            }
            if (txtVendorID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã nhà cung cấp");
                txtVendorID.Focus();
                return false;
            }
            if (CoVendors.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên nhà cung cấp");
                CoVendors.Focus();
                return false;
            }
            if (dataGridViewProducts1.IsError)
            {
                MessageBox.Show("Vui lòng kiểm tra lại danh sách vật tư mua hàng");
                dataGridViewProducts1.Focus();
                return false;
            }
            return true;
        }

        private void ClearNoteInputInfo()
        {
            txtNoteID.Text = "";
            txtNoteName.Text = "";
            txtDeliverer.Text = "";
            txtVendorID.Text = "";
            CoVendors.Text = "";
            txtVendorID.Text = "";
            CoVendors.Text = "";
            txtDocumentAttached.Text = "";
            txtDocumentOrigin.Text = "";

            dataGridViewProducts1.ClearProducts();
            txtSumOrder.Text = "0";
        }
        private void DisplayForNoteTemp()
        {
            coWarehouse.Enabled = false;
            cmdSaveNew.Enabled = false;
        }
        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtSumOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        #endregion

        #region [ Action Button]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (SaveNoteInputInfomation())
            {
                if (!FlagTemp)
                {
                    mNoteInput.SaveObject();
                    this.Close();
                }
                else
                    this.Close();
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveNoteInputInfomation())
            {
                mNoteInput.SaveObject();

                string strID = mNoteInput.NoteID.Trim();

                mVendor = new Customer_Info();
                mNoteInput = new Stock_Note_Input_Object();

                ClearNoteInputInfo();
                txtNoteID.Focus();
                txtNoteID.Text = Stock_Note_Data.GetAutoNoteInputID(strID);
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa phiếu nhập này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                mNoteInput.DeleteObject();
                ClearNoteInputInfo();
                txtNoteID.Focus();
            }

        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmStockNoteInputReport frm = new FrmStockNoteInputReport();
            SaveNoteInputInfomation();
            mNoteInput.WarehouseName = coWarehouse.Text;
            frm.mNote_Input = mNoteInput;
            frm.ShowDialog();
        }

        #endregion

        #region [ Short Key ]
        private void FrmStockNoteInputDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("IVT002");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

    }
}