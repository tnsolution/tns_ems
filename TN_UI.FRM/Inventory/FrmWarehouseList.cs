using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNConfig;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.SYS.Forms;
using TNLibrary.IVT;
using TNLibrary.PUL;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmWarehouseList : Form
    {
        public bool FlagView;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmWarehouseList()
        {
            InitializeComponent();

        }

        private void FrmWarehouseList_Load(object sender, EventArgs e)
        {
            CheckRole();
            InitListView();
            panelSearch.Height = 0;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            LoadDataFields(ListViewData, Products_Data.WarehouseList());
        }

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        ListViewItem ListItemSelect;
        private void InitListView()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên kho ";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa chỉ";
            colHead.Width = 500;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            // ListViewData.SmallImageList = SmallImageLV;

            LVGroup = new ListViewMyGroup(ListViewData);

        }
        private void ListViewData_Leave(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListItemSelect = ListViewData.SelectedItems[0];
                if (ListItemSelect != null)
                {
                    ListItemSelect.BackColor = Color.Silver;
                }
            }
        }
        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListItemSelect != null)
            {
                ListItemSelect.BackColor = Color.White;
            }
        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        private void ListViewData_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                ListItemSelect = e.Item;
            }
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (FlagView)
            {
                this.Close();
            }
            else
            {
                int nWarehouseKey = int.Parse(ListItemSelect.Tag.ToString());
                FrmWarehouseDetail frm = new FrmWarehouseDetail();
                frm.mWarehouseKey = nWarehouseKey;
                frm.ShowDialog();

                LoadDataFields(ListViewData, Products_Data.WarehouseList());
                SelectIndexInListView(nWarehouseKey);
            }
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {

                if (ListViewData.SelectedItems.Count > 0)
                {

                    int nResult = Stock_Note_Data.CheckExistWarehouse((int)ListViewData.SelectedItems[0].Tag);
                    if (nResult > 0)
                    {
                        MessageBox.Show("Không thể xóa kho hàng này, vui lòng kiểm tra lại!");
                    }

                    else
                    {
                        if (MessageBox.Show("Bạn có muốn xóa kho hàng : " + ListViewData.SelectedItems[0].SubItems[1].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            int nIndex = ListViewData.SelectedItems[0].Index;
                            Warehouse_Info nWarehouse = new Warehouse_Info((int)ListViewData.SelectedItems[0].Tag);
                            nWarehouse.Delete();
                            ListViewData.SelectedItems[0].Remove();
                            if (ListViewData.Items.Count > 0)
                                if (nIndex == 0)
                                    ListViewData.Items[nIndex].Selected = true;
                                else
                                    ListViewData.Items[nIndex - 1].Selected = true;
                            ListViewData.Focus();
                        }
                    }
                }
                else
                    MessageBox.Show("Chọn kho hàng để xóa");
            }


        }


        // Load Database
        public void LoadDataFields(ListView LV, DataTable nListWarehouse)
        {

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = nListWarehouse.Rows.Count;
            int No = 0;
            for (int i = 0; i < n; i++)
            {
                No = i + 1;
                DataRow nRow = nListWarehouse.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = No.ToString();
                lvi.Tag = nRow["WarehouseKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["WarehouseName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Address"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

        }
        #endregion

        #region [ Activate on Button ]
        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            FrmWarehouseDetail frm = new FrmWarehouseDetail();
            frm.ShowDialog();

            DataTable nListWarehouse = Products_Data.WarehouseList();
            LoadDataFields(ListViewData, nListWarehouse);

        }
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            DataTable nListWarehouse = Products_Data.WarehouseList();

            LoadDataFields(ListViewData, nListWarehouse);

        }
        #endregion

        #region [ Function ]
        private void SelectIndexInListView(int WarehouseKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == WarehouseKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercurity ]
        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);

            nRole.Check_Role("IVT001");

            if (nRole.Del)
                m_RoleDelete = true;
        }
        #endregion
    }
}