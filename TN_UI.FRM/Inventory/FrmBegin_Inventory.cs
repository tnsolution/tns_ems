using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.IVT;
using TNLibrary.FNC;
using TNLibrary.FNC.CloseMonth;

using TNLibrary.PUL;
using TN_UI.FRM.Reports;


namespace TN_UI.FRM.Inventory
{
    public partial class FrmBegin_Inventory : Form
    {

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mDefaultAccountProduct = GlobalSystemConfig.AccountProduct;

        private string mCloseMonth = "";
        private bool mIsFirstTimeLoad = true;
        public FrmBegin_Inventory()
        {
            InitializeComponent();

        }
        private void FrmBegin_Inventory_Load(object sender, EventArgs e)
        {
            CheckRole();
            FinancialYear_Info nYear = new FinancialYear_Info(true);
            mCloseMonth = nYear.FromMonth;
            SetupLayoutGridView(dataGridView1);

            LoadDataToToolbox.ComboBoxData(coWarehouse, "SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse", false);
            LoadDataToToolbox.ComboBoxData(CoCategories, "SELECT CategoryKey,CategoryName FROM PUL_ProductCategories", true);

            panelSearch.Height = 00;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            this.WindowState = FormWindowState.Maximized;

            coWarehouse.SelectedIndex = -1;
            mIsFirstTimeLoad = false;
            coWarehouse.SelectedIndex = 0;

            crystalReportViewer1.Dock = DockStyle.Fill;
        }
        private void coWarehouse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!mIsFirstTimeLoad)
                SearchInventory();
        }
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            SearchInventory();
        }
        private void SearchInventory()
        {
            int nWarehouseKey = int.Parse(coWarehouse.SelectedValue.ToString());
            int nCategoryKey = int.Parse(CoCategories.SelectedValue.ToString());
            DataTable nTable = CloseMonth_Data.Inventorys(mCloseMonth, nWarehouseKey, nCategoryKey, txtSearchProductID.Text.Trim(), txtSearchProductName.Text.Trim());
            PopulateDataGridView(dataGridView1, nTable);
        }

        #region [ GridView ]
        private bool FlagEdit;
        private void SetupLayoutGridView(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ProductID", "Mã Sản phẩm");
            GV.Columns.Add("ProductName", "Tên Sản phẩm");
            GV.Columns.Add("Unit", "Đơn vị");
            GV.Columns.Add("Quantity", "Số lượng");
            GV.Columns.Add("CurrencyMain", "Thành tiền");
            DataGridViewImageColumn nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Delete";
            GV.Columns.Add(nColumn);

            GV.Columns[0].Width = 60;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[0].ReadOnly = true;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 300;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[3].ReadOnly = true;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[5].Width = 150;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 30;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            // setup style view

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            // GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * 3 / 2;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.Paint += new PaintEventHandler(GV_Paint);

            GV.UserAddedRow += new DataGridViewRowEventHandler(GV_UserAddedRow);
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.CellEndEdit += new DataGridViewCellEventHandler(GV_CellEndEdit);
            GV.RowValidated += new DataGridViewCellEventHandler(GV_RowValidated);
            GV.KeyDown += new KeyEventHandler(this.GV_KeyDown);
            GV.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(GV_EditingControlShowing);
            GV.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GV_CellMouseMove);
            GV.CellContentClick += new DataGridViewCellEventHandler(this.GV_CellContentClick);

            GV.Rows[0].Cells["No"].Value = 1;
            GV.Rows[0].Tag = 0;
            GV.Rows[0].Cells["Delete"].Value = ImageListGV.Images[0];
            if (!mRole.Edit)
                GV.ReadOnly = true;

        }
        private void GV_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;

            string[] TitleColumn = { "STT", " Mã Vật tư ", "Tên vật tư", "Đơn vị", "Số lượng", "Thành tiền (" + mCurrencyMain + ")", "" };

            int nColumnTotal = GV.ColumnCount;

            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;


                r1.Width = r1.Width - 2;
                r1.Height = r1.Height - 2;
                g.DrawImage(image, r1);


                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }

        private void PopulateDataGridView(DataGridView GV, DataTable TableView)
        {

            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];

                double BeginAmountDebitCurrencyMain = double.Parse(nRow["BeginAmountDebitCurrencyMain"].ToString());
                float BeginQuantity = float.Parse(nRow["BeginQuantity"].ToString());

                GV.Rows.Add();
                GV.Rows[i].Tag = (int)nRow["CloseAccountKey"];
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
                GV.Rows[i].Cells["ProductID"].Value = nRow["ProductID"].ToString().Trim();
                GV.Rows[i].Cells["ProductID"].Tag = (int)nRow["ProductKey"];
                GV.Rows[i].Cells["ProductName"].Value = nRow["ProductName"].ToString().Trim();

                GV.Rows[i].Cells["Unit"].Value = nRow["Unit"].ToString();
                GV.Rows[i].Cells["Quantity"].Value = BeginQuantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                GV.Rows[i].Cells["CurrencyMain"].Value = BeginAmountDebitCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["Delete"].Value = ImageListGV.Images[1];
            }
            GV.Rows[i].Cells["No"].Value = GV.Rows.Count;
            GV.Rows[i].Tag = 0;
            GV.Rows[i].Cells["Delete"].Value = ImageListGV.Images[0];

        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
            FlagEdit = false;
        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            FlagEdit = true;
            DataGridViewRow nRowEdit = dataGridView1.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 1: // thong tin ve san pham
                    if (nRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        nRowEdit.Cells["ProductID"].ErrorText = "";
                        nRowEdit.Cells["ProductID"].Tag = 0;
                    }
                    else
                    {
                        string nProductID = nRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;

                        Product_Info nProduct = new Product_Info(nProductID);
                        if (nProduct.Key > 0)
                        {
                            nRowEdit.Cells["ProductID"].Tag = nProduct.Key;
                            nRowEdit.Cells["ProductID"].ErrorText = "";
                            nRowEdit.Cells["ProductName"].Value = nProduct.Name;
                            nRowEdit.Cells["Unit"].Value = nProduct.Unit;

                        }
                        else
                        {
                            nRowEdit.Cells["ProductID"].Tag = 0;
                            nRowEdit.Cells["ProductID"].ErrorText = "Không có sản phẩm này";
                            nRowEdit.Cells["ProductName"].Value = "";
                            nRowEdit.Cells["Unit"].Value = "";
                        }
                    }
                    break;

                case 4:
                    float nQuantity = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (float.TryParse(nRowEdit.Cells["Quantity"].Value.ToString(), NumberStyles.Any, mFormatProviderDecimal, out nQuantity))
                        {
                            nRowEdit.Cells["Quantity"].Value = nQuantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                            nRowEdit.Cells["Quantity"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["Quantity"].ErrorText = "Sai định dạng số lượng";
                    }
                    break;
                case 5:
                    double nAmountCurrencyMain = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["CurrencyMain"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nAmountCurrencyMain))
                        {
                            nRowEdit.Cells["CurrencyMain"].Value = nAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["CurrencyMain"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["CurrencyMain"].ErrorText = "Sai định dạng tiền tệ";
                    }
                    break;
                default:
                    break;
            }

        }
        private void GV_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["No"].Value = dataGridView1.Rows.Count;
            e.Row.Tag = 0;
            e.Row.Cells["Delete"].Value = ImageListGV.Images[0];
        }
        private void GV_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowCurrent = dataGridView1.Rows[e.RowIndex];

            nRowCurrent.DefaultCellStyle.BackColor = Color.White;
            if (FlagEdit)
            {
                if (CheckRow(nRowCurrent))
                {
                    CloseMonth_Inventory_Info nInventory = new CloseMonth_Inventory_Info((int)nRowCurrent.Tag);

                    nInventory.CloseMonth = mCloseMonth;
                    nInventory.ProductKey = (int)nRowCurrent.Cells["ProductID"].Tag;
                    nInventory.WarehouseKey = (int)coWarehouse.SelectedValue;
                    nInventory.BeginQuantity = float.Parse(nRowCurrent.Cells["Quantity"].Value.ToString(), mFormatProviderDecimal);
                    nInventory.BeginAmountDebitCurrencyMain = double.Parse(nRowCurrent.Cells["CurrencyMain"].Value.ToString(), mFormatProviderCurrency);

                    nInventory.Save();
                    //MessageBox.Show(nInventory.Message);
                    nRowCurrent.Tag = nInventory.CloseAccountKey;
                    if (nInventory.Message.Contains("Cannot insert duplicate key"))
                    {
                        nRowCurrent.Cells["ProductID"].ErrorText = "Error";

                        MessageBox.Show("Đã có hàng tồn kho này rồi");
                    }
                }
                nRowCurrent.Cells["Delete"].Value = ImageListGV.Images[1];

            }
        }
        private void GV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteRow();
            }
        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (dataGridView1.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProductID FROM PUL_Products");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;

                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                default:

                    break;
            }
        }
        private void GV_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 6)
                Cursor = Cursors.Hand;
            else
                Cursor = Cursors.Default;
        }
        private void GV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
                DeleteRow();
        }

        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }

        private bool CheckRow(DataGridViewRow RowCheck)
        {
            bool nResult = true;
            if (RowCheck.Cells["ProductID"].Value == null || RowCheck.Cells["ProductID"].Tag == null || (int)RowCheck.Cells["ProductID"].Tag == 0)
            {
                RowCheck.Cells["ProductID"].ErrorText = "Vui lòng kiểm tra sản phẩm";
                nResult = false;
            }

            if (RowCheck.Cells["Quantity"].ErrorText.Trim().Length > 0)
            {
                nResult = false;
            }
            if (RowCheck.Cells["Quantity"].Value == null)
            {
                RowCheck.Cells["Quantity"].ErrorText = "Vui lòng kiểm tra số lượng";
                nResult = false;
            }
            if (RowCheck.Cells["CurrencyMain"].Value == null)
            {
                RowCheck.Cells["CurrencyMain"].Value = 0;
            }
            return nResult;
        }
        private void DeleteRow()
        {
            if (!mRole.Del)
                return;

            int nRowIndexDel = dataGridView1.CurrentRow.Index;
            if (!dataGridView1.CurrentRow.IsNewRow)
            {
                if (dataGridView1.SelectedCells.Count > 0)//
                {
                    if ((int)dataGridView1.CurrentRow.Tag > 0)
                    {
                        if (MessageBox.Show("Bạn có muốn xóa dòng này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {

                            CloseMonth_Inventory_Info nCloseAccount = new CloseMonth_Inventory_Info();
                            nCloseAccount.CloseAccountKey = (int)dataGridView1.CurrentRow.Tag;
                            nCloseAccount.Delete();

                            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                            for (int i = nRowIndexDel; i < dataGridView1.Rows.Count; i++)
                            {
                                dataGridView1.Rows[i].Cells["No"].Value = i + 1;
                            }
                        }
                    }
                    else
                    {
                        dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                        for (int i = nRowIndexDel; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].Cells["No"].Value = i + 1;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Bạn chọn sản phẩm để xóa");
            }
        }
        #endregion

        #region [ Design Layout]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }

        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 65;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("IVT004");

        }
        #endregion

        #region [Report]
        private void PopulateCrystalReport(DataTable nTable)
        {
            ReportDocument rpt = new ReportDocument();
            rpt.Load("FilesReport\\HangTKDK.rpt");

            DataTable nTableDetail = TableReport.TableHangTKDKDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();
            int i = 0;
            foreach (DataRow nRow in nTable.Rows)
            {
                i++;
                double BeginAmountDebitCurrencyMain = double.Parse(nRow["BeginAmountDebitCurrencyMain"].ToString());
                float BeginQuantity = float.Parse(nRow["BeginQuantity"].ToString());

                DataRow nRowDetail = nTableDetail.NewRow();
                nRowDetail["No"] = i.ToString();
                nRowDetail["ProductID"] = nRow["ProductID"].ToString();
                nRowDetail["ProductName"] = nRow["ProductName"].ToString().Trim();
                nRowDetail["Unit"] = nRow["Unit"].ToString().Trim();
                nRowDetail["BeginQuantity"] = BeginQuantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRowDetail["BeginAmountDebitCurrencyMain"] = BeginAmountDebitCurrencyMain.ToString(mFormatDecimal, mFormatProviderDecimal); ;

                nTableDetail.Rows.Add(nRowDetail);

            }

            rpt.Database.Tables["TableHangTKDKDetail"].SetDataSource(nTableDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Zoom(1);

        }

        private void radioGridView_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            crystalReportViewer1.Visible = false;
            int nWarehouseKey = int.Parse(coWarehouse.SelectedValue.ToString());
            int nCategoryKey = int.Parse(CoCategories.SelectedValue.ToString());
            DataTable nTable = CloseMonth_Data.Inventorys(mCloseMonth, nWarehouseKey, nCategoryKey, txtSearchProductID.Text.Trim(), txtSearchProductName.Text.Trim());
            PopulateDataGridView(dataGridView1, nTable);
        }

        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            crystalReportViewer1.Visible = true;
            int nWarehouseKey = int.Parse(coWarehouse.SelectedValue.ToString());
            int nCategoryKey = int.Parse(CoCategories.SelectedValue.ToString());
            DataTable nTable = CloseMonth_Data.Inventorys(mCloseMonth, nWarehouseKey, nCategoryKey, txtSearchProductID.Text.Trim(), txtSearchProductName.Text.Trim());
            PopulateCrystalReport(nTable);
        }
        #endregion
    }
}