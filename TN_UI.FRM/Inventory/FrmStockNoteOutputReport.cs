using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


using TNLibrary.SYS;
using TNLibrary.IVT;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmStockNoteOutputReport : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        public Stock_Note_Output_Object mNote_Output;

        public FrmStockNoteOutputReport()
        {
            InitializeComponent();
            
        }

        private void FrmStockNoteOutputReport_Load(object sender, EventArgs e)
        {

            SetupCrystalReport();
            PopulateCrystalReport();
        }
    
        
        #region [ CrystalReport]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {

            ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\PhieuXuatKho.rpt");

            DataTable nTableTitle = TableReport.TablePhieuXuatKhoTieuDe();
            DataTable nTableDetail = TableReport.TablePhieuXuatKhoDetail();
            DataTable nTableTencongty = TableReport.CompanyInfo();


            DataRow nRow = nTableTitle.NewRow();
            nRow["BoPhan"] = " ";
            nRow["NgayXuat"] = mNote_Output.NoteDate.ToString(mFormatDate);
            nRow["No"] = "";
            nRow["Co"] = "";
            nRow["So"] = mNote_Output.NoteID;
            nRow["TenNguoiNhan"] = mNote_Output.Deliverer;
            nRow["LyDoXuat"] = mNote_Output.NoteDescription;
            //nRow["TheoChungTu"] = mNote_Output.DocumentAttached;
            nRow["XuatTaiKho"] = mNote_Output.WarehouseName;
            nRow["DiaDiem"] = "";
            nRow["TongThanhTien"] = mNote_Output.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency) ;
            nRow["TongTienBangChu"] = "  ";
            nRow["ChungTuGoc"] = mNote_Output.DocumentAttached;
            nRow["NgayVietPhieuXuat"] = mNote_Output.NoteDate;

            nTableTitle.Rows.Add(nRow);

            int No = 1;
            foreach (InventoryProduct nProduct in mNote_Output.ProductsOutput)
            {
                nRow = nTableDetail.NewRow();
                nRow["STT"] = No.ToString();
                nRow["TenSP"] = nProduct.Name;
                nRow["Ma"] = nProduct.ID;
                nRow["DonViTinh"] = nProduct.Unit;
                nRow["YeuCauXuat"] = nProduct.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRow["ThucXuat"] = nProduct.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRow["DonGia"] = nProduct.UnitPrice.ToString(mFormatCurrencyMainTwoPoint,mFormatProviderCurrency);
                nRow["ThanhTien"] = nProduct.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

                nTableDetail.Rows.Add(nRow);
                No++;
            }


            // thay doi ten table

            rptTam.Database.Tables["TablePhieuXuatKhoTieuDe"].SetDataSource(nTableTitle);
            rptTam.Database.Tables["TablePhieuXuatKhoDetail"].SetDataSource(nTableDetail);
            rptTam.Database.Tables["TableTencongty"].SetDataSource(nTableTencongty);

            crystalReportViewer1.ReportSource = rptTam;

            // thay doi do zoom
            crystalReportViewer1.Zoom(1);

        }
        #endregion

       

    }
}