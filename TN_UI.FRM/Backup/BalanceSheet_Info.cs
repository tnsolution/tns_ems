﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT
{
    public class BalanceSheet_Info
    {

        private int m_BalanceSheetKey = 0;
        private string m_BalanceSheetID = "";
        private string m_BalanceSheetNameVN = "";
        private string m_BalanceSheetNameEN = "";
        private string m_BalanceSheetNameCN = "";

        private double m_AmountBeginCurrencyMain = 0; 
        private double m_AmountEndCurrencyMain = 0;
        private int m_Level = 0;

        private bool m_IsBold;
        private bool m_IsCenter;

        private DataTable m_BalanceDetails = new DataTable();
        private string m_Message = "";
        
        public BalanceSheet_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region [ Constructor Get Information ]
        public BalanceSheet_Info(int BalanceSheetKey)
        {
            string nSQL = "SELECT * FROM ACT_ReportBalanceSheet WHERE BalanceSheetKey =@BalanceSheetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = BalanceSheetKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_BalanceSheetKey = int.Parse(nReader["BalanceSheetKey"].ToString());
                    m_BalanceSheetID = nReader["BalanceSheetID"].ToString().Trim();
                    m_BalanceSheetNameVN = nReader["BalanceSheetNameVN"].ToString();
                    m_BalanceSheetNameEN = nReader["BalanceSheetNameEN"].ToString();
                    m_BalanceSheetNameCN = nReader["BalanceSheetNameCN"].ToString();

                    m_AmountBeginCurrencyMain = double.Parse(nReader["AmountBeginCurrencyMain"].ToString());
                    m_AmountEndCurrencyMain = double.Parse(nReader["AmountEndCurrencyMain"].ToString());
               
                    m_Level = int.Parse(nReader["Level"].ToString());
                    m_IsBold = bool.Parse(nReader["IsBold"].ToString());
                    m_IsCenter = bool.Parse(nReader["IsCenter"].ToString());
               
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            if (m_BalanceSheetKey > 0)
                GetDetails();
        }
        public BalanceSheet_Info(string BalanceSheetID)
        {
            string nSQL = "SELECT * FROM ACT_ReportBalanceSheet WHERE BalanceSheetID =@BalanceSheetID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetID", SqlDbType.NVarChar).Value = BalanceSheetID;

                SqlDataReader nReader = nCommand.ExecuteReader();

                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_BalanceSheetKey = int.Parse(nReader["BalanceSheetKey"].ToString());
                    m_BalanceSheetID = nReader["BalanceSheetID"].ToString().Trim();
                    m_BalanceSheetNameVN = nReader["BalanceSheetNameVN"].ToString();
                    m_BalanceSheetNameEN = nReader["BalanceSheetNameEN"].ToString();
                    m_BalanceSheetNameCN = nReader["BalanceSheetNameCN"].ToString();
                    m_AmountBeginCurrencyMain = int.Parse(nReader["AmountBeginCurrencyMain"].ToString());
                    m_AmountEndCurrencyMain = int.Parse(nReader["AmountEndCurrencyMain"].ToString());

                    m_Level = int.Parse(nReader["Level"].ToString());
                    m_IsBold = bool.Parse(nReader["IsBold"].ToString());
                    m_IsCenter = bool.Parse(nReader["IsCenter"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            if (m_BalanceSheetKey > 0)
                GetDetails();
        }
        #endregion

        #region [ Properties ]
        // Info Account
        public int Key
        {
            get { return m_BalanceSheetKey; }
            set { m_BalanceSheetKey = value; }
        }
        public string ID
        {
            get { return m_BalanceSheetID; }
            set { m_BalanceSheetID = value; }
        }
        public string NameVN
        {
            get { return m_BalanceSheetNameVN; }
            set { m_BalanceSheetNameVN = value; }
        }
        public string NameEN
        {
            get { return m_BalanceSheetNameEN; }
            set { m_BalanceSheetNameEN = value; }
        }
        public string NameCN
        {
            get { return m_BalanceSheetNameCN; }
            set { m_BalanceSheetNameCN = value; }
        }

        public double AmountBeginCurrencyMain
        {
            get { return m_AmountBeginCurrencyMain; }
            set { m_AmountBeginCurrencyMain = value; }
        }
        public double AmountEndCurrencyMain
        {
            get { return m_AmountEndCurrencyMain; }
            set { m_AmountEndCurrencyMain = value; }
        }
        public int Level
        {
            get { return m_Level; }
            set { m_Level = value; }
        }
       
        public bool IsBold
        {
            get { return m_IsBold; }
            set { m_IsBold = value; }
        }
        public bool IsCenter
        {
            get { return m_IsCenter; }
            set { m_IsCenter = value; }
        }

        public DataTable BalanceDetails
        {
            get { return m_BalanceDetails; }
        }
        #endregion

        #region [ Getdetail ]
        public void GetDetails()
        {
            string nSQL = "SELECT * FROM ACT_ReportBalanceSheetDetails WHERE BalanceSheetKey = @BalanceSheetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = m_BalanceSheetKey;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(m_BalanceDetails);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

    }
}
