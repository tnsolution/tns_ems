﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
using TNLibrary.SYS;

namespace TNLibrary.ACT
{
    public class Accounts_Data
    {
        public static DataTable AccountByParent(int Parent)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT AccountKey,AccountID,AccountNameVN FROM ACT_AccountTable "
                        + " WHERE Parent=@Parent ORDER BY AccountID";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static string UpdateParent(int KeyMove, int KeyTo, int LevelHigh)
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE ACT_AccountTable SET Parent = @Parent, LevelHigh=@LevelHigh WHERE AccountKey = @AccountKey";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = KeyMove;
                nCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = KeyTo;
                nCommand.Parameters.Add("@LevelHigh", SqlDbType.Int).Value = LevelHigh;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static string UpdateHasChild(int Key, bool Status)
        {
            string nResult = "";

            string nSQL = "UPDATE ACT_AccountTable SET IsAccountParent = @IsAccountParent WHERE AccountKey = @AccountKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = Key;
                nCommand.Parameters.Add("@IsAccountParent", SqlDbType.Bit).Value = Status;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public static ArrayList[] GetAllAccount()
        {
            string nSQL = "SELECT * FROM ACT_AccountTable ORDER BY AccountID ASC";
            ArrayList[] AccountTable = new ArrayList[10];
            // ListItem Account = new ListItem();
            string m_AccountID, m_AccountName;
            int m_AccountKey, m_Parent;

            for (int i = 0; i < 10; i++)
            {
                AccountTable[i] = new ArrayList();
            }

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    while (nReader.Read())
                    {

                        m_AccountKey = int.Parse(nReader["AccountKey"].ToString());
                        m_AccountID = nReader["AccountID"].ToString().Trim();
                        m_AccountName = nReader["AccountNameVN"].ToString();

                        m_Parent = int.Parse(nReader["Parent"].ToString());

                        //m_ReadOnly = bool.Parse(nReader["ReadOnly"].ToString());

                        TN_Item Item = new TN_Item();
                        Item.ID = m_AccountID;
                        Item.Name = m_AccountName;
                        Item.Value = m_AccountKey;
                        int Index;
                        if (int.TryParse(Item.ID.Substring(0, 1), out Index))
                            AccountTable[Index].Add(Item);
                    }
                }
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return AccountTable;
        }

        public static int CheckExistAccount(int AccountKey)
        {
            int nResult = 0;

            string nSQL = "FNC_CheckExistAccount";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            try
            {
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = AccountKey;
                string strAmount = nCommand.ExecuteScalar().ToString();
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();
                return nResult;
            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
    }
}
