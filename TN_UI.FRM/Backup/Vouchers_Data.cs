﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT
{
    public class Vouchers_Data
    {
        #region [Config]
        public static DataTable BalanceSheetTable()
        {
            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM ACT_ReportBalanceSheet ORDER BY BalanceSheetKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        #endregion
    }
}
