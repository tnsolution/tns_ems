﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT.CloseMonth
{
    public class CloseMonth_Account_Info : CloseAccount
    {       
        public CloseMonth_Account_Info()
        {
        }
        #region [ Constructor Get Information ]
        public CloseMonth_Account_Info(int CloseAccountKey)
        {

            string nSQL = " SELECT A.*,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_Accounts A "
                        + " INNER JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                        + " WHERE CloseAccountKey = @CloseAccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.Int).Value = CloseAccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        public CloseMonth_Account_Info(string CloseMonth, int AccountKey)
        {
            base.CloseMonth = CloseMonth;
            base.AccountKey = AccountKey;

            string nSQL = " SELECT A.*,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_Accounts A "
                        + " INNER JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                        + " WHERE CloseMonth=@CloseMonth "
                                  + " AND A.AccountKey = @AccountKey  ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = AccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public CloseMonth_Account_Info(string CloseMonth, string AccountID)
        {

            string nSQL = "SELECT A.*,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_Accounts A "
                            + " INNER JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                            + " WHERE A.CloseMonth = @CloseMonth  "
                            + " AND C.AccountID = @AccountID ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AccountID;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AccountID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            string nResult = "";
            string nSQL  = "INSERT INTO ACT_CloseMonth_Accounts( "
                    + " CloseMonth, AccountKey,"
                    + " BeginAmountDebitCurrencyMain ,BeginAmountCreditCurrencyMain , BeginAmountDebitCurrencyForeign ,BeginAmountCreditCurrencyForeign, "
                    + " MiddleAmountDebitCurrencyMain,MiddleAmountCreditCurrencyMain, MiddleAmountDebitCurrencyForeign,MiddleAmountCreditCurrencyForeign, "
                    + " EndAmountDebitCurrencyMain   ,EndAmountCreditCurrencyMain   , EndAmountDebitCurrencyForeign   ,EndAmountCreditCurrencyForeign)"
                    + " VALUES(@CloseMonth, @AccountKey, "
                    + " @BeginAmountDebitCurrencyMain ,@BeginAmountCreditCurrencyMain , @BeginAmountDebitCurrencyForeign , @BeginAmountCreditCurrencyForeign, "
                    + " @MiddleAmountDebitCurrencyMain,@MiddleAmountCreditCurrencyMain, @MiddleAmountDebitCurrencyForeign, @MiddleAmountCreditCurrencyForeign, "
                    + " @EndAmountDebitCurrencyMain   ,@EndAmountCreditCurrencyMain   , @EndAmountDebitCurrencyForeign   , @EndAmountCreditCurrencyForeign)"
                    + " SELECT CloseAccountKey FROM ACT_CloseMonth_Accounts WHERE CloseAccountKey = SCOPE_IDENTITY()";
            

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;

                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;

                nResult = nCommand.ExecuteScalar().ToString();
                int nCloseAccountKey = 0;
                int.TryParse(nResult, out nCloseAccountKey);
                base.CloseAccountKey = nCloseAccountKey;


                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Update()
        {
            string nResult = "";

            string nSQL = "";
            nSQL = "UPDATE ACT_CloseMonth_Accounts SET "
                        + " AccountKey = @AccountKey, "
                        + " CloseMonth=@CloseMonth ,"

                        + " BeginAmountDebitCurrencyMain = @BeginAmountDebitCurrencyMain,"
                        + " BeginAmountCreditCurrencyMain = @BeginAmountCreditCurrencyMain, "
                        + " BeginAmountDebitCurrencyForeign = @BeginAmountDebitCurrencyForeign, "
                        + " BeginAmountCreditCurrencyForeign = @BeginAmountCreditCurrencyForeign, "

                        + " MiddleAmountDebitCurrencyMain = @MiddleAmountDebitCurrencyMain,"
                        + " MiddleAmountCreditCurrencyMain = @MiddleAmountCreditCurrencyMain, "
                        + " MiddleAmountDebitCurrencyForeign = @MiddleAmountDebitCurrencyForeign, "
                        + " MiddleAmountCreditCurrencyForeign = @MiddleAmountCreditCurrencyForeign, "

                        + " EndAmountDebitCurrencyMain = @EndAmountDebitCurrencyMain,"
                        + " EndAmountCreditCurrencyMain = @EndAmountCreditCurrencyMain, "
                        + " EndAmountDebitCurrencyForeign = @EndAmountDebitCurrencyForeign, "
                        + " EndAmountCreditCurrencyForeign = @EndAmountCreditCurrencyForeign "

                        + " WHERE CloseAccountKey = @CloseAccountKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;

                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;


                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            return nResult;
        }
        public string Save()
        {
            if (base.CloseAccountKey ==0 )
                return Create();
            else
                return Update();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = " DELETE FROM ACT_CloseMonth_Accounts "
                        + " WHERE CloseAccountKey = @CloseAccountKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
