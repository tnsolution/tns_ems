﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.ACT
{
    public class Account_Info
    {

        private int m_AccountKey = 0;
        private string m_AccountID = "";
        private string m_AccountNameVN = "";
        private string m_AccountNameEN = "";
        private string m_AccountNameCN = "";

        private string m_CurrencyID = "VND";
        private int m_AccountStyle = 0; //  no , co , luong tinh
        private int m_Parent = 0;
        private int m_LevelHigh = 0;
        private string m_Notes = "";
        private int m_CategoryKey = 0;
        // thong so quan ly TSCD
        private bool m_IsFixedAsset;
        private bool m_IsDepreciation;
        private bool m_IsDepreciationExpense;

        private bool m_IsInterestExpense;

        // thong so quan ly cau hinh
        private bool m_IsDetail;
        private bool m_IsAccountParent;
        private bool m_IsReadOnly;

        private string m_Message = "";
        public Account_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region [ Constructor Get Information ]
        public Account_Info(int AccountKey)
        {
            string nSQL = "SELECT * FROM ACT_AccountTable WHERE AccountKey =@AccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = AccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    m_AccountID = nReader["AccountID"].ToString().Trim();
                    m_AccountNameVN = nReader["AccountNameVN"].ToString();
                    m_AccountNameEN = nReader["AccountNameEN"].ToString();
                    m_AccountNameCN = nReader["AccountNameCN"].ToString();

                    m_CurrencyID = nReader["CurrencyID"].ToString();
                    m_AccountStyle = int.Parse(nReader["AccountStyle"].ToString());
                    m_Parent = int.Parse(nReader["Parent"].ToString());
                    m_LevelHigh = int.Parse(nReader["LevelHigh"].ToString());
                    m_Notes = nReader["Notes"].ToString();
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());

                    // thong so theo doi loai tai san
                    m_IsFixedAsset = bool.Parse(nReader["IsFixedAsset"].ToString());
                    m_IsDepreciation = bool.Parse(nReader["IsDepreciation"].ToString());
                    m_IsDepreciationExpense = bool.Parse(nReader["IsDepreciationExpense"].ToString());

                    m_IsInterestExpense = bool.Parse(nReader["IsInterestExpense"].ToString());

                    // co thong so quan ly
                    m_IsDetail = bool.Parse(nReader["IsDetail"].ToString());
                    m_IsAccountParent = bool.Parse(nReader["IsAccountParent"].ToString());
                    m_IsReadOnly = bool.Parse(nReader["IsReadOnly"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Account_Info(string AccountID)
        {
            string nSQL = "SELECT * FROM ACT_AccountTable WHERE AccountID =@AccountID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AccountID;

                SqlDataReader nReader = nCommand.ExecuteReader();

                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    m_AccountID = nReader["AccountID"].ToString().Trim();
                    m_AccountNameVN = nReader["AccountNameVN"].ToString();
                    m_AccountNameEN = nReader["AccountNameEN"].ToString();
                    m_AccountNameCN = nReader["AccountNameCN"].ToString();

                    m_CurrencyID = nReader["CurrencyID"].ToString();
                    m_AccountStyle = int.Parse(nReader["AccountStyle"].ToString());
                    m_Parent = int.Parse(nReader["Parent"].ToString());
                    m_LevelHigh = int.Parse(nReader["LevelHigh"].ToString());
                    m_Notes = nReader["Notes"].ToString();
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
                    // thong so theo doi loai tai san
                    m_IsFixedAsset = bool.Parse(nReader["IsFixedAsset"].ToString());
                    m_IsDepreciation = bool.Parse(nReader["IsDepreciation"].ToString());
                    m_IsDepreciationExpense = bool.Parse(nReader["IsDepreciationExpense"].ToString());

                    m_IsInterestExpense = bool.Parse(nReader["IsInterestExpense"].ToString());

                    // co thong so quan ly
                    m_IsDetail = bool.Parse(nReader["IsDetail"].ToString());
                    m_IsAccountParent = bool.Parse(nReader["IsAccountParent"].ToString());
                    m_IsReadOnly = bool.Parse(nReader["IsReadOnly"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

        #region [ Properties ]
        // Info Account
        public int Key
        {
            get { return m_AccountKey; }
            set { m_AccountKey = value; }
        }
        public string ID
        {
            get { return m_AccountID; }
            set { m_AccountID = value; }
        }
        public string NameVN
        {
            get { return m_AccountNameVN; }
            set { m_AccountNameVN = value; }
        }
        public string NameEN
        {
            get { return m_AccountNameEN; }
            set { m_AccountNameEN = value; }
        }
        public string NameCN
        {
            get { return m_AccountNameCN; }
            set { m_AccountNameCN = value; }
        }

        public string CurrencyID
        {
            get { return m_CurrencyID; }
            set { m_CurrencyID = value; }
        }
        public int AccountStyle
        {
            get { return m_AccountStyle; }
            set { m_AccountStyle = value; }
        }
        public int Parent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }
        public int LevelHigh
        {
            get { return m_LevelHigh; }
            set { m_LevelHigh = value; }
        }
        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }
        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }
        public bool IsFixedAsset
        {
            get { return m_IsFixedAsset; }
            set { m_IsFixedAsset = value; }
        }
        public bool IsDepreciation
        {
            get { return m_IsDepreciation; }
            set { m_IsDepreciation = value; }
        }
        public bool IsDepreciationExpense
        {
            get { return m_IsDepreciationExpense; }
            set { m_IsDepreciationExpense = value; }
        }

        public bool IsInterestExpense
        {
            get { return m_IsInterestExpense; }
            set { m_IsInterestExpense = value; }
        }

        // thong so quan  ly cau hinh 

        public bool IsDetail
        {
            get { return m_IsDetail; }
            set { m_IsDetail = value; }
        }
        public bool IsAccountParent
        {
            get { return m_IsAccountParent; }
            set { m_IsAccountParent = value; }
        }
        public bool IsReadOnly
        {
            get { return m_IsReadOnly; }
            set { m_IsReadOnly = value; }
        }

        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE ACT_AccountTable SET "
                        + " AccountID = @AccountID,"
                        + " AccountNameVN = @AccountNameVN ,"
                        + " AccountNameEN = @AccountNameEN, "
                        + " AccountNameCN = @AccountNameCN, "
                        + " CurrencyID = @CurrencyID,"
                        + " AccountStyle= @AccountStyle,"
                        + " Parent = @Parent, "
                        + " LevelHigh = @LevelHigh, "
                        + " Notes = @Notes, "
                        + " CategoryKey = @CategoryKey, "

                        + " IsFixedAsset = @IsFixedAsset, "
                        + " IsDepreciation = @IsDepreciation, "
                        + " IsDepreciationExpense = @IsDepreciationExpense, "

                        + " IsInterestExpense = @IsInterestExpense, "

                        + " IsDetail = @IsDetail, "
                        + " IsAccountParent = @IsAccountParent, "
                        + " IsReadOnly = @IsReadOnly "


                        + " WHERE AccountKey = @AccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                // thong so tai khoan
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = m_AccountKey;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = m_AccountID;
                nCommand.Parameters.Add("@AccountNameVN", SqlDbType.NVarChar).Value = m_AccountNameVN;
                nCommand.Parameters.Add("@AccountNameEN", SqlDbType.NVarChar).Value = m_AccountNameEN;
                nCommand.Parameters.Add("@AccountNameCN", SqlDbType.NVarChar).Value = m_AccountNameCN;

                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NChar).Value = m_CurrencyID;
                nCommand.Parameters.Add("@AccountStyle", SqlDbType.Int).Value = m_AccountStyle;
                nCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = m_Parent;
                nCommand.Parameters.Add("@LevelHigh", SqlDbType.Int).Value = m_LevelHigh;

                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@IsFixedAsset", SqlDbType.Bit).Value = m_IsFixedAsset;
                nCommand.Parameters.Add("@IsDepreciation", SqlDbType.Bit).Value = m_IsDepreciation;
                nCommand.Parameters.Add("@IsDepreciationExpense", SqlDbType.Bit).Value = m_IsDepreciationExpense;

                nCommand.Parameters.Add("@IsInterestExpense", SqlDbType.Bit).Value = m_IsInterestExpense;

                //thong so quan ly cau hinh
                nCommand.Parameters.Add("@IsDetail", SqlDbType.Bit).Value = m_IsDetail;
                nCommand.Parameters.Add("@IsAccountParent", SqlDbType.Bit).Value = m_IsAccountParent;
                nCommand.Parameters.Add("@IsReadOnly", SqlDbType.Bit).Value = m_IsReadOnly;


                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO ACT_AccountTable( "
                        + " AccountID, AccountNameVN, AccountNameEN, AccountNameCN, Parent, Notes,CategoryKey,  AccountStyle, CurrencyID, "
                        + " IsDetail,IsInterestExpense,LevelHigh,IsAccountParent, IsReadOnly, IsFixedAsset,IsDepreciation,IsDepreciationExpense)"
                        + " VALUES(@AccountID, @AccountNameVN, @AccountNameEN, @AccountNameCN, @Parent, @Notes,@CategoryKey,  @AccountStyle,@CurrencyID, "
                        + " @IsDetail,@IsInterestExpense, @LevelHigh,@IsAccountParent, @IsReadOnly, @IsFixedAsset,@IsDepreciation,@IsDepreciationExpense)"
                        + " SELECT AccountKey FROM ACT_AccountTable WHERE AccountKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = m_AccountID;
                nCommand.Parameters.Add("@AccountNameVN", SqlDbType.NVarChar).Value = m_AccountNameVN;
                nCommand.Parameters.Add("@AccountNameEN", SqlDbType.NVarChar).Value = m_AccountNameEN;
                nCommand.Parameters.Add("@AccountNameCN", SqlDbType.NVarChar).Value = m_AccountNameCN;
                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@AccountStyle", SqlDbType.Int).Value = m_AccountStyle;
                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NChar).Value = m_CurrencyID;

                //thong so quan ly cau hinh
                nCommand.Parameters.Add("@IsDetail", SqlDbType.Bit).Value = m_IsDetail;
                nCommand.Parameters.Add("@IsInterestExpense", SqlDbType.Bit).Value = m_IsInterestExpense;
                nCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = m_Parent;
                nCommand.Parameters.Add("@LevelHigh", SqlDbType.Int).Value = m_LevelHigh;
                nCommand.Parameters.Add("@IsAccountParent", SqlDbType.Bit).Value = 0;
                nCommand.Parameters.Add("@IsReadOnly", SqlDbType.Bit).Value = m_IsReadOnly;

                nCommand.Parameters.Add("@IsFixedAsset", SqlDbType.Bit).Value = m_IsFixedAsset;
                nCommand.Parameters.Add("@IsDepreciation", SqlDbType.Bit).Value = m_IsDepreciation;
                nCommand.Parameters.Add("@IsDepreciationExpense", SqlDbType.Bit).Value = m_IsDepreciationExpense;

                nResult = nCommand.ExecuteScalar().ToString();

                m_AccountKey = int.Parse(nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            if (m_AccountKey > 0)

                return Update();

            else
                return Create();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM ACT_AccountTable WHERE AccountKey = @AccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = m_AccountKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

    }
}
