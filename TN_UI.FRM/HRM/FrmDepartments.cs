﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS.Users;

namespace TN_UI.FRM.HRM
{
    public partial class FrmDepartments : Form
    {
        public FrmDepartments()
        {
            InitializeComponent();
        }

        private void FrmDepartments_Load(object sender, EventArgs e)
        {
            tnTreeView1.RootName = "Cơ cấu Tổ chức - Phòng ban";
            tnTreeView1.Table = "SYS_Departments";
            tnTreeView1.FieldDisplay = "DepartmentName";
            tnTreeView1.FieldValue = "DepartmentKey";
            tnTreeView1.Initialize();
            CheckRole();

        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM001");
            if (!nRole.Edit)
            {
                tnTreeView1.SetCmdEdit = false;
                tnTreeView1.SetCmdNew = false;
            }
            if (!nRole.Del)
                tnTreeView1.SetCmdDel = false;

        }
    }
}
