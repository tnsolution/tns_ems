﻿namespace TN_UI.FRM.HRM
{
    partial class FrmDepartments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tnTreeView1 = new TNLibrary.SYS.Forms.TNTreeView();
            this.SuspendLayout();
            // 
            // tnTreeView1
            // 
            this.tnTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tnTreeView1.Location = new System.Drawing.Point(0, 0);
            this.tnTreeView1.Name = "tnTreeView1";
            this.tnTreeView1.Size = new System.Drawing.Size(460, 327);
            this.tnTreeView1.TabIndex = 0;
            // 
            // FrmDepartments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 327);
            this.Controls.Add(this.tnTreeView1);
            this.Name = "FrmDepartments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cơ cấu tổ chức";
            this.Load += new System.EventHandler(this.FrmDepartments_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private TNLibrary.SYS.Forms.TNTreeView tnTreeView1;
    }
}