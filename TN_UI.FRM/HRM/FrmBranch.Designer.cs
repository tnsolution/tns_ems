﻿namespace TN_UI.FRM.HRM
{
    partial class FrmBranch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tnGridView1 = new TNLibrary.SYS.Forms.TNGridView();
            this.SuspendLayout();
            // 
            // tnGridView1
            // 
            this.tnGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tnGridView1.Location = new System.Drawing.Point(0, 0);
            this.tnGridView1.Name = "tnGridView1";
            this.tnGridView1.Size = new System.Drawing.Size(430, 287);
            this.tnGridView1.TabIndex = 0;
            // 
            // FrmBranch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 287);
            this.Controls.Add(this.tnGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBranch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Chi nhánh - Văn phòng";
            this.Load += new System.EventHandler(this.FrmBranch_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private TNLibrary.SYS.Forms.TNGridView tnGridView1;
    }
}