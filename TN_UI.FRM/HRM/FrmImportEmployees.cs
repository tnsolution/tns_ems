using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Threading;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.HRM;

namespace TN_UI.FRM.HRM
{
    public partial class FrmImportEmployees : Form
    {
        //public int mEmployeeKey = 0;

        private string mFileNameImport = "";
        private DataTable mDataExcel = new DataTable();
        private Thread mThreadExcel;
        private int mIndexExcel = 0;

        private Thread mThreadSQL;
        private int mIndexSQL = 0;

        public FrmImportEmployees()
        {
            InitializeComponent();
        }

        private void FrmImportEmployees_Load(object sender, EventArgs e)
        {
            InitListView(LVDataExcel);
            InitListView(LVDataSQL);
        }

     
        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Microsoft Excel (*.xls)|*.XLS| All files (*.*)|*.*";
            dlg.CheckFileExists = true;
            dlg.InitialDirectory = Application.StartupPath;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;
                mIndexSQL = 0;
                LVDataSQL.Items.Clear();

                mIndexExcel = 0;
                LVDataExcel.Items.Clear();
                mDataExcel.Rows.Clear();

                mFileNameImport = dlg.FileName;
                txtFileName.Text = mFileNameImport;

                if (mFileNameImport.Trim().Length > 0)
                {

                    string strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                                 "Data Source=" + mFileNameImport + "; Jet OLEDB:Engine Type=5;" +
                                 "Extended Properties=Excel 8.0;";
                    OleDbConnection cnCSV = new OleDbConnection(strConnectionString);

                    try
                    {
                        cnCSV.Open();
                        OleDbCommand cmdSelect = new OleDbCommand(@"SELECT * FROM [Sheet1$]", cnCSV);
                        OleDbDataAdapter daCSV = new OleDbDataAdapter();
                        daCSV.SelectCommand = cmdSelect;

                        daCSV.Fill(mDataExcel);
                        cnCSV.Close();
                        daCSV = null;
                    }
                    catch (Exception Err)
                    {
                        MessageBox.Show(Err.ToString());
                    }

                    mThreadExcel = new Thread(new ThreadStart(LoadDataFromTableExcelToListview));
                    mThreadExcel.Start();

                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void InitListView(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã số";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giới tính";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày sinh";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số điện thoại";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa chỉ";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Description";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
            // LV.Sorting = System.Windows.Forms.SortOrder.Ascending;

        }

        #region [Read Data From Excel to ListView ]

        private void LoadDataFromTableExcelToListview()
        {

            foreach (DataRow nRowExcel in mDataExcel.Rows)
            {
                UpdateExcelToLisView(nRowExcel);
            }
            mThreadExcel.Abort();
        }
        delegate void SetItemExcelCallback(DataRow RowExcel);
        private void UpdateExcelToLisView(DataRow RowExcel)
        {
            if (this.LVDataExcel.InvokeRequired)
            {
                SetItemExcelCallback d = new SetItemExcelCallback(UpdateExcelToLisView);
                this.Invoke(d, new object[] { RowExcel });
            }
            else
            {
                UpdateItemExcel(RowExcel);
                LVDataExcel.EnsureVisible(LVDataExcel.Items.Count - 1);
            }
        }
        private void UpdateItemExcel(DataRow nRow)
        {
            mIndexExcel++;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;


            lvi = new ListViewItem();
            lvi.Name = mIndexSQL.ToString();

            lvi.Text = mIndexExcel.ToString();
            lvi.Tag = mIndexExcel;

            lvi.ForeColor = Color.DarkBlue;
            lvi.BackColor = Color.White;

            lvi.ImageIndex = 0;


            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[0].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[1].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[2].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[3].ToString().Trim();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[4].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[5].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[6].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[6].ToString();
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = "";
            lvi.SubItems.Add(lvsi);

            LVDataExcel.Items.Add(lvi);

        }


        #endregion

        #region [Update From TableExcel to SQL ]
        private void cmdImport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            mThreadSQL = new Thread(new ThreadStart(ImportDataToSQL));
            mThreadSQL.Start();

        }
        private void ImportDataToSQL()
        {

            Employee_Info nEmployee;
            int nIndex = 0;
            foreach (DataRow nRowExcel in mDataExcel.Rows)
            {
                nEmployee = CheckEmployeeInfo(nRowExcel);

                if (nEmployee.Message.Length == 0)
                {
                    int nEmployeeKey = 0;
                    if (int.TryParse(nEmployee.Create(), out nEmployeeKey))
                    {
                        UpdateToLisViewSQL(nRowExcel);
                        DelItemLisViewExcel(nIndex);
                    }
                    else
                    {
                        UpdateItemLisViewExcel(nIndex, nEmployee.Message);
                        nIndex++;
                    }
                }
                else
                {
                    UpdateItemLisViewExcel(nIndex, nEmployee.Message);
                    nIndex++;

                }

            }
            mThreadSQL.Abort();

            Cursor.Current = Cursors.Default;
        }

        delegate void SetItemSQLCallback(DataRow RowSQL);
        private void UpdateToLisViewSQL(DataRow RowSQL)
        {
            if (this.LVDataSQL.InvokeRequired)
            {
                SetItemExcelCallback d = new SetItemExcelCallback(UpdateToLisViewSQL);
                this.Invoke(d, new object[] { RowSQL });
            }
            else
            {
                UpdateItemSQL(RowSQL);
                LVDataSQL.EnsureVisible(LVDataSQL.Items.Count - 1);
            }
        }
        private void UpdateItemSQL(DataRow nRow)
        {
            mIndexSQL++;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Name = mIndexSQL.ToString();
            lvi.Text = mIndexSQL.ToString();
            lvi.Tag = mIndexSQL;
            lvi.ForeColor = Color.DarkBlue;
            lvi.BackColor = Color.White;

            lvi.ImageIndex = 0;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[0].ToString().Trim();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[1].ToString().Trim();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[2].ToString().Trim();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[3].ToString().Trim();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[4].ToString();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[5].ToString();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[6].ToString();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = nRow[7].ToString();
            //lvsi.ForeColor = color;
            lvi.SubItems.Add(lvsi);

            LVDataSQL.Items.Add(lvi);

        }

        delegate void DelItemExcelCallback(int IndexExcel);
        private void DelItemLisViewExcel(int IndexExcel)
        {
            if (this.LVDataExcel.InvokeRequired)
            {
                DelItemExcelCallback d = new DelItemExcelCallback(DelItemLisViewExcel);
                this.Invoke(d, new object[] { IndexExcel });
            }
            else
            {
                if (IndexExcel < LVDataExcel.Items.Count)
                    LVDataExcel.Items[IndexExcel].Remove();
            }
        }

        delegate void UpdateItemExcelCallback(int IndexExcel, string MessageError);
        private void UpdateItemLisViewExcel(int IndexExcel, string MessageError)
        {
            if (this.LVDataExcel.InvokeRequired)
            {
                UpdateItemExcelCallback d = new UpdateItemExcelCallback(UpdateItemLisViewExcel);
                this.Invoke(d, new object[] { IndexExcel, MessageError });
            }
            else
            {
                if (IndexExcel < LVDataExcel.Items.Count)
                {
                    LVDataExcel.Items[IndexExcel].SubItems[6].Text = MessageError;
                    LVDataExcel.EnsureVisible(IndexExcel);
                }
            }
        }
        #endregion

        private Employee_Info CheckEmployeeInfo(DataRow RowExcel)
        {
            string nResult = "";
            Employee_Info nEmployees = new Employee_Info();
            nEmployees.EmployeeID = RowExcel[0].ToString();
            nEmployees.LastName = RowExcel[1].ToString();
            nEmployees.FirstName = RowExcel[2].ToString();
            nEmployees.Gender = bool.Parse(RowExcel[3].ToString());
            nEmployees.BirthDate = DateTime.Parse(RowExcel[4].ToString());
            nEmployees.HomePhone = RowExcel[5].ToString();
            nEmployees.Address = RowExcel[6].ToString();

            nEmployees.CreatedBy = SessionUser.UserLogin.Key;
            nEmployees.ModifiedBy = SessionUser.UserLogin.Key;

            if (RowExcel[0].ToString().Trim().Length == 0)
                nResult += "[Error ID] ";

            if (RowExcel[1].ToString().Trim().Length == 0)
                nResult += "[Error Name] ";

            nEmployees.Message = nResult;
            return nEmployees;
        }

    }
}