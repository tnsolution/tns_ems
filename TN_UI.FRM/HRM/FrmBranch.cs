﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS.Users;

namespace TN_UI.FRM.HRM
{
    public partial class FrmBranch : Form
    {
        public FrmBranch()
        {
            InitializeComponent();
        }

        private void FrmBranch_Load(object sender, EventArgs e)
        {
            tnGridView1.Table = "HRM_Branchs";
            tnGridView1.FieldDisplay = "BranchName";
            tnGridView1.FieldValue = "BranchKey";
            tnGridView1.Parent = 0;
            tnGridView1.LoadDataBase("SELECT BranchKey,BranchName FROM HRM_Branchs ORDER BY BranchName");
            CheckRole();

        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM001");
            if (!nRole.Edit)
            {
                tnGridView1.SetCmdEdit = false;
            }
            if (!nRole.Del)
                tnGridView1.SetCmdDel = false;

        }
    }
}
