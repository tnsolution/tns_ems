using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.HRM;
using CrystalDecisions.CrystalReports.Engine;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.HRM
{
    public partial class FrmSalaryMonth : Form
    {
        private ReportDocument rpt = new ReportDocument();

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mDefaultAccountProduct = GlobalSystemConfig.AccountProduct;


        public FrmSalaryMonth()
        {
            InitializeComponent();

        }
        private void FrmSalaryMonth_Load(object sender, EventArgs e)
        {
            SetupLayoutGridView(dataGridView1);

            panelSearch.Height = 00;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.Dock = DockStyle.Fill;
            crystalReportViewer1.Dock = DockStyle.Fill;

            CoMonths.SelectedIndex = DateTime.Now.Month - 1;
            CoYears.Text = DateTime.Now.Year.ToString();
            cmdCreateSalaryCurrencyMonth.Text += " " + DateTime.Now.Month.ToString();
           
            cmdUnhideCenter_Click(sender,null);         
        }
       
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            SearchInventory();
        }
        private void SearchInventory()
        {
            DataTable nTable = Employees_Data.Salarys(int.Parse(CoMonths.Text), int.Parse(CoYears.Text));
            PopulateDataGridView(dataGridView1, nTable);
        }
        private void cmdCreateSalaryCurrencyMonth_Click(object sender, EventArgs e)
        {
            DataTable nEmployees = Employees_Data.EmployeeList();
            int nDaysWorkInMonth = TNFunctions.NumberDaysWorkingInMonth(DateTime.Now.Month, DateTime.Now.Year);
            foreach (DataRow nRow in nEmployees.Rows)
            {
                int nEmployeeKey = int.Parse(nRow["EmployeeKey"].ToString());
                double nBasicSalary = double.Parse(nRow["BasicSalary"].ToString());
                
               
                int nMonth = DateTime.Now.Month;
                int nYear = DateTime.Now.Year;

                Salary_Info nSalary = new Salary_Info(nEmployeeKey, nMonth, nYear);
                if (nSalary.SalaryKey == 0)
                {
                    nSalary.BasicSalary = nBasicSalary;
                    nSalary.WorkDays = nDaysWorkInMonth;
                    nSalary.NetSalary = nBasicSalary;
                    nSalary.TotalSalary = nBasicSalary;
                    nSalary.CreatedBy = SessionUser.UserLogin.Key;
                    nSalary.ModifiedBy = SessionUser.UserLogin.Key;
                    nSalary.Save();
                }
            }
            SearchInventory();
        }
        
        #region [ GridView ]
        private void SetupLayoutGridView(DataGridView GV)
        {
            GV.Columns.Add("No", "");
            GV.Columns.Add("EmployeeID", "");
            GV.Columns.Add("EmployeeName", "");
            GV.Columns.Add("BasicSalary", "");
            GV.Columns.Add("WorkDays", "");
            GV.Columns.Add("NetSalary", "");
            GV.Columns.Add("Bonus", "");
            GV.Columns.Add("TotalSalary", "");

            GV.Columns[0].Width = 60;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[0].ReadOnly = true;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[2].Width = 300;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 150;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[5].Width = 150;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 150;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 150;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            // GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * 3 / 2;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.Paint += new PaintEventHandler(GV_Paint);

            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.CellEndEdit += new DataGridViewCellEventHandler(GV_CellEndEdit);
            GV.RowValidated += new DataGridViewCellEventHandler(GV_RowValidated);
            GV.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(GV_EditingControlShowing);

        }
        private void GV_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;

            string[] TitleColumn = { "STT", " Mã Nhân viên ", "Tên Nhân viên", "Lương cơ bản", "Ngày công", "Lương tháng", "Phụ cấp - Thưởng", "Tổng lương" };

            int nColumnTotal = GV.ColumnCount;

            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;


                r1.Width = r1.Width - 2;
                r1.Height = r1.Height - 2;
                g.DrawImage(image, r1);


                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        private void PopulateDataGridView(DataGridView GV, DataTable TableView)
        {

            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];

                double nBasicSalary = double.Parse(nRow["BasicSalary"].ToString());
                double nNetSalary = double.Parse(nRow["NetSalary"].ToString());
                double nTotalSalary = double.Parse(nRow["TotalSalary"].ToString());
                double nBonus = double.Parse(nRow["Bonus"].ToString());

                float nWorkDays = float.Parse(nRow["WorkDays"].ToString());

                GV.Rows.Add();
                GV.Rows[i].Tag = (int)nRow["EmployeeKey"];
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count ;
                GV.Rows[i].Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                GV.Rows[i].Cells["EmployeeID"].Tag = (int)nRow["MonthSalary"];
                GV.Rows[i].Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                GV.Rows[i].Cells["EmployeeName"].Tag = (int)nRow["YearSalary"];

                GV.Rows[i].Cells["BasicSalary"].Value = nBasicSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["WorkDays"].Value = nWorkDays.ToString(mFormatDecimal, mFormatProviderDecimal);
                GV.Rows[i].Cells["NetSalary"].Value = nNetSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["Bonus"].Value = nBonus.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["TotalSalary"].Value = nTotalSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            }
        }  
        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;

        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowEdit = dataGridView1.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 3:
                    double nBasicSalary = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["BasicSalary"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBasicSalary))
                        {
                            nRowEdit.Cells["BasicSalary"].Value = nBasicSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["BasicSalary"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["BasicSalary"].ErrorText = "Sai định dạng tiền tệ";
                    }
                    break;

                case 4:
                    float nWorkDays = 0;
                    if (nRowEdit.Cells[e.ColumnIndex].Value != null)
                    {
                        if (float.TryParse(nRowEdit.Cells["WorkDays"].Value.ToString(), NumberStyles.Any, mFormatProviderDecimal, out nWorkDays))
                        {
                            nRowEdit.Cells["WorkDays"].Value = nWorkDays.ToString(mFormatDecimal, mFormatProviderDecimal);
                            nRowEdit.Cells["WorkDays"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["WorkDays"].ErrorText = "Sai định dạng số ngày công";
                    }
                    break;
                case 5:
                    double nNetSalary = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["NetSalary"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nNetSalary))
                        {
                            nRowEdit.Cells["NetSalary"].Value = nNetSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["NetSalary"].ErrorText = "";
                            UpdateTotalSalary(nRowEdit);
                        }
                        else
                            nRowEdit.Cells["NetSalary"].ErrorText = "Sai định dạng tiền tệ";
                    }
                    break;
                case 6:
                    double nBonus = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["Bonus"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBonus))
                        {
                            nRowEdit.Cells["Bonus"].Value = nBonus.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["Bonus"].ErrorText = "";

                            UpdateTotalSalary(nRowEdit);
                        }
                        else
                            nRowEdit.Cells["Bonus"].ErrorText = "Sai định dạng tiền tệ";
                    }

                    break;
                case 7:
                    double nTotalSalary = 0;
                    {
                        if (double.TryParse(nRowEdit.Cells["TotalSalary"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nTotalSalary))
                        {
                            nRowEdit.Cells["TotalSalary"].Value = nTotalSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                            nRowEdit.Cells["TotalSalary"].ErrorText = "";
                        }
                        else
                            nRowEdit.Cells["TotalSalary"].ErrorText = "Sai định dạng tiền tệ";
                    }
                    break;

                default:
                    break;
            }

        }
        private void GV_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowCurrent = dataGridView1.Rows[e.RowIndex];

            nRowCurrent.DefaultCellStyle.BackColor = Color.White;

            if (CheckRow(nRowCurrent))
            {
                int nEmployeeKey = (int)nRowCurrent.Tag;
                int nMonth = (int)nRowCurrent.Cells["EmployeeID"].Tag;
                int nYear = (int)nRowCurrent.Cells["EmployeeName"].Tag;

                Salary_Info nSalary = new Salary_Info(nEmployeeKey, nMonth, nYear);
                nSalary.BasicSalary = double.Parse(nRowCurrent.Cells["BasicSalary"].Value.ToString(), mFormatProviderCurrency);
                nSalary.WorkDays = float.Parse(nRowCurrent.Cells["WorkDays"].Value.ToString(), mFormatProviderDecimal);
                nSalary.NetSalary = double.Parse(nRowCurrent.Cells["NetSalary"].Value.ToString(), mFormatProviderCurrency);
                nSalary.Bonus = double.Parse(nRowCurrent.Cells["Bonus"].Value.ToString(), mFormatProviderCurrency);
                nSalary.TotalSalary = double.Parse(nRowCurrent.Cells["TotalSalary"].Value.ToString(), mFormatProviderCurrency);

                nSalary.CreatedBy = SessionUser.UserLogin.Key;
                nSalary.ModifiedBy = SessionUser.UserLogin.Key;

                nSalary.Save();

                if (nSalary.Message.Length > 0)
                    MessageBox.Show(nSalary.Message);

            }

        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
        }

        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }

        private bool CheckRow(DataGridViewRow RowCheck)
        {
            bool nResult = true;

            if (RowCheck.Cells["BasicSalary"].ErrorText.Trim().Length > 0 || RowCheck.Cells["BasicSalary"].Value == null)
            {
                RowCheck.Cells["BasicSalary"].ErrorText = "Vui lòng kiểm tra lương cơ bản";
                nResult = false;
            }

            if (RowCheck.Cells["WorkDays"].ErrorText.Trim().Length > 0 || RowCheck.Cells["WorkDays"].Value == null)
            {
                RowCheck.Cells["WorkDays"].ErrorText = "Vui lòng kiểm tra số ngày công";
                nResult = false;
            }

            if (RowCheck.Cells["NetSalary"].ErrorText.Trim().Length > 0 || RowCheck.Cells["NetSalary"].Value == null)
            {
                RowCheck.Cells["NetSalary"].ErrorText = "Vui lòng kiểm tra lương cơ bản";
                nResult = false;
            }

            if (RowCheck.Cells["Bonus"].ErrorText.Trim().Length > 0 || RowCheck.Cells["Bonus"].Value == null)
            {
                RowCheck.Cells["Bonus"].ErrorText = "Vui lòng kiểm tra tiền thưởng";
                nResult = false;
            }

            if (RowCheck.Cells["TotalSalary"].ErrorText.Trim().Length > 0 || RowCheck.Cells["TotalSalary"].Value == null)
            {
                RowCheck.Cells["TotalSalary"].ErrorText = "Vui lòng kiểm tra tổng tiền";
                nResult = false;
            }

            return nResult;
        }
        private void UpdateTotalSalary(DataGridViewRow RowUpdate)
        {
            double nNetSalary = 0;
            double nBonus = 0;

            double.TryParse(RowUpdate.Cells["Bonus"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBonus);
            double.TryParse(RowUpdate.Cells["NetSalary"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nNetSalary);

            double nTotalSalary = nNetSalary + nBonus;
            RowUpdate.Cells["TotalSalary"].Value = nTotalSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        #endregion

        #region [ Design Layout]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }

        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 65;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        private void PopulateCrystalReport(DataTable nTable)
        {
            this.Cursor = Cursors.WaitCursor;

            rpt.Load("FilesReport\\BangluongNV.rpt");

            DataTable nTableDetail = TableReport.TableBangLuongNVDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();
            int i = 0;
            foreach (DataRow nRow in nTable.Rows)
            {
                i++;
                DataRow nRowDetail = nTableDetail.NewRow();
                nRowDetail["No"] = i.ToString();
                nRowDetail["EmployeeID"] = nRow["EmployeeID"].ToString().Trim();
                nRowDetail["EmployeeName"] = nRow["EmployeeName"].ToString().Trim();
                nRowDetail["BasicSalary"] = nRow["BasicSalary"].ToString().Trim();
                nRowDetail["WorkDays"] = nRow["WorkDays"].ToString().Trim();
                nRowDetail["NetSalary"] = nRow["NetSalary"].ToString().Trim();
                nRowDetail["Bonus"] = nRow["Bonus"].ToString().Trim();
                nRowDetail["TotalSalary"] = nRow["TotalSalary"].ToString().Trim();

                nTableDetail.Rows.Add(nRowDetail);

            }

            rpt.Database.Tables["TableBangLuongNVDetail"].SetDataSource(nTableDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Zoom(1);

            this.Cursor = Cursors.Default;

        }
        private void radioGridView_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            crystalReportViewer1.Visible = false;
            DataTable nTable = Employees_Data.Salarys(int.Parse(CoMonths.Text), int.Parse(CoYears.Text));
            PopulateDataGridView(dataGridView1, nTable);
        }

        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            crystalReportViewer1.Visible = true;
            DataTable nTable = Employees_Data.Salarys(int.Parse(CoMonths.Text), int.Parse(CoYears.Text));
            PopulateCrystalReport(nTable);
        }
     
    }
}