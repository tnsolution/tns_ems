﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using TNLibrary.HRM;
using TN_UI.FRM.Reports;
using TNConfig;
using CrystalDecisions.Shared;
using System.IO;


namespace TN_UI.FRM.HRM
{
    public partial class FrmEmployeeReport : Form
    {
        public bool FlagView;
        public int nEmployeeKey;
        public int EmployeeKey;
        
        public FrmEmployeeReport()
        {
            InitializeComponent();
        }

        private void FrmEmployeeReport_Load(object sender, EventArgs e)
        {
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            crystalReportViewer1.Dock = DockStyle.Fill;
            PopulateCrystalReport();
        }
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {
            ReportDocument rptTam = new ReportDocument();
            rptTam.Load("FilesReport\\CVNhanVien.rpt");
            DataTable nEmployeesDetail = TableReport.TableCVNhanVienDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            #region [ Set paremater for SQL Server ]
            RWConfig nConfig = new RWConfig();
            ConnectDataBaseInfo nConnectInfo = new ConnectDataBaseInfo();
            if (nConfig.ReadConfig())
            {
                nConnectInfo = nConfig.ConnectInfo;
                string a = nConnectInfo.DataSource.ToString();
                string b = nConnectInfo.Password.ToString();
            }
            TableLogOnInfo tliCurrent;
            foreach (Table tbCurrent in rptTam.Database.Tables)
            {
                tliCurrent = tbCurrent.LogOnInfo;
                tliCurrent.ConnectionInfo.ServerName = nConnectInfo.DataSource;
                tliCurrent.ConnectionInfo.DatabaseName = nConnectInfo.DataBase;
                tliCurrent.ConnectionInfo.UserID = nConnectInfo.UserName;
                tliCurrent.ConnectionInfo.Password = nConnectInfo.Password;
                tbCurrent.ApplyLogOnInfo(tliCurrent);
            }
            #endregion

            Employee_Info nEmployee = new Employee_Info(nEmployeeKey);

            DataRow nRowDetail = nEmployeesDetail.NewRow();
            nRowDetail["EmployeeID"] = nEmployee.EmployeeID;
            nRowDetail["LastName"] = nEmployee.LastName;
            nRowDetail["FirstName"] = nEmployee.FirstName;
            nRowDetail["Gender"] = nEmployee.Gender;
            nRowDetail["BirthDate"] = nEmployee.BirthDate.ToShortDateString();
            nRowDetail["Address"] = nEmployee.Address;
            nRowDetail["HomePhone"] = nEmployee.HomePhone;

            nRowDetail["PassportNumber"] = nEmployee.PassportNumber;
            nRowDetail["IssueDate"] = nEmployee.IssueDate;
            nRowDetail["ExpireDate"] = nEmployee.ExpireDate;
            nRowDetail["IssuePlace"] = nEmployee.IssuePlace;
            nRowDetail["CurrentNationality"] = nEmployee.CurrentNationality;
            nRowDetail["City"] = nEmployee.City;
            nRowDetail["Country"] = nEmployee.Country;
            nRowDetail["MobiPhone"] = nEmployee.MobiPhone;
            nRowDetail["Email"] = nEmployee.Email;
            nRowDetail["Photo"] = nEmployee.Photo;

            nEmployeesDetail.Rows.Add(nRowDetail);

          
            rptTam.Database.Tables["TableCVNhanVienDetail"].SetDataSource(nEmployeesDetail);
            //rptTam.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            crystalReportViewer1.ReportSource = rptTam;
            crystalReportViewer1.Zoom(1);
        }
   }
}
