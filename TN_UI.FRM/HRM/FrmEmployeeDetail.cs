﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.HRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TN_UI.FRM.CRM;

namespace TN_UI.FRM.HRM
{
    public partial class FrmEmployeeDetail : Form
    {
        public bool FlagNew;
        public bool FlagEdit;

        public int mEmployeeKey = 0;
        public string mEmployeeID;
        public int DepartmentKey = 0;

        private Employee_Info m_Employee;
        private bool FlagChangePic = false;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmEmployeeDetail()
        {
            InitializeComponent();
        }

        private void FrmEmployeeDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coReportTo, "SELECT EmployeeKey, LastName,FirstName FROM HRM_Employees", true);
            LoadDataToToolbox.ComboBoxData(CoCountry, "SELECT CountryKey, CountryName FROM SYS_LOC_Country", false);
            LoadDataToToolbox.ComboBoxData(coNational, "SELECT CountryKey, CountryName FROM SYS_LOC_Country", false);
            LoadDataToToolbox.ComboBoxData(coDepartment, "SELECT DepartmentKey, DepartmentName FROM SYS_Departments", true);
            LoadDataToToolbox.ComboBoxData(CoBranch, "SELECT BranchKey, BranchName FROM HRM_Branchs", true);

            if (mEmployeeKey != 0)
                LoadInfoEmployee(mEmployeeKey);

        }
        #region [ Load Info Employee ]

        protected void LoadInfoEmployee(int nKey)
        {
            m_Employee = new Employee_Info(nKey);
            FlagChangePic = false;

            txtEmployeeID.Text = m_Employee.EmployeeID;
            txtFirstName.Text = m_Employee.FirstName;
            txtLastName.Text = m_Employee.LastName;
            if (m_Employee.Gender)
            {
                radioFemale.Checked = false;
                radioMale.Checked = true;
            }
            else
            {
                radioMale.Checked = false;
                radioFemale.Checked = true;
            }
            PickerBirthdate.Value = m_Employee.BirthDate;
            coNational.Text = m_Employee.CurrentNationality;

            txtPassport.Text = m_Employee.PassportNumber;
            if (m_Employee.IssueDate.Year != 001)
                PickerIssueDate.Value = m_Employee.IssueDate;

            if (m_Employee.ExpireDate.Year != 0001)
                tnPickerExpire.Value = m_Employee.ExpireDate;
            else
                tnPickerExpire.Text = "";

            txtIssuePlace.Text = m_Employee.IssuePlace;
            coNational.SelectedText = m_Employee.CurrentNationality;

            txtAddress.Text = m_Employee.Address;
            txtCity.Text = m_Employee.City;
            CoCountry.Text = m_Employee.Country;
            txtHomePhone.Text = m_Employee.HomePhone;
            txtMobiPhone.Text = m_Employee.MobiPhone;
            txtEmail.Text = m_Employee.Email;

            PickerHireDate.Value = m_Employee.HireDate;
            if (m_Employee.QuitDate.Year != 0001)
                tnPickerQuitDate.Value = m_Employee.QuitDate;
            else
                tnPickerQuitDate.Text = "";

            chkActive.Checked = m_Employee.IsWorking;
            txtBasicSalary.Text = m_Employee.BasicSalary.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            coReportTo.SelectedValue = m_Employee.ReportTo;
            coDepartment.SelectedValue = m_Employee.DepartmentKey;
            CoBranch.SelectedValue = m_Employee.BranchKey;

            if (m_Employee.ImagePhoto != null)
            {
                PicEmployees.Image = m_Employee.ImagePhoto;
                PicEmployees.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
        #endregion

        #region [ Update Employee Information ]
        private bool CheckBeforeSave()
        {

            if (txtEmployeeID.Text.Trim().Length == 0)
            {
                MessageBox.Show("vui lòng nhập mã nhân viên");
                txtEmployeeID.Focus();
                return false;
            }
            return true;

        }
        private bool SaveInfo()
        {
            m_Employee = new Employee_Info(mEmployeeKey);
            m_Employee.EmployeeKey = mEmployeeKey;

            m_Employee.EmployeeID = txtEmployeeID.Text;
            m_Employee.FirstName = txtFirstName.Text;
            m_Employee.LastName = txtLastName.Text;

            if (radioMale.Checked)
            {
                m_Employee.Gender = true;
            }
            else
            {
                m_Employee.Gender = false;
            }

            m_Employee.BirthDate = PickerBirthdate.Value;

            m_Employee.CurrentNationality = coNational.Text;
            m_Employee.PassportNumber = txtPassport.Text;

            if (PickerIssueDate.Text.Trim().Length == 0)
                m_Employee.IssueDate = new DateTime();
            else
                m_Employee.IssueDate = PickerIssueDate.Value;

            m_Employee.IssuePlace = txtIssuePlace.Text;

            if (tnPickerExpire.Text.Trim().Length == 0)
                m_Employee.ExpireDate = new DateTime();
            else
                m_Employee.ExpireDate = tnPickerExpire.Value;
            m_Employee.CurrentNationality = coNational.Text;

            m_Employee.Address = txtAddress.Text;
            m_Employee.City = txtCity.Text;
            m_Employee.Country = CoCountry.Text;
            m_Employee.HomePhone = txtHomePhone.Text;
            m_Employee.MobiPhone = txtMobiPhone.Text;
            m_Employee.Email = txtEmail.Text;

            m_Employee.HireDate = PickerHireDate.Value;
            if (tnPickerQuitDate.Text.Trim().Length == 0)
                m_Employee.QuitDate = new DateTime();
            else
                m_Employee.QuitDate = tnPickerQuitDate.Value;

            m_Employee.IsWorking = chkActive.Checked;
            m_Employee.ReportTo = (int)coReportTo.SelectedValue;
            m_Employee.BranchKey = (int)CoBranch.SelectedValue;

            m_Employee.BasicSalary = double.Parse(txtBasicSalary.Text, mFormatProviderCurrency);

            m_Employee.DepartmentKey = (int)coDepartment.SelectedValue;
            if (FlagChangePic || m_Employee.Photo == null)
                m_Employee.Photo = GraphicProcess.ImageToByte(PicEmployees);

            m_Employee.CreatedBy = SessionUser.UserLogin.Key;
            m_Employee.ModifiedBy = SessionUser.UserLogin.Key;

            return true;
        }
        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtBasicSalary_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtBasicSalary.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;

            txtBasicSalary.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        private void txtBasicSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        #endregion

        #region [ Action Button]
        private void PicEmployees_Click(object sender, EventArgs e)
        {
            OpenFileDialog1.InitialDirectory = "C:/";
            OpenFileDialog1.Filter = "All Files|*.*|Bitmaps|*.bmp|GIFs|*.gif|JPEGs|*.jpg";
            OpenFileDialog1.FilterIndex = 3;

            if (OpenFileDialog1.ShowDialog() == DialogResult.OK)
            {
                PicEmployees.Image = Image.FromFile(OpenFileDialog1.FileName);
                PicEmployees.SizeMode = PictureBoxSizeMode.StretchImage;
                FlagChangePic = true;
            }
        }
 
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveInfo();
                m_Employee.Save();
                this.Close();
            }

        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveInfo();
                m_Employee.Save();
                ClearInfo();
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            int nResult = Employees_Data.CheckExistEmployee(m_Employee.EmployeeKey);
            if (nResult > 0)
            {
                MessageBox.Show("Không thể xóa nhân viên này, vui lòng kiểm tra lại!");
            }
            else
            {
                m_Employee.Delete();
                ClearInfo();
                this.Close();
            }
        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            //FrmPrintResume frm = new FrmPrintResume();
            //frm.m_EmployeeKey = m_Employee.mEmployeeKey;
            //frm.Show();

        }
        #endregion

        #region [ Function ]
        private void ClearInfo()
        {

            cmdPrinter.Visible = false;
            cmdDel.Visible = false;

            m_Employee = new Employee_Info();
            FlagChangePic = false;

            txtEmployeeID.Text = m_Employee.EmployeeID;
            txtFirstName.Text = m_Employee.FirstName;
            txtLastName.Text = m_Employee.LastName;

            radioFemale.Checked = true;
            coNational.Text = m_Employee.CurrentNationality;
            txtPassport.Text = m_Employee.PassportNumber;
            txtIssuePlace.Text = m_Employee.IssuePlace;

            txtAddress.Text = m_Employee.Address;
            txtCity.Text = m_Employee.City;
            CoCountry.Text = m_Employee.Country;
            txtHomePhone.Text = m_Employee.HomePhone;
            txtMobiPhone.Text = m_Employee.MobiPhone;
            txtEmail.Text = m_Employee.Email;

            chkActive.Checked = m_Employee.IsWorking;

        }
        #endregion

        #region [ Short Key ]
        private void FrmEmployeeDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("HRM002");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion


    }
}