using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.HRM;

using TN_UI.FRM.Reports;

namespace TN_UI.FRM.HRM
{
    public partial class FrmEmployeesList : Form
    {
        private ReportDocument rpt = new ReportDocument();

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        string mFromDate = GlobalSystemConfig.FormatDate;

        private DataTable mEmployessList = new DataTable();

        public string mEmployeeID = "";
        public bool FlagView;
        public string mTitle = "DANH SÁCH NHÂN VIÊN";

        public FrmEmployeesList()
        {
            InitializeComponent();
            InitLayoutForm();
        }

        private void FrmEmployeesList_Load(object sender, EventArgs e)
        {
            CheckRole();

            InitListView(ListViewData);
            cmdHide_Click(null, EventArgs.Empty);
            txtTitle.Text = mTitle;
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            mEmployessList = Employees_Data.EmployeeList();
            LoadData();
        }
        private void LoadData()
        {
            if (ListViewData.Visible)
                LoadDataToListView(ListViewData);
            else
                LoadDataToCrystalReport();
        }
        //Layout 
        private void InitLayoutForm()
        {

            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);

            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);

            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);

            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);

            this.cmdUnhide.MouseLeave += new System.EventHandler(this.cmdUnhide_MouseLeave);
            this.cmdUnhide.Click += new System.EventHandler(this.cmdUnhide_Click);
            this.cmdUnhide.MouseEnter += new System.EventHandler(this.cmdUnhide_MouseEnter);

            this.cmdHide.MouseLeave += new System.EventHandler(this.cmdHide_MouseLeave);
            this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
            this.cmdHide.MouseEnter += new System.EventHandler(this.cmdHide_MouseEnter);

            panelSearch.Height = 0;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            cmdUnhideCenter.Image = imageList3.Images[0];

            ListViewData.Dock = DockStyle.Fill;
            ReportViewer.Dock = DockStyle.Fill;
            ReportViewer.DisplayGroupTree = false;
        }

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Lay out left  ]

        private void cmdHide_MouseEnter(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[1];
        }

        private void cmdHide_MouseLeave(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[0];
        }

        private void cmdHide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 28;  //  ContainerLeft.SplitterDistance = 10;
            TreeViewData.Visible = false;

            cmdHide.Visible = false;
            txtTitleLeft.Visible = false;
            panelLineLeft.Visible = false;
            panelViewLeft.Visible = false;

            cmdUnhide.Visible = true;
        }


        private void cmdUnhide_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[3];
        }

        private void cmdUnhide_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[2];
        }

        private void cmdUnhide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 250;// ContainerLeft.SplitterDistance = 260;
            TreeViewData.Visible = true;
            cmdHide.Visible = true;
            txtTitleLeft.Visible = true;
            panelLineLeft.Visible = true;
            panelViewLeft.Visible = true;

            cmdUnhide.Visible = false;

        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            cmdHide.Left = panelTitle.Width - cmdHide.Width;
        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã số";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày sinh";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giới tính";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Phone";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa Chỉ";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            LV.SmallImageList = SmallImageLV;
            LV.Sorting = System.Windows.Forms.SortOrder.Ascending;
            LVGroup = new ListViewMyGroup(LV);

        }

        // Load Database
        public void LoadDataToListView(ListView LV)
        {
            this.Cursor = Cursors.WaitCursor;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = mEmployessList.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = mEmployessList.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = "";//nRow[""].ToString();
                lvi.Tag = nRow["EmployeeKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["EmployeeID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LastName"].ToString().Trim() + " " + nRow["FirstName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                DateTime nDate = (DateTime)nRow["BirthDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nDate.ToString(mFromDate);
                lvi.SubItems.Add(lvsi);

                string nGender = "";
                if ((bool)nRow["Gender"])
                    nGender = "Nam";
                else
                    nGender = "Nữ";

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nGender;
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["HomePhone"].ToString();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Address"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

            this.Cursor = Cursors.Default;

        }

        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count >0)
            {
                ListViewData.SelectedItems[0].BackColor = Color.White;
            }
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                int nEmployeeKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                FrmEmployeeDetail frm = new FrmEmployeeDetail();
                frm.mEmployeeKey = nEmployeeKey;
                frm.ShowDialog();

                mEmployessList = Employees_Data.EmployeeList();
                radioViewListView.Checked = true;
                LoadDataToListView(ListViewData);
                
                ListViewData.Focus();
                string nEmployeeID = frm.mEmployeeID;
                if (nEmployeeID != null && nEmployeeID.Trim().Length > 0)
                {
                    Employee_Info nEmployee = new Employee_Info(nEmployeeID);
                    SelectIndexInListView(nEmployee.EmployeeKey);
                }
            }
            else
            {
                MessageBox.Show("Bạn phải chọn nhân viên");
            }
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)//Sk xoa kh bang tay
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)//xóa rows
                {
                  
                    int nResult = Employees_Data.CheckExistEmployee((int)ListViewData.SelectedItems[0].Tag);
                    if (nResult >0)
                    {
                        MessageBox.Show("Không thể xóa nhân viên này, vui lòng kiểm tra lại!");
                    }
                    else
                    {
                        if (MessageBox.Show("Bạn có muốn xóa nhân viên : " + ListViewData.SelectedItems[0].SubItems[2].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            int nIndex = ListViewData.SelectedItems[0].Index;
                            Employee_Info nEmployee = new Employee_Info();
                            nEmployee.EmployeeKey = (int)ListViewData.SelectedItems[0].Tag;
                            nEmployee.Delete();

                            ListViewData.SelectedItems[0].Remove();
                            if (ListViewData.Items.Count > 0)
                                if (nIndex == 0)
                                    ListViewData.Items[nIndex].Selected = true;
                                else
                                    ListViewData.Items[nIndex - 1].Selected = true;
                            ListViewData.Focus();
                        }
                    }
                }
                else//
                {
                    MessageBox.Show("Bạn phải chọn nhân viên để xóa.");
                }
            }
        }
        #endregion

        #region [CrystalReport]
        private void LoadDataToCrystalReport()
        {
            // thay ten report
            this.Cursor = Cursors.WaitCursor;
            rpt.Load("FilesReport\\CrystalReporEmployees.rpt");
            DataTable nEmployeesDetail = TableReport.EmployeesDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            foreach (DataRow nRow in mEmployessList.Rows)
            {
                DataRow nRowDetail = nEmployeesDetail.NewRow();
                nRowDetail["EmployeeID"] = nRow["EmployeeID"].ToString();
                nRowDetail["LastName"] = nRow["LastName"].ToString().Trim();
                nRowDetail["FirstName"] = nRow["FirstName"].ToString().Trim();
                nRowDetail["Gender"] = nRow["Gender"].ToString().Trim();
                nRowDetail["BirthDate"] = nRow["BirthDate"].ToString().Trim();
                nRowDetail["Address"] = nRow["Address"].ToString().Trim();
                nRowDetail["HomePhone"] = nRow["HomePhone"].ToString().Trim();

                nEmployeesDetail.Rows.Add(nRowDetail);

            }

            rpt.Database.Tables["EmployeesDetail"].SetDataSource(nEmployeesDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            ReportViewer.ReportSource = rpt;
            ReportViewer.Zoom(1);

            this.Cursor = Cursors.Default;
        }
        #endregion

        #region [ Export ]
        private void cmdExcel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable nTableEmployees;
            nTableEmployees = Employees_Data.EmployeeList();

            DataTable nCustomersDetail = TableReport.EmployeesDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            foreach (DataRow nRow in mEmployessList.Rows)
            {
                DataRow nRowDetail = nCustomersDetail.NewRow();
                nRowDetail["EmployeeID"] = nRow["EmployeeID"].ToString();
                nRowDetail["LastName"] = nRow["LastName"].ToString().Trim();
                nRowDetail["FirstName"] = nRow["FirstName"].ToString().Trim();
                nRowDetail["BirthDate"] = nRow["BirthDate"].ToString().Trim();
                nRowDetail["Gender"] = nRow["Gender"].ToString().Trim();
                nRowDetail["HomePhone"] = nRow["HomePhone"].ToString().Trim();
                nRowDetail["Address"] = nRow["Address"].ToString().Trim();

                nCustomersDetail.Rows.Add(nRowDetail);

            }

            ExportToExcel(nCustomersDetail, nCompanyInfo);

            this.Cursor = Cursors.Default;
        }

        private static void ExportToExcel(DataTable dtDetail, DataTable dtCongty)
        {
            // Create an Excel object and add workbook...
            string FileName, FileType;
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            DialogSave.Title = "Save a file to...";

            //save.ShowDialog();

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                FileName = DialogSave.FileName;
                FileType = System.IO.Path.GetExtension(FileName);

                /////////////////////////
                Excel.Application excel = new Excel.Application();
                Excel.Workbook workbook = excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet exSheet = (Excel.Worksheet)workbook.Worksheets[1];
                //Excel.Range nRange;

                excel.ActiveWindow.DisplayGridlines = true;
                // Excel.Worksheet worksheet; 

                // add thong tin cong ty
                foreach (DataRow r in dtCongty.Rows)
                {

                    excel.Cells[1, 1] = r["CompanyName"].ToString();
                    excel.Cells[2, 1] = r["Address"].ToString();

                }
                //Add Tieu de
                excel.Cells[4, 4] = "DANH SÁCH NHÂN VIÊN";
                //add Column Name
                excel.Cells[6, 1] = "Mã Nhân Viên";
                excel.Cells[6, 2] = "Họ";
                excel.Cells[6, 3] = "Tên";
                excel.Cells[6, 4] = "Giới Tính";
                excel.Cells[6, 5] = "Ngày Sinh";
                excel.Cells[6, 6] = "Địa Chỉ";
                excel.Cells[6, 7] = "Số Điện Thoại";

                //bat dau ghi chi tiet vo file excel
                int iCol = 0;
                // for each row of data...
                int iRow = 0;
                foreach (DataRow r in dtDetail.Rows)
                {
                    iRow++;

                    // add each row's cell data...
                    iCol = 0;
                    foreach (DataColumn c in dtDetail.Columns)
                    {
                        iCol++;
                        excel.Cells[iRow + 6, iCol] = r[c.ColumnName];
                    }
                }

                //add Footer
                excel.Cells[iRow + 9, 1] = "    Người ghi sổ";
                excel.Cells[iRow + 9, 4] = "Kế toán trưởng";
                excel.Cells[iRow + 9, 7] = "        Giám đốc";


                ////tao border
                Excel.Range nRange;
                Excel.Worksheet worksheet1 = (Excel.Worksheet)excel.ActiveSheet;
                //dinh dang tieu de
                nRange = worksheet1.get_Range("A4", "G4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;

                //dinh dang footer
                int nRo1 = iRow + 9;
                int nRo2 = iRow + 10;
                nRange = worksheet1.get_Range("A" + nRo1, "G" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                nRange.Font.Size = 13;
                nRange = worksheet1.get_Range("A" + nRo2, "G" + nRo2);
                nRange.Font.Bold = true;

                //dinh dang columns Name
                nRange = worksheet1.get_Range("A6", "G6");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Cells.RowHeight = 25;
                nRange.Interior.Color = Color.Silver.ToArgb();
                //dinh dang Columns width
                //STT
                nRange = worksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                //Ký hiệu HĐ
                nRange = worksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //Số HĐ
                nRange = worksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //Ngày
                nRange = worksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //Tên người bán
                nRange = worksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //MST người bán
                nRange = worksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 28;
                //MST người bán
                nRange = worksheet1.get_Range("G1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                // chon vung tao border
                int x = iRow + 6;
                nRange = worksheet1.get_Range("A6", "G" + x);
                nRange.Borders.Weight = 2;
                //canh giua
                //nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                //nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo f = new FileInfo(FileName);
                if (f.Exists)
                    f.Delete(); // delete the file if it already exist.

                excel.Visible = false;
                workbook.SaveAs(FileName,
                Excel.XlFileFormat.xlWorkbookNormal, null, null,
                false, false, Excel.XlSaveAsAccessMode.xlExclusive,
                false, false, false, false, false);


                workbook.Close(false, false, false);
                excel.Workbooks.Close();
                excel.Application.Quit();
                excel.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(exSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nRange);
                exSheet = null;
                workbook = null;
                excel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

        }
        #endregion

        #region [ Import ]
        private void cmdImport_Click(object sender, EventArgs e)
        {
            
            FrmImportEmployees frm = new FrmImportEmployees();
            frm.ShowDialog();

            mEmployessList = Employees_Data.EmployeeList();

            LoadData();

        }
        #endregion

        #region [ Activate on Button ]
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            mEmployessList = Employees_Data.EmployeeList(txtEmployeeID.Text.Trim(),txtEmployeeName.Text.Trim(),txtTel.Text);
            LoadData();


        }
        private void cmdNew_Click(object sender, EventArgs e)
        {

            FrmEmployeeDetail frm = new FrmEmployeeDetail();
            frm.ShowDialog();

            mEmployessList = Employees_Data.EmployeeList();

            radioViewListView.Checked = true;
            LoadData();

            ListViewData.Focus();
            string nEmployeeID = frm.mEmployeeID;
            if (nEmployeeID != null && nEmployeeID.Trim().Length > 0)
            {
                Employee_Info nEmployee = new Employee_Info(nEmployeeID);
                SelectIndexInListView(nEmployee.EmployeeKey);
            }

        }

        private void radioViewListView_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = true;
            ReportViewer.Visible = false;
            LoadDataToListView(ListViewData);

        }
        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = false;
            ReportViewer.Visible = true;
            LoadDataToCrystalReport();
        }

        #endregion

        #region [ Function]
        private void SelectIndexInListView(int CustomerKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == CustomerKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercutiry ] 
        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM002");
            if (nRole.Del)
                m_RoleDelete = true;

            if (!nRole.Edit)
                cmdImport.Enabled = false;
        }
        #endregion

        private void PrintOneEmp_Click(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)//bat loi
            {
                int key = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                Employee_Info nEmployee = new Employee_Info(key);
                FrmEmployeeReport frm = new FrmEmployeeReport();
                frm.nEmployeeKey = key;
                frm.ShowDialog();
            }
            else//
            {
                MessageBox.Show("Bạn phải chọn nhân viên");
            }
        }

        private void PrintAllEmp_Click(object sender, EventArgs e)
        {

        }
    }
}



