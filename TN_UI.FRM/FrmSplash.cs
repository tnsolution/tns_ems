﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

using TNLibrary.SYS;
using TNConfig;

namespace TN_UI.FRM
{
    public partial class FrmSplash : Form
    {
        // Process Data
        private Thread mThread;
        public bool IsConnected = false;
        public string strDataBase = "";
        private bool IsThreadStopped = false;
        //FadeIn & FadeOut
        System.Windows.Forms.Timer tmr, tmrFadeIn, tmrFadeOut;
        int Speed = 40;
        double Lowest = 0.0;
        int Wait = 1500;
        double Step = 0.1;

        public FrmSplash()
        {
            InitializeComponent();
        }

        private void FrmSplash_Load(object sender, EventArgs e)
        {
            FadeIn();
        }

        private void FadeIn()
        {
            this.Opacity = Lowest;
            tmrFadeIn = new System.Windows.Forms.Timer();
            tmrFadeIn.Interval = Speed;
            tmrFadeIn.Tick += new EventHandler(tmrFadeIn_Tick);
            tmrFadeIn.Enabled = true;
        }
        private void FadeOut()
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Interval = Wait;
            tmr.Enabled = true;
        }
        void tmrFadeIn_Tick(object sender, EventArgs e)
        {
            if (this.Opacity == 1)
            {
                tmrFadeIn.Enabled = false;
                BeginProcess();
                CheckStatus();
            }
            else
            {
                double t = this.Opacity;
                this.Opacity = Math.Min(1, t + Step);
            }
        }
        void tmr_Tick(object sender, EventArgs e)
        {
            tmrFadeOut = new System.Windows.Forms.Timer();
            tmrFadeOut.Interval = Speed;
            tmrFadeOut.Tick += new EventHandler(tmrFadeOut_Tick);
            tmrFadeOut.Enabled = true;

            tmr.Enabled = false;
        }
        void tmrFadeOut_Tick(object sender, EventArgs e)
        {
            if (this.Opacity <= Lowest)
            {
                tmrFadeOut.Enabled = false;
                this.Dispose();
            }
            else
            {
                this.Opacity -= Step;
            }
        }

        #region [Process File Config]
        System.Windows.Forms.Timer mCheckStatus;
        private void CheckStatus()
        {
            mCheckStatus = new System.Windows.Forms.Timer();
            mCheckStatus.Tick += new EventHandler(CheckStatus_Tick);
            mCheckStatus.Interval = 1500;
            mCheckStatus.Enabled = true;
        }
        void CheckStatus_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 900)
                progressBar1.Value += 100;

            if (IsThreadStopped)
            {
                progressBar1.Value = 1000;
                mCheckStatus.Enabled = false;
                // MessageBox.Show(IsConnected.ToString());
                FadeOut();

            }

        }
        protected void BeginProcess()
        {
            lblMessage.Text = "Starting loading....";
            mThread = new Thread(new ThreadStart(LoadFileConfig));
            mThread.Start();

        }

        protected void LoadFileConfig()
        {
            ViewMessage("Loading config file...");
            RWConfig nConfig = new RWConfig();
            ConnectDataBaseInfo nConnectInfo = new ConnectDataBaseInfo();

            if (nConfig.ReadConfig())
            {
                ViewMessage("Found config file");
                nConnectInfo = nConfig.ConnectInfo;
                
                ViewMessage("Connect to database...");
                ConnectDataBase nConnect = new ConnectDataBase(nConnectInfo.ConnectionString);
                if (nConnect.Message.Length > 0)
                    IsConnected = false;
                else
                {
                    IsConnected = true;
                    if (nConnectInfo.IsConnectLocal)
                        strDataBase = nConnectInfo.DataSource + "->" + nConnectInfo.AttachDbFilename;
                    else
                        strDataBase = nConnectInfo.DataSource + "->" + nConnectInfo.DataBase;
                }
            }
            else
            {
                nConnectInfo.IsConnectLocal = true;

                ViewMessage("Connect to database...");
                ConnectDataBase nConnect = new ConnectDataBase(nConnectInfo.ConnectionString);
                if (nConnect.Message.Length > 0)
                    IsConnected = false;
                else
                {
                    IsConnected = true;
                    strDataBase = nConnectInfo.AttachDbFilename;

                    RWConfig nFileConfig = new RWConfig();
                    nFileConfig.ConnectInfo = nConnectInfo;
                    nFileConfig.SaveConfig();

                    if (nConnectInfo.IsConnectLocal)
                        strDataBase = nConnectInfo.DataSource + "->" + nConnectInfo.AttachDbFilename;
                    else
                        strDataBase = nConnectInfo.DataSource + "->" + nConnectInfo.DataBase;
                }
            }
            ViewMessage("Load UI form...");
            DisplayLang.LoadLanguage("VN");
            IsThreadStopped = true;
            mThread.Abort();

        }
    
        delegate void SetItemExcelCallback(String strMessage);
        private void ViewMessage(String strMessage)
        {
            if (this.lblMessage.InvokeRequired)
            {
                SetItemExcelCallback d = new SetItemExcelCallback(ViewMessage);
                this.Invoke(d, new object[] { strMessage });
            }
            else
            {
                lblMessage.Text = strMessage;

            }
        }
        #endregion
    }
}
