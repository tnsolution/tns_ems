using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC.Bills;
using TNLibrary.FNC.Invoices;

namespace TN_UI.FRM.Bills
{
    public partial class FrmReceiptDetail : Form
    {

        public int mReceiptKey;
        public bool FlagTemp;

        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDate = GlobalSystemConfig.FormatDate;
        string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        public bool FlagTempSaved;
        public Cash_Receipt_Object mReceipt = new Cash_Receipt_Object();
        public FrmReceiptDetail()
        {
            InitializeComponent();
            SetupLayoutGridViewInvoice(GridViewInvoice);
        }

        private void FrmReceiptDetail_Load(object sender, EventArgs e)
        {
            CheckRole();
            TN_Item nFisrtItem = new TN_Item();
            nFisrtItem.Name = "";
            nFisrtItem.Value = "";

            LoadDataToToolbox.AutoCompleteTextBox(txtCurrencyForeign, "SELECT CurrencyID FROM SYS_Currency");
            LoadDataToToolbox.AutoCompleteTextBox(txtCustomerID, "SELECT CustomerID FROM CRM_Customers WHERE IsCustomer = 1 ");

            txtCurrencyMain.Text = mCurrencyMain;

            PickerReceiptDate.CustomFormat = mFormatDate;

            mReceipt = new Cash_Receipt_Object(mReceiptKey);
            LoadCash_Receipt_Info();

            if (FlagTemp)
            {
                DisplayForTemp();
            }

        }

        #region [ Info chung ]
        private bool mReceiptIDChanged = false;
        private void txtReceiptID_TextChanged(object sender, EventArgs e)
        {
            mReceiptIDChanged = true;
        }

        private void txtReceiptID_Validated(object sender, EventArgs e)
        {
            if (mReceiptIDChanged)
            {
                Cash_Receipt_Object nReceipt = new Cash_Receipt_Object(txtReceiptID.Text);
                if (nReceipt.Key > 0)
                {
                    if (MessageBox.Show("Đã có số phiếu thu này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        mReceipt = nReceipt;
                        LoadCash_Receipt_Info();
                    }
                    else
                    {
                        txtReceiptID.Focus();
                    }
                }

            }
        }

        private void LoadCash_Receipt_Info()
        {
            txtReceiptID.Text = mReceipt.ID;
            txtReceiptDescription.Text = mReceipt.Description;
            PickerReceiptDate.Value = mReceipt.BillDate;

            txtDepositor.Text = mReceipt.Depositor;
            txtAddress.Text = mReceipt.Address;
            if (mReceipt.Key == 0)
                txtBranchName.Text = SessionUser.UserLogin.BranchName;
            else
                txtBranchName.Text = mReceipt.BranchName;

            if (mReceipt.CustomerKey > 0)
            {
                mCustomer = new Customer_Info(mReceipt.CustomerKey);
                txtCustomerID.Text = mCustomer.ID;
                CoCustomers.Text = mCustomer.Name;
            }
           // CoAccountStocks.SelectedValue =  mReceipt.AccountStockKey;

            txtAmountCurrencyForeign.Text = mReceipt.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            txtCurrencyForeign.Text = mReceipt.CurrencyIDForeign;
            txtCurrencyRate.Text = mReceipt.CurrencyRate.ToString();

            txtAmountCurrencyMain.Text = mReceipt.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            LoadInvoiceDetailToGridView(mReceipt.InvoicesPaid);

            mReceiptIDChanged = false;
        }

        private void ClearCash_Receipt_Info()
        {

            mCustomer = new Customer_Info();
            mReceipt = new Cash_Receipt_Object();

            txtBranchName.Text = SessionUser.UserLogin.BranchName;
            txtReceiptID.Text = "";
            txtReceiptDescription.Text = "";

            txtDepositor.Text = "";
            txtAddress.Text = "";

            txtCustomerID.Text = "";
            txtCustomerID.Enabled = true;

            CoCustomers.Text = "";
            CoCustomers.Enabled = true;


            txtAmountCurrencyForeign.Text = "0";
            txtCurrencyRate.Text = "1";

            txtAmountCurrencyMain.Text = "0";


            GridViewInvoice.Rows.Clear();
            for (int i = 1; i <= 6; i++)
            {
                GridViewInvoice.Rows.Add();
            }

            txtReceiptID.Focus();
        }

        #endregion

        #region [ Invoice Info ]

        public void SetupLayoutGridViewInvoice(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("InvoiceNumber", "Số hóa đơn");
            GV.Columns.Add("InvoiceDate", "Ngày");
            GV.Columns.Add("CustomerName", "Công ty");
            GV.Columns.Add("ReceiptAmount", "Số tiền");


            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 120;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


            for (int i = 1; i <= 6; i++)
            {
                GV.Rows.Add();
            }
        }
        private void GridViewInvoice_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GridViewInvoice.CurrentCell.ColumnIndex)
            {
                case 0: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT InvoiceNumber FROM FNC_Invoice_Sales");

                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 3: // So Tien
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

            }
        }
        private void GridViewInvoice_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowEdit = GridViewInvoice.Rows[e.RowIndex];
            if (nRowEdit.Cells[e.ColumnIndex].Value == null)
                return;

            switch (e.ColumnIndex)
            {
                case 0:
                    InvoiceInfo(nRowEdit.Cells[0].Value.ToString(), e.RowIndex);
                    break;
                case 3: // thong tin ve thanh tien 
                    double nReceiptAmount = 0;
                    if (double.TryParse(nRowEdit.Cells["ReceiptAmount"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nReceiptAmount))
                    {
                        nRowEdit.Cells["ReceiptAmount"].Value = nReceiptAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                        nRowEdit.Cells["ReceiptAmount"].ErrorText = "";
                    }
                    else
                        nRowEdit.Cells["ReceiptAmount"].ErrorText = "Sai định dạng tiền tệ";
                    break;


            }
        }
        private void GridViewInvoice_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            int nRowIndex = GridViewInvoice.CurrentRow.Index;
            if (!IsRowEmpty(GridViewInvoice, nRowIndex))
            {
                CheckInvoiceRowError(nRowIndex);
            }
            CaculateInvoice();
        }
        private void GridViewInvoice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                GridViewInvoice.Rows.Remove(GridViewInvoice.CurrentRow);
                GridViewInvoice.Rows.Add();
            }
        }

        private void InvoiceInfo(string InvoiceNumber, int RowCurrency)
        {
            Invoice_Sale_Info nInvoice = new Invoice_Sale_Info();

            if (InvoiceNumber.Length == 0)
                return;
            if (IsHasInvoice(InvoiceNumber, RowCurrency))
            {
                GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "đã có hóa đơn này";
                return;
            }

            int nInvoiceNumbers = Invoice_Data.CountSaleInvoiceNumber(InvoiceNumber);

            if (nInvoiceNumbers == 0)
            {
                GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "Không có hóa đơn này";
                return;
            }

            if (nInvoiceNumbers == 1)
            {
                nInvoice.GetInvoice_Sale_Info(InvoiceNumber);
            }
            else
            {
                //FrmListInvoiceInfo frm = new FrmListInvoiceInfo();
                //frm.mInvoiceNumber = InvoiceNumber;
                //frm.mType = 0; // all invoice
                //frm.ShowDialog();
                //int nInvoiceKey = frm.mInvoiceKey;
                //frm.Close();
                //nInvoice = new SaleInvoiceInfo(nInvoiceKey);
            }
            double nAmountRemain = nInvoice.AmountCurrencyMain - nInvoice.AmountPayed;

            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].Tag = nInvoice.InvoiceKey;
            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceDate"].Value = nInvoice.InvoiceDate.ToString(mFormatDate);
            GridViewInvoice.Rows[RowCurrency].Cells["CustomerName"].Value = nInvoice.CustomerName;
            GridViewInvoice.Rows[RowCurrency].Cells["ReceiptAmount"].Value = nAmountRemain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "";
            GridViewInvoice.Rows[RowCurrency].Cells["ReceiptAmount"].ErrorText = "";


        }
        private bool IsHasInvoice(string InvoiceNumber, int nRowCurrency)
        {
            foreach (DataGridViewRow nRow in GridViewInvoice.Rows)
            {
                if (nRow.Cells["InvoiceNumber"].Value != null)
                {
                    if (nRow.Index != nRowCurrency && nRow.Cells["InvoiceNumber"].Value.ToString().Trim() == InvoiceNumber)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void CaculateInvoice()
        {
            double nSumAmountInvoice = 0;
            foreach (DataGridViewRow nRow in GridViewInvoice.Rows)
            {
                if (!IsRowEmpty(GridViewInvoice, nRow.Index) && !IsRowError(GridViewInvoice, nRow.Index))
                {
                    nSumAmountInvoice += double.Parse(nRow.Cells["ReceiptAmount"].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumAmountInvoice.Text = nSumAmountInvoice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void CheckInvoiceRowError(int RowIndex)
        {
            DataGridViewRow nRow = GridViewInvoice.Rows[RowIndex];

            if (nRow.Cells["InvoiceNumber"].Value == null && nRow.Cells["ReceiptAmount"].Value == null)
            {
                nRow.Cells["InvoiceNumber"].ErrorText = "";
                nRow.Cells["ReceiptAmount"].ErrorText = "";
                return;
            }
            else
            {
                if (nRow.Cells["InvoiceNumber"].Value == null)
                    nRow.Cells["InvoiceNumber"].ErrorText = "Error";
                if (nRow.Cells["ReceiptAmount"].Value == null)
                    nRow.Cells["ReceiptAmount"].ErrorText = "Error";
            }
        }

        private void LoadInvoiceDetailToGridView(DataTable InvoicePaid)
        {
            double nSumAmount = 0;
            int i = 0;
            foreach (DataRow nRow in InvoicePaid.Rows)
            {

                DataGridViewRow nRowView = GridViewInvoice.Rows[i];
                DateTime nDateTime = (DateTime)nRow["InvoiceDate"];
                double nReceiptAmount = double.Parse(nRow["ReceiptAmount"].ToString());
                nRowView.Cells["InvoiceNumber"].Value = nRow["InvoiceNumber"].ToString();
                nRowView.Cells["InvoiceNumber"].Tag = nRow["InvoiceKey"];
                nRowView.Cells["InvoiceDate"].Value = nDateTime.ToString(mFormatDate);
                nRowView.Cells["CustomerName"].Value = nRow["CustomerName"].ToString();
                nRowView.Cells["ReceiptAmount"].Value = nReceiptAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

                i++;
                nSumAmount += nReceiptAmount;
            }
            for (int k = i; k <= 6; k++)
            {
                DataGridViewRow nRow = GridViewInvoice.Rows[k];
                nRow.Cells["InvoiceNumber"].Value = null;
                nRow.Cells["InvoiceNumber"].Tag = 0;
                nRow.Cells["InvoiceDate"].Value = null;
                nRow.Cells["CustomerName"].Value = null;
                nRow.Cells["ReceiptAmount"].Value = null;

            }
            txtSumAmountInvoice.Text = nSumAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void GetListListInvoiceInGridView()
        {
            mReceipt.InvoicesPaid.Clear();

            for (int i = 0; i < GridViewInvoice.Rows.Count; i++)
            {
                if (!IsRowEmpty(GridViewInvoice, i) && !IsRowError(GridViewInvoice, i))
                {
                    DataRow nRow = mReceipt.InvoicesPaid.NewRow();

                    DataGridViewRow nRowView = GridViewInvoice.Rows[i];
                    nRow["ReceiptKey"] = mReceipt.Key;
                    nRow["InvoiceKey"] = int.Parse(nRowView.Cells["InvoiceNumber"].Tag.ToString());
                    nRow["ReceiptAmount"] = double.Parse(nRowView.Cells["ReceiptAmount"].Value.ToString(), mFormatProviderCurrency);

                    mReceipt.InvoicesPaid.Rows.Add(nRow);
                }
            }

        }
        #endregion

        #region [ Customer Infomation]
        private Customer_Info mCustomer = new Customer_Info();
        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            if (txtCustomerID.Text.Trim() == "*")
            {
                LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1", true);
            }
            else
            {
                mCustomer = new Customer_Info(txtCustomerID.Text.Trim());
                LoadInfoCustomer();
                if (mCustomer.Key == 0 && txtCustomerID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1 AND CustomerID LIKE '" + txtCustomerID.Text.Trim() + "%'", true);

            }
        }
        private void txtCustomerID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                FRM.CRM.FrmCustomerList frm = new FRM.CRM.FrmCustomerList();
                frm.FlagView = true;
                frm.ShowDialog();
                if (frm.ListViewData.SelectedItems.Count > 0)
                    txtCustomerID.Text = frm.ListViewData.SelectedItems[0].Text;
                frm.Close();
            }
        }



        private void CoCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nCustomerKey;
            if (int.TryParse(CoCustomers.SelectedValue.ToString(), out nCustomerKey))
            {
                mCustomer = new Customer_Info(nCustomerKey);
                LoadInfoCustomer();
            }

        }
        private void LoadInfoCustomer()
        {
            if (mCustomer.Key == 0)
            {
                CoCustomers.Text = "...";
                txtAddress.Text = "...";

            }
            else
            {
                txtCustomerID.Text = mCustomer.ID.Trim();
                CoCustomers.Text = mCustomer.Name;
                txtAddress.Text = mCustomer.Address + ", " + mCustomer.CityName;

            }
        }
        #endregion

        #region [ Process Input & Outpur ]
        private void txtAmountCurrencyForeign_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtAmountCurrencyMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtCurrencyMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtBankVAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtBankFee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }




        private void txtAmountCurrencyForeign_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtAmountCurrencyForeign.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;

            txtAmountCurrencyForeign.Text = Amount.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            CaculateAmountMain();
        }
        private void txtCurrencyRate_Leave(object sender, EventArgs e)
        {
            CaculateAmountMain();
        }
        private void txtAmountCurrencyMain_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtAmountCurrencyMain.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;

            txtAmountCurrencyMain.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }

        #endregion

        #region [ Function ]
        private bool CheckBeforeSave()
        {
            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerReceiptDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerReceiptDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

            if (txtReceiptID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtReceiptID.Focus();
                return false;
            }
            //if (txtCustomerID.Text.Trim().Length == 0)//
            //{
            //    MessageBox.Show("Bạn phải nhập khách hàng");
            //    txtCustomerID.Focus();
            //    return false;
            //}
            double nAmount = 0;
            double.TryParse(txtAmountCurrencyMain.Text, NumberStyles.Any, mFormatProviderCurrency, out nAmount);
            if (nAmount == 0)
            {
                MessageBox.Show("Bạn phải nhập số tiền vào");
                txtAmountCurrencyMain.Focus();
                return false;
            }

            if (GridViewIsHasError(GridViewInvoice))
            {
                MessageBox.Show("Kiểm tra lại thông tin hóa đơn");
                return false;
            }
            return true;
        }
        private void CaculateAmountMain()
        {
            double nAmountCurrencyForeign = double.Parse(txtAmountCurrencyForeign.Text, mFormatProviderCurrency);
            double nRate = 0;
            double.TryParse(txtCurrencyRate.Text, out nRate);
            double nAmountCurrencyMain = nAmountCurrencyForeign * nRate;

            txtAmountCurrencyMain.Text = nAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void DisplayForTemp()
        {
            //txtVendorID.Enabled = false;
            //CoVendors.Enabled = false;
            cmdSaveNew.Enabled = false;

        }
        #endregion

        #region [ Update Invoice DataBase]
        private bool SaveCash_Receipt_Infomation()
        {
            if (!CheckBeforeSave()) return false;

            mReceipt.ID = txtReceiptID.Text.Trim();
            mReceipt.BillDate = PickerReceiptDate.Value;
            mReceipt.Description = txtReceiptDescription.Text;

            mReceipt.Depositor = txtDepositor.Text;
            mReceipt.Address = txtAddress.Text;

            mReceipt.BranchKey = SessionUser.UserLogin.BranchKey;
            mReceipt.AccountStockKey = "";// CoAccountStocks.SelectedValue.ToString();

            if (txtCustomerID.Enabled == false)
                mReceipt.CustomerKey = 0;
            else
                mReceipt.CustomerKey = mCustomer.Key;

            mReceipt.AmountCurrencyForeign = double.Parse(txtAmountCurrencyForeign.Text, mFormatProviderCurrency);
            mReceipt.CurrencyIDForeign = txtCurrencyForeign.Text;
            mReceipt.CurrencyRate = double.Parse(txtCurrencyRate.Text);

            mReceipt.AmountCurrencyMain = double.Parse(txtAmountCurrencyMain.Text, mFormatProviderCurrency);

            mReceipt.BranchKey = SessionUser.UserLogin.BranchKey;
            mReceipt.CreatedBy = SessionUser.UserLogin.Key;
            mReceipt.ModifiedBy = SessionUser.UserLogin.Key;


            GetListListInvoiceInGridView();

            return true;


        }

        #endregion

        #region [ Button Activate ]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (FlagTemp)
            {
                if (SaveCash_Receipt_Infomation())
                {
                    FlagTempSaved = true;
                    this.Close();
                }

            }
            else
            {
                if (SaveCash_Receipt_Infomation())
                {
                    mReceipt.SaveObject();
                    this.Close();
                }
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveCash_Receipt_Infomation())
            {
                mReceipt.SaveObject();
                string strID = mReceipt.ID.Trim();
                ClearCash_Receipt_Info();
                txtReceiptID.Text = Cash_Bills_Data.GetAutoReceiptID(strID);

            }
        }

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa phiếu thu này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mReceipt.Delete();
                ClearCash_Receipt_Info();
            }
        }

        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmReceiptReport frm = new FrmReceiptReport();
            SaveCash_Receipt_Infomation();
            frm.mReceipt = mReceipt;
            frm.ShowDialog();
        }
        #endregion

        #region [ Function For DatagridView]
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private bool GridViewIsHasError(DataGridView GV)
        {
            int n = GV.ColumnCount;
            foreach (DataGridViewRow nRow in GV.Rows)
            {
                for (int i = 0; i < n; i++)
                {
                    if (nRow.Cells[i].ErrorText.Length > 0)
                        return true;
                }
            }
            return false;
        }
        private bool IsRowEmpty(DataGridView GV, int RowIndex)
        {
            for (int i = 0; i < GV.ColumnCount; i++)
            {
                if (GV.Rows[RowIndex].Cells[i].Value != null)
                    return false;
            }
            return true;
        }
        private bool IsRowError(DataGridView GV, int RowIndex)
        {
            for (int i = 0; i < GV.ColumnCount; i++)
            {
                if (GV.Rows[RowIndex].Cells[i].ErrorText.Length > 0)
                    return true;
            }
            return false;
        }

        #endregion

        #region [ Short Key ]
        private void FrmReceiptDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC004");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

       
    }
}