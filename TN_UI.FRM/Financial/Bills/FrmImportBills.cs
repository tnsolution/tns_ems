using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC.Bills;

namespace TN_UI.FRM.Bills
{
    public partial class FrmImportBills : Form
    {

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private IFormatProvider mFormatProviderDate = GlobalSystemConfig.FormatProviderDate;
        private string mFormatDate = GlobalSystemConfig.FormatDate;

        public FrmImportBills()
        {
            InitializeComponent();

        }
        private void FrmImportBills_Load(object sender, EventArgs e)
        {
            CheckRole();
            txtID.Text = "PC" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "/11/001";

            SetupLayoutGridView(dataGridView1);
            LoadDataToToolbox.ComboBoxData(CoBranchs, "SELECT BranchKey,BranchName FROM HRM_Branchs", false);
            CoBranchs.SelectedIndex = 0;

            panelSearch.Height = 00;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            //  this.WindowState = FormWindowState.Maximized;
            cmdUnhideCenter_Click(null, EventArgs.Empty);
            //SearchOrder();
        }


        #region [ GridView ]
        private bool FlagEdit;
        private void SetupLayoutGridView(DataGridView GV)
        {
            GV.Columns.Add("PaymentID", "");
            GV.Columns.Add("PaymentDate", "");
            GV.Columns.Add("WhoContact", "");
            GV.Columns.Add("Description", "");
            GV.Columns.Add("AmountCurrencyMain", "");

            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[2].Width = 200;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 300;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[3].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns[4].Width = 100;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            //GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * (3 / 2);

            GV.Dock = DockStyle.Fill;
            //Activate On Gridview
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.RowLeave += new DataGridViewCellEventHandler(GV_RowLeave);
            GV.KeyDown += new KeyEventHandler(this.GV_KeyDown);

            GV.Paint += new PaintEventHandler(GV_Paint);

            GV.Rows.Add(100);
        }
        private void GV_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            string[] TitleColumn = { "Mã phiếu", "Ngày", "Người nhận/đưa", "Diễn Giải", "Số tiền" };

            int nColumnTotal = GV.ColumnCount;

            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;


                r1.Width = r1.Width - 2;
                r1.Height = r1.Height - 2;
                g.DrawImage(image, r1);

                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }

            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
            FlagEdit = false;
        }
        private void GV_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
            FlagEdit = false;

        }
        private void GV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                for (int i = 0; i < dataGridView1.SelectedCells.Count; i++)
                {
                    dataGridView1.SelectedCells[i].Value = "";
                }

            }
        }

        private void PasteClipboard(DataGridView GV)
        {
            try
            {
                string s = Clipboard.GetText();
                string[] nLines = s.Split('\n');
                int iFail = 0, nRow = GV.CurrentCell.RowIndex;
                int nCol = GV.CurrentCell.ColumnIndex;
                foreach (string nLine in nLines)
                {
                    if (nLine.Length > 0)
                    {
                        string[] nCells = nLine.Split('\t');

                        for (int k = 0; k < nCells.Length; k++)
                        {
                            if (nCol + k >= GV.Columns.Count)
                                break;
                            else
                                GV.Rows[nRow].Cells[nCol + k].Value = nCells[k].ToString().Trim();

                        }
                        GV.AutoResizeRow(nRow);
                        nRow++;
                    }
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("The data you pasted is in the wrong format for the cell");
            }

        }
        #endregion

        #region [ Design Layout]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }

        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 95;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        #region [ Sercutiry ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC005");
        }
        #endregion

    
        private void FrmImportBills_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.V))
            {
                if (mRole.Edit)
                    PasteClipboard(dataGridView1);
            }
        }

     

        #region [ Function ]
        private bool IsRowEmpty(DataGridViewRow RowCheck)
        {
            bool nIsRowEmpty = true;
            foreach (DataGridViewCell nCell in RowCheck.Cells)
            {
                if (nCell.Value != null && nCell.Value.ToString().Length > 0)
                {
                    nIsRowEmpty = false;
                    break;
                }
            }
            return nIsRowEmpty;
        }
        private bool IsRowError(DataGridViewRow RowCheck)
        {
            bool nIsRowError = false;
            foreach (DataGridViewCell nCell in RowCheck.Cells)
            {
                if (nCell.ErrorText.ToString().Length > 0)
                {
                    nIsRowError = true;
                    break;
                }
            }
            return nIsRowError;
        }
        private void CheckBeforeSave(DataGridViewRow RowCheck)
        {
            if (RowCheck.Cells["PaymentID"].Value == null)
            {
                RowCheck.Cells["PaymentID"].ErrorText = "Xin vui lòng nhập Mã";
            }
            else
            {
                RowCheck.Cells["PaymentID"].ErrorText = "";
            }
            DateTime nPaymentDate;
            if (RowCheck.Cells["PaymentDate"].Value == null)
            {
                RowCheck.Cells["PaymentDate"].ErrorText = "Sai định dạng ngày";
            }
            else
            {
                if (DateTime.TryParseExact(RowCheck.Cells["PaymentDate"].Value.ToString(), mFormatDate, mFormatProviderDate, DateTimeStyles.None, out nPaymentDate))
                {
                    RowCheck.Cells["PaymentDate"].ErrorText = "";
                    RowCheck.Cells["PaymentDate"].Tag = nPaymentDate;
                }
                else
                {
                    RowCheck.Cells["PaymentDate"].ErrorText = "Sai định dạng ngày";
                }

            }
            if (RowCheck.Cells["WhoContact"].Value == null)
                RowCheck.Cells["WhoContact"].Value = "";
            if (RowCheck.Cells["Description"].Value == null)
                RowCheck.Cells["Description"].Value = "";
            double nAmountCurrencyMain = 0;
            if (RowCheck.Cells["AmountCurrencyMain"].Value == null ||  !double.TryParse(RowCheck.Cells["AmountCurrencyMain"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nAmountCurrencyMain))
            {
                RowCheck.Cells["AmountCurrencyMain"].ErrorText = "Sai định dạng tiền";
            }
            else
            {
                RowCheck.Cells["AmountCurrencyMain"].ErrorText = "";
                RowCheck.Cells["AmountCurrencyMain"].Tag = nAmountCurrencyMain;

            }
        }
        private void UpdateID(string IDLeft, string IDRight, int NumberBegin)
        {
            foreach (DataGridViewRow nRow in dataGridView1.Rows)
            {
                if (!IsRowEmpty(nRow))
                {
                    string ID = IDLeft + NumberBegin.ToString().PadLeft(IDRight.Length, '0');

                    DataGridViewCell nCell = nRow.Cells[0];
                    nCell.Value = ID;
                    NumberBegin++;
                }
            }
        }
        #endregion

        #region [Action Button ]
        private void cmdCheckBeforeSave_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow nRow in dataGridView1.Rows)
            {
                if (!IsRowEmpty(nRow))
                    CheckBeforeSave(nRow);
            }
        }

        //Payment
        private void cmdGetAutoPaymentID_Click(object sender, EventArgs e)
        {
            string nIDLeft = "";
            string nIDRight = "";
            int nNumberBegin = 0;

            Cash_Bills_Data.GetAutoPaymentID(txtID.Text, out nIDLeft, out nIDRight, out nNumberBegin);
            UpdateID(nIDLeft, nIDRight, nNumberBegin);
        }

        private void cmdSavePayment_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow nRow in dataGridView1.Rows)
            {
                if (!IsRowEmpty(nRow))
                {
                    CheckBeforeSave(nRow); ;
                    if (!IsRowError(nRow))
                    {
                        SavePaymentInfo(nRow);
                    }
                }
            }
        }
        
        private void SavePaymentInfo(DataGridViewRow RowUpdate)
        {

            Cash_Payment_Info nPayment = new Cash_Payment_Info();
            nPayment.ID = RowUpdate.Cells["PaymentID"].Value.ToString();
            nPayment.BillDate = (DateTime)RowUpdate.Cells["PaymentDate"].Tag;
            nPayment.Receiver = RowUpdate.Cells["WhoContact"].Value.ToString();
            nPayment.Description = RowUpdate.Cells["Description"].Value.ToString();
            nPayment.AmountCurrencyMain = (double)RowUpdate.Cells["AmountCurrencyMain"].Tag;
            nPayment.CurrencyIDForeign = mCurrencyMain;
            nPayment.BranchKey = (int)CoBranchs.SelectedValue;

            nPayment.CreatedBy = SessionUser.UserLogin.Key;
            nPayment.ModifiedBy = SessionUser.UserLogin.Key;

            nPayment.Save();
            for (int i = 0; i < RowUpdate.Cells.Count; i++)
            {
                RowUpdate.Cells[i].Value = "";
            }

        }

        //Receipts
        private void cmdGetAutoReceiptID_Click(object sender, EventArgs e)
        {
            string nIDLeft = "";
            string nIDRight = "";
            int nNumberBegin = 0;

            Cash_Bills_Data.GetAutoReceiptID(txtID.Text, out nIDLeft, out nIDRight, out nNumberBegin);
            UpdateID(nIDLeft, nIDRight, nNumberBegin);
        }

        private void cmdSaveReceipt_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow nRow in dataGridView1.Rows)
            {
                if (!IsRowEmpty(nRow))
                {
                    CheckBeforeSave(nRow); ;
                    if (!IsRowError(nRow))
                    {
                        SaveReceiptInfo(nRow);
                    }
                }
            }
        }
        private void SaveReceiptInfo(DataGridViewRow RowUpdate)
        {

            Cash_Receipt_Info nReceipt = new Cash_Receipt_Info();
            nReceipt.ID = RowUpdate.Cells["PaymentID"].Value.ToString();
            nReceipt.BillDate = (DateTime)RowUpdate.Cells["PaymentDate"].Tag;
            nReceipt.Depositor = RowUpdate.Cells["WhoContact"].Value.ToString();
            nReceipt.Description = RowUpdate.Cells["Description"].Value.ToString();
            nReceipt.AmountCurrencyMain = (double)RowUpdate.Cells["AmountCurrencyMain"].Tag;
            nReceipt.CurrencyIDForeign = mCurrencyMain;
            nReceipt.BranchKey = (int)CoBranchs.SelectedValue;

            nReceipt.CreatedBy = SessionUser.UserLogin.Key;
            nReceipt.ModifiedBy = SessionUser.UserLogin.Key;

            nReceipt.Save();
            for (int i = 0; i < RowUpdate.Cells.Count; i++)
            {
                RowUpdate.Cells[i].Value = "";
            }

        }

        #endregion

     
    }
}