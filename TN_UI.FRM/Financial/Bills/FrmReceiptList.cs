using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;

using TNLibrary.SYS.Forms;
using TNLibrary.CRM;

using TNLibrary.FNC.Bills;

namespace TN_UI.FRM.Bills
{
    public partial class FrmReceiptList : Form
    {
        public string mReceiptID = "";
        public bool FlagView;
        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDate = GlobalSystemConfig.FormatDate;

        public FrmReceiptList()
        {
            InitializeComponent();
        }

        private void FrmBillReceiptList_Load(object sender, EventArgs e)
        {

            InitListView();
            CheckRole();

            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            PickerToDay.CustomFormat = mFormatDate;
            PickerFromDay.CustomFormat = mFormatDate;
            PickerFromDay.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            LoadDataToToolbox.AutoCompleteTextBox(txtCustomerID, "SELECT CustomerID FROM CRM_Customers WHERE IsCustomer = 1 ");

            SearchReceipts();

            panelSearch.Height = 90;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;

            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;
        }
        private void SearchReceipts()
        {
            DataTable nTable = new DataTable();
            mRole.Check_Role("SYS006");
         //   if (mRole.Read)
                nTable = Cash_Bills_Data.ReceiptList(PickerFromDay.Value, PickerToDay.Value, txtCustomerID.Text, txtCustomerName.Text);
         //   else
         //       nTable = Cash_Bills_Data.ReceiptList(SessionUser.UserLogin.BranchKey, PickerFromDay.Value, PickerToDay.Value, txtCustomerID.Text, txtCustomerName.Text);

            LoadDataFields(ListViewData, nTable);

        }
        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView()
        {

            ColumnHeader colHead;
            // First header
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            // Second header
            colHead = new ColumnHeader();
            colHead.Text = "Mã phiếu thu";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            // Second header
            colHead = new ColumnHeader();
            colHead.Text = "Mã chứng từ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            // Third header
            colHead = new ColumnHeader();
            colHead.Text = "Diễn giải";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng tiền";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            ListViewData.SmallImageList = SmallImageLV;

            LVGroup = new ListViewMyGroup(ListViewData);

        }

        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            int nReceiptKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
            if (FlagView)
            {
                mReceiptID = ListViewData.SelectedItems[0].SubItems[2].Text;
                this.Close();
            }
            else
            {
                FrmReceiptDetail frm = new FrmReceiptDetail();
                frm.mReceiptKey = nReceiptKey;
                frm.ShowDialog();

                SearchReceipts();
                SelectIndexInListView(nReceiptKey);
            }

        }

        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)//
                {

                    if (MessageBox.Show("Bạn có muốn xóa phiếu chi này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        int nIndex = ListViewData.SelectedItems[0].Index;
                        Cash_Receipt_Object nReceipt = new Cash_Receipt_Object();
                        nReceipt.Key = (int)ListViewData.SelectedItems[0].Tag;
                        nReceipt.DeleteObject();
                        ListViewData.SelectedItems[0].Remove();
                        if (ListViewData.Items.Count > 0)
                            if (nIndex == 0)
                                ListViewData.Items[nIndex].Selected = true;
                            else
                                ListViewData.Items[nIndex - 1].Selected = true;
                        ListViewData.Focus();
                    }

                }
                else//
                {
                    MessageBox.Show("Bạn phải chọn phiếu để xóa.");
                }
            }
        }
        public void LoadDataFields(ListView LV, DataTable nTable)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = nTable.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nTable.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = " ";
                lvi.Tag = nRow["ReceiptKey"]; // Set the tag to 

                lvi.ImageIndex = 0;
                DateTime nReceiptDate = (DateTime)nRow["ReceiptDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nReceiptDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ReceiptID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";//nRow["VoucherID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ReceiptDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }
            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }


        }
        #endregion

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 90;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        #region  [ Button Activate ]
        private void cmdEdit_Click(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                int nReceiptKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                FrmReceiptDetail frm = new FrmReceiptDetail();
                frm.mReceiptKey = nReceiptKey;
                frm.ShowDialog();
                SearchReceipts();
                SelectIndexInListView(nReceiptKey);
            }
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {

            SearchReceipts();
        }

        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            FrmReceiptDetail frm = new FrmReceiptDetail();
            frm.ShowDialog();
            SearchReceipts();
        }

        #endregion

        #region [ Function ]
        private void SelectIndexInListView(int ReceiptKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == ReceiptKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercutiry ]
        private bool m_RoleDelete = false;
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC004");
            if (mRole.Del)
                m_RoleDelete = true;

        }
        #endregion

        private void cmdPrintAll_Click(object sender, EventArgs e)
        {
            FrmPrintAllReceipt frm = new FrmPrintAllReceipt();
            frm.ListReceiptKey = new ArrayList();
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                frm.ListReceiptKey.Add(ListViewData.Items[i].Tag);
            }
            frm.ShowDialog();
        }
    }
}