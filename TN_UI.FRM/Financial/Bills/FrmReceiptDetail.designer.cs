namespace TN_UI.FRM.Bills
{
    partial class FrmReceiptDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReceiptDetail));
            this.label1 = new System.Windows.Forms.Label();
            this.PickerReceiptDate = new System.Windows.Forms.DateTimePicker();
            this.txtReceiptID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDepositor = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReceiptDescription = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAmountCurrencyMain = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            this.CoCustomers = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAmountCurrencyForeign = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdSaveClose = new System.Windows.Forms.ToolStripButton();
            this.cmdSaveNew = new System.Windows.Forms.ToolStripButton();
            this.cmdDel = new System.Windows.Forms.ToolStripButton();
            this.cmdPrinter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtCurrencyMain = new System.Windows.Forms.TextBox();
            this.txtCurrencyForeign = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtSumAmountInvoice = new System.Windows.Forms.TextBox();
            this.GridViewInvoice = new System.Windows.Forms.DataGridView();
            this.lbInvoiceAmount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDebitNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCreditNo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBranchName = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewInvoice)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(167, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 50;
            this.label1.Text = "Ngày ";
            // 
            // PickerReceiptDate
            // 
            this.PickerReceiptDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PickerReceiptDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerReceiptDate.Location = new System.Drawing.Point(217, 39);
            this.PickerReceiptDate.Name = "PickerReceiptDate";
            this.PickerReceiptDate.Size = new System.Drawing.Size(107, 21);
            this.PickerReceiptDate.TabIndex = 0;
            // 
            // txtReceiptID
            // 
            this.txtReceiptID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtReceiptID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReceiptID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtReceiptID.ForeColor = System.Drawing.Color.Navy;
            this.txtReceiptID.Location = new System.Drawing.Point(434, 17);
            this.txtReceiptID.Name = "txtReceiptID";
            this.txtReceiptID.Size = new System.Drawing.Size(116, 21);
            this.txtReceiptID.TabIndex = 1;
            this.txtReceiptID.TextChanged += new System.EventHandler(this.txtReceiptID_TextChanged);
            this.txtReceiptID.Validated += new System.EventHandler(this.txtReceiptID_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(369, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Số phiếu ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(11, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 15);
            this.label4.TabIndex = 61;
            this.label4.Text = "Lý do thu";
            // 
            // txtDepositor
            // 
            this.txtDepositor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtDepositor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtDepositor.ForeColor = System.Drawing.Color.Navy;
            this.txtDepositor.Location = new System.Drawing.Point(102, 44);
            this.txtDepositor.Name = "txtDepositor";
            this.txtDepositor.Size = new System.Drawing.Size(438, 21);
            this.txtDepositor.TabIndex = 6;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCustomerID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtCustomerID.ForeColor = System.Drawing.Color.Navy;
            this.txtCustomerID.Location = new System.Drawing.Point(102, 18);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(106, 21);
            this.txtCustomerID.TabIndex = 4;
            this.txtCustomerID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCustomerID_KeyDown);
            this.txtCustomerID.Leave += new System.EventHandler(this.txtCustomerID_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(11, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 15);
            this.label2.TabIndex = 80;
            this.label2.Text = "Khách hàng";
            // 
            // txtReceiptDescription
            // 
            this.txtReceiptDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtReceiptDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtReceiptDescription.ForeColor = System.Drawing.Color.Navy;
            this.txtReceiptDescription.Location = new System.Drawing.Point(102, 100);
            this.txtReceiptDescription.Name = "txtReceiptDescription";
            this.txtReceiptDescription.Size = new System.Drawing.Size(438, 21);
            this.txtReceiptDescription.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(11, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 84;
            this.label6.Text = "Người đưa";
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtAddress.ForeColor = System.Drawing.Color.Navy;
            this.txtAddress.Location = new System.Drawing.Point(102, 72);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(438, 21);
            this.txtAddress.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(11, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 15);
            this.label8.TabIndex = 88;
            this.label8.Text = "Địa chỉ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(11, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 116;
            this.label5.Text = "Số Tiền";
            // 
            // txtAmountCurrencyMain
            // 
            this.txtAmountCurrencyMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAmountCurrencyMain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountCurrencyMain.ForeColor = System.Drawing.Color.Navy;
            this.txtAmountCurrencyMain.Location = new System.Drawing.Point(102, 179);
            this.txtAmountCurrencyMain.Name = "txtAmountCurrencyMain";
            this.txtAmountCurrencyMain.Size = new System.Drawing.Size(379, 21);
            this.txtAmountCurrencyMain.TabIndex = 12;
            this.txtAmountCurrencyMain.Text = "0";
            this.txtAmountCurrencyMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmountCurrencyMain.Leave += new System.EventHandler(this.txtAmountCurrencyMain_Leave);
            this.txtAmountCurrencyMain.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmountCurrencyMain_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(11, 153);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 15);
            this.label12.TabIndex = 102;
            this.label12.Text = "Tỷ giá";
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCurrencyRate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.Navy;
            this.txtCurrencyRate.Location = new System.Drawing.Point(102, 153);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.Size = new System.Drawing.Size(438, 21);
            this.txtCurrencyRate.TabIndex = 11;
            this.txtCurrencyRate.Text = "0";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.Leave += new System.EventHandler(this.txtCurrencyRate_Leave);
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // CoCustomers
            // 
            this.CoCustomers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.CoCustomers.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.CoCustomers.ForeColor = System.Drawing.Color.Navy;
            this.CoCustomers.FormattingEnabled = true;
            this.CoCustomers.Location = new System.Drawing.Point(216, 18);
            this.CoCustomers.Name = "CoCustomers";
            this.CoCustomers.Size = new System.Drawing.Size(324, 23);
            this.CoCustomers.TabIndex = 5;
            this.CoCustomers.SelectedIndexChanged += new System.EventHandler(this.CoCustomers_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(416, 129);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 15);
            this.label11.TabIndex = 96;
            this.label11.Text = "Loại Tiền";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(11, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 15);
            this.label10.TabIndex = 95;
            this.label10.Text = "Ngoại Tệ";
            // 
            // txtAmountCurrencyForeign
            // 
            this.txtAmountCurrencyForeign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAmountCurrencyForeign.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountCurrencyForeign.ForeColor = System.Drawing.Color.Navy;
            this.txtAmountCurrencyForeign.Location = new System.Drawing.Point(102, 127);
            this.txtAmountCurrencyForeign.Name = "txtAmountCurrencyForeign";
            this.txtAmountCurrencyForeign.Size = new System.Drawing.Size(308, 21);
            this.txtAmountCurrencyForeign.TabIndex = 9;
            this.txtAmountCurrencyForeign.Text = "0";
            this.txtAmountCurrencyForeign.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmountCurrencyForeign.Leave += new System.EventHandler(this.txtAmountCurrencyForeign_Leave);
            this.txtAmountCurrencyForeign.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmountCurrencyForeign_KeyPress);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(192, 12);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(118, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "PHIẾU THU";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSaveClose,
            this.cmdSaveNew,
            this.cmdDel,
            this.cmdPrinter,
            this.toolStripSeparator1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(556, 25);
            this.toolStrip1.TabIndex = 72;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdSaveClose
            // 
            this.cmdSaveClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveClose.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveClose.Image")));
            this.cmdSaveClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdSaveClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveClose.Name = "cmdSaveClose";
            this.cmdSaveClose.Size = new System.Drawing.Size(95, 22);
            this.cmdSaveClose.Text = "Save && Close";
            this.cmdSaveClose.Click += new System.EventHandler(this.cmdSaveClose_Click);
            // 
            // cmdSaveNew
            // 
            this.cmdSaveNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveNew.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveNew.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveNew.Image")));
            this.cmdSaveNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveNew.Name = "cmdSaveNew";
            this.cmdSaveNew.Size = new System.Drawing.Size(94, 22);
            this.cmdSaveNew.Text = "Save && New";
            this.cmdSaveNew.Click += new System.EventHandler(this.cmdSaveNew_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDel.ForeColor = System.Drawing.Color.Navy;
            this.cmdDel.Image = ((System.Drawing.Image)(resources.GetObject("cmdDel.Image")));
            this.cmdDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.Size = new System.Drawing.Size(63, 22);
            this.cmdDel.Text = "Delete";
            this.cmdDel.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // cmdPrinter
            // 
            this.cmdPrinter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrinter.ForeColor = System.Drawing.Color.Navy;
            this.cmdPrinter.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrinter.Image")));
            this.cmdPrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdPrinter.Name = "cmdPrinter";
            this.cmdPrinter.Size = new System.Drawing.Size(63, 22);
            this.cmdPrinter.Text = "Printer";
            this.cmdPrinter.Click += new System.EventHandler(this.cmdPrinter_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 116);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(556, 265);
            this.tabControl1.TabIndex = 77;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtCurrencyMain);
            this.tabPage1.Controls.Add(this.txtCurrencyForeign);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.CoCustomers);
            this.tabPage1.Controls.Add(this.txtAmountCurrencyMain);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtCustomerID);
            this.tabPage1.Controls.Add(this.txtDepositor);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Controls.Add(this.txtCurrencyRate);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtReceiptDescription);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.txtAmountCurrencyForeign);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.ForeColor = System.Drawing.Color.Navy;
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(548, 239);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin chung";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtCurrencyMain
            // 
            this.txtCurrencyMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCurrencyMain.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrencyMain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyMain.ForeColor = System.Drawing.Color.Navy;
            this.txtCurrencyMain.Location = new System.Drawing.Point(487, 179);
            this.txtCurrencyMain.Name = "txtCurrencyMain";
            this.txtCurrencyMain.Size = new System.Drawing.Size(55, 21);
            this.txtCurrencyMain.TabIndex = 13;
            this.txtCurrencyMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCurrencyMain.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyMain_KeyPress);
            // 
            // txtCurrencyForeign
            // 
            this.txtCurrencyForeign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCurrencyForeign.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCurrencyForeign.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyForeign.ForeColor = System.Drawing.Color.Navy;
            this.txtCurrencyForeign.Location = new System.Drawing.Point(485, 127);
            this.txtCurrencyForeign.Name = "txtCurrencyForeign";
            this.txtCurrencyForeign.Size = new System.Drawing.Size(55, 21);
            this.txtCurrencyForeign.TabIndex = 10;
            this.txtCurrencyForeign.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtSumAmountInvoice);
            this.tabPage2.Controls.Add(this.GridViewInvoice);
            this.tabPage2.Controls.Add(this.lbInvoiceAmount);
            this.tabPage2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.ForeColor = System.Drawing.Color.Navy;
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(548, 239);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Thông tin hóa đơn thu tiền";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtSumAmountInvoice
            // 
            this.txtSumAmountInvoice.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumAmountInvoice.ForeColor = System.Drawing.Color.Navy;
            this.txtSumAmountInvoice.Location = new System.Drawing.Point(430, 199);
            this.txtSumAmountInvoice.Name = "txtSumAmountInvoice";
            this.txtSumAmountInvoice.Size = new System.Drawing.Size(115, 21);
            this.txtSumAmountInvoice.TabIndex = 120;
            this.txtSumAmountInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GridViewInvoice
            // 
            this.GridViewInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewInvoice.Location = new System.Drawing.Point(3, 5);
            this.GridViewInvoice.Name = "GridViewInvoice";
            this.GridViewInvoice.Size = new System.Drawing.Size(543, 189);
            this.GridViewInvoice.TabIndex = 119;
            this.GridViewInvoice.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewInvoice_CellEndEdit);
            this.GridViewInvoice.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridViewInvoice_RowValidated);
            this.GridViewInvoice.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GridViewInvoice_EditingControlShowing);
            this.GridViewInvoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridViewInvoice_KeyDown);
            // 
            // lbInvoiceAmount
            // 
            this.lbInvoiceAmount.AutoSize = true;
            this.lbInvoiceAmount.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInvoiceAmount.ForeColor = System.Drawing.Color.Navy;
            this.lbInvoiceAmount.Location = new System.Drawing.Point(317, 202);
            this.lbInvoiceAmount.Name = "lbInvoiceAmount";
            this.lbInvoiceAmount.Size = new System.Drawing.Size(107, 15);
            this.lbInvoiceAmount.TabIndex = 118;
            this.lbInvoiceAmount.Text = "Tồng tiền hóa đơn";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(370, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 15);
            this.label9.TabIndex = 113;
            this.label9.Text = "Nợ";
            // 
            // txtDebitNo
            // 
            this.txtDebitNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtDebitNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtDebitNo.ForeColor = System.Drawing.Color.Navy;
            this.txtDebitNo.Location = new System.Drawing.Point(434, 41);
            this.txtDebitNo.Name = "txtDebitNo";
            this.txtDebitNo.Size = new System.Drawing.Size(116, 21);
            this.txtDebitNo.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(370, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 15);
            this.label13.TabIndex = 115;
            this.label13.Text = "Có";
            // 
            // txtCreditNo
            // 
            this.txtCreditNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtCreditNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtCreditNo.ForeColor = System.Drawing.Color.Navy;
            this.txtCreditNo.Location = new System.Drawing.Point(434, 67);
            this.txtCreditNo.Name = "txtCreditNo";
            this.txtCreditNo.Size = new System.Drawing.Size(116, 21);
            this.txtCreditNo.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtBranchName);
            this.panel1.Controls.Add(this.txtTitle);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.PickerReceiptDate);
            this.panel1.Controls.Add(this.txtCreditNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtReceiptID);
            this.panel1.Controls.Add(this.txtDebitNo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(556, 91);
            this.panel1.TabIndex = 116;
            // 
            // txtBranchName
            // 
            this.txtBranchName.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBranchName.ForeColor = System.Drawing.Color.Navy;
            this.txtBranchName.Location = new System.Drawing.Point(15, 12);
            this.txtBranchName.Name = "txtBranchName";
            this.txtBranchName.Size = new System.Drawing.Size(148, 62);
            this.txtBranchName.TabIndex = 117;
            this.txtBranchName.Text = "Chi Nhánh : ";
            // 
            // FrmReceiptDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(556, 381);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmReceiptDetail";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu thu";
            this.Load += new System.EventHandler(this.FrmReceiptDetail_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmReceiptDetail_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewInvoice)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton cmdSaveNew;
        private System.Windows.Forms.ToolStripButton cmdPrinter;
        private System.Windows.Forms.ToolStripButton cmdSaveClose;
        private System.Windows.Forms.ToolStripButton cmdDel;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker PickerReceiptDate;
        private System.Windows.Forms.TextBox txtReceiptID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDepositor;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtReceiptDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAmountCurrencyForeign;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CoCustomers;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAmountCurrencyMain;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDebitNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCreditNo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtCurrencyMain;
        private System.Windows.Forms.TextBox txtCurrencyForeign;
        private System.Windows.Forms.TextBox txtSumAmountInvoice;
        private System.Windows.Forms.DataGridView GridViewInvoice;
        private System.Windows.Forms.Label lbInvoiceAmount;
        private System.Windows.Forms.Label txtBranchName;
    }
}