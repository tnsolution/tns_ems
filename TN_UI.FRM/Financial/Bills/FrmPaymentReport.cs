using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary.SYS;
using TNLibrary.FNC.Bills;
using TNLibrary.CRM;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.Bills
{
    public partial class FrmPaymentReport : Form
    {
        public bool FlagView;

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        public Cash_Payment_Info mPayment;
        public FrmPaymentReport()
        {
            InitializeComponent();

        }

        private void FrmPaymentReport_Load(object sender, EventArgs e)
        {
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            crystalReportViewer1.Dock = DockStyle.Fill;

            SetupCrystalReport();
            PopulateCrystalReport();
        }


        #region [ CrystalReport]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {
            ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\PhieuChi.rpt");

            DataTable nTable = TableReport.TablePhieuChi();
            DataTable nTableTencongty = TableReport.CompanyInfo();

            DataRow nRow = nTable.NewRow();
            nRow["TieuDeTren"] = "Ngày " + mPayment.BillDate.Day.ToString() + " tháng " + mPayment.BillDate.Month.ToString() + " năm " + mPayment.BillDate.Year.ToString();
            nRow["So"] = mPayment.ID;
            nRow["No"] = "";
            nRow["Co"] = "";
            if (mPayment.Receiver != "")
                nRow["HoTenNguoiChiTien"] = mPayment.Receiver;
            else
            {
                Customer_Info nVendor = new Customer_Info(mPayment.CustomerKey);
                nRow["HoTenNguoiChiTien"] = nVendor.Name;
            }
            nRow["DiaChiNguoiChi"] = mPayment.Address;
            nRow["LyDoChi"] = mPayment.Description;
            nRow["SoTien"] = mPayment.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            nRow["VietBangChu"] = TNFunctions.NumberWords(mPayment.AmountCurrencyMain) + " đồng ";
            double a = mPayment.AmountCurrencyMain;
            nRow["KemTheo"] = "1";
            nRow["ChungTuGoc"] = "..";
            nRow["TieuDeDuoi"] = "Ngày " + mPayment.BillDate.Day.ToString() + " tháng " + mPayment.BillDate.Month.ToString() + " năm " + mPayment.BillDate.Year.ToString();
            nRow["DaNhanDuSoTien"] = TNFunctions.NumberWords(mPayment.AmountCurrencyMain) + " đồng "; ;
            if (mPayment.AmountCurrencyForeign > 0)
            {
                nRow["TyGiaNgoaiTe"] = mPayment.CurrencyRate;
                nRow["SoTienQuyDoi"] = mPayment.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency) + mPayment.CurrencyIDForeign;
            }
            else
            {
                nRow["TyGiaNgoaiTe"] = "";
                nRow["SoTienQuyDoi"] = "";

            }
            nTable.Rows.Add(nRow);


            // thay doi ten table

            rptTam.Database.Tables["TablePhieuChi"].SetDataSource(nTable);
            rptTam.Database.Tables["TableTencongty"].SetDataSource(nTableTencongty);

            crystalReportViewer1.ReportSource = rptTam;

            // thay doi do zoom
            crystalReportViewer1.Zoom(1);

        }
        #endregion



    }
}