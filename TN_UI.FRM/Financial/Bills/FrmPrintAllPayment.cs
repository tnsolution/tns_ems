using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary;
using TNLibrary.FNC.Bills;
using TNLibrary.SYS;
using TN_UI.FRM.Reports;
using TNLibrary.CRM;

namespace TN_UI.FRM.Bills
{
    public partial class FrmPrintAllPayment : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;      
        public ArrayList ListPaymentKey;

        public FrmPrintAllPayment()
        {
            InitializeComponent();
        }

        private void FrmPrintAllPayment_Load(object sender, EventArgs e)
        {
            txtTotalPayment.Text += ListPaymentKey.Count.ToString();
        }  
      
        #region [ CrystalReport]
        
        private void PopulateCrystalReport(int PaymentKey)
        {
            Cash_Payment_Info mPayment = new Cash_Payment_Info(PaymentKey);

            ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\PhieuChi.rpt");

            DataTable nTable = TableReport.TablePhieuChi();
            DataTable nTableTencongty = TableReport.CompanyInfo();

            DataRow nRow = nTable.NewRow();
            nRow["TieuDeTren"] = "Ngày " + mPayment.BillDate.Day.ToString() + " tháng " + mPayment.BillDate.Month.ToString() +" năm " + mPayment.BillDate.Year.ToString();
            nRow["So"] = mPayment.ID;
            nRow["No"] = "";
            nRow["Co"] = "";
            nRow["HoTenNguoiChiTien"] = mPayment.Receiver;
            nRow["DiaChiNguoiChi"] = mPayment.Address;
            nRow["LyDoChi"] = mPayment.Description;
            nRow["SoTien"] = mPayment.AmountCurrencyMain.ToString(mFormatCurrencyMain,mFormatProviderCurrency);
            nRow["VietBangChu"] = TNFunctions.NumberWords(mPayment.AmountCurrencyMain) + " đồng ";
            nRow["KemTheo"] = "1";
            nRow["ChungTuGoc"] ="..";
            nRow["TieuDeDuoi"] = "Ngày " + mPayment.BillDate.Day.ToString() + " tháng " + mPayment.BillDate.Month.ToString() + " năm " + mPayment.BillDate.Year.ToString();
            nRow["DaNhanDuSoTien"] = TNFunctions.NumberWords(mPayment.AmountCurrencyMain) + " đồng "; ;
            if (mPayment.AmountCurrencyForeign > 0)
            {
                nRow["TyGiaNgoaiTe"] = mPayment.CurrencyRate;
                nRow["SoTienQuyDoi"] = mPayment.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency) + mPayment.CurrencyIDForeign;
            }
            else
            {
                nRow["TyGiaNgoaiTe"] = "";
                nRow["SoTienQuyDoi"] = "";

            }
            nTable.Rows.Add(nRow);


            // thay doi ten table

            rptTam.Database.Tables["TablePhieuChi"].SetDataSource(nTable);
            rptTam.Database.Tables["TableTencongty"].SetDataSource(nTableTencongty);

            rptTam.PrintToPrinter(1, true, 1, 1);
        }
        #endregion

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = ListPaymentKey.Count;
            progressBar1.Minimum = 0;
            foreach (int Key in ListPaymentKey)
            {
                PopulateCrystalReport(Key);
                progressBar1.Value++; 
            }
        }

       

    }
}