using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC.Bills;
using TNLibrary.FNC.Invoices;
using TNLibrary.FNC.Contracts;
namespace TN_UI.FRM.Bills
{
    public partial class FrmPaymentDetail : Form
    {

        public int mPaymentKey;
        public bool FlagTemp;

        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDate = GlobalSystemConfig.FormatDate;
        string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        public bool FlagTempSaved;
        public Cash_Payment_Object mPayment = new Cash_Payment_Object();
        public FrmPaymentDetail()
        {
            InitializeComponent();
            SetupLayoutGridViewInvoice(GridViewInvoice);
        }

        private void FrmPaymentDetail_Load(object sender, EventArgs e)
        {
            CheckRole();
            TN_Item nFisrtItem = new TN_Item();
            nFisrtItem.Name = "";
            nFisrtItem.Value = "";

            LoadDataToToolbox.AutoCompleteTextBox(txtCurrencyForeign, "SELECT CurrencyID FROM SYS_Currency");
            LoadDataToToolbox.AutoCompleteTextBox(txtCustomerID, "SELECT CustomerID FROM CRM_Customers WHERE IsVendor = 1 ");
            txtCurrencyMain.Text = mCurrencyMain;

            PickerPaymentDate.CustomFormat = mFormatDate;

            mPayment = new Cash_Payment_Object(mPaymentKey);
            LoadCash_Payment_Info();

            if (FlagTemp)
            {
                DisplayForTemp();
            }

        }

        #region [ Info chung ]
        private bool mPaymentIDChanged = false;
        private void txtPaymentID_TextChanged(object sender, EventArgs e)
        {
            mPaymentIDChanged = true;
        }

        private void txtPaymentID_Validated(object sender, EventArgs e)
        {
            if (mPaymentIDChanged)
            {
                Cash_Payment_Object nPayment = new Cash_Payment_Object(txtPaymentID.Text);
                if (nPayment.Key > 0)
                {
                    if (MessageBox.Show("Đã có số phiếu thu này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        mPayment = nPayment;
                        LoadCash_Payment_Info();
                    }
                    else
                    {
                        txtPaymentID.Focus();
                    }
                }

            }
        }

        private void LoadCash_Payment_Info()
        {
            txtPaymentID.Text = mPayment.ID;
            txtPaymentDescription.Text = mPayment.Description;
            PickerPaymentDate.Value = mPayment.BillDate;

            txtReceiver.Text = mPayment.Receiver;
            txtAddress.Text = mPayment.Address;
            if (mPayment.Key == 0)
                txtBranchName.Text = SessionUser.UserLogin.BranchName;
            else
                txtBranchName.Text = mPayment.BranchName;

            if (mPayment.CustomerKey > 0)
            {
                mCustomer = new Customer_Info(mPayment.CustomerKey);
                txtCustomerID.Text = mCustomer.ID;
                CoCustomers.Text = mCustomer.Name;
            }
           // CoAccountStocks.SelectedValue = mPayment.AccountStockKey;

            txtAmountCurrencyForeign.Text = mPayment.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            txtCurrencyForeign.Text = mPayment.CurrencyIDForeign;
            txtCurrencyRate.Text = mPayment.CurrencyRate.ToString();

            txtAmountCurrencyMain.Text = mPayment.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            LoadInvoiceDetailToGridView(mPayment.InvoicesPaid);
            mPaymentIDChanged = false;
        }

        private void ClearCash_Payment_Info()
        {

            mCustomer = new Customer_Info();
            mPayment = new Cash_Payment_Object();

            txtBranchName.Text = SessionUser.UserLogin.BranchName;
            txtPaymentID.Text = "";
            txtPaymentDescription.Text = "";

            txtReceiver.Text = "";
            txtAddress.Text = "";

            txtCustomerID.Text = "";
            txtCustomerID.Enabled = true;

            CoCustomers.Text = "";
            CoCustomers.Enabled = true;


            txtAmountCurrencyForeign.Text = "0";
            txtCurrencyRate.Text = "1";

            txtAmountCurrencyMain.Text = "0";


            GridViewInvoice.Rows.Clear();
            for (int i = 1; i <= 6; i++)
            {
                GridViewInvoice.Rows.Add();
            }

            txtPaymentID.Focus();
        }

        #endregion

        #region [ Invoice Info ]

        public void SetupLayoutGridViewInvoice(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("InvoiceNumber", "Số hóa đơn");
            GV.Columns.Add("InvoiceDate", "Ngày");
            GV.Columns.Add("CustomerName", "Công ty");
            GV.Columns.Add("PaymentAmount", "Số tiền");


            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 220;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 120;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;


            for (int i = 1; i <= 6; i++)
            {
                GV.Rows.Add();
            }
        }
        private void GridViewInvoice_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (GridViewInvoice.CurrentCell.ColumnIndex)
            {
                case 0: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT InvoiceNumber FROM FNC_Invoice_Sales");

                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 3: // So Tien
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;

            }
        }
        private void GridViewInvoice_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowEdit = GridViewInvoice.Rows[e.RowIndex];
            if (nRowEdit.Cells[e.ColumnIndex].Value == null)
                return;

            switch (e.ColumnIndex)
            {
                case 0:
                    InvoiceInfo(nRowEdit.Cells[0].Value.ToString(), e.RowIndex);
                    break;
                case 3: // thong tin ve thanh tien 
                    double nPaymentAmount = 0;
                    if (double.TryParse(nRowEdit.Cells["PaymentAmount"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nPaymentAmount))
                    {
                        nRowEdit.Cells["PaymentAmount"].Value = nPaymentAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                        nRowEdit.Cells["PaymentAmount"].ErrorText = "";
                    }
                    else
                        nRowEdit.Cells["PaymentAmount"].ErrorText = "Sai định dạng tiền tệ";
                    break;


            }
        }
        private void GridViewInvoice_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            int nRowIndex = GridViewInvoice.CurrentRow.Index;
            if (!IsRowEmpty(GridViewInvoice, nRowIndex))
            {
                CheckInvoiceRowError(nRowIndex);
            }
            CaculateInvoice();
        }
        private void GridViewInvoice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                GridViewInvoice.Rows.Remove(GridViewInvoice.CurrentRow);
                GridViewInvoice.Rows.Add();
            }
        }

        private void InvoiceInfo(string InvoiceNumber, int RowCurrency)
        {
            Invoice_Sale_Info nInvoice = new Invoice_Sale_Info();

            if (InvoiceNumber.Length == 0)
                return;
            if (IsHasInvoice(InvoiceNumber, RowCurrency))
            {
                GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "đã có hóa đơn này";
                return;
            }

            int nInvoiceNumbers = Invoice_Data.CountSaleInvoiceNumber(InvoiceNumber);

            if (nInvoiceNumbers == 0)
            {
                GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "Không có hóa đơn này";
                return;
            }

            if (nInvoiceNumbers == 1)
            {
                nInvoice.GetInvoice_Sale_Info(InvoiceNumber);
            }
            else
            {
                //FrmListInvoiceInfo frm = new FrmListInvoiceInfo();
                //frm.mInvoiceNumber = InvoiceNumber;
                //frm.mType = 0; // all invoice
                //frm.ShowDialog();
                //int nInvoiceKey = frm.mInvoiceKey;
                //frm.Close();
                //nInvoice = new SaleInvoiceInfo(nInvoiceKey);
            }
            double nAmountRemain = nInvoice.AmountCurrencyMain - nInvoice.AmountPayed;

            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].Tag = nInvoice.InvoiceKey;
            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceDate"].Value = nInvoice.InvoiceDate.ToString(mFormatDate);
            GridViewInvoice.Rows[RowCurrency].Cells["CustomerName"].Value = nInvoice.CustomerName;
            GridViewInvoice.Rows[RowCurrency].Cells["PaymentAmount"].Value = nAmountRemain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            GridViewInvoice.Rows[RowCurrency].Cells["InvoiceNumber"].ErrorText = "";
            GridViewInvoice.Rows[RowCurrency].Cells["PaymentAmount"].ErrorText = "";


        }
        private bool IsHasInvoice(string InvoiceNumber, int nRowCurrency)
        {
            foreach (DataGridViewRow nRow in GridViewInvoice.Rows)
            {
                if (nRow.Cells["InvoiceNumber"].Value != null)
                {
                    if (nRow.Index != nRowCurrency && nRow.Cells["InvoiceNumber"].Value.ToString().Trim() == InvoiceNumber)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void CaculateInvoice()
        {
            double nSumAmountInvoice = 0;
            foreach (DataGridViewRow nRow in GridViewInvoice.Rows)
            {
                if (!IsRowEmpty(GridViewInvoice, nRow.Index) && !IsRowError(GridViewInvoice, nRow.Index))
                {
                    nSumAmountInvoice += double.Parse(nRow.Cells["PaymentAmount"].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumAmountInvoice.Text = nSumAmountInvoice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void CheckInvoiceRowError(int RowIndex)
        {
            DataGridViewRow nRow = GridViewInvoice.Rows[RowIndex];

            if (nRow.Cells["InvoiceNumber"].Value == null && nRow.Cells["PaymentAmount"].Value == null)
            {
                nRow.Cells["InvoiceNumber"].ErrorText = "";
                nRow.Cells["PaymentAmount"].ErrorText = "";
                return;
            }
            else
            {
                if (nRow.Cells["InvoiceNumber"].Value == null)
                    nRow.Cells["InvoiceNumber"].ErrorText = "Error";
                if (nRow.Cells["PaymentAmount"].Value == null)
                    nRow.Cells["PaymentAmount"].ErrorText = "Error";
            }
        }

        private void LoadInvoiceDetailToGridView(DataTable InvoicePaid)
        {
            double nSumAmount = 0;
            int i = 0;
            foreach (DataRow nRow in InvoicePaid.Rows)
            {

                DataGridViewRow nRowView = GridViewInvoice.Rows[i];
                DateTime nDateTime = (DateTime)nRow["InvoiceDate"];
                double nPaymentAmount = double.Parse(nRow["PaymentAmount"].ToString());
                nRowView.Cells["InvoiceNumber"].Value = nRow["InvoiceNumber"].ToString();
                nRowView.Cells["InvoiceNumber"].Tag = nRow["InvoiceKey"];
                nRowView.Cells["InvoiceDate"].Value = nDateTime.ToString(mFormatDate);
                nRowView.Cells["CustomerName"].Value = nRow["CustomerName"].ToString();
                nRowView.Cells["PaymentAmount"].Value = nPaymentAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

                i++;
                nSumAmount += nPaymentAmount;
            }
            for (int k = i; k <= 6; k++)
            {
                DataGridViewRow nRow = GridViewInvoice.Rows[k];
                nRow.Cells["InvoiceNumber"].Value = null;
                nRow.Cells["InvoiceNumber"].Tag = 0;
                nRow.Cells["InvoiceDate"].Value = null;
                nRow.Cells["CustomerName"].Value = null;
                nRow.Cells["PaymentAmount"].Value = null;

            }
            txtSumAmountInvoice.Text = nSumAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void GetListInvoiceInGridView()
        {
            mPayment.InvoicesPaid.Clear();

            for (int i = 0; i < GridViewInvoice.Rows.Count; i++)
            {
                if (!IsRowEmpty(GridViewInvoice, i) && !IsRowError(GridViewInvoice, i))
                {
                    DataRow nRow = mPayment.InvoicesPaid.NewRow();

                    DataGridViewRow nRowView = GridViewInvoice.Rows[i];
                    nRow["PaymentKey"] = mPayment.Key;
                    nRow["InvoiceKey"] = int.Parse(nRowView.Cells["InvoiceNumber"].Tag.ToString());
                    nRow["PaymentAmount"] = double.Parse(nRowView.Cells["PaymentAmount"].Value.ToString(), mFormatProviderCurrency);

                    mPayment.InvoicesPaid.Rows.Add(nRow);
                }
            }

        }
        #endregion

        #region [ Customer Infomation]
        private Customer_Info mCustomer = new Customer_Info();
        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            if (txtCustomerID.Text.Trim() == "*")
            {
                LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsVendor = 1", true);
            }
            else
            {
                mCustomer = new Customer_Info(txtCustomerID.Text.Trim());
                LoadInfoCustomer();
                if (mCustomer.Key == 0 && txtCustomerID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsVendor = 1 AND CustomerID LIKE '" + txtCustomerID.Text.Trim() + "%'", true);

            }
        }
        private void txtCustomerID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                FRM.CRM.FrmCustomerList frm = new FRM.CRM.FrmCustomerList();
                frm.FlagView = true;
                frm.ShowDialog();
                //if (frm.ListItemSelect != null)
                //    txtCustomerID.Text = frm.ListItemSelect.Text;
                frm.Close();
            }
        }



        private void CoCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nCustomerKey;
            if (int.TryParse(CoCustomers.SelectedValue.ToString(), out nCustomerKey))
            {
                mCustomer = new Customer_Info(nCustomerKey);
                LoadInfoCustomer();
            }

        }
        private void LoadInfoCustomer()
        {
            if (mCustomer.Key == 0)
            {
                CoCustomers.Text = "...";
                txtAddress.Text = "...";

            }
            else
            {
                txtCustomerID.Text = mCustomer.ID.Trim();
                CoCustomers.Text = mCustomer.Name;
                txtAddress.Text = mCustomer.Address + ", " + mCustomer.CityName;

            }
        }
        #endregion

        #region [ Process Input & Outpur ]
        private void txtAmountCurrencyForeign_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtAmountCurrencyMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtCurrencyMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtBankVAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtBankFee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }




        private void txtAmountCurrencyForeign_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtAmountCurrencyForeign.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;

            txtAmountCurrencyForeign.Text = Amount.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
            CaculateAmountMain();
        }
        private void txtCurrencyRate_Leave(object sender, EventArgs e)
        {
            CaculateAmountMain();
        }
        private void txtAmountCurrencyMain_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtAmountCurrencyMain.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;

            txtAmountCurrencyMain.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }

        #endregion

        #region [ Function ]
        private bool CheckBeforeSave()
        {
            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerPaymentDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerPaymentDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

            if (txtPaymentID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtPaymentID.Focus();
                return false;
            }

            double nAmount = double.Parse(txtAmountCurrencyMain.Text, mFormatProviderCurrency);
            if (nAmount == 0)
            {
                MessageBox.Show("Bạn phải nhập số tiền vào");
                txtAmountCurrencyMain.Focus();
                return false;
            }

          
            return true;
        }
        private void CaculateAmountMain()
        {
            double nAmountCurrencyForeign = double.Parse(txtAmountCurrencyForeign.Text, mFormatProviderCurrency);
            double nRate = 0;
            double.TryParse(txtCurrencyRate.Text, out nRate);
            double nAmountCurrencyMain = nAmountCurrencyForeign * nRate;

            txtAmountCurrencyMain.Text = nAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void DisplayForTemp()
        {
            //txtVendorID.Enabled = false;
            //CoVendors.Enabled = false;
            cmdSaveNew.Enabled = false;

        }
        #endregion

        #region [ Update Cash DataBase]
        private bool SaveCash_Payment_Infomation()
        {
            if (!CheckBeforeSave()) return false;

            mPayment.ID = txtPaymentID.Text.Trim();
            mPayment.BillDate = PickerPaymentDate.Value;
            mPayment.Description = txtPaymentDescription.Text;

            mPayment.Receiver = txtReceiver.Text;
            mPayment.Address = txtAddress.Text;

            mPayment.BranchKey = SessionUser.UserLogin.BranchKey;
            mPayment.CreatedBy = SessionUser.UserLogin.Key;
            mPayment.ModifiedBy = SessionUser.UserLogin.Key;

            if (txtCustomerID.Enabled == false)
                mPayment.CustomerKey = 0;
            else
                mPayment.CustomerKey = mCustomer.Key;
          //  mPayment.AccountStockKey = CoAccountStocks.SelectedValue.ToString();

            mPayment.AmountCurrencyForeign = double.Parse(txtAmountCurrencyForeign.Text, mFormatProviderCurrency);
            mPayment.CurrencyIDForeign = txtCurrencyForeign.Text;
            mPayment.CurrencyRate = double.Parse(txtCurrencyRate.Text);

            mPayment.AmountCurrencyMain = double.Parse(txtAmountCurrencyMain.Text, mFormatProviderCurrency);

            GetListInvoiceInGridView();
            return true;


        }

        #endregion

        #region [ Button Activate ]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (FlagTemp)
            {
                if (SaveCash_Payment_Infomation())
                {
                    FlagTempSaved = true;
                    this.Close();
                }

            }
            else
            {
                if (SaveCash_Payment_Infomation())
                {
                    mPayment.SaveObject();
                    this.Close();
                }
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveCash_Payment_Infomation())
            {
                mPayment.SaveObject();
                string strID = mPayment.ID.Trim();
                ClearCash_Payment_Info();
                txtPaymentID.Text = Cash_Bills_Data.GetAutoPaymentID(strID);

            }
        }

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa phiếu thu này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mPayment.Delete();
                ClearCash_Payment_Info();
            }
        }

        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmPaymentReport frm = new FrmPaymentReport();
            SaveCash_Payment_Infomation();
            frm.mPayment = mPayment;
            frm.ShowDialog();
        }
        #endregion

        #region [ Function For DatagridView]
        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private bool GridViewIsHasError(DataGridView GV)
        {
            int n = GV.ColumnCount;
            foreach (DataGridViewRow nRow in GV.Rows)
            {
                for (int i = 0; i < n; i++)
                {
                    if (nRow.Cells[i].ErrorText.Length > 0)
                        return true;
                }
            }
            return false;
        }
        private bool IsRowEmpty(DataGridView GV, int RowIndex)
        {
            for (int i = 0; i < GV.ColumnCount; i++)
            {
                if (GV.Rows[RowIndex].Cells[i].Value != null)
                    return false;
            }
            return true;
        }
        private bool IsRowError(DataGridView GV, int RowIndex)
        {
            for (int i = 0; i < GV.ColumnCount; i++)
            {
                if (GV.Rows[RowIndex].Cells[i].ErrorText.Length > 0)
                    return true;
            }
            return false;
        }

        #endregion

        #region [ Short Key ]
        private void FrmPaymentDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC003");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

     }
}