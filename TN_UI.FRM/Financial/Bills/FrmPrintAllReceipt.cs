using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary;
using TNLibrary.FNC.Bills;
using TNLibrary.SYS;
using TN_UI.FRM.Reports;


namespace TN_UI.FRM.Bills
{
    public partial class FrmPrintAllReceipt : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;      
        public ArrayList ListReceiptKey;

        public FrmPrintAllReceipt()
        {
            InitializeComponent();
            
        }
       
        private void FrmPrintAllReceipt_Load(object sender, EventArgs e)
        {
            txtTotalReceipt.Text += ListReceiptKey.Count.ToString();
        }
         
        #region [ CrystalReport]

        private void PopulateCrystalReport(int ReceiptKey)
        {
           Cash_Receipt_Info mReceipt = new Cash_Receipt_Info(ReceiptKey);

           ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\PhieuThu.rpt");

            DataTable nTable = TableReport.TablePhieuThu();
            DataTable nTableTencongty = TableReport.CompanyInfo();
          

                DataRow nRow = nTable.NewRow();
                nRow["TieuDeTren"] = "Ngày " + mReceipt.BillDate.Day.ToString() + " tháng " + mReceipt.BillDate.Month.ToString() + " năm " + mReceipt.BillDate.Year.ToString();
                nRow["So"] = mReceipt.ID;
                nRow["No"] = "";
                nRow["Co"] = "";
                nRow["HoTenNguoiNopTien"] = mReceipt.Depositor;
                nRow["DiaChiNguoiNop"] = mReceipt.Address;
                nRow["LyDoNop"] = mReceipt.Description;
                nRow["SoTien"] = mReceipt.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                nRow["VietBangChu"] = TNFunctions.NumberWords(mReceipt.AmountCurrencyMain) + " đồng";
                nRow["KemTheo"] = "1";
                nRow["ChungTuGoc"] = "..";
                nRow["TieuDeDuoi"] = "Ngày " + mReceipt.BillDate.Day.ToString() + " tháng " + mReceipt.BillDate.Month.ToString() + " năm " + mReceipt.BillDate.Year.ToString();
                nRow["DaNhanDuSoTien"] = TNFunctions.NumberWords(mReceipt.AmountCurrencyMain) + " đồng";
                if (mReceipt.AmountCurrencyForeign > 0)
                {
                    nRow["TyGiaNgoaiTe"] = mReceipt.CurrencyRate;
                    nRow["SoTienQuyDoi"] = mReceipt.AmountCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency) + mReceipt.CurrencyIDForeign;
                }
                else
                {
                    nRow["TyGiaNgoaiTe"] = "";
                    nRow["SoTienQuyDoi"] = "";

                }
                nTable.Rows.Add(nRow);
          
            // thay doi ten table

            rptTam.Database.Tables["TablePhieuThu"].SetDataSource(nTable);
            rptTam.Database.Tables["TableTencongty"].SetDataSource(nTableTencongty);

            rptTam.PrintToPrinter(1, true, 1, 1);
        }
        #endregion

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = ListReceiptKey.Count;
            progressBar1.Minimum = 0;
            foreach (int Key in ListReceiptKey)
            {
                PopulateCrystalReport(Key);
                progressBar1.Value++;
            }
        }

       
       

    }
}