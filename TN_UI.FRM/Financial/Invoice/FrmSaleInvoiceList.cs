using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;
using TNLibrary.FNC.Invoices;

namespace TN_UI.FRM.Invoice
{
    public partial class FrmSaleInvoiceList : Form
    {

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProvider = GlobalSystemConfig.FormatProviderDecimal;
        string mFormatDate = GlobalSystemConfig.FormatDate;
        public ListViewItem ListItemSelect;
        public bool FlagView;
        public FrmSaleInvoiceList()
        {
            InitializeComponent();

        }
        private void FrmSaleInvoiceList_Load(object sender, EventArgs e)
        {
            CheckRole();

            InitListView();
            panelSearch.Height = 0;

            PickerToDay.CustomFormat = mFormatDate;
            PickerFromDay.CustomFormat = mFormatDate;

            PickerFromDay.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            DataTable SaleList = Invoice_Data.SaleInvoices(PickerFromDay.Value, PickerToDay.Value);
            LoadDataFields(ListViewData, SaleList);

            panelSearch.Height = 90;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
            //LoadContextMenu();
            this.WindowState = FormWindowState.Maximized;
        }

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhà cung cấp";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diễn Giải";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng giá trị";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thuế xuất";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "VAT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Chứng từ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            ListViewData.SmallImageList = SmallImageLV;

            LVGroup = new ListViewMyGroup(ListViewData);

        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (FlagView)
            {
                ListItemSelect = ListViewData.SelectedItems[0];
                this.Close();
            }
            else
            {
                bool IsFixedAsset = (bool)ListViewData.SelectedItems[0].SubItems[1].Tag;
                int nInvoiceKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());

                FrmSaleInvoiceDetail frm = new FrmSaleInvoiceDetail();
                frm.mInvoiceKey = nInvoiceKey;
                frm.ShowDialog();

                SearchInvoice();
                SelectIndexInListView(nInvoiceKey);
            }
        }
        public void LoadDataFields(ListView LV, DataTable Table)
        {
            //----------------------------------------------------------
            int n = Table.Rows.Count;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = Table.Rows[i];
                int No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = " ";
                lvi.Tag = nRow["InvoiceKey"]; // Set the tag to 
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceID"].ToString().Trim();
                lvsi.Tag = nRow["IsFixedAsset"];
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceNumber"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                DateTime nInvoiceDate = (DateTime)nRow["InvoiceDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nInvoiceDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["VATPercent"].ToString().Trim() + "%";
                lvi.SubItems.Add(lvsi);

                double nAmountVATForTax = double.Parse(nRow["VATForTax"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmountVATForTax.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = "";// nRow["VoucherID"].ToString().Trim();

                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

        }
        #endregion

        #region [Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 88;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtTitle.Left = panelTitleCenter.Width / 2 - txtTitle.Width / 2;
        }
        #endregion

        #region [ Button Activate]
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            SearchInvoice();
        }
        private void SearchInvoice()
        {
            DataTable nTable = new DataTable();
            nTable = Invoice_Data.SaleInvoices(PickerFromDay.Value, PickerToDay.Value);

            LoadDataFields(ListViewData, nTable);

        }
        private void cmdAddNew_Click(object sender, EventArgs e)
        {

            FrmSaleInvoiceDetail frm = new FrmSaleInvoiceDetail();
            frm.ShowDialog();
            SearchInvoice();

        }

        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)
                {

                    if (MessageBox.Show("Bạn có muốn xóa hóa đơn này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        int nIndex = ListViewData.SelectedItems[0].Index;
                        Invoice_Sale_Object nInvoice = new Invoice_Sale_Object();
                        nInvoice.InvoiceKey = (int)ListViewData.SelectedItems[0].Tag;
                        nInvoice.DeleteObject();
                        ListViewData.SelectedItems[0].Remove();
                        if (ListViewData.Items.Count > 0)
                            if (nIndex == 0)
                                ListViewData.Items[nIndex].Selected = true;
                            else
                                ListViewData.Items[nIndex - 1].Selected = true;
                        ListViewData.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Bạn phải chọn hóa đơn để xóa.");
                }
            }
        }

        #endregion

        #region [ Context Menu]
        private void MenuVoucher_Click(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                string nVoucherID = ListViewData.SelectedItems[0].SubItems[1].Text;
                if (nVoucherID.Trim().Length > 0)
                {
                    //TNSolution.Vouchers.FrmVoucherToRaw frm = new TNSolution.Vouchers.FrmVoucherToRaw();
                    //frm.FlagEdit = true;
                    //frm.m_VoucherID = nVoucherID;
                    //frm.ShowDialog();
                }
            }
        }

        private void MenuInvoice_Click(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                if (FlagView)
                {
                    ListItemSelect = ListViewData.SelectedItems[0];
                    this.Close();
                }
                else
                {
                    bool IsFixedAsset = (bool)ListViewData.SelectedItems[0].SubItems[1].Tag;
                    int nInvoiceKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                    if (IsFixedAsset)
                    {
                        //FrmFixedAssetInvoice frm = new FrmFixedAssetInvoice();
                        //frm.FlagEdit = true;
                        //frm.mInvoiceKey = nInvoiceKey;
                        //frm.Show();
                    }
                    else
                    {
                        //FrmSaleInvoiceDetail frm = new FrmSaleInvoiceDetail();
                        //frm.FlagEdit = true;
                        //frm.mInvoiceKey = nInvoiceKey;
                        //frm.Show();
                    }
                    SearchInvoice();
                    SelectIndexInListView(nInvoiceKey);
                }
            }
        }
        #endregion

        #region [ Function ]
        private void SelectIndexInListView(int InvoiceKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == InvoiceKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercutiry ]
        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC002");
            if (nRole.Del)
                m_RoleDelete = true;

        }
        #endregion

    }
}