﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.SYS;
using CrystalDecisions.CrystalReports.Engine;
using TN_UI.FRM.Reports;
using TNLibrary.IVT;
using TNLibrary.FNC.Invoices;

namespace TN_UI.FRM.Invoice
{
    public partial class FrmPurchaseInvoiceReport : Form
    {
        public bool FlagView;

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;

        public Invoice_Purchase_Object mInvoice;

        public FrmPurchaseInvoiceReport()
        {
            InitializeComponent();
        }

        private void FrmPurchaseInvoiceRerort_Load(object sender, EventArgs e)
        {
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            crystalReportViewer1.Dock = DockStyle.Fill;

            SetupCrystalReport();
            PopulateCrystalReport();
        }
        #region [ CrystalReport]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {
            ReportDocument rptTam = new ReportDocument();
            // thay ten report
            rptTam.Load("FilesReport\\HoaDonMuaHang.rpt");

            DataTable nTableTitle = TableReport.TableHoaDonMuaHangTitle();
            DataTable nTableDetail = TableReport.TableHoaDonMuaHangDetail();


            DataRow nRow = nTableTitle.NewRow();
            nRow["NgayHD"] = mInvoice.InvoiceDate.ToString(mFormatDate);
            nRow["MauSo"] = mInvoice.InvoiceDesign;
            nRow["SoHieu"] = mInvoice.InvoiceSign;
            nRow["SoHD"] = mInvoice.InvoiceNumber;
            nRow["SoHieu"] = mInvoice.InvoiceID;
            nRow["ThueSuat"] = mInvoice.VATPercent;
            nRow["TienThue"] = mInvoice.VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency); ;
            nRow["ThanhTien"] = mInvoice.AmountOrder.ToString(mFormatCurrencyMain,mFormatProviderCurrency);
            nRow["NhaCC"] = mInvoice.CustomerName;
         
            nRow["TongThanhTien"] = mInvoice.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            nRow["TongTienBangChu"] = " ";
        

            nTableTitle.Rows.Add(nRow);

            int No = 1;
            foreach (InventoryProduct nProduct in mInvoice.Items)
            {
                nRow = nTableDetail.NewRow();
                nRow["STT"] = No.ToString();
                nRow["TenSP"] = nProduct.Name;
                nRow["Ma"] = nProduct.ID;
                nRow["DonViTinh"] = nProduct.Unit;
                nRow["YeuCauXuat"] = nProduct.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRow["ThucXuat"] = nProduct.Quantity.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRow["DonGia"] = nProduct.UnitPrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
                nRow["ThanhTien"] = nProduct.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

                nTableDetail.Rows.Add(nRow);
                No++;
            }


            // thay doi ten table
            rptTam.Database.Tables["TableHoaDonMuaHangTitle"].SetDataSource(nTableTitle);
            rptTam.Database.Tables["TableHoaDonMuaHangDetail"].SetDataSource(nTableDetail);

            crystalReportViewer1.ReportSource = rptTam;
            // thay doi do zoom
            crystalReportViewer1.Zoom(1);

        }
        #endregion

    }
}
