// SumOrder : la tong thanh tien cua cac san pham, dich vu truoc thue
// SumTotal : La tong thanh tien bao gom thue
// AmountForTAX : danh de khai bao thue

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC.Invoices;

namespace TN_UI.FRM.Invoice
{
    public partial class FrmSaleInvoiceDetail : Form
    {

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        public bool FlagTemp;
        public int mInvoiceKey;

        public FrmSaleInvoiceDetail()
        {
            InitializeComponent();
            dataGridViewProducts1.ImageHeader = imageList1.Images[0];
            dataGridViewProducts1.SetupLayoutGridView();

            dataGridViewProducts1.KeyDown += new System.Windows.Forms.KeyEventHandler(dataGridViewProducts1_KeyDown);
            dataGridViewProducts1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridViewProducts1_RowValidated);

        }

        private void FrmSaleInvoiceDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coInvoiceDesign, "SELECT DesignKey, DesignName FROM FNC_Invoice_Design", false);
            LoadDataToToolbox.ComboBoxData(coCategory, "SELECT CategoryKey, CategoryName FROM FNC_Invoice_Categories", false);

            LoadDataToToolbox.AutoCompleteTextBox(txtCustomerID, "SELECT CustomerID FROM CRM_Customers WHERE IsCustomer = 1 ");
            coCategory.SelectedIndex = 0;

            mInvoice = new Invoice_Sale_Object(mInvoiceKey);
            LoadSaleInvoiceInfo();


            if (FlagTemp)
            {
                DisplayForInvoiceTemp();
            }

            PickerInvoiceDate.CustomFormat = mFormatDate;
            tnPickerPaymentMaturity.CustomFormat = mFormatDate;


        }

        #region [ Invoice Infomation ]
        public Invoice_Sale_Object mInvoice = new Invoice_Sale_Object();

        private void LoadSaleInvoiceInfo()
        {
            txtInvoiceID.Text = mInvoice.InvoiceID.Trim();
            txtInvoiceNumber.Text = mInvoice.InvoiceNumber.Trim();
            txtInvoiceSign.Text = mInvoice.InvoiceSign;
            coInvoiceDesign.Text = mInvoice.InvoiceDesign;
            txtInvoiceDescription.Text = mInvoice.InvoiceDescription;

            PickerInvoiceDate.Value = mInvoice.InvoiceDate;
            tnPickerPaymentMaturity.Value = mInvoice.PaymentMaturity;

            txtFollowDocument.Text = mInvoice.DocumentFollow;
            chkPayByCash.Checked = mInvoice.PayByCash;
            chkPayByAccountBank.Checked = mInvoice.PayByAccountBank;

            coCategory.SelectedValue = mInvoice.CategoryKey;
            mCustomer = new Customer_Info(mInvoice.CustomerKey);
            LoadInfoCustomer();

            dataGridViewProducts1.ListProduct = mInvoice.Items;

            txtVATPercent.Text = mInvoice.VATPercent.ToString(mFormatDecimal, mFormatProviderCurrency);
            txtVAT.Text = mInvoice.VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            txtSumOrder.Text = mInvoice.AmountOrder.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtSubTotal.Text = mInvoice.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            txtAmountForTax.Text = mInvoice.AmountForTAX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            //kiem tra VATFORTAX
            int tmp = string.Compare(txtAmountForTax.Text, txtSumOrder.Text);
            if (tmp == 0) //VAT==VATFORTAX
                txtVATForTax.Text = mInvoice.VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            else
                txtVATForTax.Text = mInvoice.VATForTAX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            mInvoiceNumberChanged = false;
            mInvoiceIDChanged = false;
        }

        private bool mInvoiceNumberChanged = false, mInvoiceIDChanged = false;
        private void txtInvoiceNumber_TextChanged(object sender, EventArgs e)
        {
            mInvoiceNumberChanged = true;
        }
        private void txtInvoiceNumber_Leave(object sender, EventArgs e)
        {
            if (txtInvoiceNumber.Text.Trim().Length == 0)
                return;
            if (mInvoiceNumberChanged)
            {
                int nQuantityInvoice = Invoice_Data.CountSaleInvoiceNumber(txtInvoiceNumber.Text.Trim());
                if (nQuantityInvoice > 0)
                {
                    if (MessageBox.Show("Đã có số hóa đơn này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (nQuantityInvoice == 1)
                        {
                            mInvoice = new Invoice_Sale_Object(txtInvoiceNumber.Text.Trim());
                        }
                        else
                        {
                            //FrmListInvoiceInfo frm = new FrmListInvoiceInfo();
                            //frm.mInvoiceNumber = txtInvoiceNumber.Text.Trim();
                            //frm.mType = 1; // invoice general
                            //frm.ShowDialog();
                            //int nInvoiceKey = frm.mInvoiceKey;
                            //frm.Close();
                            //mInvoice = new SaleInvoiceInfo(nInvoiceKey);
                        }
                        LoadSaleInvoiceInfo();
                    }
                }
                mInvoiceNumberChanged = false;
            }
        }

        private void txtInvoiceID_TextChanged(object sender, EventArgs e)
        {
            mInvoiceIDChanged = true;
        }
        private void txtInvoiceID_Leave(object sender, EventArgs e)
        {
            if (mInvoiceIDChanged)
            {
                Invoice_Sale_Object nInvoice = new Invoice_Sale_Object(txtInvoiceID.Text.Trim());
                if (nInvoice.InvoiceKey > 0)
                {
                    if (MessageBox.Show("Đã có số hiệu hóa đơn này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        mInvoice = nInvoice;
                        LoadSaleInvoiceInfo();
                    }
                    else
                    {
                        txtInvoiceID.Focus();
                    }
                }
            }
        }

        private void coCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbInvoiceType.Text = coCategory.Text.ToUpper();
            lbInvoiceType.Left = (txtTitle.Left + txtTitle.Width / 2) - lbInvoiceType.Width / 2;
        }

        #endregion

        #region [ Invoice Details Infomation ]
        private void dataGridViewProducts1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            double nSumAmountCurrencyMain = 0;
            foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
            {
                if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                {
                    nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtAmountForTax.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
        }
        private void dataGridViewProducts1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Control)
            {
                if (dataGridViewProducts1.CurrentCell.ColumnIndex == 1)
                {
                    FRM.List.FrmProductList Frm = new FRM.List.FrmProductList();
                    Frm.FlagView = true;
                    Frm.ShowDialog();
                    dataGridViewProducts1.CurrentCell.Value = Frm.mProductID;
                    dataGridViewProducts1.BeginEdit(true);

                }
            }
        }

        #endregion

        #region [ Customer Infomation]
        private Customer_Info mCustomer = new Customer_Info();
        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            if (txtCustomerID.Text.Trim() == "*")
            {
                LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1", true);
            }
            else
            {
                mCustomer = new Customer_Info(txtCustomerID.Text.Trim());
                LoadInfoCustomer();
                if (mCustomer.Key == 0 && txtCustomerID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE IsCustomer = 1 AND CustomerID LIKE '" + txtCustomerID.Text.Trim() + "%'", true);

            }
        }
        private void txtCustomerID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                //FrmCustomerList frm = new FrmCustomerList();
                //frm.FlagView = true;
                //frm.ShowDialog();
                //if (frm.ListItemSelect != null)
                //    txtCustomerID.Text = frm.ListItemSelect.Text;
                //frm.Close();
            }
        }
        private void CoCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nCustomerKey;
            if (int.TryParse(CoCustomers.SelectedValue.ToString(), out nCustomerKey))
            {
                mCustomer = new Customer_Info(nCustomerKey);
                LoadInfoCustomer();
            }

        }

        private void LoadInfoCustomer()
        {
            if (mCustomer.Key == 0)
            {
                CoCustomers.Text = "...";
                txtAddress.Text = "...";
                txtAccountBank.Text = "...";
                txtTAX.Text = "...";
            }
            else
            {
                txtCustomerID.Text = mCustomer.ID.Trim();
                CoCustomers.Text = mCustomer.Name;
                txtAddress.Text = mCustomer.Address + ", " + mCustomer.CityName + ", " + mCustomer.CountryName;
                txtAccountBank.Text = mCustomer.AccountBank;
                txtTAX.Text = mCustomer.TaxNumber;
            }
        }

        #endregion

        #region [ Activate Button ]
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa hóa đơn này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mInvoice.DeleteObject();
                ClearInvoiceInfo();
            }
        }
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {

            if (SaveSaleInvoiceInfomation())
            {
                if (!FlagTemp)
                {
                    mInvoice.SaveObject();
                    this.Close();
                }
            }
            else
                this.Close();

        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {

            if (SaveSaleInvoiceInfomation())
            {
                mInvoice.SaveObject();
                string strID = mInvoice.InvoiceID.Trim();

                ClearInvoiceInfo();
                txtInvoiceID.Text = Invoice_Data.GetAutoSaleInVoiceID(strID);
            }
        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmSaleInvoiceReport frm = new FrmSaleInvoiceReport();
            SaveSaleInvoiceInfomation();
            frm.mInvoice = mInvoice;
            frm.ShowDialog();
        }
        #endregion

        #region [ Update Invoice DataBase]

        private bool SaveSaleInvoiceInfomation()
        {
            if (!CheckBeforeSave()) return false;
            if (!FlagTemp)
            {
                if (mCustomer.Key == 0)
                {
                    if (MessageBox.Show("Chưa có khách hàng này, ? Bạn có muốn thêm khách hàng này ? ", "Thêm ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        mCustomer = new Customer_Info();
                        mCustomer.ID = txtCustomerID.Text;
                        mCustomer.Name = CoCustomers.Text;
                        mCustomer.Address = txtAddress.Text;
                        mCustomer.TaxNumber = txtTAX.Text;
                        mCustomer.CategoryKey = 1; // khach hang
                        mCustomer.IsCustomer = true;
                        mCustomer.Create();
                    }
                    else
                        return false;
                }
            }

            //kiem tra VATFORTAX
            int tmp = string.Compare(txtAmountForTax.Text, txtSumOrder.Text);

            // dung de chinh lai phan lap trinh
            txtInvoiceNumber.Focus(); //dung khi chinh lai ngay hoa don
            mInvoice.InvoiceID = txtInvoiceID.Text;
            mInvoice.InvoiceNumber = txtInvoiceNumber.Text;
            mInvoice.InvoiceSign = txtInvoiceSign.Text;
            mInvoice.InvoiceDesign = coInvoiceDesign.Text;
            mInvoice.InvoiceDescription = txtInvoiceDescription.Text;

            mInvoice.CustomerKey = mCustomer.Key;
            mInvoice.CategoryKey = (int)coCategory.SelectedValue;
            mInvoice.IsFixedAsset = false;

            mInvoice.InvoiceDate = PickerInvoiceDate.Value;
            mInvoice.PaymentMaturity = tnPickerPaymentMaturity.Value;

            mInvoice.AmountOrder = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            mInvoice.AmountCurrencyMain = double.Parse(txtSubTotal.Text, mFormatProviderCurrency);
            mInvoice.VAT = double.Parse(txtVAT.Text, mFormatProviderCurrency);
            mInvoice.VATPercent = int.Parse(txtVATPercent.Text);

            mInvoice.AmountForTAX = double.Parse(txtAmountForTax.Text, mFormatProviderCurrency);
            mInvoice.VATForTAX = double.Parse(txtVATForTax.Text, mFormatProviderCurrency);

            mInvoice.PayByCash = chkPayByCash.Checked;
            mInvoice.PayByAccountBank = chkPayByAccountBank.Checked;

            mInvoice.BranchKey = SessionUser.UserLogin.BranchKey;
            mInvoice.CreatedBy = SessionUser.UserLogin.Key;
            mInvoice.ModifiedBy = SessionUser.UserLogin.Key;

            mInvoice.Items = dataGridViewProducts1.ListProduct;

            return true;

        }
        #endregion

        #region [ Function]
        private bool CheckBeforeSave()
        {
            txtInvoiceID.Focus();

            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerInvoiceDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerInvoiceDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

            if (txtInvoiceNumber.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số hóa đơn");
                txtInvoiceNumber.Focus();
                return false;
            }

            if (txtInvoiceID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtInvoiceID.Focus();
                return false;
            }
            if (txtCustomerID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã khách hàng");
                txtCustomerID.Focus();
                return false;
            }
            if (CoCustomers.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên khách hàng");
                CoCustomers.Focus();
                return false;
            }
            if (dataGridViewProducts1.IsError)
            {
                MessageBox.Show("Vui lòng kiểm tra lại danh sách vật tư mua hàng");
                dataGridViewProducts1.Focus();
                return false;
            }
            return true;
        }
        private void CaculateSubTotal()
        {
            double Total = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            double VAT = double.Parse(txtVAT.Text, mFormatProviderCurrency);

            double SubTotal = Total + VAT;
            txtSubTotal.Text = SubTotal.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        private void ClearInvoiceInfo()
        {

            mCustomer = new Customer_Info();
            mInvoice = new Invoice_Sale_Object();

            txtInvoiceID.Text = "";
            txtInvoiceDescription.Text = "";
            txtInvoiceNumber.Text = "";
            txtInvoiceSign.Text = "";
            txtCustomerID.Text = "";
            CoCustomers.Text = "";
            txtAddress.Text = "";
            txtSumOrder.Text = "0";
            txtVAT.Text = "0";
            txtVATPercent.Text = "0";
            txtSubTotal.Text = "0";

            dataGridViewProducts1.ClearProducts();

            txtInvoiceID.Focus();
        }

        #endregion

        #region [ Process Inupt & Output]

        private void txtSumOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVAT_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVATPercent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtSubTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtAmountForTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVATForTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }

        private void txtSumOrder_Leave(object sender, EventArgs e)
        {
            if (txtSumOrder.Text.Trim().Length == 0)
                txtSumOrder.Text = "0";

            double Amount;
            if (!double.TryParse(txtSumOrder.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtSumOrder.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
        }
        private void txtVAT_Leave(object sender, EventArgs e)
        {
            if (txtVAT.Text.Trim().Length == 0)
                txtVAT.Text = "0";

            double VAT;
            if (!double.TryParse(txtVAT.Text, NumberStyles.Any, mFormatProviderCurrency, out VAT))
                VAT = 0;
            txtVAT.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
            txtVATForTax.Text = txtVAT.Text;
        }
        private void txtVATPercent_Leave(object sender, EventArgs e)
        {
            if (txtVATPercent.Text.Trim().Length == 0)
                txtVATPercent.Text = "0";

            float VAT_Percent = float.Parse(txtVATPercent.Text, mFormatProviderCurrency);

            double VAT = (double.Parse(txtSumOrder.Text, mFormatProviderCurrency) * VAT_Percent) / 100;
            txtVAT.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            CaculateSubTotal();
            txtVATForTax.Text = txtVAT.Text;
        }

        private void txtAmountForTax_Leave(object sender, EventArgs e)
        {
            if (txtAmountForTax.Text.Trim().Length == 0)
                txtAmountForTax.Text = "0";

            double Amount;
            if (!double.TryParse(txtAmountForTax.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtAmountForTax.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            float VAT_Percent = float.Parse(txtVATPercent.Text, mFormatProviderCurrency);
            double VATForTax = (double.Parse(txtAmountForTax.Text, mFormatProviderCurrency) * VAT_Percent) / 100;
            txtVATForTax.Text = VATForTax.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        private void txtVATForTax_Leave(object sender, EventArgs e)
        {
            if (txtVATForTax.Text.Trim().Length == 0)
                txtVATForTax.Text = "0";

            double VAT;
            if (!double.TryParse(txtVATForTax.Text, NumberStyles.Any, mFormatProviderCurrency, out VAT))
                VAT = 0;
            txtVATForTax.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }

        #endregion

        #region [ Layout]

        private void DisplayForInvoiceTemp()
        {
            // txtCustomerID.Enabled = false;
            //CoCustomers.Enabled = false;
            txtAddress.Enabled = false;
            txtTAX.Enabled = false;
            txtAccountBank.Enabled = false;

            cmdSaveNew.Enabled = false;
        }
        #endregion

        #region [ Short Key ]
        private void FrmSaleInvoiceDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
           mRole.Check_Role("FNC002");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

      
    }
}