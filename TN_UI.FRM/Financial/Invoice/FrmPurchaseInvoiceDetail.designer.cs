namespace TN_UI.FRM.Invoice
{
    partial class FrmPurchasesInvoiceDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchasesInvoiceDetail));
            this.label1 = new System.Windows.Forms.Label();
            this.PickerInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVendorID = new System.Windows.Forms.TextBox();
            this.lbVendorID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CoVendors = new System.Windows.Forms.ComboBox();
            this.txtTAX = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInvoiceID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.coInvoiceDesign = new System.Windows.Forms.ComboBox();
            this.txtInvoiceNumber = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtInvoiceSign = new System.Windows.Forms.TextBox();
            this.txtFollowDocument = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSumOrder = new System.Windows.Forms.TextBox();
            this.lbSumOrder = new System.Windows.Forms.Label();
            this.lbVAT = new System.Windows.Forms.Label();
            this.txtVAT = new System.Windows.Forms.TextBox();
            this.lbSubTotal = new System.Windows.Forms.Label();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.txtVATPercent = new System.Windows.Forms.TextBox();
            this.lbPercent = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdSaveClose = new System.Windows.Forms.ToolStripButton();
            this.cmdSaveNew = new System.Windows.Forms.ToolStripButton();
            this.cmdDel = new System.Windows.Forms.ToolStripButton();
            this.cmdPrinter = new System.Windows.Forms.ToolStripButton();
            this.txtTitle = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbInvoiceType = new System.Windows.Forms.Label();
            this.txtAccountBank = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInvoiceDescription = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.coCategory = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chkPayByCash = new System.Windows.Forms.CheckBox();
            this.chkPayByAccountBank = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVATForTax = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAmountForTax = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tnPickerPaymentMaturity = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dataGridViewProducts1 = new TNLibrary.IVT.Forms.DataGridViewProducts();
            this.groupBox3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(359, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 50;
            this.label1.Text = "Ngày hóa đơn";
            // 
            // PickerInvoiceDate
            // 
            this.PickerInvoiceDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PickerInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerInvoiceDate.Location = new System.Drawing.Point(449, 87);
            this.PickerInvoiceDate.Name = "PickerInvoiceDate";
            this.PickerInvoiceDate.Size = new System.Drawing.Size(113, 21);
            this.PickerInvoiceDate.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 15);
            this.label3.TabIndex = 56;
            this.label3.Text = "Số Hóa đơn";
            // 
            // txtVendorID
            // 
            this.txtVendorID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVendorID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVendorID.ForeColor = System.Drawing.Color.Navy;
            this.txtVendorID.Location = new System.Drawing.Point(96, 151);
            this.txtVendorID.Name = "txtVendorID";
            this.txtVendorID.Size = new System.Drawing.Size(116, 21);
            this.txtVendorID.TabIndex = 6;
            this.txtVendorID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVendorID_KeyDown);
            this.txtVendorID.Leave += new System.EventHandler(this.txtVendorID_Leave);
            // 
            // lbVendorID
            // 
            this.lbVendorID.AutoSize = true;
            this.lbVendorID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVendorID.ForeColor = System.Drawing.Color.Navy;
            this.lbVendorID.Location = new System.Drawing.Point(19, 155);
            this.lbVendorID.Name = "lbVendorID";
            this.lbVendorID.Size = new System.Drawing.Size(41, 15);
            this.lbVendorID.TabIndex = 80;
            this.lbVendorID.Text = "Mã Số";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(231, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 15);
            this.label5.TabIndex = 82;
            this.label5.Text = "Tên nhà cung cấp";
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtAddress.ForeColor = System.Drawing.Color.Navy;
            this.txtAddress.Location = new System.Drawing.Point(96, 177);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(893, 21);
            this.txtAddress.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(19, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 15);
            this.label8.TabIndex = 88;
            this.label8.Text = "Địa chỉ";
            // 
            // CoVendors
            // 
            this.CoVendors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.CoVendors.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.CoVendors.ForeColor = System.Drawing.Color.Navy;
            this.CoVendors.FormattingEnabled = true;
            this.CoVendors.Location = new System.Drawing.Point(344, 150);
            this.CoVendors.Name = "CoVendors";
            this.CoVendors.Size = new System.Drawing.Size(645, 23);
            this.CoVendors.TabIndex = 7;
            this.CoVendors.SelectedIndexChanged += new System.EventHandler(this.CoVendors_SelectedIndexChanged);
            // 
            // txtTAX
            // 
            this.txtTAX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtTAX.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtTAX.ForeColor = System.Drawing.Color.Navy;
            this.txtTAX.Location = new System.Drawing.Point(96, 203);
            this.txtTAX.Name = "txtTAX";
            this.txtTAX.Size = new System.Drawing.Size(290, 21);
            this.txtTAX.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(19, 209);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 90;
            this.label15.Text = "Mã số thuế";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtInvoiceID);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.coInvoiceDesign);
            this.groupBox3.Controls.Add(this.txtInvoiceNumber);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtInvoiceSign);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(758, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(231, 117);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(6, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 15);
            this.label16.TabIndex = 109;
            this.label16.Text = "Số hiệu";
            // 
            // txtInvoiceID
            // 
            this.txtInvoiceID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInvoiceID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceID.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceID.Location = new System.Drawing.Point(82, 86);
            this.txtInvoiceID.Name = "txtInvoiceID";
            this.txtInvoiceID.Size = new System.Drawing.Size(139, 21);
            this.txtInvoiceID.TabIndex = 5;
            this.txtInvoiceID.TextChanged += new System.EventHandler(this.txtInvoiceID_TextChanged);
            this.txtInvoiceID.Leave += new System.EventHandler(this.txtInvoiceID_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(6, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 15);
            this.label12.TabIndex = 95;
            this.label12.Text = "Mẫu số";
            // 
            // coInvoiceDesign
            // 
            this.coInvoiceDesign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.coInvoiceDesign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coInvoiceDesign.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coInvoiceDesign.ForeColor = System.Drawing.Color.Navy;
            this.coInvoiceDesign.FormattingEnabled = true;
            this.coInvoiceDesign.Location = new System.Drawing.Point(81, 10);
            this.coInvoiceDesign.Name = "coInvoiceDesign";
            this.coInvoiceDesign.Size = new System.Drawing.Size(140, 22);
            this.coInvoiceDesign.TabIndex = 2;
            // 
            // txtInvoiceNumber
            // 
            this.txtInvoiceNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceNumber.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceNumber.Location = new System.Drawing.Point(81, 61);
            this.txtInvoiceNumber.Name = "txtInvoiceNumber";
            this.txtInvoiceNumber.Size = new System.Drawing.Size(140, 21);
            this.txtInvoiceNumber.TabIndex = 4;
            this.txtInvoiceNumber.TextChanged += new System.EventHandler(this.txtInvoiceNumber_TextChanged);
            this.txtInvoiceNumber.Leave += new System.EventHandler(this.txtInvoiceNumber_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(6, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 15);
            this.label13.TabIndex = 97;
            this.label13.Text = "Ký hiệu";
            // 
            // txtInvoiceSign
            // 
            this.txtInvoiceSign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceSign.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceSign.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceSign.Location = new System.Drawing.Point(81, 36);
            this.txtInvoiceSign.Name = "txtInvoiceSign";
            this.txtInvoiceSign.Size = new System.Drawing.Size(140, 21);
            this.txtInvoiceSign.TabIndex = 3;
            // 
            // txtFollowDocument
            // 
            this.txtFollowDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtFollowDocument.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtFollowDocument.ForeColor = System.Drawing.Color.Navy;
            this.txtFollowDocument.Location = new System.Drawing.Point(690, 259);
            this.txtFollowDocument.Name = "txtFollowDocument";
            this.txtFollowDocument.Size = new System.Drawing.Size(299, 21);
            this.txtFollowDocument.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(582, 263);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 15);
            this.label10.TabIndex = 92;
            this.label10.Text = "Theo chứng từ số";
            // 
            // txtSumOrder
            // 
            this.txtSumOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtSumOrder.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSumOrder.ForeColor = System.Drawing.Color.Maroon;
            this.txtSumOrder.Location = new System.Drawing.Point(767, 517);
            this.txtSumOrder.Name = "txtSumOrder";
            this.txtSumOrder.Size = new System.Drawing.Size(202, 20);
            this.txtSumOrder.TabIndex = 25;
            this.txtSumOrder.Text = "0";
            this.txtSumOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSumOrder.Leave += new System.EventHandler(this.txtSumOrder_Leave);
            this.txtSumOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSumOrder_KeyPress);
            // 
            // lbSumOrder
            // 
            this.lbSumOrder.AutoSize = true;
            this.lbSumOrder.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSumOrder.ForeColor = System.Drawing.Color.Navy;
            this.lbSumOrder.Location = new System.Drawing.Point(694, 520);
            this.lbSumOrder.Name = "lbSumOrder";
            this.lbSumOrder.Size = new System.Drawing.Size(65, 15);
            this.lbSumOrder.TabIndex = 91;
            this.lbSumOrder.Text = "Thành tiền";
            // 
            // lbVAT
            // 
            this.lbVAT.AutoSize = true;
            this.lbVAT.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVAT.ForeColor = System.Drawing.Color.Navy;
            this.lbVAT.Location = new System.Drawing.Point(694, 546);
            this.lbVAT.Name = "lbVAT";
            this.lbVAT.Size = new System.Drawing.Size(70, 15);
            this.lbVAT.TabIndex = 93;
            this.lbVAT.Text = "Thuế GTGT";
            // 
            // txtVAT
            // 
            this.txtVAT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVAT.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVAT.ForeColor = System.Drawing.Color.Maroon;
            this.txtVAT.Location = new System.Drawing.Point(834, 543);
            this.txtVAT.Name = "txtVAT";
            this.txtVAT.Size = new System.Drawing.Size(136, 20);
            this.txtVAT.TabIndex = 27;
            this.txtVAT.Text = "0";
            this.txtVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVAT.Leave += new System.EventHandler(this.txtVAT_Leave);
            this.txtVAT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVAT_KeyPress);
            // 
            // lbSubTotal
            // 
            this.lbSubTotal.AutoSize = true;
            this.lbSubTotal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSubTotal.ForeColor = System.Drawing.Color.Navy;
            this.lbSubTotal.Location = new System.Drawing.Point(694, 570);
            this.lbSubTotal.Name = "lbSubTotal";
            this.lbSubTotal.Size = new System.Drawing.Size(65, 15);
            this.lbSubTotal.TabIndex = 95;
            this.lbSubTotal.Text = "Tổng cộng";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtSubTotal.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSubTotal.ForeColor = System.Drawing.Color.Maroon;
            this.txtSubTotal.Location = new System.Drawing.Point(767, 567);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(203, 20);
            this.txtSubTotal.TabIndex = 28;
            this.txtSubTotal.Text = "0";
            this.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSubTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubTotal_KeyPress);
            // 
            // txtVATPercent
            // 
            this.txtVATPercent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVATPercent.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVATPercent.ForeColor = System.Drawing.Color.Maroon;
            this.txtVATPercent.Location = new System.Drawing.Point(767, 542);
            this.txtVATPercent.Name = "txtVATPercent";
            this.txtVATPercent.Size = new System.Drawing.Size(41, 20);
            this.txtVATPercent.TabIndex = 26;
            this.txtVATPercent.Text = "0";
            this.txtVATPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVATPercent.Leave += new System.EventHandler(this.txtVATPercent_Leave);
            this.txtVATPercent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVATPercent_KeyPress);
            // 
            // lbPercent
            // 
            this.lbPercent.AutoSize = true;
            this.lbPercent.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPercent.ForeColor = System.Drawing.Color.Navy;
            this.lbPercent.Location = new System.Drawing.Point(810, 544);
            this.lbPercent.Name = "lbPercent";
            this.lbPercent.Size = new System.Drawing.Size(18, 15);
            this.lbPercent.TabIndex = 97;
            this.lbPercent.Text = "%";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSaveClose,
            this.cmdSaveNew,
            this.cmdDel,
            this.cmdPrinter});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1001, 25);
            this.toolStrip1.TabIndex = 72;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdSaveClose
            // 
            this.cmdSaveClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveClose.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveClose.Image")));
            this.cmdSaveClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdSaveClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveClose.Name = "cmdSaveClose";
            this.cmdSaveClose.Size = new System.Drawing.Size(95, 22);
            this.cmdSaveClose.Text = "Save && Close";
            this.cmdSaveClose.Click += new System.EventHandler(this.cmdSaveClose_Click);
            // 
            // cmdSaveNew
            // 
            this.cmdSaveNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveNew.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveNew.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveNew.Image")));
            this.cmdSaveNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveNew.Name = "cmdSaveNew";
            this.cmdSaveNew.Size = new System.Drawing.Size(94, 22);
            this.cmdSaveNew.Text = "Save && New";
            this.cmdSaveNew.Click += new System.EventHandler(this.cmdSaveNew_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDel.ForeColor = System.Drawing.Color.Navy;
            this.cmdDel.Image = ((System.Drawing.Image)(resources.GetObject("cmdDel.Image")));
            this.cmdDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.Size = new System.Drawing.Size(63, 22);
            this.cmdDel.Text = "Delete";
            this.cmdDel.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // cmdPrinter
            // 
            this.cmdPrinter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrinter.ForeColor = System.Drawing.Color.Navy;
            this.cmdPrinter.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrinter.Image")));
            this.cmdPrinter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdPrinter.Name = "cmdPrinter";
            this.cmdPrinter.Size = new System.Drawing.Size(63, 22);
            this.cmdPrinter.Text = "Printer";
            this.cmdPrinter.Click += new System.EventHandler(this.cmdPrinter_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(412, 38);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(107, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "HÓA ĐƠN";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(143)))), ((int)(((byte)(191)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1001, 5);
            this.panel2.TabIndex = 74;
            // 
            // lbInvoiceType
            // 
            this.lbInvoiceType.AutoSize = true;
            this.lbInvoiceType.BackColor = System.Drawing.Color.Transparent;
            this.lbInvoiceType.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbInvoiceType.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lbInvoiceType.Location = new System.Drawing.Point(398, 64);
            this.lbInvoiceType.Name = "lbInvoiceType";
            this.lbInvoiceType.Size = new System.Drawing.Size(136, 18);
            this.lbInvoiceType.TabIndex = 98;
            this.lbInvoiceType.Text = "GIÁ TRỊ GIA TĂNG";
            // 
            // txtAccountBank
            // 
            this.txtAccountBank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAccountBank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtAccountBank.ForeColor = System.Drawing.Color.Navy;
            this.txtAccountBank.Location = new System.Drawing.Point(496, 201);
            this.txtAccountBank.Name = "txtAccountBank";
            this.txtAccountBank.Size = new System.Drawing.Size(493, 21);
            this.txtAccountBank.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(427, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 15);
            this.label7.TabIndex = 104;
            this.label7.Text = "Tài khoản";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(19, 262);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 15);
            this.label14.TabIndex = 105;
            this.label14.Text = "Hình thức thanh toán";
            // 
            // txtInvoiceDescription
            // 
            this.txtInvoiceDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceDescription.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceDescription.Location = new System.Drawing.Point(95, 229);
            this.txtInvoiceDescription.Name = "txtInvoiceDescription";
            this.txtInvoiceDescription.Size = new System.Drawing.Size(894, 21);
            this.txtInvoiceDescription.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.coCategory);
            this.groupBox2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox2.Location = new System.Drawing.Point(12, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(162, 49);
            this.groupBox2.TabIndex = 107;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Loại hóa đơn";
            // 
            // coCategory
            // 
            this.coCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.coCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coCategory.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coCategory.ForeColor = System.Drawing.Color.Navy;
            this.coCategory.FormattingEnabled = true;
            this.coCategory.Location = new System.Drawing.Point(7, 19);
            this.coCategory.Name = "coCategory";
            this.coCategory.Size = new System.Drawing.Size(140, 22);
            this.coCategory.TabIndex = 0;
            this.coCategory.SelectedIndexChanged += new System.EventHandler(this.coCategory_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(19, 236);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 15);
            this.label17.TabIndex = 108;
            this.label17.Text = "Diễn giải";
            // 
            // chkPayByCash
            // 
            this.chkPayByCash.AutoSize = true;
            this.chkPayByCash.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkPayByCash.ForeColor = System.Drawing.Color.Navy;
            this.chkPayByCash.Location = new System.Drawing.Point(172, 262);
            this.chkPayByCash.Name = "chkPayByCash";
            this.chkPayByCash.Size = new System.Drawing.Size(74, 19);
            this.chkPayByCash.TabIndex = 12;
            this.chkPayByCash.Text = "Tiền mặt";
            this.chkPayByCash.UseVisualStyleBackColor = true;
            // 
            // chkPayByAccountBank
            // 
            this.chkPayByAccountBank.AutoSize = true;
            this.chkPayByAccountBank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.chkPayByAccountBank.ForeColor = System.Drawing.Color.Navy;
            this.chkPayByAccountBank.Location = new System.Drawing.Point(269, 263);
            this.chkPayByAccountBank.Name = "chkPayByAccountBank";
            this.chkPayByAccountBank.Size = new System.Drawing.Size(105, 19);
            this.chkPayByAccountBank.TabIndex = 13;
            this.chkPayByAccountBank.Text = "Chuyển khoản";
            this.chkPayByAccountBank.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtVATForTax);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtAmountForTax);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox1.Location = new System.Drawing.Point(19, 517);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 71);
            this.groupBox1.TabIndex = 112;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dành cho khai báo thuế";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(8, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 15);
            this.label4.TabIndex = 97;
            this.label4.Text = "Thuế GTGT";
            // 
            // txtVATForTax
            // 
            this.txtVATForTax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVATForTax.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVATForTax.ForeColor = System.Drawing.Color.Maroon;
            this.txtVATForTax.Location = new System.Drawing.Point(93, 43);
            this.txtVATForTax.Name = "txtVATForTax";
            this.txtVATForTax.Size = new System.Drawing.Size(136, 20);
            this.txtVATForTax.TabIndex = 30;
            this.txtVATForTax.Text = "0";
            this.txtVATForTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVATForTax.Leave += new System.EventHandler(this.txtVATForTax_Leave);
            this.txtVATForTax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVATForTax_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(8, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 15);
            this.label6.TabIndex = 96;
            this.label6.Text = "Thành tiền";
            // 
            // txtAmountForTax
            // 
            this.txtAmountForTax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtAmountForTax.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtAmountForTax.ForeColor = System.Drawing.Color.Maroon;
            this.txtAmountForTax.Location = new System.Drawing.Point(93, 19);
            this.txtAmountForTax.Name = "txtAmountForTax";
            this.txtAmountForTax.Size = new System.Drawing.Size(136, 20);
            this.txtAmountForTax.TabIndex = 29;
            this.txtAmountForTax.Text = "0";
            this.txtAmountForTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmountForTax.Leave += new System.EventHandler(this.txtAmountForTax_Leave);
            this.txtAmountForTax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmountForTax_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(380, 263);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 15);
            this.label9.TabIndex = 116;
            this.label9.Text = "Ngày thanh toán";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // tnPickerPaymentMaturity
            // 
            this.tnPickerPaymentMaturity.CustomFormat = "dd/MM/yyyy";
            this.tnPickerPaymentMaturity.Location = new System.Drawing.Point(480, 260);
            this.tnPickerPaymentMaturity.Name = "tnPickerPaymentMaturity";
            this.tnPickerPaymentMaturity.Size = new System.Drawing.Size(102, 20);
            this.tnPickerPaymentMaturity.TabIndex = 118;
            this.tnPickerPaymentMaturity.Value = new System.DateTime(((long)(0)));
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bkHeader.png");
            // 
            // dataGridViewProducts1
            // 
            this.dataGridViewProducts1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProducts1.ListProduct = ((System.Collections.ArrayList)(resources.GetObject("dataGridViewProducts1.ListProduct")));
            this.dataGridViewProducts1.Location = new System.Drawing.Point(19, 287);
            this.dataGridViewProducts1.Name = "dataGridViewProducts1";
            this.dataGridViewProducts1.Size = new System.Drawing.Size(970, 224);
            this.dataGridViewProducts1.TabIndex = 117;
            // 
            // FrmPurchasesInvoiceDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1001, 594);
            this.Controls.Add(this.tnPickerPaymentMaturity);
            this.Controls.Add(this.dataGridViewProducts1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkPayByAccountBank);
            this.Controls.Add(this.chkPayByCash);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtInvoiceDescription);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtAccountBank);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CoVendors);
            this.Controls.Add(this.txtTAX);
            this.Controls.Add(this.txtFollowDocument);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtVendorID);
            this.Controls.Add(this.lbVendorID);
            this.Controls.Add(this.lbInvoiceType);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.txtVATPercent);
            this.Controls.Add(this.lbSubTotal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PickerInvoiceDate);
            this.Controls.Add(this.txtSubTotal);
            this.Controls.Add(this.lbVAT);
            this.Controls.Add(this.txtVAT);
            this.Controls.Add(this.lbSumOrder);
            this.Controls.Add(this.txtSumOrder);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lbPercent);
            this.Controls.Add(this.groupBox3);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPurchasesInvoiceDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hóa đơn mua hàng";
            this.Load += new System.EventHandler(this.FrmPurchasesInvoice_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmPurchasesInvoiceDetail_KeyDown);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripButton cmdSaveNew;
        private System.Windows.Forms.ToolStripButton cmdPrinter;
        private System.Windows.Forms.ToolStripButton cmdSaveClose;
        private System.Windows.Forms.ToolStripButton cmdDel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker PickerInvoiceDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVendorID;
        private System.Windows.Forms.Label lbVendorID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSumOrder;
        private System.Windows.Forms.Label lbSumOrder;
        private System.Windows.Forms.Label lbVAT;
        private System.Windows.Forms.TextBox txtVAT;
        private System.Windows.Forms.Label lbSubTotal;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.TextBox txtFollowDocument;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVATPercent;
        private System.Windows.Forms.Label lbPercent;
        private System.Windows.Forms.TextBox txtTAX;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtInvoiceNumber;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox coInvoiceDesign;
        private System.Windows.Forms.TextBox txtInvoiceSign;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox CoVendors;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbInvoiceType;
        private System.Windows.Forms.TextBox txtAccountBank;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtInvoiceDescription;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInvoiceID;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox coCategory;
        private System.Windows.Forms.CheckBox chkPayByCash;
        private System.Windows.Forms.CheckBox chkPayByAccountBank;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVATForTax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAmountForTax;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private TNLibrary.IVT.Forms.DataGridViewProducts dataGridViewProducts1;
        private TNLibrary.SYS.Forms.TNDateTimePicker tnPickerPaymentMaturity;
        private System.Windows.Forms.ImageList imageList1;
    }
}