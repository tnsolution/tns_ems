// SumOrder : la tong thanh tien cua cac san pham, dich vu truoc thue
// SumTotal : La tong thanh tien bao gom thue
// AmountForTAX : danh de khai bao thue

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.FNC.Invoices;
using TNLibrary.IVT;
using TNLibrary.IVT.Forms;
using TNLibrary.CRM;
using TNLibrary.PUL;

namespace TN_UI.FRM.Invoice
{
    public partial class FrmPurchasesInvoiceDetail : Form
    {

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        public int mInvoiceKey;
        public bool FlagTemp;

        public FrmPurchasesInvoiceDetail()
        {
            InitializeComponent();
            dataGridViewProducts1.ImageHeader = imageList1.Images[0];
            dataGridViewProducts1.SetupLayoutGridView();
            dataGridViewProducts1.KeyDown += new System.Windows.Forms.KeyEventHandler(dataGridViewProducts1_KeyDown);
            dataGridViewProducts1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridViewProducts1_RowValidated);
        }

        private void FrmPurchasesInvoice_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coInvoiceDesign, "SELECT DesignKey, DesignName FROM FNC_Invoice_Design", false);
            LoadDataToToolbox.ComboBoxData(coCategory, "SELECT CategoryKey, CategoryName FROM FNC_Invoice_Categories", false);

            LoadDataToToolbox.AutoCompleteTextBox(txtVendorID, "SELECT CustomerID FROM CRM_Customers WHERE Isvendor = 1 ");
            coCategory.SelectedIndex = 0;

            mInvoice = new Invoice_Purchase_Object(mInvoiceKey);
            LoadInvoice_Purchase_Info();

            if (FlagTemp)
            {
                DisplayForInvoiceTemp();
            }

            PickerInvoiceDate.CustomFormat = mFormatDate;
            tnPickerPaymentMaturity.CustomFormat = mFormatDate;

        }

        #region [ Load Languare]
        private void LoadLanguage()
        {

        }
        #endregion

        #region [ Invoice Infomation ]

        public Invoice_Purchase_Object mInvoice = new Invoice_Purchase_Object();

        private void LoadInvoice_Purchase_Info()
        {
            txtInvoiceID.Text = mInvoice.InvoiceID.Trim();
            txtInvoiceNumber.Text = mInvoice.InvoiceNumber.Trim();
            txtInvoiceSign.Text = mInvoice.InvoiceSign;
            coInvoiceDesign.Text = mInvoice.InvoiceDesign;
            txtInvoiceDescription.Text = mInvoice.InvoiceDescription;

            PickerInvoiceDate.Value = mInvoice.InvoiceDate;
            tnPickerPaymentMaturity.Value = mInvoice.PaymentMaturity;

            txtFollowDocument.Text = mInvoice.DocumentFollow;
            chkPayByCash.Checked = mInvoice.PayByCash;
            chkPayByAccountBank.Checked = mInvoice.PayByAccountBank;


            coCategory.SelectedValue = mInvoice.CategoryKey;
            mVendor = new Customer_Info(mInvoice.CustomerKey);
            LoadInfoVendor();

            dataGridViewProducts1.ListProduct = mInvoice.Items;

            txtSumOrder.Text = mInvoice.AmountOrder.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtSubTotal.Text = mInvoice.AmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            txtVATPercent.Text = mInvoice.VATPercent.ToString(mFormatDecimal, mFormatProviderCurrency);
            txtVAT.Text = mInvoice.VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtAmountForTax.Text = mInvoice.AmountForTAX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            //kiem tra VATFORTAX
            int tmp = string.Compare(txtAmountForTax.Text, txtSumOrder.Text);
            if (tmp == 0) // VAT =VATFORTAX
                txtVATForTax.Text = mInvoice.VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            else
                txtVATForTax.Text = mInvoice.VATForTAX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);


            mInvoiceNumberChanged = false;
            mInvoiceIDChanged = false;
        }

        private bool mInvoiceNumberChanged = false, mInvoiceIDChanged = false;
        private void txtInvoiceNumber_TextChanged(object sender, EventArgs e)
        {
            mInvoiceNumberChanged = true;
        }
        private void txtInvoiceNumber_Leave(object sender, EventArgs e)
        {
            if (txtInvoiceNumber.Text.Trim().Length == 0)
                return;

            if (mInvoiceNumberChanged)
            {
                int nQuantityInvoice = Invoice_Data.CountPurchaseInvoiceNumber(txtInvoiceNumber.Text.Trim());
                if (nQuantityInvoice > 0)
                {
                    if (MessageBox.Show("Đã có số hóa đơn này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (nQuantityInvoice == 1)
                        {
                            mInvoice = new Invoice_Purchase_Object();
                            mInvoice.Get_InvoiceNumber_Info(txtInvoiceNumber.Text);
                        }
                        else
                        {
                            //FrmListInvoiceInfo frm = new FrmListInvoiceInfo();
                            //frm.mInvoiceNumber = txtInvoiceNumber.Text.Trim();
                            //frm.mType = 1; // invoice general
                            //frm.ShowDialog();
                            //int nInvoiceKey = frm.mInvoiceKey;
                            //frm.Close();
                            //mInvoice = new Invoice_Purchase_Info(nInvoiceKey);
                        }
                        LoadInvoice_Purchase_Info();
                    }
                }
                mInvoiceNumberChanged = false;
            }
        }

        private void txtInvoiceID_TextChanged(object sender, EventArgs e)
        {
            mInvoiceIDChanged = true;
        }
        private void txtInvoiceID_Leave(object sender, EventArgs e)
        {
            if (mInvoiceIDChanged)
            {
                Invoice_Purchase_Object nInvoice = new Invoice_Purchase_Object(txtInvoiceID.Text.Trim());
                if (nInvoice.InvoiceKey > 0)
                {
                    if (MessageBox.Show("Đã có số hiệu hóa đơn này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        mInvoice = nInvoice;
                        LoadInvoice_Purchase_Info();
                    }
                    else
                    {
                        txtInvoiceID.Focus();
                    }
                }
            }
        }

        private void coCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbInvoiceType.Text = coCategory.Text.ToUpper();
            lbInvoiceType.Left = (txtTitle.Left + txtTitle.Width / 2) - lbInvoiceType.Width / 2;
        }

        #endregion

        #region [ Invoice Details Infomation ]
        private void dataGridViewProducts1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            double nSumAmountCurrencyMain = 0;
            foreach (DataGridViewRow nRow in dataGridViewProducts1.Rows)
            {
                if (nRow.Cells[9].Value != null && nRow.Cells[9].ErrorText.Length == 0)
                {
                    nSumAmountCurrencyMain += double.Parse(nRow.Cells[9].Value.ToString(), mFormatProviderCurrency);
                }
            }
            txtSumOrder.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtAmountForTax.Text = nSumAmountCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
        }
        private void dataGridViewProducts1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Control)
            {
                if (dataGridViewProducts1.CurrentCell.ColumnIndex == 1)
                {
                    FRM.List.FrmProductList Frm = new FRM.List.FrmProductList();
                    Frm.FlagView = true;
                    Frm.ShowDialog();
                    dataGridViewProducts1.CurrentCell.Value = Frm.mProductID;
                    dataGridViewProducts1.BeginEdit(true);
                }
            }
        }

        #endregion

        #region [ Vendor Infomation]
        private Customer_Info mVendor = new Customer_Info();
        private void txtVendorID_Leave(object sender, EventArgs e)
        {
            if (txtVendorID.Text.Trim() == "*")
            {
                LoadDataToToolbox.ComboBoxData(CoVendors, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE Isvendor = 1", false);
            }
            else
            {
                mVendor = new Customer_Info(txtVendorID.Text.Trim());
                LoadInfoVendor();
                if (mVendor.Key == 0 && txtVendorID.Text.Trim().Length >= 1)
                    LoadDataToToolbox.ComboBoxData(CoVendors, "SELECT CustomerKey,CustomerName FROM CRM_Customers WHERE Isvendor = 1 AND CustomerID LIKE '" + txtVendorID.Text.Trim() + "%'", true);

            }
        }
        private void txtVendorID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
            {
                //FrmVendorList frm = new FrmVendorList();
                //frm.FlagView = true;
                //frm.ShowDialog();
                //if (frm.ListItemSelect != null)
                //    txtVendorID.Text = frm.ListItemSelect.Text;
                //frm.Close();
            }
        }
        private void CoVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            int nVendorKey;
            if (int.TryParse(CoVendors.SelectedValue.ToString(), out nVendorKey))
            {
                mVendor = new Customer_Info(nVendorKey);
                LoadInfoVendor();
            }

        }

        private void LoadInfoVendor()
        {
            if (mVendor.Key == 0)
            {
                CoVendors.Text = "...";
                txtAddress.Text = "...";
                txtAccountBank.Text = "...";
                txtTAX.Text = "...";
            }
            else
            {
                txtVendorID.Text = mVendor.ID.Trim();
                CoVendors.Text = mVendor.Name;
                txtAddress.Text = mVendor.Address + ", " + mVendor.CityName + ", " + mVendor.CountryName;
                txtAccountBank.Text = mVendor.AccountBank;
                txtTAX.Text = mVendor.TaxNumber;

            }
        }

        #endregion

        #region [ Activate Button ]
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa hóa đơn này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mInvoice.DeleteObject();
                ClearInvoiceInfo();
            }
        }
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (SaveInvoice_Purchase_Infomation())
            {
                if (!FlagTemp)
                {
                    mInvoice.SaveObject();
                    this.Close();
                }
                else
                {
                    this.Close();
                }
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveInvoice_Purchase_Infomation())
            {
                mInvoice.SaveObject();
                string strID = mInvoice.InvoiceID.Trim();

                ClearInvoiceInfo();
                txtInvoiceID.Text = Invoice_Data.GetAutoPurchaseInvoiceID(strID);

            }
        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmPurchaseInvoiceReport frm = new FrmPurchaseInvoiceReport();
            SaveInvoice_Purchase_Infomation();
            frm.mInvoice = mInvoice;
            frm.ShowDialog();
        }
        #endregion

        #region [ Update Invoice DataBase]

        private bool SaveInvoice_Purchase_Infomation()
        {
            if (!CheckBeforeSave()) return false;

            if (mVendor.Key == 0)
            {
                if (MessageBox.Show("Chưa có nhà cung cấp này, ? Bạn có muốn thêm nhà cung cấp này ? ", "Thêm ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    mVendor = new Customer_Info();
                    mVendor.ID = txtVendorID.Text;
                    mVendor.Name = CoVendors.Text;
                    mVendor.Address = txtAddress.Text;
                    mVendor.TaxNumber = txtTAX.Text;
                    mVendor.CategoryKey = 2; // nha cung cap
                    mVendor.IsVendor = true;
                    mVendor.CustomerType = 1;
                    mVendor.CreatedBy = SessionUser.UserLogin.Key;
                    mVendor.ModifiedBy = SessionUser.UserLogin.Key;

                    mVendor.Create();
                }
                else
                    return false;
            }

            //kiem tra VATFORTAX
            int tmp = string.Compare(txtAmountForTax.Text, txtSumOrder.Text);


            // dung de chinh lai phan lap trinh
            txtInvoiceNumber.Focus(); //dung khi sua lai ngay hoa don
            mInvoice.InvoiceID = txtInvoiceID.Text;
            mInvoice.InvoiceNumber = txtInvoiceNumber.Text;
            mInvoice.InvoiceSign = txtInvoiceSign.Text;
            mInvoice.InvoiceDesign = coInvoiceDesign.Text;
            mInvoice.InvoiceDescription = txtInvoiceDescription.Text;

            mInvoice.CustomerKey = mVendor.Key;
            mInvoice.CustomerID = mVendor.ID;
            mInvoice.CustomerName = mVendor.Name;
            mInvoice.CategoryKey = (int)coCategory.SelectedValue;
            mInvoice.IsFixedAsset = false;

            mInvoice.InvoiceDate = PickerInvoiceDate.Value;
            mInvoice.PaymentMaturity = tnPickerPaymentMaturity.Value;

            mInvoice.AmountOrder = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            mInvoice.AmountCurrencyMain = double.Parse(txtSubTotal.Text, mFormatProviderCurrency);
            mInvoice.VAT = double.Parse(txtVAT.Text, mFormatProviderCurrency);
            mInvoice.VATPercent = int.Parse(txtVATPercent.Text);

            mInvoice.AmountForTAX = double.Parse(txtAmountForTax.Text, mFormatProviderCurrency);
            mInvoice.VATForTAX = double.Parse(txtVATForTax.Text, mFormatProviderCurrency);

            mInvoice.PayByCash = chkPayByCash.Checked;
            mInvoice.PayByAccountBank = chkPayByAccountBank.Checked;

            mInvoice.BranchKey = SessionUser.UserLogin.BranchKey;
            mInvoice.CreatedBy = SessionUser.UserLogin.Key;
            mInvoice.ModifiedBy = SessionUser.UserLogin.Key;
            
            mInvoice.Items = dataGridViewProducts1.ListProduct;

            return true;

        }
        #endregion

        #region [ Function]
        private bool CheckBeforeSave()
        {

            txtInvoiceID.Focus();
            // Version trial
            DateTime nDateEndTrial = new DateTime(2012, 08, 15);
            DateTime nDateWarringTrial = new DateTime(2012, 07, 15);
            if (PickerInvoiceDate.Value < nDateWarringTrial)
            {
                // don't do anything
            }
            else
            {
                if (PickerInvoiceDate.Value < nDateEndTrial)
                {
                    MessageBox.Show("chỉ còn 1 tháng nữa là hết thời gian dùng thử. vui lòng liên hệ công ty HHT", "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Đây là bảng thử nghiệm, vui lòng liên hệ công ty HHT", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }
            if (txtInvoiceNumber.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số hóa đơn");
                txtInvoiceNumber.Focus();
                return false;
            }

            if (txtInvoiceID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập số phiếu");
                txtInvoiceID.Focus();
                return false;
            }
            if (txtVendorID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập mã nhà cung cấp");
                txtVendorID.Focus();
                return false;
            }
            if (CoVendors.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên nhà cung cấp");
                CoVendors.Focus();
                return false;
            }

            if (dataGridViewProducts1.IsError)
            {
                MessageBox.Show("Vui lòng kiểm tra lại danh sách vật tư mua hàng");
                dataGridViewProducts1.Focus();
                return false;
            }
            return true;
        }
        private void CaculateSubTotal()
        {
            double Total = double.Parse(txtSumOrder.Text, mFormatProviderCurrency);
            double VAT = double.Parse(txtVAT.Text, mFormatProviderCurrency);

            double SubTotal = Total + VAT;
            txtSubTotal.Text = SubTotal.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        Product_Info mProduct = new Product_Info();
        private void ClearInvoiceInfo()
        {

            mProduct = new Product_Info();
            mVendor = new Customer_Info();
            mInvoice = new Invoice_Purchase_Object();

            txtInvoiceID.Text = "";
            txtInvoiceDescription.Text = "";
            txtInvoiceNumber.Text = "";
            txtInvoiceSign.Text = "";
            txtVendorID.Text = "";
            CoVendors.Text = "";
            txtAddress.Text = "";

            dataGridViewProducts1.ClearProducts();

            txtSumOrder.Text = "0";
            txtVAT.Text = "0";
            txtVATPercent.Text = "0";
            txtSubTotal.Text = "0";

            txtAmountForTax.Text = "0";
            txtVATForTax.Text = "0";

            txtInvoiceID.Focus();
        }
        #endregion

        #region [ Process Inupt & Output]

        private void txtSumOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVAT_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVATPercent_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }

        }
        private void txtSubTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtAmountForTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtVATForTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }

        private void txtSumOrder_Leave(object sender, EventArgs e)
        {
            if (txtSumOrder.Text.Trim().Length == 0)
                txtSumOrder.Text = "0";

            double Amount;
            if (!double.TryParse(txtSumOrder.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtSumOrder.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
        }
        private void txtVAT_Leave(object sender, EventArgs e)
        {
            if (txtVAT.Text.Trim().Length == 0)
                txtVAT.Text = "0";

            double VAT;
            if (!double.TryParse(txtVAT.Text, NumberStyles.Any, mFormatProviderCurrency, out VAT))
                VAT = 0;
            txtVAT.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            CaculateSubTotal();
            txtVATForTax.Text = txtVAT.Text;
        }
        private void txtVATPercent_Leave(object sender, EventArgs e)
        {
            if (txtVATPercent.Text.Trim().Length == 0)
                txtVATPercent.Text = "0";

            float VAT_Percent = float.Parse(txtVATPercent.Text, mFormatProviderCurrency);

            double VAT = (double.Parse(txtSumOrder.Text, mFormatProviderCurrency) * VAT_Percent) / 100;
            txtVAT.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            CaculateSubTotal();
            txtVATForTax.Text = txtVAT.Text;
        }

        private void txtAmountForTax_Leave(object sender, EventArgs e)
        {
            if (txtAmountForTax.Text.Trim().Length == 0)
                txtAmountForTax.Text = "0";

            double Amount;
            if (!double.TryParse(txtAmountForTax.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtAmountForTax.Text = Amount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            float VAT_Percent = float.Parse(txtVATPercent.Text);
            double VATForTax = (double.Parse(txtAmountForTax.Text, mFormatProviderCurrency) * VAT_Percent) / 100;
            txtVATForTax.Text = VATForTax.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        private void txtVATForTax_Leave(object sender, EventArgs e)
        {
            if (txtVATForTax.Text.Trim().Length == 0)
                txtVATForTax.Text = "0";

            double VAT;
            if (!double.TryParse(txtVATForTax.Text, NumberStyles.Any, mFormatProviderCurrency, out VAT))
                VAT = 0;
            txtVATForTax.Text = VAT.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtInvoiceNumber.Focus();
        }

        #endregion

        #region [ Layout]

        private void DisplayForInvoiceTemp()
        {
            //txtVendorID.Enabled = false;
            // CoVendors.Enabled = false;
            txtAddress.Enabled = false;
            txtTAX.Enabled = false;
            txtAccountBank.Enabled = false;
            cmdSaveNew.Enabled = false;
        }
        #endregion

        #region [ Short Key ]
        private void FrmPurchasesInvoiceDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC001");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

       
    }
}