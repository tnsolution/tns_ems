namespace TN_UI.FRM.Invoice
{
    partial class FrmSaleInvoiceList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSaleInvoiceList));
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.ListViewData = new System.Windows.Forms.ListView();
            this.ContextMenuListView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuVoucher = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuInvoice = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.groupInvoice = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtInvoiceID = new System.Windows.Forms.TextBox();
            this.txtInvoiceNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupVendor = new System.Windows.Forms.GroupBox();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.txtVendorID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PickerToDay = new System.Windows.Forms.DateTimePicker();
            this.PickerFromDay = new System.Windows.Forms.DateTimePicker();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.txtTitle = new System.Windows.Forms.Label();
            this.cmdAddNew = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmdUnhideCenter = new System.Windows.Forms.PictureBox();
            this.cmdHideCenter = new System.Windows.Forms.PictureBox();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ContextMenuListView.SuspendLayout();
            this.panelSearch.SuspendLayout();
            this.groupInvoice.SuspendLayout();
            this.groupVendor.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panelTitleCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).BeginInit();
            this.SuspendLayout();
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "invoice2.png");
            this.SmallImageLV.Images.SetKeyName(1, "InvoiceNoFinish.png");
            // 
            // ListViewData
            // 
            this.ListViewData.ContextMenuStrip = this.ContextMenuListView;
            this.ListViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListViewData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewData.ForeColor = System.Drawing.Color.Navy;
            this.ListViewData.FullRowSelect = true;
            this.ListViewData.GridLines = true;
            this.ListViewData.Location = new System.Drawing.Point(0, 117);
            this.ListViewData.MultiSelect = false;
            this.ListViewData.Name = "ListViewData";
            this.ListViewData.ShowGroups = false;
            this.ListViewData.Size = new System.Drawing.Size(1042, 508);
            this.ListViewData.TabIndex = 16;
            this.ListViewData.UseCompatibleStateImageBehavior = false;
            this.ListViewData.View = System.Windows.Forms.View.Details;
            this.ListViewData.ItemActivate += new System.EventHandler(this.ListViewData_ItemActivate);
            this.ListViewData.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListViewData_ColumnClick);
            this.ListViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListViewData_KeyDown);
            // 
            // ContextMenuListView
            // 
            this.ContextMenuListView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuVoucher,
            this.MenuInvoice});
            this.ContextMenuListView.Name = "ContextMenuListView";
            this.ContextMenuListView.Size = new System.Drawing.Size(127, 48);
            // 
            // MenuVoucher
            // 
            this.MenuVoucher.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuVoucher.ForeColor = System.Drawing.Color.Navy;
            this.MenuVoucher.Name = "MenuVoucher";
            this.MenuVoucher.Size = new System.Drawing.Size(126, 22);
            this.MenuVoucher.Text = "Chứng từ";
            this.MenuVoucher.Click += new System.EventHandler(this.MenuVoucher_Click);
            // 
            // MenuInvoice
            // 
            this.MenuInvoice.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuInvoice.ForeColor = System.Drawing.Color.Navy;
            this.MenuInvoice.Name = "MenuInvoice";
            this.MenuInvoice.Size = new System.Drawing.Size(126, 22);
            this.MenuInvoice.Text = "Hóa đơn";
            this.MenuInvoice.Click += new System.EventHandler(this.MenuInvoice_Click);
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelSearch.Controls.Add(this.groupInvoice);
            this.panelSearch.Controls.Add(this.groupVendor);
            this.panelSearch.Controls.Add(this.groupBox3);
            this.panelSearch.Controls.Add(this.cmdSearch);
            this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSearch.Location = new System.Drawing.Point(0, 29);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(1042, 88);
            this.panelSearch.TabIndex = 19;
            // 
            // groupInvoice
            // 
            this.groupInvoice.Controls.Add(this.label16);
            this.groupInvoice.Controls.Add(this.txtInvoiceID);
            this.groupInvoice.Controls.Add(this.txtInvoiceNumber);
            this.groupInvoice.Controls.Add(this.label1);
            this.groupInvoice.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupInvoice.ForeColor = System.Drawing.Color.Navy;
            this.groupInvoice.Location = new System.Drawing.Point(513, 6);
            this.groupInvoice.Name = "groupInvoice";
            this.groupInvoice.Size = new System.Drawing.Size(239, 76);
            this.groupInvoice.TabIndex = 89;
            this.groupInvoice.TabStop = false;
            this.groupInvoice.Text = "Theo hóa đơn";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(6, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 15);
            this.label16.TabIndex = 113;
            this.label16.Text = "Số hiệu";
            // 
            // txtInvoiceID
            // 
            this.txtInvoiceID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceID.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceID.Location = new System.Drawing.Point(82, 44);
            this.txtInvoiceID.Name = "txtInvoiceID";
            this.txtInvoiceID.Size = new System.Drawing.Size(139, 21);
            this.txtInvoiceID.TabIndex = 111;
            // 
            // txtInvoiceNumber
            // 
            this.txtInvoiceNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtInvoiceNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtInvoiceNumber.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceNumber.Location = new System.Drawing.Point(81, 19);
            this.txtInvoiceNumber.Name = "txtInvoiceNumber";
            this.txtInvoiceNumber.Size = new System.Drawing.Size(140, 21);
            this.txtInvoiceNumber.TabIndex = 110;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 112;
            this.label1.Text = "Số Hóa đơn";
            // 
            // groupVendor
            // 
            this.groupVendor.Controls.Add(this.txtVendorName);
            this.groupVendor.Controls.Add(this.txtVendorID);
            this.groupVendor.Controls.Add(this.label2);
            this.groupVendor.Controls.Add(this.label5);
            this.groupVendor.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupVendor.ForeColor = System.Drawing.Color.Navy;
            this.groupVendor.Location = new System.Drawing.Point(180, 6);
            this.groupVendor.Name = "groupVendor";
            this.groupVendor.Size = new System.Drawing.Size(327, 76);
            this.groupVendor.TabIndex = 87;
            this.groupVendor.TabStop = false;
            this.groupVendor.Text = "Theo Khách hàng";
            // 
            // txtVendorName
            // 
            this.txtVendorName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVendorName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVendorName.ForeColor = System.Drawing.Color.Navy;
            this.txtVendorName.Location = new System.Drawing.Point(63, 47);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(258, 21);
            this.txtVendorName.TabIndex = 87;
            // 
            // txtVendorID
            // 
            this.txtVendorID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtVendorID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtVendorID.ForeColor = System.Drawing.Color.Navy;
            this.txtVendorID.Location = new System.Drawing.Point(63, 19);
            this.txtVendorID.Name = "txtVendorID";
            this.txtVendorID.Size = new System.Drawing.Size(258, 21);
            this.txtVendorID.TabIndex = 83;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 85;
            this.label2.Text = "Mã Số";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 15);
            this.label5.TabIndex = 86;
            this.label5.Text = "Tên ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.PickerToDay);
            this.groupBox3.Controls.Add(this.PickerFromDay);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Navy;
            this.groupBox3.Location = new System.Drawing.Point(12, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(162, 76);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Theo ngày";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(6, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 34;
            this.label3.Text = "Đến ngày";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 16);
            this.label4.TabIndex = 33;
            this.label4.Text = "Từ ngày";
            // 
            // PickerToDay
            // 
            this.PickerToDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PickerToDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerToDay.Location = new System.Drawing.Point(71, 49);
            this.PickerToDay.Name = "PickerToDay";
            this.PickerToDay.Size = new System.Drawing.Size(82, 21);
            this.PickerToDay.TabIndex = 32;
            // 
            // PickerFromDay
            // 
            this.PickerFromDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PickerFromDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PickerFromDay.Location = new System.Drawing.Point(71, 20);
            this.PickerFromDay.Name = "PickerFromDay";
            this.PickerFromDay.Size = new System.Drawing.Size(82, 21);
            this.PickerFromDay.TabIndex = 31;
            // 
            // cmdSearch
            // 
            this.cmdSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cmdSearch.ForeColor = System.Drawing.Color.Navy;
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.Location = new System.Drawing.Point(955, 9);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(75, 30);
            this.cmdSearch.TabIndex = 14;
            this.cmdSearch.Text = "Tìm";
            this.cmdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.Navy;
            this.textBox12.Location = new System.Drawing.Point(87, 289);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(157, 23);
            this.textBox12.TabIndex = 24;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 291);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 16);
            this.label20.TabIndex = 23;
            this.label20.Text = "Email";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.Navy;
            this.textBox13.Location = new System.Drawing.Point(87, 165);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(157, 23);
            this.textBox13.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(6, 168);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Region";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(87, 135);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(157, 22);
            this.comboBox3.TabIndex = 20;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.Navy;
            this.textBox14.Location = new System.Drawing.Point(87, 258);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(157, 23);
            this.textBox14.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(6, 230);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 16);
            this.label22.TabIndex = 18;
            this.label22.Text = "Home Phone";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.Navy;
            this.textBox15.Location = new System.Drawing.Point(87, 196);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(157, 23);
            this.textBox15.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(6, 197);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 16;
            this.label23.Text = "Postal";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.Navy;
            this.textBox16.Location = new System.Drawing.Point(87, 227);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(157, 23);
            this.textBox16.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(6, 260);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 16);
            this.label24.TabIndex = 16;
            this.label24.Text = "Mobile";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(6, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 16);
            this.label25.TabIndex = 14;
            this.label25.Text = "Country";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.Navy;
            this.textBox17.Location = new System.Drawing.Point(87, 104);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(157, 23);
            this.textBox17.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(6, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 16);
            this.label26.TabIndex = 12;
            this.label26.Text = "City";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.Navy;
            this.textBox18.Location = new System.Drawing.Point(87, 14);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox18.Size = new System.Drawing.Size(157, 82);
            this.textBox18.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(6, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Address";
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.ForeColor = System.Drawing.Color.Navy;
            this.textBox19.Location = new System.Drawing.Point(87, 289);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(157, 23);
            this.textBox19.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(6, 291);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Email";
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.ForeColor = System.Drawing.Color.Navy;
            this.textBox20.Location = new System.Drawing.Point(87, 165);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(157, 23);
            this.textBox20.TabIndex = 22;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(6, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "Region";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(87, 135);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(157, 22);
            this.comboBox4.TabIndex = 20;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.ForeColor = System.Drawing.Color.Navy;
            this.textBox21.Location = new System.Drawing.Point(87, 258);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(157, 23);
            this.textBox21.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(6, 230);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 16);
            this.label30.TabIndex = 18;
            this.label30.Text = "Home Phone";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.ForeColor = System.Drawing.Color.Navy;
            this.textBox22.Location = new System.Drawing.Point(87, 196);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(157, 23);
            this.textBox22.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(6, 197);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 16;
            this.label31.Text = "Postal";
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Navy;
            this.textBox23.Location = new System.Drawing.Point(87, 227);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(157, 23);
            this.textBox23.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(6, 260);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 16);
            this.label32.TabIndex = 16;
            this.label32.Text = "Mobile";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(6, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 16);
            this.label33.TabIndex = 14;
            this.label33.Text = "Country";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.Navy;
            this.textBox24.Location = new System.Drawing.Point(87, 104);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(157, 23);
            this.textBox24.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(6, 105);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 16);
            this.label34.TabIndex = 12;
            this.label34.Text = "City";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.Navy;
            this.textBox25.Location = new System.Drawing.Point(87, 14);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox25.Size = new System.Drawing.Size(157, 82);
            this.textBox25.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(6, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 10;
            this.label35.Text = "Address";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Controls.Add(this.txtTitle);
            this.panelTitleCenter.Controls.Add(this.cmdAddNew);
            this.panelTitleCenter.Controls.Add(this.pictureBox1);
            this.panelTitleCenter.Controls.Add(this.cmdUnhideCenter);
            this.panelTitleCenter.Controls.Add(this.cmdHideCenter);
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(0, 0);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(1042, 29);
            this.panelTitleCenter.TabIndex = 18;
            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(336, 5);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(276, 19);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "DANH SÁCH HÓA ĐƠN BÁN HÀNG";
            // 
            // cmdAddNew
            // 
            this.cmdAddNew.AutoSize = true;
            this.cmdAddNew.BackColor = System.Drawing.Color.Transparent;
            this.cmdAddNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAddNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cmdAddNew.ForeColor = System.Drawing.Color.Maroon;
            this.cmdAddNew.Location = new System.Drawing.Point(34, 11);
            this.cmdAddNew.Name = "cmdAddNew";
            this.cmdAddNew.Size = new System.Drawing.Size(95, 14);
            this.cmdAddNew.TabIndex = 37;
            this.cmdAddNew.Text = "Thêm hóa đơn";
            this.cmdAddNew.Click += new System.EventHandler(this.cmdAddNew_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 27);
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // cmdUnhideCenter
            // 
            this.cmdUnhideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhideCenter.BackgroundImage")));
            this.cmdUnhideCenter.Location = new System.Drawing.Point(1021, 3);
            this.cmdUnhideCenter.Name = "cmdUnhideCenter";
            this.cmdUnhideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdUnhideCenter.TabIndex = 6;
            this.cmdUnhideCenter.TabStop = false;
            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);
            // 
            // cmdHideCenter
            // 
            this.cmdHideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHideCenter.BackgroundImage")));
            this.cmdHideCenter.Location = new System.Drawing.Point(1021, 3);
            this.cmdHideCenter.Name = "cmdHideCenter";
            this.cmdHideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdHideCenter.TabIndex = 5;
            this.cmdHideCenter.TabStop = false;
            this.cmdHideCenter.Visible = false;
            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // FrmSaleInvoiceList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 625);
            this.Controls.Add(this.ListViewData);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.panelTitleCenter);
            this.Name = "FrmSaleInvoiceList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Danh sách hóa đơn bán hàng";
            this.Load += new System.EventHandler(this.FrmSaleInvoiceList_Load);
            this.ContextMenuListView.ResumeLayout(false);
            this.panelSearch.ResumeLayout(false);
            this.groupInvoice.ResumeLayout(false);
            this.groupInvoice.PerformLayout();
            this.groupVendor.ResumeLayout(false);
            this.groupVendor.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panelTitleCenter.ResumeLayout(false);
            this.panelTitleCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.Label txtTitle;
        public System.Windows.Forms.ListView ListViewData;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.PictureBox cmdHideCenter;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.PictureBox cmdUnhideCenter;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker PickerToDay;
        private System.Windows.Forms.DateTimePicker PickerFromDay;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label cmdAddNew;
        private System.Windows.Forms.GroupBox groupVendor;
        private System.Windows.Forms.TextBox txtVendorID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupInvoice;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtInvoiceID;
        private System.Windows.Forms.TextBox txtInvoiceNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip ContextMenuListView;
        private System.Windows.Forms.ToolStripMenuItem MenuVoucher;
        private System.Windows.Forms.ToolStripMenuItem MenuInvoice;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TextBox txtVendorName;





    }
}