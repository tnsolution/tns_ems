﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS.Users;
using TNLibrary.SYS;

namespace TN_UI.FRM
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
        }
        private void cmdLogin_Click(object sender, EventArgs e)
        {
            CheckLogin();

        }
        private void CheckLogin()
        {
            string[] nResult = Users_Data.CheckUser(txtUserName.Text, txtPassword.Text);

            if (nResult[0] == "ERR")
                lblMessage.Text = DisplayLang.LanguageMessage(nResult[1]);
            else
            {
                string nUserKey = nResult[1];
                SessionUser UserLogin = new SessionUser(nUserKey);
                this.Close();
            }
        }
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            SessionUser UserLogin = new SessionUser("");
            this.Close();
        }

        #region [ Chang Language ]
        private void DisplayLanguage()
        {
           // Menu_System_ConnectDatabase.Text = DisplayLang.Message(Menu_System_ConnectDatabase.Name.ToUpper());
        }
        
        #endregion

        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CheckLogin();
            }
        }
    }
}
