﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.List
{
    public partial class FrmCategories : Form
    {
        public FrmCategories()
        {
            InitializeComponent();
        }

        private void FrmCategories_Load(object sender, EventArgs e)
        {
            tnGridView1.Table = "PUL_ProductCategories";
            tnGridView1.FieldDisplay = "CategoryName";
            tnGridView1.FieldValue = "CategoryKey";
            tnGridView1.LoadDataBase();

            CheckRole();
        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);

            nRole.Check_Role("PUL001");

            if (!nRole.Edit)
            {
                tnGridView1.SetCmdEdit = false;
            }
            if (!nRole.Del)
                tnGridView1.SetCmdDel = false;

        }
    }
}
