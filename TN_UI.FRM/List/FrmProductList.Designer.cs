namespace TN_UI.FRM.List
{
    partial class FrmProductList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProductList));
            this.ImgTreeView = new System.Windows.Forms.ImageList(this.components);
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panelSearch = new System.Windows.Forms.Panel();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmdAddNew = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.Label();
            this.cmdUnhideCenter = new System.Windows.Forms.PictureBox();
            this.cmdHideCenter = new System.Windows.Forms.PictureBox();
            this.panelHead = new System.Windows.Forms.Panel();
            this.txtTitle = new System.Windows.Forms.Label();
            this.ListViewData = new System.Windows.Forms.ListView();
            this.ReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.TreeViewData = new System.Windows.Forms.TreeView();
            this.panelLineLeft = new System.Windows.Forms.Panel();
            this.panelViewLeft = new System.Windows.Forms.Panel();
            this.cmdExcel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioViewReport = new System.Windows.Forms.RadioButton();
            this.radioViewListView = new System.Windows.Forms.RadioButton();
            this.cmdImport = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.txtTitleLeft = new System.Windows.Forms.Label();
            this.cmdHide = new System.Windows.Forms.PictureBox();
            this.cmdUnhide = new System.Windows.Forms.PictureBox();
            this.panelSearch.SuspendLayout();
            this.panelTitleCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).BeginInit();
            this.panelHead.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelViewLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).BeginInit();
            this.SuspendLayout();
            // 
            // ImgTreeView
            // 
            this.ImgTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgTreeView.ImageStream")));
            this.ImgTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgTreeView.Images.SetKeyName(0, "home.png");
            this.ImgTreeView.Images.SetKeyName(1, "avalaibity_26.png");
            this.ImgTreeView.Images.SetKeyName(2, "inventory_categories_ok_26.png");
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "send_box_20.png");
            this.SmallImageLV.Images.SetKeyName(1, "product_finished_20.png");
            this.SmallImageLV.Images.SetKeyName(2, "product5.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelSearch.Controls.Add(this.cmdSearch);
            this.panelSearch.Controls.Add(this.txtProductName);
            this.panelSearch.Controls.Add(this.label36);
            this.panelSearch.Controls.Add(this.txtProductID);
            this.panelSearch.Controls.Add(this.label11);
            this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSearch.Location = new System.Drawing.Point(250, 88);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(1021, 70);
            this.panelSearch.TabIndex = 19;
            // 
            // cmdSearch
            // 
            this.cmdSearch.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.ForeColor = System.Drawing.Color.Navy;
            this.cmdSearch.Location = new System.Drawing.Point(383, 38);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(75, 23);
            this.cmdSearch.TabIndex = 14;
            this.cmdSearch.Text = "Tìm kiếm";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtProductName
            // 
            this.txtProductName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductName.ForeColor = System.Drawing.Color.Navy;
            this.txtProductName.Location = new System.Drawing.Point(119, 38);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(236, 21);
            this.txtProductName.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(13, 41);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 14);
            this.label36.TabIndex = 10;
            this.label36.Text = "Tên";
            // 
            // txtProductID
            // 
            this.txtProductID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductID.ForeColor = System.Drawing.Color.Navy;
            this.txtProductID.Location = new System.Drawing.Point(119, 9);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(236, 21);
            this.txtProductID.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(13, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 14);
            this.label11.TabIndex = 10;
            this.label11.Text = "Mã Số";
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.Navy;
            this.textBox12.Location = new System.Drawing.Point(87, 289);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(157, 23);
            this.textBox12.TabIndex = 24;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 291);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 16);
            this.label20.TabIndex = 23;
            this.label20.Text = "Email";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.Navy;
            this.textBox13.Location = new System.Drawing.Point(87, 165);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(157, 23);
            this.textBox13.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(6, 168);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Region";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(87, 135);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(157, 22);
            this.comboBox3.TabIndex = 20;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.Navy;
            this.textBox14.Location = new System.Drawing.Point(87, 258);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(157, 23);
            this.textBox14.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(6, 230);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 16);
            this.label22.TabIndex = 18;
            this.label22.Text = "Home Phone";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.Navy;
            this.textBox15.Location = new System.Drawing.Point(87, 196);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(157, 23);
            this.textBox15.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(6, 197);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 16;
            this.label23.Text = "Postal";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.Navy;
            this.textBox16.Location = new System.Drawing.Point(87, 227);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(157, 23);
            this.textBox16.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(6, 260);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 16);
            this.label24.TabIndex = 16;
            this.label24.Text = "Mobile";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(6, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 16);
            this.label25.TabIndex = 14;
            this.label25.Text = "Country";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.Navy;
            this.textBox17.Location = new System.Drawing.Point(87, 104);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(157, 23);
            this.textBox17.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(6, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 16);
            this.label26.TabIndex = 12;
            this.label26.Text = "City";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.Navy;
            this.textBox18.Location = new System.Drawing.Point(87, 14);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox18.Size = new System.Drawing.Size(157, 82);
            this.textBox18.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(6, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Address";
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.ForeColor = System.Drawing.Color.Navy;
            this.textBox19.Location = new System.Drawing.Point(87, 289);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(157, 23);
            this.textBox19.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(6, 291);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Email";
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.ForeColor = System.Drawing.Color.Navy;
            this.textBox20.Location = new System.Drawing.Point(87, 165);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(157, 23);
            this.textBox20.TabIndex = 22;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(6, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "Region";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(87, 135);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(157, 22);
            this.comboBox4.TabIndex = 20;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.ForeColor = System.Drawing.Color.Navy;
            this.textBox21.Location = new System.Drawing.Point(87, 258);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(157, 23);
            this.textBox21.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(6, 230);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 16);
            this.label30.TabIndex = 18;
            this.label30.Text = "Home Phone";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.ForeColor = System.Drawing.Color.Navy;
            this.textBox22.Location = new System.Drawing.Point(87, 196);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(157, 23);
            this.textBox22.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(6, 197);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 16;
            this.label31.Text = "Postal";
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Navy;
            this.textBox23.Location = new System.Drawing.Point(87, 227);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(157, 23);
            this.textBox23.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(6, 260);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 16);
            this.label32.TabIndex = 16;
            this.label32.Text = "Mobile";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(6, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 16);
            this.label33.TabIndex = 14;
            this.label33.Text = "Country";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.Navy;
            this.textBox24.Location = new System.Drawing.Point(87, 104);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(157, 23);
            this.textBox24.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(6, 105);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 16);
            this.label34.TabIndex = 12;
            this.label34.Text = "City";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.Navy;
            this.textBox25.Location = new System.Drawing.Point(87, 14);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox25.Size = new System.Drawing.Size(157, 82);
            this.textBox25.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(6, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 10;
            this.label35.Text = "Address";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Controls.Add(this.pictureBox1);
            this.panelTitleCenter.Controls.Add(this.cmdAddNew);
            this.panelTitleCenter.Controls.Add(this.txtSearch);
            this.panelTitleCenter.Controls.Add(this.cmdUnhideCenter);
            this.panelTitleCenter.Controls.Add(this.cmdHideCenter);
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(0, 59);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(1271, 29);
            this.panelTitleCenter.TabIndex = 18;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.TabIndex = 42;
            this.pictureBox1.TabStop = false;
            // 
            // cmdAddNew
            // 
            this.cmdAddNew.AutoSize = true;
            this.cmdAddNew.BackColor = System.Drawing.Color.Transparent;
            this.cmdAddNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAddNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cmdAddNew.ForeColor = System.Drawing.Color.Maroon;
            this.cmdAddNew.Location = new System.Drawing.Point(41, 12);
            this.cmdAddNew.Name = "cmdAddNew";
            this.cmdAddNew.Size = new System.Drawing.Size(66, 14);
            this.cmdAddNew.TabIndex = 41;
            this.cmdAddNew.Text = "Thêm mới";
            this.cmdAddNew.Click += new System.EventHandler(this.cmdAddNew_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = true;
            this.txtSearch.BackColor = System.Drawing.Color.Transparent;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.Navy;
            this.txtSearch.Location = new System.Drawing.Point(623, 4);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(61, 16);
            this.txtSearch.TabIndex = 9;
            this.txtSearch.Text = "Tìm kiếm";
            // 
            // cmdUnhideCenter
            // 
            this.cmdUnhideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhideCenter.BackgroundImage")));
            this.cmdUnhideCenter.Location = new System.Drawing.Point(694, 3);
            this.cmdUnhideCenter.Name = "cmdUnhideCenter";
            this.cmdUnhideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdUnhideCenter.TabIndex = 6;
            this.cmdUnhideCenter.TabStop = false;
            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);
            // 
            // cmdHideCenter
            // 
            this.cmdHideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHideCenter.BackgroundImage")));
            this.cmdHideCenter.Location = new System.Drawing.Point(693, 4);
            this.cmdHideCenter.Name = "cmdHideCenter";
            this.cmdHideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdHideCenter.TabIndex = 5;
            this.cmdHideCenter.TabStop = false;
            this.cmdHideCenter.Visible = false;
            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);
            // 
            // panelHead
            // 
            this.panelHead.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelHead.BackgroundImage")));
            this.panelHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHead.Controls.Add(this.txtTitle);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(1271, 59);
            this.panelHead.TabIndex = 0;
            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(317, 23);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(348, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "DANH SÁCH SẢN PHẨM - DỊCH VỤ";
            // 
            // ListViewData
            // 
            this.ListViewData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewData.ForeColor = System.Drawing.Color.Navy;
            this.ListViewData.FullRowSelect = true;
            this.ListViewData.GridLines = true;
            this.ListViewData.Location = new System.Drawing.Point(321, 164);
            this.ListViewData.MultiSelect = false;
            this.ListViewData.Name = "ListViewData";
            this.ListViewData.ShowGroups = false;
            this.ListViewData.Size = new System.Drawing.Size(257, 361);
            this.ListViewData.TabIndex = 16;
            this.ListViewData.UseCompatibleStateImageBehavior = false;
            this.ListViewData.View = System.Windows.Forms.View.Details;
            this.ListViewData.ItemActivate += new System.EventHandler(this.ListViewData_ItemActivate);
            this.ListViewData.Leave += new System.EventHandler(this.ListViewData_Leave);
            this.ListViewData.Enter += new System.EventHandler(this.ListViewData_Enter);
            this.ListViewData.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListViewData_ColumnClick);
            this.ListViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListViewData_KeyDown);
            // 
            // ReportViewer
            // 
            this.ReportViewer.ActiveViewIndex = -1;
            this.ReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ReportViewer.Location = new System.Drawing.Point(626, 221);
            this.ReportViewer.Name = "ReportViewer";
            this.ReportViewer.SelectionFormula = "";
            this.ReportViewer.ShowCloseButton = false;
            this.ReportViewer.Size = new System.Drawing.Size(339, 206);
            this.ReportViewer.TabIndex = 28;
            this.ReportViewer.ViewTimeSelectionFormula = "";
            this.ReportViewer.Visible = false;
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(192)))), ((int)(((byte)(232)))));
            this.panelLeft.Controls.Add(this.TreeViewData);
            this.panelLeft.Controls.Add(this.panelLineLeft);
            this.panelLeft.Controls.Add(this.panelViewLeft);
            this.panelLeft.Controls.Add(this.panelTitle);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 88);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(250, 585);
            this.panelLeft.TabIndex = 143;
            // 
            // TreeViewData
            // 
            this.TreeViewData.AllowDrop = true;
            this.TreeViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeViewData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeViewData.ForeColor = System.Drawing.Color.Navy;
            this.TreeViewData.Location = new System.Drawing.Point(0, 29);
            this.TreeViewData.Name = "TreeViewData";
            this.TreeViewData.Size = new System.Drawing.Size(250, 388);
            this.TreeViewData.TabIndex = 8;
            // 
            // panelLineLeft
            // 
            this.panelLineLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelLineLeft.BackgroundImage")));
            this.panelLineLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLineLeft.Location = new System.Drawing.Point(0, 417);
            this.panelLineLeft.Name = "panelLineLeft";
            this.panelLineLeft.Size = new System.Drawing.Size(250, 20);
            this.panelLineLeft.TabIndex = 7;
            // 
            // panelViewLeft
            // 
            this.panelViewLeft.BackColor = System.Drawing.Color.White;
            this.panelViewLeft.Controls.Add(this.cmdExcel);
            this.panelViewLeft.Controls.Add(this.pictureBox2);
            this.panelViewLeft.Controls.Add(this.label1);
            this.panelViewLeft.Controls.Add(this.radioViewReport);
            this.panelViewLeft.Controls.Add(this.radioViewListView);
            this.panelViewLeft.Controls.Add(this.cmdImport);
            this.panelViewLeft.Controls.Add(this.pictureBox3);
            this.panelViewLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelViewLeft.Location = new System.Drawing.Point(0, 437);
            this.panelViewLeft.Name = "panelViewLeft";
            this.panelViewLeft.Size = new System.Drawing.Size(250, 148);
            this.panelViewLeft.TabIndex = 6;
            // 
            // cmdExcel
            // 
            this.cmdExcel.AutoSize = true;
            this.cmdExcel.BackColor = System.Drawing.Color.Transparent;
            this.cmdExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExcel.Font = new System.Drawing.Font("Arial", 8F);
            this.cmdExcel.ForeColor = System.Drawing.Color.Navy;
            this.cmdExcel.Location = new System.Drawing.Point(49, 87);
            this.cmdExcel.Name = "cmdExcel";
            this.cmdExcel.Size = new System.Drawing.Size(81, 14);
            this.cmdExcel.TabIndex = 94;
            this.cmdExcel.Text = "Export To Excel";
            this.cmdExcel.Click += new System.EventHandler(this.cmdExcel_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(14, 84);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(36, 23);
            this.pictureBox2.TabIndex = 95;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "-----------------------------------------------------------";
            // 
            // radioViewReport
            // 
            this.radioViewReport.AutoSize = true;
            this.radioViewReport.ForeColor = System.Drawing.Color.Navy;
            this.radioViewReport.Location = new System.Drawing.Point(15, 29);
            this.radioViewReport.Name = "radioViewReport";
            this.radioViewReport.Size = new System.Drawing.Size(118, 17);
            this.radioViewReport.TabIndex = 2;
            this.radioViewReport.Text = "Hiện thị dạng report";
            this.radioViewReport.UseVisualStyleBackColor = true;
            this.radioViewReport.CheckedChanged += new System.EventHandler(this.radioViewReport_CheckedChanged);
            // 
            // radioViewListView
            // 
            this.radioViewListView.AutoSize = true;
            this.radioViewListView.Checked = true;
            this.radioViewListView.ForeColor = System.Drawing.Color.Navy;
            this.radioViewListView.Location = new System.Drawing.Point(15, 9);
            this.radioViewListView.Name = "radioViewListView";
            this.radioViewListView.Size = new System.Drawing.Size(107, 17);
            this.radioViewListView.TabIndex = 1;
            this.radioViewListView.TabStop = true;
            this.radioViewListView.Text = "Hiện thị dạng lưới";
            this.radioViewListView.UseVisualStyleBackColor = true;
            this.radioViewListView.CheckedChanged += new System.EventHandler(this.radioViewListView_CheckedChanged);
            // 
            // cmdImport
            // 
            this.cmdImport.AutoSize = true;
            this.cmdImport.BackColor = System.Drawing.Color.Transparent;
            this.cmdImport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmdImport.ForeColor = System.Drawing.Color.Navy;
            this.cmdImport.Location = new System.Drawing.Point(50, 63);
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Size = new System.Drawing.Size(62, 13);
            this.cmdImport.TabIndex = 44;
            this.cmdImport.Text = "Import Data";
            this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(15, 58);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(24, 23);
            this.pictureBox3.TabIndex = 45;
            this.pictureBox3.TabStop = false;
            // 
            // panelTitle
            // 
            this.panelTitle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitle.BackgroundImage")));
            this.panelTitle.Controls.Add(this.txtTitleLeft);
            this.panelTitle.Controls.Add(this.cmdHide);
            this.panelTitle.Controls.Add(this.cmdUnhide);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(250, 29);
            this.panelTitle.TabIndex = 5;
            // 
            // txtTitleLeft
            // 
            this.txtTitleLeft.AutoSize = true;
            this.txtTitleLeft.BackColor = System.Drawing.Color.Transparent;
            this.txtTitleLeft.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitleLeft.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitleLeft.Location = new System.Drawing.Point(12, 7);
            this.txtTitleLeft.Name = "txtTitleLeft";
            this.txtTitleLeft.Size = new System.Drawing.Size(64, 16);
            this.txtTitleLeft.TabIndex = 4;
            this.txtTitleLeft.Text = "Advances";
            // 
            // cmdHide
            // 
            this.cmdHide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHide.BackgroundImage")));
            this.cmdHide.Location = new System.Drawing.Point(223, 0);
            this.cmdHide.Name = "cmdHide";
            this.cmdHide.Size = new System.Drawing.Size(27, 28);
            this.cmdHide.TabIndex = 3;
            this.cmdHide.TabStop = false;
            // 
            // cmdUnhide
            // 
            this.cmdUnhide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhide.BackgroundImage")));
            this.cmdUnhide.Location = new System.Drawing.Point(1, 0);
            this.cmdUnhide.Name = "cmdUnhide";
            this.cmdUnhide.Size = new System.Drawing.Size(27, 28);
            this.cmdUnhide.TabIndex = 2;
            this.cmdUnhide.TabStop = false;
            this.cmdUnhide.Visible = false;
            // 
            // FrmProductList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1271, 673);
            this.Controls.Add(this.ListViewData);
            this.Controls.Add(this.ReportViewer);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelTitleCenter);
            this.Controls.Add(this.panelHead);
            this.Name = "FrmProductList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sản phẩm - Dịch vụ";
            this.Load += new System.EventHandler(this.FrmProductList_Load);
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            this.panelTitleCenter.ResumeLayout(false);
            this.panelTitleCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).EndInit();
            this.panelHead.ResumeLayout(false);
            this.panelHead.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelViewLeft.ResumeLayout(false);
            this.panelViewLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.ImageList ImgTreeView;
        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.PictureBox cmdHideCenter;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.PictureBox cmdUnhideCenter;
        private System.Windows.Forms.Label txtSearch;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label cmdAddNew;
        public System.Windows.Forms.ListView ListViewData;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer ReportViewer;
        private System.Windows.Forms.Panel panelLeft;
        public System.Windows.Forms.TreeView TreeViewData;
        private System.Windows.Forms.Panel panelLineLeft;
        private System.Windows.Forms.Panel panelViewLeft;
        private System.Windows.Forms.Label cmdExcel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioViewReport;
        private System.Windows.Forms.RadioButton radioViewListView;
        private System.Windows.Forms.Label cmdImport;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Label txtTitleLeft;
        private System.Windows.Forms.PictureBox cmdHide;
        private System.Windows.Forms.PictureBox cmdUnhide;





    }
}