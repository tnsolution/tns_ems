using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary.SYS;
using TNLibrary.IVT;
using TN_UI.FRM.Reports;
using TNLibrary.PUL;


using TN_UI.FRM.Inventory;


namespace TN_UI.FRM.List
{
    public partial class FrmproductDetailReport : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        public Product_Info mProductList;

        public FrmproductDetailReport()
        {
            InitializeComponent();
            
        }

        private void FrmStockNoteInputReport_Load(object sender, EventArgs e)
        {
           
            //LoadDataToToolbox.ComboBoxData(coCategories, "SELECT CategoryKey,CategoryName FROM PUL_ProductCategories", true);
            SetupCrystalReport();
            PopulateCrystalReport();
        }
    
        
        #region [ CrystalReport]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {
            ReportDocument rpt = new ReportDocument();
            rpt.Load("..//..//FilesReport\\ProductDetail.rpt");

            DataTable nTableDetail = TableReport.TableProducts();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            int nNo = 0;

         
            nNo++;
               

                DataRow nRowDetail = nTableDetail.NewRow();
                nRowDetail["No"] = nNo.ToString();
                nRowDetail["ProductID"] = mProductList.ID;
                nRowDetail["ProductName"] = mProductList.Name;
                nRowDetail["Unit"] = mProductList.Unit;
                nRowDetail["SalePrice"] = mProductList.SalePrice.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRowDetail["PurchasePrice"] = mProductList.PurchasePrice.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRowDetail["VAT"] = mProductList.VAT;
                //string ckey = mProductList.CategoryKey;
               // p
                nRowDetail["CategoryName"] = mProductList.CategoryName;
               
                nTableDetail.Rows.Add(nRowDetail);



                rpt.Database.Tables["Products"].SetDataSource(nTableDetail);
                rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

                crystalReportViewer1.ReportSource = rpt;
                crystalReportViewer1.Zoom(1);


            }
        #endregion

        }   

    }
