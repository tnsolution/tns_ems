using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.CrystalReports.Engine;

using TNConfig;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.SYS.Forms;
using TNLibrary.IVT;
using TNLibrary.PUL;

using TN_UI.FRM.Inventory;
using TN_UI.FRM.Reports;


namespace TN_UI.FRM.List
{
    public partial class FrmProductList : Form
    {
        TreeViewData tv = new TreeViewData();
        private ReportDocument rpt = new ReportDocument();
        public string mProductID = "";
        public bool FlagView;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private DataTable mProductList;

        public FrmProductList()
        {
            InitializeComponent();
            InitLayoutForm();
        }

        private void FrmProductList_Load(object sender, EventArgs e)
        {
            CheckRole();

            TreeViewData.ImageList = ImgTreeView;
            tv.Table = "PUL_ProductCategories";
            tv.RootName = "Danh mục sản phẩm";
            tv.Initialize(TreeViewData);

            InitListView(ListViewData);
            cmdHide_Click(null, EventArgs.Empty);


            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;
            mProductList = Products_Data.ProductList();
            LoadData();
        }
        private void LoadData()
        {

            if (ListViewData.Visible)
                LoadDataToListView(ListViewData);
            else
                LoadDataToCrystalReport();
        }
        //Layout 
        private void InitLayoutForm()
        {

            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);

            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);

            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);

            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);

            this.cmdUnhide.MouseLeave += new System.EventHandler(this.cmdUnhide_MouseLeave);
            this.cmdUnhide.Click += new System.EventHandler(this.cmdUnhide_Click);
            this.cmdUnhide.MouseEnter += new System.EventHandler(this.cmdUnhide_MouseEnter);

            this.cmdHide.MouseLeave += new System.EventHandler(this.cmdHide_MouseLeave);
            this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
            this.cmdHide.MouseEnter += new System.EventHandler(this.cmdHide_MouseEnter);

            this.TreeViewData.Enter += new System.EventHandler(this.TreeViewData_Enter);
            this.TreeViewData.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewData_AfterSelect);
            this.TreeViewData.Leave += new System.EventHandler(this.TreeViewData_Leave);

            this.TreeViewData.DragLeave += new System.EventHandler(this.TreeViewData_DragLeave);
            this.TreeViewData.DragDrop += new System.Windows.Forms.DragEventHandler(this.TreeViewData_DragDrop);
            this.TreeViewData.DragEnter += new System.Windows.Forms.DragEventHandler(this.TreeViewData_DragEnter);
            this.TreeViewData.DragOver += new System.Windows.Forms.DragEventHandler(this.TreeViewData_DragOver);
            this.ListViewData.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.ListViewData_ItemDrag);

            panelSearch.Height = 0;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            cmdUnhideCenter.Image = imageList3.Images[0];

            ListViewData.Dock = DockStyle.Fill;
            ReportViewer.Dock = DockStyle.Fill;
            ReportViewer.DisplayGroupTree = false;
        }

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Lay out left  ]

        private void cmdHide_MouseEnter(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[1];
        }

        private void cmdHide_MouseLeave(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[0];
        }

        private void cmdHide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 28;  //  ContainerLeft.SplitterDistance = 10;
            TreeViewData.Visible = false;

            cmdHide.Visible = false;
            txtTitleLeft.Visible = false;
            panelLineLeft.Visible = false;
            panelViewLeft.Visible = false;

            cmdUnhide.Visible = true;
        }


        private void cmdUnhide_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[3];
        }

        private void cmdUnhide_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[2];
        }

        private void cmdUnhide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 250;// ContainerLeft.SplitterDistance = 260;
            TreeViewData.Visible = true;
            cmdHide.Visible = true;
            txtTitleLeft.Visible = true;
            panelLineLeft.Visible = true;
            panelViewLeft.Visible = true;

            cmdUnhide.Visible = false;

        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            cmdHide.Left = panelTitle.Width - cmdHide.Width;
        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ Tree View ]

        private TreeNode mNodeSave;
        private void TreeViewData_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            TreeNode nodeCurrent = e.Node;
            //clear all sub-folders
            nodeCurrent.Nodes.Clear();

            tv.LoadChildNodeTree(nodeCurrent, true);
            int nKey = int.Parse(nodeCurrent.Tag.ToString());

            mProductList = Products_Data.ProductList(nKey);
            LoadData();

            nodeCurrent.ForeColor = Color.Navy;

            mNodeSave = TreeViewData.SelectedNode;
            this.Cursor = Cursors.Default;
        }
        private void TreeViewData_Leave(object sender, EventArgs e)
        {
            mNodeSave = TreeViewData.SelectedNode;
            if (mNodeSave != null)
                mNodeSave.BackColor = Color.Silver;
        }
        private void TreeViewData_Enter(object sender, EventArgs e)
        {
            if (mNodeSave != null)
                mNodeSave.BackColor = Color.White;
        }

        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;

        private void InitListView(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "Mã số";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên ";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá Nhập";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn giá Bán";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "VAT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Loại";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            LV.SmallImageList = SmallImageLV;

            LVGroup = new ListViewMyGroup(LV);

        }
        public void LoadDataToListView(ListView LV)
        {

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = mProductList.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = mProductList.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = nRow["ProductID"].ToString();
                lvi.Tag = nRow["ProductKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Unit"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double PurchasePrice = double.Parse(nRow["PurchasePrice"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = PurchasePrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                double SalePrice = double.Parse(nRow["SalePrice"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = SalePrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["VAT"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim(); ;
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }


        }

        private void ListViewData_Leave(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListViewItem ListItemSelect = ListViewData.SelectedItems[0];
                if (ListItemSelect != null)
                {
                    ListItemSelect.BackColor = Color.Silver;
                }
            }
        }
        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListViewData.SelectedItems[0].BackColor = Color.White;
            }
        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }


        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (FlagView)
            {
                mProductID = ListViewData.SelectedItems[0].SubItems[0].Text;
                this.Close();
            }
            else
            {
                int nProductKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                FrmProductDetail frm = new FrmProductDetail();
                frm.mProductKey = nProductKey;
                frm.ShowDialog();
                mProductList = Products_Data.ProductList();
                LoadDataToListView(ListViewData);
                SelectIndexInListView(nProductKey);
            }
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode.ToString() == "Delete" && mRole.Del)
            {

                if (ListViewData.SelectedItems.Count > 0)
                {
                    int nResult =  Products_Data.CheckExistProduct((int)ListViewData.SelectedItems[0].Tag);
                    if (nResult > 0)
                    {
                        MessageBox.Show("Không thể xóa sản phẩm này, vui lòng kiểm tra lại!");
                    }
                    else
                    {
                        if (MessageBox.Show("Bạn có muốn xóa sản phẩm " + ListViewData.SelectedItems[0].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            int nIndex = ListViewData.SelectedItems[0].Index;
                            Product_Info nProduct = new Product_Info((int)ListViewData.SelectedItems[0].Tag);
                            nProduct.Delete();
                            ListViewData.SelectedItems[0].Remove();
                            if (ListViewData.Items.Count > 0)
                                if (nIndex == 0)
                                    ListViewData.Items[nIndex].Selected = true;
                                else
                                    ListViewData.Items[nIndex - 1].Selected = true;
                            ListViewData.Focus();
                        }
                    }
                }
                else
                    MessageBox.Show("Chọn sản phẩm để xóa");

            }
        }
        #endregion

        #region [ Drap and Drop List view to Tree View]
        private ListViewItem ItemMove;
        private void ListViewData_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (mRole.Edit)
            {
                ItemMove = (ListViewItem)e.Item;
                (sender as ListView).DoDragDrop(ItemMove.Text, DragDropEffects.Move);
            }
        }

        private TreeNode hoverNode;
        private void TreeViewData_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
        private void TreeViewData_DragOver(object sender, DragEventArgs e)
        {
            Point mouseLocation = TreeViewData.PointToClient(new Point(e.X, e.Y));
            TreeNode node = TreeViewData.GetNodeAt(mouseLocation);

            if (node != null)
            {
                if (hoverNode == null)
                {
                    node.BackColor = Color.LightBlue;
                    node.ForeColor = Color.White;
                    hoverNode = node;
                }
                else if (hoverNode != node)
                {
                    hoverNode.BackColor = Color.White;
                    hoverNode.ForeColor = Color.Navy;
                    node.BackColor = Color.LightBlue;
                    node.ForeColor = Color.White;
                    hoverNode = node;
                }
            }
        }
        private void TreeViewData_DragDrop(object sender, DragEventArgs e)
        {
            Point dropLocation = (sender as TreeView).PointToClient(new Point(e.X, e.Y));
            TreeNode dropNode = (sender as TreeView).GetNodeAt(dropLocation);

            ChangeCategory((int)ItemMove.Tag, (int)dropNode.Tag);
            ResetNodeColor(hoverNode);
            // ResetTreeColors(null, EventArgs.Empty); 

        }
        private void TreeViewData_DragLeave(object sender, EventArgs e)
        {
            ResetNodeColor(hoverNode);
        }

        private void ChangeCategory(int ProductKey, int CategoryKey)
        {
            Products_Data.UpdateCategory(ProductKey, CategoryKey);
            ItemMove.Remove();
        }
        private void ResetNodeColor(TreeNode Node)
        {
            if (Node != null)
            {
                Node.BackColor = Color.White;
                Node.ForeColor = Color.Navy;
            }
        }

        #endregion

        #region [ Crystall Report ]
        private void LoadDataToCrystalReport()
        {
            ReportDocument rpt = new ReportDocument();
            rpt.Load("..//..//FilesReport\\CrystalReportProducts.rpt");

            DataTable nTableDetail = TableReport.TableProducts();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            int nNo = 0;

            foreach (DataRow nRow in mProductList.Rows)
            {
                nNo++;
                double nSalePrice = double.Parse(nRow["SalePrice"].ToString());
                double nPurchasePrice = double.Parse(nRow["PurchasePrice"].ToString());

                DataRow nRowDetail = nTableDetail.NewRow();
                nRowDetail["No"] = nNo.ToString();
                nRowDetail["ProductID"] = nRow["ProductID"].ToString();
                nRowDetail["ProductName"] = nRow["ProductName"].ToString().Trim();
                nRowDetail["Unit"] = nRow["Unit"].ToString().Trim();
                nRowDetail["SalePrice"] = nSalePrice.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRowDetail["PurchasePrice"] = nPurchasePrice.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRowDetail["VAT"] = nRow["VAT"].ToString().Trim();
                nRowDetail["CategoryName"] = nRow["CategoryName"].ToString().Trim();

                nTableDetail.Rows.Add(nRowDetail);

            }

            rpt.Database.Tables["Products"].SetDataSource(nTableDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            ReportViewer.ReportSource = rpt;
            ReportViewer.Zoom(1);

        }
        #endregion

        #region [ Export ]
        private void cmdExcel_Click(object sender, EventArgs e)
        {
            DataTable nTableDetail = TableReport.TableProducts();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            int nNo = 0;

            foreach (DataRow nRow in mProductList.Rows)
            {
                nNo++;
                double nSalePrice = double.Parse(nRow["SalePrice"].ToString());
                double nPurchasePrice = double.Parse(nRow["PurchasePrice"].ToString());

                DataRow nRowDetail = nTableDetail.NewRow();
                nRowDetail["No"] = nNo.ToString();
                nRowDetail["ProductID"] = nRow["ProductID"].ToString();
                nRowDetail["ProductName"] = nRow["ProductName"].ToString().Trim();
                nRowDetail["Unit"] = nRow["Unit"].ToString().Trim();
                nRowDetail["SalePrice"] = nSalePrice.ToString(mFormatDecimal, mFormatProviderDecimal);
                nRowDetail["PurchasePrice"] = nPurchasePrice.ToString(mFormatDecimal, mFormatProviderDecimal); ;
                nRowDetail["VAT"] = nRow["VAT"].ToString().Trim();
                nRowDetail["CategoryName"] = nRow["CategoryName"].ToString().Trim();

                nTableDetail.Rows.Add(nRowDetail);
            }

            ExportExcel(nTableDetail, nCompanyInfo);
        }
        private void ExportExcel(DataTable TableDetail, DataTable TableCompanyInfo)
        {
            string nFileName, nFileType;
            SaveFileDialog nDialogSave = new SaveFileDialog();
            nDialogSave.DefaultExt = "xls";
            nDialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            nDialogSave.Title = "Save a file to...";

            if (nDialogSave.ShowDialog() == DialogResult.OK)
            {
                nFileName = nDialogSave.FileName;
                nFileType = System.IO.Path.GetExtension(nFileName);

                /////////////////////////

                Excel.Application nExcel = new Excel.Application();
                Excel.Workbook nWorkbook = nExcel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet nExSheet = (Excel.Worksheet)nWorkbook.Worksheets[1];

                nExcel.ActiveWindow.DisplayGridlines = true;

                // Add Head Infor
                DataRow nRowHead = TableCompanyInfo.Rows[0];
                nExcel.Cells[1, 1] = nRowHead["CompanyName"].ToString();
                nExcel.Cells[2, 1] = nRowHead["Address"].ToString();

                //Add Tieu de
                nExcel.Cells[4, 4] = "DANH SÁCH SẢN PHẨM - DỊCH VỤ";
                //add Column Name

                nExcel.Cells[6, 1] = "STT";
                nExcel.Cells[6, 2] = "Mã sản phẩm";
                nExcel.Cells[6, 3] = "Tên sản phẩm";
                nExcel.Cells[6, 4] = "Đơn vị";
                nExcel.Cells[6, 5] = "Giá bán";
                nExcel.Cells[6, 6] = "Giá mua";
                nExcel.Cells[6, 7] = "VAT";
                nExcel.Cells[6, 8] = "Loai sản phẩm";

                int iCol = 0;
                int iRow = 0;
                foreach (DataRow nRow in TableDetail.Rows)
                {
                    iRow++;
                    iCol = 0;
                    foreach (DataColumn nCol in TableDetail.Columns)
                    {
                        iCol++;
                        nExcel.Cells[iRow + 6, iCol] = nRow[nCol.ColumnName];
                    }
                }

                //add Footer
                nExcel.Cells[iRow + 9, 2] = "    Người ghi sổ";
                nExcel.Cells[iRow + 9, 4] = "Kế toán trưởng";
                nExcel.Cells[iRow + 9, 8] = "        Giám đốc";


                ////tao border
                Excel.Range nRange;
                Excel.Worksheet worksheet1 = (Excel.Worksheet)nExcel.ActiveSheet;
                //dinh dang tieu de
                nRange = worksheet1.get_Range("A4", "H4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;

                //dinh dang footer

                int nRo1 = iRow + 9;

                //int nRo3 = iRow + 6;
                nRange = worksheet1.get_Range("A" + nRo1, "H" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                nRange.Font.Size = 13;

                //nRange = worksheet1.get_Range("A" + nRo2, "H" + nRo2);
                //nRange.Font.Bold = true;


                //nRange = worksheet1.get_Range("A" + nRo3, "H" + nRo3);
                //nRange.Font.Bold = true;
                //nRange.Font.Size = 13;
                nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;


                //dinh dang columns Name
                nRange = worksheet1.get_Range("A6", "H6");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Cells.RowHeight = 25;
                nRange.Interior.Color = Color.Silver.ToArgb();

                nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                //dinh dang Columns width

                nRange = worksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 5;

                nRange = worksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = worksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 45;

                nRange = worksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = worksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = worksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = worksheet1.get_Range("G1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = worksheet1.get_Range("H1", Type.Missing);
                nRange.Cells.ColumnWidth = 30;

                // chon vung tao border
                int x = iRow + 6;
                nRange = worksheet1.get_Range("A6", "H" + x);
                nRange.Borders.Weight = 2;

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo nFileSave = new FileInfo(nFileName);
                if (nFileSave.Exists)
                    nFileSave.Delete(); // delete the file if it already exist.

                nExcel.Visible = false;
                nWorkbook.SaveAs(nFileName,
                Excel.XlFileFormat.xlWorkbookNormal, null, null,
                false, false, Excel.XlSaveAsAccessMode.xlExclusive,
                false, false, false, false, false);


                nWorkbook.Close(false, false, false);
                nExcel.Workbooks.Close();
                nExcel.Application.Quit();
                nExcel.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(nWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExcel);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nRange);
                nExSheet = null;
                nWorkbook = null;
                nExcel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

        }

        #endregion

        #region [ Import ]
        private void cmdImport_Click(object sender, EventArgs e)
        {
            int nCategoryKey = 1;
            FrmImportProducts frm = new FrmImportProducts();
            frm.mCategoryKey = nCategoryKey;
            frm.ShowDialog();
            mProductList = Products_Data.ProductList();
            LoadData();
        }
        #endregion

        #region [ Activate on Button ]
        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            FrmProductDetail frm = new FrmProductDetail();
            frm.ShowDialog();

            mProductList = Products_Data.ProductList();
            LoadData();

        }
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            mProductList = Products_Data.ProductList(txtProductID.Text.Trim(), txtProductName.Text.Trim());
            LoadData();
        }

        private void radioViewListView_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = true;
            ReportViewer.Visible = false;
            LoadDataToListView(ListViewData);
        }
        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = false;
            ReportViewer.Visible = true;
            LoadDataToCrystalReport();
        }
        #endregion

        #region [ Function ]
        private void SelectIndexInListView(int ProductKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == ProductKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }
        #endregion

        #region [ Sercutiry ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("PUL002");

            if (!mRole.Edit)
                cmdImport.Enabled = false;
        }
        #endregion

    }
}