using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.IVT;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.PUL;
using CrystalDecisions.CrystalReports.Engine;
using TN_UI.FRM.Inventory;


namespace TN_UI.FRM.List
{
    public partial class FrmProductDetail : Form
    {
        public int mProductKey = 0;
        public Product_Info mProduct;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mFromatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        public bool FlagTemp;
        public FrmProductDetail()
        {
            InitializeComponent();
        }

        private void FrmProductDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(coCategories, "SELECT CategoryKey,CategoryName FROM PUL_ProductCategories", true);
            LoadDataToToolbox.ComboBoxData(CoProductType, "SELECT ProductTypeKey,ProductTypeName FROM PUL_ProductType", false);
            CoProductType.SelectedIndex = 0;

            mProduct = new Product_Info(mProductKey);
            LoadInfoProduct();


        }

        #region [ Product Infomation ]
        private bool mProductIDChanged = false;
        private void txtProductID_TextChanged(object sender, EventArgs e)
        {
            mProductIDChanged = true;
        }
        private void txtProductID_Leave(object sender, EventArgs e)
        {
            if (mProductIDChanged)
            {
                Product_Info nProduct = new Product_Info(txtProductID.Text.Trim());
                if (nProduct.Key > 0)
                {
                    if (MessageBox.Show("Đã có mã sản phẩm này!,Bạn muốn hiển thị ra thông tin không ? ", "Thông báo", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        mProduct = nProduct;
                        LoadInfoProduct();
                    }
                    else
                    {
                        txtProductID.Focus();
                    }
                }
                mProductIDChanged = false;
            }
        }
        protected void LoadInfoProduct()
        {
            txtProductID.Text = mProduct.ID;
            txtProductName.Text = mProduct.Name;
            txtUnit.Text = mProduct.Unit;
            txtVAT.Text = mProduct.VAT.ToString("N0");
            txtSalePrice.Text = mProduct.SalePrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
            txtPurchasePrice.Text = mProduct.PurchasePrice.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);
            txtDescription.Text = mProduct.Descriptiontion;

            txtStoreMin.Text = mProduct.StoreMin.ToString(mFromatDecimal, mFormatProviderDecimal);
            txtStoreMax.Text = mProduct.StoreMax.ToString(mFromatDecimal, mFormatProviderDecimal);

            coCategories.SelectedValue = mProduct.CategoryKey;
            CoProductType.SelectedValue = mProduct.TypeKey;
        }

        #endregion

        #region [ Update Product Infomation]

        private bool SaveProductInfomation()
        {
            mProduct.ID = txtProductID.Text.Trim();
            mProduct.Name = txtProductName.Text;
            mProduct.Unit = txtUnit.Text;
            mProduct.VAT = float.Parse(txtVAT.Text);
            mProduct.PurchasePrice = double.Parse(txtPurchasePrice.Text, NumberStyles.Any, mFormatProviderCurrency);
            mProduct.SalePrice = double.Parse(txtSalePrice.Text, NumberStyles.Any, mFormatProviderCurrency);

            mProduct.CategoryKey = int.Parse(coCategories.SelectedValue.ToString());
            mProduct.CategoryName = coCategories.Text;
            mProduct.TypeKey = int.Parse(CoProductType.SelectedValue.ToString());

            mProduct.Descriptiontion = txtDescription.Text;
            mProduct.StoreMin = float.Parse(txtStoreMin.Text, NumberStyles.Any, mFormatProviderDecimal);
            mProduct.StoreMax = float.Parse(txtStoreMax.Text, NumberStyles.Any, mFormatProviderDecimal);

            // Tracking History
            mProduct.CreatedBy = SessionUser.UserLogin.Key;
            mProduct.ModifiedBy = SessionUser.UserLogin.Key;


            return true;


        }
        private bool CheckBeforeSave()
        {

            if (txtProductID.Text.Trim().Length == 0)
            {
                MessageBox.Show("vui lòng nhập mã sản phẩm");
                txtProductID.Focus();
                return false;
            }
            float nStoreMin = float.Parse(txtStoreMin.Text, NumberStyles.Any, mFormatProviderDecimal);
            float nStoreMax = float.Parse(txtStoreMax.Text, NumberStyles.Any, mFormatProviderDecimal);
            if (nStoreMin > nStoreMax)
            {
                MessageBox.Show("vui lòng kiểm tra số lượng giới hạn tồn kho");
                txtStoreMax.Focus();
                return false;
            }
            return true;

        }

        #endregion

        #region [ Process Input & Output Toolbox ]

        private void txtPurchasePrice_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtPurchasePrice.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtPurchasePrice.Text = Amount.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);

        }
        private void txtSalePrice_Leave(object sender, EventArgs e)
        {
            double Amount;
            if (!double.TryParse(txtSalePrice.Text, NumberStyles.Any, mFormatProviderCurrency, out Amount))
                Amount = 0;
            txtSalePrice.Text = Amount.ToString(mFormatCurrencyMainTwoPoint, mFormatProviderCurrency);

        }
        private void txtPurchasePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void txtSalePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }

        #endregion

        #region [ Action Button]

        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveProductInfomation();
                mProduct.Save();
                this.Close();
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave())
            {
                SaveProductInfomation();
                mProduct.Save();

                mProduct = new Product_Info();
                LoadInfoProduct();
                txtProductID.Focus();
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (Projects_Data.CheckExistProject(mProduct.Key) > 0)
                MessageBox.Show("Không thể xóa sản phẩm - dịch vụ này");
            else
            {
                mProduct.Delete();
                this.Close();
            }
        }

        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmproductDetailReport frm = new FrmproductDetailReport();
            SaveProductInfomation();
            frm.mProductList = mProduct;
            frm.ShowDialog();
        }

        #endregion

        #region [ Short Key]
        private void FrmProductDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Security ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("PUL002");
            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion
    }
}