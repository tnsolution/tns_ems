﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TN_UI.FRM.Systems;
using TNConfig;
namespace TN_UI.FRM
{
    public partial class FrmMain : Form
    {
        GlobalSystemConfig GlobalSystemConfig = new GlobalSystemConfig();
        private bool IsConnected = false;
        public FrmMain()
        {
            InitializeComponent();
            FrmSplash f = new FrmSplash();
            f.ShowDialog();
            IsConnected = f.IsConnected;
            lblDataBase.Text = "Database : " + f.strDataBase;

            f.Dispose();
            txtLanguage.Text = "Language : VN  ";
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            LoadMenuEMS();
            LoadMenuAction();

            DisplayLanguage();
            if (!IsConnected)
            {
                SetStatusMenu(false);
                lblStatusLogin.Text = "Status : Connect failed ";
                Menu_System_ConnectDatabase_Click(sender, null);
            }
            else
            {
                lblStatusLogin.Text = "Status : Connected  ";
                FrmLogin frm = new FrmLogin();
                frm.ShowDialog();
                string nUserNameLogin = SessionUser.UserLogin.Name;
                if (nUserNameLogin == "")
                {
                    //Application.Exit();n
                    SetStatusMenu(false);
                }
                else
                {
                    SetStatusMenu(true);
                    lblUserName.Text = SessionUser.UserLogin.Name;
                    GlobalSystemConfig.LoadGlobalSystemConfig();

                    FRM.FrmReportMain frmReport = new FrmReportMain();
                    frmReport.MdiParent = this;
                    frmReport.Show();
                }

            }

        }

        #region [ Menu ]
        private void LoadMenuEMS()
        {
            MenuMain_System.Visible = true;

            MenuMain_List.Visible = true;

            MenuMain_HRM.Visible = true;
            Menu_hrm_Payroll.Visible = false;
            Menu_hrm_Payroll_List.Visible = true;
            Menu_hrm_Timekeeping.Visible = false;

            MenuMain_CRM.Visible = true;

            MenuMain_Inventory.Visible = true;
            MenuMain_Financial.Visible = true;

        }



        private void LoadMenuAction()
        {
            //=============[ System      ]==============
            this.Menu_System_CompanyInfo.Click += new System.EventHandler(this.Menu_System_CompanyInfo_Click);
            this.Menu_System_FinancialYears.Click += new System.EventHandler(this.Menu_System_FinancialYear_Click);
            this.Menu_System_ConnectDatabase.Click += new System.EventHandler(this.Menu_System_ConnectDatabase_Click);
            this.Menu_System_Preferences.Click += new System.EventHandler(this.Menu_System_Preferences_Click);
            this.Menu_System_Users.Click += new System.EventHandler(this.Menu_System_Users_Click);
            this.Menu_System_ChangPass.Click += new System.EventHandler(this.Menu_System_ChangPass_Click);
            this.Menu_System_Logout.Click += new System.EventHandler(this.Menu_System_Logout_Click);
            this.Menu_System_Exit.Click += new System.EventHandler(this.Menu_System_Exit_Click);

            //=============[ List        ]==============
            this.Menu_List_Product_Categories.Click += new System.EventHandler(this.Menu_List_Product_Categories_Click);
            this.Menu_List_Product_AddNew.Click += new System.EventHandler(this.Menu_List_Product_AddNew_Click);
            this.Menu_List_Products.Click += new System.EventHandler(this.Menu_List_Products_Click);

            //=============[ HRM         ]==============
            this.Menu_hrm_Department.Click += new System.EventHandler(this.Menu_hrm_Department_Click);
            this.Menu_hrm_Branch.Click += new System.EventHandler(this.Menu_hrm_Branch_Click);
            this.Menu_hrm_Employees.Click += new System.EventHandler(this.Menu_hrm_Employees_Click);
            this.Menu_hrm_Payroll_List.Click += new System.EventHandler(this.Menu_hrm_Payroll_List_Click);
            //=============[ CRM         ]==============
            this.Menu_crm_Vendor_AddNew.Click += new System.EventHandler(this.Menu_crm_Vendor_AddNew_Click);
            this.Menu_crm_Vendor_List.Click += new System.EventHandler(this.Menu_crm_Vendor_List_Click);
            this.Menu_crm_Vendor_Categories.Click += new System.EventHandler(this.Menu_crm_Vendor_Categories_Click);
            this.Menu_crm_Customer_AddNew.Click += new System.EventHandler(this.Menu_crm_Customer_AddNew_Click);
            this.Menu_crm_Customer_List.Click += new System.EventHandler(this.Menu_crm_Customer_List_Click);
            this.Menu_crm_Customer_Categories.Click += new System.EventHandler(this.Menu_crm_Customer_Categories_Click);
            this.Menu_crm_Task_AddNew.Click += new System.EventHandler(this.Menu_crm_Task_AddNew_Click);
            this.Menu_crm_Task_List.Click += new System.EventHandler(this.Menu_crm_Task_List_Click);
            this.Menu_crm_Contract_AddNew.Click += new System.EventHandler(this.Menu_crm_Contract_AddNew_Click);
            this.Menu_crm_Contract_List.Click += new System.EventHandler(this.Menu_crm_Contract_List_Click);

            //=============[ Inventory   ]==============
            this.Menu_Inventory_Warehouse.Click += new System.EventHandler(this.Menu_Inventory_Warehouse_Click);
            this.Menu_Inventory_Input_AddNew.Click += new System.EventHandler(this.Menu_Inventory_Input_AddNew_Click);
            this.Menu_Inventory_Input_List.Click += new System.EventHandler(this.Menu_Inventory_Input_List_Click);
            this.Menu_Inventory_Output_AddNew.Click += new System.EventHandler(this.Menu_Inventory_Output_AddNew_Click);
            this.Menu_Inventory_Output_List.Click += new System.EventHandler(this.Menu_Inventory_Output_List_Click);
            this.Menu_Inventory_Begin.Click += new System.EventHandler(this.Menu_Inventory_Begin_Click);
            this.Menu_Inventory_List.Click += new System.EventHandler(this.Menu_Inventory_List_Click);

            //=============[ Financial   ]==============
            this.Menu_Financial_Purchase_AddNew.Click += new System.EventHandler(this.Menu_Financial_Purchase_AddNew_Click);
            this.Menu_Financial_Purchase_List.Click += new System.EventHandler(this.Menu_Financial_Purchase_List_Click);
            this.Menu_Financial_Sale_AddNew.Click += new System.EventHandler(this.Menu_Financial_Sale_AddNew_Click);
            this.Menu_Financial_Sale_List.Click += new System.EventHandler(this.Menu_Financial_Sale_List_Click);
            this.Menu_Financial_Payment_AddNew.Click += new System.EventHandler(this.Menu_Financial_Payment_AddNew_Click);
            this.Menu_Financial_Payment_List.Click += new System.EventHandler(this.Menu_Financial_Payment_List_Click);
            this.Menu_Financial_Receipt_AddNew.Click += new System.EventHandler(this.Menu_Financial_Receipt_AddNew_Click);
            this.Menu_Financial_Receipt_List.Click += new System.EventHandler(this.Menu_Financial_Receipt_List_Click);
            this.Menu_Financial_Dept_Begin.Click += new System.EventHandler(this.Menu_Financial_Dept_Begin_Click);

            this.Menu_Financial_Report_Dept.Click += new System.EventHandler(this.Menu_Financial_Report_Dept_Click);
            this.Menu_Financial_Report_BillDetail.Click += new System.EventHandler(this.Menu_Financial_Report_BillDetail_Click);


        }

        private void SetStatusMenu(bool Value)
        {
            this.Menu_System_CompanyInfo.Enabled = Value;
            this.Menu_System_FinancialYears.Enabled = Value;
            this.Menu_System_Preferences.Enabled = Value;
            this.Menu_System_Users.Enabled = Value;
            this.Menu_System_ChangPass.Enabled = Value;

            this.MenuMain_List.Enabled = Value;
            this.MenuMain_HRM.Enabled = Value;
            this.MenuMain_CRM.Enabled = Value;
            this.MenuMain_Inventory.Enabled = Value;
            this.MenuMain_Financial.Enabled = Value;
            if (!Value)
            {
                lblStatusLogin.Text = "Status : Connect failed ";
                lblDataBase.Text = "Database : ";
            }
        }

        #endregion

        #region [ Change language ]
        private void DisplayLanguage()
        {
            Menu_System_ConnectDatabase.Text = DisplayLang.Menu(Menu_System_ConnectDatabase.Name.ToUpper());
        }
        #endregion

        #region [ System ]
        private void Menu_System_CompanyInfo_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS001");
            if (nRole.Read)
            {
                FrmCompanyInfo frm = new FrmCompanyInfo();
                frm.ShowDialog();
            }
        }
        private void Menu_System_FinancialYear_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS002");
            if (nRole.Read)
            {
                FRM.Systems.FrmFinancialYear frm = new FrmFinancialYear();
                frm.ShowDialog();
            }
        }
        private void Menu_System_ConnectDatabase_Click(object sender, EventArgs e)
        {
            FrmConnectServer frm = new FrmConnectServer();
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.Yes)
            {
                ConnectDataBase nConnect = new ConnectDataBase(frm.mConnectDataBaseInfo.ConnectionString);

                lblStatusLogin.Text = "Status : Connected  ";
                FrmLogin frmLogin = new FrmLogin();
                frmLogin.ShowDialog();
                string nUserNameLogin = SessionUser.UserLogin.Name;
                if (nUserNameLogin == "")
                {
                    SetStatusMenu(false);
                    //Application.Exit();
                }
                else
                {
                    SetStatusMenu(true);
                    lblUserName.Text = SessionUser.UserLogin.Name;
                    if (!frm.mConnectDataBaseInfo.IsConnectLocal)
                        lblDataBase.Text = "Database : " + frm.mConnectDataBaseInfo.DataSource + "->" + frm.mConnectDataBaseInfo.DataBase;
                    else
                        lblDataBase.Text = "Database : " + frm.mConnectDataBaseInfo.DataSource + "->" + frm.mConnectDataBaseInfo.AttachDbFilename;

                    GlobalSystemConfig.LoadGlobalSystemConfig();
                }

            }
        }

        private void Menu_System_Preferences_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS004");
            if (nRole.Read)
            {
                FrmSystemConfig frm = new FrmSystemConfig();
                frm.ShowDialog();
            }
        }

        private void Menu_System_Users_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS005");
            if (nRole.Read)
            {
                FRM.Users.FrmUserList frm = new TN_UI.FRM.Users.FrmUserList();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        private void Menu_System_ChangPass_Click(object sender, EventArgs e)
        {

            FRM.Users.FrmChangpassUser frm = new TN_UI.FRM.Users.FrmChangpassUser();
            frm.ShowDialog();

        }
        private void Menu_System_Logout_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.ShowDialog();
            string nUserNameLogin = SessionUser.UserLogin.Name;
            if (nUserNameLogin == "")
            {
                SetStatusMenu(false);
                //Application.Exit();
            }
            else
            {
                SetStatusMenu(true);
                lblUserName.Text = SessionUser.UserLogin.Name;
                GlobalSystemConfig.LoadGlobalSystemConfig();
            }
        }

        private void Menu_System_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion

        #region [ List ]

        private void Menu_List_Product_Categories_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("PUL001");
            if (nRole.Read)
            {
                FRM.List.FrmCategories frm = new FRM.List.FrmCategories();
                frm.ShowDialog();
            }
        }

        private void Menu_List_Product_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("PUL002");
            if (nRole.Edit)
            {
                FRM.List.FrmProductDetail frm = new TN_UI.FRM.List.FrmProductDetail();
                frm.ShowDialog();
            }
        }

        private void Menu_List_Products_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("PUL002");
            if (nRole.Read)
            {
                FRM.List.FrmProductList frm = new TN_UI.FRM.List.FrmProductList();
                frm.MdiParent = this;
                frm.Show();
            }
        }


        #endregion

        #region [ HRM ]
        private void Menu_hrm_Department_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM001");
            if (nRole.Read)
            {
                FRM.HRM.FrmDepartments frm = new TN_UI.FRM.HRM.FrmDepartments();
                frm.ShowDialog();
            }
        }
        private void Menu_hrm_Branch_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM001");
            if (nRole.Read)
            {
                FRM.HRM.FrmBranch frm = new TN_UI.FRM.HRM.FrmBranch();
                frm.ShowDialog();
            }
        }
        private void Menu_hrm_Employees_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM002");
            if (nRole.Read)
            {
                FRM.HRM.FrmEmployeesList frm = new TN_UI.FRM.HRM.FrmEmployeesList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_hrm_Payroll_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("HRM003");
            if (nRole.Read)
            {
                FRM.HRM.FrmSalaryMonth frm = new TN_UI.FRM.HRM.FrmSalaryMonth();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        #endregion

        #region [ CRM ]
        private void Menu_crm_Vendor_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM002");
            if (nRole.Edit)
            {
                FRM.CRM.FrmCustomerDetail frm = new TN_UI.FRM.CRM.FrmCustomerDetail();
                frm.mCustomerTypeUI = 1;
                frm.ShowDialog();
            }
        }
        private void Menu_crm_Vendor_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM002");
            if (nRole.Read)
            {
                FRM.CRM.FrmCustomerList frm = new TN_UI.FRM.CRM.FrmCustomerList();
                frm.MdiParent = this;
                frm.mTitle = "DANH SÁCH NHÀ CUNG CẤP";
                frm.Text = "Danh sách nhà cung cấp";
                frm.mCustomerType = 1;
                frm.Show();
            }
        }
        private void Menu_crm_Vendor_Categories_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM001");
            if (nRole.Read)
            {
                FRM.CRM.FrmCategories frm = new FRM.CRM.FrmCategories();
                frm.m_Parent = 1;
                frm.Text = "Danh mục nhà cung cấp";
                frm.ShowDialog();
            }
        }

        private void Menu_crm_Customer_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM004");
            if (nRole.Edit)
            {
                FRM.CRM.FrmCustomerDetail frm = new TN_UI.FRM.CRM.FrmCustomerDetail();
                frm.mCustomerTypeUI = 2;
                frm.ShowDialog();
            }
        }

        private void Menu_crm_Customer_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM004");
            if (nRole.Read)
            {
                FRM.CRM.FrmCustomerList frm = new TN_UI.FRM.CRM.FrmCustomerList();
                frm.MdiParent = this;
                frm.mTitle = "DANH SÁCH KHÁCH HÀNG";
                frm.Text = "Danh sách khách hàng";
                frm.mCustomerType = 2;
                frm.Show();
            }
        }

        private void Menu_crm_Customer_Categories_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM003");
            if (nRole.Read)
            {
                FRM.CRM.FrmCategories frm = new FRM.CRM.FrmCategories();
                frm.m_Parent = 2;
                frm.Text = "Danh mục Khách hàng";
                frm.ShowDialog();
            }
        }

        private void Menu_crm_Task_AddNew_Click(object sender, EventArgs e)
        {
            FRM.CRM.FrmTaskDetail frm = new TN_UI.FRM.CRM.FrmTaskDetail();
            frm.ShowDialog();
        }

        private void Menu_crm_Task_List_Click(object sender, EventArgs e)
        {
            FRM.CRM.FrmTaskList frm = new TN_UI.FRM.CRM.FrmTaskList();
            frm.MdiParent = this;
            frm.Show();
        }

        private void Menu_crm_Task_Categories_Click(object sender, EventArgs e)
        {

        }

        private void Menu_crm_Contract_AddNew_Click(object sender, EventArgs e)
        {

        }

        private void Menu_crm_Contract_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM006");
            if (nRole.Read)
            {
                FRM.CRM.FrmProjectList frm = new TN_UI.FRM.CRM.FrmProjectList();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        #endregion

        #region [ Inventory ]
        private void Menu_Inventory_Warehouse_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT001");
            if (nRole.Read)
            {
                FRM.Inventory.FrmWarehouseList frm = new TN_UI.FRM.Inventory.FrmWarehouseList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Inventory_Input_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT002");
            if (nRole.Edit)
            {
                FRM.Inventory.FrmStockNoteInputDetail frm = new TN_UI.FRM.Inventory.FrmStockNoteInputDetail();
                frm.ShowDialog();
            }
        }

        private void Menu_Inventory_Input_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT002");
            if (nRole.Read)
            {
                FRM.Inventory.FrmStockNoteInputList frm = new TN_UI.FRM.Inventory.FrmStockNoteInputList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Inventory_Output_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT003");
            if (nRole.Edit)
            {
                FRM.Inventory.FrmStockNoteOutputDetail frm = new TN_UI.FRM.Inventory.FrmStockNoteOutputDetail();
                frm.ShowDialog();
            }
        }

        private void Menu_Inventory_Output_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT003");
            if (nRole.Read)
            {
                FRM.Inventory.FrmStockNoteOutputList frm = new TN_UI.FRM.Inventory.FrmStockNoteOutputList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Inventory_Begin_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT004");
            if (nRole.Read)
            {
                FRM.Inventory.FrmBegin_Inventory frm = new TN_UI.FRM.Inventory.FrmBegin_Inventory();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Inventory_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("IVT005");
            if (nRole.Read)
            {
                FRM.Inventory.FrmReportInventory frm = new TN_UI.FRM.Inventory.FrmReportInventory();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        #endregion

        #region [ Financial ]
        private void Menu_Financial_Purchase_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC001");
            if (nRole.Edit)
            {
                FRM.Invoice.FrmPurchasesInvoiceDetail frm = new TN_UI.FRM.Invoice.FrmPurchasesInvoiceDetail();
                frm.ShowDialog();
            }
        }
        private void Menu_Financial_Purchase_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC001");
            if (nRole.Read)
            {
                FRM.Invoice.FrmPurchaseInvoiceList frm = new TN_UI.FRM.Invoice.FrmPurchaseInvoiceList();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        private void Menu_Financial_Sale_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC002");
            if (nRole.Edit)
            {
                FRM.Invoice.FrmSaleInvoiceDetail frm = new TN_UI.FRM.Invoice.FrmSaleInvoiceDetail();
                frm.ShowDialog();
            }
        }
        private void Menu_Financial_Sale_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC002");
            if (nRole.Read)
            {
                FRM.Invoice.FrmSaleInvoiceList frm = new TN_UI.FRM.Invoice.FrmSaleInvoiceList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Financial_Payment_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC003");
            if (nRole.Edit)
            {
                FRM.Bills.FrmPaymentDetail frm = new TN_UI.FRM.Bills.FrmPaymentDetail();
                frm.ShowDialog();
            }
        }
        private void Menu_Financial_Payment_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC003");
            if (nRole.Read)
            {
                FRM.Bills.FrmPaymentList frm = new TN_UI.FRM.Bills.FrmPaymentList();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        private void Menu_Financial_Receipt_AddNew_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC004");
            if (nRole.Edit)
            {
                FRM.Bills.FrmReceiptDetail frm = new TN_UI.FRM.Bills.FrmReceiptDetail();
                frm.ShowDialog();
            }
        }
        private void Menu_Financial_Receipt_List_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC004");
            if (nRole.Read)
            {
                FRM.Bills.FrmReceiptList frm = new TN_UI.FRM.Bills.FrmReceiptList();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Financial_Dept_Begin_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC005");
            if (nRole.Read)
            {
                FRM.CRM.FrmBegin_Customer frm = new TN_UI.FRM.CRM.FrmBegin_Customer();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void Menu_Financial_Report_Dept_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC006");
            if (nRole.Read)
            {
                FRM.CRM.FrmCustomerLiabilities frm = new TN_UI.FRM.CRM.FrmCustomerLiabilities();
                frm.MdiParent = this;
                frm.Show();
            }
        }
        private void Menu_Financial_Report_BillDetail_Click(object sender, EventArgs e)
        {
            FRM.Reports.Frm_FNC_BillDetail frm = new FRM.Reports.Frm_FNC_BillDetail();
            frm.MdiParent = this;
            frm.Show();
        }
        private void Menu_Financial_Report_Misc_Click(object sender, EventArgs e)
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("FNC007");
            if (nRole.Read)
            {
                FRM.Reports.FrmMain frm = new TN_UI.FRM.Reports.FrmMain();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        #endregion

        private void Menu_Help_AboutUs_Click(object sender, EventArgs e)
        {
            FrmAboutUs frm = new FrmAboutUs();
            frm.ShowDialog();

        }

        private void MenuMain_Inventory_Click(object sender, EventArgs e)
        {

        }
    }
}
