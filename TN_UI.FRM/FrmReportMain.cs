﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TNLibrary.SYS;
using TNLibrary.FNC.Invoices;
using TNLibrary.FNC.Bills;

namespace TN_UI.FRM
{
    public partial class FrmReportMain : Form
    {
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmReportMain()
        {
            InitializeComponent();
        }

        private void FrmReportMain_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
           
           
           // InitListViewInventory(ListViewInventory);
            LoadPurcharInfo();
            LoadSaleInfo();
            LoadInfoFinancial();
            LoadTaskToDay();
        }
        #region [Invoice ]
        ////////// Purchase Invoice ///////////
        private void LoadPurcharInfo()
        {
            InitListViewInvoice(ListViewPurchases);
            LoadDataInvoice(ListViewPurchases, Invoice_Data.VendorPayToDay());
        }
        
        ///////// Sale Invoice ///////////
        private void LoadSaleInfo()
        {
            InitListViewInvoice(ListViewSale);
            LoadDataInvoice(ListViewSale, Invoice_Data.CustomerPayToDay());
        }
        public void LoadDataInvoice(ListView LV, DataTable InvoiceList)
        {

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = InvoiceList.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = InvoiceList.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = nRow["InvoiceID"].ToString();
                lvi.Tag = nRow["InvoiceKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmountCurrency = double.Parse(nRow["AmountCurrencyMain"].ToString());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmountCurrency.ToString(mFormatCurrencyMain,mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

        }
        private void InitListViewInvoice(ListView LV)
        {

            ColumnHeader colHead;
           
            colHead = new ColumnHeader();
            colHead.Text = "Mã HĐ ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Công ty/Khách hàng";
            colHead.Width = 280;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số tiền";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);
        }
        #endregion

        #region [ Chart Financial ]
        private void LoadInfoFinancial()
        {
            
            GroupFinancial.Text += DateTime.Now.Month.ToString();  
            double nAmountInvoicePurchase = Invoice_Data.Purchase_TotalAmount(DateTime.Now.Month,DateTime.Now.Year);
            double nAmountInvoiceSale = Invoice_Data.Sale_TotalAmount(DateTime.Now.Month, DateTime.Now.Year);
            double nAmountPayment = Cash_Bills_Data.Payment_TotalAmount(DateTime.Now.Month, DateTime.Now.Year);
            double nAmountReceipt = Cash_Bills_Data.Receipt_TotalAmount(DateTime.Now.Month, DateTime.Now.Year);

            double nMax = nAmountInvoicePurchase;
            if (nAmountInvoiceSale > nMax)
                nMax = nAmountInvoiceSale;
            if (nAmountPayment > nMax)
                nMax = nAmountPayment;
            if (nAmountReceipt > nMax)
                nMax = nAmountReceipt;

            if (nMax == 0)
                nMax = 1;
            int nWidth = 170;

            ChartInvoicePurchar.Width = Convert.ToInt32((nAmountInvoicePurchase * nWidth) / nMax);
            txtInvoicePurcharse.Text = nAmountInvoicePurchase.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            ChartInvoiceSale.Width = Convert.ToInt32((nAmountInvoiceSale * nWidth) / nMax);
            txtInvoiceSale.Text = nAmountInvoiceSale.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            ChartPayment.Width = Convert.ToInt32((nAmountPayment * nWidth) / nMax);
            txtPayment.Text = nAmountPayment.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            ChartReceipt.Width = Convert.ToInt32((nAmountReceipt * nWidth) / nMax);
            txtReceipt.Text = nAmountReceipt.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }
        #endregion

        #region [ Task ]
        private void LoadTaskToDay()
        {
            DataTable nTable = Tasks_Data.Task_Today(SessionUser.UserLogin.Key);
            foreach (DataRow nRow in nTable.Rows)
            {
                LBTaskToday.Items.Add(nRow["TaskName"].ToString());
            }
        }
        #endregion
    }
}
