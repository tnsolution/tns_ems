﻿namespace TN_UI.FRM.Users
{
    partial class FrmUserRole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.coRoleList = new System.Windows.Forms.ComboBox();
            this.chkRead = new System.Windows.Forms.CheckBox();
            this.chkEdit = new System.Windows.Forms.CheckBox();
            this.chkDel = new System.Windows.Forms.CheckBox();
            this.cmdSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtRoleName = new System.Windows.Forms.TextBox();
            this.chkAdd = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // coRoleList
            // 
            this.coRoleList.FormattingEnabled = true;
            this.coRoleList.Location = new System.Drawing.Point(12, 12);
            this.coRoleList.Name = "coRoleList";
            this.coRoleList.Size = new System.Drawing.Size(246, 21);
            this.coRoleList.TabIndex = 0;
            // 
            // chkRead
            // 
            this.chkRead.AutoSize = true;
            this.chkRead.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRead.ForeColor = System.Drawing.Color.Navy;
            this.chkRead.Location = new System.Drawing.Point(12, 39);
            this.chkRead.Name = "chkRead";
            this.chkRead.Size = new System.Drawing.Size(47, 18);
            this.chkRead.TabIndex = 1;
            this.chkRead.Text = "Xem";
            this.chkRead.UseVisualStyleBackColor = true;
            // 
            // chkEdit
            // 
            this.chkEdit.AutoSize = true;
            this.chkEdit.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEdit.ForeColor = System.Drawing.Color.Navy;
            this.chkEdit.Location = new System.Drawing.Point(104, 39);
            this.chkEdit.Name = "chkEdit";
            this.chkEdit.Size = new System.Drawing.Size(46, 18);
            this.chkEdit.TabIndex = 3;
            this.chkEdit.Text = "Sửa";
            this.chkEdit.UseVisualStyleBackColor = true;
            // 
            // chkDel
            // 
            this.chkDel.AutoSize = true;
            this.chkDel.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDel.ForeColor = System.Drawing.Color.Navy;
            this.chkDel.Location = new System.Drawing.Point(213, 39);
            this.chkDel.Name = "chkDel";
            this.chkDel.Size = new System.Drawing.Size(45, 18);
            this.chkDel.TabIndex = 4;
            this.chkDel.Text = "Xóa";
            this.chkDel.UseVisualStyleBackColor = true;
            // 
            // cmdSave
            // 
            this.cmdSave.Location = new System.Drawing.Point(91, 6);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(75, 23);
            this.cmdSave.TabIndex = 5;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.AliceBlue;
            this.panel1.Controls.Add(this.cmdSave);
            this.panel1.Controls.Add(this.chkAdd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(274, 32);
            this.panel1.TabIndex = 6;
            // 
            // txtRoleName
            // 
            this.txtRoleName.Location = new System.Drawing.Point(13, 11);
            this.txtRoleName.Name = "txtRoleName";
            this.txtRoleName.ReadOnly = true;
            this.txtRoleName.Size = new System.Drawing.Size(245, 20);
            this.txtRoleName.TabIndex = 7;
            // 
            // chkAdd
            // 
            this.chkAdd.AutoSize = true;
            this.chkAdd.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAdd.ForeColor = System.Drawing.Color.Navy;
            this.chkAdd.Location = new System.Drawing.Point(186, 10);
            this.chkAdd.Name = "chkAdd";
            this.chkAdd.Size = new System.Drawing.Size(72, 18);
            this.chkAdd.TabIndex = 2;
            this.chkAdd.Text = "Thêm mới";
            this.chkAdd.UseVisualStyleBackColor = true;
            this.chkAdd.Visible = false;
            // 
            // FrmUserRole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(274, 99);
            this.Controls.Add(this.txtRoleName);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkDel);
            this.Controls.Add(this.chkEdit);
            this.Controls.Add(this.chkRead);
            this.Controls.Add(this.coRoleList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmUserRole";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Phân Quyền";
            this.Load += new System.EventHandler(this.FrmUserRole_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox coRoleList;
        private System.Windows.Forms.CheckBox chkRead;
        private System.Windows.Forms.CheckBox chkEdit;
        private System.Windows.Forms.CheckBox chkDel;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtRoleName;
        private System.Windows.Forms.CheckBox chkAdd;
    }
}