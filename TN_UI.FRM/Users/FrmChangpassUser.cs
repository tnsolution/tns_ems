using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.HRM;

namespace TN_UI.FRM.Users
{
    public partial class FrmChangpassUser : Form
    {
        public string mUserKey = "";
        public string mUserName;
        public User_Info mUser = new User_Info();

        string mFormatDate = GlobalSystemConfig.FormatDate;

        public FrmChangpassUser()
        {
            InitializeComponent();
        }

        private void FrmChangpassUser_Load(object sender, EventArgs e)
        {
           
            LoadDataToToolbox.ComboBoxData(coEmployees, "SELECT EmployeeKey,EmployeeID,LastName,FirstName FROM HRM_Employees ORDER BY FirstName", 15, true);
            coEmployees.SelectedValue = 0;
            mUser = new User_Info(SessionUser.UserLogin.Key);
            LoadInfoUser();

        }

        #region [ Load Info User ]
        private bool mUserNameChanged = false;
        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            mUserNameChanged = true;
        }
        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length == 0) return;
            if (mUserNameChanged)
            {
                User_Info nUser = new User_Info(txtUserName.Text.Trim(), true);
                if (nUser.Key.Trim().Length > 0)
                {
                    if (MessageBox.Show(" Have this ID ! do you want view it ?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        mUser = nUser;
                        LoadInfoUser();
                    }
                    else
                    {
                        txtUserName.Focus();
                    }
                }
            }
        }
        protected void LoadInfoUser()
        {

            txtUserName.Text = mUser.Name;

            txtPass.Text = mUser.Password;
            txtConfirm.Text = mUser.Password;
            txtDescription.Text = mUser.Description;

            txtFailedPass.Text = mUser.FailedPasswordAttemptCount.ToString();
            txtLastLogion.Text = mUser.LastLoginDate.ToString(mFormatDate) + " " + mUser.LastLoginDate.ToLongTimeString();
            chkIsLockOut.Checked = !mUser.Activate;
            PickerExpireDate.Value = mUser.ExpireDate;

            coEmployees.SelectedValue = mUser.EmployeeKey;
            mUserNameChanged = false;
            // LoadData();
        }

        #endregion

        #region [ Update User Infomation]
        private bool CheckBeforeSave()
        {

            if (txtUserName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please input UserName");
                txtUserName.Focus();
                return false;
            }
            if (txtPass.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please input password");
                txtPass.Focus();
                return false;
            }
            if (txtPass.Text != txtConfirm.Text)
            {
                MessageBox.Show("not the same pass, input again");
                txtPass.Focus();
                return false;
            }
            return true;

        }
        private bool SaveUser_Infomation()
        {
            if (CheckBeforeSave())
            {
                string strResult = "0";
                mUserName = txtUserName.Text.Trim();
                mUser.Name = txtUserName.Text;

                mUser.Description = txtDescription.Text;
                if (mUser.Password != txtPass.Text.Trim())
                {
                    mUser.Password = txtPass.Text.Trim();
                }
                mUser.ExpireDate = PickerExpireDate.Value;
                mUser.Activate = !chkIsLockOut.Checked;
                if (coEmployees.SelectedValue == null)
                    mUser.EmployeeKey = 0;
                else
                    mUser.EmployeeKey = int.Parse(coEmployees.SelectedValue.ToString());
                mUser.CreatedBy = SessionUser.UserLogin.Key;
                mUser.ModifiedBy = SessionUser.UserLogin.Key;

                strResult = mUser.Save();

                // MessageBox.Show(strResult); 
                if (strResult.Contains("insert duplicate key"))
                {
                    MessageBox.Show("Duplicate UserName ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtUserName.Focus();
                    return false;
                }

                return true;
            }
            else
                return false;
        }
        #endregion

        #region [ Action Button]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (SaveUser_Infomation())
                this.Close();
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveUser_Infomation())
            {
                mUser = new User_Info();
                txtUserName.Text = "";
                txtDescription.Text = "";
                txtPass.Text = "";

                txtLastLogion.Text = "";
                txtFailedPass.Text = "";
                chkIsLockOut.Checked = false;

                txtUserName.Focus();
                //   LoadData();
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            mUser.Delete();
            this.Close();

        }
        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            txtLastLogion.Text = "true";
        }
        #endregion

   
        #region [ Short Key ]
        private void FrmChangpassUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

    
    }
}