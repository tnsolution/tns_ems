﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.Users
{
    public partial class FrmUserRole : Form
    {
        public User_Role_Info mRole = new User_Role_Info();
        public bool IsOK = false;
        public FrmUserRole()
        {
            InitializeComponent();
        }

        private void FrmUserRole_Load(object sender, EventArgs e)
        {
           // CheckRole();
            string nSQL = "SELECT * FROM SYS_Roles "
                        + " WHERE NOT EXISTS(SELECT * FROM SYS_Users_Roles "
                        + " WHERE SYS_Roles.RoleKey = SYS_Users_Roles.RoleKey AND UserKey ='" + mRole.UserKey+"')"
                        + " ORDER BY RoleID ";
            LoadDataToToolbox.ComboBoxData(coRoleList, nSQL,8, true);
            if (mRole.Key.Trim().Length > 0)
            {
                coRoleList.Visible = false;
                txtRoleName.Visible = true;
                txtRoleName.Text = mRole.Name;
                chkRead.Checked = mRole.Read;
                chkAdd.Checked = mRole.Add;
                chkEdit.Checked = mRole.Edit;
                chkDel.Checked = mRole.Del;
            }
            else
            {
                coRoleList.Visible = true;
                txtRoleName.Visible = false;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            UpdateRole();
            //for (int i = 1; i < coRoleList.Items.Count; i++)
            //{
            //    coRoleList.SelectedIndex = i;
            //    UpdateRole();
            //}
        }
        private void UpdateRole()
        {
            int nIndex = 0;
            if (coRoleList.Visible)
            {
                if (coRoleList.SelectedIndex == 0)
                    return;
             
                
                string[] strRole = coRoleList.Text.Split(':');
                if (strRole.Length > 1)
                {
                    mRole.ID = strRole[0].Trim();
                    mRole.Name = strRole[1].Trim();
                    mRole.Key = coRoleList.SelectedValue.ToString();
                }
                nIndex = coRoleList.SelectedIndex;
            }

            mRole.Read = chkRead.Checked;
            mRole.Add = chkAdd.Checked;
            mRole.Edit = chkEdit.Checked;
            mRole.Del = chkDel.Checked;
            IsOK = true;
            if (coRoleList.Visible)
                mRole.Create();
            else
                mRole.Update();
            if (!coRoleList.Visible)
                this.Close();
            else
            {
                string nSQL = "SELECT * FROM SYS_Roles "
            + " WHERE NOT EXISTS(SELECT * FROM SYS_Users_Roles "
            + " WHERE SYS_Roles.RoleKey = SYS_Users_Roles.RoleKey AND UserKey ='" + mRole.UserKey + "')"
            + " ORDER BY RoleID ";

                LoadDataToToolbox.ComboBoxData(coRoleList, nSQL, 8, true);
            }
        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS005");

            if (!nRole.Edit)
            {
                cmdSave.Enabled = false;
            }

        }
    }
}
