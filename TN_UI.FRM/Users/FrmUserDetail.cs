using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.HRM;

namespace TN_UI.FRM.Users
{
    public partial class FrmUserDetail : Form
    {
        public string mUserKey = "";
        public string mUserName;
        public User_Info mUser = new User_Info();

        string mFormatDate = GlobalSystemConfig.FormatDate;

        public FrmUserDetail()
        {
            InitializeComponent();
        }

        private void FrmUserDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            SetupLayoutGridView(dataGridView1);
            LoadDataToToolbox.ComboBoxData(coEmployees, "SELECT EmployeeKey,EmployeeID,LastName,FirstName FROM HRM_Employees ORDER BY FirstName", 15, true);
            coEmployees.SelectedValue = 0;
            mUser = new User_Info(mUserKey);
            LoadInfoUser();

        }

        #region [ Load Info User ]
        private bool mUserNameChanged = false;
        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            mUserNameChanged = true;
        }
        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length == 0) return;
            if (mUserNameChanged)
            {
                User_Info nUser = new User_Info(txtUserName.Text.Trim(), true);
                if (nUser.Key.Trim().Length > 0)
                {
                    if (MessageBox.Show(" Have this ID ! do you want view it ?", "Message", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        mUser = nUser;
                        LoadInfoUser();
                    }
                    else
                    {
                        txtUserName.Focus();
                    }
                }
            }
        }
        protected void LoadInfoUser()
        {

            txtUserName.Text = mUser.Name;

            txtPass.Text = mUser.Password;
            txtDescription.Text = mUser.Description;

            txtFailedPass.Text = mUser.FailedPasswordAttemptCount.ToString();
            txtLastLogion.Text = mUser.LastLoginDate.ToString(mFormatDate) + " " + mUser.LastLoginDate.ToLongTimeString();
            chkIsLockOut.Checked = !mUser.Activate;
            PickerExpireDate.Value = mUser.ExpireDate;

            coEmployees.SelectedValue = mUser.EmployeeKey;
            mUserNameChanged = false;
            if (mUser.Key.Length > 0)
                LoadRoleName();
            else
                DisplayNewUser();
            // LoadData();
        }

        #endregion

        #region [ Update User Infomation]
        private bool CheckBeforeSave()
        {

            if (txtUserName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please input UserName");
                txtUserName.Focus();
                return false;
            }
            if (txtPass.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please input password");
                txtPass.Focus();
                return false;
            }

            return true;

        }
        private bool SaveUser_Infomation()
        {
            if (CheckBeforeSave())
            {
                string strResult = "0";
                mUserName = txtUserName.Text.Trim();
                mUser.Name = txtUserName.Text;

                mUser.Description = txtDescription.Text;
                if (mUser.Password != txtPass.Text.Trim())
                {
                    mUser.Password = txtPass.Text.Trim();
                }
                mUser.ExpireDate = PickerExpireDate.Value;
                mUser.Activate = !chkIsLockOut.Checked;
                if (coEmployees.SelectedValue == null)
                    mUser.EmployeeKey = 0;
                else
                    mUser.EmployeeKey = int.Parse(coEmployees.SelectedValue.ToString());
                mUser.CreatedBy = SessionUser.UserLogin.Key;
                mUser.ModifiedBy = SessionUser.UserLogin.Key;

                strResult = mUser.Save();

                // MessageBox.Show(strResult); 
                if (strResult.Contains("insert duplicate key"))
                {
                    MessageBox.Show("Duplicate UserName ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtUserName.Focus();
                    return false;
                }

                return true;
            }
            else
                return false;
        }
        private void DisplayNewUser()
        {
            dataGridView1.Visible = false;
            cmdAddRole.Visible = false;
            cmdDelRole.Visible = false;
            cmdEditRole.Visible = false;
        }
        #endregion

        #region [ Action Button]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            if (SaveUser_Infomation())
                this.Close();
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            if (SaveUser_Infomation())
            {
                mUser = new User_Info();
                txtUserName.Text = "";
                txtDescription.Text = "";
                txtPass.Text = "";

                txtLastLogion.Text = "";
                txtFailedPass.Text = "";
                chkIsLockOut.Checked = false;

                dataGridView1.Rows.Clear();
                DisplayNewUser();
                txtUserName.Focus();
                //   LoadData();
            }
        }
        private void cmdDel_Click(object sender, EventArgs e)
        {
            mUser.Delete();
            this.Close();

        }
        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            txtLastLogion.Text = "true";
        }
        #endregion

        #region [Role]
        private void SetupLayoutGridView(DataGridView GV)
        {
            // Setup Column 
            DataGridViewImageColumn nColumn;

            GV.Columns.Add("RoleID", "Mã số");
            GV.Columns.Add("RoleName", "Tên phân quyền");

            nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Read";
            nColumn.HeaderText = "Xem";
            GV.Columns.Add(nColumn);

            nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Add";
            nColumn.HeaderText = "Thêm mới";
            GV.Columns.Add(nColumn);

            nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Edit";
            nColumn.HeaderText = "Sửa";
            GV.Columns.Add(nColumn);

            nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Del";
            nColumn.HeaderText = "Xóa";
            GV.Columns.Add(nColumn);

            GV.Columns[0].Width = 100;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 250;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 50;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns[2].DefaultCellStyle.NullValue = ImageListGV.Images[1];

            GV.Columns[3].Width = 50;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns[3].Visible = false;

            GV.Columns[4].Width = 50;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[5].Width = 50;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // setup style view
            GV.ScrollBars = ScrollBars.Vertical;
            GV.BackgroundColor = Color.White;

            GV.AllowUserToResizeRows = false;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GV.ReadOnly = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.Rows[0].Cells["RoleID"].Value = "Empty";
            GV.Rows[0].Cells["RoleName"].Value = "Empty";
            GV.Rows[0].Tag = "";
            GV.Rows[0].Cells["Read"].Value = ImageListGV.Images[1];
            GV.Rows[0].Cells["Add"].Value = ImageListGV.Images[1];
            GV.Rows[0].Cells["Edit"].Value = ImageListGV.Images[0];
            GV.Rows[0].Cells["Del"].Value = ImageListGV.Images[0];

        }
        private void LoadRoleName()
        {
            DataTable RoleList = Users_Data.RoleList(mUser.Key);

            dataGridView1.Rows.Clear();
            int n = RoleList.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {
                DataRow nRow = RoleList.Rows[i];
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].Cells["RoleID"].Value = nRow["RoleID"].ToString();
                dataGridView1.Rows[i].Cells["RoleName"].Value = nRow["RoleName"].ToString();
                dataGridView1.Rows[i].Tag = nRow["RoleKey"].ToString();
                dataGridView1.Rows[i].Cells["Read"].Value = ImageListGV.Images[nRow["RoleRead"].ToString()];
                dataGridView1.Rows[i].Cells["Add"].Value = ImageListGV.Images[nRow["RoleAdd"].ToString()];
                dataGridView1.Rows[i].Cells["Edit"].Value = ImageListGV.Images[nRow["RoleEdit"].ToString()];
                dataGridView1.Rows[i].Cells["Del"].Value = ImageListGV.Images[nRow["RoleDel"].ToString()];

            }
            dataGridView1.Rows[i].Cells["RoleID"].Value = "";
            dataGridView1.Rows[i].Cells["RoleName"].Value = "";
            dataGridView1.Rows[i].Tag = "";
            dataGridView1.Rows[i].Cells["Read"].Value = ImageListGV.Images[0];
            dataGridView1.Rows[i].Cells["Add"].Value = ImageListGV.Images[0];
            dataGridView1.Rows[i].Cells["Edit"].Value = ImageListGV.Images[0];
            dataGridView1.Rows[i].Cells["Del"].Value = ImageListGV.Images[0];

        }
        private void cmdAddRole_Click(object sender, EventArgs e)
        {
            AddRole();
        }

        private void dataGridView1_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["RoleName"].Value = "";
            e.Row.Tag = "";
            e.Row.Cells["Read"].Value = ImageListGV.Images["False"];
            e.Row.Cells["Add"].Value = ImageListGV.Images["False"];
            e.Row.Cells["Edit"].Value = ImageListGV.Images["False"];
            e.Row.Cells["Del"].Value = ImageListGV.Images["False"];
        }
        private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow nRow = dataGridView1.CurrentRow;
            if (nRow.Tag.ToString().Length == 0)
                AddRole();
            else
                EditRole(nRow);
        }
        private void cmdDelRole_Click(object sender, EventArgs e)
        {
            if (!mRole.Del)
                return;

            DataGridViewRow nRow = dataGridView1.CurrentRow;
            string nRoleKey = nRow.Tag.ToString();
            User_Role_Info nRole = new User_Role_Info();
            nRole.UserKey = mUser.Key;
            nRole.Key = nRoleKey;
            nRole.Delete();
            dataGridView1.Rows.Remove(nRow);
        }
        private void cmdEditRole_Click(object sender, EventArgs e)
        {
            DataGridViewRow nRow = dataGridView1.CurrentRow;
            if (nRow.Tag.ToString().Length == 0)
                AddRole();
            else
                EditRole(nRow);
        }
        private void AddRole()
        {
            if (!mRole.Edit)
                return;

            FrmUserRole frm = new FrmUserRole();
            frm.mRole.UserKey = mUser.Key;
            frm.ShowDialog();
            if (mUser.Key.Length > 0)
                LoadRoleName();
        }
        private void EditRole(DataGridViewRow RowUpdate)
        {
            if (!mRole.Edit)
                return;

            if (RowUpdate.Tag != null)
            {
                string nRoleKey = RowUpdate.Tag.ToString();
                FrmUserRole frm = new FrmUserRole();
                frm.mRole.UserKey = mUser.Key;
                if (nRoleKey.Length > 0)
                    frm.mRole = new User_Role_Info(mUser.Key, nRoleKey);
                frm.ShowDialog();

                RowUpdate.Tag = nRoleKey;
                RowUpdate.Cells["RoleID"].Value = frm.mRole.ID;
                RowUpdate.Cells["RoleName"].Value = frm.mRole.Name;
                RowUpdate.Cells["Read"].Value = ImageListGV.Images[frm.mRole.Read.ToString()];
                RowUpdate.Cells["Add"].Value = ImageListGV.Images[frm.mRole.Add.ToString()];
                RowUpdate.Cells["Edit"].Value = ImageListGV.Images[frm.mRole.Edit.ToString()];
                RowUpdate.Cells["Del"].Value = ImageListGV.Images[frm.mRole.Del.ToString()];
            }
        }
        #endregion

        #region [ Short Key ]
        private void FrmUserDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }
        #endregion

        #region [ Sercurity ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {

            mRole.Check_Role("SYS005");

            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

    }
}