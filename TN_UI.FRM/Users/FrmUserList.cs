using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.Users
{
    public partial class FrmUserList : Form
    {
        public string mUserID = "";
        public bool FlagView;
        public string mTitle = "DANH SÁCH USER";
        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        string mFormatDate = GlobalSystemConfig.FormatDate;

        public FrmUserList()
        {
            InitializeComponent();

        }

        private void FrmUserList_Load(object sender, EventArgs e)
        {

            InitListView();
            panelSearch.Height = 0;
            txtTitle.Text = mTitle;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

            this.WindowState = FormWindowState.Maximized;

            LoadDataFields(ListViewData, Users_Data.UserList());

        }

        //Layout 
        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        public ListViewItem ListItemSelect;
        private void InitListView()
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "User Name";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân viên";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Activate";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày hết hạn";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Login lần cuối";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            ListViewData.Columns.Add(colHead);

            ListViewData.SmallImageList = SmallImageLV;
            ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            LVGroup = new ListViewMyGroup(ListViewData);

        }
        private void ListViewData_Leave(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListItemSelect = ListViewData.SelectedItems[0];
                if (ListItemSelect != null)
                {
                    ListItemSelect.BackColor = Color.Silver;
                }
            }
        }
        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListItemSelect != null)
            {
                ListItemSelect.BackColor = Color.White;
            }
        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {

            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        private void ListViewData_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                ListItemSelect = e.Item;
            }
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (FlagView)
            {
                mUserID = ListItemSelect.SubItems[0].Text;
                this.Close();
            }
            else
            {
                string nUserKey = ListItemSelect.Tag.ToString();
                FrmUserDetail frm = new FrmUserDetail();
                frm.mUserKey = nUserKey;
                frm.ShowDialog();

                LoadDataFields(ListViewData, Users_Data.UserList());

                SelectIndexInListView(nUserKey);
            }
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                string nResult = "0";// UserDataAccess.CheckExitUser((int)ListViewData.SelectedItems[0].Tag);
                if (nResult == "1")
                {
                    MessageBox.Show("Không thể xóa user này, vui lòng kiểm tra lại!");
                }
                else
                {
                    if (MessageBox.Show("Bạn có muốn xóa user " + ListViewData.SelectedItems[0].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        int nIndex = ListViewData.SelectedItems[0].Index;
                        User_Info nUser = new User_Info();
                        nUser.Key = ListViewData.SelectedItems[0].Tag.ToString();
                        nUser.Delete();
                        ListViewData.SelectedItems[0].Remove();
                        if (ListViewData.Items.Count > 0)
                            if (nIndex == 0)
                                ListViewData.Items[nIndex].Selected = true;
                            else
                                ListViewData.Items[nIndex - 1].Selected = true;
                        ListViewData.Focus();
                    }
                }
            }
        }

        // Load Database
        public void LoadDataFields(ListView LV, DataTable nListUser)
        {

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = nListUser.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nListUser.Rows[i];
                lvi = new ListViewItem();
                lvi.Tag = nRow["UserKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UserName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["LastName"].ToString().Trim() + " " + nRow["FirstName"].ToString();
                lvi.SubItems.Add(lvsi);

                string nActivate = "";
                if ((bool)nRow["Activate"])
                    nActivate = "X";
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nActivate;
                lvi.SubItems.Add(lvsi);

                string nDate = "";
                if (nRow["ExpireDate"] != DBNull.Value)
                {
                    DateTime nExpireDate = (DateTime)nRow["ExpireDate"];
                    nDate = nExpireDate.ToString(mFormatDate);
                }

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nDate;
                lvi.SubItems.Add(lvsi);

                DateTime nLastLoginDate = (DateTime)nRow["LastLoginDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nLastLoginDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }


        }
        #endregion

        #region [ Activate on Button ]
        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            try
            {

                FrmUserDetail frm = new FrmUserDetail();
                frm.ShowDialog();

                LoadDataFields(ListViewData, Users_Data.UserList());

                ListViewData.Focus();

            }
            catch
            {
            }
        }
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            DataTable nListUser = new DataTable();

            nListUser = Users_Data.UserList(txtUserID.Text.Trim(), txtUserName.Text.Trim());

            LoadDataFields(ListViewData, nListUser);

        }

        #endregion

        #region [ Search ]
        private void SelectIndexInListView(string UserKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if (ListViewData.Items[i].Tag.ToString() == UserKey)
                {
                    ListViewData.Items[i].Selected = true;
                    //ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }

        #endregion

        private void cmdImport_Click(object sender, EventArgs e)
        {
            //int nCategoryKey = (int)mNodeSave.Tag;

            //FrmImportUser frm = new FrmImportUser();
            //frm.mCategoryKey = nCategoryKey;
            //frm.ShowDialog();

            //DataTable nListUser = UserDataAccess.ListUser(nCategoryKey);
            //if (ListViewData.Visible)
            //    LoadDataFields(ListViewData, nListUser);
            //else
            //    PopulateCrystalReport(nListUser);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            ListViewData.Visible = false;
            //    crystalReportViewer1.Visible = true;

        }


        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS005");

            if (nRole.Del)
                m_RoleDelete = true;
        }


    }
}