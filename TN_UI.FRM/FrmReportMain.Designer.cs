﻿namespace TN_UI.FRM
{
    partial class FrmReportMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ListViewPurchases = new System.Windows.Forms.ListView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LBTaskToday = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ListViewSale = new System.Windows.Forms.ListView();
            this.GroupFinancial = new System.Windows.Forms.GroupBox();
            this.ChartReceipt = new System.Windows.Forms.Panel();
            this.txtReceipt = new System.Windows.Forms.Label();
            this.ChartPayment = new System.Windows.Forms.Panel();
            this.txtPayment = new System.Windows.Forms.Label();
            this.ChartInvoiceSale = new System.Windows.Forms.Panel();
            this.txtInvoiceSale = new System.Windows.Forms.Label();
            this.ChartInvoicePurchar = new System.Windows.Forms.Panel();
            this.txtInvoicePurcharse = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.GroupFinancial.SuspendLayout();
            this.ChartReceipt.SuspendLayout();
            this.ChartPayment.SuspendLayout();
            this.ChartInvoiceSale.SuspendLayout();
            this.ChartInvoicePurchar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ListViewPurchases);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(557, 187);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hóa đơn phải thu hôm nay";
            // 
            // ListViewPurchases
            // 
            this.ListViewPurchases.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListViewPurchases.Font = new System.Drawing.Font("Arial", 8.25F);
            this.ListViewPurchases.FullRowSelect = true;
            this.ListViewPurchases.GridLines = true;
            this.ListViewPurchases.Location = new System.Drawing.Point(6, 23);
            this.ListViewPurchases.Name = "ListViewPurchases";
            this.ListViewPurchases.Size = new System.Drawing.Size(542, 150);
            this.ListViewPurchases.TabIndex = 0;
            this.ListViewPurchases.UseCompatibleStateImageBehavior = false;
            this.ListViewPurchases.View = System.Windows.Forms.View.Details;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LBTaskToday);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(575, 161);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(284, 241);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Công việc hôm nay";
            // 
            // LBTaskToday
            // 
            this.LBTaskToday.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LBTaskToday.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBTaskToday.ForeColor = System.Drawing.Color.Navy;
            this.LBTaskToday.FormattingEnabled = true;
            this.LBTaskToday.ItemHeight = 14;
            this.LBTaskToday.Location = new System.Drawing.Point(18, 19);
            this.LBTaskToday.Name = "LBTaskToday";
            this.LBTaskToday.Size = new System.Drawing.Size(259, 210);
            this.LBTaskToday.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ListViewSale);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Navy;
            this.groupBox3.Location = new System.Drawing.Point(12, 205);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(557, 197);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hóa đơn phải trả hôm nay";
            // 
            // ListViewSale
            // 
            this.ListViewSale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListViewSale.Font = new System.Drawing.Font("Arial", 8.25F);
            this.ListViewSale.FullRowSelect = true;
            this.ListViewSale.GridLines = true;
            this.ListViewSale.Location = new System.Drawing.Point(6, 23);
            this.ListViewSale.Name = "ListViewSale";
            this.ListViewSale.Size = new System.Drawing.Size(542, 168);
            this.ListViewSale.TabIndex = 0;
            this.ListViewSale.UseCompatibleStateImageBehavior = false;
            this.ListViewSale.View = System.Windows.Forms.View.Details;
            // 
            // GroupFinancial
            // 
            this.GroupFinancial.Controls.Add(this.ChartReceipt);
            this.GroupFinancial.Controls.Add(this.ChartPayment);
            this.GroupFinancial.Controls.Add(this.ChartInvoiceSale);
            this.GroupFinancial.Controls.Add(this.ChartInvoicePurchar);
            this.GroupFinancial.Controls.Add(this.label5);
            this.GroupFinancial.Controls.Add(this.label6);
            this.GroupFinancial.Controls.Add(this.label7);
            this.GroupFinancial.Controls.Add(this.label8);
            this.GroupFinancial.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupFinancial.ForeColor = System.Drawing.Color.Navy;
            this.GroupFinancial.Location = new System.Drawing.Point(575, 12);
            this.GroupFinancial.Name = "GroupFinancial";
            this.GroupFinancial.Size = new System.Drawing.Size(284, 143);
            this.GroupFinancial.TabIndex = 4;
            this.GroupFinancial.TabStop = false;
            this.GroupFinancial.Text = "Thông tin tài chính tháng ";
            // 
            // ChartReceipt
            // 
            this.ChartReceipt.BackColor = System.Drawing.Color.Teal;
            this.ChartReceipt.Controls.Add(this.txtReceipt);
            this.ChartReceipt.Location = new System.Drawing.Point(107, 105);
            this.ChartReceipt.Name = "ChartReceipt";
            this.ChartReceipt.Size = new System.Drawing.Size(170, 21);
            this.ChartReceipt.TabIndex = 7;
            // 
            // txtReceipt
            // 
            this.txtReceipt.AutoSize = true;
            this.txtReceipt.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceipt.ForeColor = System.Drawing.Color.White;
            this.txtReceipt.Location = new System.Drawing.Point(6, 4);
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Size = new System.Drawing.Size(13, 14);
            this.txtReceipt.TabIndex = 1;
            this.txtReceipt.Text = "0";
            // 
            // ChartPayment
            // 
            this.ChartPayment.BackColor = System.Drawing.Color.Teal;
            this.ChartPayment.Controls.Add(this.txtPayment);
            this.ChartPayment.Location = new System.Drawing.Point(107, 78);
            this.ChartPayment.Name = "ChartPayment";
            this.ChartPayment.Size = new System.Drawing.Size(170, 21);
            this.ChartPayment.TabIndex = 6;
            // 
            // txtPayment
            // 
            this.txtPayment.AutoSize = true;
            this.txtPayment.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayment.ForeColor = System.Drawing.Color.White;
            this.txtPayment.Location = new System.Drawing.Point(6, 3);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(13, 14);
            this.txtPayment.TabIndex = 1;
            this.txtPayment.Text = "0";
            // 
            // ChartInvoiceSale
            // 
            this.ChartInvoiceSale.BackColor = System.Drawing.Color.Teal;
            this.ChartInvoiceSale.Controls.Add(this.txtInvoiceSale);
            this.ChartInvoiceSale.Location = new System.Drawing.Point(107, 51);
            this.ChartInvoiceSale.Name = "ChartInvoiceSale";
            this.ChartInvoiceSale.Size = new System.Drawing.Size(170, 21);
            this.ChartInvoiceSale.TabIndex = 5;
            // 
            // txtInvoiceSale
            // 
            this.txtInvoiceSale.AutoSize = true;
            this.txtInvoiceSale.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceSale.ForeColor = System.Drawing.Color.White;
            this.txtInvoiceSale.Location = new System.Drawing.Point(6, 4);
            this.txtInvoiceSale.Name = "txtInvoiceSale";
            this.txtInvoiceSale.Size = new System.Drawing.Size(13, 14);
            this.txtInvoiceSale.TabIndex = 1;
            this.txtInvoiceSale.Text = "0";
            // 
            // ChartInvoicePurchar
            // 
            this.ChartInvoicePurchar.BackColor = System.Drawing.Color.Teal;
            this.ChartInvoicePurchar.Controls.Add(this.txtInvoicePurcharse);
            this.ChartInvoicePurchar.Location = new System.Drawing.Point(107, 23);
            this.ChartInvoicePurchar.Name = "ChartInvoicePurchar";
            this.ChartInvoicePurchar.Size = new System.Drawing.Size(170, 21);
            this.ChartInvoicePurchar.TabIndex = 4;
            // 
            // txtInvoicePurcharse
            // 
            this.txtInvoicePurcharse.AutoSize = true;
            this.txtInvoicePurcharse.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoicePurcharse.ForeColor = System.Drawing.Color.White;
            this.txtInvoicePurcharse.Location = new System.Drawing.Point(6, 3);
            this.txtInvoicePurcharse.Name = "txtInvoicePurcharse";
            this.txtInvoicePurcharse.Size = new System.Drawing.Size(13, 14);
            this.txtInvoicePurcharse.TabIndex = 0;
            this.txtInvoicePurcharse.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 3;
            this.label5.Text = "Tồng phiếu chi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 14);
            this.label6.TabIndex = 2;
            this.label6.Text = "Tồng phiếu thu";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 14);
            this.label7.TabIndex = 1;
            this.label7.Text = "Hóa đơn bán ra";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Hóa đơn mua vào";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(12, 408);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(847, 64);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bản quyền";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bản quyền thuộc về công ty Công Nghệ Hồng Hoàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Phiên bản dùng thử";
            // 
            // FrmReportMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(871, 575);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.GroupFinancial);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Name = "FrmReportMain";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.FrmReportMain_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.GroupFinancial.ResumeLayout(false);
            this.GroupFinancial.PerformLayout();
            this.ChartReceipt.ResumeLayout(false);
            this.ChartReceipt.PerformLayout();
            this.ChartPayment.ResumeLayout(false);
            this.ChartPayment.PerformLayout();
            this.ChartInvoiceSale.ResumeLayout(false);
            this.ChartInvoiceSale.PerformLayout();
            this.ChartInvoicePurchar.ResumeLayout(false);
            this.ChartInvoicePurchar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox LBTaskToday;
        private System.Windows.Forms.ListView ListViewPurchases;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView ListViewSale;
        private System.Windows.Forms.GroupBox GroupFinancial;
        private System.Windows.Forms.Panel ChartReceipt;
        private System.Windows.Forms.Panel ChartPayment;
        private System.Windows.Forms.Panel ChartInvoiceSale;
        private System.Windows.Forms.Panel ChartInvoicePurchar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label txtReceipt;
        private System.Windows.Forms.Label txtPayment;
        private System.Windows.Forms.Label txtInvoiceSale;
        private System.Windows.Forms.Label txtInvoicePurcharse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}