﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TNLibrary.SYS.Users;

namespace TN_UI.FRM
{
    public class SessionUser
    {
        private static User_Info m_UserLogin = new User_Info();
        public SessionUser()
        {

        }
        public SessionUser(string UserKey)
        {
            m_UserLogin = new User_Info(UserKey);
        }

        public static User_Info UserLogin
        {
            get
            {
                return m_UserLogin;
            }

        }

        //Role
        public static bool IsInRole(string Role)
        {
            return true;
        }
    }
}
