﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.FNC;

namespace TN_UI.FRM.Systems
{
    public partial class FrmFinancialYear : Form
    {
        string mFormatDate = GlobalSystemConfig.FormatDate;
        CultureInfo mProvider = CultureInfo.InvariantCulture;
        public FrmFinancialYear()
        {
            InitializeComponent();
        }

        private void FrmFinancialYear_Load(object sender, EventArgs e)
        {
            CheckRole();
            SetupLayoutGridViewInvoice(dataGridView1);
            DataTable nTable = FinancialYear_Data.Years();
            LoadDataToGridView(nTable);
        }
        public void SetupLayoutGridViewInvoice(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("FinancialYear", "Năm Tài chính");
            GV.Columns.Add("FromDate", "Từ Ngày");
            GV.Columns.Add("ToDate", "Đến Ngày");
            DataGridViewCheckBoxColumn nColumn = new DataGridViewCheckBoxColumn(false);
            nColumn.Name = "Activate";
            GV.Columns.Add(nColumn);

            GV.Columns[0].Width = 50;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[0].ReadOnly = true;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[1].ReadOnly = true;

            GV.Columns[2].Width = 100;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 100;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 50;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // setup style view

            GV.BackgroundColor = Color.White;
            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.AllowUserToResizeRows = false;
            GV.AllowUserToResizeColumns = true;

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;

            //// setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

        }
        private void LoadDataToGridView(DataTable FinancialYears)
        {
            int i = 0;
            foreach (DataRow nRow in FinancialYears.Rows)
            {
                dataGridView1.Rows.Add();
                DataGridViewRow nRowView = dataGridView1.Rows[i];
                DateTime nFromDate = (DateTime)nRow["FromDate"];
                DateTime nToDate = (DateTime)nRow["ToDate"];

                nRowView.Cells["No"].Value = nRow["FinancialYearKey"].ToString();
                nRowView.Cells["FinancialYear"].Value = nRow["FinancialYearName"].ToString();
                nRowView.Cells["FromDate"].Value = nFromDate.ToString(mFormatDate);
                nRowView.Cells["FromDate"].Tag = nFromDate;
                nRowView.Cells["ToDate"].Value = nToDate.ToString(mFormatDate);
                nRowView.Cells["ToDate"].Tag = nToDate;
                nRowView.Cells["Activate"].Value = (bool)nRow["Activate"];

                i++;
            }
        }

        private void dataGridView1_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 2 :
                    string strFromDate = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    DateTime nFromDate;
                    if (!DateTime.TryParseExact(strFromDate, mFormatDate, mProvider, DateTimeStyles.None, out nFromDate))
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Error";
                    }
                    else
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag = nFromDate;
                    }
                    break;
                case 3:
                    string strToDate = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    DateTime nToDate;
                    if (!DateTime.TryParseExact(strToDate, mFormatDate, mProvider, DateTimeStyles.None, out nToDate))
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Error";
                    }
                    else
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText="";
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag = nToDate;
                    }

                    break;
                case 4:
                    DataGridViewCheckBoxCell nCell = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    bool nValueCell = (bool)nCell.Value;
                    if (nValueCell)
                    {
                        int n = dataGridView1.Rows.Count;
                        for (int i = 0; i < n; i++)
                        {
                            if (i != e.RowIndex)
                                dataGridView1.Rows[i].Cells[e.ColumnIndex].Value = false;
                        }
                    }
                    break;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++) 
            {
                
                FinancialYear_Info nYear = new FinancialYear_Info();
                nYear.FinancialYearKey = int.Parse(dataGridView1.Rows[i].Cells[0].Value.ToString());
                nYear.FinancialYearName = int.Parse(dataGridView1.Rows[i].Cells[1].Value.ToString());
                nYear.FromDate = (DateTime)dataGridView1.Rows[i].Cells[2].Tag;
                nYear.ToDate = (DateTime)dataGridView1.Rows[i].Cells[3].Tag;
                nYear.Activate = (bool)dataGridView1.Rows[i].Cells[4].Value;
                nYear.Update();
            }
        }

        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS002");
            if (!nRole.Edit)
                cmdSave.Enabled = false;
        }
    }
}
