using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Threading;

using TNLibrary.SYS;
using TNConfig;

namespace TN_UI.FRM.Systems
{
    public partial class FrmConnectServer : Form
    {
        public ConnectDataBaseInfo mConnectDataBaseInfo = new ConnectDataBaseInfo();
        string strMess01 = "Kết nối thành công";
        string strMess02 = "Không thể kết nối, vui lòng chọn kiểu kết nối khác ";
        string strMess03 = "Connecting...";
        string strMess04 = "Bạn phải chọn File Data ";
        public FrmConnectServer()
        {
            InitializeComponent();
        }
        private void FrmConnectServer_Load(object sender, EventArgs e)
        {
            DisplayLanguage();

            RWConfig nFileConfig = new RWConfig();
            nFileConfig.ReadConfig();
            mConnectDataBaseInfo = nFileConfig.ConnectInfo;
            if (!mConnectDataBaseInfo.IsConnectLocal)
            {
                radioConnectServer.Checked = true;
                coSQLServer.Text = mConnectDataBaseInfo.DataSource;
                txtDataBase.Text = mConnectDataBaseInfo.DataBase;
                txtUserName.Text = mConnectDataBaseInfo.UserName;
                txtPass.Text = mConnectDataBaseInfo.Password;
            }
            else
            {
                radioOpenFile.Checked = true;
                coSQLServer.Text = mConnectDataBaseInfo.DataSource;
                txtFileName.Text = mConnectDataBaseInfo.AttachDbFilename;
            }
        }

        private void cmdConnect_Click(object sender, EventArgs e)
        {
            Connect();
        }
        private void Connect()
        {
            this.Cursor = Cursors.WaitCursor;
            lblMessage.Text = strMess03;

            if (CheckConnect())
            {

                this.DialogResult = DialogResult.Yes;
                this.Close();

            }
            else
            {
                lblMessage.Text = strMess02;
            }
            this.Cursor = Cursors.Default;
        }
        private bool CheckConnect()
        {

            mConnectDataBaseInfo = new ConnectDataBaseInfo();

            bool isConnected = false;

            if (radioConnectServer.Checked)
            {
                mConnectDataBaseInfo.DataSource = coSQLServer.Text;
                mConnectDataBaseInfo.DataBase = txtDataBase.Text;
                mConnectDataBaseInfo.UserName = txtUserName.Text;
                mConnectDataBaseInfo.Password = txtPass.Text;
                mConnectDataBaseInfo.IsConnectLocal = false;
                mConnectDataBaseInfo.AttachDbFilename = "Nofile";

                mConnectDataBaseInfo.ConnectionString = "Data Source = " + mConnectDataBaseInfo.DataSource + ";DataBase= " + mConnectDataBaseInfo.DataBase + ";User=" + mConnectDataBaseInfo.UserName + ";Password= " + mConnectDataBaseInfo.Password;// +";Integrated Security=SSPI";

            }
            if (radioOpenFile.Checked)
            {
                if (txtFileName.Text.Trim().Length == 0)
                {
                    MessageBox.Show(strMess04);
                    return false;
                }
                else
                {
                    mConnectDataBaseInfo.DataSource = ".\\SQLEXPRESS";
                    mConnectDataBaseInfo.AttachDbFilename = txtFileName.Text;
                    mConnectDataBaseInfo.IsConnectLocal = true;
                    mConnectDataBaseInfo.ConnectionString = "Data Source = " + mConnectDataBaseInfo.DataSource + ";AttachDbFilename=" + mConnectDataBaseInfo.AttachDbFilename + ";User Instance=true;Integrated Security=SSPI";

                }
            }
            SqlConnection nConSQL = new SqlConnection();
            try
            {


                nConSQL.ConnectionString = mConnectDataBaseInfo.ConnectionString;
                nConSQL.Open();

                if (nConSQL.State == ConnectionState.Open)
                {
                    isConnected = true;
                    MessageBox.Show(strMess01);
                    RWConfig nFileConfig = new RWConfig();
                    nFileConfig.ConnectInfo = mConnectDataBaseInfo;
                    nFileConfig.SaveConfig();
                }
            }
            catch (System.Exception err)
            {
                //   lbStatueConnect.Text = "Error Connect DataBase : " + err.Message;
                MessageBox.Show(err.Message);
                isConnected = false;
            }
            finally
            {
                nConSQL.Close();
            }
            return isConnected;
        }

        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            if (radioOpenFile.Checked)
            {
                openFileDialog1.InitialDirectory = "D:/";
                openFileDialog1.Filter = "|Slq|*.MDF|";
                openFileDialog1.FilterIndex = 3;
                openFileDialog1.InitialDirectory = txtFileName.Text;
                openFileDialog1.FileName = txtFileName.Text;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtFileName.Text = openFileDialog1.FileName;
                }
            }
        }

        #region Change language
        private void DisplayLanguage()
        {
            strMess01 = DisplayLang.frm_ConnectServer("Mess01");
        }
        #endregion
    }
}