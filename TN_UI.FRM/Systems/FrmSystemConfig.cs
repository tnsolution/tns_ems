using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.FNC;

namespace TN_UI.FRM.Systems
{
    public partial class FrmSystemConfig : Form
    {
        SystemConfig_Info mConfig = new SystemConfig_Info();
        
        public FrmSystemConfig()
        {
            InitializeComponent();
        }

        private void FrmSystemConfig_Load(object sender, EventArgs e)
        {
            CheckRole();
            LoadDataToToolbox.ComboBoxData(coCurrencyMain, "SELECT CurrencyID,CurrencyID, CurrencyName FROM SYS_Currency WHERE Activate = 1",false);
            LoadDataToToolbox.ComboBoxData(coFinancialYear, "SELECT FinancialYearKey,FinancialYearName FROM FNC_FinancialYears ORDER BY FinancialYearKey",false);
            coLanguage.SelectedIndex = 0;

            LoadInfo();
            LoadFinancialYearActivate();
        }
        private void LoadInfo()
        {
            // tien theo doi
            coCurrencyMain.SelectedValue  = mConfig.CurrencyMain ;
            // dinh dang tien te
            if (mConfig.FormatProviderCurrency == "es-ES") //es-ES dau cham ,"es-US" dau phay ,0 dau phay, 1 dau cham
                coDigitGroupingCurrency.SelectedIndex = 1;
            else
                coDigitGroupingCurrency.SelectedIndex = 0;

            string[] nList = mConfig.FormatCurrencyMain.Split('.');
            if (nList.Length > 1)
                coDigitsAfterCurrencyMain.Text = nList[1].Length.ToString();
            else
                coDigitsAfterCurrencyMain.Text = "0";

            nList = mConfig.FormatCurrencyForeign.Split('.');
            if (nList.Length > 1)
                coDigitsAfterCurrencyForeign.Text = nList[1].Length.ToString();
            else
                coDigitsAfterCurrencyForeign.Text = "0";
            
            // dinh dang so
            if (mConfig.FormatProviderDecimal == "es-ES") // es-ES dau cham ,"es-US" dau phay ,0 dau phay, 1 dau cham
                coDigitGroupDecimal.SelectedIndex = 1;
            else
                coDigitGroupDecimal.SelectedIndex = 0;

            nList = mConfig.FormatDecimal.Split('.');
            if (nList.Length > 1)
                coDigitsAfterDecimal.Text = nList[1].Length.ToString();
            else
                coDigitsAfterDecimal.Text = "0";

            // dinh dang thoi gian
            coFormatDate.Text = mConfig.FormatDate;

            // phuong phap tinh hang ton kho
            if (mConfig.MethorInventory == "FIFO")
                radioFIFO.Checked = true;
            else
                if (mConfig.MethorInventory == "LIFO")
                    radioLIFO.Checked = true;
                else
                    if (mConfig.MethorInventory == "AVER1")
                        radioAVER1.Checked = true;
                    else
                        radioAVER2.Checked = true;

            txtAccountCustomer.Text = mConfig.AccountCustomer;
            txtAccountProduct.Text = mConfig.AccountProduct;
            txtAccountVendor.Text = mConfig.AccountVendor;

            txtVATOutput.Text = mConfig.VATOutput;
            txtVATDeduction.Text = mConfig.VATDeduction;

            txtCashOnHand.Text = mConfig.CashOnHand;
            txtCashOnBank.Text = mConfig.CashOnBank;

            if (mConfig.Language == "VN")
                coLanguage.SelectedIndex = 1;
            if (mConfig.Language == "EN")
                coLanguage.SelectedIndex = 0;
        }

        private void cmdSaveItem_Click(object sender, EventArgs e)
        {
            // up date dinh dang so
            if (coDigitGroupingCurrency.SelectedIndex == 0)
                mConfig.FormatProviderCurrency = "en-US"; // dau phay
            else
                mConfig.FormatProviderCurrency = "es-ES"; //dau cham

            int nLength = int.Parse(coDigitsAfterCurrencyMain.Text);
            if (nLength == 0)
                mConfig.FormatCurrencyMain = "#,###,##0";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                mConfig.FormatCurrencyMain = "#,###,##0." + nDigitsAfter;
            }

            nLength = int.Parse(coDigitsAfterCurrencyForeign.Text);
            if (nLength == 0)
                mConfig.FormatCurrencyForeign = "#,###,##0";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                mConfig.FormatCurrencyForeign = "#,###,##0." + nDigitsAfter;
            }

            // up date dinh dang so
            if (coDigitGroupDecimal.SelectedIndex == 0)
                mConfig.FormatProviderDecimal = "en-US"; // dau phay
            else
                mConfig.FormatProviderDecimal = "es-ES"; //dau cham

            nLength = int.Parse(coDigitsAfterDecimal.Text);
            if (nLength == 0)
                mConfig.FormatDecimal = "#,###,##0";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                mConfig.FormatDecimal = "#,###,##0." + nDigitsAfter;
            }
            // luu dinh dang thoi gian
            mConfig.FormatDate = coFormatDate.Text;
            // tien te theo doi
            mConfig.CurrencyMain = coCurrencyMain.SelectedValue.ToString();

            // phuong phap tinh hang ton kho
            if (radioFIFO.Checked == true)
                mConfig.MethorInventory = "FIFO";
            else
                if (radioLIFO.Checked == true)
                    mConfig.MethorInventory = "LIFO";
                else
                    if (radioAVER1.Checked == true)
                        mConfig.MethorInventory = "AVER1";
                    else
                        if (radioAVER2.Checked == true)
                            mConfig.MethorInventory = "AVER2";
                        else
                            mConfig.MethorInventory = "REAL";


            mConfig.AccountProduct = txtAccountProduct.Text.Trim();
            mConfig.AccountVendor = txtAccountVendor.Text.Trim();
            mConfig.AccountCustomer = txtAccountCustomer.Text.Trim();
            mConfig.VATOutput = txtVATOutput.Text;
            mConfig.VATDeduction = txtVATDeduction.Text;
            mConfig.CashOnHand = txtCashOnHand.Text;
            mConfig.CashOnBank = txtCashOnBank.Text;
            mConfig.Language = coLanguage.Text.Substring(coLanguage.Text.Trim().Length - 2, 2);
            mConfig.UpdateAll();

            FinancialYear_Info nFinancial =  new FinancialYear_Info();
            nFinancial.FinancialYearKey = (int)coFinancialYear.SelectedValue;
            nFinancial.SetActivate(true);

            // GlobalSystemConfigInfo.Reload();
            MessageBox.Show("Đã lưu cấu hình hệ thống");
          //  GlobalSystemConfig nGlobalConfig = new GlobalSystemConfig();
          //  nGlobalConfig.LoadGlobalSystemConfig();
            this.Close();
        }

       
        #region [ Financial Year]
        private void LoadFinancialYear(int FinancialYearKey)
        {
            FinancialYear_Info nFinancialYear = new FinancialYear_Info(FinancialYearKey);
            txtFromDate.Text = nFinancialYear.FromDate.ToString(mConfig.FormatDate);
            txtToDate.Text = nFinancialYear.ToDate.ToString(mConfig.FormatDate);
        }
        private void coFinancialYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int FinancialYearKey = (int)coFinancialYear.SelectedValue;
            LoadFinancialYear(FinancialYearKey);
        }
        private void LoadFinancialYearActivate()
        {
            FinancialYear_Info nFinancialYear = new FinancialYear_Info(true);
            txtFromDate.Text = nFinancialYear.FromDate.ToString(mConfig.FormatDate);
            txtToDate.Text = nFinancialYear.ToDate.ToString(mConfig.FormatDate);
           coFinancialYear.SelectedValue = nFinancialYear.FinancialYearKey;
        }
        #endregion

        #region [ Dinh dang tien te]
        private void coDigitGroupingCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewFormatCurrency();
        }

        private void coDigitsAfterCurrencyMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewFormatCurrency();
        }

        private void coDigitsAfterCurrencyForeign_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewFormatCurrency();
        }

        private void ViewFormatCurrency()
        {
            if (coDigitsAfterCurrencyMain.Text.Length == 0) return;
            if (coDigitsAfterCurrencyForeign.Text.Length == 0) return;

            string nFormatCurrencyMain;
            string nFormatProviderCurrency;
            string nFormatCurrencyForeign;
            if (coDigitGroupingCurrency.SelectedIndex == 0)
                nFormatProviderCurrency = "en-US"; // dau phay
            else
                nFormatProviderCurrency = "es-ES"; //dau cham

            int nLength = int.Parse(coDigitsAfterCurrencyMain.Text);
            if (nLength == 0)
                nFormatCurrencyMain = "#,###,###";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                nFormatCurrencyMain = "#,###,###." + nDigitsAfter;
            }

            nLength = int.Parse(coDigitsAfterCurrencyForeign.Text);
            if (nLength == 0)
                nFormatCurrencyForeign = "#,###,###";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                nFormatCurrencyForeign = "#,###,###." + nDigitsAfter;
            }
            double nAmountTest = 1234567;
            lbFromatCurrencyMain.Text = nAmountTest.ToString(nFormatCurrencyMain, new CultureInfo(nFormatProviderCurrency, true));
            lbFormatCurrencyForeign.Text = nAmountTest.ToString(nFormatCurrencyForeign, new CultureInfo(nFormatProviderCurrency, true));

        }
        #endregion

        #region [ Dinh dang so]
        private void coDigitGroupDecimal_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewFormatDecimal();
        }

        private void coDigitsAfterDecimal_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewFormatDecimal();
        }
        private void ViewFormatDecimal()
        {
            if (coDigitsAfterDecimal.Text.Length == 0) return;

            string nFormatDecimal;
            string nFormatProviderDecimal;

            if (coDigitGroupDecimal.SelectedIndex == 0)
                nFormatProviderDecimal = "en-US"; // dau phay
            else
                nFormatProviderDecimal = "es-ES"; //dau cham

            int nLength = int.Parse(coDigitsAfterDecimal.Text);
            if (nLength == 0)
                nFormatDecimal = "#,###,###";
            else
            {
                string nDigitsAfter = "";
                for (int i = 1; i <= nLength; i++)
                {
                    nDigitsAfter += "0";
                }
                nFormatDecimal = "#,###,###." + nDigitsAfter;
            }

            double nAmountTest = 1234567;
            lbFormatDecimal.Text = nAmountTest.ToString(nFormatDecimal, new CultureInfo(nFormatProviderDecimal, true));
        }
        #endregion

        #region [ Dinh dang ngay ]
        private void coFormatDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(coFormatDate.Text.Length >0)
                lbFormatDate.Text = DateTime.Now.ToString(coFormatDate.Text);
        }
        #endregion

        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS004");
            if (!nRole.Edit)
                cmdSaveItem.Enabled = false;
        }
    }
}