namespace TN_UI.FRM.Systems
{
    partial class FrmSystemConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtToDate = new System.Windows.Forms.TextBox();
            this.txtFromDate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.coFinancialYear = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.coCurrencyMain = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdSaveItem = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioAVER2 = new System.Windows.Forms.RadioButton();
            this.radioLIFO = new System.Windows.Forms.RadioButton();
            this.radioFIFO = new System.Windows.Forms.RadioButton();
            this.radioREAL = new System.Windows.Forms.RadioButton();
            this.radioAVER1 = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbFromatCurrencyMain = new System.Windows.Forms.Label();
            this.lbFormatCurrencyForeign = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.coDigitsAfterCurrencyForeign = new System.Windows.Forms.ComboBox();
            this.coDigitsAfterCurrencyMain = new System.Windows.Forms.ComboBox();
            this.coDigitGroupingCurrency = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label15 = new System.Windows.Forms.Label();
            this.lbFormatDecimal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.coDigitsAfterDecimal = new System.Windows.Forms.ComboBox();
            this.coDigitGroupDecimal = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbFormatDate = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.coFormatDate = new System.Windows.Forms.ComboBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCashOnBank = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCashOnHand = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtVATDeduction = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtVATOutput = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccountCustomer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAccountVendor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAccountProduct = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.coLanguage = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.txtToDate);
            this.groupBox1.Controls.Add(this.txtFromDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.coFinancialYear);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.coCurrencyMain);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox1.Location = new System.Drawing.Point(10, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 138);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tài chính";
            // 
            // txtToDate
            // 
            this.txtToDate.Font = new System.Drawing.Font("Arial", 9F);
            this.txtToDate.ForeColor = System.Drawing.Color.Navy;
            this.txtToDate.Location = new System.Drawing.Point(123, 71);
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.ReadOnly = true;
            this.txtToDate.Size = new System.Drawing.Size(118, 21);
            this.txtToDate.TabIndex = 12;
            this.txtToDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtFromDate
            // 
            this.txtFromDate.Font = new System.Drawing.Font("Arial", 9F);
            this.txtFromDate.ForeColor = System.Drawing.Color.Navy;
            this.txtFromDate.Location = new System.Drawing.Point(123, 44);
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.ReadOnly = true;
            this.txtFromDate.Size = new System.Drawing.Size(118, 21);
            this.txtFromDate.TabIndex = 11;
            this.txtFromDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(13, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ngày kết thúc";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(11, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ngày bắt đầu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(11, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Năm Tài Chính";
            // 
            // coFinancialYear
            // 
            this.coFinancialYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coFinancialYear.Font = new System.Drawing.Font("Arial", 9F);
            this.coFinancialYear.ForeColor = System.Drawing.Color.Navy;
            this.coFinancialYear.FormattingEnabled = true;
            this.coFinancialYear.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.coFinancialYear.Location = new System.Drawing.Point(123, 15);
            this.coFinancialYear.Name = "coFinancialYear";
            this.coFinancialYear.Size = new System.Drawing.Size(118, 23);
            this.coFinancialYear.TabIndex = 5;
            this.coFinancialYear.SelectionChangeCommitted += new System.EventHandler(this.coFinancialYear_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(13, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Tiền hạch toán";
            // 
            // coCurrencyMain
            // 
            this.coCurrencyMain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coCurrencyMain.Font = new System.Drawing.Font("Arial", 9F);
            this.coCurrencyMain.ForeColor = System.Drawing.Color.Navy;
            this.coCurrencyMain.FormattingEnabled = true;
            this.coCurrencyMain.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.coCurrencyMain.Location = new System.Drawing.Point(123, 98);
            this.coCurrencyMain.Name = "coCurrencyMain";
            this.coCurrencyMain.Size = new System.Drawing.Size(118, 23);
            this.coCurrencyMain.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.cmdSaveItem);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 370);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 36);
            this.panel1.TabIndex = 7;
            // 
            // cmdSaveItem
            // 
            this.cmdSaveItem.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSaveItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.cmdSaveItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSaveItem.Location = new System.Drawing.Point(216, 6);
            this.cmdSaveItem.Name = "cmdSaveItem";
            this.cmdSaveItem.Size = new System.Drawing.Size(100, 24);
            this.cmdSaveItem.TabIndex = 7;
            this.cmdSaveItem.Text = "Lưu";
            this.cmdSaveItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSaveItem.UseVisualStyleBackColor = true;
            this.cmdSaveItem.Click += new System.EventHandler(this.cmdSaveItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.radioAVER2);
            this.groupBox3.Controls.Add(this.radioLIFO);
            this.groupBox3.Controls.Add(this.radioFIFO);
            this.groupBox3.Controls.Add(this.radioREAL);
            this.groupBox3.Controls.Add(this.radioAVER1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox3.Location = new System.Drawing.Point(293, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 138);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Giá vốn hàng bán";
            // 
            // radioAVER2
            // 
            this.radioAVER2.AutoSize = true;
            this.radioAVER2.Checked = true;
            this.radioAVER2.Font = new System.Drawing.Font("Arial", 9F);
            this.radioAVER2.ForeColor = System.Drawing.Color.Navy;
            this.radioAVER2.Location = new System.Drawing.Point(16, 37);
            this.radioAVER2.Name = "radioAVER2";
            this.radioAVER2.Size = new System.Drawing.Size(143, 19);
            this.radioAVER2.TabIndex = 4;
            this.radioAVER2.TabStop = true;
            this.radioAVER2.Tag = "1";
            this.radioAVER2.Text = "Giá bình quân định kỳ";
            this.radioAVER2.UseVisualStyleBackColor = true;
            // 
            // radioLIFO
            // 
            this.radioLIFO.AutoSize = true;
            this.radioLIFO.Enabled = false;
            this.radioLIFO.Font = new System.Drawing.Font("Arial", 9F);
            this.radioLIFO.ForeColor = System.Drawing.Color.Navy;
            this.radioLIFO.Location = new System.Drawing.Point(16, 112);
            this.radioLIFO.Name = "radioLIFO";
            this.radioLIFO.Size = new System.Drawing.Size(51, 19);
            this.radioLIFO.TabIndex = 3;
            this.radioLIFO.Tag = "3";
            this.radioLIFO.Text = "LIFO";
            this.radioLIFO.UseVisualStyleBackColor = true;
            // 
            // radioFIFO
            // 
            this.radioFIFO.AutoSize = true;
            this.radioFIFO.Font = new System.Drawing.Font("Arial", 9F);
            this.radioFIFO.ForeColor = System.Drawing.Color.Navy;
            this.radioFIFO.Location = new System.Drawing.Point(16, 87);
            this.radioFIFO.Name = "radioFIFO";
            this.radioFIFO.Size = new System.Drawing.Size(51, 19);
            this.radioFIFO.TabIndex = 2;
            this.radioFIFO.Tag = "2";
            this.radioFIFO.Text = "FIFO";
            this.radioFIFO.UseVisualStyleBackColor = true;
            // 
            // radioREAL
            // 
            this.radioREAL.AutoSize = true;
            this.radioREAL.Enabled = false;
            this.radioREAL.Font = new System.Drawing.Font("Arial", 9F);
            this.radioREAL.ForeColor = System.Drawing.Color.Navy;
            this.radioREAL.Location = new System.Drawing.Point(16, 62);
            this.radioREAL.Name = "radioREAL";
            this.radioREAL.Size = new System.Drawing.Size(106, 19);
            this.radioREAL.TabIndex = 1;
            this.radioREAL.Tag = "4";
            this.radioREAL.Text = "Xuất đích danh";
            this.radioREAL.UseVisualStyleBackColor = true;
            // 
            // radioAVER1
            // 
            this.radioAVER1.AutoSize = true;
            this.radioAVER1.Enabled = false;
            this.radioAVER1.Font = new System.Drawing.Font("Arial", 9F);
            this.radioAVER1.ForeColor = System.Drawing.Color.Navy;
            this.radioAVER1.Location = new System.Drawing.Point(16, 16);
            this.radioAVER1.Name = "radioAVER1";
            this.radioAVER1.Size = new System.Drawing.Size(179, 19);
            this.radioAVER1.TabIndex = 0;
            this.radioAVER1.Tag = "1";
            this.radioAVER1.Text = "Giá bình quân thường xuyên";
            this.radioAVER1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(547, 370);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.lbFromatCurrencyMain);
            this.tabPage1.Controls.Add(this.lbFormatCurrencyForeign);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.coDigitsAfterCurrencyForeign);
            this.tabPage1.Controls.Add(this.coDigitsAfterCurrencyMain);
            this.tabPage1.Controls.Add(this.coDigitGroupingCurrency);
            this.tabPage1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.tabPage1.ForeColor = System.Drawing.Color.Navy;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(539, 344);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Định dạng tiền tệ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F);
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(11, 140);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 15);
            this.label14.TabIndex = 27;
            this.label14.Text = "Ngoại tệ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(11, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 15);
            this.label13.TabIndex = 26;
            this.label13.Text = "Tiền tệ theo dõi";
            // 
            // lbFromatCurrencyMain
            // 
            this.lbFromatCurrencyMain.AutoSize = true;
            this.lbFromatCurrencyMain.Font = new System.Drawing.Font("Arial", 9F);
            this.lbFromatCurrencyMain.ForeColor = System.Drawing.Color.Navy;
            this.lbFromatCurrencyMain.Location = new System.Drawing.Point(150, 113);
            this.lbFromatCurrencyMain.Name = "lbFromatCurrencyMain";
            this.lbFromatCurrencyMain.Size = new System.Drawing.Size(56, 15);
            this.lbFromatCurrencyMain.TabIndex = 25;
            this.lbFromatCurrencyMain.Text = "1234567";
            // 
            // lbFormatCurrencyForeign
            // 
            this.lbFormatCurrencyForeign.AutoSize = true;
            this.lbFormatCurrencyForeign.Font = new System.Drawing.Font("Arial", 9F);
            this.lbFormatCurrencyForeign.ForeColor = System.Drawing.Color.Navy;
            this.lbFormatCurrencyForeign.Location = new System.Drawing.Point(150, 139);
            this.lbFormatCurrencyForeign.Name = "lbFormatCurrencyForeign";
            this.lbFormatCurrencyForeign.Size = new System.Drawing.Size(56, 15);
            this.lbFormatCurrencyForeign.TabIndex = 24;
            this.lbFormatCurrencyForeign.Text = "1234567";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(11, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 15);
            this.label10.TabIndex = 23;
            this.label10.Text = "Số lẽ của tiền ngoại tệ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(11, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 15);
            this.label7.TabIndex = 22;
            this.label7.Text = "Số lẽ tiền tệ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(11, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "Dấu ngăn cách";
            // 
            // coDigitsAfterCurrencyForeign
            // 
            this.coDigitsAfterCurrencyForeign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coDigitsAfterCurrencyForeign.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coDigitsAfterCurrencyForeign.ForeColor = System.Drawing.Color.Navy;
            this.coDigitsAfterCurrencyForeign.FormattingEnabled = true;
            this.coDigitsAfterCurrencyForeign.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.coDigitsAfterCurrencyForeign.Location = new System.Drawing.Point(153, 68);
            this.coDigitsAfterCurrencyForeign.Name = "coDigitsAfterCurrencyForeign";
            this.coDigitsAfterCurrencyForeign.Size = new System.Drawing.Size(128, 23);
            this.coDigitsAfterCurrencyForeign.TabIndex = 20;
            this.coDigitsAfterCurrencyForeign.SelectedIndexChanged += new System.EventHandler(this.coDigitsAfterCurrencyForeign_SelectedIndexChanged);
            // 
            // coDigitsAfterCurrencyMain
            // 
            this.coDigitsAfterCurrencyMain.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coDigitsAfterCurrencyMain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coDigitsAfterCurrencyMain.ForeColor = System.Drawing.Color.Navy;
            this.coDigitsAfterCurrencyMain.FormattingEnabled = true;
            this.coDigitsAfterCurrencyMain.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.coDigitsAfterCurrencyMain.Location = new System.Drawing.Point(153, 40);
            this.coDigitsAfterCurrencyMain.Name = "coDigitsAfterCurrencyMain";
            this.coDigitsAfterCurrencyMain.Size = new System.Drawing.Size(128, 23);
            this.coDigitsAfterCurrencyMain.TabIndex = 19;
            this.coDigitsAfterCurrencyMain.SelectedIndexChanged += new System.EventHandler(this.coDigitsAfterCurrencyMain_SelectedIndexChanged);
            // 
            // coDigitGroupingCurrency
            // 
            this.coDigitGroupingCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coDigitGroupingCurrency.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coDigitGroupingCurrency.ForeColor = System.Drawing.Color.Navy;
            this.coDigitGroupingCurrency.FormattingEnabled = true;
            this.coDigitGroupingCurrency.Items.AddRange(new object[] {
            "1,234,567",
            "1.234.567"});
            this.coDigitGroupingCurrency.Location = new System.Drawing.Point(153, 10);
            this.coDigitGroupingCurrency.Name = "coDigitGroupingCurrency";
            this.coDigitGroupingCurrency.Size = new System.Drawing.Size(128, 24);
            this.coDigitGroupingCurrency.TabIndex = 18;
            this.coDigitGroupingCurrency.SelectedIndexChanged += new System.EventHandler(this.coDigitGroupingCurrency_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.lbFormatDecimal);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.coDigitsAfterDecimal);
            this.tabPage2.Controls.Add(this.coDigitGroupDecimal);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(539, 344);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Định dạng số";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F);
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(15, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 28;
            this.label15.Text = "Định dạng Số";
            // 
            // lbFormatDecimal
            // 
            this.lbFormatDecimal.AutoSize = true;
            this.lbFormatDecimal.Font = new System.Drawing.Font("Arial", 9F);
            this.lbFormatDecimal.ForeColor = System.Drawing.Color.Navy;
            this.lbFormatDecimal.Location = new System.Drawing.Point(116, 85);
            this.lbFormatDecimal.Name = "lbFormatDecimal";
            this.lbFormatDecimal.Size = new System.Drawing.Size(56, 15);
            this.lbFormatDecimal.TabIndex = 27;
            this.lbFormatDecimal.Text = "1234567";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(12, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 15);
            this.label11.TabIndex = 26;
            this.label11.Text = "Số lẽ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F);
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(10, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 15);
            this.label12.TabIndex = 25;
            this.label12.Text = "Dấu ngăn cách";
            // 
            // coDigitsAfterDecimal
            // 
            this.coDigitsAfterDecimal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coDigitsAfterDecimal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coDigitsAfterDecimal.ForeColor = System.Drawing.Color.Navy;
            this.coDigitsAfterDecimal.FormattingEnabled = true;
            this.coDigitsAfterDecimal.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4"});
            this.coDigitsAfterDecimal.Location = new System.Drawing.Point(119, 42);
            this.coDigitsAfterDecimal.Name = "coDigitsAfterDecimal";
            this.coDigitsAfterDecimal.Size = new System.Drawing.Size(128, 23);
            this.coDigitsAfterDecimal.TabIndex = 24;
            this.coDigitsAfterDecimal.SelectedIndexChanged += new System.EventHandler(this.coDigitsAfterDecimal_SelectedIndexChanged);
            // 
            // coDigitGroupDecimal
            // 
            this.coDigitGroupDecimal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coDigitGroupDecimal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coDigitGroupDecimal.ForeColor = System.Drawing.Color.Navy;
            this.coDigitGroupDecimal.FormattingEnabled = true;
            this.coDigitGroupDecimal.Items.AddRange(new object[] {
            "1,234,567",
            "1.234.567"});
            this.coDigitGroupDecimal.Location = new System.Drawing.Point(119, 15);
            this.coDigitGroupDecimal.Name = "coDigitGroupDecimal";
            this.coDigitGroupDecimal.Size = new System.Drawing.Size(128, 24);
            this.coDigitGroupDecimal.TabIndex = 23;
            this.coDigitGroupDecimal.SelectedIndexChanged += new System.EventHandler(this.coDigitGroupDecimal_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbFormatDate);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.coFormatDate);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(539, 344);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Định dạng ngày";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbFormatDate
            // 
            this.lbFormatDate.AutoSize = true;
            this.lbFormatDate.Font = new System.Drawing.Font("Arial", 9F);
            this.lbFormatDate.ForeColor = System.Drawing.Color.Navy;
            this.lbFormatDate.Location = new System.Drawing.Point(81, 51);
            this.lbFormatDate.Name = "lbFormatDate";
            this.lbFormatDate.Size = new System.Drawing.Size(69, 15);
            this.lbFormatDate.TabIndex = 29;
            this.lbFormatDate.Text = "15/10/2009";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F);
            this.label17.ForeColor = System.Drawing.Color.Navy;
            this.label17.Location = new System.Drawing.Point(17, 51);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 15);
            this.label17.TabIndex = 28;
            this.label17.Text = "Hôm nay";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F);
            this.label16.ForeColor = System.Drawing.Color.Navy;
            this.label16.Location = new System.Drawing.Point(17, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 15);
            this.label16.TabIndex = 27;
            this.label16.Text = "Kiểu ngày";
            // 
            // coFormatDate
            // 
            this.coFormatDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coFormatDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.coFormatDate.ForeColor = System.Drawing.Color.Navy;
            this.coFormatDate.FormattingEnabled = true;
            this.coFormatDate.Items.AddRange(new object[] {
            "dd/MM/yyyy",
            "dd/MM/yy",
            "MM/dd/yyyy",
            "MM/dd/yy"});
            this.coFormatDate.Location = new System.Drawing.Point(84, 14);
            this.coFormatDate.Name = "coFormatDate";
            this.coFormatDate.Size = new System.Drawing.Size(128, 23);
            this.coFormatDate.TabIndex = 26;
            this.coFormatDate.SelectedIndexChanged += new System.EventHandler(this.coFormatDate_SelectedIndexChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox2);
            this.tabPage6.Controls.Add(this.groupBox1);
            this.tabPage6.Controls.Add(this.groupBox3);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(539, 344);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Kế toán";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtCashOnBank);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtCashOnHand);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtVATDeduction);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtVATOutput);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtAccountCustomer);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtAccountVendor);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtAccountProduct);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.groupBox2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox2.Location = new System.Drawing.Point(10, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 172);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tài khoản mặt định";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F);
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(295, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 15);
            this.label20.TabIndex = 33;
            this.label20.Text = "Tiền ngân hàng";
            // 
            // txtCashOnBank
            // 
            this.txtCashOnBank.Location = new System.Drawing.Point(394, 47);
            this.txtCashOnBank.Name = "txtCashOnBank";
            this.txtCashOnBank.Size = new System.Drawing.Size(100, 21);
            this.txtCashOnBank.TabIndex = 32;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9F);
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(295, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 15);
            this.label21.TabIndex = 31;
            this.label21.Text = "Tiền mặt";
            // 
            // txtCashOnHand
            // 
            this.txtCashOnHand.Location = new System.Drawing.Point(394, 21);
            this.txtCashOnHand.Name = "txtCashOnHand";
            this.txtCashOnHand.Size = new System.Drawing.Size(100, 21);
            this.txtCashOnHand.TabIndex = 30;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F);
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(9, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 15);
            this.label19.TabIndex = 29;
            this.label19.Text = "VAT Khấu trừ";
            // 
            // txtVATDeduction
            // 
            this.txtVATDeduction.Location = new System.Drawing.Point(168, 131);
            this.txtVATDeduction.Name = "txtVATDeduction";
            this.txtVATDeduction.Size = new System.Drawing.Size(100, 21);
            this.txtVATDeduction.TabIndex = 28;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F);
            this.label18.ForeColor = System.Drawing.Color.Navy;
            this.label18.Location = new System.Drawing.Point(9, 105);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 15);
            this.label18.TabIndex = 27;
            this.label18.Text = "VAT Phải nộp";
            // 
            // txtVATOutput
            // 
            this.txtVATOutput.Location = new System.Drawing.Point(168, 105);
            this.txtVATOutput.Name = "txtVATOutput";
            this.txtVATOutput.Size = new System.Drawing.Size(100, 21);
            this.txtVATOutput.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(9, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "Tài khoản công nợ phải thu";
            // 
            // txtAccountCustomer
            // 
            this.txtAccountCustomer.Location = new System.Drawing.Point(168, 79);
            this.txtAccountCustomer.Name = "txtAccountCustomer";
            this.txtAccountCustomer.Size = new System.Drawing.Size(100, 21);
            this.txtAccountCustomer.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(9, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "Tài khoản công nợ phải trả";
            // 
            // txtAccountVendor
            // 
            this.txtAccountVendor.Location = new System.Drawing.Point(168, 50);
            this.txtAccountVendor.Name = "txtAccountVendor";
            this.txtAccountVendor.Size = new System.Drawing.Size(100, 21);
            this.txtAccountVendor.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(9, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 15);
            this.label4.TabIndex = 21;
            this.label4.Text = "Tài khoản vật tư";
            // 
            // txtAccountProduct
            // 
            this.txtAccountProduct.Location = new System.Drawing.Point(168, 23);
            this.txtAccountProduct.Name = "txtAccountProduct";
            this.txtAccountProduct.Size = new System.Drawing.Size(100, 21);
            this.txtAccountProduct.TabIndex = 20;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label22);
            this.tabPage5.Controls.Add(this.coLanguage);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(539, 344);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Khác";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F);
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(11, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 15);
            this.label22.TabIndex = 6;
            this.label22.Text = "Language";
            // 
            // coLanguage
            // 
            this.coLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coLanguage.Font = new System.Drawing.Font("Arial", 9F);
            this.coLanguage.ForeColor = System.Drawing.Color.Navy;
            this.coLanguage.FormattingEnabled = true;
            this.coLanguage.Items.AddRange(new object[] {
            "English - EN",
            "Vietnamese - VN",
            "China - CN"});
            this.coLanguage.Location = new System.Drawing.Point(80, 8);
            this.coLanguage.Name = "coLanguage";
            this.coLanguage.Size = new System.Drawing.Size(175, 23);
            this.coLanguage.TabIndex = 5;
            // 
            // FrmSystemConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(547, 406);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSystemConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông số hệ thống";
            this.Load += new System.EventHandler(this.FrmSystemConfig_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdSaveItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioLIFO;
        private System.Windows.Forms.RadioButton radioFIFO;
        private System.Windows.Forms.RadioButton radioREAL;
        private System.Windows.Forms.RadioButton radioAVER1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox coCurrencyMain;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox coFinancialYear;
        private System.Windows.Forms.TextBox txtToDate;
        private System.Windows.Forms.TextBox txtFromDate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lbFromatCurrencyMain;
        private System.Windows.Forms.Label lbFormatCurrencyForeign;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox coDigitsAfterCurrencyForeign;
        private System.Windows.Forms.ComboBox coDigitsAfterCurrencyMain;
        private System.Windows.Forms.ComboBox coDigitGroupingCurrency;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox coDigitsAfterDecimal;
        private System.Windows.Forms.ComboBox coDigitGroupDecimal;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbFormatDecimal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox coFormatDate;
        private System.Windows.Forms.Label lbFormatDate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton radioAVER2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox coLanguage;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCashOnBank;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtCashOnHand;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtVATDeduction;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtVATOutput;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAccountCustomer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAccountVendor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAccountProduct;
    }
}