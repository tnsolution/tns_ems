using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.Systems
{
    public partial class FrmCompanyInfo : Form
    {
        private CompanyInfo mCompany = new CompanyInfo();
        private bool FlagChangePic;
        public FrmCompanyInfo()
        {
            InitializeComponent();
        }

        private void FrmCompanyInfo_Load(object sender, EventArgs e)
        {
            CheckRole();

            txtCompanyName.Text = mCompany.CompanyName;
            txtAddress.Text = mCompany.Address;
            txtTel.Text = mCompany.Tel;
            txtFax.Text = mCompany.Fax;
            txtEmail.Text = mCompany.Email;
            txtWeb.Text = mCompany.Web;

            if (mCompany.Logo != null)
            {
                GraphicProcess.ViewImage(picLogo, mCompany.Logo);
                picLogo.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            mCompany.CompanyName = txtCompanyName.Text;
            mCompany.Address = txtAddress.Text;
            mCompany.Tel = txtTel.Text;
            mCompany.Fax = txtFax.Text;
            mCompany.Email = txtEmail.Text;
            mCompany.Web = txtWeb.Text;

            if (FlagChangePic)
                mCompany.Logo = GraphicProcess.ImageToByte(picLogo);

            mCompany.Update();

            this.Close();
        }

        private void picLogo_Click(object sender, EventArgs e)
        {
            OpenFileDialog1.InitialDirectory = "C:/";
            OpenFileDialog1.Filter = "All Files|*.*|Bitmaps|*.bmp|GIFs|*.gif|JPEGs|*.jpg";
            OpenFileDialog1.FilterIndex = 3;

            if (OpenFileDialog1.ShowDialog() == DialogResult.OK)
            {
                picLogo.Image = Image.FromFile(OpenFileDialog1.FileName);
                picLogo.SizeMode = PictureBoxSizeMode.AutoSize;
                FlagChangePic = true;
            }
        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("SYS001");
            if (!nRole.Edit)
                cmdSave.Enabled = false;
        }
    }
}