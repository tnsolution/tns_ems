﻿namespace TN_UI.FRM
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatusLogin = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDataBase = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtLanguage = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUserName = new System.Windows.Forms.ToolStripStatusLabel();
            this.mdiTabStrip1 = new MdiTabStrip.MdiTabStrip();
            this.MenuMain_System = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_System_CompanyInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_System_FinancialYears = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_System_ConnectDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_System_Preferences = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_System_Users = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_System_ChangPass = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_System_Logout = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_System_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_List = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_List_Product_Categories = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_List_Product_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_List_Products = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator37 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuMain_CRM = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Vendor_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Vendor_List = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Vendor_Categories = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_crm_Customer_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Customer_List = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Customer_Categories = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_crm_Task_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Task_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator39 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_crm_Contract_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_crm_Contract_List = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain = new System.Windows.Forms.MenuStrip();
            this.MenuMain_HRM = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_hrm_Department = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_hrm_Branch = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_hrm_Employees = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator38 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_hrm_Timekeeping = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_hrm_Payroll = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_hrm_Payroll_List = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Inventory = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Inventory_Warehouse = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Inventory_Input_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Inventory_Input_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator35 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Inventory_Output_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Inventory_Output_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator36 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Inventory_Begin = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Inventory_List = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Financial = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Purchase_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Purchase_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator40 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Financial_Sale_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Sale_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Financial_Payment_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Payment_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Financial_Receipt_AddNew = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Receipt_List = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Financial_Dept_Begin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator46 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Financial_Reports = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Report_Dept = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Financial_Report_BillDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMain_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.hướngDẫnSửDụngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_AboutUs = new System.Windows.Forms.ToolStripMenuItem();
            this.tnGridView1 = new TNLibrary.SYS.Forms.TNGridView();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mdiTabStrip1)).BeginInit();
            this.MenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusLogin,
            this.lblDataBase,
            this.txtLanguage,
            this.lblUserName});
            this.statusStrip1.Location = new System.Drawing.Point(0, 331);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1276, 24);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatusLogin
            // 
            this.lblStatusLogin.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblStatusLogin.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblStatusLogin.Name = "lblStatusLogin";
            this.lblStatusLogin.Size = new System.Drawing.Size(49, 19);
            this.lblStatusLogin.Text = "Status :";
            // 
            // lblDataBase
            // 
            this.lblDataBase.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblDataBase.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblDataBase.Name = "lblDataBase";
            this.lblDataBase.Size = new System.Drawing.Size(68, 19);
            this.lblDataBase.Text = "DataBase : ";
            // 
            // txtLanguage
            // 
            this.txtLanguage.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.txtLanguage.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.txtLanguage.Name = "txtLanguage";
            this.txtLanguage.Size = new System.Drawing.Size(26, 19);
            this.txtLanguage.Text = "EN";
            // 
            // lblUserName
            // 
            this.lblUserName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lblUserName.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(75, 19);
            this.lblUserName.Text = "UserName : ";
            // 
            // mdiTabStrip1
            // 
            this.mdiTabStrip1.ActiveTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdiTabStrip1.AllowDrop = true;
            this.mdiTabStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(202)))), ((int)(((byte)(241)))));
            this.mdiTabStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.mdiTabStrip1.InactiveTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdiTabStrip1.Location = new System.Drawing.Point(0, 34);
            this.mdiTabStrip1.MdiNewTabImage = null;
            this.mdiTabStrip1.MinimumSize = new System.Drawing.Size(50, 33);
            this.mdiTabStrip1.MouseOverTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdiTabStrip1.Name = "mdiTabStrip1";
            this.mdiTabStrip1.NewTabToolTipText = "New Tab";
            this.mdiTabStrip1.Padding = new System.Windows.Forms.Padding(5, 3, 20, 5);
            this.mdiTabStrip1.Size = new System.Drawing.Size(1276, 35);
            this.mdiTabStrip1.TabIndex = 7;
            this.mdiTabStrip1.Text = "mdiTabStrip1";
            // 
            // MenuMain_System
            // 
            this.MenuMain_System.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_System_CompanyInfo,
            this.toolStripSeparator9,
            this.Menu_System_FinancialYears,
            this.toolStripSeparator10,
            this.Menu_System_ConnectDatabase,
            this.Menu_System_Preferences,
            this.toolStripSeparator11,
            this.Menu_System_Users,
            this.Menu_System_ChangPass,
            this.toolStripSeparator1,
            this.Menu_System_Logout,
            this.Menu_System_Exit});
            this.MenuMain_System.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_System.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_System.Image")));
            this.MenuMain_System.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_System.Name = "MenuMain_System";
            this.MenuMain_System.Size = new System.Drawing.Size(96, 30);
            this.MenuMain_System.Text = "Hệ thống";
            // 
            // Menu_System_CompanyInfo
            // 
            this.Menu_System_CompanyInfo.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_CompanyInfo.Image = ((System.Drawing.Image)(resources.GetObject("Menu_System_CompanyInfo.Image")));
            this.Menu_System_CompanyInfo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_System_CompanyInfo.Name = "Menu_System_CompanyInfo";
            this.Menu_System_CompanyInfo.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_CompanyInfo.Text = "Thông tin công ty";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(183, 6);
            // 
            // Menu_System_FinancialYears
            // 
            this.Menu_System_FinancialYears.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_FinancialYears.Name = "Menu_System_FinancialYears";
            this.Menu_System_FinancialYears.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_FinancialYears.Text = "Năm tài chính";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(183, 6);
            // 
            // Menu_System_ConnectDatabase
            // 
            this.Menu_System_ConnectDatabase.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_ConnectDatabase.Image = ((System.Drawing.Image)(resources.GetObject("Menu_System_ConnectDatabase.Image")));
            this.Menu_System_ConnectDatabase.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_System_ConnectDatabase.Name = "Menu_System_ConnectDatabase";
            this.Menu_System_ConnectDatabase.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_ConnectDatabase.Text = "Connect Database";
            // 
            // Menu_System_Preferences
            // 
            this.Menu_System_Preferences.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_Preferences.Name = "Menu_System_Preferences";
            this.Menu_System_Preferences.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_Preferences.Text = "Cấu hình";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(183, 6);
            // 
            // Menu_System_Users
            // 
            this.Menu_System_Users.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_Users.Name = "Menu_System_Users";
            this.Menu_System_Users.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_Users.Text = "Danh sách users";
            // 
            // Menu_System_ChangPass
            // 
            this.Menu_System_ChangPass.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_ChangPass.Name = "Menu_System_ChangPass";
            this.Menu_System_ChangPass.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_ChangPass.Text = "Thay đổi password";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // Menu_System_Logout
            // 
            this.Menu_System_Logout.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_Logout.Name = "Menu_System_Logout";
            this.Menu_System_Logout.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_Logout.Text = "Logout";
            // 
            // Menu_System_Exit
            // 
            this.Menu_System_Exit.ForeColor = System.Drawing.Color.Navy;
            this.Menu_System_Exit.Name = "Menu_System_Exit";
            this.Menu_System_Exit.Size = new System.Drawing.Size(186, 24);
            this.Menu_System_Exit.Text = "Thoát";
            // 
            // MenuMain_List
            // 
            this.MenuMain_List.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_List_Product_Categories,
            this.Menu_List_Product_AddNew,
            this.Menu_List_Products,
            this.toolStripSeparator37});
            this.MenuMain_List.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_List.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_List.Image")));
            this.MenuMain_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_List.Name = "MenuMain_List";
            this.MenuMain_List.Size = new System.Drawing.Size(105, 30);
            this.MenuMain_List.Text = "Danh mục";
            // 
            // Menu_List_Product_Categories
            // 
            this.Menu_List_Product_Categories.ForeColor = System.Drawing.Color.Navy;
            this.Menu_List_Product_Categories.Name = "Menu_List_Product_Categories";
            this.Menu_List_Product_Categories.Size = new System.Drawing.Size(265, 32);
            this.Menu_List_Product_Categories.Text = "Danh mục Sản phẩm - Dịch vụ";
            // 
            // Menu_List_Product_AddNew
            // 
            this.Menu_List_Product_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_List_Product_AddNew.Name = "Menu_List_Product_AddNew";
            this.Menu_List_Product_AddNew.Size = new System.Drawing.Size(265, 32);
            this.Menu_List_Product_AddNew.Text = "Tạo mới Sản phẩm - Dịch vụ";
            // 
            // Menu_List_Products
            // 
            this.Menu_List_Products.ForeColor = System.Drawing.Color.Navy;
            this.Menu_List_Products.Image = ((System.Drawing.Image)(resources.GetObject("Menu_List_Products.Image")));
            this.Menu_List_Products.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_List_Products.Name = "Menu_List_Products";
            this.Menu_List_Products.Size = new System.Drawing.Size(265, 32);
            this.Menu_List_Products.Text = "Danh sách Sản phẩm - Dịch vụ";
            // 
            // toolStripSeparator37
            // 
            this.toolStripSeparator37.Name = "toolStripSeparator37";
            this.toolStripSeparator37.Size = new System.Drawing.Size(262, 6);
            // 
            // MenuMain_CRM
            // 
            this.MenuMain_CRM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_crm_Vendor_AddNew,
            this.Menu_crm_Vendor_List,
            this.Menu_crm_Vendor_Categories,
            this.toolStripSeparator14,
            this.Menu_crm_Customer_AddNew,
            this.Menu_crm_Customer_List,
            this.Menu_crm_Customer_Categories,
            this.toolStripSeparator16,
            this.Menu_crm_Task_AddNew,
            this.Menu_crm_Task_List,
            this.toolStripSeparator39,
            this.Menu_crm_Contract_AddNew,
            this.Menu_crm_Contract_List});
            this.MenuMain_CRM.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_CRM.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_CRM.Image")));
            this.MenuMain_CRM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_CRM.Name = "MenuMain_CRM";
            this.MenuMain_CRM.Size = new System.Drawing.Size(113, 30);
            this.MenuMain_CRM.Text = "Khách hàng";
            // 
            // Menu_crm_Vendor_AddNew
            // 
            this.Menu_crm_Vendor_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Vendor_AddNew.Name = "Menu_crm_Vendor_AddNew";
            this.Menu_crm_Vendor_AddNew.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Vendor_AddNew.Text = "Tạo mới nhà cung cấp";
            // 
            // Menu_crm_Vendor_List
            // 
            this.Menu_crm_Vendor_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Vendor_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_crm_Vendor_List.Image")));
            this.Menu_crm_Vendor_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_crm_Vendor_List.Name = "Menu_crm_Vendor_List";
            this.Menu_crm_Vendor_List.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Vendor_List.Text = "Danh sách nhà cung cấp";
            // 
            // Menu_crm_Vendor_Categories
            // 
            this.Menu_crm_Vendor_Categories.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Vendor_Categories.Name = "Menu_crm_Vendor_Categories";
            this.Menu_crm_Vendor_Categories.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Vendor_Categories.Text = "Nhóm nhà cung cấp";
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(227, 6);
            // 
            // Menu_crm_Customer_AddNew
            // 
            this.Menu_crm_Customer_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Customer_AddNew.Name = "Menu_crm_Customer_AddNew";
            this.Menu_crm_Customer_AddNew.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Customer_AddNew.Text = "Tạo mới khách hàng";
            // 
            // Menu_crm_Customer_List
            // 
            this.Menu_crm_Customer_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Customer_List.Name = "Menu_crm_Customer_List";
            this.Menu_crm_Customer_List.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Customer_List.Text = "Danh sách khách hàng";
            // 
            // Menu_crm_Customer_Categories
            // 
            this.Menu_crm_Customer_Categories.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Customer_Categories.Name = "Menu_crm_Customer_Categories";
            this.Menu_crm_Customer_Categories.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Customer_Categories.Text = "Nhóm khách hàng";
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(227, 6);
            // 
            // Menu_crm_Task_AddNew
            // 
            this.Menu_crm_Task_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Task_AddNew.Name = "Menu_crm_Task_AddNew";
            this.Menu_crm_Task_AddNew.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Task_AddNew.Text = "Tạo mới công việc";
            // 
            // Menu_crm_Task_List
            // 
            this.Menu_crm_Task_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Task_List.Name = "Menu_crm_Task_List";
            this.Menu_crm_Task_List.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Task_List.Text = "Danh sách công việc";
            // 
            // toolStripSeparator39
            // 
            this.toolStripSeparator39.Name = "toolStripSeparator39";
            this.toolStripSeparator39.Size = new System.Drawing.Size(227, 6);
            // 
            // Menu_crm_Contract_AddNew
            // 
            this.Menu_crm_Contract_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Contract_AddNew.Name = "Menu_crm_Contract_AddNew";
            this.Menu_crm_Contract_AddNew.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Contract_AddNew.Text = "Tạo hợp đồng mới";
            // 
            // Menu_crm_Contract_List
            // 
            this.Menu_crm_Contract_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_crm_Contract_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_crm_Contract_List.Image")));
            this.Menu_crm_Contract_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_crm_Contract_List.Name = "Menu_crm_Contract_List";
            this.Menu_crm_Contract_List.Size = new System.Drawing.Size(230, 32);
            this.Menu_crm_Contract_List.Text = "Danh sách hợp đồng";
            // 
            // MenuMain
            // 
            this.MenuMain.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MenuMain.BackgroundImage")));
            this.MenuMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MenuMain.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMain_System,
            this.MenuMain_List,
            this.MenuMain_HRM,
            this.MenuMain_CRM,
            this.MenuMain_Inventory,
            this.MenuMain_Financial,
            this.MenuMain_Help});
            this.MenuMain.Location = new System.Drawing.Point(0, 0);
            this.MenuMain.Name = "MenuMain";
            this.MenuMain.Size = new System.Drawing.Size(1276, 34);
            this.MenuMain.TabIndex = 11;
            this.MenuMain.Text = "MenuMain";
            // 
            // MenuMain_HRM
            // 
            this.MenuMain_HRM.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_hrm_Department,
            this.Menu_hrm_Branch,
            this.Menu_hrm_Employees,
            this.toolStripSeparator38,
            this.Menu_hrm_Timekeeping,
            this.Menu_hrm_Payroll,
            this.Menu_hrm_Payroll_List});
            this.MenuMain_HRM.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_HRM.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_HRM.Image")));
            this.MenuMain_HRM.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_HRM.Name = "MenuMain_HRM";
            this.MenuMain_HRM.Size = new System.Drawing.Size(94, 30);
            this.MenuMain_HRM.Text = "Nhân sự";
            // 
            // Menu_hrm_Department
            // 
            this.Menu_hrm_Department.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Department.Name = "Menu_hrm_Department";
            this.Menu_hrm_Department.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Department.Text = "Cơ cấu Tổ chức - Phòng ban";
            // 
            // Menu_hrm_Branch
            // 
            this.Menu_hrm_Branch.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Branch.Name = "Menu_hrm_Branch";
            this.Menu_hrm_Branch.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Branch.Text = "Danh sách Trụ sở - Chi nhánh";
            // 
            // Menu_hrm_Employees
            // 
            this.Menu_hrm_Employees.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Employees.Image = ((System.Drawing.Image)(resources.GetObject("Menu_hrm_Employees.Image")));
            this.Menu_hrm_Employees.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_hrm_Employees.Name = "Menu_hrm_Employees";
            this.Menu_hrm_Employees.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Employees.Text = "Hồ sơ nhân sự";
            // 
            // toolStripSeparator38
            // 
            this.toolStripSeparator38.Name = "toolStripSeparator38";
            this.toolStripSeparator38.Size = new System.Drawing.Size(257, 6);
            // 
            // Menu_hrm_Timekeeping
            // 
            this.Menu_hrm_Timekeeping.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Timekeeping.Name = "Menu_hrm_Timekeeping";
            this.Menu_hrm_Timekeeping.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Timekeeping.Text = "Chấm công";
            // 
            // Menu_hrm_Payroll
            // 
            this.Menu_hrm_Payroll.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Payroll.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_hrm_Payroll.Name = "Menu_hrm_Payroll";
            this.Menu_hrm_Payroll.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Payroll.Text = "Phiếu lương";
            // 
            // Menu_hrm_Payroll_List
            // 
            this.Menu_hrm_Payroll_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_hrm_Payroll_List.Name = "Menu_hrm_Payroll_List";
            this.Menu_hrm_Payroll_List.Size = new System.Drawing.Size(260, 32);
            this.Menu_hrm_Payroll_List.Text = "Bảng lương";
            // 
            // MenuMain_Inventory
            // 
            this.MenuMain_Inventory.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Inventory_Warehouse,
            this.toolStripSeparator18,
            this.Menu_Inventory_Input_AddNew,
            this.Menu_Inventory_Input_List,
            this.toolStripSeparator35,
            this.Menu_Inventory_Output_AddNew,
            this.Menu_Inventory_Output_List,
            this.toolStripSeparator36,
            this.Menu_Inventory_Begin,
            this.Menu_Inventory_List});
            this.MenuMain_Inventory.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Inventory.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_Inventory.Image")));
            this.MenuMain_Inventory.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Inventory.Name = "MenuMain_Inventory";
            this.MenuMain_Inventory.Size = new System.Drawing.Size(144, 30);
            this.MenuMain_Inventory.Text = "Nhập - Xuất - Tồn";
            this.MenuMain_Inventory.Click += new System.EventHandler(this.MenuMain_Inventory_Click);
            // 
            // Menu_Inventory_Warehouse
            // 
            this.Menu_Inventory_Warehouse.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Warehouse.Name = "Menu_Inventory_Warehouse";
            this.Menu_Inventory_Warehouse.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Warehouse.Text = "Danh sách kho hàng";
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(237, 6);
            // 
            // Menu_Inventory_Input_AddNew
            // 
            this.Menu_Inventory_Input_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Input_AddNew.Name = "Menu_Inventory_Input_AddNew";
            this.Menu_Inventory_Input_AddNew.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Input_AddNew.Text = "Tạo mới phiếu nhập kho";
            // 
            // Menu_Inventory_Input_List
            // 
            this.Menu_Inventory_Input_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Input_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Inventory_Input_List.Image")));
            this.Menu_Inventory_Input_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Inventory_Input_List.Name = "Menu_Inventory_Input_List";
            this.Menu_Inventory_Input_List.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Input_List.Text = "Danh sách phiếu nhập kho";
            // 
            // toolStripSeparator35
            // 
            this.toolStripSeparator35.Name = "toolStripSeparator35";
            this.toolStripSeparator35.Size = new System.Drawing.Size(237, 6);
            // 
            // Menu_Inventory_Output_AddNew
            // 
            this.Menu_Inventory_Output_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Output_AddNew.Name = "Menu_Inventory_Output_AddNew";
            this.Menu_Inventory_Output_AddNew.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Output_AddNew.Text = "Tạo mới Phiếu xuất kho";
            // 
            // Menu_Inventory_Output_List
            // 
            this.Menu_Inventory_Output_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Output_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Inventory_Output_List.Image")));
            this.Menu_Inventory_Output_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Inventory_Output_List.Name = "Menu_Inventory_Output_List";
            this.Menu_Inventory_Output_List.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Output_List.Text = "Danh sách Phiếu xuất kho";
            // 
            // toolStripSeparator36
            // 
            this.toolStripSeparator36.Name = "toolStripSeparator36";
            this.toolStripSeparator36.Size = new System.Drawing.Size(237, 6);
            // 
            // Menu_Inventory_Begin
            // 
            this.Menu_Inventory_Begin.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_Begin.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Inventory_Begin.Image")));
            this.Menu_Inventory_Begin.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Inventory_Begin.Name = "Menu_Inventory_Begin";
            this.Menu_Inventory_Begin.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_Begin.Text = "Hàng tồn kho đầu kỳ";
            // 
            // Menu_Inventory_List
            // 
            this.Menu_Inventory_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Inventory_List.Name = "Menu_Inventory_List";
            this.Menu_Inventory_List.Size = new System.Drawing.Size(240, 32);
            this.Menu_Inventory_List.Text = "Bảng kê Nhập - Xuất - Tồn";
            // 
            // MenuMain_Financial
            // 
            this.MenuMain_Financial.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Financial_Purchase_AddNew,
            this.Menu_Financial_Purchase_List,
            this.toolStripSeparator40,
            this.Menu_Financial_Sale_AddNew,
            this.Menu_Financial_Sale_List,
            this.toolStripSeparator13,
            this.Menu_Financial_Payment_AddNew,
            this.Menu_Financial_Payment_List,
            this.toolStripSeparator15,
            this.Menu_Financial_Receipt_AddNew,
            this.Menu_Financial_Receipt_List,
            this.toolStripSeparator17,
            this.Menu_Financial_Dept_Begin,
            this.toolStripSeparator46,
            this.Menu_Financial_Reports});
            this.MenuMain_Financial.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Financial.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_Financial.Image")));
            this.MenuMain_Financial.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Financial.Name = "MenuMain_Financial";
            this.MenuMain_Financial.Size = new System.Drawing.Size(98, 30);
            this.MenuMain_Financial.Text = "Tài chính";
            // 
            // Menu_Financial_Purchase_AddNew
            // 
            this.Menu_Financial_Purchase_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Purchase_AddNew.Name = "Menu_Financial_Purchase_AddNew";
            this.Menu_Financial_Purchase_AddNew.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Purchase_AddNew.Text = "Tạo mới hóa đơn mua hàng";
            // 
            // Menu_Financial_Purchase_List
            // 
            this.Menu_Financial_Purchase_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Purchase_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Financial_Purchase_List.Image")));
            this.Menu_Financial_Purchase_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Financial_Purchase_List.Name = "Menu_Financial_Purchase_List";
            this.Menu_Financial_Purchase_List.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Purchase_List.Text = "Danh sách hóa đơn mua hàng";
            // 
            // toolStripSeparator40
            // 
            this.toolStripSeparator40.Name = "toolStripSeparator40";
            this.toolStripSeparator40.Size = new System.Drawing.Size(258, 6);
            // 
            // Menu_Financial_Sale_AddNew
            // 
            this.Menu_Financial_Sale_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Sale_AddNew.Name = "Menu_Financial_Sale_AddNew";
            this.Menu_Financial_Sale_AddNew.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Sale_AddNew.Text = "Tạo mới hóa đơn bán hàng";
            // 
            // Menu_Financial_Sale_List
            // 
            this.Menu_Financial_Sale_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Sale_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Financial_Sale_List.Image")));
            this.Menu_Financial_Sale_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Financial_Sale_List.Name = "Menu_Financial_Sale_List";
            this.Menu_Financial_Sale_List.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Sale_List.Text = "Danh sách hóa đơn bán hàng";
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(258, 6);
            // 
            // Menu_Financial_Payment_AddNew
            // 
            this.Menu_Financial_Payment_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Payment_AddNew.Name = "Menu_Financial_Payment_AddNew";
            this.Menu_Financial_Payment_AddNew.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Payment_AddNew.Text = "Tạo mới phiếu chi";
            // 
            // Menu_Financial_Payment_List
            // 
            this.Menu_Financial_Payment_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Payment_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Financial_Payment_List.Image")));
            this.Menu_Financial_Payment_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Financial_Payment_List.Name = "Menu_Financial_Payment_List";
            this.Menu_Financial_Payment_List.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Payment_List.Text = "Danh sách phiếu chi";
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.ForeColor = System.Drawing.Color.Navy;
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(258, 6);
            // 
            // Menu_Financial_Receipt_AddNew
            // 
            this.Menu_Financial_Receipt_AddNew.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Receipt_AddNew.Name = "Menu_Financial_Receipt_AddNew";
            this.Menu_Financial_Receipt_AddNew.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Receipt_AddNew.Text = "Tạo mới phiếu thu";
            // 
            // Menu_Financial_Receipt_List
            // 
            this.Menu_Financial_Receipt_List.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Receipt_List.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Financial_Receipt_List.Image")));
            this.Menu_Financial_Receipt_List.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Financial_Receipt_List.Name = "Menu_Financial_Receipt_List";
            this.Menu_Financial_Receipt_List.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Receipt_List.Text = "Danh sách phiếu thu";
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.ForeColor = System.Drawing.Color.Navy;
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(258, 6);
            // 
            // Menu_Financial_Dept_Begin
            // 
            this.Menu_Financial_Dept_Begin.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Dept_Begin.Name = "Menu_Financial_Dept_Begin";
            this.Menu_Financial_Dept_Begin.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Dept_Begin.Text = "Công nợ đầu kỳ";
            // 
            // toolStripSeparator46
            // 
            this.toolStripSeparator46.Name = "toolStripSeparator46";
            this.toolStripSeparator46.Size = new System.Drawing.Size(258, 6);
            // 
            // Menu_Financial_Reports
            // 
            this.Menu_Financial_Reports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Financial_Report_Dept,
            this.Menu_Financial_Report_BillDetail});
            this.Menu_Financial_Reports.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Reports.Name = "Menu_Financial_Reports";
            this.Menu_Financial_Reports.ShowShortcutKeys = false;
            this.Menu_Financial_Reports.Size = new System.Drawing.Size(261, 32);
            this.Menu_Financial_Reports.Text = "Thống kê && Báo cáo";
            // 
            // Menu_Financial_Report_Dept
            // 
            this.Menu_Financial_Report_Dept.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Report_Dept.Name = "Menu_Financial_Report_Dept";
            this.Menu_Financial_Report_Dept.Size = new System.Drawing.Size(131, 22);
            this.Menu_Financial_Report_Dept.Text = "Công nợ";
            // 
            // Menu_Financial_Report_BillDetail
            // 
            this.Menu_Financial_Report_BillDetail.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Financial_Report_BillDetail.Name = "Menu_Financial_Report_BillDetail";
            this.Menu_Financial_Report_BillDetail.Size = new System.Drawing.Size(131, 22);
            this.Menu_Financial_Report_BillDetail.Text = "Dòng tiền";
            // 
            // MenuMain_Help
            // 
            this.MenuMain_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hướngDẫnSửDụngToolStripMenuItem,
            this.Menu_Help_AboutUs});
            this.MenuMain_Help.ForeColor = System.Drawing.Color.Navy;
            this.MenuMain_Help.Image = ((System.Drawing.Image)(resources.GetObject("MenuMain_Help.Image")));
            this.MenuMain_Help.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.MenuMain_Help.Name = "MenuMain_Help";
            this.MenuMain_Help.Size = new System.Drawing.Size(94, 30);
            this.MenuMain_Help.Text = "Trợ giúp";
            // 
            // hướngDẫnSửDụngToolStripMenuItem
            // 
            this.hướngDẫnSửDụngToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.hướngDẫnSửDụngToolStripMenuItem.Name = "hướngDẫnSửDụngToolStripMenuItem";
            this.hướngDẫnSửDụngToolStripMenuItem.Size = new System.Drawing.Size(196, 24);
            this.hướngDẫnSửDụngToolStripMenuItem.Text = "Hướng dẫn sử dụng";
            // 
            // Menu_Help_AboutUs
            // 
            this.Menu_Help_AboutUs.ForeColor = System.Drawing.Color.Navy;
            this.Menu_Help_AboutUs.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Help_AboutUs.Image")));
            this.Menu_Help_AboutUs.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Menu_Help_AboutUs.Name = "Menu_Help_AboutUs";
            this.Menu_Help_AboutUs.Size = new System.Drawing.Size(196, 24);
            this.Menu_Help_AboutUs.Text = "Thông tin sản phẩm";
            this.Menu_Help_AboutUs.Click += new System.EventHandler(this.Menu_Help_AboutUs_Click);
            // 
            // tnGridView1
            // 
            this.tnGridView1.Location = new System.Drawing.Point(8, 42);
            this.tnGridView1.Name = "tnGridView1";
            //this.tnGridView1.Parent = this;
            this.tnGridView1.Size = new System.Drawing.Size(425, 282);
            this.tnGridView1.TabIndex = 13;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1276, 355);
            this.Controls.Add(this.tnGridView1);
            this.Controls.Add(this.mdiTabStrip1);
            this.Controls.Add(this.MenuMain);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.MenuMain;
            this.Name = "FrmMain";
            this.Text = "Enterprise Management System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mdiTabStrip1)).EndInit();
            this.MenuMain.ResumeLayout(false);
            this.MenuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusLogin;
        private System.Windows.Forms.ToolStripStatusLabel txtLanguage;
        private System.Windows.Forms.ToolStripStatusLabel lblUserName;
        private System.Windows.Forms.ToolStripStatusLabel lblDataBase;
        private MdiTabStrip.MdiTabStrip mdiTabStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_System;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_CompanyInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_FinancialYears;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_ConnectDatabase;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_Preferences;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_Users;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_List;
        private System.Windows.Forms.ToolStripMenuItem Menu_List_Product_Categories;
        private System.Windows.Forms.ToolStripMenuItem Menu_List_Product_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_List_Products;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_CRM;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Vendor_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Vendor_List;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Vendor_Categories;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Customer_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Customer_List;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Customer_Categories;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Task_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Task_List;
        private System.Windows.Forms.MenuStrip MenuMain;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator37;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_HRM;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Department;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Employees;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Inventory;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Warehouse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Input_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Input_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator35;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Output_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Output_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator36;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_Begin;
        private System.Windows.Forms.ToolStripMenuItem Menu_Inventory_List;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Financial;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Purchase_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Purchase_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator40;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Sale_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Sale_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Payment_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Payment_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Receipt_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Receipt_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Dept_Begin;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator39;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Contract_AddNew;
        private System.Windows.Forms.ToolStripMenuItem Menu_crm_Contract_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator38;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Timekeeping;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Payroll;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Payroll_List;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator46;
        private System.Windows.Forms.ToolStripMenuItem MenuMain_Help;
        private System.Windows.Forms.ToolStripMenuItem hướngDẫnSửDụngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_AboutUs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_Logout;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_Exit;
        private System.Windows.Forms.ToolStripMenuItem Menu_hrm_Branch;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Reports;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Report_Dept;
        private System.Windows.Forms.ToolStripMenuItem Menu_Financial_Report_BillDetail;
        private System.Windows.Forms.ToolStripMenuItem Menu_System_ChangPass;
        private TNLibrary.SYS.Forms.TNGridView tnGridView1;
    }
}