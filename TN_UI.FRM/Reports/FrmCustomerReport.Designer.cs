﻿namespace TN_UI.FRM.Reports
{
    partial class FrmCustomerReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomerReport));
            this.ImgTreeView = new System.Windows.Forms.ImageList(this.components);
            this.txtTitle = new System.Windows.Forms.Label();
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.LargeImageLV = new System.Windows.Forms.ImageList(this.components);
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageGV = new System.Windows.Forms.ImageList(this.components);
            this.panelTitleCenter.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImgTreeView
            // 
            this.ImgTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgTreeView.ImageStream")));
            this.ImgTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgTreeView.Images.SetKeyName(0, "Icon_Home.png");
            this.ImgTreeView.Images.SetKeyName(1, "icon_ListEmp.png");
            this.ImgTreeView.Images.SetKeyName(2, "Icon_TreeViewOpen.png");
            this.ImgTreeView.Images.SetKeyName(3, "Icon_TreeView.png");
            this.ImgTreeView.Images.SetKeyName(4, "Icon_TreeViewRoot.png");
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(398, 5);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(220, 19);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "DANH SÁCH KHÁCH HÀNG";
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.DisplayGroupTree = false;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 29);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.ShowCloseButton = false;
            this.crystalReportViewer1.ShowExportButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(1077, 524);
            this.crystalReportViewer1.TabIndex = 62;
            this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Controls.Add(this.txtTitle);
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(0, 0);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(1077, 29);
            this.panelTitleCenter.TabIndex = 47;
            // 
            // LargeImageLV
            // 
            this.LargeImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("LargeImageLV.ImageStream")));
            this.LargeImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.LargeImageLV.Images.SetKeyName(0, "Icon_ListViewLarge.png");
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "bill.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // imageGV
            // 
            this.imageGV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageGV.ImageStream")));
            this.imageGV.TransparentColor = System.Drawing.Color.Transparent;
            this.imageGV.Images.SetKeyName(0, "bkHeader.png");
            // 
            // FrmCustomerReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 553);
            this.Controls.Add(this.crystalReportViewer1);
            this.Controls.Add(this.panelTitleCenter);
            this.Name = "FrmCustomerReport";
            this.Text = "FrmCustomerReport";
            this.Load += new System.EventHandler(this.FrmCustomerReport_Load);
            this.panelTitleCenter.ResumeLayout(false);
            this.panelTitleCenter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImgTreeView;
        private System.Windows.Forms.Label txtTitle;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.ImageList LargeImageLV;
        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageGV;
    }
}