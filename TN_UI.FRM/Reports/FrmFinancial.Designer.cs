namespace TN_UI.FRM.Reports
{
    partial class FrmFinancial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFinancial));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ImgTreeView = new System.Windows.Forms.ImageList(this.components);
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panelLeft = new System.Windows.Forms.Panel();
            this.CalendarFinancial = new System.Windows.Forms.MonthCalendar();
            this.TreeViewData = new System.Windows.Forms.TreeView();
            this.panelLineLeft = new System.Windows.Forms.Panel();
            this.panelViewLeft = new System.Windows.Forms.Panel();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.txtTitleLeft = new System.Windows.Forms.Label();
            this.cmdHide = new System.Windows.Forms.PictureBox();
            this.cmdUnhide = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.panelHead = new System.Windows.Forms.Panel();
            this.txtTitle = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MenuCalendarRoom = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuCalendar_Booking = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuCalendar_CheckIn = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuCalendar_CheckOut = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtTitleMonth = new System.Windows.Forms.Label();
            this.cmdNextWeek = new System.Windows.Forms.PictureBox();
            this.cmdLastWeek = new System.Windows.Forms.PictureBox();
            this.panelLeft.SuspendLayout();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).BeginInit();
            this.panelHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.MenuCalendarRoom.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLastWeek)).BeginInit();
            this.SuspendLayout();
            // 
            // ImgTreeView
            // 
            this.ImgTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgTreeView.ImageStream")));
            this.ImgTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgTreeView.Images.SetKeyName(0, "home.png");
            this.ImgTreeView.Images.SetKeyName(1, "avalaibity_26.png");
            this.ImgTreeView.Images.SetKeyName(2, "inventory_categories_ok_26.png");
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "send_box_20.png");
            this.SmallImageLV.Images.SetKeyName(1, "product_finished_20.png");
            this.SmallImageLV.Images.SetKeyName(2, "product5.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(192)))), ((int)(((byte)(232)))));
            this.panelLeft.Controls.Add(this.CalendarFinancial);
            this.panelLeft.Controls.Add(this.TreeViewData);
            this.panelLeft.Controls.Add(this.panelLineLeft);
            this.panelLeft.Controls.Add(this.panelViewLeft);
            this.panelLeft.Controls.Add(this.panelTitle);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 59);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(250, 566);
            this.panelLeft.TabIndex = 2;
            // 
            // CalendarFinancial
            // 
            this.CalendarFinancial.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.CalendarFinancial.Location = new System.Drawing.Point(15, 40);
            this.CalendarFinancial.Name = "CalendarFinancial";
            this.CalendarFinancial.TabIndex = 9;
            this.CalendarFinancial.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.CalendarFinancial_DateChanged);
            // 
            // TreeViewData
            // 
            this.TreeViewData.AllowDrop = true;
            this.TreeViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeViewData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeViewData.ForeColor = System.Drawing.Color.Navy;
            this.TreeViewData.Location = new System.Drawing.Point(0, 29);
            this.TreeViewData.Name = "TreeViewData";
            this.TreeViewData.Size = new System.Drawing.Size(250, 277);
            this.TreeViewData.TabIndex = 8;
            // 
            // panelLineLeft
            // 
            this.panelLineLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelLineLeft.BackgroundImage")));
            this.panelLineLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLineLeft.Location = new System.Drawing.Point(0, 306);
            this.panelLineLeft.Name = "panelLineLeft";
            this.panelLineLeft.Size = new System.Drawing.Size(250, 20);
            this.panelLineLeft.TabIndex = 7;
            // 
            // panelViewLeft
            // 
            this.panelViewLeft.BackColor = System.Drawing.Color.White;
            this.panelViewLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelViewLeft.Location = new System.Drawing.Point(0, 326);
            this.panelViewLeft.Name = "panelViewLeft";
            this.panelViewLeft.Size = new System.Drawing.Size(250, 240);
            this.panelViewLeft.TabIndex = 6;
            // 
            // panelTitle
            // 
            this.panelTitle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitle.BackgroundImage")));
            this.panelTitle.Controls.Add(this.txtTitleLeft);
            this.panelTitle.Controls.Add(this.cmdHide);
            this.panelTitle.Controls.Add(this.cmdUnhide);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(250, 29);
            this.panelTitle.TabIndex = 5;
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);
            // 
            // txtTitleLeft
            // 
            this.txtTitleLeft.AutoSize = true;
            this.txtTitleLeft.BackColor = System.Drawing.Color.Transparent;
            this.txtTitleLeft.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitleLeft.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitleLeft.Location = new System.Drawing.Point(12, 5);
            this.txtTitleLeft.Name = "txtTitleLeft";
            this.txtTitleLeft.Size = new System.Drawing.Size(36, 18);
            this.txtTitleLeft.TabIndex = 4;
            this.txtTitleLeft.Text = "     . ";
            // 
            // cmdHide
            // 
            this.cmdHide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHide.BackgroundImage")));
            this.cmdHide.Location = new System.Drawing.Point(223, 0);
            this.cmdHide.Name = "cmdHide";
            this.cmdHide.Size = new System.Drawing.Size(27, 28);
            this.cmdHide.TabIndex = 3;
            this.cmdHide.TabStop = false;
            this.cmdHide.MouseLeave += new System.EventHandler(this.cmdHide_MouseLeave);
            this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
            this.cmdHide.MouseEnter += new System.EventHandler(this.cmdHide_MouseEnter);
            // 
            // cmdUnhide
            // 
            this.cmdUnhide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhide.BackgroundImage")));
            this.cmdUnhide.Location = new System.Drawing.Point(1, 0);
            this.cmdUnhide.Name = "cmdUnhide";
            this.cmdUnhide.Size = new System.Drawing.Size(27, 28);
            this.cmdUnhide.TabIndex = 2;
            this.cmdUnhide.TabStop = false;
            this.cmdUnhide.Visible = false;
            this.cmdUnhide.MouseLeave += new System.EventHandler(this.cmdUnhide_MouseLeave);
            this.cmdUnhide.Click += new System.EventHandler(this.cmdUnhide_Click);
            this.cmdUnhide.MouseEnter += new System.EventHandler(this.cmdUnhide_MouseEnter);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(127)))), ((int)(((byte)(179)))));
            this.splitter1.Location = new System.Drawing.Point(250, 59);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 566);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.ForeColor = System.Drawing.Color.Navy;
            this.textBox12.Location = new System.Drawing.Point(87, 289);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(157, 23);
            this.textBox12.TabIndex = 24;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Navy;
            this.label20.Location = new System.Drawing.Point(6, 291);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 16);
            this.label20.TabIndex = 23;
            this.label20.Text = "Email";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.ForeColor = System.Drawing.Color.Navy;
            this.textBox13.Location = new System.Drawing.Point(87, 165);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(157, 23);
            this.textBox13.TabIndex = 22;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(6, 168);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 21;
            this.label21.Text = "Region";
            // 
            // comboBox3
            // 
            this.comboBox3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(87, 135);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(157, 22);
            this.comboBox3.TabIndex = 20;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.ForeColor = System.Drawing.Color.Navy;
            this.textBox14.Location = new System.Drawing.Point(87, 258);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(157, 23);
            this.textBox14.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(6, 230);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 16);
            this.label22.TabIndex = 18;
            this.label22.Text = "Home Phone";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.ForeColor = System.Drawing.Color.Navy;
            this.textBox15.Location = new System.Drawing.Point(87, 196);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(157, 23);
            this.textBox15.TabIndex = 17;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(6, 197);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(42, 16);
            this.label23.TabIndex = 16;
            this.label23.Text = "Postal";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.ForeColor = System.Drawing.Color.Navy;
            this.textBox16.Location = new System.Drawing.Point(87, 227);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(157, 23);
            this.textBox16.TabIndex = 17;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(6, 260);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 16);
            this.label24.TabIndex = 16;
            this.label24.Text = "Mobile";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(6, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 16);
            this.label25.TabIndex = 14;
            this.label25.Text = "Country";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.ForeColor = System.Drawing.Color.Navy;
            this.textBox17.Location = new System.Drawing.Point(87, 104);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(157, 23);
            this.textBox17.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Navy;
            this.label26.Location = new System.Drawing.Point(6, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 16);
            this.label26.TabIndex = 12;
            this.label26.Text = "City";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.ForeColor = System.Drawing.Color.Navy;
            this.textBox18.Location = new System.Drawing.Point(87, 14);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox18.Size = new System.Drawing.Size(157, 82);
            this.textBox18.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(6, 17);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 16);
            this.label27.TabIndex = 10;
            this.label27.Text = "Address";
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.ForeColor = System.Drawing.Color.Navy;
            this.textBox19.Location = new System.Drawing.Point(87, 289);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(157, 23);
            this.textBox19.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(6, 291);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(39, 16);
            this.label28.TabIndex = 23;
            this.label28.Text = "Email";
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.ForeColor = System.Drawing.Color.Navy;
            this.textBox20.Location = new System.Drawing.Point(87, 165);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(157, 23);
            this.textBox20.TabIndex = 22;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Navy;
            this.label29.Location = new System.Drawing.Point(6, 168);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "Region";
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(87, 135);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(157, 22);
            this.comboBox4.TabIndex = 20;
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.ForeColor = System.Drawing.Color.Navy;
            this.textBox21.Location = new System.Drawing.Point(87, 258);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(157, 23);
            this.textBox21.TabIndex = 19;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Navy;
            this.label30.Location = new System.Drawing.Point(6, 230);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(80, 16);
            this.label30.TabIndex = 18;
            this.label30.Text = "Home Phone";
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.ForeColor = System.Drawing.Color.Navy;
            this.textBox22.Location = new System.Drawing.Point(87, 196);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(157, 23);
            this.textBox22.TabIndex = 17;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Navy;
            this.label31.Location = new System.Drawing.Point(6, 197);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 16;
            this.label31.Text = "Postal";
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Navy;
            this.textBox23.Location = new System.Drawing.Point(87, 227);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(157, 23);
            this.textBox23.TabIndex = 17;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Navy;
            this.label32.Location = new System.Drawing.Point(6, 260);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 16);
            this.label32.TabIndex = 16;
            this.label32.Text = "Mobile";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(6, 139);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 16);
            this.label33.TabIndex = 14;
            this.label33.Text = "Country";
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.ForeColor = System.Drawing.Color.Navy;
            this.textBox24.Location = new System.Drawing.Point(87, 104);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(157, 23);
            this.textBox24.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(6, 105);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 16);
            this.label34.TabIndex = 12;
            this.label34.Text = "City";
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox25.ForeColor = System.Drawing.Color.Navy;
            this.textBox25.Location = new System.Drawing.Point(87, 14);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox25.Size = new System.Drawing.Size(157, 82);
            this.textBox25.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Navy;
            this.label35.Location = new System.Drawing.Point(6, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 10;
            this.label35.Text = "Address";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(255, 59);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(723, 29);
            this.panelTitleCenter.TabIndex = 18;
            // 
            // panelHead
            // 
            this.panelHead.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelHead.BackgroundImage")));
            this.panelHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHead.Controls.Add(this.txtTitle);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(978, 59);
            this.panelHead.TabIndex = 0;
            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(317, 23);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(196, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "TÌNH HÌNH THU CHI";
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.MenuCalendarRoom;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView1.Location = new System.Drawing.Point(255, 141);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.Size = new System.Drawing.Size(723, 313);
            this.dataGridView1.TabIndex = 19;
            // 
            // MenuCalendarRoom
            // 
            this.MenuCalendarRoom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuCalendar_Booking,
            this.MenuCalendar_CheckIn,
            this.MenuCalendar_CheckOut});
            this.MenuCalendarRoom.Name = "MenuCalendarRoom";
            this.MenuCalendarRoom.Size = new System.Drawing.Size(132, 70);
            // 
            // MenuCalendar_Booking
            // 
            this.MenuCalendar_Booking.Font = new System.Drawing.Font("Arial", 9F);
            this.MenuCalendar_Booking.ForeColor = System.Drawing.Color.Navy;
            this.MenuCalendar_Booking.Name = "MenuCalendar_Booking";
            this.MenuCalendar_Booking.Size = new System.Drawing.Size(131, 22);
            this.MenuCalendar_Booking.Text = "Booking";
            // 
            // MenuCalendar_CheckIn
            // 
            this.MenuCalendar_CheckIn.Font = new System.Drawing.Font("Arial", 9F);
            this.MenuCalendar_CheckIn.ForeColor = System.Drawing.Color.Navy;
            this.MenuCalendar_CheckIn.Name = "MenuCalendar_CheckIn";
            this.MenuCalendar_CheckIn.Size = new System.Drawing.Size(131, 22);
            this.MenuCalendar_CheckIn.Text = "Check In";
            // 
            // MenuCalendar_CheckOut
            // 
            this.MenuCalendar_CheckOut.Font = new System.Drawing.Font("Arial", 9F);
            this.MenuCalendar_CheckOut.ForeColor = System.Drawing.Color.Navy;
            this.MenuCalendar_CheckOut.Name = "MenuCalendar_CheckOut";
            this.MenuCalendar_CheckOut.Size = new System.Drawing.Size(131, 22);
            this.MenuCalendar_CheckOut.Text = "Check Out";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.txtTitleMonth);
            this.panel4.Controls.Add(this.cmdNextWeek);
            this.panel4.Controls.Add(this.cmdLastWeek);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(255, 88);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(723, 53);
            this.panel4.TabIndex = 21;
            // 
            // txtTitleMonth
            // 
            this.txtTitleMonth.AutoSize = true;
            this.txtTitleMonth.BackColor = System.Drawing.Color.Transparent;
            this.txtTitleMonth.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitleMonth.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitleMonth.Location = new System.Drawing.Point(85, 15);
            this.txtTitleMonth.Name = "txtTitleMonth";
            this.txtTitleMonth.Size = new System.Drawing.Size(92, 17);
            this.txtTitleMonth.TabIndex = 8;
            this.txtTitleMonth.Text = "Time Record";
            // 
            // cmdNextWeek
            // 
            this.cmdNextWeek.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdNextWeek.BackgroundImage")));
            this.cmdNextWeek.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdNextWeek.Location = new System.Drawing.Point(41, 11);
            this.cmdNextWeek.Name = "cmdNextWeek";
            this.cmdNextWeek.Size = new System.Drawing.Size(24, 24);
            this.cmdNextWeek.TabIndex = 7;
            this.cmdNextWeek.TabStop = false;
            this.cmdNextWeek.Click += new System.EventHandler(this.cmdNextWeek_Click);
            // 
            // cmdLastWeek
            // 
            this.cmdLastWeek.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdLastWeek.BackgroundImage")));
            this.cmdLastWeek.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdLastWeek.Location = new System.Drawing.Point(12, 11);
            this.cmdLastWeek.Name = "cmdLastWeek";
            this.cmdLastWeek.Size = new System.Drawing.Size(24, 24);
            this.cmdLastWeek.TabIndex = 6;
            this.cmdLastWeek.TabStop = false;
            this.cmdLastWeek.Click += new System.EventHandler(this.cmdLastWeek_Click);
            // 
            // FrmFinancial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 625);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panelTitleCenter);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelHead);
            this.Name = "FrmFinancial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tình hình thu chi";
            this.Load += new System.EventHandler(this.FrmCalendarRoom_Load);
            this.panelLeft.ResumeLayout(false);
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).EndInit();
            this.panelHead.ResumeLayout(false);
            this.panelHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.MenuCalendarRoom.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNextWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLastWeek)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.ImageList ImgTreeView;
        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Panel panelLeft;
        public System.Windows.Forms.TreeView TreeViewData;
        private System.Windows.Forms.Panel panelLineLeft;
        private System.Windows.Forms.Panel panelViewLeft;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.PictureBox cmdHide;
        private System.Windows.Forms.PictureBox cmdUnhide;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MonthCalendar CalendarFinancial;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip MenuCalendarRoom;
        private System.Windows.Forms.ToolStripMenuItem MenuCalendar_Booking;
        private System.Windows.Forms.ToolStripMenuItem MenuCalendar_CheckIn;
        private System.Windows.Forms.ToolStripMenuItem MenuCalendar_CheckOut;
        private System.Windows.Forms.Label txtTitleLeft;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label txtTitleMonth;
        private System.Windows.Forms.PictureBox cmdNextWeek;
        private System.Windows.Forms.PictureBox cmdLastWeek;





    }
}