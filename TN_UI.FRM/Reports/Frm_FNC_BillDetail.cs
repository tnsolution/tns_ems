using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC;

using TNLibrary.FNC.Bills;

namespace TN_UI.FRM.Reports
{
    public partial class Frm_FNC_BillDetail : Form
    {
        public string mPaymentID = "";
        public bool FlagView;
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mCloseMonth = "";

        public Frm_FNC_BillDetail()
        {
            InitializeComponent();
        }

        private void FrmBillPaymentList_Load(object sender, EventArgs e)
        {
            CheckRole();

            FinancialYear_Info nYear = new FinancialYear_Info(true);
            mCloseMonth = nYear.FromMonth;

            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            PickerToDay.CustomFormat = mFormatDate;
            PickerFromDay.CustomFormat = mFormatDate;
            PickerFromDay.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            panelSearch.Height = 90;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
            SetupLayoutGridView_Day(dataGridView1);

            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;
        }
        private void SearchPayments()
        {
            mRole.Check_Role("SYS006");
            DataTable nTable = new DataTable();

            nTable = Cash_Bills_Data.Cash_Bills_Branchs(mCloseMonth, PickerFromDay.Value, PickerToDay.Value);

            PopulateDataGridView_Month(dataGridView1, nTable);

        }

        #region [ GridView ]
        private void SetupLayoutGridView_Day(DataGridView GV)
        {
            GV.Columns.Clear();

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("BillDate", "Ngày");
            GV.Columns.Add("ReceiptAmount", "Số tiền thu");
            GV.Columns.Add("PaymentAmount", "Số tiền chi");
            GV.Columns.Add("Balance", "Tồn quỹ");

            GV.Columns[0].Width = 60;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[2].Width = 150;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[3].Width = 150;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 150;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);

            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            //GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * (3 / 2);

            GV.Dock = DockStyle.Fill;
            //Activate On Gridview
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.RowLeave += new DataGridViewCellEventHandler(GV_RowLeave);

            GV.Rows.Add(100);
        }
        private void PopulateDataGridView_Day(DataGridView GV, DataTable TableView)
        {
            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            double nBalance = 0;
            double nSumPayment = 0, nSumReceipt = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];
                DateTime nBillDate = (DateTime)nRow["BillDate"];
                double nPaymentAmount = double.Parse(nRow["PaymentAmount"].ToString());
                double nReceiptAmount = double.Parse(nRow["ReceiptAmount"].ToString());

                nSumPayment += nPaymentAmount;
                nSumReceipt += nReceiptAmount;

                if (i == 0)
                    nBalance = double.Parse(nRow["Balance"].ToString());
                else
                    nBalance = nBalance + nReceiptAmount - nPaymentAmount;


                GV.Rows.Add();
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
                GV.Rows[i].Cells["BillDate"].Value = nBillDate.ToString(mFormatDate);

                GV.Rows[i].Cells["PaymentAmount"].Value = nPaymentAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["ReceiptAmount"].Value = nReceiptAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["Balance"].Value = nBalance.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            }
            GV.Rows.Add();

            GV.Rows[i].DefaultCellStyle.BackColor = Color.Azure;
            GV.Rows[i].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
            GV.Rows[i].Cells["BillDate"].Value = "Tổng cộng";

            GV.Rows[i].Cells["PaymentAmount"].Value = nSumPayment.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            GV.Rows[i].Cells["ReceiptAmount"].Value = nSumReceipt.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            // GV.Rows[i].Cells["Balance"].Value = nBalance.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

        }

        private void SetupLayoutGridView_Month(DataGridView GV)
        {
            GV.Columns.Clear();

            GV.Columns.Add("No", "STT");
            GV.Columns.Add("BranchName", "Chi nhánh");
            GV.Columns.Add("BeginAmount", "Tồn quỹ đầu kỳ");
            GV.Columns.Add("ReceiptAmount", "Số tiền thu");
            GV.Columns.Add("PaymentAmount", "Số tiền chi");
            GV.Columns.Add("EndAmount", "Tồn quỹ cuối kỳ");

            GV.Columns[0].Width = 60;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[1].Width = 200;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns[2].Width = 150;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[3].Width = 150;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 150;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 150;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            //GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * (3 / 2);

            GV.Dock = DockStyle.Fill;
            //Activate On Gridview
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.RowLeave += new DataGridViewCellEventHandler(GV_RowLeave);

            GV.Rows.Add(100);
        }
        private void PopulateDataGridView_Month(DataGridView GV, DataTable TableView)
        {
            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;

            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];
                double nBeginAmount = double.Parse(nRow["BeginAmount"].ToString());
                double nPaymentAmount = double.Parse(nRow["PaymentAmount"].ToString());
                double nReceiptAmount = double.Parse(nRow["ReceiptAmount"].ToString());
                double nEndAmount = nBeginAmount + nReceiptAmount - nPaymentAmount;

                GV.Rows.Add();
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
                GV.Rows[i].Cells["BranchName"].Value = nRow["BranchName"].ToString();

                GV.Rows[i].Cells["BeginAmount"].Value = nBeginAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["PaymentAmount"].Value = nPaymentAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["ReceiptAmount"].Value = nReceiptAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["EndAmount"].Value = nEndAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            }

        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void GV_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;


        }

        #endregion

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 90;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        #region  [ Button Activate ]


        private void cmdSearch_Click(object sender, EventArgs e)
        {

            SearchPayments();
        }



        #endregion

        #region [ Sercutiry ]
        private bool mRoleDelete = false;
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC003");
            if (mRole.Del)
            {
                mRoleDelete = true;
            }
        }
        #endregion
    }
}