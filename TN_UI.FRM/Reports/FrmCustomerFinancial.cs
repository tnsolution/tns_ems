using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.CRM;
using TNLibrary.FNC.Bills;
using TNLibrary.FNC.Invoices;
using TNLibrary.IVT;
using TNLibrary.FNC;

namespace TN_UI.FRM.Reports
{
    public partial class FrmCustomerFinancial : Form
    {
        public int mCustomerKey = 0;
        string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        string mFormatDate = GlobalSystemConfig.FormatDate;
        private int mFinancialYear = 0;
        public FrmCustomerFinancial()
        {
            InitializeComponent();
        }

        private void FrmCustomerFinancial_Load(object sender, EventArgs e)
        {
            FinancialYear_Info nFinancialYear = new FinancialYear_Info(true);
            mFinancialYear = nFinancialYear.FromDate.Year;

            InitListViewReceipts(ListViewReceipts);
            LoadDataReciepts(ListViewReceipts, Cash_Bills_Data.ReceiptList(mCustomerKey, mFinancialYear));
            LoadChartsReceipts();

            InitListViewPayments(ListViewPayments);
            LoadDataPayments(ListViewPayments, Cash_Bills_Data.PaymentList(mCustomerKey, mFinancialYear));
            LoadChartsPayment();

            InitListViewInvoicesPurchase(ListViewInvoicePurchase);
            LoadDataInvoicesPurchase(ListViewInvoicePurchase, Invoice_Data.PurchaseInvoices(mCustomerKey, mFinancialYear));
            LoadChartsInvoicePurchase();

            InitListViewInvoicesSale(ListViewInvoiceSale);
            LoadDataInvoicesSale(ListViewInvoiceSale, Invoice_Data.SaleInvoices(mCustomerKey, mFinancialYear));
            LoadChartsInvoiceSale();

            InitListViewInventory(ListView_Inventory);
            LoadDataInventory(ListView_Inventory, Stock_Note_Data.Inventory(mCustomerKey, mFinancialYear));

            LoadReport();
        }

        #region [ Receipts ]
        private void InitListViewReceipts(ListView LV)
        {
            ColumnHeader colHead;
            // First header
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            // Second header
            colHead = new ColumnHeader();
            colHead.Text = "Mã phiếu thu";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            // Third header
            colHead = new ColumnHeader();
            colHead.Text = "Diễn giải";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng tiền";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "";
            colHead.Width = 230;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);
        }
        public void LoadDataReciepts(ListView LV, DataTable nTable)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = nTable.Rows.Count;
            int No = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nTable.Rows[i];
                No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = No.ToString();
                lvi.Tag = nRow["ReceiptKey"]; // Set the tag to 

                lvi.ImageIndex = 0;
                DateTime nReceiptDate = (DateTime)nRow["ReceiptDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nReceiptDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ReceiptID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ReceiptDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text ="";
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }
          

        }
        private void LoadChartsReceipts()
        {
            int nSpace = 70;
            int nLineX = 170;
            // Load Database
            ArrayList nMonths = new ArrayList();
            double nMax = 0;
            nMonths.Add(0);
            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth = Cash_Bills_Data.Receipt_Customer_TotalAmount(mCustomerKey, i, mFinancialYear);
                if (nMax < nTotalMonth)
                    nMax = nTotalMonth;

                nMonths.Add(nTotalMonth);
            }
            if (nMax == 0)
                nMax = 1;

            // Drawing
            Panel nLine = new Panel();
            nLine.Width = 1200;
            nLine.Height = 1;
            nLine.Top = nLineX;
            nLine.Left = 0;
            nLine.BackColor = Color.Navy;

            Label strMonthName = new Label();
            strMonthName.Text = "Tháng";
            strMonthName.Font = new Font("Arial", 9);

            strMonthName.Top = nLineX + 8;
            strMonthName.Width = 50;
            strMonthName.Height = 16;
            strMonthName.Left = 10;
            strMonthName.TextAlign = ContentAlignment.MiddleLeft;

            ChartsReceipt.Controls.Add(nLine);
            ChartsReceipt.Controls.Add(strMonthName);

            int k = 1;
            int BeginCol = 1;
            for (k = 1; k <= 12; k++)
            {
                if ((double)nMonths[k] > 0)
                    break;
            }
            for (int i = k; i <= 12; i++)
            {
                double nTotalMonth_i = (double)nMonths[i];
                int nWidth = 20;
                int nLeftCol = ((nWidth + nSpace) * BeginCol) - 20;
                int nHeight = Convert.ToInt32((nTotalMonth_i * 150) / nMax);

                if (nHeight > 0)
                {
                    Label strTotal = new Label();
                    strTotal.Text = nTotalMonth_i.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                    strTotal.Font = new Font("Arial", 8);

                    strTotal.Top = nLineX - nHeight - 15;
                    strTotal.Width = nWidth + nSpace - 5;
                    strTotal.Left = (nLeftCol + (nWidth / 2)) - strTotal.Width / 2;
                    strTotal.Height = 16;
                    strTotal.ForeColor = Color.Maroon;
                    //  strTotal.BackColor = Color.DeepPink;
                    strTotal.TextAlign = ContentAlignment.MiddleCenter;

                    Panel nCol = new Panel();
                    nCol.Width = nWidth;
                    nCol.Height = nHeight;
                    nCol.Top = nLineX - nHeight;
                    nCol.Left = nLeftCol;
                    nCol.BackColor = Color.DodgerBlue;

                    ChartsReceipt.Controls.Add(nCol);
                    ChartsReceipt.Controls.Add(strTotal);

                }
                Label strMonth = new Label();
                strMonth.Text = i.ToString();
                strMonth.Font = new Font("Arial", 9);

                strMonth.Top = nLineX + 8;
                strMonth.Width = 25;
                strMonth.Height = 16;
                strMonth.Left = nLeftCol;
                strMonth.TextAlign = ContentAlignment.MiddleCenter;
                ChartsReceipt.Controls.Add(strMonth);

                BeginCol++;
            }


        }
        #endregion

        #region [ Payments ]
        private void InitListViewPayments(ListView LV)
        {

            ColumnHeader colHead;
            // First header
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            // Second header
            colHead = new ColumnHeader();
            colHead.Text = "Mã phiếu thu";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            // Third header
            colHead = new ColumnHeader();
            colHead.Text = "Diễn giải";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng tiền";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);


        }
        public void LoadDataPayments(ListView LV, DataTable nTable)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = nTable.Rows.Count;
            int No;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nTable.Rows[i];
                No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = No.ToString();
                lvi.Tag = nRow["PaymentKey"]; // Set the tag to 

                lvi.ImageIndex = 0;
                DateTime nPaymentDate = (DateTime)nRow["PaymentDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nPaymentDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PaymentID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["PaymentDescription"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

            }


        }
        private void LoadChartsPayment()
        {
            int nSpace = 70;
            int nLineX = 170;
            // Load Database
            ArrayList nMonths = new ArrayList();
            double nMax = 0;
            nMonths.Add(0);
            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth = Cash_Bills_Data.Payment_Customer_TotalAmount(mCustomerKey, i, mFinancialYear);
                if (nMax < nTotalMonth)
                    nMax = nTotalMonth;

                nMonths.Add(nTotalMonth);
            }
            if (nMax == 0)
                nMax = 1;

            // Drawing
            Panel nLine = new Panel();
            nLine.Width = 1200;
            nLine.Height = 1;
            nLine.Top = nLineX;
            nLine.Left = 0;
            nLine.BackColor = Color.Navy;

            Label strMonthName = new Label();
            strMonthName.Text = "Tháng";
            strMonthName.Font = new Font("Arial", 9);

            strMonthName.Top = nLineX + 8;
            strMonthName.Width = 50;
            strMonthName.Height = 16;
            strMonthName.Left = 10;
            strMonthName.TextAlign = ContentAlignment.MiddleLeft;

            ChartsPayment.Controls.Add(nLine);
            ChartsPayment.Controls.Add(strMonthName);

            int k = 1;
            int BeginCol = 1;
            for (k = 1; k <= 12; k++)
            {
                if ((double)nMonths[k] > 0)
                    break;
            }
            for (int i = k; i <= 12; i++)
            {
                double nTotalMonth_i = (double)nMonths[i];
                int nWidth = 20;
                int nLeftCol = ((nWidth + nSpace) * BeginCol) - 20;
                int nHeight = Convert.ToInt32((nTotalMonth_i * 150) / nMax);

                if (nHeight > 0)
                {
                    Label strTotal = new Label();
                    strTotal.Text = nTotalMonth_i.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                    strTotal.Font = new Font("Arial", 8);

                    strTotal.Top = nLineX - nHeight - 15;
                    strTotal.Width = nWidth + nSpace - 5;
                    strTotal.Left = (nLeftCol + (nWidth / 2)) - strTotal.Width / 2;
                    strTotal.Height = 16;
                    strTotal.ForeColor = Color.Maroon;
                    //  strTotal.BackColor = Color.DeepPink;
                    strTotal.TextAlign = ContentAlignment.MiddleCenter;

                    Panel nCol = new Panel();
                    nCol.Width = nWidth;
                    nCol.Height = nHeight;
                    nCol.Top = nLineX - nHeight;
                    nCol.Left = nLeftCol;
                    nCol.BackColor = Color.DodgerBlue;

                    ChartsPayment.Controls.Add(nCol);
                    ChartsPayment.Controls.Add(strTotal);

                }
                Label strMonth = new Label();
                strMonth.Text = i.ToString();
                strMonth.Font = new Font("Arial", 9);

                strMonth.Top = nLineX + 8;
                strMonth.Width = 25;
                strMonth.Height = 16;
                strMonth.Left = nLeftCol;
                strMonth.TextAlign = ContentAlignment.MiddleCenter;
                ChartsPayment.Controls.Add(strMonth);

                BeginCol++;
            }


        }
        #endregion

        #region [ Invoice Purchase]
        private void InitListViewInvoicesPurchase(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng giá trị";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thuế xuất";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "VAT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

        }
        public void LoadDataInvoicesPurchase(ListView LV, DataTable Table)
        {
            //----------------------------------------------------------
            int n = Table.Rows.Count;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int No = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = Table.Rows[i];
                No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = No.ToString();
                lvi.Tag = nRow["InvoiceKey"]; // Set the tag to 
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceID"].ToString().Trim();
                lvsi.Tag = nRow["IsFixedAsset"];
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceNumber"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                DateTime nInvoiceDate = (DateTime)nRow["InvoiceDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nInvoiceDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);


                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["VATPercent"].ToString().Trim() + "%";
                lvi.SubItems.Add(lvsi);

                double nAmountVATForTax = double.Parse(nRow["VATForTax"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmountVATForTax.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }


        }
        private void LoadChartsInvoicePurchase()
        {
            int nSpace = 70;
            int nLineX = 170;
            // Load Database
            ArrayList nMonths = new ArrayList();
            double nMax = 0;
            nMonths.Add(0);
            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth = Invoice_Data.Purchase_Customer_TotalAmount(mCustomerKey, i, mFinancialYear);
                if (nMax < nTotalMonth)
                    nMax = nTotalMonth;

                nMonths.Add(nTotalMonth);
            }
            if (nMax == 0)
                nMax = 1;

            // Drawing
            Panel nLine = new Panel();
            nLine.Width = 1200;
            nLine.Height = 1;
            nLine.Top = nLineX;
            nLine.Left = 0;
            nLine.BackColor = Color.Navy;

            Label strMonthName = new Label();
            strMonthName.Text = "Tháng";
            strMonthName.Font = new Font("Arial", 9);

            strMonthName.Top = nLineX + 8;
            strMonthName.Width = 50;
            strMonthName.Height = 16;
            strMonthName.Left = 10;
            strMonthName.TextAlign = ContentAlignment.MiddleLeft;

            ChartsInvoicePurchase.Controls.Add(nLine);
            ChartsInvoicePurchase.Controls.Add(strMonthName);

            int k = 1;
            int BeginCol = 1;
            for (k = 1; k <= 12; k++)
            {
                if ((double)nMonths[k] > 0)
                    break;
            }
            for (int i = k; i <= 12; i++)
            {
                double nTotalMonth_i = (double)nMonths[i];
                int nWidth = 20;
                int nLeftCol = ((nWidth + nSpace) * BeginCol) - 20;
                int nHeight = Convert.ToInt32((nTotalMonth_i * 150) / nMax);

                if (nHeight > 0)
                {
                    Label strTotal = new Label();
                    strTotal.Text = nTotalMonth_i.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                    strTotal.Font = new Font("Arial", 8);

                    strTotal.Top = nLineX - nHeight - 15;
                    strTotal.Width = nWidth + nSpace - 5;
                    strTotal.Left = (nLeftCol + (nWidth / 2)) - strTotal.Width / 2;
                    strTotal.Height = 16;
                    strTotal.ForeColor = Color.Maroon;
                    //  strTotal.BackColor = Color.DeepPink;
                    strTotal.TextAlign = ContentAlignment.MiddleCenter;

                    Panel nCol = new Panel();
                    nCol.Width = nWidth;
                    nCol.Height = nHeight;
                    nCol.Top = nLineX - nHeight;
                    nCol.Left = nLeftCol;
                    nCol.BackColor = Color.DodgerBlue;

                    ChartsInvoicePurchase.Controls.Add(nCol);
                    ChartsInvoicePurchase.Controls.Add(strTotal);

                }
                Label strMonth = new Label();
                strMonth.Text = i.ToString();
                strMonth.Font = new Font("Arial", 9);

                strMonth.Top = nLineX + 8;
                strMonth.Width = 25;
                strMonth.Height = 16;
                strMonth.Left = nLeftCol;
                strMonth.TextAlign = ContentAlignment.MiddleCenter;
                ChartsInvoicePurchase.Controls.Add(strMonth);

                BeginCol++;
            }


        }
        #endregion

        #region [ Invoice Sale ]
        private void InitListViewInvoicesSale(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số hóa đơn";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tổng giá trị";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thuế xuất";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "VAT";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);


        }
        public void LoadDataInvoicesSale(ListView LV, DataTable Table)
        {
            //----------------------------------------------------------
            int n = Table.Rows.Count;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int No = 0;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = Table.Rows[i];
                No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = No.ToString();
                lvi.Tag = nRow["InvoiceKey"]; // Set the tag to 
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["InvoiceNumber"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                DateTime nInvoiceDate = (DateTime)nRow["InvoiceDate"];

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nInvoiceDate.ToString(mFormatDate);
                lvi.SubItems.Add(lvsi);

                double nAmount = double.Parse(nRow["AmountCurrencyMain"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["VATPercent"].ToString().Trim() + "%";
                lvi.SubItems.Add(lvsi);

                double nAmountVATForTax = double.Parse(nRow["VATForTax"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmountVATForTax.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

        }
        private void LoadChartsInvoiceSale()
        {
            int nSpace = 70;
            int nLineX = 170;
            // Load Database
            ArrayList nMonths = new ArrayList();
            double nMax = 0;
            nMonths.Add(0);
            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth = Invoice_Data.Sale_Customer_TotalAmount(mCustomerKey, i, mFinancialYear);
                if (nMax < nTotalMonth)
                    nMax = nTotalMonth;

                nMonths.Add(nTotalMonth);
            }
            if (nMax == 0)
                nMax = 1;

            // Drawing
            Panel nLine = new Panel();
            nLine.Width = 1200;
            nLine.Height = 1;
            nLine.Top = nLineX;
            nLine.Left = 0;
            nLine.BackColor = Color.Navy;

            Label strMonthName = new Label();
            strMonthName.Text = "Tháng";
            strMonthName.Font = new Font("Arial", 9);

            strMonthName.Top = nLineX + 8;
            strMonthName.Width = 50;
            strMonthName.Height = 16;
            strMonthName.Left = 10;
            strMonthName.TextAlign = ContentAlignment.MiddleLeft;

            ChartsInvoiceSale.Controls.Add(nLine);
            ChartsInvoiceSale.Controls.Add(strMonthName);

            int k = 1;
            int BeginCol = 1;
            for (k = 1; k <= 12; k++)
            {
                if ((double)nMonths[k] > 0)
                    break;
            }
            for (int i = k; i <= 12; i++)
            {
                double nTotalMonth_i = (double)nMonths[i];
                int nWidth = 20;
                int nLeftCol = ((nWidth + nSpace) * BeginCol) - 20;
                int nHeight = Convert.ToInt32((nTotalMonth_i * 150) / nMax);

                if (nHeight > 0)
                {
                    Label strTotal = new Label();
                    strTotal.Text = nTotalMonth_i.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                    strTotal.Font = new Font("Arial", 8);

                    strTotal.Top = nLineX - nHeight - 15;
                    strTotal.Width = nWidth + nSpace - 5;
                    strTotal.Left = (nLeftCol + (nWidth / 2)) - strTotal.Width / 2;
                    strTotal.Height = 16;
                    strTotal.ForeColor = Color.Maroon;
                    //  strTotal.BackColor = Color.DeepPink;
                    strTotal.TextAlign = ContentAlignment.MiddleCenter;

                    Panel nCol = new Panel();
                    nCol.Width = nWidth;
                    nCol.Height = nHeight;
                    nCol.Top = nLineX - nHeight;
                    nCol.Left = nLeftCol;
                    nCol.BackColor = Color.DodgerBlue;

                    ChartsInvoiceSale.Controls.Add(nCol);
                    ChartsInvoiceSale.Controls.Add(strTotal);

                }
                Label strMonth = new Label();
                strMonth.Text = i.ToString();
                strMonth.Font = new Font("Arial", 9);

                strMonth.Top = nLineX + 8;
                strMonth.Width = 25;
                strMonth.Height = 16;
                strMonth.Left = nLeftCol;
                strMonth.TextAlign = ContentAlignment.MiddleCenter;
                ChartsInvoiceSale.Controls.Add(strMonth);

                BeginCol++;
            }


        }
        #endregion

        #region [ Inventory]
        private void InitListViewInventory(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sản phẩm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số lượng nhập";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số lượng xuất";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

        }
        public void LoadDataInventory(ListView LV, DataTable nTable)
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            int n = nTable.Rows.Count;

            LV.Items.Clear();
            int No;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = nTable.Rows[i];
                No = i + 1;
                lvi = new ListViewItem();
                lvi.Text = No.ToString();


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                float nIn_Quantity = float.Parse(nRow["In_Quantity"].ToString());
                float nOut_Quantity = float.Parse(nRow["Out_Quantity"].ToString());

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nIn_Quantity.ToString(mFormatDecimal,mFormatProviderDecimal);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nOut_Quantity.ToString(mFormatDecimal, mFormatProviderDecimal);
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }



        }

        #endregion 
       
        #region [ Report ]
        private void LoadReport()
        {
            txtInvoicePurcharses.Text = Invoice_Data.Purchase_Customer_TotalAmount(mCustomerKey, mFinancialYear).ToString(mFormatCurrencyMain,mFormatProviderCurrency);
            txtInvoiceSale.Text = Invoice_Data.Sale_Customer_TotalAmount(mCustomerKey, mFinancialYear).ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            txtPayment.Text = Cash_Bills_Data.Payment_Customer_TotalAmount(mCustomerKey, mFinancialYear).ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            txtReceipt.Text = Cash_Bills_Data.Receipt_Customer_TotalAmount(mCustomerKey, mFinancialYear).ToString(mFormatCurrencyMain, mFormatProviderCurrency);
        }
        #endregion
    }
}