namespace TN_UI.FRM.Reports
{
    partial class FrmCustomerFinancial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomerFinancial));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ListViewReceipts = new System.Windows.Forms.ListView();
            this.ChartsReceipt = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ListViewPayments = new System.Windows.Forms.ListView();
            this.ChartsPayment = new System.Windows.Forms.Panel();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.ListView_Inventory = new System.Windows.Forms.ListView();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ListViewInvoicePurchase = new System.Windows.Forms.ListView();
            this.ChartsInvoicePurchase = new System.Windows.Forms.Panel();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.ListViewInvoiceSale = new System.Windows.Forms.ListView();
            this.ChartsInvoiceSale = new System.Windows.Forms.Panel();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.txtReceipt = new System.Windows.Forms.TextBox();
            this.txtInvoiceSale = new System.Windows.Forms.TextBox();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.txtInvoicePurcharses = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(935, 25);
            this.toolStrip1.TabIndex = 31;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Arial", 8.25F);
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(935, 537);
            this.tabControl1.TabIndex = 32;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ListViewReceipts);
            this.tabPage2.Controls.Add(this.ChartsReceipt);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(927, 510);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Phiếu thu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ListViewReceipts
            // 
            this.ListViewReceipts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListViewReceipts.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewReceipts.ForeColor = System.Drawing.Color.Navy;
            this.ListViewReceipts.FullRowSelect = true;
            this.ListViewReceipts.GridLines = true;
            this.ListViewReceipts.Location = new System.Drawing.Point(0, 0);
            this.ListViewReceipts.MultiSelect = false;
            this.ListViewReceipts.Name = "ListViewReceipts";
            this.ListViewReceipts.ShowGroups = false;
            this.ListViewReceipts.Size = new System.Drawing.Size(927, 310);
            this.ListViewReceipts.TabIndex = 17;
            this.ListViewReceipts.UseCompatibleStateImageBehavior = false;
            this.ListViewReceipts.View = System.Windows.Forms.View.Details;
            // 
            // ChartsReceipt
            // 
            this.ChartsReceipt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChartsReceipt.Location = new System.Drawing.Point(0, 310);
            this.ChartsReceipt.Name = "ChartsReceipt";
            this.ChartsReceipt.Size = new System.Drawing.Size(927, 200);
            this.ChartsReceipt.TabIndex = 18;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ListViewPayments);
            this.tabPage3.Controls.Add(this.ChartsPayment);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(927, 510);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Phiếu chi";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ListViewPayments
            // 
            this.ListViewPayments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListViewPayments.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewPayments.ForeColor = System.Drawing.Color.Navy;
            this.ListViewPayments.FullRowSelect = true;
            this.ListViewPayments.GridLines = true;
            this.ListViewPayments.Location = new System.Drawing.Point(0, 0);
            this.ListViewPayments.MultiSelect = false;
            this.ListViewPayments.Name = "ListViewPayments";
            this.ListViewPayments.ShowGroups = false;
            this.ListViewPayments.Size = new System.Drawing.Size(927, 310);
            this.ListViewPayments.TabIndex = 18;
            this.ListViewPayments.UseCompatibleStateImageBehavior = false;
            this.ListViewPayments.View = System.Windows.Forms.View.Details;
            // 
            // ChartsPayment
            // 
            this.ChartsPayment.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChartsPayment.Location = new System.Drawing.Point(0, 310);
            this.ChartsPayment.Name = "ChartsPayment";
            this.ChartsPayment.Size = new System.Drawing.Size(927, 200);
            this.ChartsPayment.TabIndex = 19;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.ListView_Inventory);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(927, 510);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Xuất - Nhập";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // ListView_Inventory
            // 
            this.ListView_Inventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListView_Inventory.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListView_Inventory.ForeColor = System.Drawing.Color.Navy;
            this.ListView_Inventory.FullRowSelect = true;
            this.ListView_Inventory.GridLines = true;
            this.ListView_Inventory.Location = new System.Drawing.Point(0, 0);
            this.ListView_Inventory.MultiSelect = false;
            this.ListView_Inventory.Name = "ListView_Inventory";
            this.ListView_Inventory.ShowGroups = false;
            this.ListView_Inventory.Size = new System.Drawing.Size(927, 510);
            this.ListView_Inventory.TabIndex = 18;
            this.ListView_Inventory.UseCompatibleStateImageBehavior = false;
            this.ListView_Inventory.View = System.Windows.Forms.View.Details;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.ListViewInvoicePurchase);
            this.tabPage6.Controls.Add(this.ChartsInvoicePurchase);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(927, 510);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Hóa đơn mua hàng";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // ListViewInvoicePurchase
            // 
            this.ListViewInvoicePurchase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListViewInvoicePurchase.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewInvoicePurchase.ForeColor = System.Drawing.Color.Navy;
            this.ListViewInvoicePurchase.FullRowSelect = true;
            this.ListViewInvoicePurchase.GridLines = true;
            this.ListViewInvoicePurchase.Location = new System.Drawing.Point(0, 0);
            this.ListViewInvoicePurchase.MultiSelect = false;
            this.ListViewInvoicePurchase.Name = "ListViewInvoicePurchase";
            this.ListViewInvoicePurchase.ShowGroups = false;
            this.ListViewInvoicePurchase.Size = new System.Drawing.Size(927, 310);
            this.ListViewInvoicePurchase.TabIndex = 19;
            this.ListViewInvoicePurchase.UseCompatibleStateImageBehavior = false;
            this.ListViewInvoicePurchase.View = System.Windows.Forms.View.Details;
            // 
            // ChartsInvoicePurchase
            // 
            this.ChartsInvoicePurchase.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChartsInvoicePurchase.Location = new System.Drawing.Point(0, 310);
            this.ChartsInvoicePurchase.Name = "ChartsInvoicePurchase";
            this.ChartsInvoicePurchase.Size = new System.Drawing.Size(927, 200);
            this.ChartsInvoicePurchase.TabIndex = 20;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.ListViewInvoiceSale);
            this.tabPage7.Controls.Add(this.ChartsInvoiceSale);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(927, 510);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Hóa đơn bán hàng";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // ListViewInvoiceSale
            // 
            this.ListViewInvoiceSale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListViewInvoiceSale.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewInvoiceSale.ForeColor = System.Drawing.Color.Navy;
            this.ListViewInvoiceSale.FullRowSelect = true;
            this.ListViewInvoiceSale.GridLines = true;
            this.ListViewInvoiceSale.Location = new System.Drawing.Point(0, 0);
            this.ListViewInvoiceSale.MultiSelect = false;
            this.ListViewInvoiceSale.Name = "ListViewInvoiceSale";
            this.ListViewInvoiceSale.ShowGroups = false;
            this.ListViewInvoiceSale.Size = new System.Drawing.Size(927, 310);
            this.ListViewInvoiceSale.TabIndex = 19;
            this.ListViewInvoiceSale.UseCompatibleStateImageBehavior = false;
            this.ListViewInvoiceSale.View = System.Windows.Forms.View.Details;
            // 
            // ChartsInvoiceSale
            // 
            this.ChartsInvoiceSale.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChartsInvoiceSale.Location = new System.Drawing.Point(0, 310);
            this.ChartsInvoiceSale.Name = "ChartsInvoiceSale";
            this.ChartsInvoiceSale.Size = new System.Drawing.Size(927, 200);
            this.ChartsInvoiceSale.TabIndex = 20;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.txtReceipt);
            this.tabPage5.Controls.Add(this.txtInvoiceSale);
            this.tabPage5.Controls.Add(this.txtPayment);
            this.tabPage5.Controls.Add(this.txtInvoicePurcharses);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(927, 510);
            this.tabPage5.TabIndex = 7;
            this.tabPage5.Text = "Tổng hợp";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // txtReceipt
            // 
            this.txtReceipt.Font = new System.Drawing.Font("Arial", 9F);
            this.txtReceipt.ForeColor = System.Drawing.Color.Navy;
            this.txtReceipt.Location = new System.Drawing.Point(211, 100);
            this.txtReceipt.Name = "txtReceipt";
            this.txtReceipt.Size = new System.Drawing.Size(256, 21);
            this.txtReceipt.TabIndex = 7;
            this.txtReceipt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtInvoiceSale
            // 
            this.txtInvoiceSale.Font = new System.Drawing.Font("Arial", 9F);
            this.txtInvoiceSale.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoiceSale.Location = new System.Drawing.Point(211, 74);
            this.txtInvoiceSale.Name = "txtInvoiceSale";
            this.txtInvoiceSale.Size = new System.Drawing.Size(256, 21);
            this.txtInvoiceSale.TabIndex = 6;
            this.txtInvoiceSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPayment
            // 
            this.txtPayment.Font = new System.Drawing.Font("Arial", 9F);
            this.txtPayment.ForeColor = System.Drawing.Color.Navy;
            this.txtPayment.Location = new System.Drawing.Point(211, 48);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(256, 21);
            this.txtPayment.TabIndex = 5;
            this.txtPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtInvoicePurcharses
            // 
            this.txtInvoicePurcharses.Font = new System.Drawing.Font("Arial", 9F);
            this.txtInvoicePurcharses.ForeColor = System.Drawing.Color.Navy;
            this.txtInvoicePurcharses.Location = new System.Drawing.Point(211, 22);
            this.txtInvoicePurcharses.Name = "txtInvoicePurcharses";
            this.txtInvoicePurcharses.Size = new System.Drawing.Size(256, 21);
            this.txtInvoicePurcharses.TabIndex = 4;
            this.txtInvoicePurcharses.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(34, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 15);
            this.label10.TabIndex = 3;
            this.label10.Text = "Tổng tiền Phiếu thu";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F);
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(34, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(164, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "Tổng tiền Hóa đơn bán hàng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(34, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Tổng tiền Phiếu chi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(34, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tổng tiền Hóa đơn mua hàng";
            // 
            // FrmCustomerFinancial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 562);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.Name = "FrmCustomerFinancial";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thông tin chi tiết";
            this.Load += new System.EventHandler(this.FrmCustomerFinancial_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        public System.Windows.Forms.ListView ListViewReceipts;
        public System.Windows.Forms.ListView ListViewPayments;
        public System.Windows.Forms.ListView ListView_Inventory;
        public System.Windows.Forms.ListView ListViewInvoicePurchase;
        public System.Windows.Forms.ListView ListViewInvoiceSale;
        private System.Windows.Forms.Panel ChartsReceipt;
        private System.Windows.Forms.Panel ChartsPayment;
        private System.Windows.Forms.Panel ChartsInvoicePurchase;
        private System.Windows.Forms.Panel ChartsInvoiceSale;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtReceipt;
        private System.Windows.Forms.TextBox txtInvoiceSale;
        private System.Windows.Forms.TextBox txtPayment;
        private System.Windows.Forms.TextBox txtInvoicePurcharses;
    }
}