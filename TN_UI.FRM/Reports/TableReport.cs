﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TNLibrary.SYS;

namespace TN_UI.FRM.Reports
{
    class TableReport
    {
        #region [ Report Company]
        public static DataTable CompanyInfo()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("CompanyInfo");
            // thay doi ten columns

            nTable.Columns.Add("CompanyName", typeof(string));
            nTable.Columns.Add("Address", typeof(string));

            CompanyInfo nCompany = new CompanyInfo();

            // them du lieu vao bang 

            DataRow nRow = nTable.NewRow();

            nRow["CompanyName"] = nCompany.CompanyName;
            nRow["Address"] = nCompany.Address;


            nTable.Rows.Add(nRow);
            return nTable;
        }
        #endregion

        #region[Report Customer]
        public static DataTable CustomersTitle()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("CustomersTitle");
            // thay doi ten columns

            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));
            //nTable.Columns.Add("SumBanRa", typeof(string));
            //nTable.Columns.Add("SumVATBanRa", typeof(string));

            return nTable;
        }

        public static DataTable CustomersDetail()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("CustomersDetail");
            // thay doi ten columns

            nTable.Columns.Add("CustomerID", typeof(string));
            nTable.Columns.Add("CustomerName", typeof(string));
            nTable.Columns.Add("TaxNumber", typeof(string));
            nTable.Columns.Add("Address", typeof(string));
            nTable.Columns.Add("CityName", typeof(string));
            nTable.Columns.Add("Phone", typeof(string));

            return nTable;
        }
        #endregion

        #region[Report Employees]
        public static DataTable EmployeesTitle()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("EmployeesTitle");
            // thay doi ten columns

            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));


            return nTable;
        }

        public static DataTable EmployeesDetail()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("EmployeesDetail");
            // thay doi ten columns

            nTable.Columns.Add("EmployeeID", typeof(string));
            nTable.Columns.Add("LastName", typeof(string));
            nTable.Columns.Add("FirstName", typeof(string));
            nTable.Columns.Add("Gender", typeof(string));
            nTable.Columns.Add("BirthDate", typeof(string));
            nTable.Columns.Add("Address", typeof(string));
            nTable.Columns.Add("HomePhone", typeof(string));

            return nTable;
        }
        #endregion

        #region[Report Cong no]
        public static DataTable LiabilitiesTitle()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("LiabilitiesTitle");
            // thay doi ten columns

            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));
            //
            nTable.Columns.Add("SumBeginAmountPay", typeof(string));
            nTable.Columns.Add("SumBeginAmountReceipt", typeof(string));
            nTable.Columns.Add("SumMiddleAmountPay", typeof(string));
            nTable.Columns.Add("SumMiddleAmountReceipt", typeof(string));
            nTable.Columns.Add("SumEndAmountPay", typeof(string));
            nTable.Columns.Add("SumEndAmountReceipt", typeof(string));
            return nTable;
        }

        public static DataTable LiabilitiesDetail()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("LiabilitiesDetail");
            // thay doi ten columns

            nTable.Columns.Add("CustomerKey", typeof(string));
            nTable.Columns.Add("CustomerID", typeof(string));
            nTable.Columns.Add("CustomerName", typeof(string));
            nTable.Columns.Add("BeginAmountPay", typeof(string));
            nTable.Columns.Add("BeginAmountReceipt", typeof(string));
            nTable.Columns.Add("MiddleAmountPay", typeof(string));
            nTable.Columns.Add("MiddleAmountReceipt", typeof(string));
            nTable.Columns.Add("EndAmountPay", typeof(string));
            nTable.Columns.Add("EndAmountReceipt", typeof(string));

            return nTable;
        }
        #endregion

        #region [ Products ]

        public static DataTable TableProducts()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableProducts");
            // thay doi ten columns
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("ProductID", typeof(string));
            nTable.Columns.Add("ProductName", typeof(string));
            nTable.Columns.Add("Unit", typeof(string));
            nTable.Columns.Add("SalePrice", typeof(string));
            nTable.Columns.Add("PurchasePrice", typeof(string));
            nTable.Columns.Add("VAT", typeof(string));
            nTable.Columns.Add("CategoryName", typeof(string));


            return nTable;
        }
        public static DataTable TableInventory()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableInventory");
            // thay doi ten columns

            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("WarehouseKey", typeof(string));

            nTable.Columns.Add("ProductName", typeof(string));
            nTable.Columns.Add("Unit", typeof(string));
            nTable.Columns.Add("Begin_Quantity", typeof(string));
            nTable.Columns.Add("Begin_AmountCurrencyMain", typeof(string));
            nTable.Columns.Add("In_Quantity", typeof(string));
            nTable.Columns.Add("In_AmountCurrencyMain", typeof(string));
            nTable.Columns.Add("Out_Quantity", typeof(string));
            nTable.Columns.Add("Out_AmountCurrencyMain", typeof(string));
            nTable.Columns.Add("End_Quantity", typeof(string));
            nTable.Columns.Add("End_AmountCurrencyMain", typeof(string));

            return nTable;
        }
        public static DataTable TableInventoryDetail()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableInventoryDetail");
            // thay doi ten columns

            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));
            nTable.Columns.Add("TaiKhoan", typeof(string));
            nTable.Columns.Add("SumTTNhap", typeof(string));
            nTable.Columns.Add("SumTTXuat", typeof(string));
            nTable.Columns.Add("SumTTTonDau", typeof(string));
            nTable.Columns.Add("SumTTTonCuoi", typeof(string));


            return nTable;
        }

        #endregion

        #region [ Report PhieuThu]

        public static DataTable TablePhieuThu()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("TenCongTy", typeof(string));
            nTable.Columns.Add("DiaChi", typeof(string));
            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("So", typeof(string));
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("Co", typeof(string));
            nTable.Columns.Add("HoTenNguoiNopTien", typeof(string));
            nTable.Columns.Add("DiaChiNguoiNop", typeof(string));
            nTable.Columns.Add("LyDoNop", typeof(string));
            nTable.Columns.Add("SoTien", typeof(string));
            nTable.Columns.Add("VietBangChu", typeof(string));
            nTable.Columns.Add("KemTheo", typeof(string));
            nTable.Columns.Add("ChungTuGoc", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));
            nTable.Columns.Add("DaNhanDuSoTien", typeof(string));
            nTable.Columns.Add("TyGiaNgoaiTe", typeof(string));
            nTable.Columns.Add("SoTienQuyDoi", typeof(string));



            return nTable;
        }


        #endregion

        #region [ Report PhieuChi]

        public static DataTable TablePhieuChi()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("TieuDeTren", typeof(string));
            nTable.Columns.Add("So", typeof(string));
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("Co", typeof(string));
            nTable.Columns.Add("HoTenNguoiChiTien", typeof(string));
            nTable.Columns.Add("DiaChiNguoiChi", typeof(string));
            nTable.Columns.Add("LyDoChi", typeof(string));
            nTable.Columns.Add("SoTien", typeof(string));
            nTable.Columns.Add("VietBangChu", typeof(string));
            nTable.Columns.Add("KemTheo", typeof(string));
            nTable.Columns.Add("ChungTuGoc", typeof(string));
            nTable.Columns.Add("TieuDeDuoi", typeof(string));
            nTable.Columns.Add("DaNhanDuSoTien", typeof(string));
            nTable.Columns.Add("TyGiaNgoaiTe", typeof(string));
            nTable.Columns.Add("SoTienQuyDoi", typeof(string));




            return nTable;
        }


        #endregion    }

        #region [ Report PhieuNhapKho]

        public static DataTable TablePhieuNhapKhoTieuDe()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("BoPhan", typeof(string));
            nTable.Columns.Add("NgayNhap", typeof(string));
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("So", typeof(string));
            nTable.Columns.Add("Co", typeof(string));
            nTable.Columns.Add("TenNguoiGiao", typeof(string));
            nTable.Columns.Add("LyDoNhap", typeof(string));
            nTable.Columns.Add("NhapTaiKho", typeof(string));
            nTable.Columns.Add("DiaDiem", typeof(string));
            nTable.Columns.Add("TongThanhTien", typeof(string));
            nTable.Columns.Add("TongTienBangChu", typeof(string));
            nTable.Columns.Add("ChungTuGoc", typeof(string));
            nTable.Columns.Add("NgayVietPhieuNhap", typeof(string));

            return nTable;
        }

        public static DataTable TablePhieuNhapKhoDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("STT", typeof(string));
            nTable.Columns.Add("TenSP", typeof(string));
            nTable.Columns.Add("Ma", typeof(string));
            nTable.Columns.Add("DonViTinh", typeof(string));
            nTable.Columns.Add("SoLuongChungTu", typeof(string));
            nTable.Columns.Add("SoLuongThucNhap", typeof(string));
            nTable.Columns.Add("DonGia", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report PhieuXuatKho]

        public static DataTable TablePhieuXuatKhoTieuDe()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("BoPhan", typeof(string));
            nTable.Columns.Add("NgayXuat", typeof(string));
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("So", typeof(string));
            nTable.Columns.Add("Co", typeof(string));
            nTable.Columns.Add("TenNguoiNhan", typeof(string));
            nTable.Columns.Add("DiaChiNguoiNhan", typeof(string));
            nTable.Columns.Add("LyDoXuat", typeof(string));
            nTable.Columns.Add("XuatTaiKho", typeof(string));
            nTable.Columns.Add("DiaDiem", typeof(string));
            nTable.Columns.Add("TongThanhTien", typeof(string));
            nTable.Columns.Add("TongTienBangChu", typeof(string));
            nTable.Columns.Add("ChungTuGoc", typeof(string));
            nTable.Columns.Add("NgayVietPhieuXuat", typeof(string));

            return nTable;
        }

        public static DataTable TablePhieuXuatKhoDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("STT", typeof(string));
            nTable.Columns.Add("TenSP", typeof(string));
            nTable.Columns.Add("Ma", typeof(string));
            nTable.Columns.Add("DonViTinh", typeof(string));
            nTable.Columns.Add("YeuCauXuat", typeof(string));
            nTable.Columns.Add("ThucXuat", typeof(string));
            nTable.Columns.Add("DonGia", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report HoaDonMuaHang]

        public static DataTable TableHoaDonMuaHangTitle()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("NgayHD", typeof(string));
            nTable.Columns.Add("Mauso", typeof(string));
            nTable.Columns.Add("KyHieu", typeof(string));
            nTable.Columns.Add("SoHD", typeof(string));
            nTable.Columns.Add("SoHieu", typeof(string));
            nTable.Columns.Add("ThueSuat", typeof(string));
            nTable.Columns.Add("TienThue", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));
            nTable.Columns.Add("NhaCC", typeof(string));


            nTable.Columns.Add("TongThanhTien", typeof(string));
            nTable.Columns.Add("TongTienBangChu", typeof(string));

            return nTable;
        }

        public static DataTable TableHoaDonMuaHangDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("STT", typeof(string));
            nTable.Columns.Add("TenSP", typeof(string));
            nTable.Columns.Add("Ma", typeof(string));
            nTable.Columns.Add("DonViTinh", typeof(string));
            nTable.Columns.Add("YeuCauXuat", typeof(string));
            nTable.Columns.Add("ThucXuat", typeof(string));
            nTable.Columns.Add("DonGia", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report HoaDonBanHang]

        public static DataTable TableHoaDonBanHangTitle()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("NgayHD", typeof(string));
            nTable.Columns.Add("Mauso", typeof(string));
            nTable.Columns.Add("KyHieu", typeof(string));
            nTable.Columns.Add("SoHD", typeof(string));
            nTable.Columns.Add("SoHieu", typeof(string));
            nTable.Columns.Add("ThueSuat", typeof(string));
            nTable.Columns.Add("TienThue", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));
            nTable.Columns.Add("NhaCC", typeof(string));


            nTable.Columns.Add("TongThanhTien", typeof(string));
            nTable.Columns.Add("TongTienBangChu", typeof(string));

            return nTable;
        }

        public static DataTable TableHoaDonBanHangDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("STT", typeof(string));
            nTable.Columns.Add("TenSP", typeof(string));
            nTable.Columns.Add("Ma", typeof(string));
            nTable.Columns.Add("DonViTinh", typeof(string));
            nTable.Columns.Add("YeuCauXuat", typeof(string));
            nTable.Columns.Add("ThucXuat", typeof(string));
            nTable.Columns.Add("DonGia", typeof(string));
            nTable.Columns.Add("ThanhTien", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report CVNhanVien]

        public static DataTable TableCVNhanVienDetail()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableCVNhanVienDetail");
            // thay doi ten columns

            nTable.Columns.Add("EmployeeID", typeof(string));
            nTable.Columns.Add("LastName", typeof(string));
            nTable.Columns.Add("FirstName", typeof(string));
            nTable.Columns.Add("Gender", typeof(string));
            nTable.Columns.Add("BirthDate", typeof(string));
            nTable.Columns.Add("Address", typeof(string));
            nTable.Columns.Add("HomePhone", typeof(string));

            nTable.Columns.Add("PassportNumber", typeof(string));
            nTable.Columns.Add("IssueDate", typeof(string));
            nTable.Columns.Add("ExpireDate", typeof(string));
            nTable.Columns.Add("IssuePlace", typeof(string));
            nTable.Columns.Add("CurrentNationality", typeof(string));
            nTable.Columns.Add("City", typeof(string));
            nTable.Columns.Add("Country", typeof(string));
            nTable.Columns.Add("MobiPhone", typeof(string));
            nTable.Columns.Add("Email", typeof(string));
            nTable.Columns.Add("Photo", typeof(byte[]));

            return nTable;
        }
        #endregion    }

        #region [ Report BangLuongNhanVien]

        public static DataTable TableBangLuongNVDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("EmployeeID", typeof(string));
            nTable.Columns.Add("EmployeeName", typeof(string));
            nTable.Columns.Add("BasicSalary", typeof(string));
            nTable.Columns.Add("WorkDays", typeof(string));
            nTable.Columns.Add("NetSalary", typeof(string));
            nTable.Columns.Add("Bonus", typeof(string));
            nTable.Columns.Add("TotalSalary", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report HangTKDK]

        public static DataTable TableHangTKDKDetail()
        {
            DataTable nTable = new DataTable();
            nTable.Columns.Add("No", typeof(string));
            nTable.Columns.Add("ProductID", typeof(string));
            nTable.Columns.Add("ProductName", typeof(string));
            nTable.Columns.Add("Unit", typeof(string));
            nTable.Columns.Add("BeginQuantity", typeof(string));
            nTable.Columns.Add("BeginAmountDebitCurrencyMain", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report DanhSachCongViec]

        public static DataTable TableTaskList()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableTaskList");
            // thay doi ten columns

            nTable.Columns.Add("CategoryName", typeof(string));
            nTable.Columns.Add("TaskName", typeof(string));
            nTable.Columns.Add("StatusName", typeof(string));
            nTable.Columns.Add("StartDate", typeof(string));
            nTable.Columns.Add("DueDate", typeof(string));
            nTable.Columns.Add("Complete", typeof(string));
            nTable.Columns.Add("ProjectName", typeof(string));
            nTable.Columns.Add("CustomerName", typeof(string));

            return nTable;
        }
        #endregion    }

        #region [ Report DanhSachHopDong]

        public static DataTable TableProjectList()
        {
            //thay doi ten table
            DataTable nTable = new DataTable("TableProjectList");
            // thay doi ten columns

            nTable.Columns.Add("ProjectID", typeof(string));
            nTable.Columns.Add("ProjectName", typeof(string));
            nTable.Columns.Add("StatusName", typeof(string));
            nTable.Columns.Add("StartDate", typeof(string));
            nTable.Columns.Add("DueDate", typeof(string));
            nTable.Columns.Add("Complete", typeof(string));
            nTable.Columns.Add("CustomerName", typeof(string));

            return nTable;
        }
        #endregion    }
    }
}
