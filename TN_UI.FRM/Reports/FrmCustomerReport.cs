﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using TNLibrary.CRM;
using System.Drawing.Drawing2D;
namespace TN_UI.FRM.Reports
{
    public partial class FrmCustomerReport : Form
    {
        public bool FlagView;
        ReportDocument rpt = new ReportDocument();
        public FrmCustomerReport()
        {
            InitializeComponent();
        }

        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport(DataTable Table)
        {
            // thay ten report
            rpt.Load("FilesReport\\CrystalReporCustomers.rpt");

            DataTable nCustomersTitle = TableReport.CustomersTitle();
            DataTable nCustomersDetail = TableReport.CustomersDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            DataRow nRow;


            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];


                string[] nRowView = 
                    { 
                        nRow["CustomerID"].ToString(), 
                        nRow["CustomerName"].ToString().Trim(),
                        nRow["TaxNumber"].ToString().Trim(),
                        nRow["Address"].ToString().Trim(),
                        nRow["CityName"].ToString().Trim(),
                        nRow["Phone"].ToString().Trim()
                        
                    };
                nCustomersDetail.Rows.Add(nRowView);
            }
            rpt.Database.Tables["CustomersDetail"].SetDataSource(nCustomersDetail);
            rpt.Database.Tables["CustomersTitle"].SetDataSource(nCustomersTitle);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);
            crystalReportViewer1.ReportSource = rpt;
            // thay doi do zoom
            crystalReportViewer1.Zoom(1);
        }

        private void PopulateExportToExcel(DataTable Table)
        {
            DataTable nCustomersDetail = TableReport.CustomersDetail();
            DataTable nCustomersTitle = TableReport.CustomersTitle();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            DataRow nRow;


            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];


                string[] nRowView = 
                    { 
                        nRow["CustomerID"].ToString(), 
                        nRow["CustomerName"].ToString().Trim(),
                        nRow["TaxNumber"].ToString().Trim(),
                        nRow["Address"].ToString().Trim(),
                        nRow["CityName"].ToString().Trim(),
                        nRow["Phone"].ToString().Trim()
                        
                    };
                nCustomersDetail.Rows.Add(nRowView);
            }

            //ExportExcel(nTableTencongty, nTableDetail,nTableTieuDe);
        }

        private void FrmCustomerReport_Load(object sender, EventArgs e)
        {
            //panelSearch.Height = 0;
            //txtTitle.Text = mTitle;
            //txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            crystalReportViewer1.Dock = DockStyle.Fill;

            DataTable nTableCustomer;
            nTableCustomer = Customers_Data.CustomerList();
            PopulateCrystalReport(nTableCustomer);
            //SetupCrystalReport();
        }
    }
}
