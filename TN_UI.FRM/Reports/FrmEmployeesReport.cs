﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using TNLibrary.HRM;

namespace TN_UI.FRM.Reports
{
    public partial class FrmEmployeesReport : Form
    {
        public bool FlagView;
        ReportDocument rpt = new ReportDocument();

        public FrmEmployeesReport()
        {
            InitializeComponent();
        }

        private void FrmEmployeesReport_Load(object sender, EventArgs e)
        {
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            crystalReportViewer1.Dock = DockStyle.Fill;

            DataTable nTableEmployees;
            nTableEmployees = Employees_Data.EmployeeList();
            PopulateCrystalReport(nTableEmployees);
        }
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport(DataTable Table)
        {
            // thay ten report
            rpt.Load("FilesReport\\CrystalReporEmployees.rpt");

            DataTable nEmployeesTitle = TableReport.EmployeesTitle();
            DataTable nEmployeesDetail = TableReport.EmployeesDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            DataRow nRow;


            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];


                string[] nRowView = 
                    { 
                        nRow["EmployeeID"].ToString(), 
                        nRow["LastName"].ToString().Trim(),
                        nRow["FirstName"].ToString().Trim(),
                        nRow["Gender"].ToString().Trim(),
                        nRow["BirthDate"].ToString().Trim(),
                        nRow["Address"].ToString().Trim(),
                        nRow["HomePhone"].ToString().Trim()
                        
                    };
                nEmployeesDetail.Rows.Add(nRowView);
            }

            rpt.Database.Tables["EmployeesTitle"].SetDataSource(nEmployeesTitle);
            rpt.Database.Tables["EmployeesDetail"].SetDataSource(nEmployeesDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);
            crystalReportViewer1.ReportSource = rpt;
            // thay doi do zoom
            crystalReportViewer1.Zoom(1);
        }
    }
}
