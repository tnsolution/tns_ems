using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.FNC.Bills;

namespace TN_UI.FRM.Reports
{
    public partial class FrmFinancial : Form
    {
        public string mRoomID = "";

        string mFromatDate = GlobalSystemConfig.FormatDate;
        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmFinancial()
        {
            InitializeComponent();

        }

        private void FrmCalendarRoom_Load(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            this.WindowState = FormWindowState.Maximized;
            LoadInfoFinancial();
        }

        //Layout 
        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Lay out left  ]

        private void cmdHide_MouseEnter(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[1];
        }

        private void cmdHide_MouseLeave(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[0];
        }

        private void cmdHide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 28;  //  ContainerLeft.SplitterDistance = 10;
            TreeViewData.Visible = false;

            cmdHide.Visible = false;
            txtTitleLeft.Visible = false;
            panelLineLeft.Visible = false;
            panelViewLeft.Visible = false;

            cmdUnhide.Visible = true;
        }


        private void cmdUnhide_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[3];
        }

        private void cmdUnhide_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[2];
        }

        private void cmdUnhide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 250;// ContainerLeft.SplitterDistance = 260;
            TreeViewData.Visible = true;
            cmdHide.Visible = true;
            txtTitleLeft.Visible = true;
            panelLineLeft.Visible = true;
            panelViewLeft.Visible = true;

            cmdUnhide.Visible = false;

        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            cmdHide.Left = panelTitle.Width - cmdHide.Width;
        }
        #endregion

        #region [Display Info]
        private void cmdNextWeek_Click(object sender, EventArgs e)
        {
            CalendarFinancial.SelectionStart = CalendarFinancial.SelectionStart.AddDays(7);

        }
        private void cmdLastWeek_Click(object sender, EventArgs e)
        {
            CalendarFinancial.SelectionStart = CalendarFinancial.SelectionStart.AddDays(-7);
            CalendarFinancial.SelectionEnd = CalendarFinancial.SelectionStart;

        }

        #endregion

        #region [ GridView ]
        private int mWidthCellDate = 120;
        DataTable mCalendar = new DataTable();
        private void SetupLayoutGridView(DataGridView GV, DateTime FirstDayInWeek)
        {
            // Setup Column 
            FirstDayInWeek = new DateTime(FirstDayInWeek.Year, FirstDayInWeek.Month, FirstDayInWeek.Day, 0, 0, 0);
            GV.Columns.Clear();
            // Setup Column 
            GV.Columns.Add("BranchName", "BranchName");
            GV.Columns.Add("Monday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(0).Day.ToString() + " [Thứ hai]");
            GV.Columns.Add("Tuesday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(1).Day.ToString() + " [Thứ ba]");
            GV.Columns.Add("Wednesday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(2).Day.ToString() + " [Thứ tư]");
            GV.Columns.Add("Thursday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(3).Day.ToString() + " [Thứ năm]");
            GV.Columns.Add("Friday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(4).Day.ToString() + " [Thứ sáu]");
            GV.Columns.Add("Saturday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(5).Day.ToString() + " [Thứ 7]");
            GV.Columns.Add("Sunday", FirstDayInWeek.AddDays(0).Month.ToString() + "/" + FirstDayInWeek.AddDays(6).Day.ToString() + " [Chủ nhật]");

            GV.Columns[0].Width = mWidthCellDate;
            for (int i = 1; i <= 5; i++)
            {
                GV.Columns[i].Width = mWidthCellDate;
                GV.Columns[i].ReadOnly = true;
                GV.Columns[i].Tag = FirstDayInWeek.AddDays(i - 1);
                GV.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            }
            for (int i = 6; i <= 7; i++)
            {
                GV.Columns[i].Width = mWidthCellDate;
                GV.Columns[i].ReadOnly = true;
                GV.Columns[i].DefaultCellStyle.BackColor = Color.OldLace;
                GV.Columns[i].Tag = FirstDayInWeek.AddDays(i - 1);
                GV.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            }

            // setup style view
            GV.ScrollBars = ScrollBars.Horizontal;
            GV.BackgroundColor = Color.White;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            // GV.Dock = DockStyle.Fill;
            //Activate On Gridview

        }

        #endregion

        #region [ Function ]
        private int Day_Of_Week(string strDayOfWeek)
        {
            int nDay = 0;
            switch (strDayOfWeek)
            {
                case "Monday":
                    nDay = 2;
                    break;
                case "Tuesday":
                    nDay = 3;
                    break;
                case "Wednesday":
                    nDay = 4;
                    break;
                case "Thursday":
                    nDay = 5;
                    break;
                case "Friday":
                    nDay = 6;
                    break;
                case "Saturday":
                    nDay = 7;
                    break;
                case "Sunday":
                    nDay = 8;
                    break;
            }
            return nDay;
        }
        #endregion

        private void CalendarFinancial_DateChanged(object sender, DateRangeEventArgs e)
        {
            LoadInfoFinancial();
        }

        private void LoadInfoFinancial()
        {
            this.Cursor = Cursors.WaitCursor;
            DateTime nDateStart = CalendarFinancial.SelectionStart.AddDays((-1) * (Day_Of_Week(CalendarFinancial.SelectionStart.DayOfWeek.ToString()) - 2));
            DateTime nDateEnd = CalendarFinancial.SelectionEnd.AddDays(8 - (Day_Of_Week(CalendarFinancial.SelectionEnd.DayOfWeek.ToString())));

            txtTitleMonth.Text = nDateStart.ToString(mFromatDate) + "->" + nDateEnd.ToString(mFromatDate);
            SetupLayoutGridView(dataGridView1, nDateStart);

            DataTable nTable = Cash_Bills_Data.ReceiptBranchList(nDateStart, nDateEnd);
            int i = 0;
            double[] nAmountTotal = new double[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

            foreach (DataRow nRow in nTable.Rows)
            {
                dataGridView1.Rows.Add();
                DataGridViewRow nGV_Row = dataGridView1.Rows[i];
                nGV_Row.Cells["BranchName"].Value = nRow["BranchName"].ToString();
                nGV_Row.Cells["BranchName"].Tag = nRow["BranchKey"];
                for (int k = 1; k <= 7; k++)
                {
                    string strAmount = nRow[k + 1].ToString();
                    double nAmount = 0;
                    if (strAmount.Length > 0)
                        nAmount = double.Parse(strAmount);

                    nAmountTotal[k] += nAmount;

                    nGV_Row.Cells[k].Value = nAmount.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                }
                i++;
            }

            dataGridView1.Rows.Add();
            DataGridViewRow nGV_RowTotal = dataGridView1.Rows[i];
            nGV_RowTotal.DefaultCellStyle.BackColor = Color.Azure;
            nGV_RowTotal.DefaultCellStyle.Font = new Font("Arial",9,FontStyle.Bold);

            nGV_RowTotal.Cells[0].Value = "Tổng cộng";

            for (int k = 1; k <= 7; k++)
            {
                nGV_RowTotal.Cells[k].Value = nAmountTotal[k].ToString(mFormatCurrencyMain, mFormatProviderCurrency);
            }
            this.Cursor = Cursors.Default;
        }

    }
}