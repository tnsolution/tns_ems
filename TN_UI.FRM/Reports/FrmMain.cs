﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.FNC;
using TNLibrary.FNC.Invoices;
using TNLibrary.FNC.Bills;

namespace TN_UI.FRM.Reports
{
    public partial class FrmMain : Form
    {
        private int mFinancialYear = 0;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FinancialYear_Info nFinancialYear = new FinancialYear_Info(true);
            mFinancialYear = nFinancialYear.FromDate.Year;
            this.WindowState = FormWindowState.Maximized;
            //this.ChartInvoice.Paint += new PaintEventHandler(this.ChartInvoice_Paint);
            this.ChartPayment.Paint += new PaintEventHandler(this.ChartPayment_Paint);
            this.ChartBillBranch_XuanTruong.Paint += new PaintEventHandler(this.ChartBillBranch_XuanTruong_Paint);
            this.ChartBillBranch_CaMau.Paint += new PaintEventHandler(this.ChartBillBranch_CaMau_Paint);
            this.ChartBillBranch_DongNai.Paint += new PaintEventHandler(this.ChartBillBranch_DongNai_Paint);
            this.ChartBillBranch_VinaBeauty.Paint += new PaintEventHandler(this.ChartBillBranch_VinaBeauty_Paint);
            this.ChartBillBranch_DaNang.Paint += new PaintEventHandler(this.ChartBillBranch_DaNang_Paint);
           
        }

        private void ChartInvoice_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupInvoicePurchase = new SolidBrush(Color.Red);
            Brush BackgroupInvoiceSale = new SolidBrush(Color.Navy);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartInvoice.Width, ChartInvoice.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartInvoice.Height - 30);
            int nWidth = ChartInvoice.Width - 150;//(100 + 50)
            int nHeight = ChartInvoice.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi
            ArrayList nMonths = new ArrayList();
            double nMax = 0;
            nMonths.Add(0);
            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth = Invoice_Data.Sale_TotalAmount(i, mFinancialYear);
                if (nMax < nTotalMonth)
                    nMax = nTotalMonth;

                nMonths.Add(nTotalMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartInvoice.Controls.Add(lblValueY);
            }

            for (int i = 1; i <= 12; i++)
            {
                double nTotalMonth_i = (double)nMonths[i];
                int nInvoiceHeight = Convert.ToInt32((nTotalMonth_i * 150) / nMax);
                int Y = nXoY.Y - nInvoiceHeight;
                g.FillRectangle(BackgroupInvoicePurchase, nXoY.X + (30 * i), Y, 10, nInvoiceHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i);
                ChartInvoice.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }

        private void ChartPayment_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartPayment.Width, ChartPayment.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartPayment.Height - 30);
            int nWidth = ChartPayment.Width - 150;//(100 + 50)
            int nHeight = ChartPayment.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartPayment.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartPayment.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }
        private void ChartBillBranch_XuanTruong_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartBillBranch_XuanTruong.Width, ChartBillBranch_XuanTruong.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartBillBranch_XuanTruong.Height - 30);
            int nWidth = ChartBillBranch_XuanTruong.Width - 150;//(100 + 50)
            int nHeight = ChartBillBranch_XuanTruong.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(6,i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(6,i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartBillBranch_XuanTruong.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartBillBranch_XuanTruong.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }
        private void ChartBillBranch_CaMau_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartBillBranch_CaMau.Width, ChartBillBranch_CaMau.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartBillBranch_CaMau.Height - 30);
            int nWidth = ChartBillBranch_CaMau.Width - 150;//(100 + 50)
            int nHeight = ChartBillBranch_CaMau.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(1, i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(1, i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartBillBranch_CaMau.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartBillBranch_CaMau.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }
        private void ChartBillBranch_DaNang_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartBillBranch_DaNang.Width, ChartBillBranch_DaNang.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartBillBranch_DaNang.Height - 30);
            int nWidth = ChartBillBranch_DaNang.Width - 150;//(100 + 50)
            int nHeight = ChartBillBranch_DaNang.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(3, i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(3, i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartBillBranch_DaNang.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartBillBranch_DaNang.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }
        private void ChartBillBranch_VinaBeauty_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartBillBranch_VinaBeauty.Width, ChartBillBranch_VinaBeauty.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartBillBranch_VinaBeauty.Height - 30);
            int nWidth = ChartBillBranch_VinaBeauty.Width - 150;//(100 + 50)
            int nHeight = ChartBillBranch_VinaBeauty.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(4, i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(4, i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartBillBranch_VinaBeauty.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartBillBranch_VinaBeauty.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }
        private void ChartBillBranch_DongNai_Paint(object sender, PaintEventArgs e)
        {
            Bitmap drawing = null;
            Graphics g;
            Pen m_LinePenXY = new Pen(Color.Navy, 1);
            Pen m_LinePen = new Pen(Color.Silver, 1);

            Pen m_LinePenRec = new Pen(Color.Orange, 1);
            Brush BackgroupPayment = new SolidBrush(Color.Navy);
            Brush BackgroupReceipt = new SolidBrush(Color.Red);

            Font font = new Font("Arial", 8);
            drawing = new Bitmap(ChartBillBranch_DongNai.Width, ChartBillBranch_DongNai.Height, e.Graphics);
            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point nXoY = new Point(100, ChartBillBranch_DongNai.Height - 30);
            int nWidth = ChartBillBranch_DongNai.Width - 150;//(100 + 50)
            int nHeight = ChartBillBranch_DongNai.Height - 50;

            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X, nXoY.Y - nHeight); //Line y
            g.DrawLine(m_LinePenXY, nXoY.X, nXoY.Y, nXoY.X + nWidth, nXoY.Y); // Line X

            // cac duong ke
            for (int i = 1; i <= 5; i++)
            {
                g.DrawLine(m_LinePen, nXoY.X, nXoY.Y - (30 * i), nXoY.X + nWidth, nXoY.Y - (30 * i));
            }
            // ve cac cot do thi Payments
            ArrayList nPaymentMonths = new ArrayList();
            ArrayList nReceiptMonths = new ArrayList();

            double nMax = 0;
            nPaymentMonths.Add(0);
            nReceiptMonths.Add(0);

            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth = Cash_Bills_Data.Payment_TotalAmount(2, i, mFinancialYear);
                double nTotalReceiptMonth = Cash_Bills_Data.Receipt_TotalAmount(2, i, mFinancialYear);

                if (nMax < nTotalPaymentMonth)
                    nMax = nTotalPaymentMonth;
                if (nMax < nTotalReceiptMonth)
                    nMax = nTotalReceiptMonth;

                nPaymentMonths.Add(nTotalPaymentMonth);
                nReceiptMonths.Add(nTotalReceiptMonth);
            }
            if (nMax == 0)
                nMax = 1;
            else
            {
                nMax = TopValue(nMax);
            }
            // Value Y
            double nUnitValueY = nMax / 5;

            for (int i = 1; i <= 5; i++)
            {
                double nValueX = nUnitValueY + ((nUnitValueY) * (i - 1));
                Label lblValueY = new Label();
                lblValueY.AutoSize = true;
                //lblValueY.Width = 20;
                lblValueY.Text = nValueX.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                lblValueY.Font = font;
                lblValueY.Top = (nXoY.Y - (30 * i)) - (5);
                lblValueY.Left = 10;
                ChartBillBranch_DongNai.Controls.Add(lblValueY);
            }
            for (int i = 1; i <= 12; i++)
            {
                double nTotalPaymentMonth_i = (double)nPaymentMonths[i];
                double nTotalReceiptMonth_i = (double)nReceiptMonths[i];

                int nPaymentHeight = Convert.ToInt32((nTotalPaymentMonth_i * 150) / nMax);
                int nReceiptHeight = Convert.ToInt32((nTotalReceiptMonth_i * 150) / nMax);

                int Y_Payment = nXoY.Y - nPaymentHeight;
                int Y_Receipt = nXoY.Y - nReceiptHeight;

                g.FillRectangle(BackgroupPayment, nXoY.X + (30 * i) - 10, Y_Payment, 10, nPaymentHeight);
                g.FillRectangle(BackgroupReceipt, nXoY.X + (30 * i), Y_Receipt, 10, nReceiptHeight);

                Label nValueX = new Label();
                nValueX.Width = 20;
                nValueX.Text = i.ToString();
                nValueX.Font = font;
                nValueX.Top = nXoY.Y + 5;
                nValueX.Left = nXoY.X + (30 * i) - 7;
                ChartBillBranch_DongNai.Controls.Add(nValueX);

            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);

            g.Dispose();
        }

        private double TopValue(double ProcessValue)
        {
            string nNumberMax = ProcessValue.ToString();
            int nLen = nNumberMax.Length;

            string nFirst = nNumberMax.Substring(0, 1);
            string nSecond = nNumberMax.Substring(1, 1);

            if (int.Parse(nSecond) >= 5)
            {
                nFirst = Convert.ToString(int.Parse(nFirst) + 1);
                nSecond = "0";
            }
            else
                nSecond = "5";
            string nResult = nFirst + nSecond;
            nResult = nResult.PadRight(nLen, '0');

            return double.Parse(nResult);
        }
    }
}
