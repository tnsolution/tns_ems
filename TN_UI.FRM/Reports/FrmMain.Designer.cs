﻿namespace TN_UI.FRM.Reports
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChartPayment = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ChartInvoice = new System.Windows.Forms.GroupBox();
            this.ChartBillBranch_XuanTruong = new System.Windows.Forms.GroupBox();
            this.ChartBillBranch_DongNai = new System.Windows.Forms.GroupBox();
            this.ChartBillBranch_CaMau = new System.Windows.Forms.GroupBox();
            this.ChartBillBranch_DaNang = new System.Windows.Forms.GroupBox();
            this.ChartBillBranch_VinaBeauty = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // ChartPayment
            // 
            this.ChartPayment.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartPayment.ForeColor = System.Drawing.Color.Navy;
            this.ChartPayment.Location = new System.Drawing.Point(12, 34);
            this.ChartPayment.Name = "ChartPayment";
            this.ChartPayment.Size = new System.Drawing.Size(558, 228);
            this.ChartPayment.TabIndex = 4;
            this.ChartPayment.TabStop = false;
            this.ChartPayment.Text = "Tình hình thu chi toàn hệ thống";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Red;
            this.panel2.Location = new System.Drawing.Point(16, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(15, 15);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Navy;
            this.panel1.Location = new System.Drawing.Point(118, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(15, 15);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số tiền thu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(137, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số tiền chi";
            // 
            // ChartInvoice
            // 
            this.ChartInvoice.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartInvoice.ForeColor = System.Drawing.Color.Navy;
            this.ChartInvoice.Location = new System.Drawing.Point(616, 544);
            this.ChartInvoice.Name = "ChartInvoice";
            this.ChartInvoice.Size = new System.Drawing.Size(83, 63);
            this.ChartInvoice.TabIndex = 3;
            this.ChartInvoice.TabStop = false;
            this.ChartInvoice.Text = "Doanh số bán hàng";
            this.ChartInvoice.Visible = false;
            // 
            // ChartBillBranch_XuanTruong
            // 
            this.ChartBillBranch_XuanTruong.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartBillBranch_XuanTruong.ForeColor = System.Drawing.Color.Navy;
            this.ChartBillBranch_XuanTruong.Location = new System.Drawing.Point(12, 268);
            this.ChartBillBranch_XuanTruong.Name = "ChartBillBranch_XuanTruong";
            this.ChartBillBranch_XuanTruong.Size = new System.Drawing.Size(558, 228);
            this.ChartBillBranch_XuanTruong.TabIndex = 5;
            this.ChartBillBranch_XuanTruong.TabStop = false;
            this.ChartBillBranch_XuanTruong.Text = "Tình hình thu chi : Xuân trường";
            // 
            // ChartBillBranch_DongNai
            // 
            this.ChartBillBranch_DongNai.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartBillBranch_DongNai.ForeColor = System.Drawing.Color.Navy;
            this.ChartBillBranch_DongNai.Location = new System.Drawing.Point(597, 34);
            this.ChartBillBranch_DongNai.Name = "ChartBillBranch_DongNai";
            this.ChartBillBranch_DongNai.Size = new System.Drawing.Size(558, 228);
            this.ChartBillBranch_DongNai.TabIndex = 6;
            this.ChartBillBranch_DongNai.TabStop = false;
            this.ChartBillBranch_DongNai.Text = "Tình hình thu chi : Đồng Nai";
            // 
            // ChartBillBranch_CaMau
            // 
            this.ChartBillBranch_CaMau.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartBillBranch_CaMau.ForeColor = System.Drawing.Color.Navy;
            this.ChartBillBranch_CaMau.Location = new System.Drawing.Point(597, 268);
            this.ChartBillBranch_CaMau.Name = "ChartBillBranch_CaMau";
            this.ChartBillBranch_CaMau.Size = new System.Drawing.Size(558, 228);
            this.ChartBillBranch_CaMau.TabIndex = 7;
            this.ChartBillBranch_CaMau.TabStop = false;
            this.ChartBillBranch_CaMau.Text = "Tình hình thu chi : Cà Mau";
            // 
            // ChartBillBranch_DaNang
            // 
            this.ChartBillBranch_DaNang.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartBillBranch_DaNang.ForeColor = System.Drawing.Color.Navy;
            this.ChartBillBranch_DaNang.Location = new System.Drawing.Point(12, 507);
            this.ChartBillBranch_DaNang.Name = "ChartBillBranch_DaNang";
            this.ChartBillBranch_DaNang.Size = new System.Drawing.Size(558, 228);
            this.ChartBillBranch_DaNang.TabIndex = 8;
            this.ChartBillBranch_DaNang.TabStop = false;
            this.ChartBillBranch_DaNang.Text = "Tình hình thu chi : Đà nẵng";
            // 
            // ChartBillBranch_VinaBeauty
            // 
            this.ChartBillBranch_VinaBeauty.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChartBillBranch_VinaBeauty.ForeColor = System.Drawing.Color.Navy;
            this.ChartBillBranch_VinaBeauty.Location = new System.Drawing.Point(597, 507);
            this.ChartBillBranch_VinaBeauty.Name = "ChartBillBranch_VinaBeauty";
            this.ChartBillBranch_VinaBeauty.Size = new System.Drawing.Size(558, 228);
            this.ChartBillBranch_VinaBeauty.TabIndex = 9;
            this.ChartBillBranch_VinaBeauty.TabStop = false;
            this.ChartBillBranch_VinaBeauty.Text = "Tình hình thu chi : VinaBeauty";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1240, 760);
            this.Controls.Add(this.ChartBillBranch_VinaBeauty);
            this.Controls.Add(this.ChartBillBranch_DaNang);
            this.Controls.Add(this.ChartBillBranch_CaMau);
            this.Controls.Add(this.ChartBillBranch_DongNai);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ChartBillBranch_XuanTruong);
            this.Controls.Add(this.ChartPayment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChartInvoice);
            this.Name = "FrmMain";
            this.Text = "Thông số tài chính";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox ChartPayment;
        private System.Windows.Forms.GroupBox ChartInvoice;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox ChartBillBranch_XuanTruong;
        private System.Windows.Forms.GroupBox ChartBillBranch_DongNai;
        private System.Windows.Forms.GroupBox ChartBillBranch_CaMau;
        private System.Windows.Forms.GroupBox ChartBillBranch_DaNang;
        private System.Windows.Forms.GroupBox ChartBillBranch_VinaBeauty;
    }
}