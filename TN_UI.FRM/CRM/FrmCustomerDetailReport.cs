using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using TNLibrary.SYS;
using TNLibrary.IVT;
using TN_UI.FRM.Reports;
using TNLibrary.CRM;

namespace TN_UI.FRM.Inventory
{
    public partial class FrmCustomerDetailReport : Form
    {
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;

        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        private string mFormatDate = GlobalSystemConfig.FormatDate;
        public Customer_Object mCustomerList;
        public int mCustomerTypeUI = 1;
        public FrmCustomerDetailReport()
        {
            InitializeComponent();

        }

        private void FrmStockNoteInputReport_Load(object sender, EventArgs e)
        {

            
            SetupCrystalReport();
            PopulateCrystalReport();
        }


        #region [ CrystalReport]
        private void SetupCrystalReport()
        {

            //LoadDataToToolbox.ComboBoxData(CoCategories, nSQLCategory, true);
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport()
        {

            this.Cursor = Cursors.WaitCursor;
            ReportDocument rpt = new ReportDocument();
            if (mCustomerTypeUI == 1) //Vendor
            {
                if (mCustomerList.IsVendor)
                {
                    rpt.Load("FilesReport\\CrystalReportSupplierDetail.rpt");
                }
                else
                    rpt.Load("FilesReport\\CrystalReporCustomersDetail.rpt");



                DataTable nCustomersDetail = TableReport.CustomersDetail();
                DataTable nCompanyInfo = TableReport.CompanyInfo();


                DataRow nRowDetail = nCustomersDetail.NewRow();
                nRowDetail["CustomerID"] = mCustomerList.ID;
                nRowDetail["CustomerName"] = mCustomerList.Name;
                nRowDetail["Phone"] = mCustomerList.Phone;
                nRowDetail["TaxNumber"] = mCustomerList.TaxNumber;
                nRowDetail["Address"] = mCustomerList.Address;
                nRowDetail["CityName"] = mCustomerList.CityName;

                nCustomersDetail.Rows.Add(nRowDetail);



                rpt.Database.Tables["CustomersDetail"].SetDataSource(nCustomersDetail);
                rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

                crystalReportViewer1.ReportSource = rpt;
                crystalReportViewer1.Zoom(1);

                this.Cursor = Cursors.Default;

            }
        #endregion



        }
    }
}