﻿namespace TN_UI.FRM.CRM
{
    partial class FrmProjectDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjectDetail));
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.CoStatus = new System.Windows.Forms.ComboBox();
            this.txtComplete = new System.Windows.Forms.NumericUpDown();
            this.CoCustomers = new System.Windows.Forms.ComboBox();
            this.txtProjectContent = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdSaveClose = new System.Windows.Forms.ToolStripButton();
            this.cmdSaveNew = new System.Windows.Forms.ToolStripButton();
            this.cmdDel = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtProjectID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProjectPrice = new System.Windows.Forms.TextBox();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tnPickerDueDate = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.tnPickerStartDate = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtComplete)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtProjectName
            // 
            this.txtProjectName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectName.Location = new System.Drawing.Point(103, 38);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(547, 21);
            this.txtProjectName.TabIndex = 1;
            // 
            // CoStatus
            // 
            this.CoStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoStatus.FormattingEnabled = true;
            this.CoStatus.Location = new System.Drawing.Point(321, 104);
            this.CoStatus.Name = "CoStatus";
            this.CoStatus.Size = new System.Drawing.Size(331, 23);
            this.CoStatus.TabIndex = 6;
            this.CoStatus.SelectedIndexChanged += new System.EventHandler(this.CoStatus_SelectedIndexChanged);
            this.CoStatus.Click += new System.EventHandler(this.CoStatus_Click);
            // 
            // txtComplete
            // 
            this.txtComplete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplete.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.txtComplete.Location = new System.Drawing.Point(321, 137);
            this.txtComplete.Name = "txtComplete";
            this.txtComplete.Size = new System.Drawing.Size(60, 21);
            this.txtComplete.TabIndex = 12;
            this.txtComplete.ValueChanged += new System.EventHandler(this.txtComplete_ValueChanged);
            this.txtComplete.Click += new System.EventHandler(this.txtComplete_Click);
            // 
            // CoCustomers
            // 
            this.CoCustomers.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CoCustomers.FormattingEnabled = true;
            this.CoCustomers.Location = new System.Drawing.Point(103, 65);
            this.CoCustomers.Name = "CoCustomers";
            this.CoCustomers.Size = new System.Drawing.Size(545, 23);
            this.CoCustomers.TabIndex = 17;
            // 
            // txtProjectContent
            // 
            this.txtProjectContent.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectContent.Location = new System.Drawing.Point(20, 180);
            this.txtProjectContent.Name = "txtProjectContent";
            this.txtProjectContent.Size = new System.Drawing.Size(630, 232);
            this.txtProjectContent.TabIndex = 19;
            this.txtProjectContent.Text = "";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStrip1.BackgroundImage")));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSaveClose,
            this.cmdSaveNew,
            this.cmdDel});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(691, 25);
            this.toolStrip1.TabIndex = 32;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdSaveClose
            // 
            this.cmdSaveClose.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmdSaveClose.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveClose.Image")));
            this.cmdSaveClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdSaveClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveClose.Name = "cmdSaveClose";
            this.cmdSaveClose.Size = new System.Drawing.Size(95, 22);
            this.cmdSaveClose.Text = "Save && Close";
            this.cmdSaveClose.Click += new System.EventHandler(this.cmdSaveClose_Click);
            // 
            // cmdSaveNew
            // 
            this.cmdSaveNew.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmdSaveNew.ForeColor = System.Drawing.Color.Navy;
            this.cmdSaveNew.Image = ((System.Drawing.Image)(resources.GetObject("cmdSaveNew.Image")));
            this.cmdSaveNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSaveNew.Name = "cmdSaveNew";
            this.cmdSaveNew.Size = new System.Drawing.Size(94, 22);
            this.cmdSaveNew.Text = "Save && New";
            this.cmdSaveNew.Click += new System.EventHandler(this.cmdSaveNew_Click);
            // 
            // cmdDel
            // 
            this.cmdDel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmdDel.ForeColor = System.Drawing.Color.Navy;
            this.cmdDel.Image = ((System.Drawing.Image)(resources.GetObject("cmdDel.Image")));
            this.cmdDel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdDel.Name = "cmdDel";
            this.cmdDel.Size = new System.Drawing.Size(63, 22);
            this.cmdDel.Text = "Delete";
            this.cmdDel.Click += new System.EventHandler(this.cmdDel_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.panel1.Location = new System.Drawing.Point(20, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(630, 1);
            this.panel1.TabIndex = 33;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(147)))), ((int)(((byte)(207)))));
            this.panel3.Location = new System.Drawing.Point(20, 173);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(630, 1);
            this.panel3.TabIndex = 35;
            // 
            // txtProjectID
            // 
            this.txtProjectID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectID.Location = new System.Drawing.Point(103, 11);
            this.txtProjectID.Name = "txtProjectID";
            this.txtProjectID.Size = new System.Drawing.Size(130, 21);
            this.txtProjectID.TabIndex = 121;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label5.Location = new System.Drawing.Point(457, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 124;
            this.label5.Text = "Giá trị";
            // 
            // txtProjectPrice
            // 
            this.txtProjectPrice.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectPrice.Location = new System.Drawing.Point(502, 136);
            this.txtProjectPrice.Name = "txtProjectPrice";
            this.txtProjectPrice.Size = new System.Drawing.Size(146, 21);
            this.txtProjectPrice.TabIndex = 123;
            this.txtProjectPrice.Text = "0";
            this.txtProjectPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage7);
            this.tabControl4.Controls.Add(this.tabPage8);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(0, 25);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(691, 479);
            this.tabControl4.TabIndex = 125;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.txtProjectName);
            this.tabPage7.Controls.Add(this.label5);
            this.tabPage7.Controls.Add(this.txtProjectPrice);
            this.tabPage7.Controls.Add(this.label18);
            this.tabPage7.Controls.Add(this.label1);
            this.tabPage7.Controls.Add(this.label20);
            this.tabPage7.Controls.Add(this.CoStatus);
            this.tabPage7.Controls.Add(this.txtProjectID);
            this.tabPage7.Controls.Add(this.tnPickerDueDate);
            this.tabPage7.Controls.Add(this.label22);
            this.tabPage7.Controls.Add(this.tnPickerStartDate);
            this.tabPage7.Controls.Add(this.label24);
            this.tabPage7.Controls.Add(this.txtComplete);
            this.tabPage7.Controls.Add(this.panel3);
            this.tabPage7.Controls.Add(this.panel1);
            this.tabPage7.Controls.Add(this.label26);
            this.tabPage7.Controls.Add(this.CoCustomers);
            this.tabPage7.Controls.Add(this.txtProjectContent);
            this.tabPage7.Controls.Add(this.label27);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(683, 453);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "Thông tin hợp đồng";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label18.Location = new System.Drawing.Point(17, 109);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 15);
            this.label18.TabIndex = 3;
            this.label18.Text = "Ngày bắt đầu";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label20.Location = new System.Drawing.Point(17, 136);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(80, 15);
            this.label20.TabIndex = 5;
            this.label20.Text = "Ngày kết thúc";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label22.Location = new System.Drawing.Point(230, 109);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 15);
            this.label22.TabIndex = 7;
            this.label22.Text = "Tình trạng";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label24.Location = new System.Drawing.Point(230, 139);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 15);
            this.label24.TabIndex = 11;
            this.label24.Text = "% Hoàn thành";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label26.Location = new System.Drawing.Point(17, 44);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 15);
            this.label26.TabIndex = 16;
            this.label26.Text = "Diễn giải";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label27.Location = new System.Drawing.Point(17, 68);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 15);
            this.label27.TabIndex = 18;
            this.label27.Text = "Khách hàng";
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(683, 453);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Công việc của hợp đồng";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tnPickerDueDate
            // 
            this.tnPickerDueDate.CustomFormat = "dd/MM/yyyy";
            this.tnPickerDueDate.Location = new System.Drawing.Point(103, 135);
            this.tnPickerDueDate.Name = "tnPickerDueDate";
            this.tnPickerDueDate.Size = new System.Drawing.Size(102, 20);
            this.tnPickerDueDate.TabIndex = 120;
            this.tnPickerDueDate.Value = new System.DateTime(((long)(0)));
            // 
            // tnPickerStartDate
            // 
            this.tnPickerStartDate.CustomFormat = "dd/MM/yyyy";
            this.tnPickerStartDate.Location = new System.Drawing.Point(105, 109);
            this.tnPickerStartDate.Name = "tnPickerStartDate";
            this.tnPickerStartDate.Size = new System.Drawing.Size(102, 20);
            this.tnPickerStartDate.TabIndex = 119;
            this.tnPickerStartDate.Value = new System.DateTime(((long)(0)));
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 122;
            this.label1.Text = "Hợp đồng số";
            // 
            // FrmProjectDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(691, 504);
            this.Controls.Add(this.tabControl4);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmProjectDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin hợp đồng";
            this.Load += new System.EventHandler(this.FrmProjectDetail_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmProjectDetail_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.txtComplete)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtProjectName;
        private System.Windows.Forms.ComboBox CoStatus;
        private System.Windows.Forms.NumericUpDown txtComplete;
        private System.Windows.Forms.ComboBox CoCustomers;
        private System.Windows.Forms.RichTextBox txtProjectContent;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton cmdSaveClose;
        private System.Windows.Forms.ToolStripButton cmdSaveNew;
        private System.Windows.Forms.ToolStripButton cmdDel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private TNLibrary.SYS.Forms.TNDateTimePicker tnPickerDueDate;
        private System.Windows.Forms.TextBox txtProjectID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProjectPrice;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private TNLibrary.SYS.Forms.TNDateTimePicker tnPickerStartDate;
    }
}