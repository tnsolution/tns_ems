using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TN_UI.FRM.Reports;


namespace TN_UI.FRM.CRM
{
    public partial class FrmCustomerList : Form
    {
        private ReportDocument rpt = new ReportDocument();
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private DataTable mCustomerList = new DataTable();

        public string mCustomerID = "";
        public bool FlagView;
        public string mTitle = "DANH SÁCH NHÀ CUNG CẤP";
        public int mCustomerType = 1;

        public FrmCustomerList()
        {
            InitializeComponent();
            InitLayoutForm();
        }

        private void FrmCustomerList_Load(object sender, EventArgs e)
        {
            CheckRole();

            InitListView(ListViewData);
            cmdHide_Click(null, EventArgs.Empty);
            txtTitle.Text = mTitle;
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            if (mCustomerType == 1)
                mCustomerList = Customers_Data.VendorList();
            else
                mCustomerList = Customers_Data.CustomerList();


            LoadData();

        }
        private void LoadData()
        {

            if (ListViewData.Visible)
                LoadDataToListView(ListViewData);
            else
                LoadDataToCrystalReport();
        }
        //Layout 
        private void InitLayoutForm()
        {

            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);

            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);

            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);

            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);

            this.cmdUnhide.MouseLeave += new System.EventHandler(this.cmdUnhide_MouseLeave);
            this.cmdUnhide.Click += new System.EventHandler(this.cmdUnhide_Click);
            this.cmdUnhide.MouseEnter += new System.EventHandler(this.cmdUnhide_MouseEnter);

            this.cmdHide.MouseLeave += new System.EventHandler(this.cmdHide_MouseLeave);
            this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
            this.cmdHide.MouseEnter += new System.EventHandler(this.cmdHide_MouseEnter);

            panelSearch.Height = 0;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            cmdUnhideCenter.Image = imageList3.Images[0];

            ListViewData.Dock = DockStyle.Fill;
            ReportViewer.Dock = DockStyle.Fill;
            ReportViewer.DisplayGroupTree = false;
        }

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Lay out left  ]

        private void cmdHide_MouseEnter(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[1];
        }

        private void cmdHide_MouseLeave(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[0];
        }

        private void cmdHide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 28;  //  ContainerLeft.SplitterDistance = 10;
            TreeViewData.Visible = false;

            cmdHide.Visible = false;
            txtTitleLeft.Visible = false;
            panelLineLeft.Visible = false;
            panelViewLeft.Visible = false;

            cmdUnhide.Visible = true;
        }


        private void cmdUnhide_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[3];
        }

        private void cmdUnhide_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[2];
        }

        private void cmdUnhide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 250;// ContainerLeft.SplitterDistance = 260;
            TreeViewData.Visible = true;
            cmdHide.Visible = true;
            txtTitleLeft.Visible = true;
            panelLineLeft.Visible = true;
            panelViewLeft.Visible = true;

            cmdUnhide.Visible = false;

        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            cmdHide.Left = panelTitle.Width - cmdHide.Width;
        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "Mã số";
            colHead.Width = 130;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên";
            colHead.Width = 280;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Điện Thoại";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Số Thuế";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Địa Chỉ";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "TP/Tỉnh";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            LV.SmallImageList = SmallImageLV;
            LV.Sorting = System.Windows.Forms.SortOrder.Ascending;
            LVGroup = new ListViewMyGroup(LV);

        }
        // Load Database
        public void LoadDataToListView(ListView LV)
        {
            this.Cursor = Cursors.WaitCursor;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = mCustomerList.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = mCustomerList.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = nRow["CustomerID"].ToString();
                lvi.Tag = nRow["CustomerKey"]; // Set the tag to 

                lvi.ForeColor = Color.DarkBlue;
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Phone"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TaxNumber"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Address"].ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CityName"].ToString();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

            this.Cursor = Cursors.Default;

        }

        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListViewData.SelectedItems[0].BackColor = Color.White;
            }
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                int nCustomerKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                FrmCustomerDetail frm = new FrmCustomerDetail();
                frm.mCustomerTypeUI = mCustomerType;
                frm.mCustomerKey = nCustomerKey;
                frm.ShowDialog();

                if (mCustomerType == 1)
                    mCustomerList = Customers_Data.VendorList();
                else
                    mCustomerList = Customers_Data.CustomerList();

                radioViewListView.Checked = true;
                ListViewData.Focus();
                string nCustomerID = frm.mCustomerID;
                if (nCustomerID != null && nCustomerID.Trim().Length > 0)
                {
                    Customer_Info nCustomer = new Customer_Info(nCustomerID);
                    SelectIndexInListView(nCustomer.Key);
                }
            }
            else
            {
                MessageBox.Show("Bạn phải chọn khách hàng");
            }
            LoadData();
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)//Sk xoa kh bang tay
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)//xóa rows
                {
                    int nResult = Customers_Data.CheckExistCustomer((int)ListViewData.SelectedItems[0].Tag);
                    if (nResult > 0)
                    {
                        MessageBox.Show("Không thể xóa khách hàng này, vui lòng kiểm tra lại!");
                    }
                    else
                    {
                        if (MessageBox.Show("Bạn có muốn xóa khách hàng : " + ListViewData.SelectedItems[0].SubItems[1].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            int nIndex = ListViewData.SelectedItems[0].Index;
                            Customer_Info nCustomer = new Customer_Info();
                            nCustomer.Key = (int)ListViewData.SelectedItems[0].Tag;
                            nCustomer.Delete();

                            ListViewData.SelectedItems[0].Remove();
                            if (ListViewData.Items.Count > 0)
                                if (nIndex == 0)
                                    ListViewData.Items[nIndex].Selected = true;
                                else
                                    ListViewData.Items[nIndex - 1].Selected = true;
                            ListViewData.Focus();
                        }
                    }
                }
                else//xóa rows
                {
                    MessageBox.Show("Bạn phải chọn khách hàng để xóa");
                }
            }
        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        #endregion

        #region [ CrystalReport ]
        private void LoadDataToCrystalReport()
        {
            this.Cursor = Cursors.WaitCursor;
            if (mCustomerType == 1)
                rpt.Load("FilesReport\\CrystalReportSupplier.rpt");
            else
                rpt.Load("FilesReport\\CrystalReporCustomers.rpt");



            DataTable nCustomersDetail = TableReport.CustomersDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            foreach (DataRow nRow in mCustomerList.Rows)
            {
                DataRow nRowDetail = nCustomersDetail.NewRow();
                nRowDetail["CustomerID"] = nRow["CustomerID"].ToString();
                nRowDetail["CustomerName"] = nRow["CustomerName"].ToString().Trim();
                nRowDetail["Phone"] = nRow["Phone"].ToString().Trim();
                nRowDetail["TaxNumber"] = nRow["TaxNumber"].ToString().Trim();
                nRowDetail["Address"] = nRow["Address"].ToString().Trim();
                nRowDetail["CityName"] = nRow["CityName"].ToString().Trim();

                nCustomersDetail.Rows.Add(nRowDetail);

            }

            rpt.Database.Tables["CustomersDetail"].SetDataSource(nCustomersDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);

            ReportViewer.ReportSource = rpt;
            ReportViewer.Zoom(1);

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region [ Export ]
        private void cmdExcel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable nTableCustomer;
            nTableCustomer = Customers_Data.CustomerList();

            DataTable nCustomersDetail = TableReport.CustomersDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            foreach (DataRow nRow in mCustomerList.Rows)
            {
                DataRow nRowDetail = nCustomersDetail.NewRow();
                nRowDetail["CustomerID"] = nRow["CustomerID"].ToString();
                nRowDetail["CustomerName"] = nRow["CustomerName"].ToString().Trim();
                nRowDetail["Phone"] = nRow["Phone"].ToString().Trim();
                nRowDetail["TaxNumber"] = nRow["TaxNumber"].ToString().Trim();
                nRowDetail["Address"] = nRow["Address"].ToString().Trim();
                nRowDetail["CityName"] = nRow["CityName"].ToString().Trim();

                nCustomersDetail.Rows.Add(nRowDetail);

            }

            ExportToExcel(nCustomersDetail, nCompanyInfo);

            this.Cursor = Cursors.Default;
        }

        private void ExportToExcel(DataTable TableDetail, DataTable TableCompanyInfo)
        {
            // Create an Excel object and add workbook...
            string nFileName, nFileType;
            SaveFileDialog nDialogSave = new SaveFileDialog();
            nDialogSave.DefaultExt = "xls";
            nDialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            nDialogSave.Title = "Save a file to...";

            //save.ShowDialog();

            if (nDialogSave.ShowDialog() == DialogResult.OK)
            {
                nFileName = nDialogSave.FileName;
                nFileType = System.IO.Path.GetExtension(nFileName);

                /////////////////////////
                Excel.Application nExcel = new Excel.Application();
                Excel.Workbook nWorkbook = nExcel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet nExSheet = (Excel.Worksheet)nWorkbook.Worksheets[1];

                nExcel.ActiveWindow.DisplayGridlines = true;

                // Add Head Infor
                DataRow nRowHead = TableCompanyInfo.Rows[0];
                nExcel.Cells[1, 1] = nRowHead["CompanyName"].ToString();
                nExcel.Cells[2, 1] = nRowHead["Address"].ToString();

                //Add Tieu de
                nExcel.Cells[4, 3] = "DANH SÁCH NHÀ CUNG CẤP";
                //add Column Name
                nExcel.Cells[6, 1] = "Mã Khách Hàng";
                nExcel.Cells[6, 2] = "Tên Khách Hàng";
                nExcel.Cells[6, 3] = "Số Điện Thoại";
                nExcel.Cells[6, 4] = "Mã Số Thuế";
                nExcel.Cells[6, 5] = "Địa CHỉ";
                nExcel.Cells[6, 6] = "Tên TP/Tỉnh";


                int iCol = 0;
                int iRow = 0;

                foreach (DataRow nRow in TableDetail.Rows)
                {
                    iRow++;

                    // add each row's cell data...
                    iCol = 0;
                    foreach (DataColumn nColumn in TableDetail.Columns)
                    {
                        iCol++;
                        nExcel.Cells[iRow + 6, iCol] = nRow[nColumn.ColumnName];
                    }
                }

                //add Footer
                nExcel.Cells[iRow + 9, 1] = "    Người ghi sổ";
                nExcel.Cells[iRow + 9, 3] = "Kế toán trưởng";
                nExcel.Cells[iRow + 9, 6] = "        Giám đốc";

                ////tao border
                Excel.Range nRange;
                Excel.Worksheet nWorksheet1 = (Excel.Worksheet)nExcel.ActiveSheet;
                //dinh dang tieu de
                nRange = nWorksheet1.get_Range("A4", "F4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;

                //dinh dang footer
                int nRo1 = iRow + 9;
                int nRo2 = iRow + 10;
                nRange = nWorksheet1.get_Range("A" + nRo1, "F" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                nRange.Font.Size = 13;
                nRange = nWorksheet1.get_Range("A" + nRo2, "F" + nRo2);
                nRange.Font.Bold = true;

                //dinh dang columns Name
                nRange = nWorksheet1.get_Range("A6", "F6");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Cells.RowHeight = 25;
                nRange.Interior.Color = Color.Silver.ToArgb();

                //dinh dang Columns width
                nRange = nWorksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 45;

                nRange = nWorksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 45;

                nRange = nWorksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 20;

                // chon vung tao border
                int x = iRow + 6;
                nRange = nWorksheet1.get_Range("A6", "F" + x);
                nRange.Borders.Weight = 2;

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo nFileSave = new FileInfo(nFileName);
                if (nFileSave.Exists)
                    nFileSave.Delete(); // delete the file if it already exist.

                nExcel.Visible = false;
                nWorkbook.SaveAs(nFileName,
                Excel.XlFileFormat.xlWorkbookNormal, null, null,
                false, false, Excel.XlSaveAsAccessMode.xlExclusive,
                false, false, false, false, false);


                nWorkbook.Close(false, false, false);
                nExcel.Workbooks.Close();
                nExcel.Application.Quit();
                nExcel.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(nWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExcel);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nRange);
                nExSheet = null;
                nWorkbook = null;
                nExcel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

        }
        #endregion

        #region [ Import ]
        private void cmdImport_Click(object sender, EventArgs e)
        {
            int nCategoryKey = 1;

            FrmImportCustomer frm = new FrmImportCustomer();
            frm.mCategoryKey = nCategoryKey;
            frm.mCustomerType = mCustomerType;
            frm.ShowDialog();

            if (mCustomerType == 1)
                mCustomerList = Customers_Data.VendorList();
            else
                mCustomerList = Customers_Data.CustomerList();

            LoadData();

        }
        #endregion

        #region [ Activate on Button ]
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            if (mCustomerType == 1)
                mCustomerList = Customers_Data.VendorList(txtCustomerID.Text.Trim(), txtCustomerName.Text.Trim(), txtTaxNumber.Text.Trim(), txtTel.Text);
            else
                mCustomerList = Customers_Data.CustomerList(txtCustomerID.Text.Trim(), txtCustomerName.Text.Trim(), txtTaxNumber.Text.Trim(), txtTel.Text);

            LoadData();


        }
        private void cmdNew_Click(object sender, EventArgs e)
        {

            FrmCustomerDetail frm = new FrmCustomerDetail();
            frm.mCustomerTypeUI = mCustomerType;
            frm.ShowDialog();

            if (mCustomerType == 1)
                mCustomerList = Customers_Data.VendorList();
            else
                mCustomerList = Customers_Data.CustomerList();

            radioViewListView.Checked = true;
            ListViewData.Focus();
            string nCustomerID = frm.mCustomerID;
            if (nCustomerID != null && nCustomerID.Trim().Length > 0)
            {
                Customer_Info nCustomer = new Customer_Info(nCustomerID);
                SelectIndexInListView(nCustomer.Key);
            }

            LoadData();
        }

        private void radioViewListView_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = true;
            ReportViewer.Visible = false;
            LoadDataToListView(ListViewData);

        }
        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = false;
            ReportViewer.Visible = true;
            LoadDataToCrystalReport();
        }

        #endregion

        private void SelectIndexInListView(int CustomerKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == CustomerKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }

        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            if (mCustomerType == 1)
                nRole.Check_Role("CRM002");
            else
                nRole.Check_Role("CRM004");

            if (nRole.Del)
                m_RoleDelete = true;

            if (!nRole.Edit)
                cmdImport.Enabled = false;
        }

      
    }
}



