﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.CRM;
using TNLibrary.SYS;

namespace TN_UI.FRM.CRM
{
    public partial class FrmTaskDetail : Form
    {
        public int mTaskKey = 0;
        private Task_Info mTask;
        public FrmTaskDetail()
        {
            InitializeComponent();
        }

        private void FrmTaskDetail_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(CoCategory, "SELECT CategoryKey,CategoryName FROM CRM_TaskCategories", false);
            LoadDataToToolbox.ComboBoxData(CoStatus, "SELECT StatusKey,StatusName FROM CRM_TaskStatus", false);
            LoadDataToToolbox.ComboBoxData(CoProjects, "SELECT ProjectKey,ProjectName FROM CRM_Projects", true);
            LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers", true);

            mTask = new Task_Info(mTaskKey);
            if (mTaskKey == 0)
            {
                CoCategory.SelectedIndex = 0;
                CoPriority.SelectedIndex = 1;
                CoStatus.SelectedIndex = 0;
                txtComplete.Tag = 0;
            }
            else
            {
                LoadTaskInfo();
            }
        }
        private void LoadTaskInfo()
        {
            txtTaskName.Text = mTask.TaskName;

            if (mTask.StartDate != null)
                tnPickerStartDate.Value = mTask.StartDate.Value;
            else
                tnPickerStartDate.Text = "";

            if (mTask.DueDate != null)
                tnPickerDueDate.Value = mTask.DueDate.Value;
            else
                tnPickerDueDate.Text = "";

            CoStatus.SelectedValue = mTask.StatusKey;

            CoPriority.Text = mTask.Priority;
            txtComplete.Value = mTask.Complete;
            txtComplete.Tag = txtComplete.Value;

            CoCategory.SelectedValue = mTask.CategoryKey;
            CoProjects.SelectedValue = mTask.ProjectKey;
            CoCustomers.SelectedValue = mTask.CustomerKey;

            txtTaskContent.Text = mTask.TaskContent;
        }
        #region [ Activate Button ]
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa công việc này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mTask.Delete();
                ClearTaskInfo();
            }
        }
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {

            if (SaveTaskInfomation())
            {
                mTask.Save();
                this.Close();

            }
            else
                this.Close();

        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {

            if (SaveTaskInfomation())
            {
                mTask.Save();

                ClearTaskInfo();
            }
        }
        #endregion


        #region [ Update Invoice DataBase]

        private bool SaveTaskInfomation()
        {
            if (!CheckBeforeSave()) return false;

            mTask.TaskName = txtTaskName.Text;
            if (tnPickerStartDate.Value.Year == 0001)
                mTask.StartDate = null;
            else
                mTask.StartDate = tnPickerStartDate.Value;

            if (tnPickerDueDate.Value.Year == 0001)
                mTask.DueDate = null;
            else
                mTask.DueDate = tnPickerDueDate.Value;

            mTask.StatusKey = (int)CoStatus.SelectedValue;
            mTask.Priority = CoPriority.Text;
            mTask.Complete = (int)txtComplete.Value;
            mTask.CategoryKey = (int)CoCategory.SelectedValue;
            mTask.ProjectKey = (int)CoProjects.SelectedValue;
            mTask.CustomerKey = (int)CoCustomers.SelectedValue;
            mTask.TaskContent = txtTaskContent.Text;

            mTask.CreatedBy = SessionUser.UserLogin.Key;
            mTask.ModifiedBy = SessionUser.UserLogin.Key;


            return true;

        }
        #endregion

        #region [ Function]
        private bool CheckBeforeSave()
        {
            if (txtTaskName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên công việc");
                txtTaskName.Focus();
                return false;
            }

            return true;
        }
        private void ClearTaskInfo()
        {

            mTask = new Task_Info();

            txtTaskName.Text = "";
            tnPickerDueDate.Text = "";
            tnPickerStartDate.Text = "";

            CoPriority.SelectedIndex = 1;
            txtComplete.Value = 0;
            CoProjects.SelectedIndex = 0;
            CoCustomers.SelectedIndex = 0;
            txtTaskContent.Text = "";

            txtTaskName.Focus();
        }

        #endregion
        private bool FlagStatusSelect,FlagCompleteSelect;
        private void CoStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FlagStatusSelect)
            {
                if (CoStatus.SelectedIndex == 3)
                {
                    txtComplete.Tag = txtComplete.Value;
                    txtComplete.Value = 100;
                }
                else
                {
                    if (txtComplete.Tag != null)
                        txtComplete.Value = int.Parse(txtComplete.Tag.ToString());
                }
                FlagStatusSelect = false;
            }
        }

        private void txtComplete_ValueChanged(object sender, EventArgs e)
        {
            if (FlagCompleteSelect)
            {
                if (txtComplete.Value == 100)
                {
                    CoStatus.SelectedIndex = 3;
                }
                else
                {
                    if (CoStatus.SelectedIndex == 3)
                        CoStatus.SelectedIndex = 1;
                }
                FlagCompleteSelect = false;
            }
        }

        private void CoStatus_Click(object sender, EventArgs e)
        {
            FlagStatusSelect = true;
        }

        private void txtComplete_Click(object sender, EventArgs e)
        {
            FlagCompleteSelect = true;
        }



    }
}
