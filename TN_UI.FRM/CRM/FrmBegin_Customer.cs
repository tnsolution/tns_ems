using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TNLibrary.FNC;
using TNLibrary.FNC.CloseMonth;
using TNLibrary.IVT;

namespace TN_UI.FRM.CRM
{
    public partial class FrmBegin_Customer : Form
    {
        public int mCustomerType = 1;

        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private string mFormatCurrencyMainTwoPoint = GlobalSystemConfig.FormatCurrencyMainTwoPoint;
        private string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;

        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private string mCurrencyMain = GlobalSystemConfig.CurrencyMain;
        private string mFormatDate = GlobalSystemConfig.FormatDate;
        private string mDefaultAccountCustomer = GlobalSystemConfig.AccountCustomer;

        private string mCloseMonth = "";
        public FrmBegin_Customer()
        {
            InitializeComponent();

        }
        private void FrmBegin_Customer_Load(object sender, EventArgs e)
        {
            CheckRole();

            FinancialYear_Info nYear = new FinancialYear_Info(true);
            mCloseMonth = nYear.FromMonth;
            SetupLayoutGridView(dataGridView1);

            LoadDataToToolbox.ComboBoxData(CoCategories, "SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories", true);

            panelSearch.Height = 00;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            this.WindowState = FormWindowState.Maximized;

            SearchCustomer();
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            SearchCustomer();
        }
        private void SearchCustomer()
        {
            int nCategoryKey = int.Parse(CoCategories.SelectedValue.ToString());

            DataTable nTable;
            nTable = CloseMonth_Data.CustomerVendor(mCloseMonth, nCategoryKey, txtSearchCustomerID.Text.Trim(), txtSearchCustomerName.Text.Trim());
            PopulateDataGridView(dataGridView1, nTable);

        }
        #region [ GridView ]
        private bool FlagEdit;
        private void SetupLayoutGridView(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("CustomerID", "Mã Số");
            GV.Columns.Add("CustomerName", "Tên");
            GV.Columns.Add("BeginAmountDebitCurrencyForeign", "");
            GV.Columns.Add("BeginAmountDebitCurrencyMain", "");
            GV.Columns.Add("BeginAmountCreditCurrencyForeign", "");
            GV.Columns.Add("BeginAmountCreditCurrencyMain", "");
            DataGridViewImageColumn nColumn = new DataGridViewImageColumn();
            nColumn.Name = "Delete";
            GV.Columns.Add(nColumn);

            GV.Columns[0].Width = 60;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[0].ReadOnly = true;

            GV.Columns[1].Width = 120;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 300;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns[2].ReadOnly = true;

            GV.Columns[3].Width = 150;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 150;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[5].Width = 150;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 150;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 30;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            // setup style view

            GV.BackgroundColor = Color.White;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;
            // GV.ReadOnly = true;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * 3 / 2;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.Paint += new PaintEventHandler(GV_Paint);

            GV.UserAddedRow += new DataGridViewRowEventHandler(GV_UserAddedRow);
            GV.RowEnter += new DataGridViewCellEventHandler(GV_RowEnter);
            GV.CellEndEdit += new DataGridViewCellEventHandler(GV_CellEndEdit);
            GV.RowValidated += new DataGridViewCellEventHandler(GV_RowValidated);
            GV.KeyDown += new KeyEventHandler(this.GV_KeyDown);
            GV.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(GV_EditingControlShowing);
            GV.CellMouseMove += new DataGridViewCellMouseEventHandler(this.GV_CellMouseMove);
            GV.CellContentClick += new DataGridViewCellEventHandler(this.GV_CellContentClick);

            GV.Rows[0].Cells["No"].Value = 1;
            GV.Rows[0].Tag = 0;
            GV.Rows[0].Cells["Delete"].Value = ImageListGV.Images[0];

            if (!mRole.Edit)
                GV.ReadOnly = true;
        }
        private void GV_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;
            string[] TitleMerge = { "Phải thu", "Phải trả" };
            string[] TitleColumn = { "STT", " Mã số ", "Tên " , "Ngoại tệ", mCurrencyMain, "Ngoại tệ", mCurrencyMain, "" };

            int nColumnBeginMerge = 3;

            int nColumnTotal = GV.ColumnCount;

            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;

                if (i >= nColumnBeginMerge || i == 8) // binh thuong
                {
                    r1.Height = r1.Height / 2 - 2;
                    r1.Width = r1.Width - 2;
                    r1.Y = r1.Height + 3;
                    g.DrawImage(image, r1);
                }
                else // merge
                {
                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;
                    g.DrawImage(image, r1);
                }


                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }

            int k = 0;
            for (int j = nColumnBeginMerge; j < nColumnTotal - 1; j += 2)
            {

                Rectangle r1 = GV.GetCellDisplayRectangle(j, -1, true);

                int w2 = GV.GetCellDisplayRectangle(j + 1, -1, true).Width;

                r1.X += 1;
                r1.Y += 1;

                r1.Width = r1.Width + w2 - 2;
                r1.Height = r1.Height / 2 - 2;

                g.DrawImage(image, r1);
                // g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                k++;
            }

            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }

        private void PopulateDataGridView(DataGridView GV, DataTable TableView)
        {
            GV.Rows.Clear();
            int n = TableView.Rows.Count;
            int i = 0;
            for (i = 0; i < n; i++)
            {

                DataRow nRow = TableView.Rows[i];

                double BeginAmountDebitCurrencyMain = double.Parse(nRow["BeginAmountDebitCurrencyMain"].ToString());
                double BeginAmountCreditCurrencyMain = double.Parse(nRow["BeginAmountCreditCurrencyMain"].ToString());
                double BeginAmountDebitCurrencyForeign = double.Parse(nRow["BeginAmountDebitCurrencyForeign"].ToString());
                double BeginAmountCreditCurrencyForeign = double.Parse(nRow["BeginAmountCreditCurrencyForeign"].ToString());

                GV.Rows.Add();
                GV.Rows[i].Tag = (int)nRow["CloseAccountKey"];
                GV.Rows[i].Cells["No"].Value = GV.Rows.Count - 1;
                GV.Rows[i].Cells["CustomerID"].Value = nRow["CustomerID"].ToString().Trim();
                GV.Rows[i].Cells["CustomerID"].Tag = (int)nRow["CustomerKey"];
                GV.Rows[i].Cells["CustomerName"].Value = nRow["CustomerName"].ToString().Trim();

                GV.Rows[i].Cells["BeginAmountDebitCurrencyForeign"].Value = BeginAmountDebitCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
                GV.Rows[i].Cells["BeginAmountDebitCurrencyMain"].Value = BeginAmountDebitCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["BeginAmountCreditCurrencyForeign"].Value = BeginAmountCreditCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
                GV.Rows[i].Cells["BeginAmountCreditCurrencyMain"].Value = BeginAmountCreditCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                GV.Rows[i].Cells["Delete"].Value = ImageListGV.Images[1];

            }
            GV.Rows[i].Cells["No"].Value = GV.Rows.Count;
            GV.Rows[i].Tag = 0;
            GV.Rows[i].Cells["Delete"].Value = ImageListGV.Images[0];

        }

        private void GV_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LemonChiffon;
            FlagEdit = false;
        }
        private void GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            FlagEdit = true;
            DataGridViewRow nRowEdit = dataGridView1.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 1: // thong tin ve san pham
                    if (nRowEdit.Cells[e.ColumnIndex].Value == null)
                    {
                        nRowEdit.Cells["CustomerID"].ErrorText = "";
                        nRowEdit.Cells["CustomerID"].Tag = 0;
                    }
                    else
                    {
                        string nCustomerID = nRowEdit.Cells[e.ColumnIndex].Value.ToString(); //;

                        Customer_Info nCustomer = new Customer_Info(nCustomerID);
                        if (nCustomer.Key > 0)
                        {
                            nRowEdit.Cells["CustomerID"].Tag = nCustomer.Key;
                            nRowEdit.Cells["CustomerID"].ErrorText = "";
                            nRowEdit.Cells["CustomerName"].Value = nCustomer.Name;
                            nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].Value = "0";
                            nRowEdit.Cells["BeginAmountDebitCurrencyMain"].Value = "0";
                            nRowEdit.Cells["BeginAmountCreditCurrencyForeign"].Value = "0";
                            nRowEdit.Cells["BeginAmountCreditCurrencyMain"].Value = "0";


                        }
                        else
                        {
                            nRowEdit.Cells["CustomerID"].Tag = 0;
                            nRowEdit.Cells["CustomerID"].ErrorText = "Không có sản phẩm này";
                            nRowEdit.Cells["CustomerName"].Value = "";
                        }
                    }
                    break;


                case 3:
                    double nBeginAmountDebitCurrencyForeign = 0;
                    {
                        if (nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].Value == null)
                            nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].ErrorText = "Please check item";
                        else
                        {
                            if (double.TryParse(nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBeginAmountDebitCurrencyForeign))
                            {
                                nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].Value = nBeginAmountDebitCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
                                nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].ErrorText = "";
                            }
                            else
                                nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].ErrorText = "Sai định dạng tiền tệ";
                        }
                    }
                    break;
                case 4:
                    double nBeginAmountDebitCurrencyMain = 0;
                    {
                        if (nRowEdit.Cells["BeginAmountDebitCurrencyMain"].Value == null)
                            nRowEdit.Cells["BeginAmountDebitCurrencyMain"].ErrorText = "Please check item";
                        else
                        {
                            if (double.TryParse(nRowEdit.Cells["BeginAmountDebitCurrencyMain"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBeginAmountDebitCurrencyMain))
                            {
                                nRowEdit.Cells["BeginAmountDebitCurrencyMain"].Value = nBeginAmountDebitCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                                nRowEdit.Cells["BeginAmountDebitCurrencyMain"].ErrorText = "";
                            }
                            else
                                nRowEdit.Cells["BeginAmountDebitCurrencyMain"].ErrorText = "Sai định dạng tiền tệ";
                        }
                    }
                    break;
                case 5:
                    double nBeginAmountCreditCurrencyForeign = 0;
                    {
                        if (nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].Value == null)
                            nRowEdit.Cells["BeginAmountDebitCurrencyForeign"].ErrorText = "Please check item";
                        else
                        {
                            if (double.TryParse(nRowEdit.Cells["BeginAmountCreditCurrencyForeign"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBeginAmountCreditCurrencyForeign))
                            {
                                nRowEdit.Cells["BeginAmountCreditCurrencyForeign"].Value = nBeginAmountCreditCurrencyForeign.ToString(mFormatCurrencyForeign, mFormatProviderCurrency);
                                nRowEdit.Cells["BeginAmountCreditCurrencyForeign"].ErrorText = "";
                            }
                            else
                                nRowEdit.Cells["BeginAmountCreditCurrencyForeign"].ErrorText = "Sai định dạng tiền tệ";
                        }
                    }
                    break;
                case 6:
                    double nBeginAmountCreditCurrencyMain = 0;
                    {
                        if (nRowEdit.Cells["BeginAmountCreditCurrencyMain"].Value == null)
                            nRowEdit.Cells["BeginAmountCreditCurrencyMain"].ErrorText = "Please check item";
                        else
                        {
                            if (double.TryParse(nRowEdit.Cells["BeginAmountCreditCurrencyMain"].Value.ToString(), NumberStyles.Any, mFormatProviderCurrency, out nBeginAmountCreditCurrencyMain))
                            {
                                nRowEdit.Cells["BeginAmountCreditCurrencyMain"].Value = nBeginAmountCreditCurrencyMain.ToString(mFormatCurrencyMain, mFormatProviderCurrency);
                                nRowEdit.Cells["BeginAmountCreditCurrencyMain"].ErrorText = "";
                            }
                            else
                                nRowEdit.Cells["BeginAmountCreditCurrencyMain"].ErrorText = "Sai định dạng tiền tệ";
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        private void GV_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["No"].Value = dataGridView1.Rows.Count;
            e.Row.Tag = 0;
            e.Row.Cells["Delete"].Value = ImageListGV.Images[0];
        }
        private void GV_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow nRowCurrent = dataGridView1.Rows[e.RowIndex];

            nRowCurrent.DefaultCellStyle.BackColor = Color.White;
            if (FlagEdit)
            {
                if (CheckRow(nRowCurrent))
                {
                    CloseMonth_Customer_Info nCustomer = new CloseMonth_Customer_Info((int)nRowCurrent.Tag);

                    nCustomer.CloseMonth = mCloseMonth;
                    nCustomer.CustomerKey = (int)nRowCurrent.Cells["CustomerID"].Tag;

                    nCustomer.BeginAmountDebitCurrencyMain = double.Parse(nRowCurrent.Cells["BeginAmountDebitCurrencyMain"].Value.ToString(), mFormatProviderCurrency);
                    nCustomer.BeginAmountCreditCurrencyMain = double.Parse(nRowCurrent.Cells["BeginAmountCreditCurrencyMain"].Value.ToString(), mFormatProviderCurrency);
                    nCustomer.BeginAmountDebitCurrencyForeign = double.Parse(nRowCurrent.Cells["BeginAmountDebitCurrencyForeign"].Value.ToString(), mFormatProviderCurrency);
                    nCustomer.BeginAmountCreditCurrencyForeign = double.Parse(nRowCurrent.Cells["BeginAmountCreditCurrencyForeign"].Value.ToString(), mFormatProviderCurrency);

                    nCustomer.Save();
                    //MessageBox.Show(nCustomer.Message);
                    nRowCurrent.Tag = nCustomer.CloseAccountKey;
                    if (nCustomer.Message.Contains("Cannot insert duplicate key"))
                    {
                        nRowCurrent.Cells["CustomerID"].ErrorText = "Error";

                        MessageBox.Show("Đã có khách hàng - nhà cung cấp này rồi");
                    }
                }
                nRowCurrent.Cells["Delete"].Value = ImageListGV.Images[1];

            }
        }
        private void GV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteRow();
            }
        }
        private void GV_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
            te.AutoCompleteMode = AutoCompleteMode.None;
            switch (dataGridView1.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT CustomerID FROM CRM_Customers");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;

                case 4:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 5:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 6:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                case 7:
                    e.Control.KeyPress += new KeyPressEventHandler(ControlNumber_KeyPress);
                    break;
                default:

                    break;
            }
        }
        private void GV_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 7)
                Cursor = Cursors.Hand;
            else
                Cursor = Cursors.Default;
        }
        private void GV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
                DeleteRow();
        }

        private void ControlNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.Handled = false;
            }
            if (e.KeyChar == ',')
            {
                e.Handled = false;
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }

        private bool CheckRow(DataGridViewRow RowCheck)
        {
            bool nResult = true;
            if (RowCheck.Cells["CustomerID"].Value == null || RowCheck.Cells["CustomerID"].Tag == null || (int)RowCheck.Cells["CustomerID"].Tag == 0)
            {
                RowCheck.Cells["CustomerID"].ErrorText = "Vui lòng kiểm tra khách hàng";
                nResult = false;
            }


            if (RowCheck.Cells["BeginAmountDebitCurrencyForeign"].ErrorText.Trim().Length > 0)
            {
                nResult = false;
            }

            if (RowCheck.Cells["BeginAmountDebitCurrencyMain"].ErrorText.Trim().Length > 0)
            {
                nResult = false;
            }
            if (RowCheck.Cells["BeginAmountCreditCurrencyForeign"].ErrorText.Trim().Length > 0)
            {
                nResult = false;
            }
            if (RowCheck.Cells["BeginAmountCreditCurrencyMain"].ErrorText.Trim().Length > 0)
            {
                nResult = false;
            }
            return nResult;
        }
        private void DeleteRow()
        {
            if (!mRole.Del)
                return;
            int nRowIndexDel = dataGridView1.CurrentRow.Index;
            if (!dataGridView1.CurrentRow.IsNewRow)
            {
                if (dataGridView1.SelectedCells.Count > 0)//
                {
                    if ((int)dataGridView1.CurrentRow.Tag > 0)
                    {

                        if (MessageBox.Show("Bạn có muốn xóa dòng này ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                        {
                            CloseMonth_Customer_Info nCloseAccount = new CloseMonth_Customer_Info();
                            nCloseAccount.CloseAccountKey = (int)dataGridView1.CurrentRow.Tag;
                            nCloseAccount.Delete();

                            dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                            for (int i = nRowIndexDel; i < dataGridView1.Rows.Count; i++)
                            {
                                dataGridView1.Rows[i].Cells["No"].Value = i + 1;
                            }

                        }
                    }
                    else
                    {
                        dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                        for (int i = nRowIndexDel; i < dataGridView1.Rows.Count; i++)
                        {
                            dataGridView1.Rows[i].Cells["No"].Value = i + 1;
                        }
                    }
                }

            }
            else//
            {
                MessageBox.Show("Bạn phải chọn khách hàng để xóa.");
            }
        }
        #endregion

        #region [ Design Layout]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }

        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 65;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
        }
        #endregion

        #region [ Sercutiry ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {
            mRole.Check_Role("FNC005");
        }
        #endregion
    }
}