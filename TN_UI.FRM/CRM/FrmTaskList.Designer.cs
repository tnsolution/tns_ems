namespace TN_UI.FRM.CRM
{
    partial class FrmTaskList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTaskList));
            this.ReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.ListViewData = new System.Windows.Forms.ListView();
            this.panelHead = new System.Windows.Forms.Panel();
            this.txtTitle = new System.Windows.Forms.Label();
            this.panelTitleCenter = new System.Windows.Forms.Panel();
            this.cmdHideCenter = new System.Windows.Forms.PictureBox();
            this.cmdUnhideCenter = new System.Windows.Forms.PictureBox();
            this.txtSearch = new System.Windows.Forms.Label();
            this.cmdNew = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelSearch = new System.Windows.Forms.Panel();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTaxNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.txtTaskName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTaskID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ImgTreeView = new System.Windows.Forms.ImageList(this.components);
            this.SmallImageLV = new System.Windows.Forms.ImageList(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.ListView_Advance = new System.Windows.Forms.ListView();
            this.panelLineLeft = new System.Windows.Forms.Panel();
            this.panelViewLeft = new System.Windows.Forms.Panel();
            this.cmdExcel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioViewReport = new System.Windows.Forms.RadioButton();
            this.radioViewListView = new System.Windows.Forms.RadioButton();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.txtTitleLeft = new System.Windows.Forms.Label();
            this.cmdHide = new System.Windows.Forms.PictureBox();
            this.cmdUnhide = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelHead.SuspendLayout();
            this.panelTitleCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelSearch.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelViewLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).BeginInit();
            this.SuspendLayout();
            // 
            // ReportViewer
            // 
            this.ReportViewer.ActiveViewIndex = -1;
            this.ReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ReportViewer.DisplayGroupTree = false;
            this.ReportViewer.Location = new System.Drawing.Point(329, 186);
            this.ReportViewer.Name = "ReportViewer";
            this.ReportViewer.SelectionFormula = "";
            this.ReportViewer.Size = new System.Drawing.Size(264, 204);
            this.ReportViewer.TabIndex = 137;
            this.ReportViewer.ViewTimeSelectionFormula = "";
            this.ReportViewer.Visible = false;
            // 
            // ListViewData
            // 
            this.ListViewData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.ListViewData.ForeColor = System.Drawing.Color.Navy;
            this.ListViewData.FullRowSelect = true;
            this.ListViewData.GridLines = true;
            this.ListViewData.Location = new System.Drawing.Point(622, 161);
            this.ListViewData.MultiSelect = false;
            this.ListViewData.Name = "ListViewData";
            this.ListViewData.ShowGroups = false;
            this.ListViewData.Size = new System.Drawing.Size(599, 456);
            this.ListViewData.TabIndex = 140;
            this.ListViewData.UseCompatibleStateImageBehavior = false;
            this.ListViewData.View = System.Windows.Forms.View.Details;
            this.ListViewData.ItemActivate += new System.EventHandler(this.ListViewData_ItemActivate);
            this.ListViewData.Enter += new System.EventHandler(this.ListViewData_Enter);
            this.ListViewData.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListViewData_ColumnClick);
            this.ListViewData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListViewData_KeyDown);
            // 
            // panelHead
            // 
            this.panelHead.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelHead.BackgroundImage")));
            this.panelHead.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelHead.Controls.Add(this.txtTitle);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHead.Location = new System.Drawing.Point(0, 0);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(1230, 59);
            this.panelHead.TabIndex = 106;
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = true;
            this.txtTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtTitle.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitle.Location = new System.Drawing.Point(348, 23);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(252, 24);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "DANH SÁCH CÔNG VIỆC";
            // 
            // panelTitleCenter
            // 
            this.panelTitleCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitleCenter.BackgroundImage")));
            this.panelTitleCenter.Controls.Add(this.cmdHideCenter);
            this.panelTitleCenter.Controls.Add(this.cmdUnhideCenter);
            this.panelTitleCenter.Controls.Add(this.txtSearch);
            this.panelTitleCenter.Controls.Add(this.cmdNew);
            this.panelTitleCenter.Controls.Add(this.pictureBox2);
            this.panelTitleCenter.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleCenter.Location = new System.Drawing.Point(0, 59);
            this.panelTitleCenter.Name = "panelTitleCenter";
            this.panelTitleCenter.Size = new System.Drawing.Size(1230, 29);
            this.panelTitleCenter.TabIndex = 141;
            // 
            // cmdHideCenter
            // 
            this.cmdHideCenter.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHideCenter.BackgroundImage")));
            this.cmdHideCenter.Location = new System.Drawing.Point(1078, 5);
            this.cmdHideCenter.Name = "cmdHideCenter";
            this.cmdHideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdHideCenter.TabIndex = 5;
            this.cmdHideCenter.TabStop = false;
            this.cmdHideCenter.Visible = false;
            // 
            // cmdUnhideCenter
            // 
            this.cmdUnhideCenter.Location = new System.Drawing.Point(1078, 5);
            this.cmdUnhideCenter.Name = "cmdUnhideCenter";
            this.cmdUnhideCenter.Size = new System.Drawing.Size(18, 18);
            this.cmdUnhideCenter.TabIndex = 6;
            this.cmdUnhideCenter.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = true;
            this.txtSearch.BackColor = System.Drawing.Color.Transparent;
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.Navy;
            this.txtSearch.Location = new System.Drawing.Point(1011, 7);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(61, 16);
            this.txtSearch.TabIndex = 9;
            this.txtSearch.Text = "Tìm kiếm";
            // 
            // cmdNew
            // 
            this.cmdNew.AutoSize = true;
            this.cmdNew.BackColor = System.Drawing.Color.Transparent;
            this.cmdNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cmdNew.ForeColor = System.Drawing.Color.Maroon;
            this.cmdNew.Location = new System.Drawing.Point(33, 7);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Size = new System.Drawing.Size(92, 14);
            this.cmdNew.TabIndex = 41;
            this.cmdNew.Text = "Công việc mới";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(36, 23);
            this.pictureBox2.TabIndex = 42;
            this.pictureBox2.TabStop = false;
            // 
            // panelSearch
            // 
            this.panelSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelSearch.Controls.Add(this.txtTel);
            this.panelSearch.Controls.Add(this.label3);
            this.panelSearch.Controls.Add(this.txtTaxNumber);
            this.panelSearch.Controls.Add(this.label4);
            this.panelSearch.Controls.Add(this.cmdSearch);
            this.panelSearch.Controls.Add(this.txtTaskName);
            this.panelSearch.Controls.Add(this.label36);
            this.panelSearch.Controls.Add(this.txtTaskID);
            this.panelSearch.Controls.Add(this.label11);
            this.panelSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSearch.Location = new System.Drawing.Point(255, 88);
            this.panelSearch.Name = "panelSearch";
            this.panelSearch.Size = new System.Drawing.Size(975, 70);
            this.panelSearch.TabIndex = 125;
            // 
            // txtTel
            // 
            this.txtTel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTel.ForeColor = System.Drawing.Color.Navy;
            this.txtTel.Location = new System.Drawing.Point(442, 41);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(174, 21);
            this.txtTel.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(361, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Điện thoại";
            // 
            // txtTaxNumber
            // 
            this.txtTaxNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaxNumber.ForeColor = System.Drawing.Color.Navy;
            this.txtTaxNumber.Location = new System.Drawing.Point(442, 9);
            this.txtTaxNumber.Name = "txtTaxNumber";
            this.txtTaxNumber.Size = new System.Drawing.Size(174, 21);
            this.txtTaxNumber.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(361, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Mã số thuế";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSearch.ForeColor = System.Drawing.Color.Navy;
            this.cmdSearch.Image = ((System.Drawing.Image)(resources.GetObject("cmdSearch.Image")));
            this.cmdSearch.Location = new System.Drawing.Point(638, 32);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(85, 34);
            this.cmdSearch.TabIndex = 14;
            this.cmdSearch.Text = "Tìm";
            this.cmdSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtTaskName
            // 
            this.txtTaskName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaskName.ForeColor = System.Drawing.Color.Navy;
            this.txtTaskName.Location = new System.Drawing.Point(119, 38);
            this.txtTaskName.Name = "txtTaskName";
            this.txtTaskName.Size = new System.Drawing.Size(236, 21);
            this.txtTaskName.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Navy;
            this.label36.Location = new System.Drawing.Point(13, 41);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 14);
            this.label36.TabIndex = 10;
            this.label36.Text = "Tên";
            // 
            // txtTaskID
            // 
            this.txtTaskID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaskID.ForeColor = System.Drawing.Color.Navy;
            this.txtTaskID.Location = new System.Drawing.Point(119, 9);
            this.txtTaskID.Name = "txtTaskID";
            this.txtTaskID.Size = new System.Drawing.Size(236, 21);
            this.txtTaskID.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(13, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 14);
            this.label11.TabIndex = 10;
            this.label11.Text = "Mã số";
            // 
            // ImgTreeView
            // 
            this.ImgTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImgTreeView.ImageStream")));
            this.ImgTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ImgTreeView.Images.SetKeyName(0, "ForderTaskRoot.png");
            this.ImgTreeView.Images.SetKeyName(1, "ForderTask.png");
            this.ImgTreeView.Images.SetKeyName(2, "ForderTaskSelect.png");
            // 
            // SmallImageLV
            // 
            this.SmallImageLV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("SmallImageLV.ImageStream")));
            this.SmallImageLV.TransparentColor = System.Drawing.Color.Transparent;
            this.SmallImageLV.Images.SetKeyName(0, "Task_Icon.png");
            this.SmallImageLV.Images.SetKeyName(1, "Meeting_Icon.png");
            this.SmallImageLV.Images.SetKeyName(2, "Tel_Icon.png");
            this.SmallImageLV.Images.SetKeyName(3, "Email_Icon.png");
            this.SmallImageLV.Images.SetKeyName(4, "Note_Icon.png");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "bt_Col1.png");
            this.imageList1.Images.SetKeyName(1, "bt_Col1_Over.png");
            this.imageList1.Images.SetKeyName(2, "icon_excol.png");
            this.imageList1.Images.SetKeyName(3, "icon_excol_Over.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "bt_Col_Right.png");
            this.imageList2.Images.SetKeyName(1, "bt_Col_oVer_Right.png");
            this.imageList2.Images.SetKeyName(2, "icon_excol_right.png");
            this.imageList2.Images.SetKeyName(3, "icon_excol_Over_right.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icon_down.png");
            this.imageList3.Images.SetKeyName(1, "icon_down_Over.png");
            this.imageList3.Images.SetKeyName(2, "icon_up.png");
            this.imageList3.Images.SetKeyName(3, "icon_up_Over.png");
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(192)))), ((int)(((byte)(232)))));
            this.panelLeft.Controls.Add(this.ListView_Advance);
            this.panelLeft.Controls.Add(this.panelLineLeft);
            this.panelLeft.Controls.Add(this.panelViewLeft);
            this.panelLeft.Controls.Add(this.panelTitle);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 88);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(250, 537);
            this.panelLeft.TabIndex = 142;
            // 
            // ListView_Advance
            // 
            this.ListView_Advance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListView_Advance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListView_Advance.ForeColor = System.Drawing.Color.Navy;
            this.ListView_Advance.FullRowSelect = true;
            this.ListView_Advance.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.ListView_Advance.Location = new System.Drawing.Point(0, 29);
            this.ListView_Advance.Name = "ListView_Advance";
            this.ListView_Advance.Size = new System.Drawing.Size(250, 340);
            this.ListView_Advance.TabIndex = 8;
            this.ListView_Advance.UseCompatibleStateImageBehavior = false;
            this.ListView_Advance.View = System.Windows.Forms.View.Details;
            // 
            // panelLineLeft
            // 
            this.panelLineLeft.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelLineLeft.BackgroundImage")));
            this.panelLineLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLineLeft.Location = new System.Drawing.Point(0, 369);
            this.panelLineLeft.Name = "panelLineLeft";
            this.panelLineLeft.Size = new System.Drawing.Size(250, 20);
            this.panelLineLeft.TabIndex = 7;
            // 
            // panelViewLeft
            // 
            this.panelViewLeft.BackColor = System.Drawing.Color.White;
            this.panelViewLeft.Controls.Add(this.cmdExcel);
            this.panelViewLeft.Controls.Add(this.pictureBox1);
            this.panelViewLeft.Controls.Add(this.label1);
            this.panelViewLeft.Controls.Add(this.radioViewReport);
            this.panelViewLeft.Controls.Add(this.radioViewListView);
            this.panelViewLeft.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelViewLeft.Location = new System.Drawing.Point(0, 389);
            this.panelViewLeft.Name = "panelViewLeft";
            this.panelViewLeft.Size = new System.Drawing.Size(250, 148);
            this.panelViewLeft.TabIndex = 6;
            // 
            // cmdExcel
            // 
            this.cmdExcel.AutoSize = true;
            this.cmdExcel.BackColor = System.Drawing.Color.Transparent;
            this.cmdExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExcel.Font = new System.Drawing.Font("Arial", 8F);
            this.cmdExcel.ForeColor = System.Drawing.Color.Navy;
            this.cmdExcel.Location = new System.Drawing.Point(49, 87);
            this.cmdExcel.Name = "cmdExcel";
            this.cmdExcel.Size = new System.Drawing.Size(81, 14);
            this.cmdExcel.TabIndex = 94;
            this.cmdExcel.Text = "Export To Excel";
            this.cmdExcel.Click += new System.EventHandler(this.cmdExcel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(14, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(36, 23);
            this.pictureBox1.TabIndex = 95;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "-----------------------------------------------------------";
            // 
            // radioViewReport
            // 
            this.radioViewReport.AutoSize = true;
            this.radioViewReport.ForeColor = System.Drawing.Color.Navy;
            this.radioViewReport.Location = new System.Drawing.Point(15, 29);
            this.radioViewReport.Name = "radioViewReport";
            this.radioViewReport.Size = new System.Drawing.Size(118, 17);
            this.radioViewReport.TabIndex = 2;
            this.radioViewReport.Text = "Hiện thị dạng report";
            this.radioViewReport.UseVisualStyleBackColor = true;
            this.radioViewReport.CheckedChanged += new System.EventHandler(this.radioViewReport_CheckedChanged);
            // 
            // radioViewListView
            // 
            this.radioViewListView.AutoSize = true;
            this.radioViewListView.Checked = true;
            this.radioViewListView.ForeColor = System.Drawing.Color.Navy;
            this.radioViewListView.Location = new System.Drawing.Point(15, 9);
            this.radioViewListView.Name = "radioViewListView";
            this.radioViewListView.Size = new System.Drawing.Size(107, 17);
            this.radioViewListView.TabIndex = 1;
            this.radioViewListView.TabStop = true;
            this.radioViewListView.Text = "Hiện thị dạng lưới";
            this.radioViewListView.UseVisualStyleBackColor = true;
            this.radioViewListView.CheckedChanged += new System.EventHandler(this.radioViewListView_CheckedChanged);
            // 
            // panelTitle
            // 
            this.panelTitle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTitle.BackgroundImage")));
            this.panelTitle.Controls.Add(this.txtTitleLeft);
            this.panelTitle.Controls.Add(this.cmdHide);
            this.panelTitle.Controls.Add(this.cmdUnhide);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(250, 29);
            this.panelTitle.TabIndex = 5;
            // 
            // txtTitleLeft
            // 
            this.txtTitleLeft.AutoSize = true;
            this.txtTitleLeft.BackColor = System.Drawing.Color.Transparent;
            this.txtTitleLeft.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitleLeft.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtTitleLeft.Location = new System.Drawing.Point(12, 7);
            this.txtTitleLeft.Name = "txtTitleLeft";
            this.txtTitleLeft.Size = new System.Drawing.Size(64, 16);
            this.txtTitleLeft.TabIndex = 4;
            this.txtTitleLeft.Text = "Advances";
            // 
            // cmdHide
            // 
            this.cmdHide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdHide.BackgroundImage")));
            this.cmdHide.Location = new System.Drawing.Point(223, 0);
            this.cmdHide.Name = "cmdHide";
            this.cmdHide.Size = new System.Drawing.Size(27, 28);
            this.cmdHide.TabIndex = 3;
            this.cmdHide.TabStop = false;
            // 
            // cmdUnhide
            // 
            this.cmdUnhide.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdUnhide.BackgroundImage")));
            this.cmdUnhide.Location = new System.Drawing.Point(1, 0);
            this.cmdUnhide.Name = "cmdUnhide";
            this.cmdUnhide.Size = new System.Drawing.Size(27, 28);
            this.cmdUnhide.TabIndex = 2;
            this.cmdUnhide.TabStop = false;
            this.cmdUnhide.Visible = false;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(127)))), ((int)(((byte)(179)))));
            this.splitter1.Location = new System.Drawing.Point(250, 88);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 537);
            this.splitter1.TabIndex = 143;
            this.splitter1.TabStop = false;
            // 
            // FrmTaskList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1230, 625);
            this.Controls.Add(this.ReportViewer);
            this.Controls.Add(this.ListViewData);
            this.Controls.Add(this.panelSearch);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelTitleCenter);
            this.Controls.Add(this.panelHead);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTaskList";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Công việc";
            this.Load += new System.EventHandler(this.FrmTaskList_Load);
            this.panelHead.ResumeLayout(false);
            this.panelHead.PerformLayout();
            this.panelTitleCenter.ResumeLayout(false);
            this.panelTitleCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHideCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhideCenter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelSearch.ResumeLayout(false);
            this.panelSearch.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.panelViewLeft.ResumeLayout(false);
            this.panelViewLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUnhide)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer ReportViewer;
        public System.Windows.Forms.ListView ListViewData;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.Panel panelTitleCenter;
        private System.Windows.Forms.Label cmdNew;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label txtSearch;
        private System.Windows.Forms.PictureBox cmdUnhideCenter;
        private System.Windows.Forms.PictureBox cmdHideCenter;
        private System.Windows.Forms.Panel panelSearch;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTaxNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.TextBox txtTaskName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtTaskID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ImageList ImgTreeView;
        private System.Windows.Forms.ImageList SmallImageLV;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelLineLeft;
        private System.Windows.Forms.Panel panelViewLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioViewReport;
        private System.Windows.Forms.RadioButton radioViewListView;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Label txtTitleLeft;
        private System.Windows.Forms.PictureBox cmdHide;
        private System.Windows.Forms.PictureBox cmdUnhide;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label cmdExcel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListView ListView_Advance;






    }
}