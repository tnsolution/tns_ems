﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.CRM;
using TNLibrary.SYS;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.CRM
{
    public partial class FrmProjectDetail : Form
    {
        public int mProjectKey = 0;
        private Project_Info mProject;
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private bool FlagStatusSelect, FlagCompleteSelect;
       
        public FrmProjectDetail()
        {
            InitializeComponent();
        }

        private void FrmProjectDetail_Load(object sender, EventArgs e)
        {
            CheckRole();
            LoadDataToToolbox.ComboBoxData(CoStatus, "SELECT StatusKey,StatusName FROM CRM_TaskStatus", false);
            LoadDataToToolbox.ComboBoxData(CoCustomers, "SELECT CustomerKey,CustomerName FROM CRM_Customers ORDER BY CustomerName", true);

            mProject = new Project_Info(mProjectKey);
            if (mProjectKey == 0)
            {
                CoStatus.SelectedIndex = 0;
                txtComplete.Tag = 0;
            }
            else
            {
                LoadProjectInfo();
            }
        }

        #region [ Load Project Information ]
        private void LoadProjectInfo()
        {
            txtProjectID.Text = mProject.ProjectID;
            txtProjectName.Text = mProject.ProjectName;
            CoCustomers.SelectedValue = mProject.CustomerKey;

            if (mProject.StartDate != null)
                tnPickerStartDate.Value = mProject.StartDate.Value;
            else
                tnPickerStartDate.Text = "";

            if (mProject.DueDate != null)
                tnPickerDueDate.Value = mProject.DueDate.Value;
            else
                tnPickerDueDate.Text = "";

            CoStatus.SelectedValue = mProject.StatusKey;

            txtComplete.Value = mProject.Complete;
            txtComplete.Tag = txtComplete.Value;

            txtProjectPrice.Text = mProject.ProjectPrice.ToString(mFormatCurrencyMain, mFormatProviderCurrency);

            txtProjectContent.Text = mProject.ProjectContent;
        }
        #endregion

        #region [ Update Invoice DataBase]
        private bool CheckBeforeSave()
        {
            if (txtProjectName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Bạn phải nhập tên hợp đồng");
                txtProjectName.Focus();
                return false;
            }

            return true;
        }
    
        private bool SaveProjectInfomation()
        {
            if (!CheckBeforeSave()) return false;

            mProject.ProjectID = txtProjectID.Text;
            mProject.ProjectName = txtProjectName.Text;
            mProject.CustomerKey = (int)CoCustomers.SelectedValue;

            if (tnPickerStartDate.Value.Year == 0001)
                mProject.StartDate = null;
            else
                mProject.StartDate = tnPickerStartDate.Value;

            if (tnPickerDueDate.Value.Year == 0001)
                mProject.DueDate = null;
            else
                mProject.DueDate = tnPickerDueDate.Value;

            mProject.StatusKey = (int)CoStatus.SelectedValue;
            mProject.Complete = (int)txtComplete.Value;
            mProject.ProjectContent = txtProjectContent.Text;
            mProject.ProjectPrice = double.Parse(txtProjectPrice.Text, mFormatProviderCurrency);

            mProject.CreatedBy = SessionUser.UserLogin.Key;
            mProject.ModifiedBy = SessionUser.UserLogin.Key;


            return true;

        }
        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtComplete_ValueChanged(object sender, EventArgs e)
        {
            if (FlagCompleteSelect)
            {
                if (txtComplete.Value == 100)
                {
                    CoStatus.SelectedIndex = 3;
                }
                else
                {
                    if (CoStatus.SelectedIndex == 3)
                        CoStatus.SelectedIndex = 1;
                }
                FlagCompleteSelect = false;
            }
        }
        private void CoStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FlagStatusSelect)
            {
                if (CoStatus.SelectedIndex == 3)
                {
                    txtComplete.Tag = txtComplete.Value;
                    txtComplete.Value = 100;
                }
                else
                {
                    if (txtComplete.Tag != null)
                        txtComplete.Value = int.Parse(txtComplete.Tag.ToString());
                }
                FlagStatusSelect = false;
            }
        }

        private void CoStatus_Click(object sender, EventArgs e)
        {
            FlagStatusSelect = true;
        }
        private void txtComplete_Click(object sender, EventArgs e)
        {
            FlagCompleteSelect = true;
        }
        #endregion

        #region [ Activate Button ]
        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn muốn xóa công việc này ? ", "Xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                mProject.Delete();
                ClearProjectInfo();
            }
        }
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {

            if (SaveProjectInfomation())
            {
                mProject.Save();
                this.Close();

            }
            else
                this.Close();

        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {

            if (SaveProjectInfomation())
            {
                mProject.Save();

                ClearProjectInfo();
            }
        }
        #endregion

        #region [ Function]
        private void ClearProjectInfo()
        {

            mProject = new Project_Info();
            txtProjectID.Text = "";
            txtProjectName.Text = "";
            tnPickerDueDate.Text = "";
            tnPickerStartDate.Text = "";

            txtComplete.Value = 0;
            CoCustomers.SelectedIndex = 0;
            txtProjectContent.Text = "";

            txtProjectName.Focus();
        }

        #endregion

        #region [Short Key]
        private void FrmProjectDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Sercurity ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);

        private void CheckRole()
        {
            mRole.Check_Role("CRM006");

            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }

        #endregion

 
    }
}

