using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Excel = Microsoft.Office.Interop.Excel;
using CrystalDecisions.CrystalReports.Engine;

using TNLibrary.FNC;
using TNLibrary.FNC.Financial;
using TNLibrary.SYS;
using TN_UI.FRM.Reports;

namespace TN_UI.FRM.CRM
{
    public partial class FrmCustomerLiabilities : Form
    {
        ReportDocument rpt = new ReportDocument();
        public bool FlagView;
        string mCloseMonth = "";
        string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        string mFormatCurrencyForeign = GlobalSystemConfig.FormatCurrencyForeign;
        string mFormatDate = GlobalSystemConfig.FormatDate ;
        IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;


        string mCurrencyMain = GlobalSystemConfig.CurrencyMain;

        public FrmCustomerLiabilities()
        {
            InitializeComponent();
            
        }

        private void FrmCustomerLiabilities_Load(object sender, EventArgs e)
        {
            panelSearch.Height = 86;
            cmdHideCenter.Visible = true;
            cmdUnhideCenter.Visible = false;
            
            FinancialYear_Data.FinancialMonth(coFromMonth);
           
            FinancialYear_Data.FinancialMonth(coToMonth);
         
            FinancialYear_Info nYear = new FinancialYear_Info(true);
            mCloseMonth = nYear.FromMonth;
          

            SetupLayoutGridView(dataGridView1);

            PickerFromDate.CustomFormat = mFormatDate;
            PickerToDate.CustomFormat = mFormatDate;
            coDisplay.SelectedIndex = 0;
            coFromMonth.SelectedIndex = 0;
            coToMonth.SelectedIndex = DateTime.Now.Month - 1;
            if (!FlagView)
            this.WindowState = FormWindowState.Maximized;
            dataGridView1.Dock = DockStyle.Fill;
            crystalReportViewer1.Dock = DockStyle.Fill;
  
        }
        private void cmdSearch_Click(object sender, EventArgs e)
        {
            DateTime nFromDate;
            DateTime nToDate;

            Cursor.Current = Cursors.WaitCursor;

            if (radioFollowMonth.Checked == true)
            {
                nFromDate = TNFunctions.TransferToDateBeginMonth(coFromMonth.Text);
                nToDate = TNFunctions.TransferToDateEndMonth(coToMonth.Text);
            }
            else
            {
                nFromDate = new DateTime(PickerFromDate.Value.Year, PickerFromDate.Value.Month, PickerFromDate.Value.Day, 0, 0, 0);
                nToDate = new DateTime(PickerToDate.Value.Year, PickerToDate.Value.Month, PickerToDate.Value.Day, 23, 59, 0);

            }
            LoadDataLiabilitiesFromDateToDate(nFromDate, nToDate,false);
            Cursor.Current = Cursors.Default;
        }

        #region [Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }

        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }

        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }

        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }

        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }

        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 82;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width-5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width-5;
            coDisplay.Left = cmdHideCenter.Left - coDisplay.Width - 5;
            lbDisplay.Left = coDisplay.Left - lbDisplay.Width - 5;

            txtTitle.Left = panelTitleCenter.Width / 2 - txtTitle.Width / 2; 
        }
        #endregion

        #region [ Process Data ]


        public void LoadDataLiabilitiesFromDateToDate(DateTime FromDate, DateTime ToDate, bool IsExport)
        {

            DataTable nTableCloseAccounts = Financial_Data.Customers(mCloseMonth, FromDate, ToDate);
            //if (!FlagExport)
            //{
                if (coDisplay.Text == "Dạng Lưới")
                    PopulateDataGridView(dataGridView1, nTableCloseAccounts);
                else
                    PopulateCrystalReport(nTableCloseAccounts);
            //}
            //else
            //{
            //    PopulateExportToExcel(nTableCloseAccounts);
            //    FlagExport = false;
            //}
        }
        #endregion

        #region [ GridView ]
        private void SetupLayoutGridView(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "");
            GV.Columns.Add("CustomerID", "");
            GV.Columns.Add("CustomerName", "");
            GV.Columns.Add("BeginAmountPay", "");
            GV.Columns.Add("BeginAmountReceipt", "");
            GV.Columns.Add("MiddleAmountPay", "");
            GV.Columns.Add("MiddleAmountReceipt", "");
            GV.Columns.Add("EndAmountPay", "");
            GV.Columns.Add("EndAmountReceipt", "");

            GV.Columns[0].Width = 50;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 100;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 300;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 130;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[4].Width = 130;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[5].Width = 130;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[6].Width = 130;
            GV.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[7].Width = 130;
            GV.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns[8].Width = 130;
            GV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // setup style view

            GV.BackgroundColor = Color.White;
            GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.ColumnHeadersHeight = GV.ColumnHeadersHeight * 2;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview

            GV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(GridView_CellFormatting);

            GV.Paint += new PaintEventHandler(GridView_Paint);

          //  GV.Scroll += new ScrollEventHandler(GridView_Scroll);

            GV.ColumnWidthChanged += new DataGridViewColumnEventHandler(GridView_ColumnWidthChanged);

            GV.Resize += new System.EventHandler(GridView_Resize);

        }
        private void PopulateDataGridView(DataGridView GV, DataTable Table)
        {
            double nSumBeginAmountPay = 0;
            double nSumBeginAmountReceipt= 0;

            double nSumMiddleAmountPay = 0;
            double nSumMiddleAmountReceipt = 0;

            double nSumEndAmountPay = 0;
            double nSumEndAmountReceipt = 0;

            GV.Rows.Clear();

            for (int i = 0; i < Table.Rows.Count; i++)
            {
                DataRow nRow = Table.Rows[i];
                
                double nBeginAmountPay = double.Parse(nRow["BeginAmountPay"].ToString());
                double nBeginAmountReceipt = double.Parse(nRow["BeginAmountReceipt"].ToString());

                double nMiddleAmountPay = double.Parse(nRow["MiddleAmountPay"].ToString());
                double nMiddleAmountReceipt = double.Parse(nRow["MiddleAmountReceipt"].ToString());

                double nEndAmountPay = double.Parse(nRow["EndAmountPay"].ToString());
                double nEndAmountReceipt = double.Parse(nRow["EndAmountReceipt"].ToString());

                nSumBeginAmountPay += nBeginAmountPay;
                nSumBeginAmountReceipt += nBeginAmountReceipt;

                nSumMiddleAmountPay += nMiddleAmountPay;
                nSumMiddleAmountReceipt += nMiddleAmountReceipt;

                nSumEndAmountPay += nEndAmountPay;
                nSumEndAmountReceipt += nEndAmountReceipt;

                //Tinh tong
                string[] nRowView = 
                    { 
                        (i+1).ToString(),//tong

                        nRow["CustomerID"].ToString(),
                        nRow["CustomerName"].ToString(),

                        nBeginAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nBeginAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
 
                        nMiddleAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency),
                        nMiddleAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
                        
                        nEndAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nEndAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency)
                    };
                GV.Rows.Add(nRowView);
            }
            //Tinh tong  
            string[] nRowTotal = 
            { 

                        " ",
                        " ", 
                        "Tổng cộng :",
                        nSumBeginAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nSumBeginAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency), 

                        nSumMiddleAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency),
                        nSumMiddleAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
                                                
                        nSumEndAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nSumEndAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency), 
                        " "

            };
            GV.Rows.Add(nRowTotal);
        }
    
        void GridView_Paint(object sender, PaintEventArgs e)
        {
            DataGridView GV = dataGridView1;
            Image image = imageList4.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);

            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Arial", 9);

            Pen nLinePen = new Pen(nColor, 1);

            drawing = new Bitmap(GV.Width, GV.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;


            string[] TitleMerge = { "Số dư đầu kỳ", "Số phát trong kỳ", "Số dư cuối kỳ" };
            string[] TitleColumn = {"STT", "Mã", "Tên", "Phải trả", "Phải thu", "Phải trả", "Phải thu", "Phải trả", "Phải thu" };

            int nColumnTotal = GV.ColumnCount;
            // bat dau merge column
            int nColumnBeginMerge = 3;
            
            for (int i = 0; i < nColumnTotal; i++)
            {
                Rectangle r1 = GV.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;


                if (i>=3) // binh thuong
                {
                    r1.Height = r1.Height / 2 - 2;
                    r1.Width = r1.Width - 2;
                    r1.Y = r1.Height + 3;
                    g.DrawImage(image, r1);
                }
                else // merge
                {
                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;
                    g.DrawImage(image, r1);
                }

                //g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);

            }

            int k = 0;

            for (int j = nColumnBeginMerge; j < nColumnTotal; j += 2)
            {

                Rectangle r1 = GV.GetCellDisplayRectangle(j, -1, true);

                int w2 = GV.GetCellDisplayRectangle(j + 1, -1, true).Width;

                r1.X += 1;
                r1.Y += 1;

                r1.Width = r1.Width + w2 - 2;
                r1.Height = r1.Height / 2 - 2;

                g.DrawImage(image, r1);
                // g.FillRectangle(new SolidBrush(GV.ColumnHeadersDefaultCellStyle.BackColor), r1);

                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;

                g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                k++;
            }
            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        void GridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            DataGridView GV = dataGridView1;

            Rectangle rtHeader = GV.DisplayRectangle;
            //  rtHeader.Height = GV.ColumnHeadersHeight / 2;
            GV.Invalidate(rtHeader);

        }
        void GridView_Scroll(object sender, ScrollEventArgs e)
        {
            DataGridView GV = dataGridView1;

            Rectangle rtHeader = GV.DisplayRectangle;
            //rtHeader.Height = this.dataGridView1.ColumnHeadersHeight / 2;
            GV.Invalidate(rtHeader);

        }
        private void GridView_Resize(object sender, EventArgs e)
        {
            DataGridView GV = dataGridView1;

            Rectangle rtHeader = GV.DisplayRectangle;
            //rtHeader.Height = this.dataGridView1.ColumnHeadersHeight / 2;
            GV.Invalidate(rtHeader);
        }

        void GridView_CellFormatting(object sender, System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            //DataGridView GV = dataGridView1;
            //if (e.RowIndex >= 0)
            //{
               
            //    if (GV.Rows[e.RowIndex].Cells[0].Value.ToString().Trim().Length ==0)
            //    {
            //        e.CellStyle.BackColor = Color.FromArgb(242, 248, 255);
            //        e.CellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            //        e.CellStyle.ForeColor = Color.Navy;
            //    }

            
        }
        #endregion

        #region [ Display ]
        private void coDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (coDisplay.SelectedIndex == 0)
            {
                dataGridView1.Visible = true;
                crystalReportViewer1.Visible = false;
            }
            else
            {
                dataGridView1.Visible = false;
                crystalReportViewer1.Visible = true;
            }
        }
        private void radioFollowDate_Click(object sender, EventArgs e)
        {
            radioFollowDate.Checked = true;
            radioFollowMonth.Checked = false;
        }
        private void radioFollowMonth_Click(object sender, EventArgs e)
        {
            radioFollowDate.Checked = false;
            radioFollowMonth.Checked = true;
        }

        #endregion

        #region[Report]
        private void SetupCrystalReport()
        {
            crystalReportViewer1.DisplayGroupTree = false;
            crystalReportViewer1.Dock = DockStyle.Fill;

        }
        private void PopulateCrystalReport(DataTable Table)
        {
            // thay ten report
            rpt.Load("FilesReport\\CrystalReporLiabilities.rpt");

            DataTable nLiabilitiesTitle = TableReport.LiabilitiesTitle();
            DataTable nLiabilitiesDetail = TableReport.LiabilitiesDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();


            DataRow nRow;
            //Tinh rong
            double nSumBeginAmountPay = 0;
            double nSumBeginAmountReceipt = 0;

            double nSumMiddleAmountPay = 0;
            double nSumMiddleAmountReceipt = 0;

            double nSumEndAmountPay = 0;
            double nSumEndAmountReceipt = 0;
            //

            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];
                //Tinh tong
                double nBeginAmountPay = double.Parse(nRow["BeginAmountPay"].ToString());
                double nBeginAmountReceipt = double.Parse(nRow["BeginAmountReceipt"].ToString());

                double nMiddleAmountPay = double.Parse(nRow["MiddleAmountPay"].ToString());
                double nMiddleAmountReceipt = double.Parse(nRow["MiddleAmountReceipt"].ToString());

                double nEndAmountPay = double.Parse(nRow["EndAmountPay"].ToString());
                double nEndAmountReceipt = double.Parse(nRow["EndAmountReceipt"].ToString());

                nSumBeginAmountPay += nBeginAmountPay;
                nSumBeginAmountReceipt += nBeginAmountReceipt;

                nSumMiddleAmountPay += nMiddleAmountPay;
                nSumMiddleAmountReceipt += nMiddleAmountReceipt;

                nSumEndAmountPay += nEndAmountPay;
                nSumEndAmountReceipt += nEndAmountReceipt;
                //
                string[] nRowView = 
                    { 
                       (i+1).ToString(), 
                        nRow["CustomerID"].ToString().Trim(),
                        nRow["CustomerName"].ToString().Trim(),
                    
                        nBeginAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nBeginAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
 
                        nMiddleAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency),
                        nMiddleAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
                        
                        nEndAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                        nEndAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency)
                    };
                nLiabilitiesDetail.Rows.Add(nRowView);
            }
            //Tieu de tren,duoi
            string strTitleHeader = "";
            string strTitleFooter = "";
            if (radioFollowMonth.Checked == true)
            {
                strTitleHeader = "Từ tháng " + coFromMonth.Text + " đến tháng " + coToMonth.Text;
            }
            else
            {
                strTitleHeader = "Từ ngày " + PickerFromDate.Value.ToString(mFormatDate) + " đến ngày " + PickerToDate.Value.ToString(mFormatDate);

            }
            if (radioFollowMonth.Checked == true)
            {
                strTitleFooter = "Ngày " + TNFunctions.DayEndMonth(coToMonth.Text) + " tháng " + coToMonth.Text.Substring(0, 2) + " năm " + coToMonth.Text.Substring(3, 4);
            }
            else
            {
                strTitleFooter = "Ngày " + PickerToDate.Value.Day.ToString() + " tháng " + PickerToDate.Value.Month.ToString() + " năm " + PickerToDate.Value.Year.ToString();
            }

            string[] nRowTotal = //Tieu de
            { 
                strTitleHeader, 
                strTitleFooter,
                //Tinh tong
                nSumBeginAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                nSumBeginAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency), 

                nSumMiddleAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency),
                nSumMiddleAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
                                                
                nSumEndAmountPay.ToString(mFormatCurrencyMain, mFormatProviderCurrency), 
                nSumEndAmountReceipt.ToString(mFormatCurrencyForeign, mFormatProviderCurrency),
            };
            nLiabilitiesTitle.Rows.Add(nRowTotal);

            rpt.Database.Tables["LiabilitiesTitle"].SetDataSource(nLiabilitiesTitle);
            rpt.Database.Tables["LiabilitiesDetail"].SetDataSource(nLiabilitiesDetail);
            rpt.Database.Tables["CompanyInfo"].SetDataSource(nCompanyInfo);
            crystalReportViewer1.ReportSource = rpt;
            // thay doi do zoom
            crystalReportViewer1.Zoom(1);
        }
        public static int DayEndMonth(string MonthCurrency)//Tieu de
        {
            DateTime nDay = new DateTime(int.Parse(MonthCurrency.Substring(3, 4)), int.Parse(MonthCurrency.Substring(0, 2)), 01);
            nDay = nDay.AddMonths(1);
            nDay = nDay.AddDays(-1);
            return nDay.Day;
        }
        #endregion

        #region [ Export ]
        private void PopulateExportToExcel(DataTable Table)
        {
            DataTable nLiabilitiesTitle = TableReport.LiabilitiesTitle();
            DataTable nLiabilitiesDetail = TableReport.LiabilitiesDetail();
            DataTable nCompanyInfo = TableReport.CompanyInfo();

            DataRow nRow;
            //Tinh rong
            double nSumBeginAmountPay = 0;
            double nSumBeginAmountReceipt = 0;

            double nSumMiddleAmountPay = 0;
            double nSumMiddleAmountReceipt = 0;

            double nSumEndAmountPay = 0;
            double nSumEndAmountReceipt = 0;
            //
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                nRow = Table.Rows[i];
                //Tinh tong
                double nBeginAmountPay = double.Parse(nRow["BeginAmountPay"].ToString());
                double nBeginAmountReceipt = double.Parse(nRow["BeginAmountReceipt"].ToString());

                double nMiddleAmountPay = double.Parse(nRow["MiddleAmountPay"].ToString());
                double nMiddleAmountReceipt = double.Parse(nRow["MiddleAmountReceipt"].ToString());

                double nEndAmountPay = double.Parse(nRow["EndAmountPay"].ToString());
                double nEndAmountReceipt = double.Parse(nRow["EndAmountReceipt"].ToString());

                nSumBeginAmountPay += nBeginAmountPay;
                nSumBeginAmountReceipt += nBeginAmountReceipt;

                nSumMiddleAmountPay += nMiddleAmountPay;
                nSumMiddleAmountReceipt += nMiddleAmountReceipt;

                nSumEndAmountPay += nEndAmountPay;
                nSumEndAmountReceipt += nEndAmountReceipt;
                //

                string[] nRowView = 
                    { 
                        //nRow["CustomerKey"].ToString().Trim(),
                        (i+1).ToString(), 
                        nRow["CustomerID"].ToString().Trim(),
                        nRow["CustomerName"].ToString().Trim(),
                        nRow["BeginAmountPay"].ToString().Trim(),
                        nRow["BeginAmountReceipt"].ToString().Trim(),
                        nRow["AmountInvoicePur"].ToString().Trim(),
                        nRow["MiddleAmountReceipt"].ToString().Trim(),
                        nRow["EndAmountPay"].ToString().Trim(),
                        nRow["EndAmountReceipt"].ToString().Trim()



                        
                    };
                nLiabilitiesDetail.Rows.Add(nRowView);
            }
            //Tieu de tren,duoi
            string strTitleHeader = "";
            string strTitleFooter = "";
            if (radioFollowMonth.Checked == true)
            {
                strTitleHeader = "Từ tháng " + coFromMonth.Text + " đến tháng " + coToMonth.Text;
            }
            else
            {
                strTitleHeader = "Từ ngày " + PickerFromDate.Value.ToString(mFormatDate) + " đến ngày " + PickerToDate.Value.ToString(mFormatDate);

            }
            if (radioFollowMonth.Checked == true)
            {
                strTitleFooter = "Ngày " + TNFunctions.DayEndMonth(coToMonth.Text) + " tháng " + coToMonth.Text.Substring(0, 2) + " năm " + coToMonth.Text.Substring(3, 4);
            }
            else
            {
                strTitleFooter = "Ngày " + PickerToDate.Value.Day.ToString() + " tháng " + PickerToDate.Value.Month.ToString() + " năm " + PickerToDate.Value.Year.ToString();
            }
            string[] nRowTotal = //Tieu de
            { 
                strTitleHeader, 
                strTitleFooter,
                //Tinh tong
                nSumBeginAmountPay.ToString(), 
                nSumBeginAmountReceipt.ToString(), 

                nSumMiddleAmountPay.ToString(),
                nSumMiddleAmountReceipt.ToString(),
                                                
                nSumEndAmountPay.ToString(), 
                nSumEndAmountReceipt.ToString()
            };
            nLiabilitiesTitle.Rows.Add(nRowTotal);

            ExportToExcel(nLiabilitiesTitle, nLiabilitiesDetail, nCompanyInfo);
        }
        private void cmdExportExcel_Click(object sender, EventArgs e)//
        {
            DateTime nFromDate;
            DateTime nToDate;

            Cursor.Current = Cursors.WaitCursor;

            if (radioFollowMonth.Checked == true)
            {
                nFromDate = TNFunctions.TransferToDateBeginMonth(coFromMonth.Text);
                nToDate = TNFunctions.TransferToDateEndMonth(coToMonth.Text);
            }
            else
            {
                nFromDate = new DateTime(PickerFromDate.Value.Year, PickerFromDate.Value.Month, PickerFromDate.Value.Day, 0, 0, 0);
                nToDate = new DateTime(PickerToDate.Value.Year, PickerToDate.Value.Month, PickerToDate.Value.Day, 23, 59, 0);

            }
            LoadDataLiabilitiesFromDateToDate(nFromDate, nToDate, false);
            Cursor.Current = Cursors.Default;

            DataTable nTableCloseAccounts = Financial_Data.Customers(mCloseMonth, nFromDate, nToDate);
            PopulateExportToExcel(nTableCloseAccounts);
        }
        private static void ExportToExcel(DataTable dtTitle, DataTable dtDetail, DataTable dtCongty)
        {
            // Create an Excel object and add workbook...
            string FileName, FileType;
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            DialogSave.Title = "Save a file to...";

            //save.ShowDialog();

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                FileName = DialogSave.FileName;
                FileType = System.IO.Path.GetExtension(FileName);

                /////////////////////////
                Excel.Application excel = new Excel.Application();
                Excel.Workbook workbook = excel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet exSheet = (Excel.Worksheet)workbook.Worksheets[1];
                // Excel.Worksheet worksheet; 
                excel.ActiveWindow.DisplayGridlines = true;
                // add thong tin cong ty
                foreach (DataRow r in dtCongty.Rows)
                {

                    excel.Cells[1, 1] = r["CompanyName"].ToString();
                    excel.Cells[2, 1] = r["Address"].ToString();

                }
                //Add Tieu de
                excel.Cells[4, 4] = "BẢNG CÂN ĐỐI CÔNG NỢ";
                //add Column Name
                excel.Cells[8, 1] = "STT";
                excel.Cells[8, 2] = "Mã";
                excel.Cells[8, 3] = "Tên";
                excel.Cells[7, 4] = "Số dư đầu kỳ";
                excel.Cells[8, 4] = "Phải Trả";
                excel.Cells[8, 5] = "Phải Thu";
                excel.Cells[7, 6] = "Phát sinh trong kỳ";
                excel.Cells[8, 6] = "Phải Trả";
                excel.Cells[8, 7] = "Phải Thu";
                excel.Cells[7, 8] = "Số dư cuối kỳ";
                excel.Cells[8, 8] = "Phải Trả";
                excel.Cells[8, 9] = "Phải Thu";

                //bat dau ghi chi tiet vo file excel
                int iCol = 0;
                // for each row of data...
                int iRow = 0;
                foreach (DataRow r in dtDetail.Rows)
                {
                    iRow++;
                    // add each row's cell data...(dl hien thi)
                    iCol = 0;
                    foreach (DataColumn c in dtDetail.Columns)
                    {
                        iCol++;
                        excel.Cells[iRow + 8, iCol] = r[c.ColumnName].ToString();
                    }
                    excel.Cells[iRow + 9, iCol] = "";
                }

                foreach (DataRow r in dtTitle.Rows)
                {
                    excel.Cells[5, 4] = "" + r["TieuDeTren"].ToString();
                    excel.Cells[iRow + 11, 8] = r["TieuDeduoi"].ToString();

                    excel.Cells[iRow + 9, 3] = "Tính Tổng";

                    excel.Cells[iRow + 9, 4] = r["SumBeginAmountPay"].ToString();
                    excel.Cells[iRow + 9, 5] = r["SumBeginAmountReceipt"].ToString();
                    excel.Cells[iRow + 9, 6] = r["SumMiddleAmountPay"].ToString();
                    excel.Cells[iRow + 9, 7] = r["SumMiddleAmountReceipt"].ToString();
                    excel.Cells[iRow + 9, 8] = r["SumEndAmountPay"].ToString();
                    excel.Cells[iRow + 9, 9] = r["SumEndAmountReceipt"].ToString();
                }
                //add Footer
                excel.Cells[iRow + 12, 1] = "    Người ghi sổ";
                excel.Cells[iRow + 12, 4] = "Kế toán trưởng";
                excel.Cells[iRow + 12, 9] = "   Giám đốc";


                ////tao border
                Excel.Range nRange;
                Excel.Worksheet worksheet1 = (Excel.Worksheet)excel.ActiveSheet;
                //dinh dang tieu de
                nRange = worksheet1.get_Range("A4", "I4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;

                //dinh dang footer
                int nRo1 = iRow + 12;
                int nRo2 = iRow + 13;
                int nRo3 = iRow + 9;    //in dam
                nRange = worksheet1.get_Range("A" + nRo1, "I" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                nRange.Font.Size = 13;
                nRange = worksheet1.get_Range("A" + nRo2, "I" + nRo2);
                nRange.Font.Bold = true;
                //in dam
                nRange = worksheet1.get_Range("A" + (iRow + 9), "C" + (iRow + 9));
                nRange.Font.Bold = true;
                nRange.Font.Size = 13;
                nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                //dinh dang columns Name
                nRange = worksheet1.get_Range("A7", "I8");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Cells.RowHeight = 25;
                nRange.Interior.Color = Color.Silver.ToArgb();//mau
                //dinh dang Columns width
                //1
                nRange = worksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 6;
                //2
                nRange = worksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;
                //3
                nRange = worksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 40;
                //4
                nRange = worksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;
                //5
                nRange = worksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;
                //6
                nRange = worksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;
                //7
                nRange = worksheet1.get_Range("G1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;

                //8
                nRange = worksheet1.get_Range("H1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;

                //9
                nRange = worksheet1.get_Range("I1", Type.Missing);
                nRange.Cells.ColumnWidth = 12;


                // chon vung tao border
                int x = iRow + 9;
                nRange = worksheet1.get_Range("A7", "I" + x);
                nRange.Borders.Weight = 2;

                //canh giua toan bo dat sao border
                //nRange.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                //nRange.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;

                //merger cac columns
                nRange = worksheet1.get_Range("D7", "E7");
                nRange.Merge(1);

                nRange = worksheet1.get_Range("F7", "G7");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("H7", "I7");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("A7", "A8");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("B7", "B8");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("C7", "C8");
                nRange.Merge(Type.Missing);

                nRange = worksheet1.get_Range("A" + (iRow + 9), "C" + (iRow + 9));
                nRange.Merge(Type.Missing);

                //dinh dang ngay thangs
                //nRange = (Excel.Range)worksheet1.Cells[RowCounter, 2];
                //nRange.NumberFormat = "MM/DD/YYYY"; 

                //dinh dang so
                nRange = worksheet1.get_Range("D9", "I" + (iRow + 9));
                nRange.NumberFormat = "#,###,##0.00";
                nRange.Font.Bold = true;

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo f = new FileInfo(FileName);
                if (f.Exists)
                    f.Delete(); // delete the file if it already exist.

                excel.Visible = false;
                workbook.SaveAs(FileName,
                Excel.XlFileFormat.xlWorkbookNormal, null, null,
                false, false, Excel.XlSaveAsAccessMode.xlExclusive,
                false, false, false, false, false);


                workbook.Close(false, false, false);
                excel.Workbooks.Close();
                excel.Application.Quit();
                excel.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excel);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(exSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nRange);
                exSheet = null;
                workbook = null;
                excel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
        #endregion
    }
}