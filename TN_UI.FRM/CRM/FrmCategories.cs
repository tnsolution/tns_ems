﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;

namespace TN_UI.FRM.CRM
{
    public partial class FrmCategories : Form
    {
        public int m_Parent = 1;
        public FrmCategories()
        {
            InitializeComponent();
        }

        private void FrmCategories_Load(object sender, EventArgs e)
        {
            tnGridView1.Table = "CRM_CustomerCategories";
            tnGridView1.FieldDisplay = "CategoryName";
            tnGridView1.FieldValue = "CategoryKey";
            tnGridView1.Parent = m_Parent;
            tnGridView1.LoadDataBase("SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories WHERE Parent = " + m_Parent);
            CheckRole();
        }
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            if (m_Parent == 1)
                nRole.Check_Role("CRM001");
            else
                nRole.Check_Role("CRM003");

            if (!nRole.Edit)
            {
                tnGridView1.SetCmdEdit = false;
            }
            if (!nRole.Del)
                tnGridView1.SetCmdDel = false;

        }
    }
}
