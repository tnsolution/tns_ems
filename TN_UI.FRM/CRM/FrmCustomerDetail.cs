using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

using TNLibrary.SYS;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TN_UI.FRM.Inventory;


namespace TN_UI.FRM.CRM
{
    public partial class FrmCustomerDetail : Form
    {
        public int mCustomerKey = 0;
        public string mCustomerID;
        //
        public int mCategoryKey;
        public bool FlagNew;
        public bool FlagEdit;
        public bool FlagTemp;
        //private Customer_Info mCustomer;

        public int mCustomerTypeUI = 2; // [1 : Vendor] [2 :Customer]
        private Customer_Object mCustomer;

        public FrmCustomerDetail()
        {
            InitializeComponent();
        }

        private void FrmCustomerDetail_Load(object sender, EventArgs e)
        {
            CheckRole();

            LoadDataToToolbox.ComboBoxData(CoCountry, "SELECT CountryKey, CountryName FROM SYS_Loc_Country", false);
            LoadDataToToolbox.ComboBoxData(coCurrency, "SELECT CurrencyID,CurrencyID, CurrencyName FROM SYS_Currency WHERE Activate = 1 ", false);

            string nSQLCategory = "";
            if (mCustomerTypeUI == 1) //Vendor
                nSQLCategory = "SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories WHERE Parent = 1";
            if (mCustomerTypeUI == 2) //Customer
                nSQLCategory = "SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories WHERE Parent = 2";
            LoadDataToToolbox.ComboBoxData(CoCategories, nSQLCategory, true);

            SetupLayoutGridView(GridViewContacts);

            mCustomer = new Customer_Object(mCustomerKey);
            LoadInfoCustomer();

        }

        #region [ Load Info Customer ]
        private bool mCustomerIDChanged = false;
        private void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            mCustomerIDChanged = true;
        }
        private void txtCustomerID_Leave(object sender, EventArgs e)
        {
            if (txtCustomerID.Text.Trim().Length == 0) return;
            if (mCustomerIDChanged)
            {
                Customer_Object nCustomer = new Customer_Object(txtCustomerID.Text.Trim());
                if (nCustomer.Key > 0)
                {
                    if (MessageBox.Show(" Đã có mã khách hàng hoặc nhà cung cấp này ! Bạn có muốn hiển thị ra không ?", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        mCustomer = nCustomer;
                        LoadInfoCustomer();
                    }
                    else
                    {
                        txtCustomerID.Focus();
                    }
                }
            }
        }
        protected void LoadInfoCustomer()
        {
            txtCustomerID.Text = mCustomer.ID.Trim();
            txtCompanyName.Text = mCustomer.Name;

            txtAccountBank.Text = mCustomer.AccountBank;
            txtTax.Text = mCustomer.TaxNumber;
            coCurrency.SelectedValue = mCustomer.CurrencyID;


            txtAddress.Text = mCustomer.Address;
            coCity.Text = mCustomer.CityName;
            coProvince.Text = mCustomer.ProvinceName;
            CoCountry.SelectedValue = mCustomer.CountryKey;

            txtCompanyPhone.Text = mCustomer.Phone;
            txtCompanyFax.Text = mCustomer.Fax;
            txtCompanyEmail.Text = mCustomer.Email;
            txtCompanyWebsite.Text = mCustomer.WebSite;

            if (mCustomerTypeUI == 1) //Vendor
            {
                if (mCustomer.IsCustomer)
                {
                    string nSQLCategory = "SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories WHERE Parent = 1 OR CategoryKey = " + mCustomer.CategoryKey.ToString();
                    LoadDataToToolbox.ComboBoxData(CoCategories, nSQLCategory, true);

                    chkBoth.Checked = true;
                }
            }
            if (mCustomerTypeUI == 2) // Customer
            {
                if (mCustomer.IsVendor)
                {
                    string nSQLCategory = "SELECT CategoryKey,CategoryName FROM CRM_CustomerCategories WHERE Parent = 2 OR CategoryKey = " + mCustomer.CategoryKey.ToString();
                    LoadDataToToolbox.ComboBoxData(CoCategories, nSQLCategory, true);

                    chkBoth.Checked = true;
                }
            }

            if (mCustomerTypeUI != mCustomer.CustomerType)
            {
                chkBoth.Enabled = false;
            }
            CoCategories.SelectedValue = mCustomer.CategoryKey;
            LoadContactPersons(mCustomer.ContactPersonals);
            mCustomerIDChanged = false;

        }

        #endregion

        #region [ Update Customer Infomation]

        private bool CheckBeforeSave()
        {

            if (txtCustomerID.Text.Trim().Length == 0)
            {
                MessageBox.Show("vui lòng nhập mã khách hàng");
                txtCustomerID.Focus();
                return false;
            }
            return true;

        }
        private bool SaveCustomerInfomation()
        {

            int nCategoryKey = 0;
            mCustomerID = txtCustomerID.Text.Trim();
            mCustomer.ID = txtCustomerID.Text;
            mCustomer.Name = txtCompanyName.Text;

            mCustomer.TaxNumber = txtTax.Text;
            mCustomer.AccountBank = txtAccountBank.Text;
            mCustomer.CurrencyID = coCurrency.SelectedValue.ToString();

            if (CoCategories.SelectedValue != null)
                nCategoryKey = (int)CoCategories.SelectedValue;

            mCustomer.CategoryKey = nCategoryKey;
            mCustomer.Address = txtAddress.Text;
            mCustomer.CityName = coCity.Text;
            mCustomer.ProvinceName = coProvince.Text;
            mCustomer.CountryKey = (int)CoCountry.SelectedValue;

            mCustomer.Phone = txtCompanyPhone.Text;
            mCustomer.Fax = txtCompanyFax.Text;
            mCustomer.Email = txtCompanyEmail.Text;
            mCustomer.WebSite = txtCompanyWebsite.Text;

            if (chkBoth.Enabled)
                mCustomer.CustomerType = mCustomerTypeUI;

            if (chkBoth.Checked)
            {
                mCustomer.IsCustomer = true;
                mCustomer.IsVendor = true;
            }
            else
            {
                if (mCustomerTypeUI == 1) //Vendor
                {
                    mCustomer.IsCustomer = false;
                    mCustomer.IsVendor = true;
                }
                if (mCustomerTypeUI == 2) //Customer
                {
                    mCustomer.IsCustomer = true;
                    mCustomer.IsVendor = false;

                }
            }

            UpdateContactPersons();

            // Tracking History
            mCustomer.CreatedBy = SessionUser.UserLogin.Key;
            mCustomer.ModifiedBy = SessionUser.UserLogin.Key;

            return true;

        }
        #endregion

        #region [ Contact Personal]

        private void SetupLayoutGridView(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("ContractName", "Người liên hệ");
            GV.Columns.Add("Mobi", "Di động");
            GV.Columns.Add("Phone", "Điện thoại");
            GV.Columns.Add("Email", "Email");
            GV.Columns.Add("Department", "Bộ phận");
            GV.Columns.Add("Position", "Chức vụ");

            GV.Columns[0].Width = 150;
            GV.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[1].Width = 130;
            GV.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[2].Width = 130;
            GV.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[3].Width = 130;
            GV.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[4].Width = 130;
            GV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns[5].Width = 120;
            GV.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;



            // setup style view
            GV.ScrollBars = ScrollBars.Horizontal;
            GV.BackgroundColor = Color.White;
            GV.MultiSelect = true;
            GV.AllowUserToResizeRows = false;

            GV.GridColor = Color.FromArgb(227, 239, 255);
            GV.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV.DefaultCellStyle.ForeColor = Color.Navy;
            GV.DefaultCellStyle.Font = new Font("Arial", 9);


            GV.RowHeadersVisible = false;
            GV.AllowUserToAddRows = true;
            GV.AllowUserToDeleteRows = true;

            // setup Height Header
            GV.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV.Dock = DockStyle.Fill;
            //Activate On Gridview
            // GV.Rows.Add(20);


        }
        private void LoadContactPersons(DataTable ContactList)
        {
            GridViewContacts.Rows.Clear();
            int n = ContactList.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                DataRow nRow = ContactList.Rows[i];
                GridViewContacts.Rows.Add();
                GridViewContacts.Rows[i].Cells[0].Value = nRow["ContactName"].ToString();
                GridViewContacts.Rows[i].Cells[1].Value = nRow["MobiPhone"].ToString();
                GridViewContacts.Rows[i].Cells[2].Value = nRow["BusinessPhone"].ToString();
                GridViewContacts.Rows[i].Cells[3].Value = nRow["BusinessPhone"].ToString();
                GridViewContacts.Rows[i].Cells[4].Value = nRow["Email"].ToString();
                GridViewContacts.Rows[i].Cells[5].Value = nRow["DepartmentName"].ToString();
                GridViewContacts.Rows[i].Cells[5].Value = nRow["Position"].ToString();

            }

        }
        private void UpdateContactPersons()
        {

            mCustomer.ContactPersonals.Rows.Clear();
            int n = GridViewContacts.Rows.Count;

            for (int i = 0; i < n; i++)
            {
                if (GridViewContacts.Rows[i].Cells[0].Value != null)
                {

                    DataRow nRow = mCustomer.ContactPersonals.NewRow();
                    nRow["ContactName"] = GridViewContacts.Rows[i].Cells[0].Value.ToString();

                    if (GridViewContacts.Rows[i].Cells[1].Value != null)
                        nRow["MobiPhone"] = GridViewContacts.Rows[i].Cells[1].Value.ToString();
                    if (GridViewContacts.Rows[i].Cells[2].Value != null)
                        nRow["BusinessPhone"] = GridViewContacts.Rows[i].Cells[2].Value.ToString();
                    if (GridViewContacts.Rows[i].Cells[3].Value != null)
                        nRow["Email"] = GridViewContacts.Rows[i].Cells[4].Value.ToString();
                    if (GridViewContacts.Rows[i].Cells[4].Value != null)
                        nRow["DepartmentName"] = GridViewContacts.Rows[i].Cells[5].Value.ToString();
                    if (GridViewContacts.Rows[i].Cells[5].Value != null)
                        nRow["Position"] = GridViewContacts.Rows[i].Cells[5].Value.ToString();
                    mCustomer.ContactPersonals.Rows.Add(nRow);
                }
            }

        }
        private void GridViewContacts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (!GridViewContacts.CurrentRow.IsNewRow)
                    GridViewContacts.Rows.Remove(GridViewContacts.CurrentRow);
            }
        }

        #endregion

        #region [Process Input & Output Toolbox ]
        private void txtCompanyPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        #endregion

        #region [ Action on Button ]
        private void cmdSaveClose_Click(object sender, EventArgs e)
        {
            string strResult = "";
            if (CheckBeforeSave())
            {
                SaveCustomerInfomation();
                strResult = mCustomer.SaveObject();
                if (strResult.Contains("insert duplicate key"))
                {
                    MessageBox.Show("Đã có mã này rồi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCustomerID.Focus();

                }
                else
                    this.Close();
            }
        }
        private void cmdSaveNew_Click(object sender, EventArgs e)
        {
            string strResult = "";
            if (CheckBeforeSave())
            {
                SaveCustomerInfomation();
                strResult = mCustomer.SaveObject();
                if (strResult.Contains("insert duplicate key"))
                {
                    MessageBox.Show("Đã có mã này rồi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCustomerID.Focus();
                }
                else
                {
                    mCustomer = new Customer_Object();
                    LoadInfoCustomer();
                    txtCustomerID.Focus();
                    chkBoth.Enabled = true;
                    chkBoth.Checked = false;
                    CoCountry.Text = "Vietnam";
                }
            }
        }

        private void cmdDel_Click(object sender, EventArgs e)
        {
            if (Customers_Data.CheckExistCustomer(mCustomer.Key) > 0)
                MessageBox.Show("Không thể xóa thông tin này");
            else
            {
                mCustomer.DeleteObject();
                mCustomer = new Customer_Object();
                LoadInfoCustomer();
            }

        }
        private void cmdPrinter_Click(object sender, EventArgs e)
        {
            FrmCustomerDetailReport frm = new FrmCustomerDetailReport();
            if (CheckBeforeSave())
            {
                SaveCustomerInfomation();
                frm.mCustomerList = mCustomer;
                frm.ShowDialog();
            }

        }
        private void cmdReport_Click(object sender, EventArgs e)
        {
            FRM.Reports.FrmCustomerFinancial frm = new TN_UI.FRM.Reports.FrmCustomerFinancial();
            frm.mCustomerKey = mCustomer.Key;
            frm.ShowDialog();
        }
        #endregion

        #region [ Short Key ]
        private void FrmCustomerDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.S))
            {
                if (mRole.Edit)
                    cmdSaveNew_Click(sender, e);
            }

            if (e.KeyData == (Keys.Control | Keys.X))
            {
                if (mRole.Edit)
                    cmdSaveClose_Click(sender, e);
            }
            if (e.KeyData == (Keys.Alt | Keys.X))
            {
                this.Close();
            }
        }

        #endregion

        #region [ Sercurity ]
        User_Role_Info mRole = new User_Role_Info(SessionUser.UserLogin.Key);
        private void CheckRole()
        {

            if (mCustomerTypeUI == 1)
                mRole.Check_Role("CRM002");
            else
                mRole.Check_Role("CRM004");

            if (!mRole.Edit)
            {
                cmdSaveNew.Enabled = false;
                cmdSaveClose.Enabled = false;
            }
            if (!mRole.Del)
                cmdDel.Enabled = false;

        }
        #endregion

       
    }
}