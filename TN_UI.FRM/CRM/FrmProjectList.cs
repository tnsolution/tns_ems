using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing.Drawing2D;

using TNLibrary.SYS;
using TNLibrary.SYS.Forms;
using TNLibrary.SYS.Users;
using TNLibrary.CRM;
using TN_UI.FRM.Reports;


namespace TN_UI.FRM.CRM
{
    public partial class FrmProjectList : Form
    {
        private ReportDocument rpt = new ReportDocument();
        private string mFormatCurrencyMain = GlobalSystemConfig.FormatCurrencyMain;
        private IFormatProvider mFormatProviderCurrency = GlobalSystemConfig.FormatProviderCurrency;
        private string mFormatDecimal = GlobalSystemConfig.FormatDecimal;
        private IFormatProvider mFormatProviderDecimal = GlobalSystemConfig.FormatProviderDecimal;

        private DataTable mProjectList = new DataTable();
        private string mFormatDate = GlobalSystemConfig.FormatDate;


        public string mProjectID = "";
        public bool FlagView;
        public string mTitle = "DANH SÁCH HỢP ĐỒNG";
        public int mProjectType = 1;

        public FrmProjectList()
        {
            InitializeComponent();
            InitLayoutForm();
        }

        private void FrmProjectList_Load(object sender, EventArgs e)
        {
            InitListView(ListViewData);
            InitListViewLeft(ListView_Advance);
            cmdHide_Click(null, EventArgs.Empty);
            txtTitle.Text = mTitle;
            if (!FlagView)
                this.WindowState = FormWindowState.Maximized;

            mProjectList = Projects_Data.Project_List();
            LoadData();

        }

        private void LoadData()
        {

            ListView_Advance.Items.Clear();
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable nTable = Tasks_Data.Status();

            foreach (DataRow nRow in nTable.Rows)
            {
                lvi = new ListViewItem();
                lvi.Text = nRow["StatusName"].ToString();

                int nAmount = Projects_Data.Count((int)nRow["StatusKey"]);
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nAmount.ToString(mFormatDecimal, mFormatProviderDecimal);
                lvi.SubItems.Add(lvsi);

                ListView_Advance.Items.Add(lvi);

            }

            if (ListViewData.Visible)
                LoadDataToListView(ListViewData);
            else
                LoadDataToCrystalReport();
        }
        //Layout 
        private void InitLayoutForm()
        {

            this.panelTitleCenter.Resize += new System.EventHandler(this.panelTitleCenter_Resize);

            this.cmdUnhideCenter.MouseLeave += new System.EventHandler(this.cmdUnhideCenter_MouseLeave);
            this.cmdUnhideCenter.Click += new System.EventHandler(this.cmdUnhideCenter_Click);
            this.cmdUnhideCenter.MouseEnter += new System.EventHandler(this.cmdUnhideCenter_MouseEnter);

            this.cmdHideCenter.MouseLeave += new System.EventHandler(this.cmdHideCenter_MouseLeave);
            this.cmdHideCenter.Click += new System.EventHandler(this.cmdHideCenter_Click);
            this.cmdHideCenter.MouseEnter += new System.EventHandler(this.cmdHideCenter_MouseEnter);

            this.panelHead.Resize += new System.EventHandler(this.panelHead_Resize);
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);

            this.cmdUnhide.MouseLeave += new System.EventHandler(this.cmdUnhide_MouseLeave);
            this.cmdUnhide.Click += new System.EventHandler(this.cmdUnhide_Click);
            this.cmdUnhide.MouseEnter += new System.EventHandler(this.cmdUnhide_MouseEnter);

            this.cmdHide.MouseLeave += new System.EventHandler(this.cmdHide_MouseLeave);
            this.cmdHide.Click += new System.EventHandler(this.cmdHide_Click);
            this.cmdHide.MouseEnter += new System.EventHandler(this.cmdHide_MouseEnter);

            panelSearch.Height = 0;
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;
            cmdUnhideCenter.Image = imageList3.Images[0];

            ListViewData.Dock = DockStyle.Fill;
            ReportViewer.Dock = DockStyle.Fill;
            ReportViewer.DisplayGroupTree = false;
        }

        #region [ Layout Head ]
        private void panelHead_Resize(object sender, EventArgs e)
        {
            txtTitle.Left = panelHead.Width / 2 - txtTitle.Width / 2;

        }
        #endregion

        #region [ Lay out left  ]

        private void cmdHide_MouseEnter(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[1];
        }

        private void cmdHide_MouseLeave(object sender, EventArgs e)
        {
            cmdHide.Image = imageList1.Images[0];
        }

        private void cmdHide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 28;  //  ContainerLeft.SplitterDistance = 10;
            ListView_Advance.Visible = false;

            cmdHide.Visible = false;
            txtTitleLeft.Visible = false;
            panelLineLeft.Visible = false;
            panelViewLeft.Visible = false;

            cmdUnhide.Visible = true;
        }


        private void cmdUnhide_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[3];
        }

        private void cmdUnhide_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhide.Image = imageList1.Images[2];
        }

        private void cmdUnhide_Click(object sender, EventArgs e)
        {
            panelLeft.Width = 250;// ContainerLeft.SplitterDistance = 260;
            ListView_Advance.Visible = true;
            cmdHide.Visible = true;
            txtTitleLeft.Visible = true;
            panelLineLeft.Visible = true;
            panelViewLeft.Visible = true;

            cmdUnhide.Visible = false;

        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            cmdHide.Left = panelTitle.Width - cmdHide.Width;
        }
        #endregion

        #region [ Layout Search ]
        private void cmdHideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[3];
        }
        private void cmdHideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdHideCenter.Image = imageList3.Images[2];
        }
        private void cmdHideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 0;
            cmdUnhideCenter.Visible = true;
            cmdHideCenter.Visible = false;
        }
        private void cmdUnhideCenter_MouseLeave(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[0];
        }
        private void cmdUnhideCenter_MouseEnter(object sender, EventArgs e)
        {
            cmdUnhideCenter.Image = imageList3.Images[1];
        }
        private void cmdUnhideCenter_Click(object sender, EventArgs e)
        {
            panelSearch.Height = 70;
            cmdUnhideCenter.Visible = false;
            cmdHideCenter.Visible = true;
        }
        private void panelTitleCenter_Resize(object sender, EventArgs e)
        {
            cmdHideCenter.Left = panelTitleCenter.Width - cmdHideCenter.Width - 5;
            cmdUnhideCenter.Left = panelTitleCenter.Width - cmdUnhideCenter.Width - 5;
            txtSearch.Left = cmdHideCenter.Left - txtSearch.Width - 10;
        }
        #endregion

        #region [ Left Process ]
        private void InitListViewLeft(ListView LV)
        {
            ColumnHeader colHead;

            colHead = new ColumnHeader();
            colHead.Text = "Công việc";
            colHead.Width = 160;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 60;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        #endregion

        #region [ List View ]
        int groupColumn = 0;
        private ListViewMyGroup LVGroup;
        private void InitListView(ListView LV)
        {

            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = " ";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hợp đồng số";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Diễn giải";
            colHead.Width = 280;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình trạng";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Bắt đầu";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Kết thúc";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hoàn Thành";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Khách hàng";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            LV.SmallImageList = SmallImageLV;
            LV.Sorting = System.Windows.Forms.SortOrder.Ascending;
            LVGroup = new ListViewMyGroup(LV);

        }
        // Load Database
        public void LoadDataToListView(ListView LV)
        {
            this.Cursor = Cursors.WaitCursor;

            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = mProjectList.Rows.Count;
            int No = 0;
            for (int i = 0; i < n; i++)
            {
                No = i + 1;
                DataRow nRow = mProjectList.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = "";// No.ToString();
                lvi.Tag = nRow["ProjectKey"]; // Set the tag to 

                if (nRow["StatusKey"].ToString() == "4")
                    lvi.ForeColor = Color.Gray;
                else
                    lvi.ForeColor = Color.DarkBlue;

                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProjectID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProjectName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["StatusName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                string strStartDate = "";
                if (nRow["StartDate"] != DBNull.Value)
                {
                    DateTime nDate = (DateTime)nRow["StartDate"];
                    strStartDate = nDate.ToString(mFormatDate);
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = strStartDate;
                lvi.SubItems.Add(lvsi);

                string strDueDate = "";
                if (nRow["DueDate"] != DBNull.Value)
                {
                    DateTime nDate = (DateTime)nRow["DueDate"];
                    strDueDate = nDate.ToString(mFormatDate);
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = strDueDate;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Complete"].ToString() + "%";
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            LVGroup.InitializeGroup();
            if (ListViewData.ShowGroups == true)
            {
                LVGroup.SetGroups(groupColumn);
            }

            this.Cursor = Cursors.Default;

        }

        private void ListViewData_Enter(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                ListViewData.SelectedItems[0].BackColor = Color.White;
            }
        }
        private void ListViewData_ItemActivate(object sender, EventArgs e)
        {
            if (ListViewData.SelectedItems.Count > 0)
            {
                int nProjectKey = int.Parse(ListViewData.SelectedItems[0].Tag.ToString());
                FrmProjectDetail frm = new FrmProjectDetail();
                frm.mProjectKey = nProjectKey;
                frm.ShowDialog();

                radioViewListView.Checked = true;
                mProjectList = Projects_Data.Project_List();

                LoadData();
                ListViewData.Focus();
                nProjectKey = frm.mProjectKey;
                if (nProjectKey != 0)
                {
                    SelectIndexInListView(nProjectKey);
                }
            }
            else
            {
                MessageBox.Show("Bạn phải chọn công việc");
            }
        }
        private void ListViewData_KeyDown(object sender, KeyEventArgs e)//Sk xoa kh bang tay
        {
            if (e.KeyCode.ToString() == "Delete" && m_RoleDelete)
            {
                if (ListViewData.SelectedItems.Count > 0)//xóa rows
                {
                    int nResult = Projects_Data.CheckExistProject((int)ListViewData.SelectedItems[0].Tag);
                    if (nResult > 0)
                    {
                        MessageBox.Show("Không thể xóa hợp đồng này, vui lòng kiểm tra lại!");
                    }
                    else
                    {
                        if (MessageBox.Show("Bạn có muốn xóa hợp đồng " + ListViewData.SelectedItems[0].Text + "  ? ", "Xóa ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            int nIndex = ListViewData.SelectedItems[0].Index;
                            Project_Info nProject = new Project_Info();
                            nProject.ProjectKey = (int)ListViewData.SelectedItems[0].Tag;
                            nProject.Delete();

                            ListViewData.SelectedItems[0].Remove();
                            if (ListViewData.Items.Count > 0)
                                if (nIndex == 0)
                                    ListViewData.Items[nIndex].Selected = true;
                                else
                                    ListViewData.Items[nIndex - 1].Selected = true;
                            ListViewData.Focus();
                        }

                    }
                }
                else//xóa rows
                {
                    MessageBox.Show("Bạn phải chọn họp đồng để xóa");
                }
            }
        }
        private void ListViewData_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ListViewData.Sorting == System.Windows.Forms.SortOrder.Ascending || (e.Column != groupColumn))
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Descending;
            }
            else
            {
                ListViewData.Sorting = System.Windows.Forms.SortOrder.Ascending;
            }

            if (ListViewData.ShowGroups == false)
            {
                ListViewData.ShowGroups = true;
                LVGroup.SetGroups(0);
            }

            groupColumn = e.Column;

            // Set the groups to those created for the clicked column.
            LVGroup.SetGroups(e.Column);
        }

        #endregion

        #region [ CrystalReport ]
        private void LoadDataToCrystalReport()
        {
            this.Cursor = Cursors.WaitCursor;

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region [ Export ]
        private void cmdExcel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable nTableProject;
            nTableProject = Projects_Data.Project_List();


            this.Cursor = Cursors.Default;
        }

        private void ExportToExcel(DataTable TableDetail, DataTable TableCompanyInfo)
        {
            // Create an Excel object and add workbook...
            string nFileName, nFileType;
            SaveFileDialog nDialogSave = new SaveFileDialog();
            nDialogSave.DefaultExt = "xls";
            nDialogSave.Filter = "Excel 2003|*.xls|Excel 2007|*.xlsx|Word 2003|*.doc|Word 2007|*.docx|PortableDocument|*.Pdf|All Files|*.*";
            nDialogSave.Title = "Save a file to...";

            //save.ShowDialog();

            if (nDialogSave.ShowDialog() == DialogResult.OK)
            {
                nFileName = nDialogSave.FileName;
                nFileType = System.IO.Path.GetExtension(nFileName);

                /////////////////////////
                Excel.Application nExcel = new Excel.Application();
                Excel.Workbook nWorkbook = nExcel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
                Excel.Worksheet nExSheet = (Excel.Worksheet)nWorkbook.Worksheets[1];

                nExcel.ActiveWindow.DisplayGridlines = true;

                // Add Head Infor
                DataRow nRowHead = TableCompanyInfo.Rows[0];
                nExcel.Cells[1, 1] = nRowHead["CompanyName"].ToString();
                nExcel.Cells[2, 1] = nRowHead["Address"].ToString();

                //Add Tieu de
                nExcel.Cells[4, 3] = "DANH SÁCH NHÀ CUNG CẤP";
                //add Column Name
                nExcel.Cells[6, 1] = "Mã Khách Hàng";
                nExcel.Cells[6, 2] = "Tên Khách Hàng";
                nExcel.Cells[6, 3] = "Mã Số Thuế";
                nExcel.Cells[6, 4] = "Địa CHỉ";
                nExcel.Cells[6, 5] = "Tên Tỉnh";
                nExcel.Cells[6, 6] = "Số Điện Thoại";

                int iCol = 0;
                int iRow = 0;

                foreach (DataRow nRow in TableDetail.Rows)
                {
                    iRow++;

                    // add each row's cell data...
                    iCol = 0;
                    foreach (DataColumn nColumn in TableDetail.Columns)
                    {
                        iCol++;
                        nExcel.Cells[iRow + 6, iCol] = nRow[nColumn.ColumnName];
                    }
                }

                //add Footer
                nExcel.Cells[iRow + 9, 1] = "    Người ghi sổ";
                nExcel.Cells[iRow + 9, 3] = "Kế toán trưởng";
                nExcel.Cells[iRow + 9, 6] = "        Giám đốc";

                ////tao border
                Excel.Range nRange;
                Excel.Worksheet nWorksheet1 = (Excel.Worksheet)nExcel.ActiveSheet;
                //dinh dang tieu de
                nRange = nWorksheet1.get_Range("A4", "F4");
                nRange.Font.Bold = true;
                nRange.Font.Size = 18;

                //dinh dang footer
                int nRo1 = iRow + 9;
                int nRo2 = iRow + 10;
                nRange = nWorksheet1.get_Range("A" + nRo1, "F" + nRo1);
                nRange.Font.Bold = true;
                nRange.Font.Italic = true;
                nRange.Font.Size = 13;
                nRange = nWorksheet1.get_Range("A" + nRo2, "F" + nRo2);
                nRange.Font.Bold = true;

                //dinh dang columns Name
                nRange = nWorksheet1.get_Range("A6", "F6");
                nRange.Font.Bold = true;
                nRange.Font.Size = 12;
                nRange.Cells.RowHeight = 25;
                nRange.Interior.Color = Color.Silver.ToArgb();

                //dinh dang Columns width
                nRange = nWorksheet1.get_Range("A1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("B1", Type.Missing);
                nRange.Cells.ColumnWidth = 45;

                nRange = nWorksheet1.get_Range("C1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("D1", Type.Missing);
                nRange.Cells.ColumnWidth = 45;

                nRange = nWorksheet1.get_Range("E1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                nRange = nWorksheet1.get_Range("F1", Type.Missing);
                nRange.Cells.ColumnWidth = 15;

                // chon vung tao border
                int x = iRow + 6;
                nRange = nWorksheet1.get_Range("A6", "F" + x);
                nRange.Borders.Weight = 2;

                // Global missing reference for objects we are not defining...
                object missing = System.Reflection.Missing.Value;
                //Save the workbook...
                FileInfo nFileSave = new FileInfo(nFileName);
                if (nFileSave.Exists)
                    nFileSave.Delete(); // delete the file if it already exist.

                nExcel.Visible = false;
                nWorkbook.SaveAs(nFileName,
                Excel.XlFileFormat.xlWorkbookNormal, null, null,
                false, false, Excel.XlSaveAsAccessMode.xlExclusive,
                false, false, false, false, false);


                nWorkbook.Close(false, false, false);
                nExcel.Workbooks.Close();
                nExcel.Application.Quit();
                nExcel.Quit();

                System.Runtime.InteropServices.Marshal.ReleaseComObject(nWorkbook);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExcel);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nExSheet);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(nRange);
                nExSheet = null;
                nWorkbook = null;
                nExcel = null;
                MessageBox.Show("Đã export file excell thành công ", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }

        }
        #endregion

        #region [ Import ]
        private void cmdImport_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region [ Activate on Button ]
        private void cmdSearch_Click(object sender, EventArgs e)
        {

            //mProjectList = Projects_Data.ProjectList(txtProjectID.Text.Trim(), txtProjectName.Text.Trim(), txtTaxNumber.Text.Trim(), txtTel.Text);

            LoadData();


        }
        private void cmdNew_Click(object sender, EventArgs e)
        {

            FrmProjectDetail frm = new FrmProjectDetail();
            frm.ShowDialog();

            mProjectList = Projects_Data.Project_List();
            radioViewListView.Checked = true;
            ListViewData.Focus();

            LoadData();
            int nProjectKey = frm.mProjectKey;
            if (nProjectKey > 0)
            {
                SelectIndexInListView(nProjectKey);
            }
        }

        private void radioViewListView_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = true;
            ReportViewer.Visible = false;
            LoadDataToListView(ListViewData);

        }
        private void radioViewReport_CheckedChanged(object sender, EventArgs e)
        {
            ListViewData.Visible = false;
            ReportViewer.Visible = true;
            LoadDataToCrystalReport();
        }

        #endregion

        private void SelectIndexInListView(int ProjectKey)
        {
            for (int i = 0; i < ListViewData.Items.Count; i++)
            {
                if ((int)ListViewData.Items[i].Tag == ProjectKey)
                {
                    ListViewData.Items[i].Selected = true;
                    ListViewData.TopItem = ListViewData.Items[i];
                    break;
                }
            }
        }

        private bool m_RoleDelete = false;
        private void CheckRole()
        {
            User_Role_Info nRole = new User_Role_Info(SessionUser.UserLogin.Key);
            nRole.Check_Role("CRM006");

            if (nRole.Del)
                m_RoleDelete = true;

           
        }
    }
}



