﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT
{
    public class BalanceSheetDetailInfo
    {
        private int m_BalanceSheetKey = 0;
        private string m_AccountID = "";
        private bool m_StyleAccount;
        private string m_Operation = "";
        private int m_MethorCaculate = 0;
        private string m_Message = "";

        #region [ Constructor Get Information ]
        public BalanceSheetDetailInfo()
        {
        }
        public BalanceSheetDetailInfo(int BalancesheetKey, string AccountID)
        {
            string nSQL = "SELECT * FROM ACT_ReportBalanceSheetDetails WHERE BalanceSheetKey = @BalanceSheetKey  ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = BalancesheetKey;
                nCommand.Parameters.Add("@AccountID", SqlDbType.Int).Value = AccountID;

                 SqlDataReader nReader = nCommand.ExecuteReader();

                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_BalanceSheetKey = int.Parse(nReader["BalanceSheetKey"].ToString());
                    m_AccountID = nReader["AccountID"].ToString().Trim();
                    m_StyleAccount = bool.Parse(nReader["StyleAccount"].ToString());
                    m_Operation = nReader["Operation"].ToString();
                    m_MethorCaculate = int.Parse(nReader["MethorCaculate"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }



        }
        #endregion

        #region [ Properties ]
        // Info Account
        public int BalanceSheetKey
        {
            get { return m_BalanceSheetKey; }
            set { m_BalanceSheetKey = value; }
        }
        public string AccountID
        {
            get { return m_AccountID; }
            set { m_AccountID = value; }
        }
        public string Operation
        {
            get { return m_Operation; }
            set { m_Operation = value; }
        }
        public bool StyleAccount
        {
            get { return m_StyleAccount; }
            set { m_StyleAccount = value; }
        }
        public int MethorCaculate
        {
            get { return m_MethorCaculate; }
            set { m_MethorCaculate = value; }
        }

        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }

        
        #endregion
        #region [ Constructor Update Information ]
        public string Update()
        {
            string nResult = "";
            	
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE ACT_ReportBalanceSheetDetails SET "
                        + " StyleAccount = @StyleAccount, "
                        + " Operation = @Operation, "
                        + " MethorCaculate = @MethorCaculate "
                      
                        + " WHERE BalanceSheetKey = @BalanceSheetKey AND AccountID = @AccountID ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = m_BalanceSheetKey;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = m_AccountID;

                nCommand.Parameters.Add("@StyleAccount", SqlDbType.Bit).Value = m_StyleAccount;
                nCommand.Parameters.Add("@Operation", SqlDbType.Char).Value = m_Operation;
                nCommand.Parameters.Add("@MethorCaculate", SqlDbType.Int).Value = m_MethorCaculate;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO ACT_ReportBalanceSheetDetails "
                                  + "(BalanceSheetKey, AccountID,StyleAccount, Operation,MethorCaculate)"
                                  + " VALUES(@BalanceSheetKey, @AccountID,@StyleAccount, @Operation,@MethorCaculate)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = m_BalanceSheetKey;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = m_AccountID;

                nCommand.Parameters.Add("@StyleAccount", SqlDbType.Bit).Value = m_StyleAccount;
                nCommand.Parameters.Add("@Operation", SqlDbType.NVarChar).Value = m_Operation;
                nCommand.Parameters.Add("@MethorCaculate", SqlDbType.Int).Value = m_MethorCaculate;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
  
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM ACT_ReportBalanceSheetDetails WHERE BalanceSheetKey = @BalanceSheetKey AND AccountID = @AccountID ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@BalanceSheetKey", SqlDbType.Int).Value = m_BalanceSheetKey;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = m_AccountID;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
      
    }
}
