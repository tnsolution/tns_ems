﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT
{
    public class Voucher_Info
    {

        private int m_VoucherKey = 0;
        private string m_VoucherID = "";
        private string m_VoucherDescription = "";

        private DateTime m_VoucherDate = DateTime.Now;
        private DateTime m_VoucherRecordBookDate = DateTime.Now;

        private double m_AmountCurrencyMain = 0;
        private int m_CategoryKey = 0;

        // thong tin ve lien ket
        private int m_ImportNoteKey = 0;
        private int m_ExportNoteKey = 0;
        private int m_StyleFixedAssetDepreciation = 0;

        private string m_Message = "";
        public Voucher_Info()
        {
        }
        public Voucher_Info(int VoucherKey)
        {
            string nSQL = "SELECT * FROM Vouchers  WHERE ACT_VoucherKey = @VoucherKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@VoucherKey", SqlDbType.Int).Value = VoucherKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_VoucherKey = int.Parse(nReader["VoucherKey"].ToString());
                    m_VoucherID = nReader["VoucherID"].ToString().Trim();
                    m_VoucherDescription = nReader["VoucherDescription"].ToString();

                    m_VoucherDate = (DateTime)nReader["VoucherDate"];
                    m_VoucherRecordBookDate = (DateTime)nReader["VoucherRecordBookDate"];

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());

                    m_ImportNoteKey = int.Parse(nReader["ImportNoteKey"].ToString());
                    m_ExportNoteKey = int.Parse(nReader["ExportNoteKey"].ToString());
                    m_StyleFixedAssetDepreciation = int.Parse(nReader["StyleFixedAssetDepreciation"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public Voucher_Info(string VoucherID)
        {
            string nSQL = "SELECT * FROM Vouchers  WHERE VoucherID = @VoucherID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@VoucherID", SqlDbType.NVarChar).Value = VoucherID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_VoucherKey = int.Parse(nReader["VoucherKey"].ToString());
                    m_VoucherID = nReader["VoucherID"].ToString().Trim();
                    m_VoucherDescription = nReader["VoucherDescription"].ToString();

                    m_VoucherDate = (DateTime)nReader["VoucherDate"];
                    m_VoucherRecordBookDate = (DateTime)nReader["VoucherRecordBookDate"];

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());

                    m_ImportNoteKey = int.Parse(nReader["ImportNoteKey"].ToString());
                    m_ExportNoteKey = int.Parse(nReader["ExportNoteKey"].ToString());
                    m_StyleFixedAssetDepreciation = int.Parse(nReader["StyleFixedAssetDepreciation"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }

        #region [ Propertion ]
        // item text

        public int Key
        {
            get
            {
                return m_VoucherKey;
            }
            set
            {
                m_VoucherKey = value;
            }
        }
        public string ID
        {
            get
            {
                return m_VoucherID;
            }
            set
            {
                m_VoucherID = value;
            }
        }
        public string Description
        {
            get
            {
                return m_VoucherDescription;
            }
            set
            {
                m_VoucherDescription = value;
            }
        }

        public DateTime VoucherDate
        {
            get
            {
                return m_VoucherDate;
            }
            set
            {
                m_VoucherDate = value;
            }
        }
        public DateTime VoucherRecordBookDate
        {
            get
            {
                return m_VoucherRecordBookDate;
            }
            set
            {
                m_VoucherRecordBookDate = value;
            }
        }

        public double AmountCurrencyMain
        {
            get
            {
                return m_AmountCurrencyMain;
            }
            set
            {
                m_AmountCurrencyMain = value;
            }
        }
        public int CategoryKey
        {
            get
            {
                return m_CategoryKey;
            }
            set
            {
                m_CategoryKey = value;
            }
        }


        public int ImportNoteKey
        {
            get
            {
                return m_ImportNoteKey;
            }
            set
            {
                m_ImportNoteKey = value;
            }
        }
        public int ExportNoteKey
        {
            get
            {
                return m_ExportNoteKey;
            }
            set
            {
                m_ExportNoteKey = value;
            }
        }


        public int StyleFixedAssetDepreciation
        {
            get
            {
                return m_StyleFixedAssetDepreciation;
            }
            set
            {
                m_StyleFixedAssetDepreciation = value;
            }
        }
    

        #endregion

        #region [ Methor Master ]
        public string Update()
        {
            string nResult = "";
            string nSQL = "UPDATE ACT_Vouchers SET  "
                          + " VoucherID = @VoucherID, "
                          + " VoucherDescription = @VoucherDescription,"

                          + " VoucherDate = @VoucherDate, "
                          + " VoucherRecordBookDate = @VoucherRecordBookDate,"

                          + " AmountCurrencyMain = @AmountCurrencyMain,"
                          + " CategoryKey = @CategoryKey,"

                          + " ImportNoteKey = @ImportNoteKey,"
                          + " ExportNoteKey = @ExportNoteKey,"
                          + " StyleFixedAssetDepreciation = StyleFixedAssetDepreciation "

                          + " WHERE (VoucherKey = @VoucherKey)";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@VoucherKey", SqlDbType.Int).Value = m_VoucherKey;
                nCommand.Parameters.Add("@VoucherID", SqlDbType.NVarChar).Value = m_VoucherID;
                nCommand.Parameters.Add("@VoucherDescription", SqlDbType.NVarChar).Value = m_VoucherDescription;

                nCommand.Parameters.Add("@VoucherDate", SqlDbType.DateTime).Value = m_VoucherDate;
                nCommand.Parameters.Add("@VoucherRecordBookDate", SqlDbType.DateTime).Value = m_VoucherRecordBookDate;

                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@ImportNoteKey", SqlDbType.Int).Value = m_ImportNoteKey;
                nCommand.Parameters.Add("@ExportNoteKey", SqlDbType.Int).Value = m_ExportNoteKey;

                nCommand.Parameters.Add("@StyleFixedAssetDepreciation", SqlDbType.Int).Value = m_StyleFixedAssetDepreciation;

                nResult = nCommand.ExecuteNonQuery().ToString();


                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
        public string Create()
        {
            string nResult = ""; 
            string nSQL = "INSERT INTO Vouchers( "
                                    + " VoucherID, VoucherDescription,"
                                    + " VoucherDate, VoucherRecordBookDate, AmountCurrencyMain, CategoryKey,"
                                    + " ImportNoteKey,ExportNoteKey,StyleFixedAssetDepreciation)"
                                    + " VALUES( @VoucherID, @VoucherDescription,"
                                    + " @VoucherDate, @VoucherRecordBookDate, @AmountCurrencyMain, @CategoryKey,"
                                    + " @ImportNoteKey,@ExportNoteKey,@StyleFixedAssetDepreciation)"
                                    + " SELECT VoucherKey FROM Vouchers WHERE VoucherKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@VoucherID", SqlDbType.NVarChar).Value = m_VoucherID;
                nCommand.Parameters.Add("@VoucherDescription", SqlDbType.NVarChar).Value = m_VoucherDescription;

                nCommand.Parameters.Add("@VoucherDate", SqlDbType.DateTime).Value = m_VoucherDate;
                nCommand.Parameters.Add("@VoucherRecordBookDate", SqlDbType.DateTime).Value = m_VoucherRecordBookDate;

                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@ImportNoteKey", SqlDbType.Int).Value = m_ImportNoteKey;
                nCommand.Parameters.Add("@ExportNoteKey", SqlDbType.Int).Value = m_ExportNoteKey;

                nCommand.Parameters.Add("@StyleFixedAssetDepreciation", SqlDbType.Int).Value = m_StyleFixedAssetDepreciation;

                nResult = nCommand.ExecuteScalar().ToString();

                m_VoucherKey = int.Parse(nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM Vouchers WHERE (VoucherKey = @VoucherKey)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@VoucherKey", SqlDbType.Int).Value = m_VoucherKey;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            return nResult;
        }
        public string Save()
        {

            string nnResult = "";
            if (m_VoucherKey == 0)
                nnResult = Create();
            else
                nnResult = Update();

            return nnResult;
        }
        #endregion

    }
}
