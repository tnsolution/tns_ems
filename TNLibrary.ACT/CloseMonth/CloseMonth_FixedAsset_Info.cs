﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.ACT.CloseMonth
{
    public class CloseMonth_FixedAsset_Info : CloseAccount
    {
        private int m_FixedAssetKey = 0;
        private string m_FixedAssetID = "";
        private string m_FixedAssetName = "";

        private int m_AccountCostKey = 0;
        private double m_CostFixedAsset = 0;

        private int m_AccountDepreciationKey = 0;
        private double m_AmountDepreciation = 0;

        public CloseMonth_FixedAsset_Info()
        {
        }
        #region [ Constructor Get Information ]
        public CloseMonth_FixedAsset_Info(int CloseAccountKey)
        {

            string nSQL = " SELECT A.*,B.FixedAssetID,B.FixedAssetName,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_FixedAssets A "
                        + " INNER JOIN FAS_FixedAssets B ON A.FixedAssetKey = B.FixedAssetKey "
                        + " LEFT JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                        + " WHERE CloseAccountKey = @CloseAccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.Int).Value = CloseAccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    m_FixedAssetKey = int.Parse(nReader["FixedAssetKey"].ToString());
                    m_FixedAssetID = nReader["FixedAssetID"].ToString();
                    m_FixedAssetName = nReader["FixedAssetName"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        public CloseMonth_FixedAsset_Info(string CloseMonth, int FixedAssetKey, int AccountKey)
        {
            base.CloseMonth = CloseMonth;
            base.AccountKey = AccountKey;
            m_FixedAssetKey = FixedAssetKey;

            string nSQL = " SELECT A.*,B.FixedAssetID,B.FixedAssetName,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_FixedAssets A "
                        + " INNER JOIN FAS_FixedAssets B ON A.FixedAssetKey = B.FixedAssetKey "
                        + " INNER JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                        + " WHERE CloseMonth=@CloseMonth "
                                  + " AND A.FixedAssetKey = @FixedAssetKey "
                                  + " AND A.AccountKey = @AccountKey  ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = FixedAssetKey;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = AccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    m_FixedAssetKey = int.Parse(nReader["FixedAssetKey"].ToString());
                    m_FixedAssetID = nReader["FixedAssetID"].ToString();
                    m_FixedAssetName = nReader["FixedAssetName"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public CloseMonth_FixedAsset_Info(string CloseMonth, string FixedAssetID, string AccountID)
        {
            string nSQL = "SELECT A.*,B.FixedAssetID,B.FixedAssetName,C.AccountID,C.AccountNameVN FROM ACT_CloseMonth_FixedAssets A "
                            + " INNER JOIN FAS_FixedAssets B ON A.FixedAssetKey = B.FixedAssetKey "
                            + " INNER JOIN ACT_AccountTable C ON A.AccountKey = C.AccountKey "
                            + " WHERE A.CloseMonth = @CloseMonth  "
                            + " AND B.FixedAssetID = @FixedAssetID "
                            + " AND C.AccountID = @AccountID ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@FixedAssetID", SqlDbType.NVarChar).Value = FixedAssetID;
                nCommand.Parameters.Add("@AccountID", SqlDbType.NVarChar).Value = AccountID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseMonth = nReader["CloseMonth"].ToString();
                    base.AccountKey = int.Parse(nReader["AccountKey"].ToString());
                    base.AccountID = nReader["AccountID"].ToString().Trim();
                    base.AccountNameVN = nReader["AccountNameVN"].ToString();

                    m_FixedAssetKey = int.Parse(nReader["FixedAssetKey"].ToString());
                    m_FixedAssetID = nReader["FixedAssetID"].ToString();
                    m_FixedAssetName = nReader["FixedAssetName"].ToString();

                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]
        public int FixedAssetKey
        {
            get
            {
                return m_FixedAssetKey;
            }
            set
            {
                m_FixedAssetKey = value;
            }
        }
        public string FixedAssetName
        {
            get
            {
                return m_FixedAssetName;
            }
            set
            {
                m_FixedAssetName = value;
            }
        }

        public int AccountCostKey
        {
            get
            {
                return m_AccountCostKey;
            }
            set
            {
                m_AccountCostKey = value;
            }
        }
        public double CostFixedAsset
        {
            get
            {
                return m_CostFixedAsset;
            }
            set
            {
                m_CostFixedAsset = value;
            }
        }
        public int AccountDepreciationKey
        {
            get
            {
                return m_AccountDepreciationKey;
            }
            set
            {
                m_AccountDepreciationKey = value;
            }
        }
        public double AmountDepreciation
        {
            get
            {
                return m_AmountDepreciation;
            }
            set
            {
                m_AmountDepreciation = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string SaveCostFixedAsset()
        {
            base.AccountKey = m_AccountCostKey;
            base.BeginAmountDebitCurrencyMain = m_CostFixedAsset;
            return Save();
        }
        public string SaveDepreciation()
        {
            base.AccountKey = m_AccountDepreciationKey;
            base.BeginAmountCreditCurrencyMain = m_AmountDepreciation;
            return Save();
        }
        public string Create()
        {
            string nResult = "";
            string nSQL  = "INSERT INTO ACT_CloseMonth_FixedAssets( "
                    + " CloseMonth, AccountKey,FixedAssetKey,"
                    + " BeginAmountDebitCurrencyMain ,BeginAmountCreditCurrencyMain , BeginAmountDebitCurrencyForeign ,BeginAmountCreditCurrencyForeign, "
                    + " MiddleAmountDebitCurrencyMain,MiddleAmountCreditCurrencyMain, MiddleAmountDebitCurrencyForeign,MiddleAmountCreditCurrencyForeign, "
                    + " EndAmountDebitCurrencyMain   ,EndAmountCreditCurrencyMain   , EndAmountDebitCurrencyForeign   ,EndAmountCreditCurrencyForeign)"
                    + " VALUES(@CloseMonth, @AccountKey, @FixedAssetKey, "
                    + " @BeginAmountDebitCurrencyMain ,@BeginAmountCreditCurrencyMain , @BeginAmountDebitCurrencyForeign , @BeginAmountCreditCurrencyForeign, "
                    + " @MiddleAmountDebitCurrencyMain,@MiddleAmountCreditCurrencyMain, @MiddleAmountDebitCurrencyForeign, @MiddleAmountCreditCurrencyForeign, "
                    + " @EndAmountDebitCurrencyMain   ,@EndAmountCreditCurrencyMain   , @EndAmountDebitCurrencyForeign   , @EndAmountCreditCurrencyForeign)"
                    + " SELECT CloseAccountKey FROM ACT_CloseMonth_FixedAssets WHERE CloseAccountKey = SCOPE_IDENTITY()";
            

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = m_FixedAssetKey;

                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;

                nResult = nCommand.ExecuteScalar().ToString();
                int nCloseAccountKey = 0;
                int.TryParse(nResult, out nCloseAccountKey);
                base.CloseAccountKey = nCloseAccountKey;


                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Update()
        {
            string nResult = "";

            string nSQL = "";
            nSQL = "UPDATE ACT_CloseMonth_FixedAssets SET "
                        + " FixedAssetKey = @FixedAssetKey, "
                        + " AccountKey = @AccountKey , "
                        + " CloseMonth=@CloseMonth ,"

                        + " BeginAmountDebitCurrencyMain = @BeginAmountDebitCurrencyMain,"
                        + " BeginAmountCreditCurrencyMain = @BeginAmountCreditCurrencyMain, "
                        + " BeginAmountDebitCurrencyForeign = @BeginAmountDebitCurrencyForeign, "
                        + " BeginAmountCreditCurrencyForeign = @BeginAmountCreditCurrencyForeign, "

                        + " MiddleAmountDebitCurrencyMain = @MiddleAmountDebitCurrencyMain,"
                        + " MiddleAmountCreditCurrencyMain = @MiddleAmountCreditCurrencyMain, "
                        + " MiddleAmountDebitCurrencyForeign = @MiddleAmountDebitCurrencyForeign, "
                        + " MiddleAmountCreditCurrencyForeign = @MiddleAmountCreditCurrencyForeign, "

                        + " EndAmountDebitCurrencyMain = @EndAmountDebitCurrencyMain,"
                        + " EndAmountCreditCurrencyMain = @EndAmountCreditCurrencyMain, "
                        + " EndAmountDebitCurrencyForeign = @EndAmountDebitCurrencyForeign, "
                        + " EndAmountCreditCurrencyForeign = @EndAmountCreditCurrencyForeign "

                        + " WHERE CloseAccountKey = @CloseAccountKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = m_FixedAssetKey;

                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;


                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            return nResult;
        }

        public string Save()
        {
            if (base.CloseAccountKey ==0 )
                return Create();
            else
                return Update();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = " DELETE FROM ACT_CloseMonth_FixedAssets "
                        + " WHERE CloseAccountKey = @CloseAccountKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        
        
        #endregion
    }
}
