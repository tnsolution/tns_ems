﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNLibrary.ACT.CloseMonth
{
    public class CloseAccount
    {
        private int m_CloseAccountKey = 0;
        private string m_CloseMonth = "";
        private int m_AccountKey = 0;
        private string m_AccountID = "0";
        private string m_AccountNameVN = "";
        private string m_CurrencyID = "VND";

        private double m_BeginAmountDebitCurrencyMain = 0;
        private double m_BeginAmountCreditCurrencyMain = 0;
        private double m_BeginAmountDebitCurrencyForeign = 0;
        private double m_BeginAmountCreditCurrencyForeign = 0;

        private double m_MiddleAmountDebitCurrencyMain = 0;
        private double m_MiddleAmountCreditCurrencyMain = 0;
        private double m_MiddleAmountDebitCurrencyForeign = 0;
        private double m_MiddleAmountCreditCurrencyForeign = 0;

        private double m_EndAmountDebitCurrencyMain = 0;
        private double m_EndAmountCreditCurrencyMain = 0;
        private double m_EndAmountDebitCurrencyForeign = 0;
        private double m_EndAmountCreditCurrencyForeign = 0;

        private string m_Message = "";
        public CloseAccount()
        {
        }

        #region [ Properties ]
        public int CloseAccountKey
        {
            set
            {
                m_CloseAccountKey = value;
            }
            get
            {
                return m_CloseAccountKey;
            }
        }
        public string CloseMonth
        {
            get
            {
                return m_CloseMonth;
            }
            set
            {
                m_CloseMonth = value;
            }
        }
        public int AccountKey
        {
            get
            {
                return m_AccountKey;
            }
            set
            {
                m_AccountKey = value;
            }
        }
        public string AccountID
        {
            get
            {
                return m_AccountID;
            }
            set
            {
                m_AccountID = value;
            }
        }
        public string AccountNameVN
        {
            get
            {
                return m_AccountNameVN;
            }
            set
            {
                m_AccountNameVN = value;
            }
        }
        public string CurrencyID
        {
            get
            {
                return m_CurrencyID;
            }
            set
            {
                m_CurrencyID = value;
            }
        }

        public double BeginAmountDebitCurrencyMain
        {
            get
            {
                return m_BeginAmountDebitCurrencyMain;
            }
            set
            {
                m_BeginAmountDebitCurrencyMain = value;
            }
        }
        public double BeginAmountCreditCurrencyMain
        {
            get
            {
                return m_BeginAmountCreditCurrencyMain;
            }
            set
            {
                m_BeginAmountCreditCurrencyMain = value;
            }
        }
        public double BeginAmountDebitCurrencyForeign
        {
            get
            {
                return m_BeginAmountDebitCurrencyForeign;
            }
            set
            {
                m_BeginAmountDebitCurrencyForeign = value;
            }
        }
        public double BeginAmountCreditCurrencyForeign
        {
            get
            {
                return m_BeginAmountCreditCurrencyForeign;
            }
            set
            {
                m_BeginAmountCreditCurrencyForeign = value;
            }
        }

        public double MiddleAmountDebitCurrencyMain
        {
            get
            {
                return m_MiddleAmountDebitCurrencyMain;
            }
            set
            {
                m_MiddleAmountDebitCurrencyMain = value;
            }
        }
        public double MiddleAmountCreditCurrencyMain
        {
            get
            {
                return m_MiddleAmountCreditCurrencyMain;
            }
            set
            {
                m_MiddleAmountCreditCurrencyMain = value;
            }
        }
        public double MiddleAmountDebitCurrencyForeign
        {
            get
            {
                return m_MiddleAmountDebitCurrencyForeign;
            }
            set
            {
                m_MiddleAmountDebitCurrencyForeign = value;
            }
        }
        public double MiddleAmountCreditCurrencyForeign
        {
            get
            {
                return m_MiddleAmountCreditCurrencyForeign;
            }
            set
            {
                m_MiddleAmountCreditCurrencyForeign = value;
            }
        }

        public double EndAmountDebitCurrencyMain
        {
            get
            {
                return m_EndAmountDebitCurrencyMain;
            }
            set
            {
                m_EndAmountDebitCurrencyMain = value;
            }
        }
        public double EndAmountCreditCurrencyMain
        {
            get
            {
                return m_EndAmountCreditCurrencyMain;
            }
            set
            {
                m_EndAmountCreditCurrencyMain = value;
            }
        }
        public double EndAmountDebitCurrencyForeign
        {
            get
            {
                return m_EndAmountDebitCurrencyForeign;
            }
            set
            {
                m_EndAmountDebitCurrencyForeign = value;
            }
        }
        public double EndAmountCreditCurrencyForeign
        {
            get
            {
                return m_EndAmountCreditCurrencyForeign;
            }
            set
            {
                m_EndAmountCreditCurrencyForeign = value;
            }
        }

        public string Message
        {
            set
            {
                m_Message = value;
            }
            get
            {
                return m_Message;
            }
        }

       
        #endregion

        #region [ Methor ]

        public bool IsAccountNull
        {
            get
            {
                double Sum = BeginAmountDebitCurrencyMain + BeginAmountCreditCurrencyMain + BeginAmountDebitCurrencyForeign + BeginAmountCreditCurrencyForeign
                + MiddleAmountDebitCurrencyMain + MiddleAmountCreditCurrencyMain + MiddleAmountDebitCurrencyForeign + MiddleAmountCreditCurrencyForeign
                + EndAmountDebitCurrencyMain + EndAmountCreditCurrencyMain + EndAmountDebitCurrencyForeign + EndAmountCreditCurrencyForeign;
                if (Sum == 0)
                    return true;
                else
                    return false;
            }
        }
        public void UpdateEndAmount()
        {
            string AccountStyle = m_AccountID.Substring(0, 3);
            if (AccountStyle == "131" || AccountStyle == "331" || AccountStyle == "333")
                return;

            AccountStyle = m_AccountID.Substring(0, 1);
            double nAmountCurrencyMain = 0;
            double nAmountCurrencyForeign = 0;
            if (AccountStyle == "1" || AccountStyle == "2" || AccountStyle == "6" || AccountStyle == "8" || AccountStyle == "9")
            {
                AccountStyle = m_AccountID.Substring(0, 3);
                if (AccountStyle == "129" || AccountStyle == "139" || AccountStyle == "159" || AccountStyle == "214" || AccountStyle == "219")
                {
                    /* Tinh gia tri tien dang theo doi*/
                    nAmountCurrencyMain = m_BeginAmountCreditCurrencyMain + m_MiddleAmountCreditCurrencyMain - m_BeginAmountDebitCurrencyMain - m_MiddleAmountDebitCurrencyMain;
                    if (nAmountCurrencyMain < 0)
                    {
                        m_EndAmountDebitCurrencyMain = -nAmountCurrencyMain;
                        m_EndAmountCreditCurrencyMain = 0;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyMain = 0;
                        m_EndAmountCreditCurrencyMain = nAmountCurrencyMain;
                    }
                    /* Tinh gia tri tien ngoai ten khac theo doi*/
                    nAmountCurrencyForeign = m_BeginAmountCreditCurrencyForeign + m_MiddleAmountCreditCurrencyForeign - m_BeginAmountDebitCurrencyForeign - m_MiddleAmountDebitCurrencyForeign;
                    if (nAmountCurrencyForeign < 0)
                    {
                        m_EndAmountDebitCurrencyForeign = -nAmountCurrencyForeign;
                        m_EndAmountCreditCurrencyForeign = 0;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyForeign = 0;
                        m_EndAmountCreditCurrencyForeign = nAmountCurrencyForeign;
                    }
                }
                else
                {
                    /* Tinh gia tri tien dang theo doi*/
                    nAmountCurrencyMain = m_BeginAmountDebitCurrencyMain + m_MiddleAmountDebitCurrencyMain - m_BeginAmountCreditCurrencyMain - m_MiddleAmountCreditCurrencyMain;

                    if (nAmountCurrencyMain < 0)
                    {
                        m_EndAmountDebitCurrencyMain = 0;
                        m_EndAmountCreditCurrencyMain = -nAmountCurrencyMain;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyMain = nAmountCurrencyMain;
                        m_EndAmountCreditCurrencyMain = 0;
                    }
                    /* Tinh gia tri tien ngoai ten khac theo doi*/
                    nAmountCurrencyForeign = m_BeginAmountDebitCurrencyForeign + m_MiddleAmountDebitCurrencyForeign - m_BeginAmountCreditCurrencyForeign - m_MiddleAmountCreditCurrencyForeign;
                    if (nAmountCurrencyForeign < 0)
                    {
                        m_EndAmountDebitCurrencyForeign = 0;
                        m_EndAmountCreditCurrencyForeign = -nAmountCurrencyForeign;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyForeign = nAmountCurrencyForeign;
                        m_EndAmountCreditCurrencyForeign = 0;
                    }
                }
            }
            else
            {
                AccountStyle = m_AccountID.Substring(0, 3);
                if (AccountStyle == "521" || AccountStyle == "531" || AccountStyle == "532")
                {
                    /* Tinh gia tri tien dang theo doi*/
                    nAmountCurrencyMain = m_BeginAmountDebitCurrencyMain + m_MiddleAmountDebitCurrencyMain - m_BeginAmountCreditCurrencyMain - m_MiddleAmountCreditCurrencyMain;

                    if (nAmountCurrencyMain < 0)
                    {
                        m_EndAmountDebitCurrencyMain = 0;
                        m_EndAmountCreditCurrencyMain = -nAmountCurrencyMain;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyMain = nAmountCurrencyMain;
                        m_EndAmountCreditCurrencyMain = 0;
                    }
                    /* Tinh gia tri tien ngoai ten khac theo doi*/
                    nAmountCurrencyForeign = m_BeginAmountDebitCurrencyForeign + m_MiddleAmountDebitCurrencyForeign - m_BeginAmountCreditCurrencyForeign - m_MiddleAmountCreditCurrencyForeign;
                    if (nAmountCurrencyForeign < 0)
                    {
                        m_EndAmountDebitCurrencyForeign = 0;
                        m_EndAmountCreditCurrencyForeign = -nAmountCurrencyForeign;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyForeign = nAmountCurrencyForeign;
                        m_EndAmountCreditCurrencyForeign = 0;
                    }
                }
                else
                {
                    /* Tinh gia tri tien dang theo doi*/
                    nAmountCurrencyMain = m_BeginAmountCreditCurrencyMain + m_MiddleAmountCreditCurrencyMain - m_BeginAmountDebitCurrencyMain - m_MiddleAmountDebitCurrencyMain;
                    if (nAmountCurrencyMain < 0)
                    {
                        m_EndAmountDebitCurrencyMain = -nAmountCurrencyMain;
                        m_EndAmountCreditCurrencyMain = 0;
                    }
                    else
                    {

                        m_EndAmountDebitCurrencyMain = 0;
                        m_EndAmountCreditCurrencyMain = nAmountCurrencyMain;
                    }
                    /* Tinh gia tri tien ngoai ten khac theo doi*/
                    nAmountCurrencyForeign = m_BeginAmountCreditCurrencyForeign + m_MiddleAmountCreditCurrencyForeign - m_BeginAmountDebitCurrencyForeign - m_MiddleAmountDebitCurrencyForeign;
                    if (nAmountCurrencyForeign < 0)
                    {
                        m_EndAmountDebitCurrencyForeign = -nAmountCurrencyForeign;
                        m_EndAmountCreditCurrencyForeign = 0;
                    }
                    else
                    {
                        m_EndAmountDebitCurrencyForeign = 0;
                        m_EndAmountCreditCurrencyForeign = nAmountCurrencyForeign;
                    }
                }
            }
        }
        #endregion

    }
}
