﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.FAS
{
    public class FixedAsset_Object : FixedAsset_Info
    {
        private double m_BeginAmountDepreciation = 0;// hao mon dau ky
        private double m_MiddleAmountDepreciation = 0; // khau hao trong ky
        private double m_EndAmountDepreciation = 0; // Khau hao luy ke

        private DataTable m_TableDepreciationYears = new DataTable();
        private DataTable m_TableDepreciationAccountExpense = new DataTable();
        private DataTable m_TableDepreciationMonthStop = new DataTable();

        #region [ Constructor Get Information ]
        public FixedAsset_Object()
            : base()
        {
            GetDepreciationYears();
            GetDepreciationAccountExpenses();
            GetDepreciationMonthStops();
        }
        public FixedAsset_Object(int NoteKey)
            : base(NoteKey)
        {
            GetDepreciationYears();
            GetDepreciationAccountExpenses();
            GetDepreciationMonthStops();
        }

        public FixedAsset_Object(string NoteID)
            : base(NoteID)
        {
            GetDepreciationYears();
            GetDepreciationAccountExpenses();
            GetDepreciationMonthStops();
        }

        #endregion

        #region [Properties]

        public DataTable DepreciationYears
        {
            set
            {
                m_TableDepreciationYears = value;
            }
            get
            {
                return m_TableDepreciationYears;

            }
        }
        public DataTable DepreciationAccountExpense
        {
            set
            {
                m_TableDepreciationAccountExpense = value;
            }
            get
            {
                return m_TableDepreciationAccountExpense;

            }
        }
        public DataTable DepreciationMonthStop
        {
            set
            {
                m_TableDepreciationMonthStop = value;
            }
            get
            {
                return m_TableDepreciationMonthStop;

            }
        }
        public double BeginAmountDepreciation
        {
            get { return m_BeginAmountDepreciation; }
            set { m_BeginAmountDepreciation = value; }
        }
        public double MiddleAmountDepreciation
        {
            get { return m_MiddleAmountDepreciation; }
            set { m_MiddleAmountDepreciation = value; }
        }
        public double EndAmountDepreciation
        {
            get { return m_EndAmountDepreciation; }
            set { m_EndAmountDepreciation = value; }
        }

        public double AmountRemain
        {
            get { return base.CostFixedAsset - EndAmountDepreciation; }
        }

        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.Key > 0)
            {
                //DeleteProductsInput();
                //SaveProductsInput();
            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
         //   DeleteProductsInput();
            return nResult;
        }

        #endregion

        #region [ DepreciationYears ]
        public void GetDepreciationYears()
        {
            string strSQL = "SELECT * FROM FAS_FixedAssetDepreciationYears "
                         + "  WHERE FixedAssetKey =@FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = base.Key;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(m_TableDepreciationYears);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion
        
        #region [ DepreciationAccountExpenses ]
        public void GetDepreciationAccountExpenses()
        {
            string strSQL = " SELECT A.*,B.AccountID,B.AccountNameVN FROM FAS_FixedAssetDepreciationAccountExpense A "
                         + " INNER JOIN ACT_AccountTable B ON A.AccountDepreciationExpenseKey = B.AccountKey "
                         + " WHERE FixedAssetKey =@FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = base.Key;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(m_TableDepreciationAccountExpense);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ DepreciationMonthStops ]
        public void GetDepreciationMonthStops()
        {
            string strSQL = "SELECT * FROM FAS_FixedAssetDepreciationMonthStop "
                         + "  WHERE FixedAssetKey =@FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = base.Key;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(m_TableDepreciationMonthStop);
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion
    }
}
