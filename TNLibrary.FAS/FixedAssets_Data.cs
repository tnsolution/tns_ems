﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FAS
{
    public class FixedAssets_Data
    {
        public static DataTable List()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FAS_FixedAssets ORDER BY FixedAssetName";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable List(int CategoryKey)
        {
            DataTable nTable = new DataTable();

            string nSQL = " SELECT * FROM FAS_FixedAssets A"
                        + " LEFT JOIN SYS_Departments B ON A.DepartmentKey = B.DepartmentKey "
                        + " LEFT JOIN FAS_Situation C ON A.SituationKey = C.SituationKey "
                        + " WHERE A.CategoryKey = @CategoryKey  "
                        + " ORDER BY A.FixedAssetName";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable List(string FixedAssetID, string FixedAssetName)
        {
            DataTable nTable = new DataTable();

            string nSQL = " SELECT * FROM FAS_FixedAssets A "
                        + " LEFT JOIN SYS_Departments B ON A.DepartmentKey = B.DepartmentKey "
                         + " LEFT JOIN FAS_Situation C ON A.SituationKey = C.SituationKey "
                        + " WHERE A.FixedAssetID LIKE  = @FixedAssetID AND A.FixedAssetName=@FixedAssetName ORDER BY FixedAssetName"
                        + " ORDER BY A.FixedAssetName";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetID", SqlDbType.NVarChar).Value = "%" + FixedAssetID + "%";
                nCommand.Parameters.Add("@FixedAssetName", SqlDbType.NVarChar).Value = "%" + FixedAssetName + "%" ;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static string UpdateCategory(int FixedAssetKey, int CategoryKey)
        {
            string nResult = "";
            string nSQL = "UPDATE FixedAssets SET CategoryKey = @CategoryKey WHERE FixedAssetKey = @FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = FixedAssetKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult; ;
        }
    }
}
