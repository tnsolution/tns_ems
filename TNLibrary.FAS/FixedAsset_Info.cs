﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FAS
{
    public class FixedAsset_Info
    {
        private int m_FixedAssetKey = 0;
        private string m_FixedAssetID = "";
        private string m_FixedAssetName = "";
        private string m_FixedAssetDescription = "";
        private int m_CountryKey = 0;

        private int m_MakeYear = 0;
        private int m_UsedYear = 0;

        private int m_SituationKey = 0;
        private int m_DepartmentKey = 0;
        private int m_CategoryKey = 0;

        private bool m_DepreciationMethod;
        private int m_DepreciationYearNumber = 0;

        private int m_AccountCostKey = 0;
        private string m_AccountCostID = "";
        private string m_AccountCostNameVN = "";
        private double m_CostFixedAsset = 0;

        private int m_AccountDepreciationKey = 0;
        private string m_AccountDepreciationID = "";
        private string m_AccountDepreciationNameVN = "";
        private double m_AmountDepreciation = 0;

        private DateTime m_DateBeginDepreciation = DateTime.Now;
        private DateTime m_DateIncreaseFixedAsset = DateTime.Now;
        private DateTime m_DateDecreaseFixedAsset = new DateTime(1978, 01, 28);

        private string m_Message = "";
        public FixedAsset_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region [ Constructor Get Information ]
        public FixedAsset_Info(int FixedAssetKey)
        {
            string nSQL = " SELECT "
                        + " A.* ,"
                        + " B.AccountID AS AccountCostID,B.AccountNameVN AS AccountCostNameVN, "
                        + " C.AccountID AS AccountDepreciationID, C.AccountNameVN AS AccountDepreciationNameVN "
                        + " FROM FAS_FixedAssets A "
                        + " LEFT JOIN ACT_AccountTable B ON B.AccountKey = A.AccountCostKey "
                        + " LEFT JOIN ACT_AccountTable C ON C.AccountKey = A.AccountDepreciationKey "
                        + " WHERE FixedAssetKey = @FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = FixedAssetKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_FixedAssetKey = (int)nReader["FixedAssetKey"];
                    m_FixedAssetID = nReader["FixedAssetID"].ToString().Trim();
                    m_FixedAssetName = nReader["FixedAssetName"].ToString();
                    m_FixedAssetDescription = nReader["FixedAssetDescription"].ToString();
                    m_CountryKey = (int)nReader["CountryKey"];

                    m_MakeYear = (int)nReader["MakeYear"];
                    m_UsedYear = (int)nReader["UsedYear"];

                    m_SituationKey = (int)nReader["SituationKey"];
                    m_DepartmentKey = (int)nReader["DepartmentKey"];
                    m_CategoryKey = (int)nReader["CategoryKey"];

                    m_DepreciationMethod = (bool)nReader["DepreciationMethod"];
                    m_DepreciationYearNumber = (int)nReader["DepreciationYearNumber"];

                    m_AccountCostKey = (int)nReader["AccountCostKey"];
                    m_AccountCostID = nReader["AccountCostID"].ToString();
                    m_AccountCostNameVN = nReader["AccountCostNameVN"].ToString();
                    m_CostFixedAsset = double.Parse(nReader["CostFixedAsset"].ToString());

                    m_AccountDepreciationID = nReader["AccountDepreciationID"].ToString();
                    m_AccountDepreciationNameVN = nReader["AccountDepreciationNameVN"].ToString();
                    m_AccountDepreciationKey = (int)nReader["AccountDepreciationKey"];

                    m_DateBeginDepreciation = (DateTime)nReader["DateBeginDepreciation"];
                    m_DateIncreaseFixedAsset = (DateTime)nReader["DateIncreaseFixedAsset"];
                    m_DateDecreaseFixedAsset = (DateTime)nReader["DateDecreaseFixedAsset"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public FixedAsset_Info(string FixedAssetID)
        {
            string nSQL = " SELECT "
              + " A.* ,"
              + " B.AccountID AS AccountCostID,B.AccountNameVN AS AccountCostNameVN, "
              + " C.AccountID AS AccountDepreciationID, C.AccountNameVN AS AccountDepreciationNameVN "
              + " FROM FAS_FixedAssets A "
              + " LEFT JOIN ACT_AccountTable B ON B.AccountKey = A.AccountCostKey "
              + " LEFT JOIN ACT_AccountTable C ON C.AccountKey = A.AccountDepreciationKey "
              + " WHERE FixedAssetID = @FixedAssetID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetID", SqlDbType.NVarChar).Value = FixedAssetID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_FixedAssetKey = (int)nReader["FixedAssetKey"];
                    m_FixedAssetID = nReader["FixedAssetID"].ToString().Trim();
                    m_FixedAssetName = nReader["FixedAssetName"].ToString();
                    m_FixedAssetDescription = nReader["FixedAssetDescription"].ToString();
                    m_CountryKey = (int)nReader["CountryKey"];

                    m_MakeYear = (int)nReader["MakeYear"];
                    m_UsedYear = (int)nReader["UsedYear"];

                    m_SituationKey = (int)nReader["SituationKey"];
                    m_DepartmentKey = (int)nReader["DepartmentKey"];
                    m_CategoryKey = (int)nReader["CategoryKey"];

                    m_DepreciationMethod = (bool)nReader["DepreciationMethod"];
                    m_DepreciationYearNumber = (int)nReader["DepreciationYearNumber"];

                    m_AccountCostKey = (int)nReader["AccountCostKey"];
                    m_AccountCostID = nReader["AccountCostID"].ToString();
                    m_AccountCostNameVN = nReader["AccountCostNameVN"].ToString();
                    m_CostFixedAsset = double.Parse(nReader["CostFixedAsset"].ToString());

                    m_AccountDepreciationID = nReader["AccountDepreciationID"].ToString();
                    m_AccountDepreciationNameVN = nReader["AccountDepreciationNameVN"].ToString();
                    m_AccountDepreciationKey = (int)nReader["AccountDepreciationKey"];

                    m_DateBeginDepreciation = (DateTime)nReader["DateBeginDepreciation"];
                    m_DateIncreaseFixedAsset = (DateTime)nReader["DateIncreaseFixedAsset"];
                    m_DateDecreaseFixedAsset = (DateTime)nReader["DateDecreaseFixedAsset"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

        #region [ Properties ]

        public int Key
        {
            get { return m_FixedAssetKey; }
            set { m_FixedAssetKey = value; }
        }
        public string ID
        {
            get { return m_FixedAssetID; }
            set { m_FixedAssetID = value; }
        }
        public string Name
        {
            get { return m_FixedAssetName; }
            set { m_FixedAssetName = value; }
        }
        public string Descriptiontion
        {
            get { return m_FixedAssetDescription; }
            set { m_FixedAssetDescription = value; }
        }
        public int CountryKey
        {
            get { return m_CountryKey; }
            set { m_CountryKey = value; }
        }

        public int MakeYear
        {
            get { return m_MakeYear; }
            set { m_MakeYear = value; }
        }
        public int UsedYear
        {
            get { return m_UsedYear; }
            set { m_UsedYear = value; }
        }

        public int DepartmentKey
        {
            get { return m_DepartmentKey; }
            set { m_DepartmentKey = value; }
        }
        public int SituationKey
        {
            get { return m_SituationKey; }
            set { m_SituationKey = value; }
        }
        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }

        public bool Method
        {
            get { return m_DepreciationMethod; }
            set { m_DepreciationMethod = value; }
        }
        public int DepreciationYearNumber
        {
            get { return m_DepreciationYearNumber; }
            set { m_DepreciationYearNumber = value; }
        }

        public int AccountCostKey
        {
            get { return m_AccountCostKey; }
            set { m_AccountCostKey = value; }
        }
        public string AccountCostID
        {
            get
            {
                return m_AccountCostID;
            }
        }
        public string AccountCostNameVN
        {
            get
            {
                return m_AccountCostNameVN;
            }
        }
        public double CostFixedAsset
        {
            set
            {
                m_CostFixedAsset = value;
            }
            get
            {
                return m_CostFixedAsset;
            }
        }        
        
        public int AccountDepreciationKey
        {
            get { return m_AccountDepreciationKey; }
            set { m_AccountDepreciationKey = value; }
        }
        public string AccountDepreciationID
        {
            get
            {
                return m_AccountDepreciationID;
            }
        }
        public string AccountDepreciationNameVN
        {
            get
            {
                return m_AccountDepreciationNameVN;
            }
        }

        public DateTime DateBeginDepreciation
        {
            get { return m_DateBeginDepreciation; }
            set { m_DateBeginDepreciation = value; }
        }
        public DateTime DateIncreaseFixedAsset
        {
            get { return m_DateIncreaseFixedAsset; }
            set { m_DateIncreaseFixedAsset = value; }
        }
        public DateTime DateDecreaseFixedAsset
        {
            get { return m_DateDecreaseFixedAsset; }
            set { m_DateDecreaseFixedAsset = value; }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string nResult = "";


            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE FAS_FixedAssets SET "
                        + " FixedAssetID = @FixedAssetID,"
                        + " FixedAssetName = @FixedAssetName, "
                        + " FixedAssetDescription = @FixedAssetDescription ,"
                        + " CountryKey = @CountryKey, "

                        + " MakeYear = @MakeYear,"
                        + " UsedYear = @UsedYear ,"

                        + " DepartmentKey = @DepartmentKey ,"
                        + " SituationKey = @SituationKey ,"
                        + " CategoryKey = @CategoryKey, "

                        + " CostFixedAsset = @CostFixedAsset ,"
                        + " DepreciationMethod = @DepreciationMethod ,"
                        + " DepreciationYearNumber = @DepreciationYearNumber, "

                        + " AccountCostKey = @AccountCostKey,"
                        + " AccountDepreciationKey = @AccountDepreciationKey,"

                        + " DateBeginDepreciation = @DateBeginDepreciation,"
                        + " DateIncreaseFixedAsset =@DateIncreaseFixedAsset, "
                        + " DateDecreaseFixedAsset =@DateDecreaseFixedAsset "
                        + " WHERE FixedAssetKey = @FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = m_FixedAssetKey;
                nCommand.Parameters.Add("@FixedAssetID", SqlDbType.NVarChar).Value = m_FixedAssetID;
                nCommand.Parameters.Add("@FixedAssetName", SqlDbType.NVarChar).Value = m_FixedAssetName;
                nCommand.Parameters.Add("@FixedAssetDescription", SqlDbType.NText).Value = m_FixedAssetDescription;
                nCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = m_CountryKey;

                nCommand.Parameters.Add("@MakeYear", SqlDbType.Int).Value = m_MakeYear;
                nCommand.Parameters.Add("@UsedYear", SqlDbType.Int).Value = m_UsedYear;

                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = m_DepartmentKey;
                nCommand.Parameters.Add("@SituationKey", SqlDbType.Int).Value = m_SituationKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@CostFixedAsset", SqlDbType.Money).Value = m_CostFixedAsset;
                nCommand.Parameters.Add("@DepreciationMethod", SqlDbType.Bit).Value = m_DepreciationMethod;
                nCommand.Parameters.Add("@DepreciationYearNumber", SqlDbType.Int).Value = m_DepreciationYearNumber;

                nCommand.Parameters.Add("@AccountCostKey", SqlDbType.Int).Value = m_AccountCostKey;
                nCommand.Parameters.Add("@AccountDepreciationKey", SqlDbType.Int).Value = m_AccountDepreciationKey;

                nCommand.Parameters.Add("@DateBeginDepreciation", SqlDbType.DateTime).Value = m_DateBeginDepreciation;
                nCommand.Parameters.Add("@DateIncreaseFixedAsset", SqlDbType.DateTime).Value = m_DateIncreaseFixedAsset;
                nCommand.Parameters.Add("@DateDecreaseFixedAsset", SqlDbType.DateTime).Value = m_DateDecreaseFixedAsset;


                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";
            string nSQL = "INSERT INTO FAS_FixedAssets( "
                        + " FixedAssetID, FixedAssetName, FixedAssetDescription,CountryKey, MakeYear, UsedYear,  "
                        + " DepartmentKey,SituationKey, CategoryKey ,"
                        + " CostFixedAsset, DepreciationMethod,  DepreciationYearNumber,"
                        + " AccountCostKey, AccountDepreciationKey, DateBeginDepreciation, DateIncreaseFixedAsset,DateDecreaseFixedAsset,VoucherKey)"
                        + " VALUES(@FixedAssetID, @FixedAssetName, @FixedAssetDescription,@CountryKey, @MakeYear,  @UsedYear, "
                        + " @DepartmentKey,@SituationKey,@CategoryKey,"
                        + " @CostFixedAsset,@DepreciationMethod,  @DepreciationYearNumber,"
                        + " @AccountCostKey, @AccountDepreciationKey,@DateBeginDepreciation, @DateIncreaseFixedAsset,@DateDecreaseFixedAsset,@VoucherKey )"
                        + " SELECT FixedAssetKey FROM FixedAssets WHERE FixedAssetKey = SCOPE_IDENTITY()";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = m_FixedAssetKey;
                nCommand.Parameters.Add("@FixedAssetID", SqlDbType.NVarChar).Value = m_FixedAssetID;
                nCommand.Parameters.Add("@FixedAssetName", SqlDbType.NVarChar).Value = m_FixedAssetName;
                nCommand.Parameters.Add("@FixedAssetDescription", SqlDbType.NText).Value = m_FixedAssetDescription;
                nCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = m_CountryKey;

                nCommand.Parameters.Add("@MakeYear", SqlDbType.Int).Value = m_MakeYear;
                nCommand.Parameters.Add("@UsedYear", SqlDbType.Int).Value = m_UsedYear;

                nCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = m_DepartmentKey;
                nCommand.Parameters.Add("@SituationKey", SqlDbType.Int).Value = m_SituationKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;

                nCommand.Parameters.Add("@CostFixedAsset", SqlDbType.Money).Value = m_CostFixedAsset;
                nCommand.Parameters.Add("@DepreciationMethod", SqlDbType.Bit).Value = m_DepreciationMethod;
                nCommand.Parameters.Add("@DepreciationYearNumber", SqlDbType.Int).Value = m_DepreciationYearNumber;

                nCommand.Parameters.Add("@AccountCostKey", SqlDbType.Int).Value = m_AccountCostKey;
                nCommand.Parameters.Add("@AccountDepreciationKey", SqlDbType.Int).Value = m_AccountDepreciationKey;

                nCommand.Parameters.Add("@DateBeginDepreciation", SqlDbType.DateTime).Value = m_DateBeginDepreciation;
                nCommand.Parameters.Add("@DateIncreaseFixedAsset", SqlDbType.DateTime).Value = m_DateIncreaseFixedAsset;
                nCommand.Parameters.Add("@DateDecreaseFixedAsset", SqlDbType.DateTime).Value = m_DateDecreaseFixedAsset;

                nResult = nCommand.ExecuteScalar().ToString();
                m_FixedAssetKey = int.Parse(nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Save()
        {
            if (m_FixedAssetKey == 0)
                return Create();
            else
                return Update();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FAS_FixedAssets WHERE FixedAssetKey = @FixedAssetKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FixedAssetKey", SqlDbType.Int).Value = m_FixedAssetKey;
                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        #endregion

    }
}
