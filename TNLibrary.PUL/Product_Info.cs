﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.PUL
{
    public class Product_Info
    {

        private int m_ProductKey = 0;
        private string m_ProductID = "";
        private string m_ProductName = "";
        private string m_ProductDescription = "";
        private string m_Unit = "";

        private double m_UnitPrice = 0; // danh cho chung tu
        private double m_SalePrice = 0;
        private double m_PurchasePrice = 0;
        private float m_VAT = 0;

        private float m_StoreMin = 0;
        private float m_StoreMax = 0;
        private string m_Notes = "";

        private int m_CategoryKey = 0;
        private string m_CategoryName = "";
        
        private int m_ProductTypeKey = 1;
        private string m_ProductTypeName = "";

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";

        public Product_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region [ Constructor Get Information ]

        public Product_Info(int ProductKey)
        {
            string nSQL = "SELECT A.*,B.CategoryName,C.ProductTypeName FROM PUL_Products A "
                        + " LEFT JOIN PUL_ProductCategories B ON A.CategoryKey = B.CategoryKey "
                        + " LEFT JOIN PUL_ProductType C ON A.ProductTypeKey = C.ProductTypeKey "
                        + " WHERE A.ProductKey = @ProductKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString().Trim();
                    m_ProductName = nReader["ProductName"].ToString();
                    m_ProductDescription = nReader["ProductDescription"].ToString();
                    m_Unit = nReader["Unit"].ToString();
                    m_SalePrice = double.Parse(nReader["SalePrice"].ToString());
                    m_PurchasePrice = double.Parse(nReader["PurchasePrice"].ToString());

                    m_StoreMin = float.Parse(nReader["StoreMin"].ToString());
                    m_StoreMax = float.Parse(nReader["StoreMax"].ToString());
                    m_VAT = float.Parse(nReader["VAT"].ToString());
                    m_Notes = nReader["Notes"].ToString();
                    
                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
                    m_CategoryName = nReader["CategoryName"].ToString();

                    m_ProductTypeKey = int.Parse(nReader["ProductTypeKey"].ToString());
                    m_ProductTypeName = nReader["ProductTypeName"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Product_Info(string ProductID)
        {
            string nSQL = "SELECT A.*,B.CategoryName,C.ProductTypeName FROM PUL_Products A "
                       + " LEFT JOIN PUL_ProductCategories B ON A.CategoryKey = B.CategoryKey "
                       + " LEFT JOIN PUL_ProductType C ON A.ProductTypeKey = C.ProductTypeKey "
                       + " WHERE A.ProductID= @ProductID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString().Trim();
                    m_ProductName = nReader["ProductName"].ToString();
                    m_Unit = nReader["Unit"].ToString();
                    m_ProductDescription = nReader["ProductDescription"].ToString();
                    m_SalePrice = double.Parse(nReader["SalePrice"].ToString());
                    m_PurchasePrice = double.Parse(nReader["PurchasePrice"].ToString());

                    m_StoreMin = float.Parse(nReader["StoreMin"].ToString());
                    m_StoreMax = float.Parse(nReader["StoreMax"].ToString());
                    m_VAT = float.Parse(nReader["VAT"].ToString());
                    m_Notes = nReader["Notes"].ToString();

                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
                    m_CategoryName = nReader["CategoryName"].ToString();

                    m_ProductTypeKey = int.Parse(nReader["ProductTypeKey"].ToString());
                    m_ProductTypeName = nReader["ProductTypeName"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public void GetProduct_Info()
        {
            string nSQL = "SELECT A.*,B.CategoryName,C.ProductTypeName FROM PUL_Products A "
                       + " LEFT JOIN PUL_ProductCategories B ON A.CategoryKey = B.CategoryKey "
                       + " LEFT JOIN PUL_ProductType C ON A.ProductTypeKey = C.ProductTypeKey "
                       + " WHERE A.ProductKey = @ProductKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = m_ProductKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString().Trim();
                    m_ProductName = nReader["ProductName"].ToString();
                    m_ProductDescription = nReader["ProductDescription"].ToString();
                    m_Unit = nReader["Unit"].ToString();
                    m_SalePrice = double.Parse(nReader["SalePrice"].ToString());
                    m_PurchasePrice = double.Parse(nReader["PurchasePrice"].ToString());

                    m_StoreMin = float.Parse(nReader["StoreMin"].ToString());
                    m_StoreMax = float.Parse(nReader["StoreMax"].ToString());
                    m_VAT = float.Parse(nReader["VAT"].ToString());
                    m_Notes = nReader["Notes"].ToString();

                    m_CategoryKey = int.Parse(nReader["CategoryKey"].ToString());
                    m_CategoryName = nReader["CategoryName"].ToString();

                    m_ProductTypeKey = int.Parse(nReader["ProductTypeKey"].ToString());
                    m_ProductTypeName = nReader["ProductTypeName"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        #endregion
        
        #region [ Properties ]

        public int Key
        {
            get { return m_ProductKey; }
            set { m_ProductKey = value; }
        }
        public string ID
        {
            get { return m_ProductID; }
            set { m_ProductID = value; }
        }
        public string Name
        {
            get { return m_ProductName; }
            set { m_ProductName = value; }
        }

        public string Descriptiontion
        {
            get { return m_ProductDescription; }
            set { m_ProductDescription = value; }
        }
        public string Unit
        {
            get { return m_Unit; }
            set { m_Unit = value; }
        }

        public double UnitPrice
        {
            get { return m_UnitPrice; }
            set { m_UnitPrice = value; }
        }
        public double SalePrice
        {
            get { return m_SalePrice; }
            set { m_SalePrice = value; }
        }

        public double PurchasePrice
        {
            get { return m_PurchasePrice; }
            set { m_PurchasePrice = value; }
        }
        public float StoreMin
        {
            get { return m_StoreMin; }
            set { m_StoreMin = value; }
        }
        public float StoreMax
        {
            get { return m_StoreMax; }
            set { m_StoreMax = value; }
        }

        public float VAT
        {
            get { return m_VAT; }
            set { m_VAT = value; }
        }
        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return m_CategoryName; }
            set { m_CategoryName = value; }
        }

        public int TypeKey
        {
            get { return m_ProductTypeKey; }
            set { m_ProductTypeKey = value; }
        }
        public string TypeName
        {
            get { return m_ProductTypeName; }
            set { m_ProductTypeName = value; }
        }
       
        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE PUL_Products SET "
                        + " ProductID = @ProductID,"
                        + " ProductName = @ProductName, "
                        + " ProductDescription = @ProductDescription ,"
                        + " Unit = @Unit ,"
                        + " SalePrice = @SalePrice ,"
                        + " PurchasePrice = @PurchasePrice ,"
                        + " StoreMin = @StoreMin ,"
                        + " StoreMax = @StoreMax,"
                        + " Notes = @Notes ,"
                        + " VAT = @VAT ,"
                        + " CategoryKey = @CategoryKey, "
                        + " ProductTypeKey = @ProductTypeKey, "
                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime=getdate() "

                        + " WHERE ProductKey = @ProductKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = m_ProductKey;
                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = m_ProductID;
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = m_ProductName;
                nCommand.Parameters.Add("@ProductDescription", SqlDbType.NVarChar).Value = m_ProductDescription;
                nCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = m_Unit;
                nCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = m_SalePrice;

                nCommand.Parameters.Add("@PurchasePrice", SqlDbType.Money).Value = m_PurchasePrice;
                nCommand.Parameters.Add("@StoreMin", SqlDbType.Float).Value = m_StoreMin;
                nCommand.Parameters.Add("@StoreMax", SqlDbType.Float).Value = m_StoreMax;
                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = m_VAT;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@ProductTypeKey", SqlDbType.Int).Value = m_ProductTypeKey;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO PUL_Products (ProductID, ProductName, ProductDescription, Unit,"
                        + " SalePrice, PurchasePrice, StoreMin, StoreMax,  Notes, VAT,CategoryKey,ProductTypeKey,"
                        + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                        + " VALUES(@ProductID, @ProductName, @ProductDescription, @Unit,"
                        + " @SalePrice, @PurchasePrice, @StoreMin, @StoreMax,  @Notes, @VAT,@CategoryKey,@ProductTypeKey,"
                        + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                        + " SELECT ProductKey FROM PUL_Products WHERE ProductKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = m_ProductID;
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = m_ProductName;
                nCommand.Parameters.Add("@ProductDescription", SqlDbType.NVarChar).Value = m_ProductDescription;
                nCommand.Parameters.Add("@Unit", SqlDbType.NVarChar).Value = m_Unit;
                nCommand.Parameters.Add("@SalePrice", SqlDbType.Money).Value = m_SalePrice;

                nCommand.Parameters.Add("@PurchasePrice", SqlDbType.Money).Value = m_PurchasePrice;
                nCommand.Parameters.Add("@StoreMin", SqlDbType.Float).Value = m_StoreMin;
                nCommand.Parameters.Add("@StoreMax", SqlDbType.Float).Value = m_StoreMax;
                nCommand.Parameters.Add("@Notes", SqlDbType.NText).Value = m_Notes;
                nCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = m_VAT;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@ProductTypeKey", SqlDbType.Int).Value = m_ProductTypeKey;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                m_ProductKey = int.Parse(nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            if (m_ProductKey == 0)
                return Create();
            else
                return Update();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM PUL_Products WHERE ProductKey = @ProductKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = m_ProductKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        #endregion

    }
}
