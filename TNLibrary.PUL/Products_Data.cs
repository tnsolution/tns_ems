﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.PUL
{
    public class Products_Data
    {
        public static DataTable ProductList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM PUL_Products A"
            + " LEFT JOIN PUL_ProductCategories B ON B.CategoryKey = A.CategoryKey ORDER BY ProductName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable ProductList(int CategoryKey)
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM PUL_Products A"
            + " LEFT JOIN PUL_ProductCategories B ON B.CategoryKey = A.CategoryKey "
            + " WHERE A.CategoryKey = @CategoryKey"
            + " ORDER BY ProductName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable ProductList(string ProductID, string ProductName)
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM PUL_Products A"
            + " LEFT JOIN PUL_ProductCategories B ON B.CategoryKey = A.CategoryKey "
            + " WHERE ProductID Like @ProductID AND ProductName Like @ProductName"
            + " ORDER BY ProductName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = "%" + ProductID + "%";
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%" + ProductName + "%";
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }
        public static DataTable WarehouseList()
        {

            DataTable nTable = new DataTable();
            string nSQL = "SELECT * FROM IVT_Warehouse ORDER BY WarehouseName DESC";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);

                nCustomers.Fill(nTable);
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception ex)
            {
                string strMessage = ex.ToString();
            }

            return nTable;
        }

        public static string UpdateCategory(int ProductKey, int CategoryKey)
        {
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                //---------- String SQL Access Database ---------------
                string nSQL = "UPDATE PUL_Products SET CategoryKey = @CategoryKey WHERE ProductKey = @ProductKey";


                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                SqlDataAdapter nCustomers = new SqlDataAdapter(nCommand);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nResult = nCommand.ExecuteNonQuery().ToString();

                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception err)
            {
                nResult = err.Message.ToString();
            }
            return nResult;
        }

        public static int CheckExistProduct(int ProductKey)
        {
            int nResult = 0;
            string nSQL = "PUL_CheckExistProduct";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
            nCommand.CommandType = CommandType.StoredProcedure;
            nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;

            string strAmount = nCommand.ExecuteScalar().ToString();
            int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
            nCommand.Dispose();

            return nResult;
        }

    }
}
