﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.FNC
{
    public class FinancialYear_Info
    {

        private int m_FinancialYearKey = 0;
        private int m_FinancialYearName = 2005;

        private DateTime m_FromDate = DateTime.Now;
        private DateTime m_ToDate = DateTime.Now;

        private bool m_Activate = false;
        private bool m_HasData = true;

        private string m_Message = "";
        public FinancialYear_Info()
        {

        }
        public FinancialYear_Info(int FinancialYearKey)
        {
            string nSQL = "SELECT * FROM FNC_FinancialYears WHERE FinancialYearKey =@FinancialYearKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FinancialYearKey", SqlDbType.Int).Value = FinancialYearKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_FinancialYearKey = int.Parse(nReader["FinancialYearKey"].ToString());
                    m_FinancialYearName = int.Parse(nReader["FinancialYearName"].ToString());
                    if (nReader["FromDate"] != null)
                        m_FromDate = (DateTime)nReader["FromDate"];
                    else
                        m_HasData = false;

                    if (nReader["ToDate"] != null)
                        m_ToDate = (DateTime)nReader["ToDate"];
                    else
                        m_HasData = false;

                    m_Activate = (bool)nReader["Activate"];

                }
                else
                    m_HasData = false;
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();
                nConnect.Close();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }

        }
        public FinancialYear_Info(bool Activate)
        {
            string nSQL = "SELECT * FROM FNC_FinancialYears WHERE Activate =@Activate";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Activate;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_FinancialYearKey = int.Parse(nReader["FinancialYearKey"].ToString());
                    m_FinancialYearName = int.Parse(nReader["FinancialYearName"].ToString());
                    if (nReader["FromDate"] != null)
                        m_FromDate = (DateTime)nReader["FromDate"];
                    else
                        m_HasData = false;

                    if (nReader["ToDate"] != null)
                        m_ToDate = (DateTime)nReader["ToDate"];
                    else
                        m_HasData = false;

                    m_Activate = (bool)nReader["Activate"];

                }
                else
                    m_HasData = false;

                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();
                nConnect.Close();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
        }
        public FinancialYear_Info(string YearFinancial)
        {
            string nSQL = "SELECT * FROM FNC_FinancialYears WHERE FinancialYearName =@FinancialYearName";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FinancialYearName", SqlDbType.Int).Value = int.Parse(YearFinancial);

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_FinancialYearKey = int.Parse(nReader["FinancialYearKey"].ToString());
                    m_FinancialYearName = int.Parse(nReader["FinancialYearName"].ToString());
                    if (nReader["FromDate"] != null)
                        m_FromDate = (DateTime)nReader["FromDate"];
                    else
                        m_HasData = false;

                    if (nReader["ToDate"] != null)
                        m_ToDate = (DateTime)nReader["ToDate"];
                    else
                        m_HasData = false;

                    m_Activate = (bool)nReader["Activate"];

                }
                else
                    m_HasData = false;
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();
                nConnect.Close();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();

            }
        }

        #region [ Properties ]
        // Info FinancialYear
        public int FinancialYearKey
        {
            get { return m_FinancialYearKey; }
            set { m_FinancialYearKey = value; }
        }
        public int FinancialYearName
        {
            get { return m_FinancialYearName; }
            set { m_FinancialYearName = value; }
        }

        public string FromMonth
        {
            get
            {
                string nCloseMonth = m_FromDate.Month.ToString() + "/" + m_FromDate.Year.ToString();
                if (nCloseMonth.Length < 7)
                    nCloseMonth = "0" + nCloseMonth;
                return nCloseMonth;
            }
        }
        public DateTime FromDate
        {
            get { return m_FromDate; }
            set
            {
                DateTime nDate = value;
                m_FromDate = new DateTime(nDate.Year, nDate.Month, nDate.Day, 0, 0, 0);
            }
        }
        public DateTime ToDate
        {
            get { return m_ToDate; }
            set
            {
                DateTime nDate = value;
                m_ToDate = new DateTime(nDate.Year, nDate.Month, nDate.Day, 23, 59, 0);
            }
        }

        public bool HasData
        {
            get { return m_HasData; }
            set { m_HasData = value; }
        }
        public bool Activate
        {
            get { return m_Activate; }
            set { m_Activate = value; }
        }

        #endregion

        #region [ Update data Information ]
        public string Update()
        {
            string Result = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE FNC_FinancialYears SET "
                        + " FromDate = @FromDate, "
                        + " ToDate = @ToDate, "
                        + " Activate = @Activate "
                        + " WHERE FinancialYearKey = @FinancialYearKey";


            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@FinancialYearKey", SqlDbType.Int).Value = m_FinancialYearKey;
                nCommand.Parameters.Add("@FromDate", SqlDbType.SmallDateTime).Value = m_FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.SmallDateTime).Value = m_ToDate;
                nCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = m_Activate;

                Result = nCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception err)
            {
                m_Message = err.Message.ToString();
            }
            return Result;
        }
        public string SetActivate(bool Flag)
        {
            string Result = "";
            SetUnActivateAll();
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE FNC_FinancialYears Set Activate =@Activate WHERE FinancialYearKey = @FinancialYearKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@FinancialYearKey", SqlDbType.Int).Value = m_FinancialYearKey;
                nCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Flag;
                Result = nCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception err)
            {
                m_Message = err.Message.ToString();
            }
            return Result;
        }
        public string SetUnActivateAll()
        {
            string Result = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE FNC_FinancialYears Set Activate = 0 ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                Result = nCommand.ExecuteNonQuery().ToString();
                //---- Close Connect SQL ----
                nCommand.Dispose();
                nConnect.Close();
            }
            catch (Exception err)
            {
                m_Message = err.Message.ToString();
            }
            return Result;
        }
        #endregion
    }
}
