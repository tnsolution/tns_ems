﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.CloseMonth
{
    public class CloseMonth_Data
    {
      
        public static DataTable CustomerVendor(string CloseMonth, int CategoryKey, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        #region [ Customer ]
        public static DataTable Customers(string CloseMonth)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Customer_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = 0;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Customers(string CloseMonth, int CategoryKey)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Customer_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Customers(string CloseMonth, int CategoryKey, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Customer_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        #endregion

        #region [ Vendor ]
        public static DataTable Vendors(string CloseMonth, int CategoryKey, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Vendor_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;

        }
        #endregion

        #region [ Inventory ]
        public static DataTable Inventorys(string CloseMonth, int WarehouseKey)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Inventory_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = 0;
                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Inventorys(string CloseMonth, int WarehouseKey, int CategoryKey)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Inventory_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Inventorys(string CloseMonth, int WarehouseKey, int CategoryKey, string ProductID, string ProductName)
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_CloseMonth_Inventory_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.Char).Value = CloseMonth;

                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = "%" + ProductID + "%";
                nCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = "%" + ProductName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        #endregion
    }
}
