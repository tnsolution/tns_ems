﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.FNC.CloseMonth
{
    public class CloseMonth_Inventory_Info : CloseAccount
    {
        private int m_ProductKey = 0;
        private string m_ProductID = "";
        private string m_ProductName = "";
        private int m_WarehouseKey = 0;

        private float m_BeginQuantity = 0;
        private float m_ImportQuantity = 0;
        private float m_ExportQuantity = 0;
        private float m_EndQuantity = 0;

        public CloseMonth_Inventory_Info()
        {
        }
        #region [ Constructor Get Information ]
        public CloseMonth_Inventory_Info(int CloseAccountKey)
        {

            string nSQL = " SELECT A.*,B.ProductID,B.ProductName FROM FNC_CloseMonth_Inventory A "
                        + " INNER JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                        + " WHERE CloseAccountKey = @CloseAccountKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.Int).Value = CloseAccountKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString();
                    m_ProductName = nReader["ProductName"].ToString();

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());

                    m_BeginQuantity = float.Parse(nReader["BeginQuantity"].ToString());
                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    m_ImportQuantity = float.Parse(nReader["ImportQuantity"].ToString());
                    m_ExportQuantity = float.Parse(nReader["ExportQuantity"].ToString());
                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    m_EndQuantity = float.Parse(nReader["EndQuantity"].ToString());
                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        public CloseMonth_Inventory_Info(string CloseMonth, int ProductKey, int WarehouseKey)
        {
            base.CloseMonth = CloseMonth;
            base.AccountKey = AccountKey;
            m_ProductKey = ProductKey;
            m_WarehouseKey = WarehouseKey;

            string nSQL = " SELECT A.*,B.ProductID,B.ProductName,C.AccountID,C.AccountNameVN FROM FNC_CloseMonth_Inventory A "
                        + " INNER JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                        + " WHERE CloseMonth=@CloseMonth "
                        + " AND A.ProductKey = @ProductKey "
                        + " AND A.WarehouseKey = @WarehouseKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = ProductKey;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseAccountKey = (int)nReader["CloseAccountKey"];
                    base.CloseMonth = nReader["CloseMonth"].ToString();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString();
                    m_ProductName = nReader["ProductName"].ToString();

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());

                    m_BeginQuantity = float.Parse(nReader["BeginQuantity"].ToString());
                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    m_ImportQuantity = float.Parse(nReader["ImportQuantity"].ToString());
                    m_ExportQuantity = float.Parse(nReader["ExportQuantity"].ToString());
                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    m_EndQuantity = float.Parse(nReader["EndQuantity"].ToString());
                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public CloseMonth_Inventory_Info(string CloseMonth, string ProductID, string AccountID, int WarehouseKey)
        {

            string nSQL = "SELECT A.*,B.ProductID,B.ProductName FROM FNC_CloseMonth_Inventory A "
                            + " INNER JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                            + " WHERE A.CloseMonth = @CloseMonth  "
                            + " AND B.ProductID = @ProductID "
                            + " AND A.WarehouseKey = @WarehouseKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = CloseMonth;
                nCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    base.CloseMonth = nReader["CloseMonth"].ToString();

                    m_ProductKey = int.Parse(nReader["ProductKey"].ToString());
                    m_ProductID = nReader["ProductID"].ToString();
                    m_ProductName = nReader["ProductName"].ToString();

                    m_WarehouseKey = int.Parse(nReader["WarehouseKey"].ToString());

                    m_BeginQuantity = float.Parse(nReader["BeginQuantity"].ToString());
                    base.BeginAmountDebitCurrencyMain = double.Parse(nReader["BeginAmountDebitCurrencyMain"].ToString());
                    base.BeginAmountCreditCurrencyMain = double.Parse(nReader["BeginAmountCreditCurrencyMain"].ToString());
                    base.BeginAmountDebitCurrencyForeign = double.Parse(nReader["BeginAmountDebitCurrencyForeign"].ToString());
                    base.BeginAmountCreditCurrencyForeign = double.Parse(nReader["BeginAmountCreditCurrencyForeign"].ToString());

                    m_ImportQuantity = float.Parse(nReader["ImportQuantity"].ToString());
                    m_ExportQuantity = float.Parse(nReader["ExportQuantity"].ToString());
                    base.MiddleAmountDebitCurrencyMain = double.Parse(nReader["MiddleAmountDebitCurrencyMain"].ToString());
                    base.MiddleAmountCreditCurrencyMain = double.Parse(nReader["MiddleAmountCreditCurrencyMain"].ToString());
                    base.MiddleAmountDebitCurrencyForeign = double.Parse(nReader["MiddleAmountDebitCurrencyForeign"].ToString());
                    base.MiddleAmountCreditCurrencyForeign = double.Parse(nReader["MiddleAmountCreditCurrencyForeign"].ToString());

                    m_EndQuantity = float.Parse(nReader["EndQuantity"].ToString());
                    base.EndAmountDebitCurrencyMain = double.Parse(nReader["EndAmountDebitCurrencyMain"].ToString());
                    base.EndAmountCreditCurrencyMain = double.Parse(nReader["EndAmountCreditCurrencyMain"].ToString());
                    base.EndAmountDebitCurrencyForeign = double.Parse(nReader["EndAmountDebitCurrencyForeign"].ToString());
                    base.EndAmountCreditCurrencyForeign = double.Parse(nReader["EndAmountCreditCurrencyForeign"].ToString());

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]
        public int ProductKey
        {
            get
            {
                return m_ProductKey;
            }
            set
            {
                m_ProductKey = value;
            }
        }
        public string ProductName
        {
            get
            {
                return m_ProductName;
            }
            set
            {
                m_ProductName = value;
            }
        }

        public int WarehouseKey
        {
            get
            {
                return m_WarehouseKey;
            }
            set
            {
                m_WarehouseKey = value;
            }
        }
        public float BeginQuantity
        {
            get
            {
                return m_BeginQuantity;
            }
            set
            {
                m_BeginQuantity = value;
            }
        }
        public float ImportQuantity
        {
            get
            {
                return m_ImportQuantity;
            }
            set
            {
                m_ImportQuantity = value;
            }
        }
        public float ExportQuantity
        {
            get
            {
                return m_ExportQuantity;
            }
            set
            {
                m_ExportQuantity = value;
            }
        }
        public float EndQuantity
        {
            get
            {
                return m_EndQuantity;
            }
            set
            {
                m_EndQuantity = value;
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            string nResult = "";
            string nSQL  = "INSERT INTO FNC_CloseMonth_Inventory( "
                    + " CloseMonth, AccountKey,ProductKey,WarehouseKey,BeginQuantity,ImportQuantity,ExportQuantity,EndQuantity,"
                    + " BeginAmountDebitCurrencyMain ,BeginAmountCreditCurrencyMain , BeginAmountDebitCurrencyForeign ,BeginAmountCreditCurrencyForeign, "
                    + " MiddleAmountDebitCurrencyMain,MiddleAmountCreditCurrencyMain, MiddleAmountDebitCurrencyForeign,MiddleAmountCreditCurrencyForeign, "
                    + " EndAmountDebitCurrencyMain   ,EndAmountCreditCurrencyMain   , EndAmountDebitCurrencyForeign   ,EndAmountCreditCurrencyForeign)"
                    + " VALUES(@CloseMonth, @AccountKey, @ProductKey,@WarehouseKey,@BeginQuantity,@ImportQuantity,@ExportQuantity,@EndQuantity, "
                    + " @BeginAmountDebitCurrencyMain ,@BeginAmountCreditCurrencyMain , @BeginAmountDebitCurrencyForeign , @BeginAmountCreditCurrencyForeign, "
                    + " @MiddleAmountDebitCurrencyMain,@MiddleAmountCreditCurrencyMain, @MiddleAmountDebitCurrencyForeign, @MiddleAmountCreditCurrencyForeign, "
                    + " @EndAmountDebitCurrencyMain   ,@EndAmountCreditCurrencyMain   , @EndAmountDebitCurrencyForeign   , @EndAmountCreditCurrencyForeign)"
                    + " SELECT CloseAccountKey FROM FNC_CloseMonth_Inventory WHERE CloseAccountKey = SCOPE_IDENTITY()";
            

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = m_ProductKey;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;

                nCommand.Parameters.Add("@BeginQuantity", SqlDbType.Float).Value = m_BeginQuantity;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@ImportQuantity", SqlDbType.Float).Value = m_ImportQuantity;
                nCommand.Parameters.Add("@ExportQuantity", SqlDbType.Float).Value = m_ExportQuantity;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndQuantity", SqlDbType.Float).Value = m_EndQuantity;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;

                nResult = nCommand.ExecuteScalar().ToString();
                int nCloseAccountKey = 0;
                int.TryParse(nResult, out nCloseAccountKey);
                base.CloseAccountKey = nCloseAccountKey;


                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Update()
        {
            string nResult = "";

            string nSQL = "";
            nSQL = "UPDATE FNC_CloseMonth_Inventory SET "
                        + " ProductKey = @ProductKey, "
                        + " AccountKey = @AccountKey , "
                        + " CloseMonth=@CloseMonth ,"
                        + " WarehouseKey = @WarehouseKey, "

                        + " BeginQuantity = @BeginQuantity,"
                        + " BeginAmountDebitCurrencyMain = @BeginAmountDebitCurrencyMain,"
                        + " BeginAmountCreditCurrencyMain = @BeginAmountCreditCurrencyMain, "
                        + " BeginAmountDebitCurrencyForeign = @BeginAmountDebitCurrencyForeign, "
                        + " BeginAmountCreditCurrencyForeign = @BeginAmountCreditCurrencyForeign, "

                        + " ImportQuantity = @ImportQuantity,"
                        + " ExportQuantity = @ExportQuantity,"
                        + " MiddleAmountDebitCurrencyMain = @MiddleAmountDebitCurrencyMain,"
                        + " MiddleAmountCreditCurrencyMain = @MiddleAmountCreditCurrencyMain, "
                        + " MiddleAmountDebitCurrencyForeign = @MiddleAmountDebitCurrencyForeign, "
                        + " MiddleAmountCreditCurrencyForeign = @MiddleAmountCreditCurrencyForeign, "

                         + " EndQuantity = @EndQuantity ,"
                        + " EndAmountDebitCurrencyMain = @EndAmountDebitCurrencyMain,"
                        + " EndAmountCreditCurrencyMain = @EndAmountCreditCurrencyMain, "
                        + " EndAmountDebitCurrencyForeign = @EndAmountDebitCurrencyForeign, "
                        + " EndAmountCreditCurrencyForeign = @EndAmountCreditCurrencyForeign "

                        + " WHERE CloseAccountKey = @CloseAccountKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NChar).Value = base.CloseMonth;
                nCommand.Parameters.Add("@AccountKey", SqlDbType.Int).Value = base.AccountKey;
                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = m_ProductKey;
                nCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = m_WarehouseKey;

                nCommand.Parameters.Add("@BeginQuantity", SqlDbType.Float).Value = m_BeginQuantity;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyMain", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyMain", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@BeginAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@BeginAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.BeginAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@ImportQuantity", SqlDbType.Float).Value = m_ImportQuantity;
                nCommand.Parameters.Add("@ExportQuantity", SqlDbType.Float).Value = m_ExportQuantity;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyMain", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@MiddleAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@MiddleAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.MiddleAmountCreditCurrencyForeign;

                nCommand.Parameters.Add("@EndQuantity", SqlDbType.Float).Value = m_EndQuantity;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyMain", SqlDbType.Money).Value = base.EndAmountDebitCurrencyMain;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyMain", SqlDbType.Money).Value = base.EndAmountCreditCurrencyMain;
                nCommand.Parameters.Add("@EndAmountDebitCurrencyForeign", SqlDbType.Money).Value = base.EndAmountDebitCurrencyForeign;
                nCommand.Parameters.Add("@EndAmountCreditCurrencyForeign", SqlDbType.Money).Value = base.EndAmountCreditCurrencyForeign;


                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            return nResult;
        }
        public string Save()
        {
            if (base.CloseAccountKey ==0 )
                return Create();
            else
                return Update();
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = " DELETE FROM FNC_CloseMonth_Inventory "
                        + " WHERE CloseAccountKey = @CloseAccountKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CloseAccountKey", SqlDbType.NChar).Value = base.CloseAccountKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
