﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.FNC.Bills
{
    public class Cash_Receipt_Advance_Info
    {
        private int m_Key = 0;
        private string m_ID = "";
        private string m_Description = "";
        private DateTime m_BillDate = DateTime.Now;

        private string m_AccountStockKey ="";
        private string m_AccountStockNumber ="";
        private string m_MoneyComingType = "T0";
        private DateTime m_MoneyComingDate = DateTime.Now.AddDays(4);

        private double m_MoneyRate = 0;
        private double m_Amount = 0;

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";

        public Cash_Receipt_Advance_Info()
        {

        }

        #region [ Constructor Get Information ]
        public Cash_Receipt_Advance_Info(int ReceiptKey)
        {
            string nSQL = "SELECT A.*,B.AccountStockNumber FROM FNC_Cash_Receipts_Advance A "
                        + " LEFT JOIN SER_Accounts B ON A.AccountStockKey = B.AccountStockKey "
                        + " WHERE ReceiptKey =@ReceiptKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = ReceiptKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_Key = int.Parse(nReader["ReceiptKey"].ToString());
                    m_ID = nReader["ReceiptID"].ToString();
                    m_Description = nReader["ReceiptDescription"].ToString();
                    m_BillDate = (DateTime)nReader["ReceiptDate"];

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();
                    m_AccountStockNumber = nReader["AccountStockNumber"].ToString();
                    m_MoneyComingType = nReader["MoneyComingType"].ToString();
                    m_MoneyComingDate = (DateTime)nReader["MoneyComingDate"];

                    m_MoneyRate = double.Parse(nReader["MoneyRate"].ToString());
                    m_Amount = double.Parse(nReader["Amount"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Cash_Receipt_Advance_Info(string ReceiptID)
        {
            string nSQL = "SELECT A.*,B.AccountStockNumber FROM FNC_Cash_Receipts_Advance A "
                        + " LEFT JOIN SER_Accounts B ON A.AccountStockKey = B.AccountStockKey "
                        + " WHERE ReceiptID =@ReceiptID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = ReceiptID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_Key = int.Parse(nReader["ReceiptKey"].ToString());
                    m_ID = nReader["ReceiptID"].ToString();
                    m_Description = nReader["ReceiptDescription"].ToString();
                    m_BillDate = (DateTime)nReader["ReceiptDate"];

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();
                    m_AccountStockNumber = nReader["AccountStockNumber"].ToString();
                    m_MoneyComingType = nReader["MoneyComingType"].ToString();
                    m_MoneyComingDate = (DateTime)nReader["MoneyComingDate"];

                    m_MoneyRate = double.Parse(nReader["MoneyRate"].ToString());
                    m_Amount = double.Parse(nReader["Amount"].ToString());

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

    
        #endregion

        #region [Properties]
        public int Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public DateTime BillDate
        {
            get { return m_BillDate; }
            set { m_BillDate = value; }
        }

        public string AccountStockKey
        {
            get { return m_AccountStockKey; }
            set { m_AccountStockKey = value; }
        }
       
        public string AccountStockNumber
        {
            get { return m_AccountStockNumber; }
            set { m_AccountStockNumber = value; }
        }

        public string MoneyComingType
        {
            get { return m_MoneyComingType; }
            set { m_MoneyComingType = value; }
        }

        public DateTime MoneyComingDate
        {
            get { return m_MoneyComingDate; }
            set { m_MoneyComingDate = value; }
        }
        public double MoneyRate
        {
            get { return m_MoneyRate; }
            set { m_MoneyRate = value; }
        }

        public double Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
    
        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string nResult = "";
            string nSQL = "UPDATE FNC_Cash_Receipts_Advance SET "
                            + " ReceiptID = @ReceiptID,"
                            + " ReceiptDescription = @ReceiptDescription, "
                            + " ReceiptDate = @ReceiptDate ,"

                            + " AccountStockKey = @AccountStockKey, "
                            + " MoneyComingType = @MoneyComingType ,"
                            + " MoneyComingDate = @MoneyComingDate ,"

                            + " MoneyRate = @MoneyRate ,"
                            + " Amount = @Amount ,"

                            + " ModifiedBy= @ModifiedBy,"
                            + " ModifiedDateTime=getdate() "

                            + " WHERE ReceiptKey = @ReceiptKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = m_Key;
                nCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = m_ID;
                nCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = m_BillDate;

                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;
                nCommand.Parameters.Add("@MoneyComingType", SqlDbType.NChar).Value = m_MoneyComingType;
                nCommand.Parameters.Add("@MoneyComingDate", SqlDbType.DateTime).Value = m_MoneyComingDate;

                nCommand.Parameters.Add("@MoneyRate", SqlDbType.Money).Value = m_MoneyRate;
                nCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = m_Amount;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
        public string Create()
        {
            string nResult = "";
            string nSQL = "INSERT INTO FNC_Cash_Receipts_Advance( "
                    + " ReceiptID, ReceiptDescription, ReceiptDate, AccountStockKey, MoneyComingType, MoneyComingDate,Amount,MoneyRate,"
                    + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                    + " VALUES(@ReceiptID, @ReceiptDescription, @ReceiptDate, @AccountStockKey, @MoneyComingType, @MoneyComingDate,@Amount,@MoneyRate,"
                    + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                    + " SELECT ReceiptKey FROM FNC_Cash_Receipts_Advance WHERE ReceiptKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ReceiptID", SqlDbType.NVarChar).Value = m_ID;
                nCommand.Parameters.Add("@ReceiptDescription", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@ReceiptDate", SqlDbType.DateTime).Value = m_BillDate;

                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;
                nCommand.Parameters.Add("@MoneyComingType", SqlDbType.NChar).Value = m_MoneyComingType;
                nCommand.Parameters.Add("@MoneyComingDate", SqlDbType.DateTime).Value = m_MoneyComingDate;

                nCommand.Parameters.Add("@MoneyRate", SqlDbType.Money).Value = m_MoneyRate;
                nCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = m_Amount;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                int nReceiptKey = 0;
                int.TryParse(nResult, out nReceiptKey);
                m_Key = nReceiptKey;
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Save()
        {
            string nResult = "";
            if (m_Key == 0)
                nResult = Create();
            else
                nResult = Update();


            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FNC_Cash_Receipts_Advance WHERE ReceiptKey = @ReceiptKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = m_Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
