﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.Bills
{
    public class Cash_Payment_Object : Cash_Payment_Info
    {
        private DataTable m_InvoicesPaid = new DataTable();
        private DataTable m_ContractCreditPaid = new DataTable();

        public Cash_Payment_Object()
            : base()
        {
            m_InvoicesPaid = GetInvoicesPaid();
            m_ContractCreditPaid = GetContractCreditPaid();
        }
        public Cash_Payment_Object(int PaymentKey)
            : base(PaymentKey)
        {
            m_InvoicesPaid = GetInvoicesPaid();
            m_ContractCreditPaid = GetContractCreditPaid();
        }
        public Cash_Payment_Object(string PaymentID)
            : base(PaymentID)
        {
            m_InvoicesPaid = GetInvoicesPaid();
             m_ContractCreditPaid = GetContractCreditPaid();
        }

        #region [ Properties ]
        public DataTable InvoicesPaid
        {
            set
            {
                m_InvoicesPaid = value;
            }
            get
            {
                return m_InvoicesPaid;
            }
        }

        public DataTable ContractCreditsPaid
        {
            set
            {
                m_ContractCreditPaid = value;
            }
            get
            {
                return m_ContractCreditPaid;
            }
        }
        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.Key > 0)
            {
                DelInvoicesPaid();
                SaveInvoicesPaid();

                DelContractCreditPaid();
                SaveContractCreditPaid();
            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DelInvoicesPaid();
            DelContractCreditPaid();
            return nResult;
        }

        #endregion

        #region [ InvoicePay ]
        private DataTable GetInvoicesPaid()
        {
            DataTable nTable = new DataTable();
            string strSQL =   " SELECT A.*,B.InvoiceNumber,B.InvoiceDate,C.CustomerName FROM FNC_Cash_Payment_InvoicePaid A "
                            + " LEFT JOIN FNC_Invoice_Sales B ON A.InvoiceKey = B.InvoiceKey "
                            + " LEFT JOIN CRM_Customers C ON B.CustomerKey = C.CustomerKey "
                            + " WHERE A.PaymentKey = @PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@PaymentKey",SqlDbType.Int).Value =  base.Key;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);

                nAdapter.Fill(nTable);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        private string SaveInvoicesPaid()
        {
            string nResult = "";
            string nSQL = " INSERT INTO FNC_Cash_Payment_InvoicePaid( "
                        + " PaymentKey, InvoiceKey, PaymentAmount)"
                        + " VALUES( @PaymentKey, @InvoiceKey, @PaymentAmount)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand;
            try
            {

                foreach (DataRow nRow in m_InvoicesPaid.Rows)
                {
                    nCommand = new SqlCommand(nSQL, nConnect);
                    nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = base.Key;

                    nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = nRow["InvoiceKey"].ToString();
                    nCommand.Parameters.Add("@PaymentAmount", SqlDbType.Money).Value = nRow["PaymentAmount"].ToString();

                    nResult = nCommand.ExecuteNonQuery().ToString();
                    nCommand.Dispose();

                }
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        private string DelInvoicesPaid()
        {
            string nResult = "";
            string strSQL = " DELETE FROM FNC_Cash_Payment_InvoicePaid WHERE PaymentKey = @PaymentKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@PaymentKey",SqlDbType.Int).Value = base.Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

        #region [ ContractCreditPay ]
        private DataTable GetContractCreditPaid()
        {
            DataTable nTable = new DataTable();
            string strSQL = " SELECT A.*,B.ContractNumber,B.ContractDate,B.StockID FROM FNC_Cash_Payment_ContractCreditPaid A "
                            + " LEFT JOIN FNC_ContractCredit B ON B.ContractCreditKey = A.ContractCreditKey "
                            + " WHERE A.PaymentKey = @PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = base.Key;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);

                nAdapter.Fill(nTable);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        private string SaveContractCreditPaid()
        {
            string nResult = "";
            string nSQL = " INSERT INTO FNC_Cash_Payment_ContractCreditPaid( "
                        + " PaymentKey, ContractCreditKey, PaymentAmount)"
                        + " VALUES( @PaymentKey, @ContractCreditKey, @PaymentAmount)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand;
            try
            {

                foreach (DataRow nRow in m_ContractCreditPaid.Rows)
                {
                    nCommand = new SqlCommand(nSQL, nConnect);
                    nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = base.Key;

                    nCommand.Parameters.Add("@ContractCreditKey", SqlDbType.Int).Value = nRow["ContractCreditKey"].ToString();
                    nCommand.Parameters.Add("@PaymentAmount", SqlDbType.Money).Value = nRow["PaymentAmount"].ToString();

                    nResult = nCommand.ExecuteNonQuery().ToString();
                    nCommand.Dispose();

                }
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        private string DelContractCreditPaid()
        {
            string nResult = "";
            string strSQL = " DELETE FROM FNC_Cash_Payment_ContractCreditPaid WHERE PaymentKey = @PaymentKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = base.Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
