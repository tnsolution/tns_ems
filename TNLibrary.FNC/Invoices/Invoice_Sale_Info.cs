﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

using TNConfig;

namespace TNLibrary.FNC.Invoices
{
    public class Invoice_Sale_Info
    {
        private int m_InvoiceKey = 0;
        private string m_InvoiceID = "";
        private string m_InvoiceNumber = "";
        private string m_InvoiceSign = "";
        private string m_InvoiceDesign = "";
        private string m_InvoiceDescription = "";
        private string m_DocumentFollow = "";

        private DateTime m_InvoiceDate = DateTime.Now;
        private DateTime m_PaymentMaturity = new DateTime();

        private double m_AmountOrder = 0;
        private double m_VAT = 0;
        private int m_VATPercent = 0;
        private double m_AmountCurrencyMain = 0;

        private double m_AmountForTAX = 0;
        private double m_VATForTAX = 0;

        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";
        private string m_CustomerCurrencyID = "";

        private int m_CategoryKey = 2;
        private bool m_IsFixedAsset = false;
        private int m_BranchKey = 0;

        private bool m_PayByCash = true;
        private bool m_PayByAccountBank = false;

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";

        public Invoice_Sale_Info()
        {


        }
        #region [ Constructor Get Information ]

        public Invoice_Sale_Info(int InvoiceKey)
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName FROM FNC_Invoice_Sales A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
                        + " WHERE InvoiceKey = @InvoiceKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = InvoiceKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_InvoiceKey = int.Parse(nReader["InvoiceKey"].ToString());
                    m_InvoiceID = nReader["InvoiceID"].ToString();
                    m_InvoiceNumber = nReader["InvoiceNumber"].ToString();
                    m_InvoiceSign = nReader["InvoiceSign"].ToString();
                    m_InvoiceDesign = nReader["InvoiceDesign"].ToString();
                    m_InvoiceDescription = nReader["InvoiceDescription"].ToString();
                    m_DocumentFollow = nReader["DocumentFollow"].ToString();

                    m_InvoiceDate = (DateTime)nReader["InvoiceDate"];
                    string nPaymentMaturity = nReader["PaymentMaturity"].ToString();
                    if (nPaymentMaturity.Length > 0)
                        m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    else
                        m_PaymentMaturity = DateTime.MinValue;

                    m_AmountOrder = double.Parse(nReader["AmountOrder"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_VAT = double.Parse(nReader["VAT"].ToString());
                    m_VATPercent = int.Parse(nReader["VATPercent"].ToString());

                    m_AmountForTAX = double.Parse(nReader["AmountForTAX"].ToString());
                    m_VATForTAX = double.Parse(nReader["VATForTAX"].ToString());

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_IsFixedAsset = (bool)nReader["IsFixedAsset"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_PayByCash = (bool)nReader["PayByCash"];
                    m_PayByAccountBank = (bool)nReader["PayByAccountBank"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public Invoice_Sale_Info(string InvoiceID)
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName FROM FNC_Invoice_Sales A "
             + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
             + " WHERE InvoiceID =@InvoiceID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);


                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = InvoiceID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_InvoiceKey = int.Parse(nReader["InvoiceKey"].ToString());
                    m_InvoiceID = nReader["InvoiceID"].ToString();
                    m_InvoiceNumber = nReader["InvoiceNumber"].ToString();
                    m_InvoiceSign = nReader["InvoiceSign"].ToString();
                    m_InvoiceDesign = nReader["InvoiceDesign"].ToString();
                    m_InvoiceDescription = nReader["InvoiceDescription"].ToString();
                    m_DocumentFollow = nReader["DocumentFollow"].ToString();

                    m_InvoiceDate = (DateTime)nReader["InvoiceDate"];
                    string nPaymentMaturity = nReader["PaymentMaturity"].ToString();
                    if (nPaymentMaturity.Length > 0)
                        m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    else
                        m_PaymentMaturity = DateTime.MinValue;

                    m_AmountOrder = double.Parse(nReader["AmountOrder"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_VAT = double.Parse(nReader["VAT"].ToString());
                    m_VATPercent = int.Parse(nReader["VATPercent"].ToString());

                    m_AmountForTAX = double.Parse(nReader["AmountForTAX"].ToString());
                    m_VATForTAX = double.Parse(nReader["VATForTAX"].ToString());


                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_IsFixedAsset = (bool)nReader["IsFixedAsset"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_PayByCash = (bool)nReader["PayByCash"];
                    m_PayByAccountBank = (bool)nReader["PayByAccountBank"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }

        public void GetInvoice_Sale_Info()
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName FROM FNC_Invoice_Sales A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
                        + " WHERE InvoiceKey =@InvoiceKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = m_InvoiceKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_InvoiceKey = int.Parse(nReader["InvoiceKey"].ToString());
                    m_InvoiceID = nReader["InvoiceID"].ToString();
                    m_InvoiceNumber = nReader["InvoiceNumber"].ToString();
                    m_InvoiceSign = nReader["InvoiceSign"].ToString();
                    m_InvoiceDesign = nReader["InvoiceDesign"].ToString();
                    m_InvoiceDescription = nReader["InvoiceDescription"].ToString();
                    m_DocumentFollow = nReader["DocumentFollow"].ToString();

                    m_InvoiceDate = (DateTime)nReader["InvoiceDate"];
                    string nPaymentMaturity = nReader["PaymentMaturity"].ToString();
                    if (nPaymentMaturity.Length > 0)
                        m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    else
                        m_PaymentMaturity = DateTime.MinValue;

                    m_AmountOrder = double.Parse(nReader["AmountOrder"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_VAT = double.Parse(nReader["VAT"].ToString());
                    m_VATPercent = int.Parse(nReader["VATPercent"].ToString());

                    m_AmountForTAX = double.Parse(nReader["AmountForTAX"].ToString());
                    m_VATForTAX = double.Parse(nReader["VATForTAX"].ToString());


                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_IsFixedAsset = (bool)nReader["IsFixedAsset"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_PayByCash = (bool)nReader["PayByCash"];
                    m_PayByAccountBank = (bool)nReader["PayByAccountBank"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        public void GetInvoice_Sale_Info(string InvoiceNumber)
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName FROM FNC_Invoice_Sales A "
             + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
             + " WHERE InvoiceNumber =@InvoiceNumber";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = InvoiceNumber;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_InvoiceKey = int.Parse(nReader["InvoiceKey"].ToString());
                    m_InvoiceID = nReader["InvoiceID"].ToString();
                    m_InvoiceNumber = nReader["InvoiceNumber"].ToString();
                    m_InvoiceSign = nReader["InvoiceSign"].ToString();
                    m_InvoiceDesign = nReader["InvoiceDesign"].ToString();
                    m_InvoiceDescription = nReader["InvoiceDescription"].ToString();
                    m_DocumentFollow = nReader["DocumentFollow"].ToString();

                    m_InvoiceDate = (DateTime)nReader["InvoiceDate"];
                    string nPaymentMaturity = nReader["PaymentMaturity"].ToString();
                    if (nPaymentMaturity.Length > 0)
                        m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    else
                        m_PaymentMaturity = DateTime.MinValue;

                    m_AmountOrder = double.Parse(nReader["AmountOrder"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    m_VAT = double.Parse(nReader["VAT"].ToString());
                    m_VATPercent = int.Parse(nReader["VATPercent"].ToString());

                    m_AmountForTAX = double.Parse(nReader["AmountForTAX"].ToString());
                    m_VATForTAX = double.Parse(nReader["VATForTAX"].ToString());


                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_IsFixedAsset = (bool)nReader["IsFixedAsset"];
                    m_BranchKey = (int)nReader["BranchKey"];

                    m_PayByCash = (bool)nReader["PayByCash"];
                    m_PayByAccountBank = (bool)nReader["PayByAccountBank"];

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

        #region [ Properties ]

        public int InvoiceKey
        {
            get { return m_InvoiceKey; }
            set { m_InvoiceKey = value; }
        }
        public string InvoiceID
        {
            get { return m_InvoiceID; }
            set { m_InvoiceID = value; }
        }
        public string InvoiceNumber
        {
            get { return m_InvoiceNumber; }
            set { m_InvoiceNumber = value; }
        }
        public string InvoiceSign
        {
            get { return m_InvoiceSign; }
            set { m_InvoiceSign = value; }
        }
        public string InvoiceDesign
        {
            get { return m_InvoiceDesign; }
            set { m_InvoiceDesign = value; }
        }
        public string InvoiceDescription
        {
            get { return m_InvoiceDescription; }
            set { m_InvoiceDescription = value; }
        }
        public string DocumentFollow
        {
            get { return m_DocumentFollow; }
            set { m_DocumentFollow = value; }
        }

        public DateTime InvoiceDate
        {
            get { return m_InvoiceDate; }
            set { m_InvoiceDate = value; }
        }
        public DateTime PaymentMaturity
        {
            get { return m_PaymentMaturity; }
            set { m_PaymentMaturity = value; }
        }

        public double AmountOrder
        {
            get { return m_AmountOrder; }
            set { m_AmountOrder = value; }
        }

        public double VAT
        {
            get { return m_VAT; }
            set { m_VAT = value; }
        }
        public int VATPercent
        {
            get { return m_VATPercent; }
            set { m_VATPercent = value; }
        }
        public double AmountCurrencyMain
        {
            get { return m_AmountCurrencyMain; }
            set { m_AmountCurrencyMain = value; }
        }
        public double AmountForTAX
        {
            get { return m_AmountForTAX; }
            set { m_AmountForTAX = value; }
        }
        public double VATForTAX
        {
            get { return m_VATForTAX; }
            set { m_VATForTAX = value; }

        }

        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }
        public string CustomerCurrencyID
        {
            get { return m_CustomerCurrencyID; }
            set { m_CustomerCurrencyID = value; }
        }

        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }
        public bool IsFixedAsset
        {
            get { return m_IsFixedAsset; }
            set { m_IsFixedAsset = value; }
        }

        public int BranchKey
        {
            get { return m_BranchKey; }
            set { m_BranchKey = value; }
        }

        public bool PayByCash
        {
            get { return m_PayByCash; }
            set { m_PayByCash = value; }
        }
        public bool PayByAccountBank
        {
            get { return m_PayByAccountBank; }
            set { m_PayByAccountBank = value; }
        }

        public double AmountPayed
        {
            get
            {
                double nResult = 0;
                string nSQL = "SELECT isnull(Sum(ReceiptAmount),0) AS SumAmount "
                             + " FROM FNC_CashReceipt_InvoicePaid WHERE InvoiceKey =@InvoiceKey";

                string nConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                try
                {
                    SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                    nCommand.CommandType = CommandType.Text;
                    nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = InvoiceKey;
                    nResult = double.Parse(nCommand.ExecuteScalar().ToString());

                    nCommand.Dispose();

                }
                catch (Exception Err)
                {
                    m_Message = Err.ToString();
                }
                finally
                {
                    nConnect.Close();
                }
                return nResult;
            }
        }

        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string nResult = "";
            string nSQL = "UPDATE FNC_Invoice_Sales SET "
                            + " InvoiceID = @InvoiceID,"
                            + " InvoiceDescription = @InvoiceDescription, "
                            + " InvoiceNumber = @InvoiceNumber,"
                            + " InvoiceSign = @InvoiceSign, "
                            + " InvoiceDesign = @InvoiceDesign, "

                            + " InvoiceDate = @InvoiceDate ,"
                            + " PaymentMaturity = @PaymentMaturity ,"

                            + " AmountOrder = @AmountOrder ,"
                            + " AmountCurrencyMain = @AmountCurrencyMain ,"
                            + " VAT = @VAT ,"
                            + " VATPercent = @VATPercent ,"

                            + " AmountForTAX = @AmountForTAX ,"
                            + " VATForTAX = @VATForTAX ,"

                            + " CustomerKey = @CustomerKey, "
                            + " CategoryKey = @CategoryKey, "
                            + " IsFixedAsset = @IsFixedAsset, "

                            + " PayByCash = @PayByCash ,"
                            + " PayByAccountBank = @PayByAccountBank, "
                            + " ModifiedBy= @ModifiedBy,"
                            + " ModifiedDateTime=getdate() "

                            + " WHERE InvoiceKey = @InvoiceKey";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = m_InvoiceKey;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = m_InvoiceID;
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = m_InvoiceNumber;
                nCommand.Parameters.Add("@InvoiceSign", SqlDbType.NVarChar).Value = m_InvoiceSign;
                nCommand.Parameters.Add("@InvoiceDesign", SqlDbType.NVarChar).Value = m_InvoiceDesign;
                nCommand.Parameters.Add("@InvoiceDescription", SqlDbType.NText).Value = m_InvoiceDescription;

                nCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = m_InvoiceDate;
                if (m_PaymentMaturity == DateTime.MinValue)
                    nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = m_PaymentMaturity;

                nCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = m_AmountOrder;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;
                nCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = m_VAT;
                nCommand.Parameters.Add("@VATPercent", SqlDbType.Int).Value = m_VATPercent;

                nCommand.Parameters.Add("@AmountForTAX", SqlDbType.Money).Value = m_AmountForTAX;
                nCommand.Parameters.Add("@VATForTAX", SqlDbType.Money).Value = m_VATForTAX;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@IsFixedAsset", SqlDbType.Bit).Value = m_IsFixedAsset;

                nCommand.Parameters.Add("@PayByCash", SqlDbType.Bit).Value = m_PayByCash;
                nCommand.Parameters.Add("@PayByAccountBank", SqlDbType.Bit).Value = m_PayByAccountBank;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {
            string nResult = "";
            string nSQL = "INSERT INTO FNC_Invoice_Sales( "
                        + " InvoiceID, InvoiceNumber, InvoiceSign, InvoiceDate, InvoiceDesign, InvoiceDescription,"
                        + " AmountOrder, AmountCurrencyMain, VAT, VATPercent,AmountForTAX, VATForTAX,  "
                        + " PayByCash,PayByAccountBank, PaymentMaturity, "
                        + " IsFixedAsset, CustomerKey, CategoryKey ,BranchKey, "
                        + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                        + " VALUES( @InvoiceID, @InvoiceNumber, @InvoiceSign, @InvoiceDate, @InvoiceDesign, @InvoiceDescription,"
                        + " @AmountOrder,@AmountCurrencyMain, @VAT, @VATPercent, @AmountForTAX, @VATForTAX, "
                        + " @PayByCash,@PayByAccountBank, @PaymentMaturity, "
                        + " @IsFixedAsset, @CustomerKey, @CategoryKey,@BranchKey, "
                        + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                        + " SELECT InvoiceKey FROM FNC_Invoice_Sales WHERE InvoiceKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = m_InvoiceID;
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = m_InvoiceNumber;
                nCommand.Parameters.Add("@InvoiceSign", SqlDbType.NVarChar).Value = m_InvoiceSign;
                nCommand.Parameters.Add("@InvoiceDesign", SqlDbType.NVarChar).Value = m_InvoiceDesign;
                nCommand.Parameters.Add("@InvoiceDescription", SqlDbType.NText).Value = m_InvoiceDescription;

                nCommand.Parameters.Add("@InvoiceDate", SqlDbType.DateTime).Value = m_InvoiceDate;
                if (m_PaymentMaturity == DateTime.MinValue)
                    nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = m_PaymentMaturity;

                nCommand.Parameters.Add("@AmountOrder", SqlDbType.Money).Value = m_AmountOrder;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;
                nCommand.Parameters.Add("@VAT", SqlDbType.Money).Value = m_VAT;
                nCommand.Parameters.Add("@VATPercent", SqlDbType.Int).Value = m_VATPercent;

                nCommand.Parameters.Add("@AmountForTAX", SqlDbType.Money).Value = m_AmountForTAX;
                nCommand.Parameters.Add("@VATForTAX", SqlDbType.Money).Value = m_VATForTAX;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@IsFixedAsset", SqlDbType.Int).Value = m_IsFixedAsset;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;

                nCommand.Parameters.Add("@PayByCash", SqlDbType.Bit).Value = m_PayByCash;
                nCommand.Parameters.Add("@PayByAccountBank", SqlDbType.Bit).Value = m_PayByAccountBank;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                int nInvoiceKey = 0;
                nResult = nCommand.ExecuteScalar().ToString();
                int.TryParse(nResult, out nInvoiceKey);
                m_InvoiceKey = nInvoiceKey;

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult = "";
            if (m_InvoiceKey == 0)
               nResult= Create();
            else
               nResult= Update();
          
            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FNC_Invoice_Sales WHERE InvoiceKey = @InvoiceKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = m_InvoiceKey;
                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
