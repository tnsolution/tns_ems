﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
using TNLibrary.IVT;

namespace TNLibrary.FNC.Invoices
{
    public class Invoice_Sale_Details_Info
    {
        private int m_InvoiceKey = 0;
        private InventoryProduct m_Item = new InventoryProduct();
        private string m_Message = "";
        public Invoice_Sale_Details_Info()
        {

        }
        #region [ Properties ]
        public int InvoiceKey
        {
            set
            {
                m_InvoiceKey = value;
            }
            get
            {
                return m_InvoiceKey;
            }
        }
        public InventoryProduct Item
        {
            set
            {
                m_Item = value;
            }
            get
            {
                return m_Item;
            }
        }
        public string Message
        {
            get
            {
                return m_Message;
            }
        }
        #endregion 

        #region [ Constructor Update Information ]
        public string Save()
        {

            string nResult = "";

            string nSQL = "INSERT INTO FNC_Invoice_Sale_Details( "
                        + " InvoiceKey, ProductKey, ItemName,ItemUnit,CurrencyID,CurrencyRate,Quantity,UnitPrice, AmountCurrencyMain, AmountCurrencyForeign)"
                        + " VALUES(@InvoiceKey, @ProductKey, @ItemName,@ItemUnit,@CurrencyID,@CurrencyRate,@Quantity,@UnitPrice, @AmountCurrencyMain, @AmountCurrencyForeign)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = m_InvoiceKey;

                nCommand.Parameters.Add("@ProductKey", SqlDbType.Int).Value = Item.Key;
                nCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = Item.ItemName;
                nCommand.Parameters.Add("@ItemUnit", SqlDbType.NVarChar).Value = Item.ItemUnit;

                nCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = Item.CurrencyID;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = Item.CurrencyRate;

                nCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Item.Quantity;
                nCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = Item.UnitPrice;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = Item.AmountCurrencyMain;
                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = Item.AmountCurrencyForeign;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
