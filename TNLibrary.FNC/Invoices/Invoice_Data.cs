﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.Invoices
{
    public class Invoice_Data
    {
        #region [ Purchase Invoice ]
        public static DataTable PurchaseInvoices(int CustomerKey, int FinancialYear)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FNC_Invoice_Purchases "
                + " WHERE CustomerKey = @CustomerKey AND YEAR(InvoiceDate) = @FinancialYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@FinancialYear", SqlDbType.Int).Value = FinancialYear;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable PurchaseInvoices()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber FROM FNC_Invoice_Purchases A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PurchaseInvoices(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Invoice_Purchase_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PurchaseInvoices(DateTime FromDate, DateTime ToDate, string InvoiceID, string InvoiceNumber, string CustomerID, string CustomerName, string CreatedBy)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Invoice_Purchase_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = "%" + InvoiceID + "%";
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = "%" + InvoiceNumber + "%";
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy ;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static int CountPurchaseInvoiceNumber(string InvoiceNumber)
        {
            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT Count(InvoiceNumber) FROM FNC_Invoice_Purchases WHERE (InvoiceNumber = @InvoiceNumber)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NChar).Value = InvoiceNumber;
                nResult = int.Parse(nCommand.ExecuteScalar().ToString());
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static string GetAutoPurchaseInvoiceID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(InvoiceID,@LenIDRight)+1) FROM FNC_Invoice_Purchases "
                        + " WHERE LEFT(InvoiceID,@LenIDLeft) =  @ID AND LEN(InvoiceID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(InvoiceID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            ID = IDLeft + nResult.ToString().PadLeft(IDRight.Length, '0');
            return ID;

        }
        
        public static string GetAuto_PurchaseInvoice_ID(string ID)
        {
          
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL =    " SELECT dbo.AutoInvoicePurchaseID(@ID)";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                nResult = nCommand.ExecuteScalar().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }

        public static double Purchase_TotalAmount(int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Invoice_Purchases "
                        + " WHERE MONTH(InvoiceDate) = @ViewMonth AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Purchase_Customer_TotalAmount(int CustomerKey, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT  ISNULL(SUM(AmountCurrencyMain),0) AS Total FROM FNC_Invoice_Purchases "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(strAmount, out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Purchase_Customer_TotalAmount(int CustomerKey, int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Invoice_Purchases "
                        + " WHERE CustomerKey =@CustomerKey AND MONTH(InvoiceDate) = @ViewMonth AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public static DataTable VendorPayToDay()
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_Invoice_VendorPayToDay";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        #endregion

        #region [Sale Invoice]
        public static DataTable SaleInvoices(int CustomerKey, int FinancialYear)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FNC_Invoice_Sales "
                + " WHERE CustomerKey = @CustomerKey AND YEAR(InvoiceDate) = @FinancialYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@FinancialYear", SqlDbType.Int).Value = FinancialYear;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable SaleInvoices()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber FROM FNC_Invoice_Sales A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable SaleInvoices(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Invoice_Sale_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = "";

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable SaleInvoices(DateTime FromDate, DateTime ToDate, string InvoiceID, string InvoiceNumber, string CustomerID, string CustomerName,string CreatedBy)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Invoice_Sale_List";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@InvoiceID", SqlDbType.NVarChar).Value = "%" + InvoiceID + "%";
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NVarChar).Value = "%" + InvoiceNumber + "%";
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static int CountSaleInvoiceNumber(string InvoiceNumber)
        {
            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT Count(InvoiceNumber) FROM FNC_Invoice_Sales WHERE (InvoiceNumber = @InvoiceNumber)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@InvoiceNumber", SqlDbType.NChar).Value = InvoiceNumber;
                nResult = int.Parse(nCommand.ExecuteScalar().ToString());
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static string GetAutoSaleInVoiceID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(InvoiceID,@LenIDRight)+1) FROM FNC_Invoice_Sales "
                        + " WHERE LEFT(InvoiceID,@LenIDLeft) =  @ID AND LEN(InvoiceID) = @LENID"
                        + " AND ISNUMERIC(RIGHT(InvoiceID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LENID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            ID = IDLeft + nResult.ToString().PadLeft(IDRight.Length, '0');
            return ID;

        }

        public static double Sale_TotalAmount(int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Invoice_Sales "
                        + " WHERE MONTH(InvoiceDate) = @ViewMonth AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Sale_Customer_TotalAmount(int CustomerKey, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT  ISNULL(SUM(AmountCurrencyMain),0) AS Total FROM FNC_Invoice_Sales "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(strAmount, out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Sale_Customer_TotalAmount(int CustomerKey, int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Invoice_Sales "
                        + " WHERE CustomerKey =@CustomerKey AND MONTH(InvoiceDate) = @ViewMonth AND YEAR(InvoiceDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public static string GetAuto_SaleInvoice_ID(string ID)
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT dbo.AutoInvoiceSaleID(@ID)";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                nResult = nCommand.ExecuteScalar().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }

        public static DataTable CustomerPayToDay()
        {
            DataTable nTable = new DataTable();

            string nSQL = "FNC_Invoice_CustomerPayToDay";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        // User 
  
        #endregion
    }
}
