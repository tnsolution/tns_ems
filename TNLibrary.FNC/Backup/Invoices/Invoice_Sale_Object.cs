﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
using TNLibrary.IVT;

namespace TNLibrary.FNC.Invoices
{
    public class Invoice_Sale_Object : Invoice_Sale_Info
    {
        private ArrayList m_Items = new ArrayList();

        #region [ Constructor Get Information ]
        public Invoice_Sale_Object():base()
        {
        }
        public Invoice_Sale_Object(int InvoiceKey)
            : base(InvoiceKey)
        {
            GetItems();
        }

        public Invoice_Sale_Object(string InvoiceID)
            : base(InvoiceID)
        {
            GetItems();
        }

        public void Get_InvoiceNumber_Info(string InvoiceNumber)
        {
            base.GetInvoice_Sale_Info(InvoiceNumber);
            GetItems();
        }
        #endregion 
        
        #region [Properties]

        public ArrayList Items
        {
            set
            {
                m_Items = value; 
            }
            get
            {
               return m_Items ;
               
            }
        }
        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.InvoiceKey > 0)
            {
                DeleteItems();
                SaveItems();
            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DeleteItems();
            return nResult;
        }

        #endregion

        #region [ Items ]
        private void GetItems()
        {
            string strSQL = "SELECT A.*,B.ProductID,B.ProductName,B.Unit "
                         + " FROM FNC_Invoice_Sale_Details A "
                         + " LEFT JOIN PUL_Products B ON A.ProductKey = B.ProductKey "
                         + " WHERE InvoiceKey =@InvoiceKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@InvoiceKey",SqlDbType.Int).Value =  base.InvoiceKey;
                SqlDataReader nReader = nCommand.ExecuteReader();
                while (nReader.Read())
                {
                    InventoryProduct nInventoryProduct = new InventoryProduct();

                    nInventoryProduct.Key = int.Parse(nReader["ProductKey"].ToString());
                    nInventoryProduct.ID = nReader["ProductID"].ToString();
                    nInventoryProduct.Name = nReader["ProductName"].ToString();
                    nInventoryProduct.Unit = nReader["Unit"].ToString();

                    nInventoryProduct.ItemName = nReader["ItemName"].ToString();
                    nInventoryProduct.ItemUnit = nReader["ItemUnit"].ToString();

                    nInventoryProduct.Quantity = float.Parse(nReader["Quantity"].ToString());
                    nInventoryProduct.UnitPrice = double.Parse(nReader["UnitPrice"].ToString());
                    nInventoryProduct.AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());
                    nInventoryProduct.AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());

                    m_Items.Add(nInventoryProduct);

                }
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }


        }
        private void SaveItems()
        {
            int nLeng = m_Items.Count;
            for (int i = 0; i < nLeng; i++)
            {
                InventoryProduct nProduct = (InventoryProduct)m_Items[i];
                Invoice_Sale_Details_Info nItem = new Invoice_Sale_Details_Info();

                nItem.InvoiceKey = base.InvoiceKey;
                nItem.Item = nProduct;
                nItem.Save();
            }
        }
        public string DeleteItems()
        {
            string nResult = "";
            string strSQL = " DELETE FROM FNC_Invoice_Sale_Details WHERE InvoiceKey = @InvoiceKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@InvoiceKey",SqlDbType.Int).Value = base.InvoiceKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

    }
}
