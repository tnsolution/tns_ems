﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;
namespace TNLibrary.FNC.Bills
{
    public class Cash_Payment_Info
    {
        private int m_Key = 0;
        private string m_ID = "";
        private string m_Description = "";
        private DateTime m_BillDate = DateTime.Now;

        private int m_CustomerKey = 0;
        private string m_CustomerID = "";
        private string m_CustomerName = "";

        private string m_Receiver = "";
        private string m_Address = "";

        private double m_AmountCurrencyForeign = 0;
        private string m_CurrencyIDForeign;
        private double m_CurrencyRate = 1;

        private double m_AmountCurrencyMain = 0;

        private double m_AmountFeeBank = 0;
        private double m_AmountVATBank = 0;
        private bool m_IsFeeInside = true;

        private int m_CategoryKey = 1;
        private int m_BranchKey = 0;
        private string m_BranchName = "";

        private string m_AccountStockKey = "";

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime;

        private string m_Message = "";

        public Cash_Payment_Info()
        {

        }

        #region [ Constructor Get Information ]
        public Cash_Payment_Info(int PaymentKey)
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey "
                        + " WHERE PaymentKey =@PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = PaymentKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_Key = int.Parse(nReader["PaymentKey"].ToString());
                    m_ID = nReader["PaymentID"].ToString();
                    m_Description = nReader["PaymentDescription"].ToString();
                    m_BillDate = (DateTime)nReader["PaymentDate"];

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_Receiver = nReader["Receiver"].ToString();
                    m_Address = nReader["Address"].ToString();

                    m_CurrencyIDForeign = nReader["CurrencyIDForeign"].ToString();
                    m_AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());
                    m_CurrencyRate = double.Parse(nReader["CurrencyRate"].ToString());

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_AmountFeeBank = double.Parse(nReader["AmountFeeBank"].ToString());
                    m_AmountVATBank = double.Parse(nReader["AmountVATBank"].ToString());
                    m_IsFeeInside = (bool)nReader["IsFeeInside"];

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_BranchName = nReader["BranchName"].ToString();

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public Cash_Payment_Info(string PaymentID)
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey "
                        + " WHERE PaymentID =@PaymentID";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = PaymentID;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_Key = int.Parse(nReader["PaymentKey"].ToString());
                    m_ID = nReader["PaymentID"].ToString();
                    m_Description = nReader["PaymentDescription"].ToString();
                    m_BillDate = (DateTime)nReader["PaymentDate"];

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_Receiver = nReader["Receiver"].ToString();
                    m_Address = nReader["Address"].ToString();

                    m_CurrencyIDForeign = nReader["CurrencyIDForeign"].ToString();
                    m_AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());
                    m_CurrencyRate = double.Parse(nReader["CurrencyRate"].ToString());

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_AmountFeeBank = double.Parse(nReader["AmountFeeBank"].ToString());
                    m_AmountVATBank = double.Parse(nReader["AmountVATBank"].ToString());
                    m_IsFeeInside = (bool)nReader["IsFeeInside"];

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_BranchName = nReader["BranchName"].ToString();

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }

        public void GetPaymentInfo()
        {
            string nSQL = "SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey "
                        + " WHERE PaymentKey =@PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = m_Key;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_Key = int.Parse(nReader["PaymentKey"].ToString());
                    m_ID = nReader["PaymentID"].ToString();
                    m_Description = nReader["PaymentDescription"].ToString();
                    m_BillDate = (DateTime)nReader["PaymentDate"];

                    m_CustomerKey = int.Parse(nReader["CustomerKey"].ToString());
                    m_CustomerID = nReader["CustomerID"].ToString();
                    m_CustomerName = nReader["CustomerName"].ToString();

                    m_Receiver = nReader["Receiver"].ToString();
                    m_Address = nReader["Address"].ToString();

                    m_CurrencyIDForeign = nReader["CurrencyIDForeign"].ToString();
                    m_AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());
                    m_CurrencyRate = double.Parse(nReader["CurrencyRate"].ToString());

                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_AmountFeeBank = double.Parse(nReader["AmountFeeBank"].ToString());
                    m_AmountVATBank = double.Parse(nReader["AmountVATBank"].ToString());
                    m_IsFeeInside = (bool)nReader["IsFeeInside"];

                    m_CategoryKey = (int)nReader["CategoryKey"];
                    m_BranchKey = (int)nReader["BranchKey"];
                    m_BranchName = nReader["BranchName"].ToString();

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];
                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
        }
        #endregion

        #region [Properties]
        public int Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }
        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public DateTime BillDate
        {
            get { return m_BillDate; }
            set { m_BillDate = value; }
        }

        public int CustomerKey
        {
            get { return m_CustomerKey; }
            set { m_CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }

        public string Receiver
        {
            get { return m_Receiver; }
            set { m_Receiver = value; }
        }
        public string Address
        {
            get { return m_Address; }
            set { m_Address = value; }
        }

        public string CurrencyIDForeign
        {
            get { return m_CurrencyIDForeign; }
            set { m_CurrencyIDForeign = value; }
        }
        public double AmountCurrencyForeign
        {
            get { return m_AmountCurrencyForeign; }
            set { m_AmountCurrencyForeign = value; }
        }
        public double CurrencyRate
        {
            get { return m_CurrencyRate; }
            set { m_CurrencyRate = value; }
        }

        public double AmountCurrencyMain
        {
            get { return m_AmountCurrencyMain; }
            set { m_AmountCurrencyMain = value; }
        }

        public double AmountFeeBank
        {
            get { return m_AmountFeeBank; }
            set { m_AmountFeeBank = value; }
        }
        public double AmountVATBank
        {
            get { return m_AmountVATBank; }
            set { m_AmountVATBank = value; }
        }
        public bool IsFeeInside
        {
            get { return m_IsFeeInside; }
            set { m_IsFeeInside = value; }
        }

        public int CategoryKey
        {
            get { return m_CategoryKey; }
            set { m_CategoryKey = value; }
        }

        public int BranchKey
        {
            get { return m_BranchKey; }
            set { m_BranchKey = value; }
        }

        public string BranchName
        {
            get { return m_BranchName; }
            set { m_BranchName = value; }
        }

        public string AccountStockKey
        {
            get { return m_AccountStockKey; }
            set { m_AccountStockKey = value; }
        }

        public string CreatedBy
        {
            set { m_CreatedBy = value; }
            get { return m_CreatedBy; }
        }
        public DateTime CreatedDateTime
        {
            set { m_CreatedDateTime = value; }
            get { return m_CreatedDateTime; }
        }

        public string ModifiedBy
        {
            set { m_ModifiedBy = value; }
            get { return m_ModifiedBy; }
        }
        public DateTime ModifiedDateTime
        {
            set { m_ModifiedDateTime = value; }
            get { return m_ModifiedDateTime; }
        }

        public string Message
        {
            set { m_Message = value; }
            get { return m_Message; }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Update()
        {
            string nResult = "";
            string nSQL = "UPDATE FNC_Cash_Payments SET "
                            + " PaymentID = @PaymentID,"
                            + " PaymentDescription = @PaymentDescription, "
                            + " PaymentDate = @PaymentDate ,"

                            + " CustomerKey = @CustomerKey, "
                            + " Receiver = @Receiver ,"
                            + " Address = @Address ,"

                            + " CurrencyIDForeign = @CurrencyIDForeign ,"
                            + " AmountCurrencyForeign = @AmountCurrencyForeign ,"
                            + " CurrencyRate = @CurrencyRate ,"

                            + " AmountCurrencyMain = @AmountCurrencyMain ,"

                            + " AmountFeeBank = @AmountFeeBank ,"
                            + " AmountVATBank = @AmountVATBank ,"
                            + " IsFeeInside = @IsFeeInside, "
                            + " CategoryKey = @CategoryKey, "
                  //          + " BranchKey = @BranchKey, "
                            + " AccountStockKey = @AccountStockKey, "
                            + " ModifiedBy= @ModifiedBy,"
                            + " ModifiedDateTime=getdate() "

                            + " WHERE PaymentKey = @PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = m_Key;
                nCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = m_ID;
                nCommand.Parameters.Add("@PaymentDescription", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = m_BillDate;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@Receiver", SqlDbType.NVarChar).Value = m_Receiver;
                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;

                nCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NVarChar).Value = m_CurrencyIDForeign;
                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = m_AmountCurrencyForeign;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = m_CurrencyRate;

                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;

                nCommand.Parameters.Add("@AmountFeeBank", SqlDbType.Money).Value = m_AmountFeeBank;
                nCommand.Parameters.Add("@AmountVATBank", SqlDbType.Money).Value = m_AmountVATBank;
                nCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = m_IsFeeInside;
                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                //nCommand.Parameters.Add("@BranchKey", SqlDbType.NVarChar).Value = m_BranchKey;
                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;

        }
        public string Create()
        {
            string nResult = "";
            string nSQL = "INSERT INTO FNC_Cash_Payments( "
                    + " PaymentID, PaymentDescription, PaymentDate, CustomerKey, Receiver, Address, CurrencyIDForeign, AmountCurrencyForeign,CurrencyRate,"
                    + " AmountCurrencyMain,AmountFeeBank,AmountVATBank, IsFeeInside,CategoryKey,BranchKey,AccountStockKey,"
                    + " CreatedBy,CreatedDateTime,ModifiedBy,ModifiedDateTime) "
                    + " VALUES(@PaymentID, @PaymentDescription, @PaymentDate, @CustomerKey, @Receiver, @Address, @CurrencyIDForeign, @AmountCurrencyForeign,@CurrencyRate,"
                    + " @AmountCurrencyMain,@AmountFeeBank,@AmountVATBank, @IsFeeInside,@CategoryKey,@BranchKey,@AccountStockKey,"
                    + " @CreatedBy,getdate(),@ModifiedBy,getdate())"
                    + " SELECT PaymentKey FROM FNC_Cash_Payments WHERE PaymentKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = m_ID;
                nCommand.Parameters.Add("@PaymentDescription", SqlDbType.NText).Value = m_Description;
                nCommand.Parameters.Add("@PaymentDate", SqlDbType.DateTime).Value = m_BillDate;

                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = m_CustomerKey;
                nCommand.Parameters.Add("@Receiver", SqlDbType.NVarChar).Value = m_Receiver;
                nCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = m_Address;

                nCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NVarChar).Value = m_CurrencyIDForeign;
                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = m_AmountCurrencyForeign;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = m_CurrencyRate;

                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;

                nCommand.Parameters.Add("@AmountFeeBank", SqlDbType.Money).Value = m_AmountFeeBank;
                nCommand.Parameters.Add("@AmountVATBank", SqlDbType.Money).Value = m_AmountVATBank;
                nCommand.Parameters.Add("@IsFeeInside", SqlDbType.Bit).Value = m_IsFeeInside;

                nCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = m_CategoryKey;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = m_BranchKey;
                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();
                int nPaymentKey = 0;
                int.TryParse(nResult, out nPaymentKey);
                m_Key = nPaymentKey;
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Save()
        {
            string nResult = "";
            if (m_Key == 0)
                nResult = Create();
            else
                nResult = Update();


            return nResult;
        }
        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FNC_Cash_Payments WHERE PaymentKey = @PaymentKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@PaymentKey", SqlDbType.Int).Value = m_Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
