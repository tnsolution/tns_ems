﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.Bills
{
    public class Cash_Bills_Data
    {
        #region [ Sercurities Stock ]
        public static DataTable ReceiptList_Ser(DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName,D.AccountStockNumber FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " LEFT JOIN SER_Accounts D ON A.AccountStockKey = CONVERT(nvarchar(50),D.AccountStockKey)"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ReceiptList_Ser(int BranchKey, DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName,D.AccountStockNumber FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " LEFT JOIN SER_Accounts D ON A.AccountStockKey = CONVERT(nvarchar(50),D.AccountStockKey)"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) "
            + " AND A.BranchKey = @BranchKey ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        #endregion
        #region [ Receipt ]
        public static DataTable ReceiptList()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber,C.BranchName FROM FNC_Cash_Receipts A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
                + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ReceiptList(int CustomerKey, int Year)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FNC_Cash_Receipts "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(ReceiptDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable ReceiptList(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Receipts A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
                        + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) "
                        + " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ReceiptList(DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ReceiptList(int BranchKey, DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) "
            + " AND A.BranchKey = @BranchKey ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static string GetAutoReceiptID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(ReceiptID,@LenIDRight)+1) FROM FNC_Cash_Receipts "
                        + " WHERE LEFT(ReceiptID,@LenIDLeft) =  @ID AND LEN(ReceiptID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(ReceiptID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

            int nIDRightNumber;
            if (nResult == 0)
                nIDRightNumber = int.Parse(IDRight) + nResult;
            else
                nIDRightNumber = nResult;

            ID = IDLeft + nIDRightNumber.ToString().PadLeft(IDRight.Length, '0');

            return ID;

        }
        public static string GetAutoReceiptID(string ID, out string IDLeft, out string IDRight, out int NumberBegin)
        {
            IDLeft = "";
            IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(ReceiptID,@LenIDRight)+1) FROM FNC_Cash_Receipts "
                        + " WHERE LEFT(ReceiptID,@LenIDLeft) =  @ID AND LEN(ReceiptID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(ReceiptID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            int nIDRightNumber;
            if (nResult == 0)
                nIDRightNumber = int.Parse(IDRight) + nResult;
            else
                nIDRightNumber = nResult;

            ID = IDLeft + nIDRightNumber.ToString().PadLeft(IDRight.Length, '0');

            NumberBegin = nIDRightNumber;
            return ID;

        }

        public static double Receipt_TotalAmount(int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Receipts "
                        + " WHERE MONTH(ReceiptDate) = @ViewMonth AND YEAR(ReceiptDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Receipt_TotalAmount(int BranchKey,int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Receipts "
                        + " WHERE MONTH(ReceiptDate) = @ViewMonth AND YEAR(ReceiptDate) = @ViewYear"
                        + " AND BranchKey = @BranchKey " ;

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Receipt_Customer_TotalAmount(int CustomerKey, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT  ISNULL(SUM(AmountCurrencyMain),0) AS Total FROM FNC_Cash_Receipts "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(ReceiptDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(strAmount, out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Receipt_Customer_TotalAmount(int CustomerKey, int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Receipts "
                        + " WHERE CustomerKey =@CustomerKey AND MONTH(ReceiptDate) = @ViewMonth AND YEAR(ReceiptDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

        #region [ Payment ]
        public static DataTable PaymentList(int CustomerKey, int Year)
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FNC_Cash_Payments "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(PaymentDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PaymentList()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*,B.CustomerName,B.TaxNumber,C.BranchName FROM FNC_Cash_Payments A"
                + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey"
                + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PaymentList(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
                        + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
                        + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
                        + " WHERE (PaymentDate >= @FromDate AND PaymentDate <=@ToDate) "

                        + " ORDER BY A.PaymentDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PaymentList(DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " WHERE (PaymentDate >= @FromDate AND PaymentDate <=@ToDate) ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.PaymentDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable PaymentList(int BranchKey, DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName FROM FNC_Cash_Payments A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " WHERE (PaymentDate >= @FromDate AND PaymentDate <=@ToDate) "
            + " AND A.BranchKey = @BranchKey ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.PaymentDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static string GetAutoPaymentID(string ID)
        {
            string IDLeft = "";
            string IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(PaymentID,@LenIDRight)+1) FROM FNC_Cash_Payments "
                        + " WHERE LEFT(PaymentID,@LenIDLeft) =  @ID AND LEN(PaymentID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(PaymentID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            int nIDRightNumber;
            if (nResult == 0)
                nIDRightNumber = int.Parse(IDRight) + nResult;
            else
                nIDRightNumber = nResult;

            ID = IDLeft + nIDRightNumber.ToString().PadLeft(IDRight.Length, '0');
            return ID;

        }
        public static string GetAutoPaymentID(string ID, out string IDLeft, out string IDRight, out int NumberBegin)
        {

            IDLeft = "";
            IDRight = "";
            int nLen = ID.Length;
            int k = 1;
            for (int i = nLen - 1; i > 0; i--)
            {
                int isNumber = 0;
                string strNumber = ID.Substring(i, k);
                if (int.TryParse(strNumber, out isNumber))
                {
                    IDRight = strNumber;
                    IDLeft = ID.Substring(0, nLen - k);
                }
                else
                    break;
                k++;
            }

            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = " SELECT MAX(RIGHT(PaymentID,@LenIDRight)+1) FROM FNC_Cash_Payments "
                        + " WHERE LEFT(PaymentID,@LenIDLeft) =  @ID AND LEN(PaymentID) = @LenID"
                        + " AND ISNUMERIC(RIGHT(PaymentID,@LenIDRight)) = 1";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = IDLeft;
                nCommand.Parameters.Add("@LenIDLeft", SqlDbType.Int).Value = IDLeft.Length;
                nCommand.Parameters.Add("@LenIDRight", SqlDbType.Int).Value = IDRight.Length;
                nCommand.Parameters.Add("@LenID", SqlDbType.Int).Value = ID.Length;
                int.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            int nIDRightNumber;
            if (nResult == 0)
                nIDRightNumber = int.Parse(IDRight) + nResult;
            else
                nIDRightNumber = nResult;

            ID = IDLeft + nIDRightNumber.ToString().PadLeft(IDRight.Length, '0');

            NumberBegin = nIDRightNumber;
            return ID;

        }
       
        public static double Payment_TotalAmount(int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Payments "
                        + " WHERE MONTH(PaymentDate) = @ViewMonth AND YEAR(PaymentDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Payment_TotalAmount(int BranchKey,int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Payments "
                        + " WHERE MONTH(PaymentDate) = @ViewMonth AND YEAR(PaymentDate) = @ViewYear"
                        + " AND BranchKey = @BranchKey ";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public static double Payment_Customer_TotalAmount(int CustomerKey, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT  ISNULL(SUM(AmountCurrencyMain),0) AS Total FROM FNC_Cash_Payments "
                        + " WHERE CustomerKey = @CustomerKey AND YEAR(PaymentDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(strAmount, out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public static double Payment_Customer_TotalAmount(int CustomerKey, int Month, int Year)
        {
            double nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT SUM(AmountCurrencyMain) FROM FNC_Cash_Payments "
                        + " WHERE CustomerKey =@CustomerKey AND MONTH(PaymentDate) = @ViewMonth AND YEAR(PaymentDate) = @ViewYear";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@CustomerKey", SqlDbType.Int).Value = CustomerKey;
                nCommand.Parameters.Add("@ViewYear", SqlDbType.Int).Value = Year;
                nCommand.Parameters.Add("@ViewMonth", SqlDbType.Int).Value = Month;
                string strAmount = nCommand.ExecuteScalar().ToString();
                double.TryParse(nCommand.ExecuteScalar().ToString(), out nResult);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

        public static DataTable ReceiptBranchList(DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string strColumn = "WITH PivotTable AS ( SELECT BranchKey ";
            string strPivot = "";

            for (DateTime nNow = FromDate; nNow <= ToDate; nNow = nNow.AddDays(1))
            {
                strColumn += ",[" + nNow.Day.ToString() + "] AS D" + nNow.Day.ToString();
                strPivot += "[" + nNow.Day.ToString() + "],";
            };
            strPivot = strPivot.Remove(strPivot.Length - 1, 1);

            string nSQL = strColumn
                        + " FROM (SELECT BranchKey,DAY(ReceiptDate) AS ReceiptDate ,SUM(AmountCurrencyMain) AS Amount FROM FNC_Cash_Receipts "
                        + " WHERE ReceiptDate >= @FromDate AND ReceiptDate <= @ToDate "
                        + " GROUP BY BranchKey,DAY(ReceiptDate)) P "
                        + " PIVOT ( "
                        + " Sum(Amount) "
                        + " FOR ReceiptDate IN "
                        + " ( " + strPivot + ") "
                        + " ) "
                        + " AS CashFollow )"
                        + " SELECT B.BranchName,A.* FROM PivotTable A "
                        + " LEFT JOIN HRM_Branchs B ON A.BranchKey = B.BranchKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable Cash_Bills(string CloseMonth,DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Cash_Bill_FromDateToDate";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Cash_Bills(int BranchKey,string CloseMonth, DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Cash_Bill_FromDateToDate_Branch";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable Cash_Bills_Branchs(string CloseMonth, DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "FNC_Cash_Bill_FromDateToDate_ALLBranch";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@CloseMonth", SqlDbType.NVarChar).Value = CloseMonth;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }


        #region [ Sercurities ]
        public static DataTable Receipt_Advance_List(DateTime FromDate, DateTime ToDate, string AccountStockNumber)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.AccountStockNumber FROM FNC_Cash_Receipts_Advance A "
            + " LEFT JOIN SER_Accounts B ON A.AccountStockKey = CAST(B.AccountStockKey AS varchar(100)) "
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) ";
            if (AccountStockNumber.Trim().Length > 0)
                nSQL += " AND B.AccountStockNumber Like @AccountStockNumber ";
         
            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@AccountStockNumber", SqlDbType.NVarChar).Value = "%" + AccountStockNumber + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        #endregion

        #region [ Hopital ]
        public static DataTable ReceiptList_Hop(int BranchKey, DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName,D.CategoryName FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " LEFT JOIN FNC_Cash_Categories D ON A.CategoryKey = D.CategoryKey"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) "
            + " AND A.BranchKey = @BranchKey ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;

                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable ReceiptList_Hop(DateTime FromDate, DateTime ToDate, string CustomerID, string CustomerName)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string nSQL = "	SELECT A.*,B.CustomerID,B.CustomerName,C.BranchName,D.CategoryName FROM FNC_Cash_Receipts A "
            + " LEFT JOIN CRM_Customers B ON A.CustomerKey = B.CustomerKey "
            + " LEFT JOIN HRM_Branchs C ON A.BranchKey = C.BranchKey"
            + " LEFT JOIN FNC_Cash_Categories D ON A.CategoryKey = D.CategoryKey"
            + " WHERE (ReceiptDate >= @FromDate AND ReceiptDate <=@ToDate) ";
            if (CustomerID.Trim().Length > 0)
                nSQL += " AND B.CustomerID Like @CustomerID ";
            if (CustomerName.Trim().Length > 0)
                nSQL += " AND B.CustomerName Like @CustomerName ";

            nSQL += " ORDER BY A.ReceiptDate ASC";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = "%" + CustomerID + "%";
                nCommand.Parameters.Add("@CustomerName", SqlDbType.NText).Value = "%" + CustomerName + "%";
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        #endregion
    }
}
