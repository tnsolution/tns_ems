﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.Bills
{
    public class Cash_Receipt_Object : Cash_Receipt_Info
    {
        private DataTable m_InvoicesPaid = new DataTable();

        public Cash_Receipt_Object()
            : base()
        {
            m_InvoicesPaid = GetInvoicesPaid();
        }
        public Cash_Receipt_Object(int ReceiptKey)
            : base(ReceiptKey)
        {
            m_InvoicesPaid = GetInvoicesPaid();
        }
        public Cash_Receipt_Object(string ReceiptID)
            : base(ReceiptID)
        {
            m_InvoicesPaid = GetInvoicesPaid();
        }

        #region [ Properties ]
        public DataTable InvoicesPaid
        {
            set
            {
                m_InvoicesPaid = value;
            }
            get
            {
                return m_InvoicesPaid;
            }
        }
        #endregion

        #region [Constructor Update Information]
        public string SaveObject()
        {
            string nResult = "";
            nResult = base.Save();
            if (base.Key > 0)
            {
                DelInvoicesPaid();
                SaveInvoicesPaid();

            }
            return nResult;
        }
        public string DeleteObject()
        {
            string nResult = "";
            nResult = base.Delete();
            DelInvoicesPaid();

            return nResult;
        }

        #endregion

        #region [ InvoicePiad ]
        private DataTable GetInvoicesPaid()
        {
            DataTable nTable = new DataTable();
            string strSQL =   " SELECT A.*,B.InvoiceNumber,B.InvoiceDate,C.CustomerName FROM FNC_Cash_Receipt_InvoicePaid A "
                            + " LEFT JOIN FNC_Invoice_Sales B ON A.InvoiceKey = B.InvoiceKey "
                            + " LEFT JOIN CRM_Customers C ON B.CustomerKey = C.CustomerKey "
                            + " WHERE A.ReceiptKey = @ReceiptKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@ReceiptKey",SqlDbType.Int).Value = base.Key;
                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);

                nAdapter.Fill(nTable);
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string n_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        private string SaveInvoicesPaid()
        {
            string nResult = "";
            string nSQL = " INSERT INTO FNC_Cash_Receipt_InvoicePaid( "
                        + " ReceiptKey, InvoiceKey, ReceiptAmount)"
                        + " VALUES( @ReceiptKey, @InvoiceKey, @ReceiptAmount)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            SqlCommand nCommand;
            try
            {

                foreach (DataRow nRow in m_InvoicesPaid.Rows)
                {
                    nCommand = new SqlCommand(nSQL, nConnect);
                    nCommand.Parameters.Add("@ReceiptKey", SqlDbType.Int).Value = base.Key;

                    nCommand.Parameters.Add("@InvoiceKey", SqlDbType.Int).Value = nRow["InvoiceKey"].ToString();
                    nCommand.Parameters.Add("@ReceiptAmount", SqlDbType.Money).Value = nRow["ReceiptAmount"].ToString();

                    nResult = nCommand.ExecuteNonQuery().ToString();
                    nCommand.Dispose();

                }
            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        private string DelInvoicesPaid()
        {
            string nResult = "";
            string strSQL = " DELETE FROM FNC_Cash_Receipt_InvoicePaid WHERE ReceiptKey = @ReceiptKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(strSQL, nConnect);

                nCommand.Parameters.Add("@ReceiptKey",SqlDbType.Int).Value =  base.Key;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                nResult = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
