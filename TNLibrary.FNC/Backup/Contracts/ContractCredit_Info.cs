﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TNConfig;

namespace TNLibrary.FNC.Contracts
{
    public class ContractCredit_Info
    {

        private int m_ContractCreditKey = 0;
        private DateTime m_ContractDate = DateTime.Now;
        private string m_ContractNumber = "";
        private int m_ContractType = 0;
        private DateTime m_PaymentMaturity = DateTime.Now;

        private string m_AccountStockKey = "";

        private string m_StockID = "";
        private float m_Quantity = 0;
        private double m_AmountCurrencyMain = 0;

        private double m_AmountCurrencyForeign = 0;
        private string m_CurrencyIDForeign = "USD";
        private double m_CurrencyRate = 1;
        private double m_MoneyRate = 0;
        private double m_Fee = 0;
        private string m_Summarize = "";

        private string m_CreatedBy = "";
        private DateTime m_CreatedDateTime ;
        private string m_ModifiedBy = "";
        private DateTime m_ModifiedDateTime ;

        private string m_Message = "";

        public ContractCredit_Info()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region [ Constructor Get Information ]
        public ContractCredit_Info(int ContractCreditKey)
        {
            string nSQL = "SELECT * FROM FNC_ContractCredit WHERE ContractCreditKey =@ContractCreditKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ContractCreditKey", SqlDbType.Int).Value = ContractCreditKey;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ContractCreditKey = int.Parse(nReader["ContractCreditKey"].ToString());
                    m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    m_ContractNumber = nReader["ContractNumber"].ToString();
                    m_ContractType = int.Parse(nReader["ContractType"].ToString());
                    m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    
                    m_AccountStockKey = nReader["AccountStockKey"].ToString();
                    m_StockID = nReader["StockID"].ToString();
                    m_Quantity = float.Parse(nReader["Quantity"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());
                    m_CurrencyIDForeign = nReader["CurrencyIDForeign"].ToString();
                    m_CurrencyRate = double.Parse(nReader["CurrencyRate"].ToString());
                    m_MoneyRate = double.Parse(nReader["MoneyRate"].ToString());
                    m_Fee = double.Parse(nReader["Fee"].ToString());

                    m_Summarize = nReader["Summarize"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        public ContractCredit_Info(string ContractNumber)
        {
            string nSQL = "SELECT * FROM FNC_ContractCredit WHERE ContractNumber =@ContractNumber";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ContractNumber", SqlDbType.NVarChar).Value = ContractNumber;

                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();

                    m_ContractCreditKey = int.Parse(nReader["ContractCreditKey"].ToString());
                    m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];
                    m_ContractNumber = nReader["ContractNumber"].ToString();
                    m_ContractType = int.Parse(nReader["ContractType"].ToString());
                    m_PaymentMaturity = (DateTime)nReader["PaymentMaturity"];

                    m_AccountStockKey = nReader["AccountStockKey"].ToString();
                    m_StockID = nReader["StockID"].ToString();
                    m_Quantity = float.Parse(nReader["Quantity"].ToString());
                    m_AmountCurrencyMain = double.Parse(nReader["AmountCurrencyMain"].ToString());

                    m_AmountCurrencyForeign = double.Parse(nReader["AmountCurrencyForeign"].ToString());
                    m_CurrencyIDForeign = nReader["CurrencyIDForeign"].ToString();
                    m_CurrencyRate = double.Parse(nReader["CurrencyRate"].ToString());
                    m_MoneyRate = double.Parse(nReader["MoneyRate"].ToString());
                    m_Fee = double.Parse(nReader["Fee"].ToString());

                    m_Summarize = nReader["Summarize"].ToString();

                    m_CreatedBy = nReader["CreatedBy"].ToString();
                    if (nReader["CreatedDateTime"] != DBNull.Value)
                        m_CreatedDateTime = (DateTime)nReader["CreatedDateTime"];

                    m_ModifiedBy = nReader["ModifiedBy"].ToString();
                    if (nReader["ModifiedDateTime"] != DBNull.Value)
                        m_ModifiedDateTime = (DateTime)nReader["ModifiedDateTime"];

                }
                //---- Close Connect SQL ----
                nReader.Close();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }

        }
        #endregion

        #region [ Properties ]

        public int ContractCreditKey
        {
            get { return m_ContractCreditKey; }
            set { m_ContractCreditKey = value; }
        }
        public DateTime ContractDate
        {
            get { return m_ContractDate; }
            set { m_ContractDate = value; }
        }
        public string ContractNumber
        {
            get { return m_ContractNumber; }
            set { m_ContractNumber = value; }
        }
        public int ContractType
        {
            get { return m_ContractType; }
            set { m_ContractType = value; }
        }
        public DateTime PaymentMaturity
        {
            get { return m_PaymentMaturity; }
            set { m_PaymentMaturity = value; }
        }
        
        public string AccountStockKey
        {
            get { return m_AccountStockKey; }
            set { m_AccountStockKey = value; }
        }
        public string StockID
        {
            get { return m_StockID; }
            set { m_StockID = value; }
        }
        public float Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }
        public double AmountCurrencyMain
        {
            get { return m_AmountCurrencyMain; }
            set { m_AmountCurrencyMain = value; }
        }

        public double AmountCurrencyForeign
        {
            get { return m_AmountCurrencyForeign; }
            set { m_AmountCurrencyForeign = value; }
        }
        public string CurrencyIDForeign
        {
            get { return m_CurrencyIDForeign; }
            set { m_CurrencyIDForeign = value; }
        }
        public double CurrencyRate
        {
            get { return m_CurrencyRate; }
            set { m_CurrencyRate = value; }
        }
        public double MoneyRate
        {
            get { return m_MoneyRate; }
            set { m_MoneyRate = value; }
        }
        public double Fee
        {
            get { return m_Fee; }
            set { m_Fee = value; }
        }

        public string Summarize
        {
            get { return m_Summarize; }
            set { m_Summarize = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }

        public double AmountPayed
        {
            get
            {
                double nResult = 0;
                string nSQL = "SELECT isnull(Sum(ReceiptAmount),0) AS SumAmount "
                             + " FROM FNC_Cash_Payment_ContractCreditPaid WHERE ContractCreditKey =@ContractCreditKey";

                string nConnectionString = ConnectDataBase.ConnectionString;
                SqlConnection nConnect = new SqlConnection(nConnectionString);
                nConnect.Open();
                try
                {
                    SqlCommand nCommand = new SqlCommand(nSQL, nConnect);

                    nCommand.CommandType = CommandType.Text;
                    nCommand.Parameters.Add("@ContractCreditKey", SqlDbType.Int).Value = m_ContractCreditKey;
                    nResult = double.Parse(nCommand.ExecuteScalar().ToString());

                    nCommand.Dispose();

                }
                catch (Exception Err)
                {
                    m_Message = Err.ToString();
                }
                finally
                {
                    nConnect.Close();
                }
                return nResult;
            }
        }

        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }
        public DateTime CreatedDateTime
        {
            get { return m_CreatedDateTime; }
            set { m_CreatedDateTime = value; }
        }
        public string ModifiedBy
        {
            get { return m_ModifiedBy; }
            set { m_ModifiedBy = value; }
        }    
        public DateTime ModifiedDateTime
        {
            get { return m_ModifiedDateTime; }
            set { m_ModifiedDateTime = value; }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Update()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "UPDATE FNC_ContractCredit SET "

                        + " ContractDate = @ContractDate ,"
                        + " ContractNumber = @ContractNumber, "
                        + " ContractType = @ContractType,"
                        + " PaymentMaturity = @PaymentMaturity, "
                        
                        + " AccountStockKey = @AccountStockKey, "
                        + " StockID = @StockID, "
                        + " Quantity = @Quantity,"
                        + " AmountCurrencyMain = @AmountCurrencyMain,"

                        + " AmountCurrencyForeign= @AmountCurrencyForeign,"
                        + " CurrencyIDForeign = @CurrencyIDForeign, "
                        + " CurrencyRate = @CurrencyRate, "
                        + " MoneyRate= @MoneyRate,"
                        + " Fee= @Fee,"

                        + " Summarize= @Summarize,"

                        + " ModifiedBy= @ModifiedBy,"
                        + " ModifiedDateTime = getdate() "

                        + " WHERE ContractCreditKey = @ContractCreditKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                // thong so tai khoan
                nCommand.Parameters.Add("@ContractCreditKey", SqlDbType.Int).Value = m_ContractCreditKey;
                nCommand.Parameters.Add("@ContractDate", SqlDbType.DateTime).Value = m_ContractDate;
                nCommand.Parameters.Add("@ContractNumber", SqlDbType.NVarChar).Value = m_ContractNumber;
                nCommand.Parameters.Add("@ContractType", SqlDbType.Int).Value = m_ContractType;
                nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = m_PaymentMaturity;

                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;
                nCommand.Parameters.Add("@StockID", SqlDbType.NVarChar).Value = m_StockID;
                nCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = m_Quantity;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;

                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = m_AmountCurrencyForeign;
                nCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = m_CurrencyIDForeign;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = m_CurrencyRate;
                nCommand.Parameters.Add("@MoneyRate", SqlDbType.Money).Value = m_MoneyRate;
                nCommand.Parameters.Add("@Fee", SqlDbType.Money).Value = m_Fee;

                nCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = m_Summarize;

                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@CreatedDateTime", SqlDbType.DateTime).Value = m_CreatedDateTime;

                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;
                nCommand.Parameters.Add("@ModifiedDateTime", SqlDbType.DateTime).Value = m_ModifiedDateTime;

                nResult = nCommand.ExecuteNonQuery().ToString();

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Create()
        {

            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO FNC_ContractCredit( "
                        + " ContractDate, ContractNumber,ContractType,PaymentMaturity, Summarize,"
                        + " AccountStockKey,StockID,Quantity,AmountCurrencyMain, AmountCurrencyForeign,CurrencyIDForeign,CurrencyRate,MoneyRate,Fee, "
                        + " CreatedBy, CreatedDateTime, ModifiedBy,ModifiedDateTime )"

                        + " VALUES(@ContractDate,@ContractNumber,@ContractType,@PaymentMaturity,@Summarize,"
                        + " @AccountStockKey,@StockID,@Quantity,@AmountCurrencyMain,@AmountCurrencyForeign,@CurrencyIDForeign,@CurrencyRate,@MoneyRate,@Fee, "
                        + " @CreatedBy, getdate(), @ModifiedBy, getdate() )"

                        + " SELECT ContractCreditKey FROM FNC_ContractCredit  WHERE ContractCreditKey = SCOPE_IDENTITY()";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                nCommand.Parameters.Add("@ContractDate", SqlDbType.DateTime).Value = m_ContractDate;
                nCommand.Parameters.Add("@ContractNumber", SqlDbType.NVarChar).Value = m_ContractNumber;
                nCommand.Parameters.Add("@ContractType", SqlDbType.Int).Value = m_ContractType;
                nCommand.Parameters.Add("@PaymentMaturity", SqlDbType.DateTime).Value = m_PaymentMaturity;

                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = m_AccountStockKey;
                nCommand.Parameters.Add("@StockID", SqlDbType.NVarChar).Value = m_StockID;
                nCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = m_Quantity;
                nCommand.Parameters.Add("@AmountCurrencyMain", SqlDbType.Money).Value = m_AmountCurrencyMain;

                nCommand.Parameters.Add("@AmountCurrencyForeign", SqlDbType.Money).Value = m_AmountCurrencyForeign;
                nCommand.Parameters.Add("@CurrencyIDForeign", SqlDbType.NChar).Value = m_CurrencyIDForeign;
                nCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = m_CurrencyRate;
                nCommand.Parameters.Add("@MoneyRate", SqlDbType.Money).Value = m_MoneyRate;
                nCommand.Parameters.Add("@Fee", SqlDbType.Money).Value = m_Fee;

                nCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = m_Summarize;


                nCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = m_CreatedBy;
                nCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = m_ModifiedBy;

                nResult = nCommand.ExecuteScalar().ToString();

                m_ContractCreditKey = int.Parse(nResult);
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        public string Save()
        {
            string nResult = "";
            if (m_ContractCreditKey ==0 )
                nResult = Create();
            else
                nResult = Update();


            return nResult;
        }
        public string Delete()
        {
            string nResult = "";

            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FNC_ContractCredit WHERE ContractCreditKey = @ContractCreditKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@ContractCreditKey", SqlDbType.Int).Value = m_ContractCreditKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
