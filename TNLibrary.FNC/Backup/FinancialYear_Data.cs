﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

using TNConfig;
namespace TNLibrary.FNC
{
    public class FinancialYear_Data
    {
        public static void FillMonth(ComboBox CB, DateTime FromDate, DateTime ToDate)
        {
            CB.Items.Clear();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, 1, 0, 0, 0);

            while (FromDate <= ToDate)
            {
                string nMonth = FromDate.Month.ToString() + "/" + FromDate.Year.ToString();
                if (nMonth.Length < 7)
                    nMonth = "0" + nMonth;
                CB.Items.Add(nMonth);
                FromDate = FromDate.AddMonths(1);
            }


        }
        public static void FinancialMonth(ComboBox CB)
        {
            CB.Items.Clear();

            FinancialYear_Info FinancialYearActivate = new FinancialYear_Info(true);
            DateTime nFromMonth = FinancialYearActivate.FromDate;
            while (nFromMonth <= FinancialYearActivate.ToDate)
            {
                string nFinancialMonth = nFromMonth.Month.ToString() + "/" + nFromMonth.Year.ToString();
                if (nFinancialMonth.Length < 7)
                    nFinancialMonth = "0" + nFinancialMonth;
                CB.Items.Add(nFinancialMonth);
                nFromMonth = nFromMonth.AddMonths(1);
            }


        }
        public static void FinancialQuarter(ComboBox CB)
        {
            CB.Items.Clear();
            string strFromMonth, strToMonth;
            FinancialYear_Info FinancialYearActivate = new FinancialYear_Info(true);
            DateTime nFromMonth = FinancialYearActivate.FromDate;
            while (nFromMonth <= FinancialYearActivate.ToDate)
            {
                strFromMonth = nFromMonth.Month.ToString() + "/" + nFromMonth.Year.ToString();
                if (strFromMonth.Length < 7)
                    strFromMonth = "0" + strFromMonth;
                nFromMonth = nFromMonth.AddMonths(2);
                if (nFromMonth <= FinancialYearActivate.ToDate)
                    strToMonth = nFromMonth.Month.ToString() + "/" + nFromMonth.Year.ToString();
                else
                    strToMonth = FinancialYearActivate.ToDate.Month.ToString() + "/" + FinancialYearActivate.ToDate.Year.ToString();

                if (strToMonth.Length < 7)
                    strToMonth = "0" + strToMonth;

                CB.Items.Add(strFromMonth + "->" + strToMonth);
                nFromMonth = nFromMonth.AddMonths(1);
            }
        }

        public static DataTable Years()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT * FROM FNC_FinancialYears";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter nAdapter = new SqlDataAdapter(nCommand);
                nAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
    }
}
