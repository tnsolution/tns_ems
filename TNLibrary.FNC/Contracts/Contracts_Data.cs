﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using TNConfig;

namespace TNLibrary.FNC.Contracts
{
    public class Contracts_Data
    {
        public static DataTable ListContractCredit()
        {
            DataTable nTable = new DataTable();

            string nSQL = "SELECT A.*, B.AccountStockNumber FROM FNC_ContractCredit A "
                        + "LEFT JOIN SER_Accounts B ON CONVERT(nvarchar(50),B.AccountStockKey) = A.AccountStockKey";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ListContractCredit(string AccountStockKey)
        {
            DataTable nTable = new DataTable();

            string nSQL = " SELECT A.*, B.AccountStockNumber FROM FNC_ContractCredit A "
                        + " LEFT JOIN SER_Accounts B ON A.AccountStockKey = B.AccountStockKey"
                        + " WHERE A.AccountStockKey = @AccountStockKey ";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = AccountStockKey;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static DataTable ListContractCredit(string AccountStockKey,DateTime FromDate,DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 0, 0, 0);

            string nSQL = " SELECT ContractDate,Summarize,StockID,Quantity,"
                        + " AmountCurrencyMain AS AmountBorrow,PaymentMaturity,"
                        + " isnull(dbo.GetContractCreditPaid(ContractCreditKey,@FromDate,@ToDate),0) AS AmountPaid"
                        + " FROM FNC_ContractCredit"
                        + " WHERE (AccountStockKey = CASE @AccountStockKey WHEN '' THEN AccountStockKey ELSE @AccountStockKey END)"
                        + " AND ContractDate >= @FromDate AND ContractDate <= @ToDate";


            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@AccountStockKey", SqlDbType.NVarChar).Value = AccountStockKey;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }
        public static DataTable ListContractCredit(int CompanyKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable nTable = new DataTable();
            FromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 0, 0, 0);

            string nSQL = " SELECT ContractDate,Summarize,StockID,Quantity,"
                        + " AmountCurrencyMain AS AmountBorrow,PaymentMaturity,"
                        + " isnull(dbo.GetContractCreditPaid(ContractCreditKey,@FromDate,@ToDate),0) AS AmountPaid"
                        + " FROM FNC_ContractCredit A"
                        + " INNER JOIN SER_Accounts B ON A.AccountStockKey = B.AccountStockKey"
                        + " WHERE (B.CompanyKey = CASE @CompanyKey WHEN '' THEN B.CompanyKey ELSE @CompanyKey END)"
                        + " AND A.ContractDate >= @FromDate AND A.ContractDate <= @ToDate";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                nCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                nCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                nCommand.Parameters.Add("@CompanyKey", SqlDbType.Int).Value = CompanyKey;

                SqlDataAdapter mAdapter = new SqlDataAdapter(nCommand);
                mAdapter.Fill(nTable);

                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nTable;
        }

        public static int CountContractCreditNumber(string ContractNumber)
        {
            int nResult = 0;

            //---------- String SQL Access Database ---------------
            string nSQL = "SELECT Count(ContractNumber) FROM FNC_ContractCredit WHERE (ContractNumber = @ContractNumber)";

            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@ContractNumber", SqlDbType.NVarChar).Value = ContractNumber;
                nResult = int.Parse(nCommand.ExecuteScalar().ToString());
                nCommand.Dispose();

            }
            catch (Exception Err)
            {
                string m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
    }
}
